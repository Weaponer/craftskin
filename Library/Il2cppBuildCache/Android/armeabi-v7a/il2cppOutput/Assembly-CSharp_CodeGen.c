﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void FileSystem::SaveFile(UnityEngine.Texture2D,System.String)
extern void FileSystem_SaveFile_m108CF3C9AF75778ED78F32912E41616BF150C4E9 (void);
// 0x00000002 TextureFile[] FileSystem::LoadFiles()
extern void FileSystem_LoadFiles_m01D795FF7A5BD65681FDA132855C3FD20412EA1F (void);
// 0x00000003 System.Void FileSystem::CheckDirectory()
extern void FileSystem_CheckDirectory_mD174C46AF8403AA047EE495C7FF593195BDBD802 (void);
// 0x00000004 System.Void TextureFile::.ctor()
extern void TextureFile__ctor_m8EF3A11095670F9C6B22E8DCE40476C34FE6C6E4 (void);
// 0x00000005 System.Void ClickItem::.ctor(System.Object,System.IntPtr)
extern void ClickItem__ctor_m6D5E2FD345695D4BAA58DF428E721D6CF430F6D4 (void);
// 0x00000006 System.Void ClickItem::Invoke(ItemFile)
extern void ClickItem_Invoke_mD291156C5F623C5C5B6D1A1426C0B4E6DCEE2E04 (void);
// 0x00000007 System.IAsyncResult ClickItem::BeginInvoke(ItemFile,System.AsyncCallback,System.Object)
extern void ClickItem_BeginInvoke_mC924A9018511D465F0205F6593661791EE02F74F (void);
// 0x00000008 System.Void ClickItem::EndInvoke(System.IAsyncResult)
extern void ClickItem_EndInvoke_mEB3019651C5123B5C7F827B03E5AFA2C4EEE1856 (void);
// 0x00000009 UnityEngine.Texture2D ItemFile::get_Texture()
extern void ItemFile_get_Texture_m53253EBBBA6158EAB8DCCD722403673B1386CF94 (void);
// 0x0000000A System.Void ItemFile::set_Texture(UnityEngine.Texture2D)
extern void ItemFile_set_Texture_mC9765B001469946A0A8EF7B8065DBBA5F043C972 (void);
// 0x0000000B System.String ItemFile::get_Text()
extern void ItemFile_get_Text_m6C1987DAD8DFE3E1D90C303D0E0AA5D7DD654564 (void);
// 0x0000000C System.Void ItemFile::set_Text(System.String)
extern void ItemFile_set_Text_m31574CAB93FC184D76FB8E097C470695BC89815F (void);
// 0x0000000D System.Void ItemFile::add_Click(ClickItem)
extern void ItemFile_add_Click_m8E9AE2693D5BDE2564D307359FB3FADB33685507 (void);
// 0x0000000E System.Void ItemFile::remove_Click(ClickItem)
extern void ItemFile_remove_Click_mC9B29E8B2CCBC5041E75B90F01168B3CFB6A8E73 (void);
// 0x0000000F System.Void ItemFile::Initialized(UnityEngine.Texture2D,System.String)
extern void ItemFile_Initialized_m7EBC018BC7600B5200E55C9D5B36433A948A8B95 (void);
// 0x00000010 System.Void ItemFile::.ctor()
extern void ItemFile__ctor_m671C49179EA50067B6473BBE1405960478CCD5CF (void);
// 0x00000011 System.Void ItemFile::<Initialized>b__14_0()
extern void ItemFile_U3CInitializedU3Eb__14_0_m7EF2031D3B9BCCEAED92F21DCDFF8641972D7AEB (void);
// 0x00000012 System.Boolean Palitra::op_Equality(Palitra,Palitra)
extern void Palitra_op_Equality_mD64701FDFD65CBCD6539DF6E544C72B83912F00C (void);
// 0x00000013 System.Boolean Palitra::op_Inequality(Palitra,Palitra)
extern void Palitra_op_Inequality_m8C6F6690EF0DF7A88AFC2395FFFFF1570141C932 (void);
// 0x00000014 System.Void Palitra::.ctor()
extern void Palitra__ctor_mE42684B5DA2510A4E21DF619AFFD8508138CF5DB (void);
// 0x00000015 System.Void PalitraDataBase::.ctor()
extern void PalitraDataBase__ctor_mBFEC4CF6BC48EA29C620D914F32B5164ED145BBF (void);
// 0x00000016 Palitra ProjectPalitra::GetPalitra()
extern void ProjectPalitra_GetPalitra_m426101BEAACA8C9F1E4F78C72028A9716DC1C513 (void);
// 0x00000017 System.Void ProjectPalitra::SetPalitra(Palitra)
extern void ProjectPalitra_SetPalitra_mD735E317BC37F0457D09110999E5FF69D9D07DDD (void);
// 0x00000018 System.Void ProjectPalitra::.ctor()
extern void ProjectPalitra__ctor_m011F65405F52D4F31561354A352255DDBBF708D6 (void);
// 0x00000019 System.Void FilerServer.LoadObject::.ctor()
extern void LoadObject__ctor_m373804582DCF64DB169EC21CF81BB37F8150A707 (void);
// 0x0000001A System.Void FilerServer.News::.ctor()
extern void News__ctor_mE8E0D38C162DAFA8E9A7234B34546CE72813115D (void);
// 0x0000001B System.Void FilerServer.Texture::.ctor()
extern void Texture__ctor_m1D3421D9ECEA775F9D63C6F9B12E8A9993419C9D (void);
// 0x0000001C System.Void FilerServer.Base.BaseConnect::LoadListNews(FilerServer.Base.EndLoadNews,System.Action)
extern void BaseConnect_LoadListNews_mDAF8192ECB408674A978F8C1C917221E3E2D7158 (void);
// 0x0000001D System.Void FilerServer.Base.BaseConnect::LoadListNameTexture(FilerServer.Base.EndLoadTexture,System.Action)
extern void BaseConnect_LoadListNameTexture_m9CE94832D364F87B5599EABEFCA6CD12F3C43317 (void);
// 0x0000001E System.Void FilerServer.Base.BaseConnect::LoadListTexture(FilerServer.Base.EndLoadTexture,System.Action)
extern void BaseConnect_LoadListTexture_m2C475BC55BB2233E071AA9C0D34FB6180BCC0959 (void);
// 0x0000001F System.Void FilerServer.Base.BaseConnect::LoadNumUpdateNews(FilerServer.Base.LoadNum,System.Action)
extern void BaseConnect_LoadNumUpdateNews_m90CC6FDBDB86A391083E4627EA721E9B7576F888 (void);
// 0x00000020 System.Void FilerServer.Base.BaseConnect::LoadNumUpdateTexture(FilerServer.Base.LoadNum,System.Action)
extern void BaseConnect_LoadNumUpdateTexture_m2B08CBD5A0956CCE5190107C173106341BFBBA9A (void);
// 0x00000021 System.Void FilerServer.Base.BaseConnect::CreateProcces(FilerServer.Base.BaseConnect/BaseProcces)
extern void BaseConnect_CreateProcces_m4105F16818B435386EDDCEB08D83F3B919B75057 (void);
// 0x00000022 System.Void FilerServer.Base.BaseConnect::RemoveProcces()
extern void BaseConnect_RemoveProcces_m3D2E114B21E59102C6E9B7D3E832F976BAC9FFD7 (void);
// 0x00000023 System.Void FilerServer.Base.BaseConnect::.ctor()
extern void BaseConnect__ctor_m503ECF30A2EFD7BC4972E2776B10B559B37B7EC7 (void);
// 0x00000024 System.Void FilerServer.Base.BaseConnect::.cctor()
extern void BaseConnect__cctor_m4EA8658D9522AE35EFBE8B930E3BA3A7032BF286 (void);
// 0x00000025 System.Void FilerServer.Base.EndLoadNews::.ctor(System.Object,System.IntPtr)
extern void EndLoadNews__ctor_m58AC263DAC22264B35E74DF1A2C408AEC5308EC4 (void);
// 0x00000026 System.Void FilerServer.Base.EndLoadNews::Invoke(FilerServer.News[])
extern void EndLoadNews_Invoke_m7013403953EA78191AE78F998F8FEE0050335948 (void);
// 0x00000027 System.IAsyncResult FilerServer.Base.EndLoadNews::BeginInvoke(FilerServer.News[],System.AsyncCallback,System.Object)
extern void EndLoadNews_BeginInvoke_m9F3CF04D64C7F38DB12CEBF27DC817CF6DCD715B (void);
// 0x00000028 System.Void FilerServer.Base.EndLoadNews::EndInvoke(System.IAsyncResult)
extern void EndLoadNews_EndInvoke_m4F1E659B2472C49C0641822081811688DD40765C (void);
// 0x00000029 System.Void FilerServer.Base.EndLoadTexture::.ctor(System.Object,System.IntPtr)
extern void EndLoadTexture__ctor_m63CE62344F5AA3FBAB65514F7DB8C66CE90955F8 (void);
// 0x0000002A System.Void FilerServer.Base.EndLoadTexture::Invoke(FilerServer.Texture[])
extern void EndLoadTexture_Invoke_m591119F3B3490E5E0FDED267145BDA2A570B7635 (void);
// 0x0000002B System.IAsyncResult FilerServer.Base.EndLoadTexture::BeginInvoke(FilerServer.Texture[],System.AsyncCallback,System.Object)
extern void EndLoadTexture_BeginInvoke_m57F1684A3DB6E7FCD3E82DF5634A2291DE0AAD4E (void);
// 0x0000002C System.Void FilerServer.Base.EndLoadTexture::EndInvoke(System.IAsyncResult)
extern void EndLoadTexture_EndInvoke_m6106EA6719886C9032290AA1E7627EE17EEEC03D (void);
// 0x0000002D System.Void FilerServer.Base.LoadNum::.ctor(System.Object,System.IntPtr)
extern void LoadNum__ctor_mCA4221CA4A0B7BFDDB12D9318C7E2E079AB4A5E0 (void);
// 0x0000002E System.Void FilerServer.Base.LoadNum::Invoke(System.UInt32)
extern void LoadNum_Invoke_mC2A61444A550B9A3985038D2E27ED883B214F14B (void);
// 0x0000002F System.IAsyncResult FilerServer.Base.LoadNum::BeginInvoke(System.UInt32,System.AsyncCallback,System.Object)
extern void LoadNum_BeginInvoke_mB0347C3B46F56B8A73F17FFA796BF49955C9E3D8 (void);
// 0x00000030 System.Void FilerServer.Base.LoadNum::EndInvoke(System.IAsyncResult)
extern void LoadNum_EndInvoke_m29D3333F1477A83D6AA169BF98CD2EDFFB7D855C (void);
// 0x00000031 System.Int32 FilerServer.Base.LoadListNews::get_CountNews()
extern void LoadListNews_get_CountNews_m92E438786ED41318DA81C1FB3DFFE5CF7234F215 (void);
// 0x00000032 System.Void FilerServer.Base.LoadListNews::set_CountNews(System.Int32)
extern void LoadListNews_set_CountNews_m0A24C4C87F4FE1FAC7E3EC4006787F0E0BDB455B (void);
// 0x00000033 System.Collections.Generic.List`1<FilerServer.News> FilerServer.Base.LoadListNews::get_NewsArray()
extern void LoadListNews_get_NewsArray_mF722DA3FD7C30C3EC761EFA3974A3B0F54AE8081 (void);
// 0x00000034 System.Void FilerServer.Base.LoadListNews::set_NewsArray(System.Collections.Generic.List`1<FilerServer.News>)
extern void LoadListNews_set_NewsArray_m166A2E8A25F0880D8C7F6FAAA5ACED2CC6BF12CF (void);
// 0x00000035 System.Boolean FilerServer.Base.LoadListNews::get_IsEndWork()
extern void LoadListNews_get_IsEndWork_m2D63D92610E037657373AA35FF09F5D7BAA25FF7 (void);
// 0x00000036 System.Void FilerServer.Base.LoadListNews::set_IsEndWork(System.Boolean)
extern void LoadListNews_set_IsEndWork_m241DDEEC0039C4CED556498BBDD2F083D8662082 (void);
// 0x00000037 System.Void FilerServer.Base.LoadListNews::CallUpdate()
extern void LoadListNews_CallUpdate_m215A9BE8B8EFA48C4F014613BABB5E32F17C5BB7 (void);
// 0x00000038 System.Void FilerServer.Base.LoadListNews::CallEnd()
extern void LoadListNews_CallEnd_m7AAF1B95C771CA1A48F7A92B6A3C549F48C40A3F (void);
// 0x00000039 System.Void FilerServer.Base.LoadListNews::.ctor()
extern void LoadListNews__ctor_mA27A6E452F4511E02A263929843882BB71761D1E (void);
// 0x0000003A System.Boolean FilerServer.Base.LoadEndUpdateNews::get_IsEndWork()
extern void LoadEndUpdateNews_get_IsEndWork_mDB2748229CAAD5B060BFA6607452B9A14EA3CA8A (void);
// 0x0000003B System.Void FilerServer.Base.LoadEndUpdateNews::set_IsEndWork(System.Boolean)
extern void LoadEndUpdateNews_set_IsEndWork_m8D75DDF4F66504494E3CDAC7DB77313817545D39 (void);
// 0x0000003C System.UInt32 FilerServer.Base.LoadEndUpdateNews::get_Num()
extern void LoadEndUpdateNews_get_Num_mF0D49D7DBB67DE0C6718301F671BC7470E8638C8 (void);
// 0x0000003D System.Void FilerServer.Base.LoadEndUpdateNews::set_Num(System.UInt32)
extern void LoadEndUpdateNews_set_Num_m37601EDFC0456EA7A4BC68273D80ADE82DBAA8A7 (void);
// 0x0000003E System.Void FilerServer.Base.LoadEndUpdateNews::CallUpdate()
extern void LoadEndUpdateNews_CallUpdate_mF67C674CC95DEB050494A6A7340CFC7482960548 (void);
// 0x0000003F System.Void FilerServer.Base.LoadEndUpdateNews::CallEnd()
extern void LoadEndUpdateNews_CallEnd_m1642D02C92D2867E91A9DAC85807FE2764EE3DB2 (void);
// 0x00000040 System.Void FilerServer.Base.LoadEndUpdateNews::.ctor()
extern void LoadEndUpdateNews__ctor_mE96164F858B039E6BDAD178DAC8B06491F5B49F5 (void);
// 0x00000041 System.Boolean FilerServer.Base.LoadEndUpdateTexture::get_IsEndWork()
extern void LoadEndUpdateTexture_get_IsEndWork_mF605A538B16DE996D43EDDB2E79ED8B7AA9999F2 (void);
// 0x00000042 System.Void FilerServer.Base.LoadEndUpdateTexture::set_IsEndWork(System.Boolean)
extern void LoadEndUpdateTexture_set_IsEndWork_m0C3FE4EBB8B3D2DD9FB8AD10A577FC1135F5FA5F (void);
// 0x00000043 System.UInt32 FilerServer.Base.LoadEndUpdateTexture::get_Num()
extern void LoadEndUpdateTexture_get_Num_mA2EE4E82CD39EBA34A69AE2EF5B4F9EECB2BA338 (void);
// 0x00000044 System.Void FilerServer.Base.LoadEndUpdateTexture::set_Num(System.UInt32)
extern void LoadEndUpdateTexture_set_Num_m321BB637BB2B8EE158C93879E71B15FEEB3F36B4 (void);
// 0x00000045 System.Void FilerServer.Base.LoadEndUpdateTexture::CallUpdate()
extern void LoadEndUpdateTexture_CallUpdate_m7307ECF90793401E9D37237710A5ACAA4DA3C01D (void);
// 0x00000046 System.Void FilerServer.Base.LoadEndUpdateTexture::CallEnd()
extern void LoadEndUpdateTexture_CallEnd_mBCB3B8C5B4B174326435ABA642AFB42114B5C6E8 (void);
// 0x00000047 System.Void FilerServer.Base.LoadEndUpdateTexture::.ctor()
extern void LoadEndUpdateTexture__ctor_mA9E190EBC0B597ACFA17A65F18FBC41A4D2D3311 (void);
// 0x00000048 System.Int32 FilerServer.Base.LoadListTextureName::get_CountTexutre()
extern void LoadListTextureName_get_CountTexutre_m147756749F5732A2C443302779F3B9699AD0E7C0 (void);
// 0x00000049 System.Void FilerServer.Base.LoadListTextureName::set_CountTexutre(System.Int32)
extern void LoadListTextureName_set_CountTexutre_m93428BEDC013472F04360688F6DE747A0FFC0926 (void);
// 0x0000004A System.Collections.Generic.List`1<FilerServer.Texture> FilerServer.Base.LoadListTextureName::get_TextureArray()
extern void LoadListTextureName_get_TextureArray_m8947F4DCF10C54A327061AEAC24400D221576596 (void);
// 0x0000004B System.Void FilerServer.Base.LoadListTextureName::set_TextureArray(System.Collections.Generic.List`1<FilerServer.Texture>)
extern void LoadListTextureName_set_TextureArray_m34688F069E93DDBC4E45C2F1A430E98ACD9119A7 (void);
// 0x0000004C System.Boolean FilerServer.Base.LoadListTextureName::get_IsEndWork()
extern void LoadListTextureName_get_IsEndWork_m7DD92BF32C6AD32573965DB0A6E90F3BCEE3BFF7 (void);
// 0x0000004D System.Void FilerServer.Base.LoadListTextureName::set_IsEndWork(System.Boolean)
extern void LoadListTextureName_set_IsEndWork_mD6F1D8C1BA620349A1455C4B252C7D85EF2ABFCC (void);
// 0x0000004E System.Void FilerServer.Base.LoadListTextureName::CallUpdate()
extern void LoadListTextureName_CallUpdate_m723ED8EF851E4E3F8FCD2B050D012D06C0B4D559 (void);
// 0x0000004F System.Void FilerServer.Base.LoadListTextureName::CallEnd()
extern void LoadListTextureName_CallEnd_m3AA822D2B53BA283A3DFF090AACF5EDABA8BE15F (void);
// 0x00000050 System.Void FilerServer.Base.LoadListTextureName::.ctor()
extern void LoadListTextureName__ctor_m443099D6A625CB448ADE3C2FD183AFFA6A9FB0C6 (void);
// 0x00000051 System.Int32 FilerServer.Base.LoadListTexture::get_CountTexutre()
extern void LoadListTexture_get_CountTexutre_m2A4B1FAC71C75827EFA5D64BD81626B19118B0B5 (void);
// 0x00000052 System.Void FilerServer.Base.LoadListTexture::set_CountTexutre(System.Int32)
extern void LoadListTexture_set_CountTexutre_m328768693BBB339C6180E37946D02693CAE85CFE (void);
// 0x00000053 System.Collections.Generic.List`1<FilerServer.Texture> FilerServer.Base.LoadListTexture::get_TextureArray()
extern void LoadListTexture_get_TextureArray_mA6E6EDBE325878BEC96FB301C9D44405193B4DE5 (void);
// 0x00000054 System.Void FilerServer.Base.LoadListTexture::set_TextureArray(System.Collections.Generic.List`1<FilerServer.Texture>)
extern void LoadListTexture_set_TextureArray_m025BC6386770316DB84C605661CF736042EE6619 (void);
// 0x00000055 System.Boolean FilerServer.Base.LoadListTexture::get_IsEndWork()
extern void LoadListTexture_get_IsEndWork_m3052A9D89F7B08D72207ACD47961E9D5C674CB1D (void);
// 0x00000056 System.Void FilerServer.Base.LoadListTexture::set_IsEndWork(System.Boolean)
extern void LoadListTexture_set_IsEndWork_m97BF73EFF2F5B1D964F5951981F639263BBBABB6 (void);
// 0x00000057 System.Void FilerServer.Base.LoadListTexture::CallUpdate()
extern void LoadListTexture_CallUpdate_m6DA05685EC5EC4F9D523E1D49FCBC4681778FD69 (void);
// 0x00000058 System.Void FilerServer.Base.LoadListTexture::CallEnd()
extern void LoadListTexture_CallEnd_m67C18E5A16F8784040E407B8E089E8BD880FD1E4 (void);
// 0x00000059 System.Void FilerServer.Base.LoadListTexture::.ctor()
extern void LoadListTexture__ctor_m932DF32B6D50F85B6A0BF34857E4F93B42C2808B (void);
// 0x0000005A System.Void Program.SaveTextureScene::SaveTexutre(UnityEngine.Texture2D)
extern void SaveTextureScene_SaveTexutre_mD647505B074F7B6999E71C576C82E100B060C400 (void);
// 0x0000005B UnityEngine.Texture2D Program.SaveTextureScene::GetTexture()
extern void SaveTextureScene_GetTexture_m9685D4CD509904E15AC2DEF7BBFBB1C6D9485A99 (void);
// 0x0000005C System.Void Program.DebugQuit::OnApplicationQuit()
extern void DebugQuit_OnApplicationQuit_m94EF4A3530DC1DB5A897861EE191444644D7914D (void);
// 0x0000005D System.Void Program.DebugQuit::.ctor()
extern void DebugQuit__ctor_mE98073CDBD5087BC4A9E2E9F570EF09E38B6B4D3 (void);
// 0x0000005E Program.ProgramStart Program.ProgramStart::get_Singleton()
extern void ProgramStart_get_Singleton_mB8385D9BEC7397EA9A4C13F6909BC82C4D193AC5 (void);
// 0x0000005F System.Void Program.ProgramStart::set_Singleton(Program.ProgramStart)
extern void ProgramStart_set_Singleton_mA48138D894D42ECD87875046BC445E11EABC6318 (void);
// 0x00000060 System.Void Program.ProgramStart::Awake()
extern void ProgramStart_Awake_mD54D916E7AF28B985F97A7694A3695F39AA76708 (void);
// 0x00000061 System.Void Program.ProgramStart::Start()
extern void ProgramStart_Start_m85A40E9800DBC4119BB48C313E6BC3A789DC2EE0 (void);
// 0x00000062 System.Void Program.ProgramStart::OnDestroy()
extern void ProgramStart_OnDestroy_mE905A1EAFE879082FF3058BAA8049345F7B203CB (void);
// 0x00000063 System.Void Program.ProgramStart::.ctor()
extern void ProgramStart__ctor_m2B94D688996A0C2414E900277F641A4C85962B04 (void);
// 0x00000064 Program.UI.DownLayer Program.UI.BodyPanel::get_LayerDown()
extern void BodyPanel_get_LayerDown_m29403D6F813A24BCB8A9239FDD6573D75D3C9D7D (void);
// 0x00000065 System.Void Program.UI.BodyPanel::set_LayerDown(Program.UI.DownLayer)
extern void BodyPanel_set_LayerDown_m065C5DE1A0C6D76E23A669094FEC4AC4718F19A4 (void);
// 0x00000066 Program.UI.BodyParts Program.UI.BodyPanel::get_Parts()
extern void BodyPanel_get_Parts_m1410D3E832A1F784DF4C35D996A6FADA5EFE4F18 (void);
// 0x00000067 System.Void Program.UI.BodyPanel::set_Parts(Program.UI.BodyParts)
extern void BodyPanel_set_Parts_m3DA8F269E710CC9622904725F13135E25DBE358E (void);
// 0x00000068 Program.UI.Render Program.UI.BodyPanel::get_StartRender()
extern void BodyPanel_get_StartRender_mAAB7A1D9289510C59929934F34C22C74FD0BE8AF (void);
// 0x00000069 System.Void Program.UI.BodyPanel::set_StartRender(Program.UI.Render)
extern void BodyPanel_set_StartRender_m65096D7AAFD44A49671DF36EC38EDD7ED0DCC304 (void);
// 0x0000006A Program.UI.UVPanel Program.UI.BodyPanel::get_UVEditor()
extern void BodyPanel_get_UVEditor_mB67B5BA8BC6C9B3D828712FFE97D6D5E3209E8CE (void);
// 0x0000006B System.Void Program.UI.BodyPanel::set_UVEditor(Program.UI.UVPanel)
extern void BodyPanel_set_UVEditor_mD108AECBA4C41EAD2B20CE1795C6B500B7706AB8 (void);
// 0x0000006C Program.UI.BodyPanel Program.UI.BodyPanel::get_Singleton()
extern void BodyPanel_get_Singleton_m1E3FF48D1E92D595061ECA0A09ACA769629914BF (void);
// 0x0000006D System.Void Program.UI.BodyPanel::set_Singleton(Program.UI.BodyPanel)
extern void BodyPanel_set_Singleton_m0AC44F12E77EE31F4B7A84DEF68A9BC1D4B5443D (void);
// 0x0000006E System.Void Program.UI.BodyPanel::Awake()
extern void BodyPanel_Awake_m8E3CC9541D5AF77E1D5322A88BA9B31AE990888A (void);
// 0x0000006F System.Void Program.UI.BodyPanel::SetOptionBodyParts()
extern void BodyPanel_SetOptionBodyParts_m4A038D594A81CFCEB333ABDF5D9272C599464CCD (void);
// 0x00000070 System.Void Program.UI.BodyPanel::SetHead()
extern void BodyPanel_SetHead_m257B56D83527FD494602BE67DE2B5A49ECBD9471 (void);
// 0x00000071 System.Void Program.UI.BodyPanel::SetBody()
extern void BodyPanel_SetBody_m19FF96A89EC1776057126E3855BFB49F26036F94 (void);
// 0x00000072 System.Void Program.UI.BodyPanel::SetLHand()
extern void BodyPanel_SetLHand_mCF620CA77D5F0747056CC8641E8E09A85FEFF8BF (void);
// 0x00000073 System.Void Program.UI.BodyPanel::SetRHand()
extern void BodyPanel_SetRHand_m25546DCB8927C4BBC9BEFDF01B7116CF0D92E7F9 (void);
// 0x00000074 System.Void Program.UI.BodyPanel::SetLFut()
extern void BodyPanel_SetLFut_m591B0519D465B230D58E29F06749440FD012740F (void);
// 0x00000075 System.Void Program.UI.BodyPanel::SetRFut()
extern void BodyPanel_SetRFut_m8CA1CDFE057BBDDD5A142C7387968F3FCB3E0BD9 (void);
// 0x00000076 System.Void Program.UI.BodyPanel::OnDestroy()
extern void BodyPanel_OnDestroy_m63C378AA8E134DF4A49F54853896C3E0B83399B5 (void);
// 0x00000077 System.Void Program.UI.BodyPanel::.ctor()
extern void BodyPanel__ctor_m2E0EFB2A7793A0E71CE6A98A2290BF6062A3F708 (void);
// 0x00000078 System.Void Program.UI.BodyPanel::<SetOptionBodyParts>b__36_1()
extern void BodyPanel_U3CSetOptionBodyPartsU3Eb__36_1_m33AD094949C86A3429421A06C84D0877DEC3330E (void);
// 0x00000079 System.Void Program.UI.BodyPanel::<SetOptionBodyParts>b__36_2()
extern void BodyPanel_U3CSetOptionBodyPartsU3Eb__36_2_mF1D6080E2B01C3DB832325EDE01AFDDAED438455 (void);
// 0x0000007A System.Void Program.UI.BodyPanel::<SetOptionBodyParts>b__36_3()
extern void BodyPanel_U3CSetOptionBodyPartsU3Eb__36_3_m95003663D46376F659843D0A630C3DAA8AA47551 (void);
// 0x0000007B System.Void Program.UI.BodyPanel::<SetOptionBodyParts>b__36_4()
extern void BodyPanel_U3CSetOptionBodyPartsU3Eb__36_4_m0537CE1C4AD26325D752F5A8497E8284FEB05898 (void);
// 0x0000007C System.Void Program.UI.BodyPanel::<SetOptionBodyParts>b__36_5()
extern void BodyPanel_U3CSetOptionBodyPartsU3Eb__36_5_mBEBBE5BD5BEE427611BD3658AC3BAE00A1496E63 (void);
// 0x0000007D System.Void Program.UI.BodyPanel::<SetOptionBodyParts>b__36_6()
extern void BodyPanel_U3CSetOptionBodyPartsU3Eb__36_6_mAF2CD75448661E15E8A71C2FB05EE01D7C12BA74 (void);
// 0x0000007E System.Void Program.UI.BodyParts::add_EditBodyParts(System.Action)
extern void BodyParts_add_EditBodyParts_m6D2CFA64ED6B35DF789F4D17BD52D2672429E2F0 (void);
// 0x0000007F System.Void Program.UI.BodyParts::remove_EditBodyParts(System.Action)
extern void BodyParts_remove_EditBodyParts_mF3DDA185AAED7C47D3E5DDF8BC2F73EAE334A722 (void);
// 0x00000080 System.Boolean Program.UI.BodyParts::get_IsHead()
extern void BodyParts_get_IsHead_m42F3D3C3D95E4E17D8A1A761F2A1F0EF09ADC616 (void);
// 0x00000081 System.Void Program.UI.BodyParts::set_IsHead(System.Boolean)
extern void BodyParts_set_IsHead_mA0DED22213717E97534AF67B9A4A3CC726BED00E (void);
// 0x00000082 System.Boolean Program.UI.BodyParts::get_IsBody()
extern void BodyParts_get_IsBody_m1AD8FE7BE4C98A163A0FB88D1873E91FF5065BDE (void);
// 0x00000083 System.Void Program.UI.BodyParts::set_IsBody(System.Boolean)
extern void BodyParts_set_IsBody_m3EC06336AFEACB098D6BD4A5E058A477C4B776AB (void);
// 0x00000084 System.Boolean Program.UI.BodyParts::get_IsLHand()
extern void BodyParts_get_IsLHand_mCFC75AF18DAA2D51DC9D1A52D07D786782C3DEB9 (void);
// 0x00000085 System.Void Program.UI.BodyParts::set_IsLHand(System.Boolean)
extern void BodyParts_set_IsLHand_m499D7DC626E85298A8E4C6DCC5A2619511EEB2ED (void);
// 0x00000086 System.Boolean Program.UI.BodyParts::get_IsRHand()
extern void BodyParts_get_IsRHand_m01EC79BE6703A765722F61B152BB06A50F66958D (void);
// 0x00000087 System.Void Program.UI.BodyParts::set_IsRHand(System.Boolean)
extern void BodyParts_set_IsRHand_m58873CBE20EB089705AE37B173D11E7DCC16F075 (void);
// 0x00000088 System.Boolean Program.UI.BodyParts::get_IsLFut()
extern void BodyParts_get_IsLFut_mD08598F8903B02CE24E20C912EF7EBD7E9F383EC (void);
// 0x00000089 System.Void Program.UI.BodyParts::set_IsLFut(System.Boolean)
extern void BodyParts_set_IsLFut_mA923404D97E0EBC15C5E34756BB34351DB200461 (void);
// 0x0000008A System.Boolean Program.UI.BodyParts::get_IsRFut()
extern void BodyParts_get_IsRFut_m1D0ACB3E78A8FA9EF4DC6ABF07511A337ED9D69F (void);
// 0x0000008B System.Void Program.UI.BodyParts::set_IsRFut(System.Boolean)
extern void BodyParts_set_IsRFut_mC32D6936657AE95FA5FB9E9AAA5EC78E88372D66 (void);
// 0x0000008C System.Void Program.UI.BodyParts::EditBody()
extern void BodyParts_EditBody_m7995F9EB6F65213B19BFD603040D0A0328CF651B (void);
// 0x0000008D System.Void Program.UI.BodyParts::.ctor()
extern void BodyParts__ctor_mD4A7F64D4D88BBDD928496D24388013CF0899520 (void);
// 0x0000008E System.Void Program.UI.DownLayer::add_EditAction(System.Action)
extern void DownLayer_add_EditAction_m2ABA6CB845A909612F0BEB7E32B27DF2C9FFACCD (void);
// 0x0000008F System.Void Program.UI.DownLayer::remove_EditAction(System.Action)
extern void DownLayer_remove_EditAction_mF49977A5F1993324D174BFAF65662BF21FED50D9 (void);
// 0x00000090 System.Boolean Program.UI.DownLayer::get_Enable()
extern void DownLayer_get_Enable_m49AAD772F67A74CE9EA1E2EF80369E7963C9F42A (void);
// 0x00000091 System.Void Program.UI.DownLayer::set_Enable(System.Boolean)
extern void DownLayer_set_Enable_m0BF1AB18F2A620C840531101B5CC7B33022E150A (void);
// 0x00000092 System.Void Program.UI.DownLayer::.ctor()
extern void DownLayer__ctor_m0C9CD01A068E620A429B5469AFE56E29F3F95EA9 (void);
// 0x00000093 System.Void Program.UI.Render::add_StartRenderEvent(System.Action)
extern void Render_add_StartRenderEvent_mB699EF95F1CDBD4322088BE240531555963D510A (void);
// 0x00000094 System.Void Program.UI.Render::remove_StartRenderEvent(System.Action)
extern void Render_remove_StartRenderEvent_m1AF5E76CEDE5897C4DCA7FD2A2A2A33D2BD6D62A (void);
// 0x00000095 System.Void Program.UI.Render::StartRender()
extern void Render_StartRender_mD3B88E93D93D28AC89F251010D7B5D6ED1A77E91 (void);
// 0x00000096 System.Void Program.UI.Render::.ctor()
extern void Render__ctor_m5784A02A02B05F34AF7F4D59AB1D328F206664F6 (void);
// 0x00000097 System.Boolean Program.UI.CenterPanel::get_IsOpen()
extern void CenterPanel_get_IsOpen_m940BF4B14CFCDE8712C73BA4977DC35915E4A8D9 (void);
// 0x00000098 System.Void Program.UI.CenterPanel::set_IsOpen(System.Boolean)
extern void CenterPanel_set_IsOpen_m152CBDFFA76B9CC104CCD915BCCBEF5DFC04561B (void);
// 0x00000099 System.Void Program.UI.CenterPanel::OpenPanel()
extern void CenterPanel_OpenPanel_m5E24F72F798F2EE904E3B0FCEE70E386C664F6A6 (void);
// 0x0000009A System.Void Program.UI.CenterPanel::OpenCallBack()
extern void CenterPanel_OpenCallBack_mDC7562EABACD6F7999129BA6F65973347F478AD2 (void);
// 0x0000009B System.Void Program.UI.CenterPanel::ClosePanel()
extern void CenterPanel_ClosePanel_mEDEA21A75A657D29CD9B31E63E43B0C005109990 (void);
// 0x0000009C System.Void Program.UI.CenterPanel::CloseCallBack()
extern void CenterPanel_CloseCallBack_mE011EDEA85AEB75EB82A25AA7E29839EE94FCE1A (void);
// 0x0000009D System.Void Program.UI.CenterPanel::.ctor()
extern void CenterPanel__ctor_mAB54FC10825229EC6B016CE6695719C22015D131 (void);
// 0x0000009E System.Void Program.UI.DefaultCamera::add_ClickButton(System.Action)
extern void DefaultCamera_add_ClickButton_m24CEFEDB1358B9016454A446AF9051A56A3B2F15 (void);
// 0x0000009F System.Void Program.UI.DefaultCamera::remove_ClickButton(System.Action)
extern void DefaultCamera_remove_ClickButton_m84647F94186D6F54187B10DFC40058CB701D2F47 (void);
// 0x000000A0 System.Void Program.UI.DefaultCamera::Start()
extern void DefaultCamera_Start_m95B80564FACF909DF7941E436BEF5951D4C3C326 (void);
// 0x000000A1 System.Void Program.UI.DefaultCamera::.ctor()
extern void DefaultCamera__ctor_mFE90765E3CFB763FE7039D02761F13E67F9F4922 (void);
// 0x000000A2 System.Void Program.UI.LoadLibraryPanel::Start()
extern void LoadLibraryPanel_Start_mBD92D971A7618E7C3BAD6AED0862A5F287C60BD4 (void);
// 0x000000A3 System.Collections.IEnumerator Program.UI.LoadLibraryPanel::LoadTextureWait()
extern void LoadLibraryPanel_LoadTextureWait_mA618B45C0CF015FB67E0CC2BAEC5C514C04186EA (void);
// 0x000000A4 System.Void Program.UI.LoadLibraryPanel::OpenPanle()
extern void LoadLibraryPanel_OpenPanle_m7A6C3B183C93A9E02CB7F38666BEBB5ECC100F4E (void);
// 0x000000A5 System.Void Program.UI.LoadLibraryPanel::LoadFile()
extern void LoadLibraryPanel_LoadFile_mF21A035C852043735D313E39C15E1F5588253994 (void);
// 0x000000A6 System.Void Program.UI.LoadLibraryPanel::LoadList(TextureFile[])
extern void LoadLibraryPanel_LoadList_mAB71BDBC92DD8B3E5FCE30F951A9AAABD689263B (void);
// 0x000000A7 System.Void Program.UI.LoadLibraryPanel::OpenCallBack()
extern void LoadLibraryPanel_OpenCallBack_m77152D53BA2B4BCB869166E02C664363F3CB5D5D (void);
// 0x000000A8 System.Void Program.UI.LoadLibraryPanel::SetLoadFile(ItemFile)
extern void LoadLibraryPanel_SetLoadFile_mC29833F0C40F2872B3C32E5CA8595981C646A757 (void);
// 0x000000A9 System.Void Program.UI.LoadLibraryPanel::CloseCallBack()
extern void LoadLibraryPanel_CloseCallBack_m1FF1FC290FAACB1704C6AA606BFA2689B53977DF (void);
// 0x000000AA System.Void Program.UI.LoadLibraryPanel::.ctor()
extern void LoadLibraryPanel__ctor_m28C5C9D3EA7CA3AD9A9779851A0F124230CEDAE4 (void);
// 0x000000AB System.Void Program.UI.LoadLibraryPanel::<Start>b__10_0()
extern void LoadLibraryPanel_U3CStartU3Eb__10_0_m77BEB50BC1A88F7C4AF9BB91AE23E0B59E668CE4 (void);
// 0x000000AC System.Void Program.UI.LoadLibraryPanel::<Start>b__10_1()
extern void LoadLibraryPanel_U3CStartU3Eb__10_1_m510E620AFE2724D80700CB9844057BB36345D472 (void);
// 0x000000AD System.Void Program.UI.LoadLibraryPanel::<Start>b__10_2(System.String)
extern void LoadLibraryPanel_U3CStartU3Eb__10_2_m4960AF4EC184CD83F48532911CDB32D3CD917A17 (void);
// 0x000000AE System.Void Program.UI.LoadPanel::Start()
extern void LoadPanel_Start_mB4883A1B57D5041D93536C1A2D7274D83729B733 (void);
// 0x000000AF System.Void Program.UI.LoadPanel::Load()
extern void LoadPanel_Load_m61D1DE91738CCE1A7D87A099B78BC2FBD76080BD (void);
// 0x000000B0 System.Void Program.UI.LoadPanel::SetLoadFile(ItemFile)
extern void LoadPanel_SetLoadFile_m5A8B7893EB4DF14C85673D59896B28C606C626FC (void);
// 0x000000B1 System.Void Program.UI.LoadPanel::LoadFile()
extern void LoadPanel_LoadFile_mB2F702D225009D9E5DBE6AFEB22E85418479F3C5 (void);
// 0x000000B2 System.Void Program.UI.LoadPanel::LoadList(TextureFile[])
extern void LoadPanel_LoadList_m89CA4D3ED73BDAAEB057FEA96BE69FC01D089577 (void);
// 0x000000B3 System.Void Program.UI.LoadPanel::OpenCallBack()
extern void LoadPanel_OpenCallBack_mA3009F302B2EDB7ED4589544FC588C8842F02D23 (void);
// 0x000000B4 System.Void Program.UI.LoadPanel::CloseCallBack()
extern void LoadPanel_CloseCallBack_m712A1A826CDE3AFF67E49E6CE1B5D45C16CAEFD6 (void);
// 0x000000B5 System.Void Program.UI.LoadPanel::.ctor()
extern void LoadPanel__ctor_mAFAEA8C101074ACE39F13DAFE50C9EA91BA34258 (void);
// 0x000000B6 System.Void Program.UI.LoadPanel::<Start>b__11_0()
extern void LoadPanel_U3CStartU3Eb__11_0_m5F905EF6A739AA5B00F23B22FD2BF2EDF64E025C (void);
// 0x000000B7 System.Void Program.UI.LoadPanel::<Start>b__11_1()
extern void LoadPanel_U3CStartU3Eb__11_1_mDF50C8201D6487D2849659B9ADEC3C9B7F2986DC (void);
// 0x000000B8 System.Void Program.UI.LoadPanel::<Start>b__11_2(System.String)
extern void LoadPanel_U3CStartU3Eb__11_2_mE799FD27D48690B248A84024F97E0E2AC1B22915 (void);
// 0x000000B9 System.Void Program.UI.NewFilePanel::Start()
extern void NewFilePanel_Start_mEBDC8C1FB963BEA5423C96FC8EDD3B46C9CB0788 (void);
// 0x000000BA System.Void Program.UI.NewFilePanel::SetButton()
extern void NewFilePanel_SetButton_m679C1138A349DA27B2BD89DFF004260858F10BE0 (void);
// 0x000000BB System.Void Program.UI.NewFilePanel::OpenCallBack()
extern void NewFilePanel_OpenCallBack_m88E732B808F15D4CDD651D229C0A5D69E96C393E (void);
// 0x000000BC System.Void Program.UI.NewFilePanel::CreateNew()
extern void NewFilePanel_CreateNew_m8A6D61F2F84A77453EDB6C6B6E5CF7164BA1F785 (void);
// 0x000000BD System.Void Program.UI.NewFilePanel::CloseCallBack()
extern void NewFilePanel_CloseCallBack_m02109F5D5BDBC058E6F6CD5A2A1EFDFACEE08E2D (void);
// 0x000000BE System.Void Program.UI.NewFilePanel::.ctor()
extern void NewFilePanel__ctor_m1EA708788406A70A7F2E742090F6CAA171230343 (void);
// 0x000000BF System.Void Program.UI.NewFilePanel::<Start>b__4_0()
extern void NewFilePanel_U3CStartU3Eb__4_0_m2D133F62A9D1BFF48F5302C4D9A6BB5C1C0C1BC9 (void);
// 0x000000C0 System.Void Program.UI.NewFilePanel::<Start>b__4_1()
extern void NewFilePanel_U3CStartU3Eb__4_1_m26F08126DE7B00399E9508433CE748BED69FAC36 (void);
// 0x000000C1 System.Void Program.UI.SavePanel::Start()
extern void SavePanel_Start_m670DA1CB11A0A31E2EC033A345F41A55998DDC21 (void);
// 0x000000C2 System.Void Program.UI.SavePanel::SaveFile()
extern void SavePanel_SaveFile_m1573E224FC03A8055C9C143C338A4020C2081E52 (void);
// 0x000000C3 System.Void Program.UI.SavePanel::Save()
extern void SavePanel_Save_mADBF1F121ED20492111FC0F0BBB16DE6BB39CF98 (void);
// 0x000000C4 System.Void Program.UI.SavePanel::LoadList(TextureFile[])
extern void SavePanel_LoadList_m24B38B52D657924E0C054520DCA3BAAD97E89ABD (void);
// 0x000000C5 System.Void Program.UI.SavePanel::OpenCallBack()
extern void SavePanel_OpenCallBack_m02D57782771C41F3C074A61D7DECC01216BC794F (void);
// 0x000000C6 System.Void Program.UI.SavePanel::CloseCallBack()
extern void SavePanel_CloseCallBack_mBE94A72C2A44EF9D734DF87D3E23148AADD557F6 (void);
// 0x000000C7 System.Void Program.UI.SavePanel::.ctor()
extern void SavePanel__ctor_m0B7AAEDB1C00497502CCF41AEB50EC92BF3E73A2 (void);
// 0x000000C8 System.Void Program.UI.SavePanel::<Start>b__9_0()
extern void SavePanel_U3CStartU3Eb__9_0_m06526363D4ED4F05D85EDFFB06F24408C0A902C1 (void);
// 0x000000C9 System.Void Program.UI.SavePanel::<Start>b__9_1()
extern void SavePanel_U3CStartU3Eb__9_1_m2800BE4939AD9E8A9CCFD2B5E5EC375596E7124C (void);
// 0x000000CA System.Void Program.UI.ControllerLaguage::add_EditLanguage(System.Action)
extern void ControllerLaguage_add_EditLanguage_m0E43F99E1EDBEB3A9D92AD2B32D2BAEDE10C73E3 (void);
// 0x000000CB System.Void Program.UI.ControllerLaguage::remove_EditLanguage(System.Action)
extern void ControllerLaguage_remove_EditLanguage_mFDD958D17D6B48C6330DD42B9ADEA5DD5338DF6E (void);
// 0x000000CC System.Boolean Program.UI.ControllerLaguage::get_isActive()
extern void ControllerLaguage_get_isActive_mAAB75859C66032897913E16442299EBF5E0E91D2 (void);
// 0x000000CD System.Void Program.UI.ControllerLaguage::set_isActive(System.Boolean)
extern void ControllerLaguage_set_isActive_mD16197CBBC968E72943AB612C47A75039709877F (void);
// 0x000000CE System.Void Program.UI.ControllerLaguage::Start()
extern void ControllerLaguage_Start_m5C79F6602A22F2DA26AA324B4A6DF9B575E39FB9 (void);
// 0x000000CF System.Void Program.UI.ControllerLaguage::Edit()
extern void ControllerLaguage_Edit_m00C7535E8AE416C44701DE76D47028A4888EB6F4 (void);
// 0x000000D0 System.Object Program.UI.ControllerLaguage::GetOption()
extern void ControllerLaguage_GetOption_mCDA69CA09D8C276B5DBF514138C5F951F229749F (void);
// 0x000000D1 System.Void Program.UI.ControllerLaguage::SetOption(System.Object)
extern void ControllerLaguage_SetOption_mDAACB38C553D0FFA59CDE218000CB48D465A31AF (void);
// 0x000000D2 System.Void Program.UI.ControllerLaguage::.ctor()
extern void ControllerLaguage__ctor_m7A5AF281A7046416FF01DDC6C6C76F233DFA3088 (void);
// 0x000000D3 System.Void Program.UI.ControllerLaguage::<Start>b__8_0()
extern void ControllerLaguage_U3CStartU3Eb__8_0_mE39D5D344153FFEFA57B2B535131900903E72576 (void);
// 0x000000D4 System.Void Program.UI.EditText::OnEnable()
extern void EditText_OnEnable_m48A38A368DB88D4DB03EFE73264C37D45F47C6E2 (void);
// 0x000000D5 System.Void Program.UI.EditText::Edit()
extern void EditText_Edit_m288BE14166DF99DDD8F4421B3D459AE29A34CAAC (void);
// 0x000000D6 System.Void Program.UI.EditText::SetParams(System.String,System.String)
extern void EditText_SetParams_m360EB8E50403904BDEBF85DE6F5808298061210F (void);
// 0x000000D7 System.Void Program.UI.EditText::OnDisable()
extern void EditText_OnDisable_m45925E645AACA1BF85B0B5DB7E1D254B2F21B905 (void);
// 0x000000D8 System.Void Program.UI.EditText::.ctor()
extern void EditText__ctor_m1732C5E869D7DEC1624585578F5D9917515CBA8C (void);
// 0x000000D9 System.Void Program.UI.MenuPanel::add_SaveFile(System.Action)
extern void MenuPanel_add_SaveFile_m05694E995B00E0A5B7C2398D638FFA488C6D9347 (void);
// 0x000000DA System.Void Program.UI.MenuPanel::remove_SaveFile(System.Action)
extern void MenuPanel_remove_SaveFile_m353A16A62CAB468992DE601B9BCF7C82DE5124D5 (void);
// 0x000000DB System.Void Program.UI.MenuPanel::add_LoadFile(System.Action)
extern void MenuPanel_add_LoadFile_m6BB857F0CAF2C51B2C0963F45B1579AEF5851978 (void);
// 0x000000DC System.Void Program.UI.MenuPanel::remove_LoadFile(System.Action)
extern void MenuPanel_remove_LoadFile_m16C9D55F4A513E0B64BE9488511963928A78E4E6 (void);
// 0x000000DD System.Void Program.UI.MenuPanel::add_NewFile(System.Action)
extern void MenuPanel_add_NewFile_m41EF1125E326092467EB209A4928228AE1B03026 (void);
// 0x000000DE System.Void Program.UI.MenuPanel::remove_NewFile(System.Action)
extern void MenuPanel_remove_NewFile_mA3249B7015A2BCF69645E1F20635414C5A9AE801 (void);
// 0x000000DF System.Void Program.UI.MenuPanel::add_LoadFileFromLibrary(System.Action)
extern void MenuPanel_add_LoadFileFromLibrary_m238B1208B65BF59D270AC282904BD000EDDD0254 (void);
// 0x000000E0 System.Void Program.UI.MenuPanel::remove_LoadFileFromLibrary(System.Action)
extern void MenuPanel_remove_LoadFileFromLibrary_m36D1A8A034C4671C4F9C42C9BDB432D9BAB8F904 (void);
// 0x000000E1 System.Void Program.UI.MenuPanel::add_EditPlayer(System.Action)
extern void MenuPanel_add_EditPlayer_m48E07A1778D3D487B49DF3F9FC7233BBBAC42DE5 (void);
// 0x000000E2 System.Void Program.UI.MenuPanel::remove_EditPlayer(System.Action)
extern void MenuPanel_remove_EditPlayer_mDD89630473B86F3CA70C5A1BE9802613FEC5CC97 (void);
// 0x000000E3 System.Void Program.UI.MenuPanel::add_EditItem(System.Action)
extern void MenuPanel_add_EditItem_mC2063CA506A5FCD945E4C57A096FD7D6DDD76CBE (void);
// 0x000000E4 System.Void Program.UI.MenuPanel::remove_EditItem(System.Action)
extern void MenuPanel_remove_EditItem_mFF58CE5FB87F808F327D2E36A9ED3C750ABEBDE4 (void);
// 0x000000E5 System.Void Program.UI.MenuPanel::add_EditBlock(System.Action)
extern void MenuPanel_add_EditBlock_mE9AD06BA55F71022E6F8D72C5E400741A318C6F9 (void);
// 0x000000E6 System.Void Program.UI.MenuPanel::remove_EditBlock(System.Action)
extern void MenuPanel_remove_EditBlock_m19CCBB8F2CD9A2BC04160F9345F28D89744EF5B0 (void);
// 0x000000E7 Program.UI.MenuPanel Program.UI.MenuPanel::get_Singleton()
extern void MenuPanel_get_Singleton_mDE37F6231B0F7936FFEEB0E801085D89EC6A8428 (void);
// 0x000000E8 System.Void Program.UI.MenuPanel::set_Singleton(Program.UI.MenuPanel)
extern void MenuPanel_set_Singleton_m635E9F118FFF8B01383828CFA59D048613B2F11F (void);
// 0x000000E9 System.Void Program.UI.MenuPanel::Awake()
extern void MenuPanel_Awake_mDCC42FA622966755AD67EF4E68312797EFF0FEB1 (void);
// 0x000000EA System.Void Program.UI.MenuPanel::Start()
extern void MenuPanel_Start_m085C9377780683BA9CDD0F59C39712644C24D9C7 (void);
// 0x000000EB System.Void Program.UI.MenuPanel::OnDestroy()
extern void MenuPanel_OnDestroy_mC3B858BD379900A01A3B1B6CFA95C32A2647A928 (void);
// 0x000000EC System.Void Program.UI.MenuPanel::.ctor()
extern void MenuPanel__ctor_m43DACB52B7FF10C1464FA7A383D9AE7028656722 (void);
// 0x000000ED System.Void Program.UI.MenuPanel::<Start>b__33_0()
extern void MenuPanel_U3CStartU3Eb__33_0_mCC6958034FDEA45ABB33ECF47C1CCAC01601BD9B (void);
// 0x000000EE System.Void Program.UI.MenuPanel::<Start>b__33_1()
extern void MenuPanel_U3CStartU3Eb__33_1_m40355F530568FDC7E1DEFBC00946261C315F4AFC (void);
// 0x000000EF System.Void Program.UI.MenuPanel::<Start>b__33_2()
extern void MenuPanel_U3CStartU3Eb__33_2_m5DA812FB86F0BDA0354F4D6875B1961C93966BA4 (void);
// 0x000000F0 System.Void Program.UI.MenuPanel::<Start>b__33_3()
extern void MenuPanel_U3CStartU3Eb__33_3_m2FC0FFC86010CBA9D4A3DF4DE23901A886D82333 (void);
// 0x000000F1 System.Void Program.UI.MenuPanel::<Start>b__33_4()
extern void MenuPanel_U3CStartU3Eb__33_4_m8F30655D551DD9CABA8D84BADB4ABBFAE5FDA0AA (void);
// 0x000000F2 System.Void Program.UI.MenuPanel::<Start>b__33_5()
extern void MenuPanel_U3CStartU3Eb__33_5_m82FA5D1ED9AD9E5BF0A3BFA85CC3F1E768F7AE79 (void);
// 0x000000F3 System.Void Program.UI.MenuPanel::<Start>b__33_6()
extern void MenuPanel_U3CStartU3Eb__33_6_m4B0C097BF677DDA5DC4441A6DAFFBBDE08D81AB6 (void);
// 0x000000F4 System.Void Program.UI.LoadMenu::Start()
extern void LoadMenu_Start_mEE4864226FBDA14AF6CE7E7DD0A6E66B66DC2D9C (void);
// 0x000000F5 System.Void Program.UI.LoadMenu::.ctor()
extern void LoadMenu__ctor_m21FFAC8987D40CC711A58C0A46EA38DE608328D8 (void);
// 0x000000F6 System.Void Program.UI.ColorItem::Init(Program.UI.ColorPalitra)
extern void ColorItem_Init_mBB4D287B35E82B43C3F43FA54592B3834F36FD53 (void);
// 0x000000F7 UnityEngine.Color Program.UI.ColorItem::GetColor()
extern void ColorItem_GetColor_m3DB30F988B5F7133D1876B30B7E35D3BF24960B5 (void);
// 0x000000F8 System.Void Program.UI.ColorItem::SetColor(UnityEngine.Color)
extern void ColorItem_SetColor_m99A9C921EA813ECD2313B6C6DB169D0E739215A1 (void);
// 0x000000F9 System.Void Program.UI.ColorItem::.ctor()
extern void ColorItem__ctor_mA831CD661DCE470D5184E15682EA18F0D8A04881 (void);
// 0x000000FA System.Void Program.UI.ColorPalitra::add_TakeItem(System.Action)
extern void ColorPalitra_add_TakeItem_m2AB67F861A515A06640B67A3F8B6230B8742787C (void);
// 0x000000FB System.Void Program.UI.ColorPalitra::remove_TakeItem(System.Action)
extern void ColorPalitra_remove_TakeItem_m0588DA2EB6E2C9BD8F33A8D394EE9BA9110D45B2 (void);
// 0x000000FC System.Void Program.UI.ColorPalitra::SetPalitra(ProjectPalitra)
extern void ColorPalitra_SetPalitra_m479A0A763BA4EBDEE1035132D6879D2D9BE81FD4 (void);
// 0x000000FD ProjectPalitra Program.UI.ColorPalitra::GetPalitra()
extern void ColorPalitra_GetPalitra_m09CE322D73B00F32490D18CC096E5E91FD9AA05E (void);
// 0x000000FE System.Void Program.UI.ColorPalitra::EditColor(UnityEngine.Color)
extern void ColorPalitra_EditColor_m237C3E1075754F4D09C429FA61B218D8F6C9B445 (void);
// 0x000000FF System.Void Program.UI.ColorPalitra::SetItems(Program.UI.ColorItem[])
extern void ColorPalitra_SetItems_m7A2F26C61E8C1C82F194A75AAD1AE3B464548128 (void);
// 0x00000100 Program.UI.ColorItem Program.UI.ColorPalitra::GetCurrentItem()
extern void ColorPalitra_GetCurrentItem_m2539A5979541103AD6631F8CCDAD6E5CFF81C344 (void);
// 0x00000101 System.Void Program.UI.ColorPalitra::Take(Program.UI.ColorItem)
extern void ColorPalitra_Take_m915CDF50CB7CE7BCF4583A354CDE2B76F5A041B0 (void);
// 0x00000102 System.Void Program.UI.ColorPalitra::.ctor()
extern void ColorPalitra__ctor_mB30CB493D941F2B4F51B18DD8C597F6832D7DC58 (void);
// 0x00000103 System.Void Program.UI.ColorPickerPanel::Start()
extern void ColorPickerPanel_Start_m947BA8A24F59240BCDFED78F49C899153B918573 (void);
// 0x00000104 System.Void Program.UI.ColorPickerPanel::OpenCallBack()
extern void ColorPickerPanel_OpenCallBack_mE617E96F5A3E7667E83435694A20AFAA71CE312F (void);
// 0x00000105 System.Void Program.UI.ColorPickerPanel::CloseCallBack()
extern void ColorPickerPanel_CloseCallBack_mBD98CE2A68054665F23ADF7EDCFD16FF7754A334 (void);
// 0x00000106 System.Void Program.UI.ColorPickerPanel::InitializedParams()
extern void ColorPickerPanel_InitializedParams_m36FD320831BF670DA309ED796C68B04B6DFB4160 (void);
// 0x00000107 System.Void Program.UI.ColorPickerPanel::Update()
extern void ColorPickerPanel_Update_m98E8DE27AD1A9B975905D51B5B7A562252394256 (void);
// 0x00000108 System.Boolean Program.UI.ColorPickerPanel::CheckMouseInPicker()
extern void ColorPickerPanel_CheckMouseInPicker_m2C88087B2377AE81B16E81ABBAD691440EBD66E0 (void);
// 0x00000109 System.Void Program.UI.ColorPickerPanel::SliderRGB()
extern void ColorPickerPanel_SliderRGB_m6B19BE722209592445E268230D894D942CBFC1D5 (void);
// 0x0000010A System.Void Program.UI.ColorPickerPanel::SetGradient()
extern void ColorPickerPanel_SetGradient_m1C7BBDD3969717ACCA93CC7E835E220A586A3A05 (void);
// 0x0000010B UnityEngine.Color Program.UI.ColorPickerPanel::GetColorJoistic()
extern void ColorPickerPanel_GetColorJoistic_mAB65A48808A7F4BA5B22E41863DB5C0B03EF3EF6 (void);
// 0x0000010C System.Void Program.UI.ColorPickerPanel::SetColorPalitra(UnityEngine.Color)
extern void ColorPickerPanel_SetColorPalitra_mAF4A3CDC5489146B68D88C805D79E1F252790AD6 (void);
// 0x0000010D System.Void Program.UI.ColorPickerPanel::SetColorFromPalitra()
extern void ColorPickerPanel_SetColorFromPalitra_m40F14B3CC725E9676F325365C73BE95A4C795F17 (void);
// 0x0000010E System.Void Program.UI.ColorPickerPanel::.ctor()
extern void ColorPickerPanel__ctor_mBFEDA0D3E8275BF46204C366A445C94158FE3023 (void);
// 0x0000010F System.Void Program.UI.ColorPickerPanel::<Start>b__8_0()
extern void ColorPickerPanel_U3CStartU3Eb__8_0_m75A75D7EA72B5C04D91F9A0E891668EF2729D771 (void);
// 0x00000110 System.Void Program.UI.ColorPickerPanel::<Start>b__8_1(System.Single)
extern void ColorPickerPanel_U3CStartU3Eb__8_1_m9F169F862462214577F1A6005F98C8EA23F315AB (void);
// 0x00000111 System.Void Program.UI.HistoryButton::add_HistoryNext(System.Action)
extern void HistoryButton_add_HistoryNext_mFA7B5A0E94A850C99B6F186447423F70F5E53D8F (void);
// 0x00000112 System.Void Program.UI.HistoryButton::remove_HistoryNext(System.Action)
extern void HistoryButton_remove_HistoryNext_m4952CFA1B71AB4C14D5E798971CB5F6E5A427941 (void);
// 0x00000113 System.Void Program.UI.HistoryButton::add_HistoryBack(System.Action)
extern void HistoryButton_add_HistoryBack_mFE5A88B7E7BF434ED70B6D60A5FB87CE48191E82 (void);
// 0x00000114 System.Void Program.UI.HistoryButton::remove_HistoryBack(System.Action)
extern void HistoryButton_remove_HistoryBack_mD2863D6DE0ACBCF75E6742C8ADF504E66A41E2F9 (void);
// 0x00000115 System.Void Program.UI.HistoryButton::BackHistory()
extern void HistoryButton_BackHistory_m87C5E919DA71B99989BBF0FB67EFFB6DA65C9C8B (void);
// 0x00000116 System.Void Program.UI.HistoryButton::NextHistory()
extern void HistoryButton_NextHistory_m809836890A22F382AD96F564748FBB68C2A3934F (void);
// 0x00000117 System.Void Program.UI.HistoryButton::.ctor()
extern void HistoryButton__ctor_m4F3E32839BD1DAE4410952C5BBC828036579D706 (void);
// 0x00000118 Program.UI.Instrument Program.UI.Instruments::get_InstrumentTake()
extern void Instruments_get_InstrumentTake_mE8360FD723E9F6466EF0BF10D48301EF3B73B59F (void);
// 0x00000119 System.Void Program.UI.Instruments::set_InstrumentTake(Program.UI.Instrument)
extern void Instruments_set_InstrumentTake_mCAD9D564496C1F839D68AA299DE96B668752E99E (void);
// 0x0000011A System.Void Program.UI.Instruments::add_TakeInstrument(System.Action)
extern void Instruments_add_TakeInstrument_m647488514FAF8BDC89B6361ACE7D5CDA7975677A (void);
// 0x0000011B System.Void Program.UI.Instruments::remove_TakeInstrument(System.Action)
extern void Instruments_remove_TakeInstrument_mB0D58527D59E36C769009665538F8831017970BF (void);
// 0x0000011C System.Void Program.UI.Instruments::TakePaint()
extern void Instruments_TakePaint_mF5A5660834B59D12BEE85DBECE098C2B9A518867 (void);
// 0x0000011D System.Void Program.UI.Instruments::TakeColor()
extern void Instruments_TakeColor_m1286AF6ABD47C645C9CA73EE5E37806B59DAF8AA (void);
// 0x0000011E System.Void Program.UI.Instruments::TakeFillColor()
extern void Instruments_TakeFillColor_mB3AE6CBE53415CD455CF250626B7B7BA72739B40 (void);
// 0x0000011F System.Void Program.UI.Instruments::TakeErase()
extern void Instruments_TakeErase_m556D71FCB88F4B8D858379D644A22DABFD6E47BE (void);
// 0x00000120 System.Void Program.UI.Instruments::.ctor()
extern void Instruments__ctor_m91668F8A830E380E3BEC8501B3B8027EBFB8E69B (void);
// 0x00000121 System.Void Program.UI.SetPalitra::.ctor(System.Object,System.IntPtr)
extern void SetPalitra__ctor_m99982AE5CAEA5EA3094C310F557F31A8832F89E9 (void);
// 0x00000122 System.Void Program.UI.SetPalitra::Invoke(Palitra)
extern void SetPalitra_Invoke_mA8861B399A428449B552B16EBB075393AB6120FB (void);
// 0x00000123 System.IAsyncResult Program.UI.SetPalitra::BeginInvoke(Palitra,System.AsyncCallback,System.Object)
extern void SetPalitra_BeginInvoke_mC8984D581B3DF9EB55556E9A5E6018FFEF16C0DF (void);
// 0x00000124 System.Void Program.UI.SetPalitra::EndInvoke(System.IAsyncResult)
extern void SetPalitra_EndInvoke_mF983AE2CD6CE707C2410BDC9861448DC728F6A6E (void);
// 0x00000125 System.Void Program.UI.MenuPaltira::add_TakePalitra(Program.UI.SetPalitra)
extern void MenuPaltira_add_TakePalitra_mE3BFDB24358C0AE3C122F29D911C18B86CA9F778 (void);
// 0x00000126 System.Void Program.UI.MenuPaltira::remove_TakePalitra(Program.UI.SetPalitra)
extern void MenuPaltira_remove_TakePalitra_mC79E72F7911BABB60AC3DA561527D15B9B038AE8 (void);
// 0x00000127 Program.UI.MenuPaltira Program.UI.MenuPaltira::get_Singleton()
extern void MenuPaltira_get_Singleton_mA27F91D69CC9387223DBCDFF52512A59CFB69CAE (void);
// 0x00000128 System.Void Program.UI.MenuPaltira::set_Singleton(Program.UI.MenuPaltira)
extern void MenuPaltira_set_Singleton_mAF9BE98C99C52395B40215E2EE515500077D654D (void);
// 0x00000129 System.Void Program.UI.MenuPaltira::Awake()
extern void MenuPaltira_Awake_mB336714D316F92431EDFF9C7299BEB3B7D7C17A1 (void);
// 0x0000012A System.Void Program.UI.MenuPaltira::Start()
extern void MenuPaltira_Start_m8A852DFA310C838079A087DF7719EA311B5E9926 (void);
// 0x0000012B System.Void Program.UI.MenuPaltira::OpenCallBack()
extern void MenuPaltira_OpenCallBack_mE5068EDB34C6721921AF2BD83022CF2DACFF96EE (void);
// 0x0000012C System.Void Program.UI.MenuPaltira::CloseCallBack()
extern void MenuPaltira_CloseCallBack_mB20D9C02F7022414208A8F20AE7B92FB195EF37A (void);
// 0x0000012D System.Void Program.UI.MenuPaltira::NewTakePalitra(Palitra)
extern void MenuPaltira_NewTakePalitra_m8504090ACB63ECC3B5A1DFD458F6995373A8A5E1 (void);
// 0x0000012E System.Void Program.UI.MenuPaltira::UpdateList(Palitra[])
extern void MenuPaltira_UpdateList_m88B0B093B4583A390C31D266D1F26C2606D3B34D (void);
// 0x0000012F System.Void Program.UI.MenuPaltira::SavePalitra(Palitra)
extern void MenuPaltira_SavePalitra_m2580523A5F49906C941E297E940B9AD7689F056F (void);
// 0x00000130 System.Void Program.UI.MenuPaltira::UpdateList()
extern void MenuPaltira_UpdateList_m0199142DFA59CF1F5F98AAA22D7868803F52DFC5 (void);
// 0x00000131 System.Void Program.UI.MenuPaltira::DeletPalitra(Palitra)
extern void MenuPaltira_DeletPalitra_mA597DC0019673F54696BE548B288C68ED4880914 (void);
// 0x00000132 Palitra[] Program.UI.MenuPaltira::GetPaliters()
extern void MenuPaltira_GetPaliters_m94DA7CDED26AEA7006C69C5D247494CAF0E67F7B (void);
// 0x00000133 System.Void Program.UI.MenuPaltira::CheckDirectory()
extern void MenuPaltira_CheckDirectory_mF88202C3E8BA135B1BC83DCBA32E4C094684FAC4 (void);
// 0x00000134 System.Void Program.UI.MenuPaltira::OnDestroy()
extern void MenuPaltira_OnDestroy_mD1E23567C45390C931E8FA2F6933EABD955428A4 (void);
// 0x00000135 System.Void Program.UI.MenuPaltira::.ctor()
extern void MenuPaltira__ctor_mE6516DD85D2AEE9C07A9E715ABA560A9027F398A (void);
// 0x00000136 System.Void Program.UI.MenuPaltira::<Start>b__18_0()
extern void MenuPaltira_U3CStartU3Eb__18_0_mB0B57DAE05470FABAE4B70A730AAA53D52EF28D4 (void);
// 0x00000137 System.Void Program.UI.MenuPaltira::<Start>b__18_1()
extern void MenuPaltira_U3CStartU3Eb__18_1_mFE44FADF95EBB75F7ECF776DA2C4C04912DAEA42 (void);
// 0x00000138 System.Void Program.UI.MenuPaltira::<Start>b__18_2()
extern void MenuPaltira_U3CStartU3Eb__18_2_m5B895236BB6DCB620EE58B5529018C9099E361CA (void);
// 0x00000139 System.Void Program.UI.PalitraItem::Init(Palitra,Program.UI.MenuPaltira,System.Int32)
extern void PalitraItem_Init_m76A3DE7F1F0A34741FCACC2633C18BC8FDDC80FB (void);
// 0x0000013A System.Void Program.UI.PalitraItem::.ctor()
extern void PalitraItem__ctor_m30744D6CD05D82870174FB15594ECDB5D4293D70 (void);
// 0x0000013B Program.UI.Instruments Program.UI.PalitraPanel::get_Instrument()
extern void PalitraPanel_get_Instrument_m7F79D1932B2C71BB7B236D976366BFDC4965020C (void);
// 0x0000013C System.Void Program.UI.PalitraPanel::set_Instrument(Program.UI.Instruments)
extern void PalitraPanel_set_Instrument_mE1F840BF319190F398A837FEA3A67DC41F936436 (void);
// 0x0000013D Program.UI.ColorPalitra Program.UI.PalitraPanel::get_ColorList()
extern void PalitraPanel_get_ColorList_m097292F6376815B20209CCD233B1F23800CD0C02 (void);
// 0x0000013E System.Void Program.UI.PalitraPanel::set_ColorList(Program.UI.ColorPalitra)
extern void PalitraPanel_set_ColorList_m0C9A3AA69E4B3C52393A5D862DCAD9165E17BF43 (void);
// 0x0000013F Program.UI.HistoryButton Program.UI.PalitraPanel::get_History()
extern void PalitraPanel_get_History_mD749E9F50722D5B8D5110C265F2CDAEAFDD881AB (void);
// 0x00000140 System.Void Program.UI.PalitraPanel::set_History(Program.UI.HistoryButton)
extern void PalitraPanel_set_History_m5F1EDBD7B6D224B8F7CFB16668B6A03FA8588EB2 (void);
// 0x00000141 Program.UI.PalitraPanel Program.UI.PalitraPanel::get_Singleton()
extern void PalitraPanel_get_Singleton_mCE3314F8DAA7B9C9C3FE5193C610299AA1549574 (void);
// 0x00000142 System.Void Program.UI.PalitraPanel::set_Singleton(Program.UI.PalitraPanel)
extern void PalitraPanel_set_Singleton_m5AAC644BDE919FCCA0846FC5CABE4328BEEF7630 (void);
// 0x00000143 System.Void Program.UI.PalitraPanel::Awake()
extern void PalitraPanel_Awake_mF47205E2B1A024705F4CD872315E44EAE839960D (void);
// 0x00000144 System.Void Program.UI.PalitraPanel::Start()
extern void PalitraPanel_Start_m994E21F280C97AC1CFD024F81C084998FC6412D2 (void);
// 0x00000145 System.Void Program.UI.PalitraPanel::EditInstrument()
extern void PalitraPanel_EditInstrument_m0626CAA879F036AC538785CB2A733605BF7534C1 (void);
// 0x00000146 System.Void Program.UI.PalitraPanel::EditTakeColor()
extern void PalitraPanel_EditTakeColor_m8A7E6F119A167F9DD119D1EB741C38ECAD8B6823 (void);
// 0x00000147 System.Void Program.UI.PalitraPanel::SetNewPalitra(Palitra)
extern void PalitraPanel_SetNewPalitra_m2A56F48F9C0E7E20588EA56983DE47A0A88B9539 (void);
// 0x00000148 System.Void Program.UI.PalitraPanel::OnDestroy()
extern void PalitraPanel_OnDestroy_m9307798DD41ECF1D45FA3FC3BA43DAD553C6DF91 (void);
// 0x00000149 System.Void Program.UI.PalitraPanel::.ctor()
extern void PalitraPanel__ctor_mD4FFA9B8C009BE68CA33A9E5A2906D74A1562CE7 (void);
// 0x0000014A System.Void Program.UI.PalitraPanel::<Start>b__39_6()
extern void PalitraPanel_U3CStartU3Eb__39_6_m5FD8DF5AED30CF7751EB8C00D5D6A4905887E247 (void);
// 0x0000014B System.Void Program.UI.PalitraPanel::<Start>b__39_7()
extern void PalitraPanel_U3CStartU3Eb__39_7_m14B1E4FB7298CCD39B2BE9108F1D9282F7D93FB0 (void);
// 0x0000014C System.Void Program.UI.PanelProgram::Start()
extern void PanelProgram_Start_mE3425AA80A4013100071C7D6817128563FD2BFB8 (void);
// 0x0000014D System.Void Program.UI.PanelProgram::OpenButton()
extern void PanelProgram_OpenButton_mB0C6C76CB8FCCF17AB0874B74F7AF9BBA01384BA (void);
// 0x0000014E System.Void Program.UI.PanelProgram::CloseButton()
extern void PanelProgram_CloseButton_m1391DCEE96C44F1F1CA84018C12141DDBE70CC7B (void);
// 0x0000014F System.Void Program.UI.PanelProgram::SetParams()
extern void PanelProgram_SetParams_m69315C8D33FF2FC662D96B98302AEE7A4AC386A6 (void);
// 0x00000150 System.Void Program.UI.PanelProgram::.ctor()
extern void PanelProgram__ctor_m7358BC33BEBF41A699490D625A87055D1CF3C579 (void);
// 0x00000151 System.Void Program.UI.PanelProgram::<Start>b__6_0()
extern void PanelProgram_U3CStartU3Eb__6_0_mB24D08F5DC87D7C9AB155E06FB3F72C4F69A8706 (void);
// 0x00000152 System.Void Program.UI.PanelProgram::<Start>b__6_1()
extern void PanelProgram_U3CStartU3Eb__6_1_m38395C281139DE98FAE39AC937126D857AFD6E52 (void);
// 0x00000153 System.Void Program.UI.ProgramUI::Set()
extern void ProgramUI_Set_m14462C9548AB7844E1B6B2061FE7CC785E3C1F63 (void);
// 0x00000154 System.Void Program.UI.ProgramUI::OpenCenterPanel(Program.UI.CenterPanel)
extern void ProgramUI_OpenCenterPanel_m3B17EA932B723968AF8D059929481E06D4E75CDE (void);
// 0x00000155 System.Void Program.UI.ProgramUI::.ctor()
extern void ProgramUI__ctor_mA3FC7E2F15D77646EB4FA4203DFB30F7B97F4E5C (void);
// 0x00000156 System.Void Program.UI.UV::Start()
extern void UV_Start_mEB248CE8486D833BE8E5AB435A075941DFB9C60E (void);
// 0x00000157 System.Void Program.UI.UV::Open()
extern void UV_Open_m98271C3143D56393B4899DF9C492F9D6FB84B0BC (void);
// 0x00000158 System.Void Program.UI.UV::ClickClose()
extern void UV_ClickClose_m086B25CBD6AB6FC1BC0EE85F9C280250D910B8D4 (void);
// 0x00000159 System.Void Program.UI.UV::Close()
extern void UV_Close_m6098F7CDD4584AF1536E15D49453F9DDDBB8D6CA (void);
// 0x0000015A System.Void Program.UI.UV::OpenCallBack()
extern void UV_OpenCallBack_m1C7DD6259DD0918459955CDB204F76564F352A4E (void);
// 0x0000015B System.Void Program.UI.UV::CloseCallBack()
extern void UV_CloseCallBack_m4E02286EEA8FE5D270EB0A686B6CE266D9727800 (void);
// 0x0000015C UnityEngine.Vector2 Program.UI.UV::GetPositionMouse()
// 0x0000015D System.Void Program.UI.UV::SetTexture(UnityEngine.Texture2D)
// 0x0000015E System.Void Program.UI.UV::.ctor()
extern void UV__ctor_mA1AC7EF3AE52D4D5FC4FD42F3B785B757DFF0413 (void);
// 0x0000015F System.Void Program.UI.UV::<Start>b__3_0()
extern void UV_U3CStartU3Eb__3_0_mD1AE53B2DA7CAAD27C216DCD91C3BCB0B121B85D (void);
// 0x00000160 System.Void Program.UI.UVPanel::add_EditUV(System.Action)
extern void UVPanel_add_EditUV_m87ACD1040A8EAFCE0561D164276C7657896F145D (void);
// 0x00000161 System.Void Program.UI.UVPanel::remove_EditUV(System.Action)
extern void UVPanel_remove_EditUV_m1F8CD365E32F47094F9F1E00E8BB22C9E2912A2E (void);
// 0x00000162 System.Boolean Program.UI.UVPanel::get_IsActive()
extern void UVPanel_get_IsActive_mF298A653781B5B2009EB060D71EE5A41EB561C75 (void);
// 0x00000163 System.Void Program.UI.UVPanel::set_IsActive(System.Boolean)
extern void UVPanel_set_IsActive_m257C20DA61922AA7F7B5CAFD94DEC2F178478008 (void);
// 0x00000164 System.Void Program.UI.UVPanel::Open()
extern void UVPanel_Open_mA452877254FF476D6AA0734706B8E329ED6A8665 (void);
// 0x00000165 System.Void Program.UI.UVPanel::Close()
extern void UVPanel_Close_m81001491C18C6AC8738D583DDB50B266199B275F (void);
// 0x00000166 UnityEngine.Vector2 Program.UI.UVPanel::GetMousePostion()
extern void UVPanel_GetMousePostion_m5498B16E3463C92479BB3C3175ECC42F0AD3E12B (void);
// 0x00000167 System.Void Program.UI.UVPanel::.ctor()
extern void UVPanel__ctor_m602C4EBC1E31AF2896B226E96682BD9A851699D2 (void);
// 0x00000168 System.Void Program.Render.BackEditor::Start()
extern void BackEditor_Start_m4B1BB26C214F6DD841B619BD4EE8F0000721E8E8 (void);
// 0x00000169 System.Void Program.Render.BackEditor::Back()
extern void BackEditor_Back_mF6994D570364043AA1D0D2F0493C7280325B5711 (void);
// 0x0000016A System.Void Program.Render.BackEditor::.ctor()
extern void BackEditor__ctor_m7FD70A655DC2FEBE8200F475B811C8D83FA98F00 (void);
// 0x0000016B System.Void Program.Render.BackEditor::<Start>b__1_0()
extern void BackEditor_U3CStartU3Eb__1_0_m6C7249A519A003BADB6F90A05660C1D4DAEA74DB (void);
// 0x0000016C UnityEngine.Vector3 Program.Render.ControllerPlayer::get_Direct()
extern void ControllerPlayer_get_Direct_m058EE79D5CDB56B7B316B20456071E6433346142 (void);
// 0x0000016D System.Void Program.Render.ControllerPlayer::set_Direct(UnityEngine.Vector3)
extern void ControllerPlayer_set_Direct_m07006D227548EB9A006A564398F8DAFEE2F0BBBF (void);
// 0x0000016E UnityEngine.Transform Program.Render.ControllerPlayer::get_DirectCamera()
extern void ControllerPlayer_get_DirectCamera_m78941F29180F06DD81DF483D3824BC54AF3A8F36 (void);
// 0x0000016F System.Void Program.Render.ControllerPlayer::Update()
extern void ControllerPlayer_Update_mEB4DB409B5927F6761EE860499ED81CF816C78A6 (void);
// 0x00000170 System.Void Program.Render.ControllerPlayer::LateUpdate()
extern void ControllerPlayer_LateUpdate_mEB4D18C9284C5A96C695BCB74874015D1A5B45D3 (void);
// 0x00000171 System.Void Program.Render.ControllerPlayer::.ctor()
extern void ControllerPlayer__ctor_m8FD95AC3D4817B057FB7B69A3B43B0FCB6D933AC (void);
// 0x00000172 System.Void Program.Render.LoadRender::Start()
extern void LoadRender_Start_mDF8D9FF20FED2DF2197ADFBE0E6FE2D6243E0CD3 (void);
// 0x00000173 System.Void Program.Render.LoadRender::Load()
extern void LoadRender_Load_m37F4B3EA7A1C6F3FD7DC627AE9AC3D42C402948F (void);
// 0x00000174 System.Void Program.Render.LoadRender::.ctor()
extern void LoadRender__ctor_mAEBFB18D3E962E7A54A4BD4751A5F76AD49A2C95 (void);
// 0x00000175 System.Void Program.Render.MovePlayer::Update()
extern void MovePlayer_Update_m2DA0EE5DF4AAF62A75CBE8D9BE75666C8C0EF261 (void);
// 0x00000176 System.Void Program.Render.MovePlayer::.ctor()
extern void MovePlayer__ctor_m72E137B7C60E9C5F6ACD3C403BEF1CC642D1411D (void);
// 0x00000177 System.Void Program.Render.SetTexture::Start()
extern void SetTexture_Start_m0CF376F4B713A1D2760EBA9AF85890DB9B37876E (void);
// 0x00000178 System.Void Program.Render.SetTexture::.ctor()
extern void SetTexture__ctor_m4754436BCD80A778C2B3A286EFB96863B396E09C (void);
// 0x00000179 System.Void Program.Option.FileOption::.ctor()
extern void FileOption__ctor_m7D31F9ADB4D02A9087B2A186DA72CE311ACB9E65 (void);
// 0x0000017A System.Void Program.Option.FilersOption::SaveFile(Program.Option.FileOption)
extern void FilersOption_SaveFile_m030F38EB0E8035A4A4CF3EC845301BAD7A27A348 (void);
// 0x0000017B Program.Option.FileOption Program.Option.FilersOption::LoadFile()
extern void FilersOption_LoadFile_m5886507A3D064BB20F9308F480D849B104D373CA (void);
// 0x0000017C System.Void Program.Option.OptionController::Start()
extern void OptionController_Start_m042CAF612D2FA8A36CFDB26F1C42A112AF9E5741 (void);
// 0x0000017D System.Void Program.Option.OptionController::OnDestroy()
extern void OptionController_OnDestroy_mEA4CEBE1936E7A53DB7C1BC7F02F755BB89506F7 (void);
// 0x0000017E System.Void Program.Option.OptionController::.ctor()
extern void OptionController__ctor_m077A9D7B2544502CE4D17CAA2E7132CB1E0AC45F (void);
// 0x0000017F System.Object Program.Option.OptionObject::GetOption()
// 0x00000180 System.Void Program.Option.OptionObject::SetOption(System.Object)
// 0x00000181 System.Void Program.Option.OptionObject::.ctor()
extern void OptionObject__ctor_mCEBD64F06C7252A5D729768B741FB0CA0DBDFD5A (void);
// 0x00000182 Program.Menu.FileNews Program.Menu.FileNews::LoadFile()
extern void FileNews_LoadFile_m50097B95C2E832E5A8C4163BAFE65ACD2D209ECA (void);
// 0x00000183 System.Void Program.Menu.FileNews::SaveFile(Program.Menu.FileNews)
extern void FileNews_SaveFile_mE8E4BE1DB3393602F760444FF40CE1D49D3F7C8D (void);
// 0x00000184 System.Void Program.Menu.FileNews::.ctor()
extern void FileNews__ctor_mB331F9DD9BB3FAB7E4AA62586D9E00ABD51E699A (void);
// 0x00000185 System.Void Program.Menu.FileNews::.cctor()
extern void FileNews__cctor_m1FE6FC05ED6F7C2AE3607A744D720AD9A4FA4CA5 (void);
// 0x00000186 System.Void Program.Menu.NewsServer::Start()
extern void NewsServer_Start_m5A55EA8BB183B715BECD9CFA9132988A9C12C61A (void);
// 0x00000187 System.Void Program.Menu.NewsServer::IsCurrentUpdateAsync(System.UInt32)
extern void NewsServer_IsCurrentUpdateAsync_m8A66DC1D9CB89A49828C1EDABDFD2502F44B0500 (void);
// 0x00000188 System.Void Program.Menu.NewsServer::LoadNewVersionNewsAsync(FilerServer.News[])
extern void NewsServer_LoadNewVersionNewsAsync_m4B3BE1FE9596130587701DE59AFEF5439B8C061E (void);
// 0x00000189 System.Void Program.Menu.NewsServer::LoadNewsAsync(FilerServer.News[])
extern void NewsServer_LoadNewsAsync_mC4724037BC08197BFE98970F1A3C54910679DDDA (void);
// 0x0000018A System.Void Program.Menu.NewsServer::IsLoadAsync(System.UInt32)
extern void NewsServer_IsLoadAsync_m5A2626648A46C68F3A26CA1A51CC0C6DA1A65ED9 (void);
// 0x0000018B System.Void Program.Menu.NewsServer::EndLoad()
extern void NewsServer_EndLoad_mFB204A6A123DEAD307F88307A4B69FE4E4B915F9 (void);
// 0x0000018C System.Void Program.Menu.NewsServer::EndLoadNoServer()
extern void NewsServer_EndLoadNoServer_mB903EA15E16BD48559BAE06ACAB2767150E49FC8 (void);
// 0x0000018D System.Void Program.Menu.NewsServer::SaveCurrentFile()
extern void NewsServer_SaveCurrentFile_mE0D4921FC0E1622C8E3143C5FF9F93D7F36A6F81 (void);
// 0x0000018E System.Void Program.Menu.NewsServer::ClearVariable()
extern void NewsServer_ClearVariable_mB9B82FD11998FA890521CEFCB90A80483CBE1037 (void);
// 0x0000018F Program.Menu.BaseMenu Program.Menu.BaseMenu::get_Singleton()
extern void BaseMenu_get_Singleton_m069EC972BD638A8FB525054E1611FB620CA75AA5 (void);
// 0x00000190 System.Void Program.Menu.BaseMenu::set_Singleton(Program.Menu.BaseMenu)
extern void BaseMenu_set_Singleton_mB1A2F5FFCDCC484C66BD011396C1192066AFBDAD (void);
// 0x00000191 System.Void Program.Menu.BaseMenu::Awake()
extern void BaseMenu_Awake_m4EFFD9A711BA2D2A9B6AA7D973F7A7087F858315 (void);
// 0x00000192 System.Void Program.Menu.BaseMenu::LoadEditor()
extern void BaseMenu_LoadEditor_mB6D54A1389D8C9489397FAA0E85E65BB159AA571 (void);
// 0x00000193 System.Void Program.Menu.BaseMenu::ExitProgram()
extern void BaseMenu_ExitProgram_m35F3098E90075D44F5141D44A15A2A730C27B7B5 (void);
// 0x00000194 System.Void Program.Menu.BaseMenu::OnDestroy()
extern void BaseMenu_OnDestroy_m0477A32B4622D6EDBDF5D2B4229D940B35135E50 (void);
// 0x00000195 System.Void Program.Menu.BaseMenu::.ctor()
extern void BaseMenu__ctor_m794261E5A19DA54209313F3859F349F236A7A571 (void);
// 0x00000196 System.Void Program.Menu.CreateNews::.ctor(FilerServer.News)
extern void CreateNews__ctor_m24BC96A27BA667759A6056F6CEC12E9B0EADDB1C (void);
// 0x00000197 System.Void Program.Menu.ItemNews::Init(System.String,System.String)
extern void ItemNews_Init_mE17235437DA055C46CBEDFD59FF33D6ABDB70792 (void);
// 0x00000198 System.Void Program.Menu.ItemNews::.ctor()
extern void ItemNews__ctor_m4A99E78C5EF4EB4EE4B083F3DA7F13AB86D68DA1 (void);
// 0x00000199 Program.Menu.ListNews Program.Menu.ListNews::get_Singleton()
extern void ListNews_get_Singleton_mA989ACDE2D531B9D55A5BE2D5432EA4AA6356CBA (void);
// 0x0000019A System.Void Program.Menu.ListNews::set_Singleton(Program.Menu.ListNews)
extern void ListNews_set_Singleton_m105CAC52256E242D85A30E5121072DF7CD25DEB8 (void);
// 0x0000019B System.Void Program.Menu.ListNews::Awake()
extern void ListNews_Awake_m2D5861C7DC975A44901B76920631A80BBCDB4647 (void);
// 0x0000019C System.Collections.IEnumerator Program.Menu.ListNews::LoadNewsWait()
extern void ListNews_LoadNewsWait_m8BA4ABE06B74001CABF6FC88B8E7C0E056C0730E (void);
// 0x0000019D System.Void Program.Menu.ListNews::LoadListNews()
extern void ListNews_LoadListNews_m9C0155DFB850A4099B7700B0B635580C28BE8999 (void);
// 0x0000019E System.Void Program.Menu.ListNews::AddNews(Program.Menu.CreateNews)
extern void ListNews_AddNews_m0F745731EE2238E38B387176B27D164263CDA081 (void);
// 0x0000019F System.Void Program.Menu.ListNews::Clear()
extern void ListNews_Clear_m27BC2C4AF4A0B176486F12FDC1B43A2FE48C6D26 (void);
// 0x000001A0 System.Void Program.Menu.ListNews::OnDestroy()
extern void ListNews_OnDestroy_mC08F006DA8E345D66378A4B9B3E7DB294D6515ED (void);
// 0x000001A1 System.Void Program.Menu.ListNews::.ctor()
extern void ListNews__ctor_mEEE52CBEDC311750AF56A189A79A83FA4B210163 (void);
// 0x000001A2 System.Void Program.Menu.UIMenu::Start()
extern void UIMenu_Start_m986C1B2404C90980318792C4F6468AA60EC14EFC (void);
// 0x000001A3 UnityEngine.Transform Program.Menu.UIMenu::GetContentNews()
extern void UIMenu_GetContentNews_m4A817491C5BD8CC3D0F085F7E290D4B343EA3071 (void);
// 0x000001A4 System.Void Program.Menu.UIMenu::.ctor()
extern void UIMenu__ctor_m087912FC379032E6163D24C0D55CE0111B0DA779 (void);
// 0x000001A5 System.Boolean Program.Editor.CameraMove::get_IsBlock()
extern void CameraMove_get_IsBlock_m7E4BFC366D0757153132373751C57DF909B456D5 (void);
// 0x000001A6 System.Void Program.Editor.CameraMove::OnEnable()
extern void CameraMove_OnEnable_mFC5BFBADF9014FE1D4EE9075F1D2066A6680AA18 (void);
// 0x000001A7 System.Void Program.Editor.CameraMove::Update()
extern void CameraMove_Update_m967D780F50D01795ABA53807AFB7802F72E96674 (void);
// 0x000001A8 System.Void Program.Editor.CameraMove::FixedUpdate()
extern void CameraMove_FixedUpdate_m70AA895BAEDAAABDC97C0797C84E0DE1E56CEE48 (void);
// 0x000001A9 System.Void Program.Editor.CameraMove::SetDefaultPosition()
extern void CameraMove_SetDefaultPosition_m0C1516CFF56F5B7F40952F7BF0CDD3DCBA4F38FB (void);
// 0x000001AA System.Void Program.Editor.CameraMove::OnDisable()
extern void CameraMove_OnDisable_m3D1AE48366A74637828C273277DECC35E8E6660D (void);
// 0x000001AB System.Void Program.Editor.CameraMove::.ctor()
extern void CameraMove__ctor_m0035D2534C340827B1B13EAC719DA2E9499EDAA8 (void);
// 0x000001AC Program.Editor.Model Program.Editor.EditModel::get_SkinModel()
extern void EditModel_get_SkinModel_m0DEB8D2D44C308E468FE2CF8ACB7BFBE94705523 (void);
// 0x000001AD Program.Editor.Model Program.Editor.EditModel::get_ItemModel()
extern void EditModel_get_ItemModel_mDC2663DB601364F6573921B40419B4E70F2FBDE5 (void);
// 0x000001AE Program.Editor.Model Program.Editor.EditModel::get_BlockModel()
extern void EditModel_get_BlockModel_mF902B8F54F4DDC6DDF8F3691AD95E836AB874D19 (void);
// 0x000001AF Program.Editor.Model Program.Editor.EditModel::get_CurrentModel()
extern void EditModel_get_CurrentModel_m93448B5F4798C5462C4E37A57C117B5472B56A4D (void);
// 0x000001B0 System.Void Program.Editor.EditModel::set_CurrentModel(Program.Editor.Model)
extern void EditModel_set_CurrentModel_m24FB61D1B1CD0F63E872BA9F32A2D262AD65E94C (void);
// 0x000001B1 Program.Editor.EditModel Program.Editor.EditModel::get_Singleton()
extern void EditModel_get_Singleton_mC5F7909A086EF7180B1518C5BF34A15278714A31 (void);
// 0x000001B2 System.Void Program.Editor.EditModel::set_Singleton(Program.Editor.EditModel)
extern void EditModel_set_Singleton_m976F3966ED7AC897C0321CB41B8E8B5BF7EB6346 (void);
// 0x000001B3 System.Void Program.Editor.EditModel::Awake()
extern void EditModel_Awake_m80587689A0A00916C9D8EC27DA9AA8139871CB9B (void);
// 0x000001B4 System.Void Program.Editor.EditModel::Start()
extern void EditModel_Start_m9011ACADDA9852AC20FF884E46B9AB630924A90C (void);
// 0x000001B5 System.Void Program.Editor.EditModel::TakeSkin()
extern void EditModel_TakeSkin_m186321DD245CCF2F642E4E234291F261CB239640 (void);
// 0x000001B6 System.Void Program.Editor.EditModel::TakeItem()
extern void EditModel_TakeItem_mE56916B6FA44A8B92BE3E430F63BB936C093BFB0 (void);
// 0x000001B7 System.Void Program.Editor.EditModel::TakeBlock()
extern void EditModel_TakeBlock_m12AE2EEBCB693107FD43D7416ADE96CF2ADDDCE8 (void);
// 0x000001B8 System.Void Program.Editor.EditModel::OnDestroy()
extern void EditModel_OnDestroy_mAC532FDA5BE78EDFA84BB04258C1541E06483304 (void);
// 0x000001B9 System.Void Program.Editor.EditModel::.ctor()
extern void EditModel__ctor_m1E46127B199266915550DA2BD4FD8FE4D1A68BFB (void);
// 0x000001BA UnityEngine.Vector2Int Program.Editor.GetPixels::GetPixel(UnityEngine.RaycastHit,System.Int32,System.Int32)
extern void GetPixels_GetPixel_mE693DC2122F29F960DC1FC0A9B6788E9A03C3F07 (void);
// 0x000001BB UnityEngine.Vector2Int[] Program.Editor.GetPixels::GetPixelsFill(UnityEngine.Vector2,System.Int32,System.Int32,UnityEngine.Mesh[])
extern void GetPixels_GetPixelsFill_m16163DF9BD5606A40871AB0FD280C3C981AADA70 (void);
// 0x000001BC UnityEngine.Vector2Int[] Program.Editor.GetPixels::GetPixelsFill(UnityEngine.RaycastHit,System.Int32,System.Int32)
extern void GetPixels_GetPixelsFill_m7BA9EAEE55F8DAF5ED8B784F41A6D3FF21DF56E2 (void);
// 0x000001BD UnityEngine.Vector2Int[] Program.Editor.GetPixels::GetPixelsFromTriangles(Program.Editor.GetPixels/Triangle[],UnityEngine.Vector2[],System.Int32,System.Int32)
extern void GetPixels_GetPixelsFromTriangles_mFE39AB1CEAD569288C829D0FA91BC42801043A3C (void);
// 0x000001BE Program.Editor.GetPixels/Triangle[] Program.Editor.GetPixels::GetConnectTriangle(System.Int32,System.Int32,System.Int32,UnityEngine.Mesh)
extern void GetPixels_GetConnectTriangle_m2E23BFDE8279853FC9B195EF8C68917FBEE536F3 (void);
// 0x000001BF System.Boolean Program.Editor.Model::get_IsWriteHistory()
extern void Model_get_IsWriteHistory_m5EDD0BAEB57D2027752B3D5FE6F654A1289B4595 (void);
// 0x000001C0 System.Void Program.Editor.Model::set_IsWriteHistory(System.Boolean)
extern void Model_set_IsWriteHistory_mC31712A7FC082CABA44BB58C5E52AAE6DA8A5EB0 (void);
// 0x000001C1 System.Void Program.Editor.Model::Awake()
extern void Model_Awake_m8C2022DEEEB8D39FD613E18DD975852CC6771815 (void);
// 0x000001C2 System.Void Program.Editor.Model::Start()
extern void Model_Start_m297FB3D2C041D4562D2B688D249A3C2DA69CCF10 (void);
// 0x000001C3 System.Void Program.Editor.Model::ActiveEditor()
extern void Model_ActiveEditor_mD81789AECF7F040CAD3D966A068D9AB941753B1E (void);
// 0x000001C4 UnityEngine.Mesh[] Program.Editor.Model::GetMeshs()
extern void Model_GetMeshs_mFBD4E34AD09A7F67A990A82FDDDBB74B3DC2ECE3 (void);
// 0x000001C5 System.Void Program.Editor.Model::ActiveEditorCallBack()
extern void Model_ActiveEditorCallBack_m1C17913D3F8F3B0425F6B6F65C236B7B9BF7C971 (void);
// 0x000001C6 System.Void Program.Editor.Model::DisableEditor()
extern void Model_DisableEditor_mC01C91F00B817B9D824AD3359CD681E80A3416C9 (void);
// 0x000001C7 Program.UI.UV Program.Editor.Model::GetUv()
extern void Model_GetUv_mDC40D9D6AB3BEC2AD8A1A8E17FC6212F401ADDB3 (void);
// 0x000001C8 System.Void Program.Editor.Model::DisableEditorCallBack()
extern void Model_DisableEditorCallBack_m2469DD124C4CF69A785C289DDE10DBF7A831F7A0 (void);
// 0x000001C9 System.Boolean Program.Editor.Model::IsRotateCamera()
extern void Model_IsRotateCamera_m4F0E7851E3851EF197F089B0E57B502E2466AF54 (void);
// 0x000001CA UnityEngine.Vector3 Program.Editor.Model::GetDefaultRotate()
extern void Model_GetDefaultRotate_m18D2E29B71E46AFE4302640F97717D07713D8390 (void);
// 0x000001CB System.Void Program.Editor.Model::CreateNewTexture()
extern void Model_CreateNewTexture_m9FDB59A1B763BEFADC20B6654588EF133450F375 (void);
// 0x000001CC UnityEngine.Vector2 Program.Editor.Model::GetSizeTexture()
// 0x000001CD UnityEngine.Vector3 Program.Editor.Model::Position()
extern void Model_Position_mDBDEC2907537EFD5EA988233CF103863E5F16145 (void);
// 0x000001CE System.Single Program.Editor.Model::ScaleFactor()
// 0x000001CF System.Void Program.Editor.Model::LoadTexture(UnityEngine.Texture2D)
extern void Model_LoadTexture_mDF870893C006DB3A544A7B8D18A59E83DE4F6312 (void);
// 0x000001D0 UnityEngine.Texture2D Program.Editor.Model::GetTexture()
extern void Model_GetTexture_mB4DD5E611A65E551A10310E4A1B38E76C55CD90A (void);
// 0x000001D1 System.Void Program.Editor.Model::CreateNewHistory()
extern void Model_CreateNewHistory_m03D409CC4DF952AD5E61EDE367B2B89584AEB19D (void);
// 0x000001D2 System.Void Program.Editor.Model::NextStory()
extern void Model_NextStory_m363C84A788A42438292385F94EE81F5EAFA663BA (void);
// 0x000001D3 System.Void Program.Editor.Model::BreakStory()
extern void Model_BreakStory_m041EC7C2BE318EFF7DEF424E7475DFC9065C2489 (void);
// 0x000001D4 System.Void Program.Editor.Model::StartWriteHistory()
extern void Model_StartWriteHistory_m2635F8A49CBE57B579FCAEBF332D711702DB8D67 (void);
// 0x000001D5 System.Void Program.Editor.Model::StopWriteHistory()
extern void Model_StopWriteHistory_mC2DE3BF386053C741CEC0FD5A0102AA5294E3C7B (void);
// 0x000001D6 System.Void Program.Editor.Model::UpdateTexture(UnityEngine.Vector2Int[],UnityEngine.Color[])
extern void Model_UpdateTexture_m4522135D13368ABC423E022DDDEE91CEEED431E2 (void);
// 0x000001D7 System.Void Program.Editor.Model::.ctor()
extern void Model__ctor_m961AC4A634D25C4917C0883170175B0DA97DD813 (void);
// 0x000001D8 System.Single Program.Editor.Block::ScaleFactor()
extern void Block_ScaleFactor_m26C6FCC0E50C205411C688ACA91510121B811080 (void);
// 0x000001D9 System.Void Program.Editor.Block::ActiveEditorCallBack()
extern void Block_ActiveEditorCallBack_m7A358A22D3522662C36F1411B441E3FB58190977 (void);
// 0x000001DA System.Boolean Program.Editor.Block::IsRotateCamera()
extern void Block_IsRotateCamera_mAE5CCE061E1F59B7BF4C334B77258D98B5D767C1 (void);
// 0x000001DB UnityEngine.Vector3 Program.Editor.Block::GetDefaultRotate()
extern void Block_GetDefaultRotate_mD838461484D447CC344C4A7D7EF3ECCBA0FCE5BF (void);
// 0x000001DC System.Void Program.Editor.Block::DisableEditorCallBack()
extern void Block_DisableEditorCallBack_m236998E868E039417FB5E46A99C59020AEBB42A1 (void);
// 0x000001DD UnityEngine.Vector2 Program.Editor.Block::GetSizeTexture()
extern void Block_GetSizeTexture_m6AED075AEAD4D994A70D5C016C80CDFADE70F497 (void);
// 0x000001DE System.Void Program.Editor.Block::.ctor()
extern void Block__ctor_m795C5CE2AA74DDFCC8DC9397CCA0DA9D00304524 (void);
// 0x000001DF System.Single Program.Editor.Item::ScaleFactor()
extern void Item_ScaleFactor_m46EEEA0C8BA0A5D51A67ADAACE393CFAF4F2CDA8 (void);
// 0x000001E0 System.Void Program.Editor.Item::ActiveEditorCallBack()
extern void Item_ActiveEditorCallBack_m8808988D224D3C22082FD08EC64EDDB75BBE316B (void);
// 0x000001E1 System.Void Program.Editor.Item::DisableEditorCallBack()
extern void Item_DisableEditorCallBack_mFC1A7D5A7B2E1ED2AAD0A87EF79F612B106B8E7D (void);
// 0x000001E2 UnityEngine.Vector3 Program.Editor.Item::GetDefaultRotate()
extern void Item_GetDefaultRotate_mC275DF63C87C713953DA9B31CEF6B56A07E3ACC5 (void);
// 0x000001E3 System.Boolean Program.Editor.Item::IsRotateCamera()
extern void Item_IsRotateCamera_m474A2A1CA0A3C775705FB4C3CA4892C3B5800BCB (void);
// 0x000001E4 UnityEngine.Vector2 Program.Editor.Item::GetSizeTexture()
extern void Item_GetSizeTexture_mB4DB5C4D62169D5AFDA415D51464AA646EA249DF (void);
// 0x000001E5 System.Void Program.Editor.Item::.ctor()
extern void Item__ctor_m21E529B698479FA292D17DE31D8FED00AB602369 (void);
// 0x000001E6 System.Void Program.Editor.Skin::ActiveEditorCallBack()
extern void Skin_ActiveEditorCallBack_m936983B7AB059ABC49DA2EC4DCFA36A683B922DA (void);
// 0x000001E7 System.Void Program.Editor.Skin::DisableEditorCallBack()
extern void Skin_DisableEditorCallBack_m5A44C73E20E7A8D9521E4403E9D4254D4FA5DB4B (void);
// 0x000001E8 UnityEngine.Vector3 Program.Editor.Skin::GetDefaultRotate()
extern void Skin_GetDefaultRotate_m537DC450E595F62330A5C4637400060C24951F52 (void);
// 0x000001E9 System.Boolean Program.Editor.Skin::IsRotateCamera()
extern void Skin_IsRotateCamera_mA830FA6AC9A168702458D7025CEB39B0B3F0B5FC (void);
// 0x000001EA System.Void Program.Editor.Skin::UpdateMesh()
extern void Skin_UpdateMesh_m930A18055EBD2BE3EDB2660C333B545AEAC371C3 (void);
// 0x000001EB System.Void Program.Editor.Skin::SetActiveDown(System.Boolean)
extern void Skin_SetActiveDown_m87F87FE391CC4BB5C31B6435A7A82236494C2AAE (void);
// 0x000001EC System.Void Program.Editor.Skin::SetActiveUp(System.Boolean)
extern void Skin_SetActiveUp_m6FF2A0733723399D26DD1BC29C1D85FAF6CE43AE (void);
// 0x000001ED UnityEngine.Vector3 Program.Editor.Skin::Position()
extern void Skin_Position_m30DDC9F4C2827A0274C1779FCC519106BC747D51 (void);
// 0x000001EE UnityEngine.Vector2 Program.Editor.Skin::GetSizeTexture()
extern void Skin_GetSizeTexture_m1A61B25C5C15E36C587FDA7D90A4759B37533AF5 (void);
// 0x000001EF System.Single Program.Editor.Skin::ScaleFactor()
extern void Skin_ScaleFactor_m2D64C8131BD7AA896A32E2B83D1A08AB1C677627 (void);
// 0x000001F0 System.Void Program.Editor.Skin::.ctor()
extern void Skin__ctor_m13B87750A5544935CA2996EE0F3D77D548EC29F1 (void);
// 0x000001F1 UnityEngine.Vector2 Program.Editor.UVBlock::GetPositionMouse()
extern void UVBlock_GetPositionMouse_mF353A3F44CE0BFA1531A6AC57089B77FD5A56E3E (void);
// 0x000001F2 System.Void Program.Editor.UVBlock::SetTexture(UnityEngine.Texture2D)
extern void UVBlock_SetTexture_m82263E26DA8F3EED156C9154EA2765213C963AF8 (void);
// 0x000001F3 System.Void Program.Editor.UVBlock::.ctor()
extern void UVBlock__ctor_mB14755C1025E71EB096D08E3A3613DEBF07ECED3 (void);
// 0x000001F4 UnityEngine.Vector2 Program.Editor.UVSkin::GetPositionMouse()
extern void UVSkin_GetPositionMouse_m3D3D63DF6249D7352F251AB3D169AC4D1BF693DF (void);
// 0x000001F5 System.Void Program.Editor.UVSkin::SetTexture(UnityEngine.Texture2D)
extern void UVSkin_SetTexture_m19BF7D63A11ED4C55732D88B0DE97F70D433EE86 (void);
// 0x000001F6 System.Void Program.Editor.UVSkin::.ctor()
extern void UVSkin__ctor_m54EFD03ACCF929D9E223D97D5BC32BB526736BEF (void);
// 0x000001F7 System.Void Program.Editor.Paint::Start()
extern void Paint_Start_mC67AC9EAACC64BB5439DFAAB0018FA197CBE2991 (void);
// 0x000001F8 System.Void Program.Editor.Paint::LateUpdate()
extern void Paint_LateUpdate_mF866A57CC8636722A01BD661DCD9472146BAC7B5 (void);
// 0x000001F9 System.Void Program.Editor.Paint::TasselPaint()
extern void Paint_TasselPaint_mD0F4D2C8B1D21B0BE578EFB9F7353B8D11A95CC4 (void);
// 0x000001FA System.Void Program.Editor.Paint::FillPaint()
extern void Paint_FillPaint_mE2994AF4EE0AD3A3478B0A7B584BBEC50636CC72 (void);
// 0x000001FB System.Void Program.Editor.Paint::TakeColor()
extern void Paint_TakeColor_mC1B92541383CBE819E0B83C48E31A08F80A72CC6 (void);
// 0x000001FC System.Void Program.Editor.Paint::Erase()
extern void Paint_Erase_m1AB3E52C0C4A38DB4A204A5E68D464CD0DBE7F08 (void);
// 0x000001FD System.Void Program.Editor.Paint::.ctor()
extern void Paint__ctor_m7BDD827DBCBB58E636BAFB413205F0B5F1334B20 (void);
// 0x000001FE System.Void Program.Editor.Erase::SetParams()
extern void Erase_SetParams_mE16F584F3EAD9EFF5E01D5DD8E5685AC9AE7E833 (void);
// 0x000001FF System.Void Program.Editor.Erase::Work(UnityEngine.RaycastHit)
extern void Erase_Work_mC7542AB7CFF499F5AAF6D9E0709A82990BEEE873 (void);
// 0x00000200 System.Void Program.Editor.Erase::WorkUV(UnityEngine.Vector2)
extern void Erase_WorkUV_m8EC7E89E081772C22E346E718D49EC08635358E2 (void);
// 0x00000201 System.Void Program.Editor.Erase::.ctor()
extern void Erase__ctor_mF7F6344AA5EE0970AD390D65CBF577076131484A (void);
// 0x00000202 System.Void Program.Editor.Fill::SetParams()
extern void Fill_SetParams_mA9E75627EC627D9BECBF15429395C1513EF6FE12 (void);
// 0x00000203 System.Void Program.Editor.Fill::Work(UnityEngine.RaycastHit)
extern void Fill_Work_m17BF5423B6125CD96F542BEC2F9AFC0AA9E05D09 (void);
// 0x00000204 System.Void Program.Editor.Fill::WorkUV(UnityEngine.Vector2)
extern void Fill_WorkUV_mB2CC381888C1FB58DE2BA7ACEBF753BD83554ED4 (void);
// 0x00000205 System.Void Program.Editor.Fill::.ctor()
extern void Fill__ctor_m3099C5D9EB450183CAE083CD9A4844B184129768 (void);
// 0x00000206 System.Void Program.Editor.PaintTassel::SetParams()
extern void PaintTassel_SetParams_m8DBF5A4D51983677ED7C53428B776D37A478D644 (void);
// 0x00000207 System.Void Program.Editor.PaintTassel::Work(UnityEngine.RaycastHit)
extern void PaintTassel_Work_mEA27015CCEFF2B3B04AC0E41E456CE1E5DE156CB (void);
// 0x00000208 System.Void Program.Editor.PaintTassel::WorkUV(UnityEngine.Vector2)
extern void PaintTassel_WorkUV_mCF33E00AE1555CF222A7BEFE6A01C8C2CC43850C (void);
// 0x00000209 System.Void Program.Editor.PaintTassel::.ctor()
extern void PaintTassel__ctor_m3D0A42EFBFD101E754064BECE4A7DFFF84A58A01 (void);
// 0x0000020A System.Void Program.Editor.TakeColor::SetParams()
extern void TakeColor_SetParams_m71065478F6AF0BF7DF1E4AD9D3E8E0715F96ACEE (void);
// 0x0000020B System.Void Program.Editor.TakeColor::Work(UnityEngine.RaycastHit)
extern void TakeColor_Work_mBE0FAD7AAD90DE99502E84BEC9A707CB0DC334EE (void);
// 0x0000020C System.Void Program.Editor.TakeColor::WorkUV(UnityEngine.Vector2)
extern void TakeColor_WorkUV_m995ACDAAD4613D176ED728BC3D015A26FF344D00 (void);
// 0x0000020D System.Void Program.Editor.TakeColor::.ctor()
extern void TakeColor__ctor_m9BEA17B68F94ED3AAE8DC3185C360FFD915B4E2D (void);
// 0x0000020E Program.UI.Instrument Program.Editor.Tassel::get_TypeInstrument()
extern void Tassel_get_TypeInstrument_m918CF89558E2EB0FCB856E8C3F13580C493DEDD2 (void);
// 0x0000020F System.Void Program.Editor.Tassel::set_TypeInstrument(Program.UI.Instrument)
extern void Tassel_set_TypeInstrument_m398D1C548B8B6AFDB3D582B5497CE0E26B635AE4 (void);
// 0x00000210 System.Void Program.Editor.Tassel::Start()
extern void Tassel_Start_m77AC2BC4D93E8DC6D08D7AA6606E223FCEB3BC2A (void);
// 0x00000211 System.Void Program.Editor.Tassel::SetTypeInstrument(Program.UI.Instrument)
extern void Tassel_SetTypeInstrument_mD033E56C6DFD9F65C87C5731CE192DD883BE1952 (void);
// 0x00000212 System.Void Program.Editor.Tassel::SetIsSaveHistory(System.Boolean)
extern void Tassel_SetIsSaveHistory_mCA8FB271C09AB7B75BAA0CDB5DF82FA7E5425A42 (void);
// 0x00000213 System.Void Program.Editor.Tassel::SetParams()
// 0x00000214 System.Void Program.Editor.Tassel::Update()
extern void Tassel_Update_m45D5763CCCF73A122A672CCC367CE5BA3F0C34E3 (void);
// 0x00000215 System.Void Program.Editor.Tassel::Work(UnityEngine.RaycastHit)
// 0x00000216 System.Void Program.Editor.Tassel::WorkUV(UnityEngine.Vector2)
// 0x00000217 System.Void Program.Editor.Tassel::.ctor()
extern void Tassel__ctor_m4BDDE6A875274FA542F45A3F6277D15957A00694 (void);
// 0x00000218 Program.Editor.FileTextures Program.Editor.FileTextures::LoadFile()
extern void FileTextures_LoadFile_m010BDC70B0674C5E1605F723ADE075E46E5E8D74 (void);
// 0x00000219 System.Void Program.Editor.FileTextures::SaveFile(Program.Editor.FileTextures)
extern void FileTextures_SaveFile_m898BAA84090E1E97174D210192C8FFCD9707E2C8 (void);
// 0x0000021A System.Void Program.Editor.FileTextures::.ctor()
extern void FileTextures__ctor_mEBACD7C851911AAC21A2C2C06A6F3617671A24F2 (void);
// 0x0000021B System.Void Program.Editor.FileTextures::.cctor()
extern void FileTextures__cctor_mA3E342EBD576A5A681E97B9E6302B954B117145B (void);
// 0x0000021C System.Void Program.Editor.TexturesServer::Start()
extern void TexturesServer_Start_mF17ABA42248EE6E7782E1CCD55123CE67C767FCB (void);
// 0x0000021D System.Void Program.Editor.TexturesServer::IsCurrentUpdateAsync(System.UInt32)
extern void TexturesServer_IsCurrentUpdateAsync_mD3230A87794A008595C3A0F0C23CA41328FFAB2A (void);
// 0x0000021E System.Void Program.Editor.TexturesServer::LoadNewVersionTextureAsync(FilerServer.Texture[])
extern void TexturesServer_LoadNewVersionTextureAsync_mE64431D473D2D7D207E44F84C225F61B3FF30649 (void);
// 0x0000021F System.Void Program.Editor.TexturesServer::LoadTextureAsync(FilerServer.Texture[])
extern void TexturesServer_LoadTextureAsync_mCB240B511CC7EF4A38BB15FADFBCCDC3766E41E3 (void);
// 0x00000220 System.Void Program.Editor.TexturesServer::IsLoadAsync(System.UInt32)
extern void TexturesServer_IsLoadAsync_mFEF1394954DB0D5AA9BC364940E628B19CF2E158 (void);
// 0x00000221 System.Void Program.Editor.TexturesServer::EndLoad()
extern void TexturesServer_EndLoad_m49F57B900308AF3DAC0ED9BCA35C012DDA8030A7 (void);
// 0x00000222 System.Void Program.Editor.TexturesServer::EndLoadNoServer()
extern void TexturesServer_EndLoadNoServer_mE418ABC6F57D2B1CC87D9F470E5594BAA6ED893D (void);
// 0x00000223 System.Void Program.Editor.TexturesServer::SaveCurrentFile()
extern void TexturesServer_SaveCurrentFile_m6ED026056AAA606513828662C42F38E530C4BC48 (void);
// 0x00000224 System.Void Program.Editor.TexturesServer::GenerateUnityTexture()
extern void TexturesServer_GenerateUnityTexture_mFB79B1E2B18B242F87BAD7FC8B4924B6A7576B6F (void);
// 0x00000225 System.Void Program.Editor.TexturesServer::ClearVariable()
extern void TexturesServer_ClearVariable_mC788FA2CE14AFC61FE18E75043680C130944D43D (void);
// 0x00000226 Program.Audio.AudioController Program.Audio.AudioController::get_Singleton()
extern void AudioController_get_Singleton_m23473DECF654EB8C9EF43BD85282CEFBF4023594 (void);
// 0x00000227 System.Void Program.Audio.AudioController::set_Singleton(Program.Audio.AudioController)
extern void AudioController_set_Singleton_m68D0A477505FE586F9AA25086DEA44F96AF3A6A9 (void);
// 0x00000228 System.Void Program.Audio.AudioController::Awake()
extern void AudioController_Awake_mAA4F19D0D6BD7B1095D315B59BD70F2E969F1AF2 (void);
// 0x00000229 System.Void Program.Audio.AudioController::PlayOnClickButton()
extern void AudioController_PlayOnClickButton_mB766132F4DA6AFD200BC1C832A64AC10B06DA0A4 (void);
// 0x0000022A System.Void Program.Audio.AudioController::PlayOnClickColorButton()
extern void AudioController_PlayOnClickColorButton_m2B6A898AB5F8DFF20BF8747D0C9DCD22A8C72DCC (void);
// 0x0000022B System.Void Program.Audio.AudioController::PlayPaint()
extern void AudioController_PlayPaint_mE753C0F7E2884E85F62A58EFF501599904F059F8 (void);
// 0x0000022C System.Void Program.Audio.AudioController::OffPaint()
extern void AudioController_OffPaint_mC95C34C18515DD2A2AB9870072A4425AEB5C4EB2 (void);
// 0x0000022D System.Void Program.Audio.AudioController::OnDestroy()
extern void AudioController_OnDestroy_m04852E91DB5900DF3886D0627FB6351181761B94 (void);
// 0x0000022E System.Void Program.Audio.AudioController::.ctor()
extern void AudioController__ctor_mE676F1CCDD16C94E5DE855AEB418212FA7556995 (void);
// 0x0000022F System.Void Program.Audio.ButtonClick::Start()
extern void ButtonClick_Start_mDD7B4BD98FAD6D3F3B0456F1876C8C3F28583A7D (void);
// 0x00000230 System.Void Program.Audio.ButtonClick::.ctor()
extern void ButtonClick__ctor_mE15DE3184E732487F71092E0529E762A317C6056 (void);
// 0x00000231 System.Void Program.Audio.ButtonClickColor::Start()
extern void ButtonClickColor_Start_mDB44CCF14679E799AD883C65D970D04840C1D737 (void);
// 0x00000232 System.Void Program.Audio.ButtonClickColor::.ctor()
extern void ButtonClickColor__ctor_m4A9ECC24E5A4C99C3439F9B38F99BF51E02AEEB8 (void);
// 0x00000233 System.Void Program.Audio.SetVolume::Start()
extern void SetVolume_Start_m2846B497841F1B406EC2CB006480019F647DF4A0 (void);
// 0x00000234 System.Void Program.Audio.SetVolume::Edit()
extern void SetVolume_Edit_m75F26D0CC88774C365A9AF2D65071B32207ADEB0 (void);
// 0x00000235 System.Void Program.Audio.SetVolume::OnDestroy()
extern void SetVolume_OnDestroy_mFB536A0B9846942C6873ABD51152C35846A75524 (void);
// 0x00000236 System.Void Program.Audio.SetVolume::.ctor()
extern void SetVolume__ctor_m398B5582FE254E12B0B2D37F3AD06971F14FBE51 (void);
// 0x00000237 System.Void Program.Audio.VolumeAudio::add_Editor(System.Action)
extern void VolumeAudio_add_Editor_m129CC5D58B5CB0F17A2CB47BAFA30D576BA84364 (void);
// 0x00000238 System.Void Program.Audio.VolumeAudio::remove_Editor(System.Action)
extern void VolumeAudio_remove_Editor_mC2A7A7BFE820002D2ED7E321813E21CFDA4DEECA (void);
// 0x00000239 System.Void Program.Audio.VolumeAudio::Start()
extern void VolumeAudio_Start_mCE3B80951A6DBAE1147A2F9D78FF0113AC9F9885 (void);
// 0x0000023A System.Void Program.Audio.VolumeAudio::SetParams()
extern void VolumeAudio_SetParams_m0CB17BA9640BD8F074AE9B693A22A77F7ACFC6E9 (void);
// 0x0000023B System.Object Program.Audio.VolumeAudio::GetOption()
extern void VolumeAudio_GetOption_mCA37FCDF325C9053696502E836629D90F5BF22A0 (void);
// 0x0000023C System.Void Program.Audio.VolumeAudio::SetOption(System.Object)
extern void VolumeAudio_SetOption_mCEEAD27F2D7A9AF578F41F49880132FDE655D59C (void);
// 0x0000023D System.Void Program.Audio.VolumeAudio::.ctor()
extern void VolumeAudio__ctor_m678EB538EC35326D94802FE1128B1AEBC6D8E3C9 (void);
// 0x0000023E System.Void Program.Audio.VolumeAudio::.cctor()
extern void VolumeAudio__cctor_m6F4BE78D061E0775FF37D2F33C01DDA8FDB63307 (void);
// 0x0000023F System.Void Program.Audio.VolumeAudio::<Start>b__5_0(System.Single)
extern void VolumeAudio_U3CStartU3Eb__5_0_mF07DE3ABA2B6761464B1DBEF8B4543BBB229E6D9 (void);
// 0x00000240 System.Void FilerServer.Texture/Pixel::.ctor()
extern void Pixel__ctor_mC565487D5E165D777AC9E446E62C4A01C3844BE3 (void);
// 0x00000241 System.Boolean FilerServer.Base.BaseConnect/BaseProcces::get_isEnd()
extern void BaseProcces_get_isEnd_m8F6AA9B2BB0E3D584BA0CB277ED21E65DD002067 (void);
// 0x00000242 System.Void FilerServer.Base.BaseConnect/BaseProcces::set_isEnd(System.Boolean)
extern void BaseProcces_set_isEnd_mB7801225FC730DD85B549BA70DDA25A1CECBAF04 (void);
// 0x00000243 System.Void FilerServer.Base.BaseConnect/BaseProcces::Init(System.Action,System.Action)
extern void BaseProcces_Init_m6A9D3FB045080842FBB72839A5371973092FD1FF (void);
// 0x00000244 System.Void FilerServer.Base.BaseConnect/BaseProcces::Start()
extern void BaseProcces_Start_m19956E45012AEF19F4054499881CDB51F047CDD5 (void);
// 0x00000245 System.Void FilerServer.Base.BaseConnect/BaseProcces::Update()
extern void BaseProcces_Update_mBC2C04C98E90C05C6C9347EDC4F03F9C64C1E5BD (void);
// 0x00000246 System.Void FilerServer.Base.BaseConnect/BaseProcces::CallUpdate()
extern void BaseProcces_CallUpdate_m1AC15AA8E1DA171F18003C180673F27ABC6AEB72 (void);
// 0x00000247 System.Void FilerServer.Base.BaseConnect/BaseProcces::Connect()
extern void BaseProcces_Connect_mCD8ED8C8738CE882039E305BE5F30B0503F5DF45 (void);
// 0x00000248 System.Void FilerServer.Base.BaseConnect/BaseProcces::End()
extern void BaseProcces_End_m1CAF78A6E42FCE331CA98540417F50F507CD356B (void);
// 0x00000249 System.Void FilerServer.Base.BaseConnect/BaseProcces::CallEnd()
extern void BaseProcces_CallEnd_m80AD04944941EC21BE886CB85AA358860B7AD1AC (void);
// 0x0000024A System.Void FilerServer.Base.BaseConnect/BaseProcces::.ctor()
extern void BaseProcces__ctor_m61F338957AFD830C5F4E6C51CF2F0901838ACC00 (void);
// 0x0000024B System.Void FilerServer.Base.BaseConnect/BaseProcces::<Update>b__8_0()
extern void BaseProcces_U3CUpdateU3Eb__8_0_mAD6652D6C6523D25840710F6A2E3C0C01767203F (void);
// 0x0000024C System.Void FilerServer.Base.BaseConnect/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m458EF191E5043A702A3C52755EF31A108D932636 (void);
// 0x0000024D System.Void FilerServer.Base.BaseConnect/<>c__DisplayClass12_0::<LoadListNews>b__1()
extern void U3CU3Ec__DisplayClass12_0_U3CLoadListNewsU3Eb__1_m0F19F288E5F00A4CD4303DCE2956E7EE61030768 (void);
// 0x0000024E System.Void FilerServer.Base.BaseConnect/<>c::.cctor()
extern void U3CU3Ec__cctor_m849E92F209610545EB3B04B9F9DB97DD78A98B21 (void);
// 0x0000024F System.Void FilerServer.Base.BaseConnect/<>c::.ctor()
extern void U3CU3Ec__ctor_m1126AF5772142BFAABC4BD9827DC04BDA6A910C5 (void);
// 0x00000250 System.Void FilerServer.Base.BaseConnect/<>c::<LoadListNews>b__12_0()
extern void U3CU3Ec_U3CLoadListNewsU3Eb__12_0_m733029865DBE1A872E6C2CB65091C83805C7626F (void);
// 0x00000251 System.Void FilerServer.Base.BaseConnect/<>c::<LoadListNameTexture>b__13_0()
extern void U3CU3Ec_U3CLoadListNameTextureU3Eb__13_0_m24FB035746901BD7EBAC3D875ED204C49A8D7A4A (void);
// 0x00000252 System.Void FilerServer.Base.BaseConnect/<>c::<LoadListTexture>b__14_0()
extern void U3CU3Ec_U3CLoadListTextureU3Eb__14_0_mA4331803C669853941BB36956E5C7943D30985C2 (void);
// 0x00000253 System.Void FilerServer.Base.BaseConnect/<>c::<LoadNumUpdateNews>b__15_0()
extern void U3CU3Ec_U3CLoadNumUpdateNewsU3Eb__15_0_m43BCAD8DF847B86917CF77EE72BB78EEBCC23848 (void);
// 0x00000254 System.Void FilerServer.Base.BaseConnect/<>c::<LoadNumUpdateTexture>b__16_0()
extern void U3CU3Ec_U3CLoadNumUpdateTextureU3Eb__16_0_mC1B7A50C363A4AD1AE146BA005E2CA632CC8C545 (void);
// 0x00000255 System.Void FilerServer.Base.BaseConnect/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mC68A492B995F2A800972B44B3D56D4D637069036 (void);
// 0x00000256 System.Void FilerServer.Base.BaseConnect/<>c__DisplayClass13_0::<LoadListNameTexture>b__1()
extern void U3CU3Ec__DisplayClass13_0_U3CLoadListNameTextureU3Eb__1_mA4A10BE42B584B61042F67CEBD4C35E6D81E2828 (void);
// 0x00000257 System.Void FilerServer.Base.BaseConnect/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m5C39B57A55779572A8376B4F9438AE999F76F803 (void);
// 0x00000258 System.Void FilerServer.Base.BaseConnect/<>c__DisplayClass14_0::<LoadListTexture>b__1()
extern void U3CU3Ec__DisplayClass14_0_U3CLoadListTextureU3Eb__1_mFB4D19B2A6B3EE7F7DE41C08C2250D1FF2481340 (void);
// 0x00000259 System.Void FilerServer.Base.BaseConnect/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m0509A99C0AD409C7F3513AD4D1933D24CEB008AA (void);
// 0x0000025A System.Void FilerServer.Base.BaseConnect/<>c__DisplayClass15_0::<LoadNumUpdateNews>b__1()
extern void U3CU3Ec__DisplayClass15_0_U3CLoadNumUpdateNewsU3Eb__1_m26B53DD2114EC93CAE27CA520380302436768D3B (void);
// 0x0000025B System.Void FilerServer.Base.BaseConnect/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m8A97FBA3810EE76ED61F7386DAFB39EAE25F7BC4 (void);
// 0x0000025C System.Void FilerServer.Base.BaseConnect/<>c__DisplayClass16_0::<LoadNumUpdateTexture>b__1()
extern void U3CU3Ec__DisplayClass16_0_U3CLoadNumUpdateTextureU3Eb__1_m42B7856416670B50B519134CEAA5361B4800F5CE (void);
// 0x0000025D System.Void Program.UI.BodyPanel/<>c::.cctor()
extern void U3CU3Ec__cctor_mFB447CA74623BFB853B20192A3BB156A896F178A (void);
// 0x0000025E System.Void Program.UI.BodyPanel/<>c::.ctor()
extern void U3CU3Ec__ctor_m219117CD33D8037CB10012C35FCCFAF38B21B619 (void);
// 0x0000025F System.Void Program.UI.BodyPanel/<>c::<Awake>b__35_0()
extern void U3CU3Ec_U3CAwakeU3Eb__35_0_m58535A70F71FED669D637CEEC787F64F987C0A8F (void);
// 0x00000260 System.Void Program.UI.BodyPanel/<>c::<Awake>b__35_1()
extern void U3CU3Ec_U3CAwakeU3Eb__35_1_m6210BB263252546A9880CB7CA24F68334C7F9ED8 (void);
// 0x00000261 System.Void Program.UI.BodyPanel/<>c::<SetOptionBodyParts>b__36_0()
extern void U3CU3Ec_U3CSetOptionBodyPartsU3Eb__36_0_mA4EBB0EB599C1DDE16716FECE0272D2371F85425 (void);
// 0x00000262 System.Void Program.UI.DefaultCamera/<>c::.cctor()
extern void U3CU3Ec__cctor_mD2BC887CE7A79175439D133D7586980F56D4479A (void);
// 0x00000263 System.Void Program.UI.DefaultCamera/<>c::.ctor()
extern void U3CU3Ec__ctor_m6E994BF29BC2D19215C5E3B73A23592ABCBC1463 (void);
// 0x00000264 System.Void Program.UI.DefaultCamera/<>c::<Start>b__4_0()
extern void U3CU3Ec_U3CStartU3Eb__4_0_mB0D4DB18A8A65187B21EDE9D57E3C5F046855AAE (void);
// 0x00000265 System.Void Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::.ctor(System.Int32)
extern void U3CLoadTextureWaitU3Ed__11__ctor_mA8EFBFFC7538B986E47910341B21AAFC99C1D578 (void);
// 0x00000266 System.Void Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::System.IDisposable.Dispose()
extern void U3CLoadTextureWaitU3Ed__11_System_IDisposable_Dispose_m32278EABA09B57F45C467C7AC661D922A044A128 (void);
// 0x00000267 System.Boolean Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::MoveNext()
extern void U3CLoadTextureWaitU3Ed__11_MoveNext_mA8B2805F31A2027DACB264C59861C31FF622618C (void);
// 0x00000268 System.Object Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadTextureWaitU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30AD20615A56147C89D07E4E825A3D1BCA280C41 (void);
// 0x00000269 System.Void Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::System.Collections.IEnumerator.Reset()
extern void U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_Reset_m1F745D6ACC0C15F9630C12F79F928F663ADC18B7 (void);
// 0x0000026A System.Object Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_get_Current_mC8A4904545EADE0AB3EEE97FED4AFE8FECF905A9 (void);
// 0x0000026B System.Void Program.UI.LoadMenu/<>c::.cctor()
extern void U3CU3Ec__cctor_m96FB4CD47DAA0FF961F65D090EA7FDDAE16F78E4 (void);
// 0x0000026C System.Void Program.UI.LoadMenu/<>c::.ctor()
extern void U3CU3Ec__ctor_mD73860545FF53EC49CA9184FCAD0FE2FFF78DEA0 (void);
// 0x0000026D System.Void Program.UI.LoadMenu/<>c::<Start>b__1_0()
extern void U3CU3Ec_U3CStartU3Eb__1_0_mD72F8ED180F42D31D96869E7F8B707EB1343F546 (void);
// 0x0000026E System.Void Program.UI.ColorItem/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m9D47667D7B30A51BEC5E15A03C1792D622CB7FDB (void);
// 0x0000026F System.Void Program.UI.ColorItem/<>c__DisplayClass2_0::<Init>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CInitU3Eb__0_m8CD7ED6D1C0E69BAA6263927FD31EC9E3F2D4ABA (void);
// 0x00000270 System.Void Program.UI.MenuPaltira/<>c::.cctor()
extern void U3CU3Ec__cctor_m493236BB568A424AD3F78541DC4A613CFBB56A9D (void);
// 0x00000271 System.Void Program.UI.MenuPaltira/<>c::.ctor()
extern void U3CU3Ec__ctor_m7E18D07EDEB57DA53149F9156B637B1C2A1EDD0B (void);
// 0x00000272 Palitra Program.UI.MenuPaltira/<>c::<OpenCallBack>b__19_0(ProjectPalitra)
extern void U3CU3Ec_U3COpenCallBackU3Eb__19_0_m646AC0C43508290335E088AAFCFB92BD6DBA8770 (void);
// 0x00000273 Palitra Program.UI.MenuPaltira/<>c::<UpdateList>b__24_0(ProjectPalitra)
extern void U3CU3Ec_U3CUpdateListU3Eb__24_0_m60AFB0C0476B0A68B3866EBC613EA173687FA62F (void);
// 0x00000274 System.Void Program.UI.PalitraItem/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mA560F3F96A665FBA0BC86AC587B274466E6760FF (void);
// 0x00000275 System.Void Program.UI.PalitraItem/<>c__DisplayClass2_0::<Init>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CInitU3Eb__0_mE57306A6603FECA78567E73E3081B6B51FBA89AE (void);
// 0x00000276 System.Void Program.UI.PalitraPanel/<>c::.cctor()
extern void U3CU3Ec__cctor_m86622DB45A148F39C2F7FA7FFD5621382DB60BCF (void);
// 0x00000277 System.Void Program.UI.PalitraPanel/<>c::.ctor()
extern void U3CU3Ec__ctor_mF7D1199BFF2C780D70E9D73848C8D33ADFDA4F72 (void);
// 0x00000278 System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_0()
extern void U3CU3Ec_U3CStartU3Eb__39_0_m70562F083C46F792C0472CB5D1679B6C75A69F2E (void);
// 0x00000279 System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_1()
extern void U3CU3Ec_U3CStartU3Eb__39_1_mF52496C719D1807D07433C2CCE00C1F67145154F (void);
// 0x0000027A System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_2()
extern void U3CU3Ec_U3CStartU3Eb__39_2_m490C9C997BDCC3AB3927EB28E491C4BB6BB1565E (void);
// 0x0000027B System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_3()
extern void U3CU3Ec_U3CStartU3Eb__39_3_mF141EB1A932A2FBBD567E52EB7222A0486E95518 (void);
// 0x0000027C System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_4()
extern void U3CU3Ec_U3CStartU3Eb__39_4_mCB1E9C476F4ADF5714F7A25C2FFDF35B4A90005A (void);
// 0x0000027D System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_5()
extern void U3CU3Ec_U3CStartU3Eb__39_5_m46A52B92D10C4136C07278213C21B8D22D463A32 (void);
// 0x0000027E System.Void Program.UI.ProgramUI/CorrectCanvance::SetScale(UnityEngine.UI.CanvasScaler,UnityEngine.Vector2)
extern void CorrectCanvance_SetScale_mA56FF70FB43A05EC9853C4F876EE8D34779434DF (void);
// 0x0000027F System.Void Program.Option.OptionController/<>c::.cctor()
extern void U3CU3Ec__cctor_mF7D5A7EBF23935B269753913300E79FE8CACC96D (void);
// 0x00000280 System.Void Program.Option.OptionController/<>c::.ctor()
extern void U3CU3Ec__ctor_m50BC0DBD2EC29FE4E39FD44C84748386CC5260E9 (void);
// 0x00000281 System.Object Program.Option.OptionController/<>c::<OnDestroy>b__3_0(Program.Option.OptionObject)
extern void U3CU3Ec_U3COnDestroyU3Eb__3_0_mF4334CBCDBCE27553F3885D1509075FE41E78823 (void);
// 0x00000282 System.Void Program.Menu.ListNews/<LoadNewsWait>d__8::.ctor(System.Int32)
extern void U3CLoadNewsWaitU3Ed__8__ctor_m954E460A8D2E865D49227C50AD953ADB62BD3751 (void);
// 0x00000283 System.Void Program.Menu.ListNews/<LoadNewsWait>d__8::System.IDisposable.Dispose()
extern void U3CLoadNewsWaitU3Ed__8_System_IDisposable_Dispose_mD506D633D37BB4EDC8E584479ED3C7D4D05326C2 (void);
// 0x00000284 System.Boolean Program.Menu.ListNews/<LoadNewsWait>d__8::MoveNext()
extern void U3CLoadNewsWaitU3Ed__8_MoveNext_mB01976F71C96F54106C92D71253B8EBF10A86D5A (void);
// 0x00000285 System.Object Program.Menu.ListNews/<LoadNewsWait>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadNewsWaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEA5FD9FFF67794B08B86C64DE1B8AA4D629E6CD (void);
// 0x00000286 System.Void Program.Menu.ListNews/<LoadNewsWait>d__8::System.Collections.IEnumerator.Reset()
extern void U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_Reset_m53159E79EBA8B80BAC673EDE60518A4F30D17A98 (void);
// 0x00000287 System.Object Program.Menu.ListNews/<LoadNewsWait>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_get_Current_m3172BAD8411C52CFB7BBDC7E0C0F447315DFB4C5 (void);
// 0x00000288 System.Void Program.Menu.UIMenu/<>c::.cctor()
extern void U3CU3Ec__cctor_m9DDBB15F18EE8E10C6D13FE76ED8E43EC5F4732A (void);
// 0x00000289 System.Void Program.Menu.UIMenu/<>c::.ctor()
extern void U3CU3Ec__ctor_mC295447BB5C8F7D4EE7E0278E80DF0EC99E1264D (void);
// 0x0000028A System.Void Program.Menu.UIMenu/<>c::<Start>b__3_0()
extern void U3CU3Ec_U3CStartU3Eb__3_0_m093F84DB5C5B1B508B2554397AC744290518253F (void);
// 0x0000028B System.Void Program.Menu.UIMenu/<>c::<Start>b__3_1()
extern void U3CU3Ec_U3CStartU3Eb__3_1_mC0EBF3283D624C5905EE1BB460955B71E640231E (void);
// 0x0000028C Program.Editor.GetPixels/Triangle[] Program.Editor.GetPixels/Triangle::GetConnect()
extern void Triangle_GetConnect_mB0722685A89A197549E71DDEDA17DC7AD119E4DD (void);
// 0x0000028D System.Void Program.Editor.GetPixels/Triangle::.ctor()
extern void Triangle__ctor_m86F2DE51B2CBE08E6040FA345E0176C69342B2ED (void);
// 0x0000028E System.Int32 Program.Editor.Model/History::get_CurrentStory()
extern void History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE (void);
// 0x0000028F System.Void Program.Editor.Model/History::set_CurrentStory(System.Int32)
extern void History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2 (void);
// 0x00000290 System.Void Program.Editor.Model/History::.ctor()
extern void History__ctor_mE60B21C617580D10000829815C4762950E4A5A28 (void);
// 0x00000291 System.Void Program.Editor.Model/History::NextStory(UnityEngine.Texture2D)
extern void History_NextStory_mD6D584E8C7743EB904F5CF263121ED1BCE876C02 (void);
// 0x00000292 System.Void Program.Editor.Model/History::BreakSory(UnityEngine.Texture2D)
extern void History_BreakSory_mEEAE1220F52A2841F4A37D0BCF265CE79854C2BB (void);
// 0x00000293 System.Void Program.Editor.Model/History::AddSotry(Program.Editor.Model/Story)
extern void History_AddSotry_m7B3C4346D3E540DBB4E3D8F9CFCF4BC8D2DFDAB7 (void);
// 0x00000294 System.Void Program.Editor.Model/History::ClearStory()
extern void History_ClearStory_m31DAD42847EB04B4B3DC76CBB4CD574D652B0845 (void);
// 0x00000295 System.Void Program.Editor.Model/History::ClearStory(System.Int32)
extern void History_ClearStory_m74055B06CDC0551ACF9A841D7F110EAED6B2DC50 (void);
// 0x00000296 System.Void Program.Editor.Model/Story::.ctor()
extern void Story__ctor_mED39062554A90BF1D8888C0729EBCABF22F6BD56 (void);
// 0x00000297 System.Void Program.Editor.Model/<>c::.cctor()
extern void U3CU3Ec__cctor_m709C23D26AAB88E6BC592F1D6C56C0974B94275C (void);
// 0x00000298 System.Void Program.Editor.Model/<>c::.ctor()
extern void U3CU3Ec__ctor_m8AF44807C9189D0DDBEB1F38D11927CD90DC7247 (void);
// 0x00000299 UnityEngine.Mesh Program.Editor.Model/<>c::<GetMeshs>b__14_0(UnityEngine.MeshRenderer)
extern void U3CU3Ec_U3CGetMeshsU3Eb__14_0_m076B9A41934A3E5FC350998E7ABF554A90322A33 (void);
// 0x0000029A System.Void Program.Audio.ButtonClick/<>c::.cctor()
extern void U3CU3Ec__cctor_m2F762773421FD81FA0A5B495455234128082FC7E (void);
// 0x0000029B System.Void Program.Audio.ButtonClick/<>c::.ctor()
extern void U3CU3Ec__ctor_m40813A2B2E6D775355D3CDFFC065FAD5F1C8F95B (void);
// 0x0000029C System.Void Program.Audio.ButtonClick/<>c::<Start>b__1_0()
extern void U3CU3Ec_U3CStartU3Eb__1_0_m213571C10571A5A8A8319FB88AF9A64F6B4635B3 (void);
// 0x0000029D System.Void Program.Audio.ButtonClickColor/<>c::.cctor()
extern void U3CU3Ec__cctor_mA4257105044085DBF05F0275D69C7AD4DCEB2241 (void);
// 0x0000029E System.Void Program.Audio.ButtonClickColor/<>c::.ctor()
extern void U3CU3Ec__ctor_mB4922A770A757AC0778C8BA26D0811911F196C4E (void);
// 0x0000029F System.Void Program.Audio.ButtonClickColor/<>c::<Start>b__1_0()
extern void U3CU3Ec_U3CStartU3Eb__1_0_m362E47FAACAFA08B0FB3B2C66EB4926C7A49472F (void);
// 0x000002A0 System.Void FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8::MoveNext()
extern void U3CUpdateU3Ed__8_MoveNext_mDDFCC9C7E5C00438CA37A40742B4CF2EF430368A_AdjustorThunk (void);
// 0x000002A1 System.Void FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUpdateU3Ed__8_SetStateMachine_m7CB0536971DEF64B4EDF5E17A72BC6F20ACC8D6B_AdjustorThunk (void);
static Il2CppMethodPointer s_methodPointers[673] = 
{
	FileSystem_SaveFile_m108CF3C9AF75778ED78F32912E41616BF150C4E9,
	FileSystem_LoadFiles_m01D795FF7A5BD65681FDA132855C3FD20412EA1F,
	FileSystem_CheckDirectory_mD174C46AF8403AA047EE495C7FF593195BDBD802,
	TextureFile__ctor_m8EF3A11095670F9C6B22E8DCE40476C34FE6C6E4,
	ClickItem__ctor_m6D5E2FD345695D4BAA58DF428E721D6CF430F6D4,
	ClickItem_Invoke_mD291156C5F623C5C5B6D1A1426C0B4E6DCEE2E04,
	ClickItem_BeginInvoke_mC924A9018511D465F0205F6593661791EE02F74F,
	ClickItem_EndInvoke_mEB3019651C5123B5C7F827B03E5AFA2C4EEE1856,
	ItemFile_get_Texture_m53253EBBBA6158EAB8DCCD722403673B1386CF94,
	ItemFile_set_Texture_mC9765B001469946A0A8EF7B8065DBBA5F043C972,
	ItemFile_get_Text_m6C1987DAD8DFE3E1D90C303D0E0AA5D7DD654564,
	ItemFile_set_Text_m31574CAB93FC184D76FB8E097C470695BC89815F,
	ItemFile_add_Click_m8E9AE2693D5BDE2564D307359FB3FADB33685507,
	ItemFile_remove_Click_mC9B29E8B2CCBC5041E75B90F01168B3CFB6A8E73,
	ItemFile_Initialized_m7EBC018BC7600B5200E55C9D5B36433A948A8B95,
	ItemFile__ctor_m671C49179EA50067B6473BBE1405960478CCD5CF,
	ItemFile_U3CInitializedU3Eb__14_0_m7EF2031D3B9BCCEAED92F21DCDFF8641972D7AEB,
	Palitra_op_Equality_mD64701FDFD65CBCD6539DF6E544C72B83912F00C,
	Palitra_op_Inequality_m8C6F6690EF0DF7A88AFC2395FFFFF1570141C932,
	Palitra__ctor_mE42684B5DA2510A4E21DF619AFFD8508138CF5DB,
	PalitraDataBase__ctor_mBFEC4CF6BC48EA29C620D914F32B5164ED145BBF,
	ProjectPalitra_GetPalitra_m426101BEAACA8C9F1E4F78C72028A9716DC1C513,
	ProjectPalitra_SetPalitra_mD735E317BC37F0457D09110999E5FF69D9D07DDD,
	ProjectPalitra__ctor_m011F65405F52D4F31561354A352255DDBBF708D6,
	LoadObject__ctor_m373804582DCF64DB169EC21CF81BB37F8150A707,
	News__ctor_mE8E0D38C162DAFA8E9A7234B34546CE72813115D,
	Texture__ctor_m1D3421D9ECEA775F9D63C6F9B12E8A9993419C9D,
	BaseConnect_LoadListNews_mDAF8192ECB408674A978F8C1C917221E3E2D7158,
	BaseConnect_LoadListNameTexture_m9CE94832D364F87B5599EABEFCA6CD12F3C43317,
	BaseConnect_LoadListTexture_m2C475BC55BB2233E071AA9C0D34FB6180BCC0959,
	BaseConnect_LoadNumUpdateNews_m90CC6FDBDB86A391083E4627EA721E9B7576F888,
	BaseConnect_LoadNumUpdateTexture_m2B08CBD5A0956CCE5190107C173106341BFBBA9A,
	BaseConnect_CreateProcces_m4105F16818B435386EDDCEB08D83F3B919B75057,
	BaseConnect_RemoveProcces_m3D2E114B21E59102C6E9B7D3E832F976BAC9FFD7,
	BaseConnect__ctor_m503ECF30A2EFD7BC4972E2776B10B559B37B7EC7,
	BaseConnect__cctor_m4EA8658D9522AE35EFBE8B930E3BA3A7032BF286,
	EndLoadNews__ctor_m58AC263DAC22264B35E74DF1A2C408AEC5308EC4,
	EndLoadNews_Invoke_m7013403953EA78191AE78F998F8FEE0050335948,
	EndLoadNews_BeginInvoke_m9F3CF04D64C7F38DB12CEBF27DC817CF6DCD715B,
	EndLoadNews_EndInvoke_m4F1E659B2472C49C0641822081811688DD40765C,
	EndLoadTexture__ctor_m63CE62344F5AA3FBAB65514F7DB8C66CE90955F8,
	EndLoadTexture_Invoke_m591119F3B3490E5E0FDED267145BDA2A570B7635,
	EndLoadTexture_BeginInvoke_m57F1684A3DB6E7FCD3E82DF5634A2291DE0AAD4E,
	EndLoadTexture_EndInvoke_m6106EA6719886C9032290AA1E7627EE17EEEC03D,
	LoadNum__ctor_mCA4221CA4A0B7BFDDB12D9318C7E2E079AB4A5E0,
	LoadNum_Invoke_mC2A61444A550B9A3985038D2E27ED883B214F14B,
	LoadNum_BeginInvoke_mB0347C3B46F56B8A73F17FFA796BF49955C9E3D8,
	LoadNum_EndInvoke_m29D3333F1477A83D6AA169BF98CD2EDFFB7D855C,
	LoadListNews_get_CountNews_m92E438786ED41318DA81C1FB3DFFE5CF7234F215,
	LoadListNews_set_CountNews_m0A24C4C87F4FE1FAC7E3EC4006787F0E0BDB455B,
	LoadListNews_get_NewsArray_mF722DA3FD7C30C3EC761EFA3974A3B0F54AE8081,
	LoadListNews_set_NewsArray_m166A2E8A25F0880D8C7F6FAAA5ACED2CC6BF12CF,
	LoadListNews_get_IsEndWork_m2D63D92610E037657373AA35FF09F5D7BAA25FF7,
	LoadListNews_set_IsEndWork_m241DDEEC0039C4CED556498BBDD2F083D8662082,
	LoadListNews_CallUpdate_m215A9BE8B8EFA48C4F014613BABB5E32F17C5BB7,
	LoadListNews_CallEnd_m7AAF1B95C771CA1A48F7A92B6A3C549F48C40A3F,
	LoadListNews__ctor_mA27A6E452F4511E02A263929843882BB71761D1E,
	LoadEndUpdateNews_get_IsEndWork_mDB2748229CAAD5B060BFA6607452B9A14EA3CA8A,
	LoadEndUpdateNews_set_IsEndWork_m8D75DDF4F66504494E3CDAC7DB77313817545D39,
	LoadEndUpdateNews_get_Num_mF0D49D7DBB67DE0C6718301F671BC7470E8638C8,
	LoadEndUpdateNews_set_Num_m37601EDFC0456EA7A4BC68273D80ADE82DBAA8A7,
	LoadEndUpdateNews_CallUpdate_mF67C674CC95DEB050494A6A7340CFC7482960548,
	LoadEndUpdateNews_CallEnd_m1642D02C92D2867E91A9DAC85807FE2764EE3DB2,
	LoadEndUpdateNews__ctor_mE96164F858B039E6BDAD178DAC8B06491F5B49F5,
	LoadEndUpdateTexture_get_IsEndWork_mF605A538B16DE996D43EDDB2E79ED8B7AA9999F2,
	LoadEndUpdateTexture_set_IsEndWork_m0C3FE4EBB8B3D2DD9FB8AD10A577FC1135F5FA5F,
	LoadEndUpdateTexture_get_Num_mA2EE4E82CD39EBA34A69AE2EF5B4F9EECB2BA338,
	LoadEndUpdateTexture_set_Num_m321BB637BB2B8EE158C93879E71B15FEEB3F36B4,
	LoadEndUpdateTexture_CallUpdate_m7307ECF90793401E9D37237710A5ACAA4DA3C01D,
	LoadEndUpdateTexture_CallEnd_mBCB3B8C5B4B174326435ABA642AFB42114B5C6E8,
	LoadEndUpdateTexture__ctor_mA9E190EBC0B597ACFA17A65F18FBC41A4D2D3311,
	LoadListTextureName_get_CountTexutre_m147756749F5732A2C443302779F3B9699AD0E7C0,
	LoadListTextureName_set_CountTexutre_m93428BEDC013472F04360688F6DE747A0FFC0926,
	LoadListTextureName_get_TextureArray_m8947F4DCF10C54A327061AEAC24400D221576596,
	LoadListTextureName_set_TextureArray_m34688F069E93DDBC4E45C2F1A430E98ACD9119A7,
	LoadListTextureName_get_IsEndWork_m7DD92BF32C6AD32573965DB0A6E90F3BCEE3BFF7,
	LoadListTextureName_set_IsEndWork_mD6F1D8C1BA620349A1455C4B252C7D85EF2ABFCC,
	LoadListTextureName_CallUpdate_m723ED8EF851E4E3F8FCD2B050D012D06C0B4D559,
	LoadListTextureName_CallEnd_m3AA822D2B53BA283A3DFF090AACF5EDABA8BE15F,
	LoadListTextureName__ctor_m443099D6A625CB448ADE3C2FD183AFFA6A9FB0C6,
	LoadListTexture_get_CountTexutre_m2A4B1FAC71C75827EFA5D64BD81626B19118B0B5,
	LoadListTexture_set_CountTexutre_m328768693BBB339C6180E37946D02693CAE85CFE,
	LoadListTexture_get_TextureArray_mA6E6EDBE325878BEC96FB301C9D44405193B4DE5,
	LoadListTexture_set_TextureArray_m025BC6386770316DB84C605661CF736042EE6619,
	LoadListTexture_get_IsEndWork_m3052A9D89F7B08D72207ACD47961E9D5C674CB1D,
	LoadListTexture_set_IsEndWork_m97BF73EFF2F5B1D964F5951981F639263BBBABB6,
	LoadListTexture_CallUpdate_m6DA05685EC5EC4F9D523E1D49FCBC4681778FD69,
	LoadListTexture_CallEnd_m67C18E5A16F8784040E407B8E089E8BD880FD1E4,
	LoadListTexture__ctor_m932DF32B6D50F85B6A0BF34857E4F93B42C2808B,
	SaveTextureScene_SaveTexutre_mD647505B074F7B6999E71C576C82E100B060C400,
	SaveTextureScene_GetTexture_m9685D4CD509904E15AC2DEF7BBFBB1C6D9485A99,
	DebugQuit_OnApplicationQuit_m94EF4A3530DC1DB5A897861EE191444644D7914D,
	DebugQuit__ctor_mE98073CDBD5087BC4A9E2E9F570EF09E38B6B4D3,
	ProgramStart_get_Singleton_mB8385D9BEC7397EA9A4C13F6909BC82C4D193AC5,
	ProgramStart_set_Singleton_mA48138D894D42ECD87875046BC445E11EABC6318,
	ProgramStart_Awake_mD54D916E7AF28B985F97A7694A3695F39AA76708,
	ProgramStart_Start_m85A40E9800DBC4119BB48C313E6BC3A789DC2EE0,
	ProgramStart_OnDestroy_mE905A1EAFE879082FF3058BAA8049345F7B203CB,
	ProgramStart__ctor_m2B94D688996A0C2414E900277F641A4C85962B04,
	BodyPanel_get_LayerDown_m29403D6F813A24BCB8A9239FDD6573D75D3C9D7D,
	BodyPanel_set_LayerDown_m065C5DE1A0C6D76E23A669094FEC4AC4718F19A4,
	BodyPanel_get_Parts_m1410D3E832A1F784DF4C35D996A6FADA5EFE4F18,
	BodyPanel_set_Parts_m3DA8F269E710CC9622904725F13135E25DBE358E,
	BodyPanel_get_StartRender_mAAB7A1D9289510C59929934F34C22C74FD0BE8AF,
	BodyPanel_set_StartRender_m65096D7AAFD44A49671DF36EC38EDD7ED0DCC304,
	BodyPanel_get_UVEditor_mB67B5BA8BC6C9B3D828712FFE97D6D5E3209E8CE,
	BodyPanel_set_UVEditor_mD108AECBA4C41EAD2B20CE1795C6B500B7706AB8,
	BodyPanel_get_Singleton_m1E3FF48D1E92D595061ECA0A09ACA769629914BF,
	BodyPanel_set_Singleton_m0AC44F12E77EE31F4B7A84DEF68A9BC1D4B5443D,
	BodyPanel_Awake_m8E3CC9541D5AF77E1D5322A88BA9B31AE990888A,
	BodyPanel_SetOptionBodyParts_m4A038D594A81CFCEB333ABDF5D9272C599464CCD,
	BodyPanel_SetHead_m257B56D83527FD494602BE67DE2B5A49ECBD9471,
	BodyPanel_SetBody_m19FF96A89EC1776057126E3855BFB49F26036F94,
	BodyPanel_SetLHand_mCF620CA77D5F0747056CC8641E8E09A85FEFF8BF,
	BodyPanel_SetRHand_m25546DCB8927C4BBC9BEFDF01B7116CF0D92E7F9,
	BodyPanel_SetLFut_m591B0519D465B230D58E29F06749440FD012740F,
	BodyPanel_SetRFut_m8CA1CDFE057BBDDD5A142C7387968F3FCB3E0BD9,
	BodyPanel_OnDestroy_m63C378AA8E134DF4A49F54853896C3E0B83399B5,
	BodyPanel__ctor_m2E0EFB2A7793A0E71CE6A98A2290BF6062A3F708,
	BodyPanel_U3CSetOptionBodyPartsU3Eb__36_1_m33AD094949C86A3429421A06C84D0877DEC3330E,
	BodyPanel_U3CSetOptionBodyPartsU3Eb__36_2_mF1D6080E2B01C3DB832325EDE01AFDDAED438455,
	BodyPanel_U3CSetOptionBodyPartsU3Eb__36_3_m95003663D46376F659843D0A630C3DAA8AA47551,
	BodyPanel_U3CSetOptionBodyPartsU3Eb__36_4_m0537CE1C4AD26325D752F5A8497E8284FEB05898,
	BodyPanel_U3CSetOptionBodyPartsU3Eb__36_5_mBEBBE5BD5BEE427611BD3658AC3BAE00A1496E63,
	BodyPanel_U3CSetOptionBodyPartsU3Eb__36_6_mAF2CD75448661E15E8A71C2FB05EE01D7C12BA74,
	BodyParts_add_EditBodyParts_m6D2CFA64ED6B35DF789F4D17BD52D2672429E2F0,
	BodyParts_remove_EditBodyParts_mF3DDA185AAED7C47D3E5DDF8BC2F73EAE334A722,
	BodyParts_get_IsHead_m42F3D3C3D95E4E17D8A1A761F2A1F0EF09ADC616,
	BodyParts_set_IsHead_mA0DED22213717E97534AF67B9A4A3CC726BED00E,
	BodyParts_get_IsBody_m1AD8FE7BE4C98A163A0FB88D1873E91FF5065BDE,
	BodyParts_set_IsBody_m3EC06336AFEACB098D6BD4A5E058A477C4B776AB,
	BodyParts_get_IsLHand_mCFC75AF18DAA2D51DC9D1A52D07D786782C3DEB9,
	BodyParts_set_IsLHand_m499D7DC626E85298A8E4C6DCC5A2619511EEB2ED,
	BodyParts_get_IsRHand_m01EC79BE6703A765722F61B152BB06A50F66958D,
	BodyParts_set_IsRHand_m58873CBE20EB089705AE37B173D11E7DCC16F075,
	BodyParts_get_IsLFut_mD08598F8903B02CE24E20C912EF7EBD7E9F383EC,
	BodyParts_set_IsLFut_mA923404D97E0EBC15C5E34756BB34351DB200461,
	BodyParts_get_IsRFut_m1D0ACB3E78A8FA9EF4DC6ABF07511A337ED9D69F,
	BodyParts_set_IsRFut_mC32D6936657AE95FA5FB9E9AAA5EC78E88372D66,
	BodyParts_EditBody_m7995F9EB6F65213B19BFD603040D0A0328CF651B,
	BodyParts__ctor_mD4A7F64D4D88BBDD928496D24388013CF0899520,
	DownLayer_add_EditAction_m2ABA6CB845A909612F0BEB7E32B27DF2C9FFACCD,
	DownLayer_remove_EditAction_mF49977A5F1993324D174BFAF65662BF21FED50D9,
	DownLayer_get_Enable_m49AAD772F67A74CE9EA1E2EF80369E7963C9F42A,
	DownLayer_set_Enable_m0BF1AB18F2A620C840531101B5CC7B33022E150A,
	DownLayer__ctor_m0C9CD01A068E620A429B5469AFE56E29F3F95EA9,
	Render_add_StartRenderEvent_mB699EF95F1CDBD4322088BE240531555963D510A,
	Render_remove_StartRenderEvent_m1AF5E76CEDE5897C4DCA7FD2A2A2A33D2BD6D62A,
	Render_StartRender_mD3B88E93D93D28AC89F251010D7B5D6ED1A77E91,
	Render__ctor_m5784A02A02B05F34AF7F4D59AB1D328F206664F6,
	CenterPanel_get_IsOpen_m940BF4B14CFCDE8712C73BA4977DC35915E4A8D9,
	CenterPanel_set_IsOpen_m152CBDFFA76B9CC104CCD915BCCBEF5DFC04561B,
	CenterPanel_OpenPanel_m5E24F72F798F2EE904E3B0FCEE70E386C664F6A6,
	CenterPanel_OpenCallBack_mDC7562EABACD6F7999129BA6F65973347F478AD2,
	CenterPanel_ClosePanel_mEDEA21A75A657D29CD9B31E63E43B0C005109990,
	CenterPanel_CloseCallBack_mE011EDEA85AEB75EB82A25AA7E29839EE94FCE1A,
	CenterPanel__ctor_mAB54FC10825229EC6B016CE6695719C22015D131,
	DefaultCamera_add_ClickButton_m24CEFEDB1358B9016454A446AF9051A56A3B2F15,
	DefaultCamera_remove_ClickButton_m84647F94186D6F54187B10DFC40058CB701D2F47,
	DefaultCamera_Start_m95B80564FACF909DF7941E436BEF5951D4C3C326,
	DefaultCamera__ctor_mFE90765E3CFB763FE7039D02761F13E67F9F4922,
	LoadLibraryPanel_Start_mBD92D971A7618E7C3BAD6AED0862A5F287C60BD4,
	LoadLibraryPanel_LoadTextureWait_mA618B45C0CF015FB67E0CC2BAEC5C514C04186EA,
	LoadLibraryPanel_OpenPanle_m7A6C3B183C93A9E02CB7F38666BEBB5ECC100F4E,
	LoadLibraryPanel_LoadFile_mF21A035C852043735D313E39C15E1F5588253994,
	LoadLibraryPanel_LoadList_mAB71BDBC92DD8B3E5FCE30F951A9AAABD689263B,
	LoadLibraryPanel_OpenCallBack_m77152D53BA2B4BCB869166E02C664363F3CB5D5D,
	LoadLibraryPanel_SetLoadFile_mC29833F0C40F2872B3C32E5CA8595981C646A757,
	LoadLibraryPanel_CloseCallBack_m1FF1FC290FAACB1704C6AA606BFA2689B53977DF,
	LoadLibraryPanel__ctor_m28C5C9D3EA7CA3AD9A9779851A0F124230CEDAE4,
	LoadLibraryPanel_U3CStartU3Eb__10_0_m77BEB50BC1A88F7C4AF9BB91AE23E0B59E668CE4,
	LoadLibraryPanel_U3CStartU3Eb__10_1_m510E620AFE2724D80700CB9844057BB36345D472,
	LoadLibraryPanel_U3CStartU3Eb__10_2_m4960AF4EC184CD83F48532911CDB32D3CD917A17,
	LoadPanel_Start_mB4883A1B57D5041D93536C1A2D7274D83729B733,
	LoadPanel_Load_m61D1DE91738CCE1A7D87A099B78BC2FBD76080BD,
	LoadPanel_SetLoadFile_m5A8B7893EB4DF14C85673D59896B28C606C626FC,
	LoadPanel_LoadFile_mB2F702D225009D9E5DBE6AFEB22E85418479F3C5,
	LoadPanel_LoadList_m89CA4D3ED73BDAAEB057FEA96BE69FC01D089577,
	LoadPanel_OpenCallBack_mA3009F302B2EDB7ED4589544FC588C8842F02D23,
	LoadPanel_CloseCallBack_m712A1A826CDE3AFF67E49E6CE1B5D45C16CAEFD6,
	LoadPanel__ctor_mAFAEA8C101074ACE39F13DAFE50C9EA91BA34258,
	LoadPanel_U3CStartU3Eb__11_0_m5F905EF6A739AA5B00F23B22FD2BF2EDF64E025C,
	LoadPanel_U3CStartU3Eb__11_1_mDF50C8201D6487D2849659B9ADEC3C9B7F2986DC,
	LoadPanel_U3CStartU3Eb__11_2_mE799FD27D48690B248A84024F97E0E2AC1B22915,
	NewFilePanel_Start_mEBDC8C1FB963BEA5423C96FC8EDD3B46C9CB0788,
	NewFilePanel_SetButton_m679C1138A349DA27B2BD89DFF004260858F10BE0,
	NewFilePanel_OpenCallBack_m88E732B808F15D4CDD651D229C0A5D69E96C393E,
	NewFilePanel_CreateNew_m8A6D61F2F84A77453EDB6C6B6E5CF7164BA1F785,
	NewFilePanel_CloseCallBack_m02109F5D5BDBC058E6F6CD5A2A1EFDFACEE08E2D,
	NewFilePanel__ctor_m1EA708788406A70A7F2E742090F6CAA171230343,
	NewFilePanel_U3CStartU3Eb__4_0_m2D133F62A9D1BFF48F5302C4D9A6BB5C1C0C1BC9,
	NewFilePanel_U3CStartU3Eb__4_1_m26F08126DE7B00399E9508433CE748BED69FAC36,
	SavePanel_Start_m670DA1CB11A0A31E2EC033A345F41A55998DDC21,
	SavePanel_SaveFile_m1573E224FC03A8055C9C143C338A4020C2081E52,
	SavePanel_Save_mADBF1F121ED20492111FC0F0BBB16DE6BB39CF98,
	SavePanel_LoadList_m24B38B52D657924E0C054520DCA3BAAD97E89ABD,
	SavePanel_OpenCallBack_m02D57782771C41F3C074A61D7DECC01216BC794F,
	SavePanel_CloseCallBack_mBE94A72C2A44EF9D734DF87D3E23148AADD557F6,
	SavePanel__ctor_m0B7AAEDB1C00497502CCF41AEB50EC92BF3E73A2,
	SavePanel_U3CStartU3Eb__9_0_m06526363D4ED4F05D85EDFFB06F24408C0A902C1,
	SavePanel_U3CStartU3Eb__9_1_m2800BE4939AD9E8A9CCFD2B5E5EC375596E7124C,
	ControllerLaguage_add_EditLanguage_m0E43F99E1EDBEB3A9D92AD2B32D2BAEDE10C73E3,
	ControllerLaguage_remove_EditLanguage_mFDD958D17D6B48C6330DD42B9ADEA5DD5338DF6E,
	ControllerLaguage_get_isActive_mAAB75859C66032897913E16442299EBF5E0E91D2,
	ControllerLaguage_set_isActive_mD16197CBBC968E72943AB612C47A75039709877F,
	ControllerLaguage_Start_m5C79F6602A22F2DA26AA324B4A6DF9B575E39FB9,
	ControllerLaguage_Edit_m00C7535E8AE416C44701DE76D47028A4888EB6F4,
	ControllerLaguage_GetOption_mCDA69CA09D8C276B5DBF514138C5F951F229749F,
	ControllerLaguage_SetOption_mDAACB38C553D0FFA59CDE218000CB48D465A31AF,
	ControllerLaguage__ctor_m7A5AF281A7046416FF01DDC6C6C76F233DFA3088,
	ControllerLaguage_U3CStartU3Eb__8_0_mE39D5D344153FFEFA57B2B535131900903E72576,
	EditText_OnEnable_m48A38A368DB88D4DB03EFE73264C37D45F47C6E2,
	EditText_Edit_m288BE14166DF99DDD8F4421B3D459AE29A34CAAC,
	EditText_SetParams_m360EB8E50403904BDEBF85DE6F5808298061210F,
	EditText_OnDisable_m45925E645AACA1BF85B0B5DB7E1D254B2F21B905,
	EditText__ctor_m1732C5E869D7DEC1624585578F5D9917515CBA8C,
	MenuPanel_add_SaveFile_m05694E995B00E0A5B7C2398D638FFA488C6D9347,
	MenuPanel_remove_SaveFile_m353A16A62CAB468992DE601B9BCF7C82DE5124D5,
	MenuPanel_add_LoadFile_m6BB857F0CAF2C51B2C0963F45B1579AEF5851978,
	MenuPanel_remove_LoadFile_m16C9D55F4A513E0B64BE9488511963928A78E4E6,
	MenuPanel_add_NewFile_m41EF1125E326092467EB209A4928228AE1B03026,
	MenuPanel_remove_NewFile_mA3249B7015A2BCF69645E1F20635414C5A9AE801,
	MenuPanel_add_LoadFileFromLibrary_m238B1208B65BF59D270AC282904BD000EDDD0254,
	MenuPanel_remove_LoadFileFromLibrary_m36D1A8A034C4671C4F9C42C9BDB432D9BAB8F904,
	MenuPanel_add_EditPlayer_m48E07A1778D3D487B49DF3F9FC7233BBBAC42DE5,
	MenuPanel_remove_EditPlayer_mDD89630473B86F3CA70C5A1BE9802613FEC5CC97,
	MenuPanel_add_EditItem_mC2063CA506A5FCD945E4C57A096FD7D6DDD76CBE,
	MenuPanel_remove_EditItem_mFF58CE5FB87F808F327D2E36A9ED3C750ABEBDE4,
	MenuPanel_add_EditBlock_mE9AD06BA55F71022E6F8D72C5E400741A318C6F9,
	MenuPanel_remove_EditBlock_m19CCBB8F2CD9A2BC04160F9345F28D89744EF5B0,
	MenuPanel_get_Singleton_mDE37F6231B0F7936FFEEB0E801085D89EC6A8428,
	MenuPanel_set_Singleton_m635E9F118FFF8B01383828CFA59D048613B2F11F,
	MenuPanel_Awake_mDCC42FA622966755AD67EF4E68312797EFF0FEB1,
	MenuPanel_Start_m085C9377780683BA9CDD0F59C39712644C24D9C7,
	MenuPanel_OnDestroy_mC3B858BD379900A01A3B1B6CFA95C32A2647A928,
	MenuPanel__ctor_m43DACB52B7FF10C1464FA7A383D9AE7028656722,
	MenuPanel_U3CStartU3Eb__33_0_mCC6958034FDEA45ABB33ECF47C1CCAC01601BD9B,
	MenuPanel_U3CStartU3Eb__33_1_m40355F530568FDC7E1DEFBC00946261C315F4AFC,
	MenuPanel_U3CStartU3Eb__33_2_m5DA812FB86F0BDA0354F4D6875B1961C93966BA4,
	MenuPanel_U3CStartU3Eb__33_3_m2FC0FFC86010CBA9D4A3DF4DE23901A886D82333,
	MenuPanel_U3CStartU3Eb__33_4_m8F30655D551DD9CABA8D84BADB4ABBFAE5FDA0AA,
	MenuPanel_U3CStartU3Eb__33_5_m82FA5D1ED9AD9E5BF0A3BFA85CC3F1E768F7AE79,
	MenuPanel_U3CStartU3Eb__33_6_m4B0C097BF677DDA5DC4441A6DAFFBBDE08D81AB6,
	LoadMenu_Start_mEE4864226FBDA14AF6CE7E7DD0A6E66B66DC2D9C,
	LoadMenu__ctor_m21FFAC8987D40CC711A58C0A46EA38DE608328D8,
	ColorItem_Init_mBB4D287B35E82B43C3F43FA54592B3834F36FD53,
	ColorItem_GetColor_m3DB30F988B5F7133D1876B30B7E35D3BF24960B5,
	ColorItem_SetColor_m99A9C921EA813ECD2313B6C6DB169D0E739215A1,
	ColorItem__ctor_mA831CD661DCE470D5184E15682EA18F0D8A04881,
	ColorPalitra_add_TakeItem_m2AB67F861A515A06640B67A3F8B6230B8742787C,
	ColorPalitra_remove_TakeItem_m0588DA2EB6E2C9BD8F33A8D394EE9BA9110D45B2,
	ColorPalitra_SetPalitra_m479A0A763BA4EBDEE1035132D6879D2D9BE81FD4,
	ColorPalitra_GetPalitra_m09CE322D73B00F32490D18CC096E5E91FD9AA05E,
	ColorPalitra_EditColor_m237C3E1075754F4D09C429FA61B218D8F6C9B445,
	ColorPalitra_SetItems_m7A2F26C61E8C1C82F194A75AAD1AE3B464548128,
	ColorPalitra_GetCurrentItem_m2539A5979541103AD6631F8CCDAD6E5CFF81C344,
	ColorPalitra_Take_m915CDF50CB7CE7BCF4583A354CDE2B76F5A041B0,
	ColorPalitra__ctor_mB30CB493D941F2B4F51B18DD8C597F6832D7DC58,
	ColorPickerPanel_Start_m947BA8A24F59240BCDFED78F49C899153B918573,
	ColorPickerPanel_OpenCallBack_mE617E96F5A3E7667E83435694A20AFAA71CE312F,
	ColorPickerPanel_CloseCallBack_mBD98CE2A68054665F23ADF7EDCFD16FF7754A334,
	ColorPickerPanel_InitializedParams_m36FD320831BF670DA309ED796C68B04B6DFB4160,
	ColorPickerPanel_Update_m98E8DE27AD1A9B975905D51B5B7A562252394256,
	ColorPickerPanel_CheckMouseInPicker_m2C88087B2377AE81B16E81ABBAD691440EBD66E0,
	ColorPickerPanel_SliderRGB_m6B19BE722209592445E268230D894D942CBFC1D5,
	ColorPickerPanel_SetGradient_m1C7BBDD3969717ACCA93CC7E835E220A586A3A05,
	ColorPickerPanel_GetColorJoistic_mAB65A48808A7F4BA5B22E41863DB5C0B03EF3EF6,
	ColorPickerPanel_SetColorPalitra_mAF4A3CDC5489146B68D88C805D79E1F252790AD6,
	ColorPickerPanel_SetColorFromPalitra_m40F14B3CC725E9676F325365C73BE95A4C795F17,
	ColorPickerPanel__ctor_mBFEDA0D3E8275BF46204C366A445C94158FE3023,
	ColorPickerPanel_U3CStartU3Eb__8_0_m75A75D7EA72B5C04D91F9A0E891668EF2729D771,
	ColorPickerPanel_U3CStartU3Eb__8_1_m9F169F862462214577F1A6005F98C8EA23F315AB,
	HistoryButton_add_HistoryNext_mFA7B5A0E94A850C99B6F186447423F70F5E53D8F,
	HistoryButton_remove_HistoryNext_m4952CFA1B71AB4C14D5E798971CB5F6E5A427941,
	HistoryButton_add_HistoryBack_mFE5A88B7E7BF434ED70B6D60A5FB87CE48191E82,
	HistoryButton_remove_HistoryBack_mD2863D6DE0ACBCF75E6742C8ADF504E66A41E2F9,
	HistoryButton_BackHistory_m87C5E919DA71B99989BBF0FB67EFFB6DA65C9C8B,
	HistoryButton_NextHistory_m809836890A22F382AD96F564748FBB68C2A3934F,
	HistoryButton__ctor_m4F3E32839BD1DAE4410952C5BBC828036579D706,
	Instruments_get_InstrumentTake_mE8360FD723E9F6466EF0BF10D48301EF3B73B59F,
	Instruments_set_InstrumentTake_mCAD9D564496C1F839D68AA299DE96B668752E99E,
	Instruments_add_TakeInstrument_m647488514FAF8BDC89B6361ACE7D5CDA7975677A,
	Instruments_remove_TakeInstrument_mB0D58527D59E36C769009665538F8831017970BF,
	Instruments_TakePaint_mF5A5660834B59D12BEE85DBECE098C2B9A518867,
	Instruments_TakeColor_m1286AF6ABD47C645C9CA73EE5E37806B59DAF8AA,
	Instruments_TakeFillColor_mB3AE6CBE53415CD455CF250626B7B7BA72739B40,
	Instruments_TakeErase_m556D71FCB88F4B8D858379D644A22DABFD6E47BE,
	Instruments__ctor_m91668F8A830E380E3BEC8501B3B8027EBFB8E69B,
	SetPalitra__ctor_m99982AE5CAEA5EA3094C310F557F31A8832F89E9,
	SetPalitra_Invoke_mA8861B399A428449B552B16EBB075393AB6120FB,
	SetPalitra_BeginInvoke_mC8984D581B3DF9EB55556E9A5E6018FFEF16C0DF,
	SetPalitra_EndInvoke_mF983AE2CD6CE707C2410BDC9861448DC728F6A6E,
	MenuPaltira_add_TakePalitra_mE3BFDB24358C0AE3C122F29D911C18B86CA9F778,
	MenuPaltira_remove_TakePalitra_mC79E72F7911BABB60AC3DA561527D15B9B038AE8,
	MenuPaltira_get_Singleton_mA27F91D69CC9387223DBCDFF52512A59CFB69CAE,
	MenuPaltira_set_Singleton_mAF9BE98C99C52395B40215E2EE515500077D654D,
	MenuPaltira_Awake_mB336714D316F92431EDFF9C7299BEB3B7D7C17A1,
	MenuPaltira_Start_m8A852DFA310C838079A087DF7719EA311B5E9926,
	MenuPaltira_OpenCallBack_mE5068EDB34C6721921AF2BD83022CF2DACFF96EE,
	MenuPaltira_CloseCallBack_mB20D9C02F7022414208A8F20AE7B92FB195EF37A,
	MenuPaltira_NewTakePalitra_m8504090ACB63ECC3B5A1DFD458F6995373A8A5E1,
	MenuPaltira_UpdateList_m88B0B093B4583A390C31D266D1F26C2606D3B34D,
	MenuPaltira_SavePalitra_m2580523A5F49906C941E297E940B9AD7689F056F,
	MenuPaltira_UpdateList_m0199142DFA59CF1F5F98AAA22D7868803F52DFC5,
	MenuPaltira_DeletPalitra_mA597DC0019673F54696BE548B288C68ED4880914,
	MenuPaltira_GetPaliters_m94DA7CDED26AEA7006C69C5D247494CAF0E67F7B,
	MenuPaltira_CheckDirectory_mF88202C3E8BA135B1BC83DCBA32E4C094684FAC4,
	MenuPaltira_OnDestroy_mD1E23567C45390C931E8FA2F6933EABD955428A4,
	MenuPaltira__ctor_mE6516DD85D2AEE9C07A9E715ABA560A9027F398A,
	MenuPaltira_U3CStartU3Eb__18_0_mB0B57DAE05470FABAE4B70A730AAA53D52EF28D4,
	MenuPaltira_U3CStartU3Eb__18_1_mFE44FADF95EBB75F7ECF776DA2C4C04912DAEA42,
	MenuPaltira_U3CStartU3Eb__18_2_m5B895236BB6DCB620EE58B5529018C9099E361CA,
	PalitraItem_Init_m76A3DE7F1F0A34741FCACC2633C18BC8FDDC80FB,
	PalitraItem__ctor_m30744D6CD05D82870174FB15594ECDB5D4293D70,
	PalitraPanel_get_Instrument_m7F79D1932B2C71BB7B236D976366BFDC4965020C,
	PalitraPanel_set_Instrument_mE1F840BF319190F398A837FEA3A67DC41F936436,
	PalitraPanel_get_ColorList_m097292F6376815B20209CCD233B1F23800CD0C02,
	PalitraPanel_set_ColorList_m0C9A3AA69E4B3C52393A5D862DCAD9165E17BF43,
	PalitraPanel_get_History_mD749E9F50722D5B8D5110C265F2CDAEAFDD881AB,
	PalitraPanel_set_History_m5F1EDBD7B6D224B8F7CFB16668B6A03FA8588EB2,
	PalitraPanel_get_Singleton_mCE3314F8DAA7B9C9C3FE5193C610299AA1549574,
	PalitraPanel_set_Singleton_m5AAC644BDE919FCCA0846FC5CABE4328BEEF7630,
	PalitraPanel_Awake_mF47205E2B1A024705F4CD872315E44EAE839960D,
	PalitraPanel_Start_m994E21F280C97AC1CFD024F81C084998FC6412D2,
	PalitraPanel_EditInstrument_m0626CAA879F036AC538785CB2A733605BF7534C1,
	PalitraPanel_EditTakeColor_m8A7E6F119A167F9DD119D1EB741C38ECAD8B6823,
	PalitraPanel_SetNewPalitra_m2A56F48F9C0E7E20588EA56983DE47A0A88B9539,
	PalitraPanel_OnDestroy_m9307798DD41ECF1D45FA3FC3BA43DAD553C6DF91,
	PalitraPanel__ctor_mD4FFA9B8C009BE68CA33A9E5A2906D74A1562CE7,
	PalitraPanel_U3CStartU3Eb__39_6_m5FD8DF5AED30CF7751EB8C00D5D6A4905887E247,
	PalitraPanel_U3CStartU3Eb__39_7_m14B1E4FB7298CCD39B2BE9108F1D9282F7D93FB0,
	PanelProgram_Start_mE3425AA80A4013100071C7D6817128563FD2BFB8,
	PanelProgram_OpenButton_mB0C6C76CB8FCCF17AB0874B74F7AF9BBA01384BA,
	PanelProgram_CloseButton_m1391DCEE96C44F1F1CA84018C12141DDBE70CC7B,
	PanelProgram_SetParams_m69315C8D33FF2FC662D96B98302AEE7A4AC386A6,
	PanelProgram__ctor_m7358BC33BEBF41A699490D625A87055D1CF3C579,
	PanelProgram_U3CStartU3Eb__6_0_mB24D08F5DC87D7C9AB155E06FB3F72C4F69A8706,
	PanelProgram_U3CStartU3Eb__6_1_m38395C281139DE98FAE39AC937126D857AFD6E52,
	ProgramUI_Set_m14462C9548AB7844E1B6B2061FE7CC785E3C1F63,
	ProgramUI_OpenCenterPanel_m3B17EA932B723968AF8D059929481E06D4E75CDE,
	ProgramUI__ctor_mA3FC7E2F15D77646EB4FA4203DFB30F7B97F4E5C,
	UV_Start_mEB248CE8486D833BE8E5AB435A075941DFB9C60E,
	UV_Open_m98271C3143D56393B4899DF9C492F9D6FB84B0BC,
	UV_ClickClose_m086B25CBD6AB6FC1BC0EE85F9C280250D910B8D4,
	UV_Close_m6098F7CDD4584AF1536E15D49453F9DDDBB8D6CA,
	UV_OpenCallBack_m1C7DD6259DD0918459955CDB204F76564F352A4E,
	UV_CloseCallBack_m4E02286EEA8FE5D270EB0A686B6CE266D9727800,
	NULL,
	NULL,
	UV__ctor_mA1AC7EF3AE52D4D5FC4FD42F3B785B757DFF0413,
	UV_U3CStartU3Eb__3_0_mD1AE53B2DA7CAAD27C216DCD91C3BCB0B121B85D,
	UVPanel_add_EditUV_m87ACD1040A8EAFCE0561D164276C7657896F145D,
	UVPanel_remove_EditUV_m1F8CD365E32F47094F9F1E00E8BB22C9E2912A2E,
	UVPanel_get_IsActive_mF298A653781B5B2009EB060D71EE5A41EB561C75,
	UVPanel_set_IsActive_m257C20DA61922AA7F7B5CAFD94DEC2F178478008,
	UVPanel_Open_mA452877254FF476D6AA0734706B8E329ED6A8665,
	UVPanel_Close_m81001491C18C6AC8738D583DDB50B266199B275F,
	UVPanel_GetMousePostion_m5498B16E3463C92479BB3C3175ECC42F0AD3E12B,
	UVPanel__ctor_m602C4EBC1E31AF2896B226E96682BD9A851699D2,
	BackEditor_Start_m4B1BB26C214F6DD841B619BD4EE8F0000721E8E8,
	BackEditor_Back_mF6994D570364043AA1D0D2F0493C7280325B5711,
	BackEditor__ctor_m7FD70A655DC2FEBE8200F475B811C8D83FA98F00,
	BackEditor_U3CStartU3Eb__1_0_m6C7249A519A003BADB6F90A05660C1D4DAEA74DB,
	ControllerPlayer_get_Direct_m058EE79D5CDB56B7B316B20456071E6433346142,
	ControllerPlayer_set_Direct_m07006D227548EB9A006A564398F8DAFEE2F0BBBF,
	ControllerPlayer_get_DirectCamera_m78941F29180F06DD81DF483D3824BC54AF3A8F36,
	ControllerPlayer_Update_mEB4DB409B5927F6761EE860499ED81CF816C78A6,
	ControllerPlayer_LateUpdate_mEB4D18C9284C5A96C695BCB74874015D1A5B45D3,
	ControllerPlayer__ctor_m8FD95AC3D4817B057FB7B69A3B43B0FCB6D933AC,
	LoadRender_Start_mDF8D9FF20FED2DF2197ADFBE0E6FE2D6243E0CD3,
	LoadRender_Load_m37F4B3EA7A1C6F3FD7DC627AE9AC3D42C402948F,
	LoadRender__ctor_mAEBFB18D3E962E7A54A4BD4751A5F76AD49A2C95,
	MovePlayer_Update_m2DA0EE5DF4AAF62A75CBE8D9BE75666C8C0EF261,
	MovePlayer__ctor_m72E137B7C60E9C5F6ACD3C403BEF1CC642D1411D,
	SetTexture_Start_m0CF376F4B713A1D2760EBA9AF85890DB9B37876E,
	SetTexture__ctor_m4754436BCD80A778C2B3A286EFB96863B396E09C,
	FileOption__ctor_m7D31F9ADB4D02A9087B2A186DA72CE311ACB9E65,
	FilersOption_SaveFile_m030F38EB0E8035A4A4CF3EC845301BAD7A27A348,
	FilersOption_LoadFile_m5886507A3D064BB20F9308F480D849B104D373CA,
	OptionController_Start_m042CAF612D2FA8A36CFDB26F1C42A112AF9E5741,
	OptionController_OnDestroy_mEA4CEBE1936E7A53DB7C1BC7F02F755BB89506F7,
	OptionController__ctor_m077A9D7B2544502CE4D17CAA2E7132CB1E0AC45F,
	NULL,
	NULL,
	OptionObject__ctor_mCEBD64F06C7252A5D729768B741FB0CA0DBDFD5A,
	FileNews_LoadFile_m50097B95C2E832E5A8C4163BAFE65ACD2D209ECA,
	FileNews_SaveFile_mE8E4BE1DB3393602F760444FF40CE1D49D3F7C8D,
	FileNews__ctor_mB331F9DD9BB3FAB7E4AA62586D9E00ABD51E699A,
	FileNews__cctor_m1FE6FC05ED6F7C2AE3607A744D720AD9A4FA4CA5,
	NewsServer_Start_m5A55EA8BB183B715BECD9CFA9132988A9C12C61A,
	NewsServer_IsCurrentUpdateAsync_m8A66DC1D9CB89A49828C1EDABDFD2502F44B0500,
	NewsServer_LoadNewVersionNewsAsync_m4B3BE1FE9596130587701DE59AFEF5439B8C061E,
	NewsServer_LoadNewsAsync_mC4724037BC08197BFE98970F1A3C54910679DDDA,
	NewsServer_IsLoadAsync_m5A2626648A46C68F3A26CA1A51CC0C6DA1A65ED9,
	NewsServer_EndLoad_mFB204A6A123DEAD307F88307A4B69FE4E4B915F9,
	NewsServer_EndLoadNoServer_mB903EA15E16BD48559BAE06ACAB2767150E49FC8,
	NewsServer_SaveCurrentFile_mE0D4921FC0E1622C8E3143C5FF9F93D7F36A6F81,
	NewsServer_ClearVariable_mB9B82FD11998FA890521CEFCB90A80483CBE1037,
	BaseMenu_get_Singleton_m069EC972BD638A8FB525054E1611FB620CA75AA5,
	BaseMenu_set_Singleton_mB1A2F5FFCDCC484C66BD011396C1192066AFBDAD,
	BaseMenu_Awake_m4EFFD9A711BA2D2A9B6AA7D973F7A7087F858315,
	BaseMenu_LoadEditor_mB6D54A1389D8C9489397FAA0E85E65BB159AA571,
	BaseMenu_ExitProgram_m35F3098E90075D44F5141D44A15A2A730C27B7B5,
	BaseMenu_OnDestroy_m0477A32B4622D6EDBDF5D2B4229D940B35135E50,
	BaseMenu__ctor_m794261E5A19DA54209313F3859F349F236A7A571,
	CreateNews__ctor_m24BC96A27BA667759A6056F6CEC12E9B0EADDB1C,
	ItemNews_Init_mE17235437DA055C46CBEDFD59FF33D6ABDB70792,
	ItemNews__ctor_m4A99E78C5EF4EB4EE4B083F3DA7F13AB86D68DA1,
	ListNews_get_Singleton_mA989ACDE2D531B9D55A5BE2D5432EA4AA6356CBA,
	ListNews_set_Singleton_m105CAC52256E242D85A30E5121072DF7CD25DEB8,
	ListNews_Awake_m2D5861C7DC975A44901B76920631A80BBCDB4647,
	ListNews_LoadNewsWait_m8BA4ABE06B74001CABF6FC88B8E7C0E056C0730E,
	ListNews_LoadListNews_m9C0155DFB850A4099B7700B0B635580C28BE8999,
	ListNews_AddNews_m0F745731EE2238E38B387176B27D164263CDA081,
	ListNews_Clear_m27BC2C4AF4A0B176486F12FDC1B43A2FE48C6D26,
	ListNews_OnDestroy_mC08F006DA8E345D66378A4B9B3E7DB294D6515ED,
	ListNews__ctor_mEEE52CBEDC311750AF56A189A79A83FA4B210163,
	UIMenu_Start_m986C1B2404C90980318792C4F6468AA60EC14EFC,
	UIMenu_GetContentNews_m4A817491C5BD8CC3D0F085F7E290D4B343EA3071,
	UIMenu__ctor_m087912FC379032E6163D24C0D55CE0111B0DA779,
	CameraMove_get_IsBlock_m7E4BFC366D0757153132373751C57DF909B456D5,
	CameraMove_OnEnable_mFC5BFBADF9014FE1D4EE9075F1D2066A6680AA18,
	CameraMove_Update_m967D780F50D01795ABA53807AFB7802F72E96674,
	CameraMove_FixedUpdate_m70AA895BAEDAAABDC97C0797C84E0DE1E56CEE48,
	CameraMove_SetDefaultPosition_m0C1516CFF56F5B7F40952F7BF0CDD3DCBA4F38FB,
	CameraMove_OnDisable_m3D1AE48366A74637828C273277DECC35E8E6660D,
	CameraMove__ctor_m0035D2534C340827B1B13EAC719DA2E9499EDAA8,
	EditModel_get_SkinModel_m0DEB8D2D44C308E468FE2CF8ACB7BFBE94705523,
	EditModel_get_ItemModel_mDC2663DB601364F6573921B40419B4E70F2FBDE5,
	EditModel_get_BlockModel_mF902B8F54F4DDC6DDF8F3691AD95E836AB874D19,
	EditModel_get_CurrentModel_m93448B5F4798C5462C4E37A57C117B5472B56A4D,
	EditModel_set_CurrentModel_m24FB61D1B1CD0F63E872BA9F32A2D262AD65E94C,
	EditModel_get_Singleton_mC5F7909A086EF7180B1518C5BF34A15278714A31,
	EditModel_set_Singleton_m976F3966ED7AC897C0321CB41B8E8B5BF7EB6346,
	EditModel_Awake_m80587689A0A00916C9D8EC27DA9AA8139871CB9B,
	EditModel_Start_m9011ACADDA9852AC20FF884E46B9AB630924A90C,
	EditModel_TakeSkin_m186321DD245CCF2F642E4E234291F261CB239640,
	EditModel_TakeItem_mE56916B6FA44A8B92BE3E430F63BB936C093BFB0,
	EditModel_TakeBlock_m12AE2EEBCB693107FD43D7416ADE96CF2ADDDCE8,
	EditModel_OnDestroy_mAC532FDA5BE78EDFA84BB04258C1541E06483304,
	EditModel__ctor_m1E46127B199266915550DA2BD4FD8FE4D1A68BFB,
	GetPixels_GetPixel_mE693DC2122F29F960DC1FC0A9B6788E9A03C3F07,
	GetPixels_GetPixelsFill_m16163DF9BD5606A40871AB0FD280C3C981AADA70,
	GetPixels_GetPixelsFill_m7BA9EAEE55F8DAF5ED8B784F41A6D3FF21DF56E2,
	GetPixels_GetPixelsFromTriangles_mFE39AB1CEAD569288C829D0FA91BC42801043A3C,
	GetPixels_GetConnectTriangle_m2E23BFDE8279853FC9B195EF8C68917FBEE536F3,
	Model_get_IsWriteHistory_m5EDD0BAEB57D2027752B3D5FE6F654A1289B4595,
	Model_set_IsWriteHistory_mC31712A7FC082CABA44BB58C5E52AAE6DA8A5EB0,
	Model_Awake_m8C2022DEEEB8D39FD613E18DD975852CC6771815,
	Model_Start_m297FB3D2C041D4562D2B688D249A3C2DA69CCF10,
	Model_ActiveEditor_mD81789AECF7F040CAD3D966A068D9AB941753B1E,
	Model_GetMeshs_mFBD4E34AD09A7F67A990A82FDDDBB74B3DC2ECE3,
	Model_ActiveEditorCallBack_m1C17913D3F8F3B0425F6B6F65C236B7B9BF7C971,
	Model_DisableEditor_mC01C91F00B817B9D824AD3359CD681E80A3416C9,
	Model_GetUv_mDC40D9D6AB3BEC2AD8A1A8E17FC6212F401ADDB3,
	Model_DisableEditorCallBack_m2469DD124C4CF69A785C289DDE10DBF7A831F7A0,
	Model_IsRotateCamera_m4F0E7851E3851EF197F089B0E57B502E2466AF54,
	Model_GetDefaultRotate_m18D2E29B71E46AFE4302640F97717D07713D8390,
	Model_CreateNewTexture_m9FDB59A1B763BEFADC20B6654588EF133450F375,
	NULL,
	Model_Position_mDBDEC2907537EFD5EA988233CF103863E5F16145,
	NULL,
	Model_LoadTexture_mDF870893C006DB3A544A7B8D18A59E83DE4F6312,
	Model_GetTexture_mB4DD5E611A65E551A10310E4A1B38E76C55CD90A,
	Model_CreateNewHistory_m03D409CC4DF952AD5E61EDE367B2B89584AEB19D,
	Model_NextStory_m363C84A788A42438292385F94EE81F5EAFA663BA,
	Model_BreakStory_m041EC7C2BE318EFF7DEF424E7475DFC9065C2489,
	Model_StartWriteHistory_m2635F8A49CBE57B579FCAEBF332D711702DB8D67,
	Model_StopWriteHistory_mC2DE3BF386053C741CEC0FD5A0102AA5294E3C7B,
	Model_UpdateTexture_m4522135D13368ABC423E022DDDEE91CEEED431E2,
	Model__ctor_m961AC4A634D25C4917C0883170175B0DA97DD813,
	Block_ScaleFactor_m26C6FCC0E50C205411C688ACA91510121B811080,
	Block_ActiveEditorCallBack_m7A358A22D3522662C36F1411B441E3FB58190977,
	Block_IsRotateCamera_mAE5CCE061E1F59B7BF4C334B77258D98B5D767C1,
	Block_GetDefaultRotate_mD838461484D447CC344C4A7D7EF3ECCBA0FCE5BF,
	Block_DisableEditorCallBack_m236998E868E039417FB5E46A99C59020AEBB42A1,
	Block_GetSizeTexture_m6AED075AEAD4D994A70D5C016C80CDFADE70F497,
	Block__ctor_m795C5CE2AA74DDFCC8DC9397CCA0DA9D00304524,
	Item_ScaleFactor_m46EEEA0C8BA0A5D51A67ADAACE393CFAF4F2CDA8,
	Item_ActiveEditorCallBack_m8808988D224D3C22082FD08EC64EDDB75BBE316B,
	Item_DisableEditorCallBack_mFC1A7D5A7B2E1ED2AAD0A87EF79F612B106B8E7D,
	Item_GetDefaultRotate_mC275DF63C87C713953DA9B31CEF6B56A07E3ACC5,
	Item_IsRotateCamera_m474A2A1CA0A3C775705FB4C3CA4892C3B5800BCB,
	Item_GetSizeTexture_mB4DB5C4D62169D5AFDA415D51464AA646EA249DF,
	Item__ctor_m21E529B698479FA292D17DE31D8FED00AB602369,
	Skin_ActiveEditorCallBack_m936983B7AB059ABC49DA2EC4DCFA36A683B922DA,
	Skin_DisableEditorCallBack_m5A44C73E20E7A8D9521E4403E9D4254D4FA5DB4B,
	Skin_GetDefaultRotate_m537DC450E595F62330A5C4637400060C24951F52,
	Skin_IsRotateCamera_mA830FA6AC9A168702458D7025CEB39B0B3F0B5FC,
	Skin_UpdateMesh_m930A18055EBD2BE3EDB2660C333B545AEAC371C3,
	Skin_SetActiveDown_m87F87FE391CC4BB5C31B6435A7A82236494C2AAE,
	Skin_SetActiveUp_m6FF2A0733723399D26DD1BC29C1D85FAF6CE43AE,
	Skin_Position_m30DDC9F4C2827A0274C1779FCC519106BC747D51,
	Skin_GetSizeTexture_m1A61B25C5C15E36C587FDA7D90A4759B37533AF5,
	Skin_ScaleFactor_m2D64C8131BD7AA896A32E2B83D1A08AB1C677627,
	Skin__ctor_m13B87750A5544935CA2996EE0F3D77D548EC29F1,
	UVBlock_GetPositionMouse_mF353A3F44CE0BFA1531A6AC57089B77FD5A56E3E,
	UVBlock_SetTexture_m82263E26DA8F3EED156C9154EA2765213C963AF8,
	UVBlock__ctor_mB14755C1025E71EB096D08E3A3613DEBF07ECED3,
	UVSkin_GetPositionMouse_m3D3D63DF6249D7352F251AB3D169AC4D1BF693DF,
	UVSkin_SetTexture_m19BF7D63A11ED4C55732D88B0DE97F70D433EE86,
	UVSkin__ctor_m54EFD03ACCF929D9E223D97D5BC32BB526736BEF,
	Paint_Start_mC67AC9EAACC64BB5439DFAAB0018FA197CBE2991,
	Paint_LateUpdate_mF866A57CC8636722A01BD661DCD9472146BAC7B5,
	Paint_TasselPaint_mD0F4D2C8B1D21B0BE578EFB9F7353B8D11A95CC4,
	Paint_FillPaint_mE2994AF4EE0AD3A3478B0A7B584BBEC50636CC72,
	Paint_TakeColor_mC1B92541383CBE819E0B83C48E31A08F80A72CC6,
	Paint_Erase_m1AB3E52C0C4A38DB4A204A5E68D464CD0DBE7F08,
	Paint__ctor_m7BDD827DBCBB58E636BAFB413205F0B5F1334B20,
	Erase_SetParams_mE16F584F3EAD9EFF5E01D5DD8E5685AC9AE7E833,
	Erase_Work_mC7542AB7CFF499F5AAF6D9E0709A82990BEEE873,
	Erase_WorkUV_m8EC7E89E081772C22E346E718D49EC08635358E2,
	Erase__ctor_mF7F6344AA5EE0970AD390D65CBF577076131484A,
	Fill_SetParams_mA9E75627EC627D9BECBF15429395C1513EF6FE12,
	Fill_Work_m17BF5423B6125CD96F542BEC2F9AFC0AA9E05D09,
	Fill_WorkUV_mB2CC381888C1FB58DE2BA7ACEBF753BD83554ED4,
	Fill__ctor_m3099C5D9EB450183CAE083CD9A4844B184129768,
	PaintTassel_SetParams_m8DBF5A4D51983677ED7C53428B776D37A478D644,
	PaintTassel_Work_mEA27015CCEFF2B3B04AC0E41E456CE1E5DE156CB,
	PaintTassel_WorkUV_mCF33E00AE1555CF222A7BEFE6A01C8C2CC43850C,
	PaintTassel__ctor_m3D0A42EFBFD101E754064BECE4A7DFFF84A58A01,
	TakeColor_SetParams_m71065478F6AF0BF7DF1E4AD9D3E8E0715F96ACEE,
	TakeColor_Work_mBE0FAD7AAD90DE99502E84BEC9A707CB0DC334EE,
	TakeColor_WorkUV_m995ACDAAD4613D176ED728BC3D015A26FF344D00,
	TakeColor__ctor_m9BEA17B68F94ED3AAE8DC3185C360FFD915B4E2D,
	Tassel_get_TypeInstrument_m918CF89558E2EB0FCB856E8C3F13580C493DEDD2,
	Tassel_set_TypeInstrument_m398D1C548B8B6AFDB3D582B5497CE0E26B635AE4,
	Tassel_Start_m77AC2BC4D93E8DC6D08D7AA6606E223FCEB3BC2A,
	Tassel_SetTypeInstrument_mD033E56C6DFD9F65C87C5731CE192DD883BE1952,
	Tassel_SetIsSaveHistory_mCA8FB271C09AB7B75BAA0CDB5DF82FA7E5425A42,
	NULL,
	Tassel_Update_m45D5763CCCF73A122A672CCC367CE5BA3F0C34E3,
	NULL,
	NULL,
	Tassel__ctor_m4BDDE6A875274FA542F45A3F6277D15957A00694,
	FileTextures_LoadFile_m010BDC70B0674C5E1605F723ADE075E46E5E8D74,
	FileTextures_SaveFile_m898BAA84090E1E97174D210192C8FFCD9707E2C8,
	FileTextures__ctor_mEBACD7C851911AAC21A2C2C06A6F3617671A24F2,
	FileTextures__cctor_mA3E342EBD576A5A681E97B9E6302B954B117145B,
	TexturesServer_Start_mF17ABA42248EE6E7782E1CCD55123CE67C767FCB,
	TexturesServer_IsCurrentUpdateAsync_mD3230A87794A008595C3A0F0C23CA41328FFAB2A,
	TexturesServer_LoadNewVersionTextureAsync_mE64431D473D2D7D207E44F84C225F61B3FF30649,
	TexturesServer_LoadTextureAsync_mCB240B511CC7EF4A38BB15FADFBCCDC3766E41E3,
	TexturesServer_IsLoadAsync_mFEF1394954DB0D5AA9BC364940E628B19CF2E158,
	TexturesServer_EndLoad_m49F57B900308AF3DAC0ED9BCA35C012DDA8030A7,
	TexturesServer_EndLoadNoServer_mE418ABC6F57D2B1CC87D9F470E5594BAA6ED893D,
	TexturesServer_SaveCurrentFile_m6ED026056AAA606513828662C42F38E530C4BC48,
	TexturesServer_GenerateUnityTexture_mFB79B1E2B18B242F87BAD7FC8B4924B6A7576B6F,
	TexturesServer_ClearVariable_mC788FA2CE14AFC61FE18E75043680C130944D43D,
	AudioController_get_Singleton_m23473DECF654EB8C9EF43BD85282CEFBF4023594,
	AudioController_set_Singleton_m68D0A477505FE586F9AA25086DEA44F96AF3A6A9,
	AudioController_Awake_mAA4F19D0D6BD7B1095D315B59BD70F2E969F1AF2,
	AudioController_PlayOnClickButton_mB766132F4DA6AFD200BC1C832A64AC10B06DA0A4,
	AudioController_PlayOnClickColorButton_m2B6A898AB5F8DFF20BF8747D0C9DCD22A8C72DCC,
	AudioController_PlayPaint_mE753C0F7E2884E85F62A58EFF501599904F059F8,
	AudioController_OffPaint_mC95C34C18515DD2A2AB9870072A4425AEB5C4EB2,
	AudioController_OnDestroy_m04852E91DB5900DF3886D0627FB6351181761B94,
	AudioController__ctor_mE676F1CCDD16C94E5DE855AEB418212FA7556995,
	ButtonClick_Start_mDD7B4BD98FAD6D3F3B0456F1876C8C3F28583A7D,
	ButtonClick__ctor_mE15DE3184E732487F71092E0529E762A317C6056,
	ButtonClickColor_Start_mDB44CCF14679E799AD883C65D970D04840C1D737,
	ButtonClickColor__ctor_m4A9ECC24E5A4C99C3439F9B38F99BF51E02AEEB8,
	SetVolume_Start_m2846B497841F1B406EC2CB006480019F647DF4A0,
	SetVolume_Edit_m75F26D0CC88774C365A9AF2D65071B32207ADEB0,
	SetVolume_OnDestroy_mFB536A0B9846942C6873ABD51152C35846A75524,
	SetVolume__ctor_m398B5582FE254E12B0B2D37F3AD06971F14FBE51,
	VolumeAudio_add_Editor_m129CC5D58B5CB0F17A2CB47BAFA30D576BA84364,
	VolumeAudio_remove_Editor_mC2A7A7BFE820002D2ED7E321813E21CFDA4DEECA,
	VolumeAudio_Start_mCE3B80951A6DBAE1147A2F9D78FF0113AC9F9885,
	VolumeAudio_SetParams_m0CB17BA9640BD8F074AE9B693A22A77F7ACFC6E9,
	VolumeAudio_GetOption_mCA37FCDF325C9053696502E836629D90F5BF22A0,
	VolumeAudio_SetOption_mCEEAD27F2D7A9AF578F41F49880132FDE655D59C,
	VolumeAudio__ctor_m678EB538EC35326D94802FE1128B1AEBC6D8E3C9,
	VolumeAudio__cctor_m6F4BE78D061E0775FF37D2F33C01DDA8FDB63307,
	VolumeAudio_U3CStartU3Eb__5_0_mF07DE3ABA2B6761464B1DBEF8B4543BBB229E6D9,
	Pixel__ctor_mC565487D5E165D777AC9E446E62C4A01C3844BE3,
	BaseProcces_get_isEnd_m8F6AA9B2BB0E3D584BA0CB277ED21E65DD002067,
	BaseProcces_set_isEnd_mB7801225FC730DD85B549BA70DDA25A1CECBAF04,
	BaseProcces_Init_m6A9D3FB045080842FBB72839A5371973092FD1FF,
	BaseProcces_Start_m19956E45012AEF19F4054499881CDB51F047CDD5,
	BaseProcces_Update_mBC2C04C98E90C05C6C9347EDC4F03F9C64C1E5BD,
	BaseProcces_CallUpdate_m1AC15AA8E1DA171F18003C180673F27ABC6AEB72,
	BaseProcces_Connect_mCD8ED8C8738CE882039E305BE5F30B0503F5DF45,
	BaseProcces_End_m1CAF78A6E42FCE331CA98540417F50F507CD356B,
	BaseProcces_CallEnd_m80AD04944941EC21BE886CB85AA358860B7AD1AC,
	BaseProcces__ctor_m61F338957AFD830C5F4E6C51CF2F0901838ACC00,
	BaseProcces_U3CUpdateU3Eb__8_0_mAD6652D6C6523D25840710F6A2E3C0C01767203F,
	U3CU3Ec__DisplayClass12_0__ctor_m458EF191E5043A702A3C52755EF31A108D932636,
	U3CU3Ec__DisplayClass12_0_U3CLoadListNewsU3Eb__1_m0F19F288E5F00A4CD4303DCE2956E7EE61030768,
	U3CU3Ec__cctor_m849E92F209610545EB3B04B9F9DB97DD78A98B21,
	U3CU3Ec__ctor_m1126AF5772142BFAABC4BD9827DC04BDA6A910C5,
	U3CU3Ec_U3CLoadListNewsU3Eb__12_0_m733029865DBE1A872E6C2CB65091C83805C7626F,
	U3CU3Ec_U3CLoadListNameTextureU3Eb__13_0_m24FB035746901BD7EBAC3D875ED204C49A8D7A4A,
	U3CU3Ec_U3CLoadListTextureU3Eb__14_0_mA4331803C669853941BB36956E5C7943D30985C2,
	U3CU3Ec_U3CLoadNumUpdateNewsU3Eb__15_0_m43BCAD8DF847B86917CF77EE72BB78EEBCC23848,
	U3CU3Ec_U3CLoadNumUpdateTextureU3Eb__16_0_mC1B7A50C363A4AD1AE146BA005E2CA632CC8C545,
	U3CU3Ec__DisplayClass13_0__ctor_mC68A492B995F2A800972B44B3D56D4D637069036,
	U3CU3Ec__DisplayClass13_0_U3CLoadListNameTextureU3Eb__1_mA4A10BE42B584B61042F67CEBD4C35E6D81E2828,
	U3CU3Ec__DisplayClass14_0__ctor_m5C39B57A55779572A8376B4F9438AE999F76F803,
	U3CU3Ec__DisplayClass14_0_U3CLoadListTextureU3Eb__1_mFB4D19B2A6B3EE7F7DE41C08C2250D1FF2481340,
	U3CU3Ec__DisplayClass15_0__ctor_m0509A99C0AD409C7F3513AD4D1933D24CEB008AA,
	U3CU3Ec__DisplayClass15_0_U3CLoadNumUpdateNewsU3Eb__1_m26B53DD2114EC93CAE27CA520380302436768D3B,
	U3CU3Ec__DisplayClass16_0__ctor_m8A97FBA3810EE76ED61F7386DAFB39EAE25F7BC4,
	U3CU3Ec__DisplayClass16_0_U3CLoadNumUpdateTextureU3Eb__1_m42B7856416670B50B519134CEAA5361B4800F5CE,
	U3CU3Ec__cctor_mFB447CA74623BFB853B20192A3BB156A896F178A,
	U3CU3Ec__ctor_m219117CD33D8037CB10012C35FCCFAF38B21B619,
	U3CU3Ec_U3CAwakeU3Eb__35_0_m58535A70F71FED669D637CEEC787F64F987C0A8F,
	U3CU3Ec_U3CAwakeU3Eb__35_1_m6210BB263252546A9880CB7CA24F68334C7F9ED8,
	U3CU3Ec_U3CSetOptionBodyPartsU3Eb__36_0_mA4EBB0EB599C1DDE16716FECE0272D2371F85425,
	U3CU3Ec__cctor_mD2BC887CE7A79175439D133D7586980F56D4479A,
	U3CU3Ec__ctor_m6E994BF29BC2D19215C5E3B73A23592ABCBC1463,
	U3CU3Ec_U3CStartU3Eb__4_0_mB0D4DB18A8A65187B21EDE9D57E3C5F046855AAE,
	U3CLoadTextureWaitU3Ed__11__ctor_mA8EFBFFC7538B986E47910341B21AAFC99C1D578,
	U3CLoadTextureWaitU3Ed__11_System_IDisposable_Dispose_m32278EABA09B57F45C467C7AC661D922A044A128,
	U3CLoadTextureWaitU3Ed__11_MoveNext_mA8B2805F31A2027DACB264C59861C31FF622618C,
	U3CLoadTextureWaitU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30AD20615A56147C89D07E4E825A3D1BCA280C41,
	U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_Reset_m1F745D6ACC0C15F9630C12F79F928F663ADC18B7,
	U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_get_Current_mC8A4904545EADE0AB3EEE97FED4AFE8FECF905A9,
	U3CU3Ec__cctor_m96FB4CD47DAA0FF961F65D090EA7FDDAE16F78E4,
	U3CU3Ec__ctor_mD73860545FF53EC49CA9184FCAD0FE2FFF78DEA0,
	U3CU3Ec_U3CStartU3Eb__1_0_mD72F8ED180F42D31D96869E7F8B707EB1343F546,
	U3CU3Ec__DisplayClass2_0__ctor_m9D47667D7B30A51BEC5E15A03C1792D622CB7FDB,
	U3CU3Ec__DisplayClass2_0_U3CInitU3Eb__0_m8CD7ED6D1C0E69BAA6263927FD31EC9E3F2D4ABA,
	U3CU3Ec__cctor_m493236BB568A424AD3F78541DC4A613CFBB56A9D,
	U3CU3Ec__ctor_m7E18D07EDEB57DA53149F9156B637B1C2A1EDD0B,
	U3CU3Ec_U3COpenCallBackU3Eb__19_0_m646AC0C43508290335E088AAFCFB92BD6DBA8770,
	U3CU3Ec_U3CUpdateListU3Eb__24_0_m60AFB0C0476B0A68B3866EBC613EA173687FA62F,
	U3CU3Ec__DisplayClass2_0__ctor_mA560F3F96A665FBA0BC86AC587B274466E6760FF,
	U3CU3Ec__DisplayClass2_0_U3CInitU3Eb__0_mE57306A6603FECA78567E73E3081B6B51FBA89AE,
	U3CU3Ec__cctor_m86622DB45A148F39C2F7FA7FFD5621382DB60BCF,
	U3CU3Ec__ctor_mF7D1199BFF2C780D70E9D73848C8D33ADFDA4F72,
	U3CU3Ec_U3CStartU3Eb__39_0_m70562F083C46F792C0472CB5D1679B6C75A69F2E,
	U3CU3Ec_U3CStartU3Eb__39_1_mF52496C719D1807D07433C2CCE00C1F67145154F,
	U3CU3Ec_U3CStartU3Eb__39_2_m490C9C997BDCC3AB3927EB28E491C4BB6BB1565E,
	U3CU3Ec_U3CStartU3Eb__39_3_mF141EB1A932A2FBBD567E52EB7222A0486E95518,
	U3CU3Ec_U3CStartU3Eb__39_4_mCB1E9C476F4ADF5714F7A25C2FFDF35B4A90005A,
	U3CU3Ec_U3CStartU3Eb__39_5_m46A52B92D10C4136C07278213C21B8D22D463A32,
	CorrectCanvance_SetScale_mA56FF70FB43A05EC9853C4F876EE8D34779434DF,
	U3CU3Ec__cctor_mF7D5A7EBF23935B269753913300E79FE8CACC96D,
	U3CU3Ec__ctor_m50BC0DBD2EC29FE4E39FD44C84748386CC5260E9,
	U3CU3Ec_U3COnDestroyU3Eb__3_0_mF4334CBCDBCE27553F3885D1509075FE41E78823,
	U3CLoadNewsWaitU3Ed__8__ctor_m954E460A8D2E865D49227C50AD953ADB62BD3751,
	U3CLoadNewsWaitU3Ed__8_System_IDisposable_Dispose_mD506D633D37BB4EDC8E584479ED3C7D4D05326C2,
	U3CLoadNewsWaitU3Ed__8_MoveNext_mB01976F71C96F54106C92D71253B8EBF10A86D5A,
	U3CLoadNewsWaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEA5FD9FFF67794B08B86C64DE1B8AA4D629E6CD,
	U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_Reset_m53159E79EBA8B80BAC673EDE60518A4F30D17A98,
	U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_get_Current_m3172BAD8411C52CFB7BBDC7E0C0F447315DFB4C5,
	U3CU3Ec__cctor_m9DDBB15F18EE8E10C6D13FE76ED8E43EC5F4732A,
	U3CU3Ec__ctor_mC295447BB5C8F7D4EE7E0278E80DF0EC99E1264D,
	U3CU3Ec_U3CStartU3Eb__3_0_m093F84DB5C5B1B508B2554397AC744290518253F,
	U3CU3Ec_U3CStartU3Eb__3_1_mC0EBF3283D624C5905EE1BB460955B71E640231E,
	Triangle_GetConnect_mB0722685A89A197549E71DDEDA17DC7AD119E4DD,
	Triangle__ctor_m86F2DE51B2CBE08E6040FA345E0176C69342B2ED,
	History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE,
	History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2,
	History__ctor_mE60B21C617580D10000829815C4762950E4A5A28,
	History_NextStory_mD6D584E8C7743EB904F5CF263121ED1BCE876C02,
	History_BreakSory_mEEAE1220F52A2841F4A37D0BCF265CE79854C2BB,
	History_AddSotry_m7B3C4346D3E540DBB4E3D8F9CFCF4BC8D2DFDAB7,
	History_ClearStory_m31DAD42847EB04B4B3DC76CBB4CD574D652B0845,
	History_ClearStory_m74055B06CDC0551ACF9A841D7F110EAED6B2DC50,
	Story__ctor_mED39062554A90BF1D8888C0729EBCABF22F6BD56,
	U3CU3Ec__cctor_m709C23D26AAB88E6BC592F1D6C56C0974B94275C,
	U3CU3Ec__ctor_m8AF44807C9189D0DDBEB1F38D11927CD90DC7247,
	U3CU3Ec_U3CGetMeshsU3Eb__14_0_m076B9A41934A3E5FC350998E7ABF554A90322A33,
	U3CU3Ec__cctor_m2F762773421FD81FA0A5B495455234128082FC7E,
	U3CU3Ec__ctor_m40813A2B2E6D775355D3CDFFC065FAD5F1C8F95B,
	U3CU3Ec_U3CStartU3Eb__1_0_m213571C10571A5A8A8319FB88AF9A64F6B4635B3,
	U3CU3Ec__cctor_mA4257105044085DBF05F0275D69C7AD4DCEB2241,
	U3CU3Ec__ctor_mB4922A770A757AC0778C8BA26D0811911F196C4E,
	U3CU3Ec_U3CStartU3Eb__1_0_m362E47FAACAFA08B0FB3B2C66EB4926C7A49472F,
	U3CUpdateU3Ed__8_MoveNext_mDDFCC9C7E5C00438CA37A40742B4CF2EF430368A_AdjustorThunk,
	U3CUpdateU3Ed__8_SetStateMachine_m7CB0536971DEF64B4EDF5E17A72BC6F20ACC8D6B_AdjustorThunk,
};
static const int32_t s_InvokerIndices[673] = 
{
	2248,
	2445,
	2458,
	1476,
	788,
	1241,
	382,
	1241,
	1440,
	1241,
	1440,
	1241,
	1241,
	1241,
	789,
	1476,
	1476,
	2176,
	2176,
	1476,
	1476,
	1440,
	1241,
	1476,
	1476,
	1476,
	1476,
	2248,
	2248,
	2248,
	2248,
	2248,
	2421,
	2458,
	1476,
	2458,
	788,
	1241,
	382,
	1241,
	788,
	1241,
	382,
	1241,
	788,
	1231,
	373,
	1241,
	1428,
	1231,
	1440,
	1241,
	1459,
	1257,
	1476,
	1476,
	1476,
	1459,
	1257,
	1428,
	1231,
	1476,
	1476,
	1476,
	1459,
	1257,
	1428,
	1231,
	1476,
	1476,
	1476,
	1428,
	1231,
	1440,
	1241,
	1459,
	1257,
	1476,
	1476,
	1476,
	1428,
	1231,
	1440,
	1241,
	1459,
	1257,
	1476,
	1476,
	1476,
	2421,
	2445,
	1476,
	1476,
	2445,
	2421,
	1476,
	1476,
	1476,
	1476,
	2445,
	2421,
	2445,
	2421,
	2445,
	2421,
	2445,
	2421,
	2445,
	2421,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1241,
	1241,
	1459,
	1257,
	1459,
	1257,
	1459,
	1257,
	1459,
	1257,
	1459,
	1257,
	1459,
	1257,
	1476,
	1476,
	1241,
	1241,
	1459,
	1257,
	1476,
	1241,
	1241,
	1476,
	1476,
	1459,
	1257,
	1476,
	1476,
	1476,
	1476,
	1476,
	2421,
	2421,
	1476,
	1476,
	1476,
	1440,
	1476,
	1476,
	1241,
	1476,
	1241,
	1476,
	1476,
	1476,
	1476,
	1241,
	1476,
	1476,
	1241,
	1476,
	1241,
	1476,
	1476,
	1476,
	1476,
	1476,
	1241,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1241,
	1476,
	1476,
	1476,
	1476,
	1476,
	2421,
	2421,
	2452,
	2423,
	1476,
	1476,
	1440,
	1241,
	1476,
	1476,
	1476,
	1476,
	789,
	1476,
	1476,
	1241,
	1241,
	1241,
	1241,
	1241,
	1241,
	1241,
	1241,
	1241,
	1241,
	1241,
	1241,
	1241,
	1241,
	2445,
	2421,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1241,
	1410,
	1210,
	1476,
	1241,
	1241,
	1241,
	1440,
	1210,
	1241,
	1440,
	1241,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1459,
	1476,
	1476,
	1410,
	1210,
	1476,
	1476,
	1476,
	1261,
	1241,
	1241,
	1241,
	1241,
	1476,
	1476,
	1476,
	1428,
	1231,
	1241,
	1241,
	1476,
	1476,
	1476,
	1476,
	1476,
	788,
	1241,
	382,
	1241,
	1241,
	1241,
	2445,
	2421,
	1476,
	1476,
	1476,
	1476,
	1241,
	1241,
	1241,
	1476,
	1241,
	1440,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	487,
	1476,
	2445,
	2421,
	2445,
	2421,
	2445,
	2421,
	2445,
	2421,
	1476,
	1476,
	1476,
	1476,
	1241,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1241,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1471,
	1241,
	1476,
	1476,
	1241,
	1241,
	1459,
	1257,
	1476,
	1476,
	1471,
	1476,
	1476,
	1476,
	1476,
	1476,
	1473,
	1270,
	1440,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	2421,
	2445,
	1476,
	1476,
	1476,
	1440,
	1241,
	1476,
	2445,
	2421,
	1476,
	2458,
	2458,
	2419,
	2421,
	2421,
	2419,
	2458,
	2458,
	2458,
	2458,
	2445,
	2421,
	1476,
	1476,
	1476,
	1476,
	1476,
	1241,
	789,
	1476,
	2445,
	2421,
	1476,
	1440,
	1476,
	1241,
	1476,
	1476,
	1476,
	1476,
	1440,
	1476,
	1459,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1440,
	1440,
	1440,
	1440,
	1241,
	2445,
	2421,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1976,
	1765,
	1928,
	1755,
	1739,
	1459,
	1257,
	1476,
	1476,
	1476,
	1440,
	1476,
	1476,
	1440,
	1476,
	1459,
	1473,
	1476,
	1471,
	1473,
	1463,
	1241,
	1440,
	1476,
	1476,
	1476,
	1476,
	1476,
	789,
	1476,
	1463,
	1476,
	1459,
	1473,
	1476,
	1471,
	1476,
	1463,
	1476,
	1476,
	1473,
	1459,
	1471,
	1476,
	1476,
	1476,
	1473,
	1459,
	1476,
	1257,
	1257,
	1473,
	1471,
	1463,
	1476,
	1471,
	1241,
	1476,
	1471,
	1241,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1251,
	1268,
	1476,
	1476,
	1251,
	1268,
	1476,
	1476,
	1251,
	1268,
	1476,
	1476,
	1251,
	1268,
	1476,
	1428,
	1231,
	1476,
	1231,
	1257,
	1476,
	1476,
	1251,
	1268,
	1476,
	2445,
	2421,
	1476,
	2458,
	2458,
	2419,
	2421,
	2421,
	2419,
	2458,
	2458,
	2458,
	2458,
	2458,
	2445,
	2421,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	2421,
	2421,
	1476,
	1476,
	1440,
	1241,
	1476,
	2458,
	1261,
	1476,
	1459,
	1257,
	789,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	2458,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	2458,
	1476,
	1476,
	1476,
	1476,
	2458,
	1476,
	1476,
	1231,
	1476,
	1459,
	1440,
	1476,
	1440,
	2458,
	1476,
	1476,
	1476,
	1476,
	2458,
	1476,
	1000,
	1000,
	1476,
	1476,
	2458,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	2253,
	2458,
	1476,
	1000,
	1231,
	1476,
	1459,
	1440,
	1476,
	1440,
	2458,
	1476,
	1476,
	1476,
	1440,
	1476,
	1428,
	1231,
	1476,
	1241,
	1241,
	1241,
	1476,
	1231,
	1476,
	2458,
	1476,
	1000,
	2458,
	1476,
	1476,
	2458,
	1476,
	1476,
	1476,
	1241,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	673,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
