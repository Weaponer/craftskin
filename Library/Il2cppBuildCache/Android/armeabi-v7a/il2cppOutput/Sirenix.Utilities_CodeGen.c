﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Color Sirenix.Utilities.ColorExtensions::Lerp(UnityEngine.Color[],System.Single)
extern void ColorExtensions_Lerp_m9889C6B728D39820E6B193DBFF1E3F7B094167C4 (void);
// 0x00000002 UnityEngine.Color Sirenix.Utilities.ColorExtensions::MoveTowards(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void ColorExtensions_MoveTowards_mA4FC1B1A0D4E73C9582A603442CF90F303688987 (void);
// 0x00000003 System.Boolean Sirenix.Utilities.ColorExtensions::TryParseString(System.String,UnityEngine.Color&)
extern void ColorExtensions_TryParseString_mEF98BBAA6B1B6083DBF029FB450B3974127E8C30 (void);
// 0x00000004 System.String Sirenix.Utilities.ColorExtensions::ToCSharpColor(UnityEngine.Color)
extern void ColorExtensions_ToCSharpColor_mAB3E86EFCCD979A2A9EDD3A7CC909003D12F16DB (void);
// 0x00000005 UnityEngine.Color Sirenix.Utilities.ColorExtensions::Pow(UnityEngine.Color,System.Single)
extern void ColorExtensions_Pow_mAA9F009DF4579636872558B8C413716B5A699F1C (void);
// 0x00000006 UnityEngine.Color Sirenix.Utilities.ColorExtensions::NormalizeRGB(UnityEngine.Color)
extern void ColorExtensions_NormalizeRGB_mA7D50C50232CDB4E393640BC60C4AE1CE71DC2B1 (void);
// 0x00000007 System.String Sirenix.Utilities.ColorExtensions::TrimFloat(System.Single)
extern void ColorExtensions_TrimFloat_mBA88D820423EF634F4F2902B8DE1D225DDD6EDF7 (void);
// 0x00000008 System.Void Sirenix.Utilities.ColorExtensions::.cctor()
extern void ColorExtensions__cctor_mFD96C17056331628FF551669C4DF46DBBDEE9A65 (void);
// 0x00000009 System.Func`1<TResult> Sirenix.Utilities.DelegateExtensions::Memoize(System.Func`1<TResult>)
// 0x0000000A System.Func`2<T,TResult> Sirenix.Utilities.DelegateExtensions::Memoize(System.Func`2<T,TResult>)
// 0x0000000B System.Void Sirenix.Utilities.DelegateExtensions/<>c__DisplayClass0_0`1::.ctor()
// 0x0000000C TResult Sirenix.Utilities.DelegateExtensions/<>c__DisplayClass0_0`1::<Memoize>b__0()
// 0x0000000D System.Void Sirenix.Utilities.DelegateExtensions/<>c__DisplayClass1_0`2::.ctor()
// 0x0000000E TResult Sirenix.Utilities.DelegateExtensions/<>c__DisplayClass1_0`2::<Memoize>b__0(T)
// 0x0000000F System.Boolean Sirenix.Utilities.FieldInfoExtensions::IsAliasField(System.Reflection.FieldInfo)
extern void FieldInfoExtensions_IsAliasField_m6845C750241C884522A12B3DA22744346CCA3E46 (void);
// 0x00000010 System.Reflection.FieldInfo Sirenix.Utilities.FieldInfoExtensions::DeAliasField(System.Reflection.FieldInfo,System.Boolean)
extern void FieldInfoExtensions_DeAliasField_m0E168E8E55F57E758C73B014393C579F393AB179 (void);
// 0x00000011 Sirenix.Utilities.GarbageFreeIterators/ListIterator`1<T> Sirenix.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.List`1<T>)
// 0x00000012 Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2<T1,T2> Sirenix.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x00000013 Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2<T1,T2> Sirenix.Utilities.GarbageFreeIterators::GFValueIterator(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x00000014 Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1<T> Sirenix.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.HashSet`1<T>)
// 0x00000015 System.Void Sirenix.Utilities.GarbageFreeIterators/ListIterator`1::.ctor(System.Collections.Generic.List`1<T>)
// 0x00000016 Sirenix.Utilities.GarbageFreeIterators/ListIterator`1<T> Sirenix.Utilities.GarbageFreeIterators/ListIterator`1::GetEnumerator()
// 0x00000017 T Sirenix.Utilities.GarbageFreeIterators/ListIterator`1::get_Current()
// 0x00000018 System.Boolean Sirenix.Utilities.GarbageFreeIterators/ListIterator`1::MoveNext()
// 0x00000019 System.Void Sirenix.Utilities.GarbageFreeIterators/ListIterator`1::Dispose()
// 0x0000001A System.Void Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000001B Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1<T> Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1::GetEnumerator()
// 0x0000001C T Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1::get_Current()
// 0x0000001D System.Boolean Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1::MoveNext()
// 0x0000001E System.Void Sirenix.Utilities.GarbageFreeIterators/HashsetIterator`1::Dispose()
// 0x0000001F System.Void Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2::.ctor(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x00000020 Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2<T1,T2> Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2::GetEnumerator()
// 0x00000021 System.Collections.Generic.KeyValuePair`2<T1,T2> Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2::get_Current()
// 0x00000022 System.Boolean Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2::MoveNext()
// 0x00000023 System.Void Sirenix.Utilities.GarbageFreeIterators/DictionaryIterator`2::Dispose()
// 0x00000024 System.Void Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::.ctor(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x00000025 Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2<T1,T2> Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::GetEnumerator()
// 0x00000026 T2 Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::get_Current()
// 0x00000027 System.Boolean Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::MoveNext()
// 0x00000028 System.Void Sirenix.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::Dispose()
// 0x00000029 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::Examine(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x0000002A System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::ForEach(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x0000002B System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::ForEach(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
// 0x0000002C System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::Convert(System.Collections.IEnumerable,System.Func`2<System.Object,T>)
// 0x0000002D System.Collections.Generic.HashSet`1<T> Sirenix.Utilities.LinqExtensions::ToHashSet(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000002E System.Collections.Generic.HashSet`1<T> Sirenix.Utilities.LinqExtensions::ToHashSet(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000002F Sirenix.Utilities.ImmutableList`1<T> Sirenix.Utilities.LinqExtensions::ToImmutableList(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000030 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::Prepend(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<T>)
// 0x00000031 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::Prepend(System.Collections.Generic.IEnumerable`1<T>,T)
// 0x00000032 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::Prepend(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000033 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,System.Func`1<T>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,T)
// 0x00000035 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,System.Func`1<T>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,T)
// 0x00000038 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<System.Collections.Generic.IEnumerable`1<T>,System.Boolean>,System.Func`1<T>)
// 0x0000003A System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<System.Collections.Generic.IEnumerable`1<T>,System.Boolean>,T)
// 0x0000003B System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::PrependIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<System.Collections.Generic.IEnumerable`1<T>,System.Boolean>,System.Collections.Generic.IEnumerable`1<T>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendWith(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<T>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendWith(System.Collections.Generic.IEnumerable`1<T>,T)
// 0x0000003E System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendWith(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x0000003F System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,System.Func`1<T>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,T)
// 0x00000041 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Boolean,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,System.Func`1<T>)
// 0x00000043 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,T)
// 0x00000044 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::AppendIf(System.Collections.Generic.IEnumerable`1<T>,System.Func`1<System.Boolean>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000045 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.LinqExtensions::FilterCast(System.Collections.IEnumerable)
// 0x00000046 System.Void Sirenix.Utilities.LinqExtensions::AddRange(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000047 System.Boolean Sirenix.Utilities.LinqExtensions::IsNullOrEmpty(System.Collections.Generic.IList`1<T>)
// 0x00000048 System.Void Sirenix.Utilities.LinqExtensions::Populate(System.Collections.Generic.IList`1<T>,T)
// 0x00000049 System.Void Sirenix.Utilities.LinqExtensions::AddRange(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x0000004A System.Void Sirenix.Utilities.LinqExtensions::Sort(System.Collections.Generic.IList`1<T>,System.Comparison`1<T>)
// 0x0000004B System.Void Sirenix.Utilities.LinqExtensions::Sort(System.Collections.Generic.IList`1<T>)
// 0x0000004C System.Void Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::.ctor(System.Int32)
// 0x0000004D System.Void Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.IDisposable.Dispose()
// 0x0000004E System.Boolean Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::MoveNext()
// 0x0000004F System.Void Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::<>m__Finally1()
// 0x00000050 T Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000051 System.Void Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.Collections.IEnumerator.Reset()
// 0x00000052 System.Object Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.Collections.IEnumerator.get_Current()
// 0x00000053 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000054 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<Examine>d__0`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000055 System.Void Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::.ctor(System.Int32)
// 0x00000056 System.Void Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.IDisposable.Dispose()
// 0x00000057 System.Boolean Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::MoveNext()
// 0x00000058 System.Void Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::<>m__Finally1()
// 0x00000059 T Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000005A System.Void Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.Collections.IEnumerator.Reset()
// 0x0000005B System.Object Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.Collections.IEnumerator.get_Current()
// 0x0000005C System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000005D System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<Convert>d__3`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005E System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__7`1::.ctor(System.Int32)
// 0x0000005F System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__7`1::System.IDisposable.Dispose()
// 0x00000060 System.Boolean Sirenix.Utilities.LinqExtensions/<Prepend>d__7`1::MoveNext()
// 0x00000061 System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__7`1::<>m__Finally1()
// 0x00000062 T Sirenix.Utilities.LinqExtensions/<Prepend>d__7`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000063 System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__7`1::System.Collections.IEnumerator.Reset()
// 0x00000064 System.Object Sirenix.Utilities.LinqExtensions/<Prepend>d__7`1::System.Collections.IEnumerator.get_Current()
// 0x00000065 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<Prepend>d__7`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000066 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<Prepend>d__7`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000067 System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__8`1::.ctor(System.Int32)
// 0x00000068 System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__8`1::System.IDisposable.Dispose()
// 0x00000069 System.Boolean Sirenix.Utilities.LinqExtensions/<Prepend>d__8`1::MoveNext()
// 0x0000006A System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__8`1::<>m__Finally1()
// 0x0000006B T Sirenix.Utilities.LinqExtensions/<Prepend>d__8`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000006C System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__8`1::System.Collections.IEnumerator.Reset()
// 0x0000006D System.Object Sirenix.Utilities.LinqExtensions/<Prepend>d__8`1::System.Collections.IEnumerator.get_Current()
// 0x0000006E System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<Prepend>d__8`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000006F System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<Prepend>d__8`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000070 System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__9`1::.ctor(System.Int32)
// 0x00000071 System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__9`1::System.IDisposable.Dispose()
// 0x00000072 System.Boolean Sirenix.Utilities.LinqExtensions/<Prepend>d__9`1::MoveNext()
// 0x00000073 System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__9`1::<>m__Finally1()
// 0x00000074 System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__9`1::<>m__Finally2()
// 0x00000075 T Sirenix.Utilities.LinqExtensions/<Prepend>d__9`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000076 System.Void Sirenix.Utilities.LinqExtensions/<Prepend>d__9`1::System.Collections.IEnumerator.Reset()
// 0x00000077 System.Object Sirenix.Utilities.LinqExtensions/<Prepend>d__9`1::System.Collections.IEnumerator.get_Current()
// 0x00000078 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<Prepend>d__9`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000079 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<Prepend>d__9`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007A System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::.ctor(System.Int32)
// 0x0000007B System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.IDisposable.Dispose()
// 0x0000007C System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::MoveNext()
// 0x0000007D System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::<>m__Finally1()
// 0x0000007E T Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000007F System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.Collections.IEnumerator.Reset()
// 0x00000080 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.Collections.IEnumerator.get_Current()
// 0x00000081 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000082 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__10`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000083 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::.ctor(System.Int32)
// 0x00000084 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.IDisposable.Dispose()
// 0x00000085 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::MoveNext()
// 0x00000086 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::<>m__Finally1()
// 0x00000087 T Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000088 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.Collections.IEnumerator.Reset()
// 0x00000089 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.Collections.IEnumerator.get_Current()
// 0x0000008A System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000008B System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__11`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000008C System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::.ctor(System.Int32)
// 0x0000008D System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.IDisposable.Dispose()
// 0x0000008E System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::MoveNext()
// 0x0000008F System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::<>m__Finally1()
// 0x00000090 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::<>m__Finally2()
// 0x00000091 T Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000092 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.Collections.IEnumerator.Reset()
// 0x00000093 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.Collections.IEnumerator.get_Current()
// 0x00000094 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000095 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__12`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000096 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::.ctor(System.Int32)
// 0x00000097 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.IDisposable.Dispose()
// 0x00000098 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::MoveNext()
// 0x00000099 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::<>m__Finally1()
// 0x0000009A T Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000009B System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.Collections.IEnumerator.Reset()
// 0x0000009C System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.Collections.IEnumerator.get_Current()
// 0x0000009D System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000009E System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__13`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009F System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::.ctor(System.Int32)
// 0x000000A0 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.IDisposable.Dispose()
// 0x000000A1 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::MoveNext()
// 0x000000A2 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::<>m__Finally1()
// 0x000000A3 T Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000A4 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.Collections.IEnumerator.Reset()
// 0x000000A5 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.Collections.IEnumerator.get_Current()
// 0x000000A6 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000A7 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__14`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A8 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::.ctor(System.Int32)
// 0x000000A9 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.IDisposable.Dispose()
// 0x000000AA System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::MoveNext()
// 0x000000AB System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::<>m__Finally1()
// 0x000000AC System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::<>m__Finally2()
// 0x000000AD T Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000AE System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.Collections.IEnumerator.Reset()
// 0x000000AF System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.Collections.IEnumerator.get_Current()
// 0x000000B0 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000B1 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__15`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B2 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::.ctor(System.Int32)
// 0x000000B3 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.IDisposable.Dispose()
// 0x000000B4 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::MoveNext()
// 0x000000B5 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::<>m__Finally1()
// 0x000000B6 T Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000B7 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.Collections.IEnumerator.Reset()
// 0x000000B8 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.Collections.IEnumerator.get_Current()
// 0x000000B9 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000BA System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__16`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BB System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::.ctor(System.Int32)
// 0x000000BC System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.IDisposable.Dispose()
// 0x000000BD System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::MoveNext()
// 0x000000BE System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::<>m__Finally1()
// 0x000000BF T Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000C0 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.Collections.IEnumerator.Reset()
// 0x000000C1 System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.Collections.IEnumerator.get_Current()
// 0x000000C2 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000C3 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__17`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C4 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::.ctor(System.Int32)
// 0x000000C5 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.IDisposable.Dispose()
// 0x000000C6 System.Boolean Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::MoveNext()
// 0x000000C7 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::<>m__Finally1()
// 0x000000C8 System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::<>m__Finally2()
// 0x000000C9 T Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000CA System.Void Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.Collections.IEnumerator.Reset()
// 0x000000CB System.Object Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.Collections.IEnumerator.get_Current()
// 0x000000CC System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000CD System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<PrependIf>d__18`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000CE System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::.ctor(System.Int32)
// 0x000000CF System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.IDisposable.Dispose()
// 0x000000D0 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::MoveNext()
// 0x000000D1 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::<>m__Finally1()
// 0x000000D2 T Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000D3 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.Collections.IEnumerator.Reset()
// 0x000000D4 System.Object Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.Collections.IEnumerator.get_Current()
// 0x000000D5 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000D6 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendWith>d__19`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D7 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::.ctor(System.Int32)
// 0x000000D8 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.IDisposable.Dispose()
// 0x000000D9 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::MoveNext()
// 0x000000DA System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::<>m__Finally1()
// 0x000000DB T Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000DC System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.Collections.IEnumerator.Reset()
// 0x000000DD System.Object Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.Collections.IEnumerator.get_Current()
// 0x000000DE System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000DF System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendWith>d__20`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000E0 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::.ctor(System.Int32)
// 0x000000E1 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.IDisposable.Dispose()
// 0x000000E2 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::MoveNext()
// 0x000000E3 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::<>m__Finally1()
// 0x000000E4 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::<>m__Finally2()
// 0x000000E5 T Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000E6 System.Void Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.Collections.IEnumerator.Reset()
// 0x000000E7 System.Object Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.Collections.IEnumerator.get_Current()
// 0x000000E8 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000E9 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendWith>d__21`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000EA System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::.ctor(System.Int32)
// 0x000000EB System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.IDisposable.Dispose()
// 0x000000EC System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::MoveNext()
// 0x000000ED System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::<>m__Finally1()
// 0x000000EE T Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000EF System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.Collections.IEnumerator.Reset()
// 0x000000F0 System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.Collections.IEnumerator.get_Current()
// 0x000000F1 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000F2 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__22`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000F3 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::.ctor(System.Int32)
// 0x000000F4 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.IDisposable.Dispose()
// 0x000000F5 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::MoveNext()
// 0x000000F6 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::<>m__Finally1()
// 0x000000F7 T Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000F8 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.Collections.IEnumerator.Reset()
// 0x000000F9 System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.Collections.IEnumerator.get_Current()
// 0x000000FA System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000FB System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__23`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000FC System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::.ctor(System.Int32)
// 0x000000FD System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.IDisposable.Dispose()
// 0x000000FE System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::MoveNext()
// 0x000000FF System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::<>m__Finally1()
// 0x00000100 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::<>m__Finally2()
// 0x00000101 T Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000102 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.Collections.IEnumerator.Reset()
// 0x00000103 System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.Collections.IEnumerator.get_Current()
// 0x00000104 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000105 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__24`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000106 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::.ctor(System.Int32)
// 0x00000107 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.IDisposable.Dispose()
// 0x00000108 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::MoveNext()
// 0x00000109 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::<>m__Finally1()
// 0x0000010A T Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000010B System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.Collections.IEnumerator.Reset()
// 0x0000010C System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x0000010D System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000010E System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000010F System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::.ctor(System.Int32)
// 0x00000110 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.IDisposable.Dispose()
// 0x00000111 System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::MoveNext()
// 0x00000112 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::<>m__Finally1()
// 0x00000113 T Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000114 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.Collections.IEnumerator.Reset()
// 0x00000115 System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.Collections.IEnumerator.get_Current()
// 0x00000116 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000117 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__26`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000118 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::.ctor(System.Int32)
// 0x00000119 System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.IDisposable.Dispose()
// 0x0000011A System.Boolean Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::MoveNext()
// 0x0000011B System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::<>m__Finally1()
// 0x0000011C System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::<>m__Finally2()
// 0x0000011D T Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000011E System.Void Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.Collections.IEnumerator.Reset()
// 0x0000011F System.Object Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.Collections.IEnumerator.get_Current()
// 0x00000120 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000121 System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<AppendIf>d__27`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000122 System.Void Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::.ctor(System.Int32)
// 0x00000123 System.Void Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.IDisposable.Dispose()
// 0x00000124 System.Boolean Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::MoveNext()
// 0x00000125 System.Void Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::<>m__Finally1()
// 0x00000126 T Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000127 System.Void Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.Collections.IEnumerator.Reset()
// 0x00000128 System.Object Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.Collections.IEnumerator.get_Current()
// 0x00000129 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000012A System.Collections.IEnumerator Sirenix.Utilities.LinqExtensions/<FilterCast>d__28`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000012B System.Boolean Sirenix.Utilities.MemberInfoExtensions::IsDefined(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x0000012C System.Boolean Sirenix.Utilities.MemberInfoExtensions::IsDefined(System.Reflection.ICustomAttributeProvider)
// 0x0000012D T Sirenix.Utilities.MemberInfoExtensions::GetAttribute(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x0000012E T Sirenix.Utilities.MemberInfoExtensions::GetAttribute(System.Reflection.ICustomAttributeProvider)
// 0x0000012F System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider)
// 0x00000130 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x00000131 System.Attribute[] Sirenix.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider)
extern void MemberInfoExtensions_GetAttributes_m15AAE26BE285B269CAE126FB926279E0A826E987 (void);
// 0x00000132 System.Attribute[] Sirenix.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
extern void MemberInfoExtensions_GetAttributes_m2FFD9512045ED25E441863CA105281FB2C5E0B9F (void);
// 0x00000133 System.String Sirenix.Utilities.MemberInfoExtensions::GetNiceName(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_GetNiceName_m233C26DA9440A5BD00027679176E7A1207811170 (void);
// 0x00000134 System.Boolean Sirenix.Utilities.MemberInfoExtensions::IsStatic(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_IsStatic_m0B82DFA6FA5B65EF48F2D3B2799A342F48353F7E (void);
// 0x00000135 System.Boolean Sirenix.Utilities.MemberInfoExtensions::IsAlias(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_IsAlias_mA690365F38B68B1AB5041F68D0A6995FD6DADD57 (void);
// 0x00000136 System.Reflection.MemberInfo Sirenix.Utilities.MemberInfoExtensions::DeAlias(System.Reflection.MemberInfo,System.Boolean)
extern void MemberInfoExtensions_DeAlias_m6C0863F27B3C8362850BF8B960A7AE376D425D68 (void);
// 0x00000137 System.Boolean Sirenix.Utilities.MemberInfoExtensions::SignaturesAreEqual(System.Reflection.MemberInfo,System.Reflection.MemberInfo)
extern void MemberInfoExtensions_SignaturesAreEqual_mFFAA6FB976110B9E1B5CFB81EC23FFA528363A26 (void);
// 0x00000138 System.String Sirenix.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase,System.String)
extern void MethodInfoExtensions_GetFullName_m2B1D1CF37601A3B322FEA19473EEAB83BE9322C8 (void);
// 0x00000139 System.String Sirenix.Utilities.MethodInfoExtensions::GetParamsNames(System.Reflection.MethodBase)
extern void MethodInfoExtensions_GetParamsNames_mE7B9162DDC988D41D65DD34E54A7C29839C38F58 (void);
// 0x0000013A System.String Sirenix.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase)
extern void MethodInfoExtensions_GetFullName_m5324B40A2D0B23F9F4989566B50AB7EE99F88A47 (void);
// 0x0000013B System.Boolean Sirenix.Utilities.MethodInfoExtensions::IsExtensionMethod(System.Reflection.MethodBase)
extern void MethodInfoExtensions_IsExtensionMethod_m7796506EB32D9061808BF565AEDC681C66EDDBED (void);
// 0x0000013C System.Boolean Sirenix.Utilities.MethodInfoExtensions::IsAliasMethod(System.Reflection.MethodInfo)
extern void MethodInfoExtensions_IsAliasMethod_mB15831FE197849EA54CA5E0826EE7EE29275A01E (void);
// 0x0000013D System.Reflection.MethodInfo Sirenix.Utilities.MethodInfoExtensions::DeAliasMethod(System.Reflection.MethodInfo,System.Boolean)
extern void MethodInfoExtensions_DeAliasMethod_mF8824D0D8741B82891672059C62A31876EF1C900 (void);
// 0x0000013E System.String Sirenix.Utilities.PathUtilities::GetDirectoryName(System.String)
extern void PathUtilities_GetDirectoryName_m4D874AE3875B253A3B44DEA2763CC2D260901890 (void);
// 0x0000013F System.Boolean Sirenix.Utilities.PathUtilities::HasSubDirectory(System.IO.DirectoryInfo,System.IO.DirectoryInfo)
extern void PathUtilities_HasSubDirectory_mD034F326CD0D7AF99E6F0EFE93550D684CB0A652 (void);
// 0x00000140 System.IO.DirectoryInfo Sirenix.Utilities.PathUtilities::FindParentDirectoryWithName(System.IO.DirectoryInfo,System.String)
extern void PathUtilities_FindParentDirectoryWithName_mE67A19F3CB8AA227659DDD7C5F7958C36E1378A3 (void);
// 0x00000141 System.Boolean Sirenix.Utilities.PathUtilities::CanMakeRelative(System.String,System.String)
extern void PathUtilities_CanMakeRelative_m45418D5B6BDD5E886719A845076E2C70DA3A60F5 (void);
// 0x00000142 System.String Sirenix.Utilities.PathUtilities::MakeRelative(System.String,System.String)
extern void PathUtilities_MakeRelative_mAF0FD824193DACF09321255C6E76E89554327178 (void);
// 0x00000143 System.Boolean Sirenix.Utilities.PathUtilities::TryMakeRelative(System.String,System.String,System.String&)
extern void PathUtilities_TryMakeRelative_m0A7838E666713CBEAAB3691E0162FDFFF4CA824E (void);
// 0x00000144 System.String Sirenix.Utilities.PathUtilities::Combine(System.String,System.String)
extern void PathUtilities_Combine_m45D096DDDF2FD232BD42283DE55F968A2B699D8B (void);
// 0x00000145 System.Boolean Sirenix.Utilities.PropertyInfoExtensions::IsAutoProperty(System.Reflection.PropertyInfo)
extern void PropertyInfoExtensions_IsAutoProperty_m17D6F1D04D64078B40A4DFC8DB2678E0C88B4863 (void);
// 0x00000146 System.Boolean Sirenix.Utilities.PropertyInfoExtensions::IsAliasProperty(System.Reflection.PropertyInfo)
extern void PropertyInfoExtensions_IsAliasProperty_m26C4A48FCE446591C97B7BF2BE9F4716559AB645 (void);
// 0x00000147 System.Reflection.PropertyInfo Sirenix.Utilities.PropertyInfoExtensions::DeAliasProperty(System.Reflection.PropertyInfo,System.Boolean)
extern void PropertyInfoExtensions_DeAliasProperty_m5420C600E36D84EB08C57C666C3080A09C191CED (void);
// 0x00000148 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetWidth(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetWidth_mC853B7063708E8C35014ACD1210681C80A1BCB47 (void);
// 0x00000149 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetHeight(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetHeight_mF6D4E394172A8BF3335A949463B4D7031E0BD5BB (void);
// 0x0000014A UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetSize(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_SetSize_mD39716901BD09A67A29FF813F535B0415F09DD3D (void);
// 0x0000014B UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetSize(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SetSize_mA7B1826F31962FB667C60F5291929BD2D779411F (void);
// 0x0000014C UnityEngine.Rect Sirenix.Utilities.RectExtensions::HorizontalPadding(UnityEngine.Rect,System.Single)
extern void RectExtensions_HorizontalPadding_m8E8E3A615990471E3586A9BB20DDB018DB909EB0 (void);
// 0x0000014D UnityEngine.Rect Sirenix.Utilities.RectExtensions::HorizontalPadding(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_HorizontalPadding_m89B3727FB917838AF9D05AFF14D389517798BDA0 (void);
// 0x0000014E UnityEngine.Rect Sirenix.Utilities.RectExtensions::VerticalPadding(UnityEngine.Rect,System.Single)
extern void RectExtensions_VerticalPadding_m5C4CCF230E193C556128BF8481F1FBBC167D67AF (void);
// 0x0000014F UnityEngine.Rect Sirenix.Utilities.RectExtensions::VerticalPadding(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_VerticalPadding_m590B82183DB97091EDA9E0C994F54C7D64613C05 (void);
// 0x00000150 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Padding(UnityEngine.Rect,System.Single)
extern void RectExtensions_Padding_mFB407178B4A570B3E8AD035E7C25C1E1F5431F55 (void);
// 0x00000151 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Padding(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_Padding_mDA5649C8E3559871C74BA5D72FA1B690F7107486 (void);
// 0x00000152 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Padding(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single)
extern void RectExtensions_Padding_m32E39A56FBB4E5707FB0B008A1E0E59CAB187C79 (void);
// 0x00000153 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignLeft(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignLeft_m614D187B42334A09858A7F7D393538FE7FEB0889 (void);
// 0x00000154 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenter(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignCenter_m3E956AC4B8A25F9474FCA34A34E53F84D45CEEB2 (void);
// 0x00000155 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenter(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_AlignCenter_m9B01538E9E37E34EE70A05A6F776BFE9B8DE6184 (void);
// 0x00000156 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignRight(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignRight_m574DBF16789091EAA5DBA75D1D3763EF34C046BA (void);
// 0x00000157 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignRight(UnityEngine.Rect,System.Single,System.Boolean)
extern void RectExtensions_AlignRight_m05BE19656017535D2F9E004CB946C36C94FB98F9 (void);
// 0x00000158 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignTop(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignTop_m6D8F9DE46371856A5B061230B35C6571CE5A617C (void);
// 0x00000159 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignMiddle(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignMiddle_mC5E340F841C52962FCBA450A79F86B76C5AE2886 (void);
// 0x0000015A UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignBottom(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignBottom_mBEACA9FC17D0F71F94305F734A21582880214A9B (void);
// 0x0000015B UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenterX(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignCenterX_m69C778A0DABE4398737D79D19860AA1FCF428037 (void);
// 0x0000015C UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenterY(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignCenterY_m7641F9F0AA5704BD40BDDA8B2096C4252686FB08 (void);
// 0x0000015D UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenterXY(UnityEngine.Rect,System.Single)
extern void RectExtensions_AlignCenterXY_mF1D830A1A24F00608669BA7ACE783E0C407FE6A4 (void);
// 0x0000015E UnityEngine.Rect Sirenix.Utilities.RectExtensions::AlignCenterXY(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_AlignCenterXY_m3F714CE8FECD8AAD7E1D6CDF276850C7E5FF95C8 (void);
// 0x0000015F UnityEngine.Rect Sirenix.Utilities.RectExtensions::Expand(UnityEngine.Rect,System.Single)
extern void RectExtensions_Expand_mABE02EAE258191B576ECFADFABDE0DFFF8B9177C (void);
// 0x00000160 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Expand(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_Expand_m81781A1F595C5F5CA55814EE85555D0B2A912233 (void);
// 0x00000161 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Expand(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single)
extern void RectExtensions_Expand_m72D5AC60DDE7B7C771FDFD9663F927DC7D91C08B (void);
// 0x00000162 UnityEngine.Rect Sirenix.Utilities.RectExtensions::Split(UnityEngine.Rect,System.Int32,System.Int32)
extern void RectExtensions_Split_m97F4A1E73113DB65F4C04D542747DF3E92DB10A8 (void);
// 0x00000163 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SplitVertical(UnityEngine.Rect,System.Int32,System.Int32)
extern void RectExtensions_SplitVertical_m939E05A9013EE2CD669EC050750E468957F50BB0 (void);
// 0x00000164 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SplitGrid(UnityEngine.Rect,System.Single,System.Single,System.Int32)
extern void RectExtensions_SplitGrid_m6916D7314A9130D72E3DFBE3D9DC81A69218E90C (void);
// 0x00000165 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SplitTableGrid(UnityEngine.Rect,System.Int32,System.Single,System.Int32)
extern void RectExtensions_SplitTableGrid_mEE23BBAEBD316E42C0731267C95488D9381BDD5A (void);
// 0x00000166 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetCenterX(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetCenterX_m8654D5E6F3D90DFAB618CDE1ABFC4BFB34349A5D (void);
// 0x00000167 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetCenterY(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetCenterY_mD217F8DB58C73B90DF3D0519F3E21387898B0E33 (void);
// 0x00000168 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetCenter(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_SetCenter_m5D32A84E8B2B0A5AB1190ECD3D2362104E12836E (void);
// 0x00000169 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetCenter(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SetCenter_m9B56A386F413A3ABCD25D840E2E1990D1CEAB2AD (void);
// 0x0000016A UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetPosition(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SetPosition_m35513C3D84420597C326DB9F71B1E53ADE6B86F7 (void);
// 0x0000016B UnityEngine.Rect Sirenix.Utilities.RectExtensions::ResetPosition(UnityEngine.Rect)
extern void RectExtensions_ResetPosition_mB818EF964F8D1E78385E0D479D7286DD1A53C140 (void);
// 0x0000016C UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddPosition(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_AddPosition_m1D21D830B6D383D35D05666220FEF082A13A9AFD (void);
// 0x0000016D UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddPosition(UnityEngine.Rect,System.Single,System.Single)
extern void RectExtensions_AddPosition_m029ADEE6AAC59E615BB356040108B633E2E4F60F (void);
// 0x0000016E UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetX(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetX_m00477B241A08B949553D77C69BBCA5A322F22A35 (void);
// 0x0000016F UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddX(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddX_mAC5D747A732603B30BE1BA06C3BB209FB542B266 (void);
// 0x00000170 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubX(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubX_m0AC8AB5C0B438C5D03CD0E51895858EC0B890718 (void);
// 0x00000171 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetY(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetY_m08E7C979CC840E57214E979DF30C08F5ADD24B02 (void);
// 0x00000172 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddY(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddY_mC0A509D0023FC36DF2AAA3702E463850B73C0415 (void);
// 0x00000173 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubY(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubY_m0776E2852F6DACCBA11D7472D7D1AA76F51C6057 (void);
// 0x00000174 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetMin(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SetMin_m68BC6C0D21F9F0558B957515260B08CFE44832B5 (void);
// 0x00000175 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddMin(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_AddMin_m2860A1F53A2FF827A6EAAC51570CB198DA547564 (void);
// 0x00000176 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubMin(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SubMin_mE28419D690ADD2516463FA44BCB7C86EAA04346B (void);
// 0x00000177 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetMax(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SetMax_m855BDDE7EAEC02DB394244E15E5F54A045FAE316 (void);
// 0x00000178 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddMax(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_AddMax_mD7B96050B949AFA58E950D9EEF27E722B013ADA4 (void);
// 0x00000179 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubMax(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_SubMax_m8B41E31D6702D6E3F91DED8E2AD8D707EA1B882C (void);
// 0x0000017A UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetXMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetXMin_mEB4BB8CCD0C42D6078F892475907D18190174627 (void);
// 0x0000017B UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddXMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddXMin_mBF12C4963D04484010D0B3039FB59E19ABD3F39F (void);
// 0x0000017C UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubXMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubXMin_m834AF0100D5BF61C3D7FC980C4B85A63D5C04CFE (void);
// 0x0000017D UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetXMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetXMax_m3079E8E87F725074D0C38FB308C7205E0AB03985 (void);
// 0x0000017E UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddXMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddXMax_m00BCE6D7A36B8B9D5CFEC8AAFCE6032917A6DB31 (void);
// 0x0000017F UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubXMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubXMax_m60AF370F697312E645E8F30F0CF99B8BB93E851C (void);
// 0x00000180 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetYMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetYMin_m11A869747613438D095F20A504AA0989F6C0718A (void);
// 0x00000181 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddYMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddYMin_m4124C8B31491D21DF6B930295B712C60AE2E32A6 (void);
// 0x00000182 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubYMin(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubYMin_mBFFAC829D2AD00C17749B310642E403BFFD63CC6 (void);
// 0x00000183 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SetYMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_SetYMax_mC70AF8DCBFB45C4FC72CEE1E8085DF5F5DB7B61B (void);
// 0x00000184 UnityEngine.Rect Sirenix.Utilities.RectExtensions::AddYMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_AddYMax_mA63EBA978081E7C2CED937F2B563AD1F28CBDA2A (void);
// 0x00000185 UnityEngine.Rect Sirenix.Utilities.RectExtensions::SubYMax(UnityEngine.Rect,System.Single)
extern void RectExtensions_SubYMax_m51DB920309335CD0C3A7D629016E7C48418DAFDF (void);
// 0x00000186 UnityEngine.Rect Sirenix.Utilities.RectExtensions::MinWidth(UnityEngine.Rect,System.Single)
extern void RectExtensions_MinWidth_m77CAE2D37F322D97EE565DBD6F33224ABCAD0DBE (void);
// 0x00000187 UnityEngine.Rect Sirenix.Utilities.RectExtensions::MaxWidth(UnityEngine.Rect,System.Single)
extern void RectExtensions_MaxWidth_m6D39F9B0FC8CE7ACFDE83CD74805C15ABFEB1AB6 (void);
// 0x00000188 UnityEngine.Rect Sirenix.Utilities.RectExtensions::MinHeight(UnityEngine.Rect,System.Single)
extern void RectExtensions_MinHeight_m8B4B10F23FBA6A890FE5B8F5B6154FE7BF3AF330 (void);
// 0x00000189 UnityEngine.Rect Sirenix.Utilities.RectExtensions::MaxHeight(UnityEngine.Rect,System.Single)
extern void RectExtensions_MaxHeight_mE30E285546FD1051FCD4B826B0A539447E022252 (void);
// 0x0000018A UnityEngine.Rect Sirenix.Utilities.RectExtensions::ExpandTo(UnityEngine.Rect,UnityEngine.Vector2)
extern void RectExtensions_ExpandTo_mB98847B1A65AF6FEB80FE423BE4F29707004647D (void);
// 0x0000018B System.String Sirenix.Utilities.StringExtensions::ToTitleCase(System.String)
extern void StringExtensions_ToTitleCase_mA8A1408F983DDE057FF2DC283C24A931599C25C0 (void);
// 0x0000018C System.Boolean Sirenix.Utilities.StringExtensions::Contains(System.String,System.String,System.StringComparison)
extern void StringExtensions_Contains_m8AB548F34460F21E93D1F70FF99B15246F4A8D2E (void);
// 0x0000018D System.String Sirenix.Utilities.StringExtensions::SplitPascalCase(System.String)
extern void StringExtensions_SplitPascalCase_m5539F652FCECED016F2AC168F3FB04FD54205163 (void);
// 0x0000018E System.Boolean Sirenix.Utilities.StringExtensions::IsNullOrWhitespace(System.String)
extern void StringExtensions_IsNullOrWhitespace_mEE67BC64AC520011BDC8485A9CF67819CEDC6F2D (void);
// 0x0000018F System.String Sirenix.Utilities.TypeExtensions::GetCachedNiceName(System.Type)
extern void TypeExtensions_GetCachedNiceName_m5117FF5BB8BEE5CECB334268738338EB459CC3BC (void);
// 0x00000190 System.String Sirenix.Utilities.TypeExtensions::CreateNiceName(System.Type)
extern void TypeExtensions_CreateNiceName_m1A225A46571812F45ED50EA207016068A0A8223D (void);
// 0x00000191 System.Boolean Sirenix.Utilities.TypeExtensions::HasCastDefined(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_HasCastDefined_m2119B5754B8D819AA028BCD56C3AAF744B4F4960 (void);
// 0x00000192 System.Boolean Sirenix.Utilities.TypeExtensions::IsValidIdentifier(System.String)
extern void TypeExtensions_IsValidIdentifier_mDE74E73508BEAAEE954948E88D01BEBA5A6FB424 (void);
// 0x00000193 System.Boolean Sirenix.Utilities.TypeExtensions::IsValidIdentifierStartCharacter(System.Char)
extern void TypeExtensions_IsValidIdentifierStartCharacter_m02CD46B4D1CD9661EA0117D032FD260ACE80941B (void);
// 0x00000194 System.Boolean Sirenix.Utilities.TypeExtensions::IsValidIdentifierPartCharacter(System.Char)
extern void TypeExtensions_IsValidIdentifierPartCharacter_m1246D10BE585C5D7C17F8CDF089C2867472ABF78 (void);
// 0x00000195 System.Boolean Sirenix.Utilities.TypeExtensions::IsCastableTo(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_IsCastableTo_mD75B14DD15DD5B1CC04062319CFBC91E22CDC85A (void);
// 0x00000196 System.Func`2<System.Object,System.Object> Sirenix.Utilities.TypeExtensions::GetCastMethodDelegate(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_GetCastMethodDelegate_m2861121E8A7BB7BB3761C01E8FAD5F1FF94D7C51 (void);
// 0x00000197 System.Func`2<TFrom,TTo> Sirenix.Utilities.TypeExtensions::GetCastMethodDelegate(System.Boolean)
// 0x00000198 System.Reflection.MethodInfo Sirenix.Utilities.TypeExtensions::GetCastMethod(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_GetCastMethod_mF07C9F8F7ED2C41152EB2C564246358694A4F587 (void);
// 0x00000199 System.Func`3<T,T,System.Boolean> Sirenix.Utilities.TypeExtensions::GetEqualityComparerDelegate()
// 0x0000019A T Sirenix.Utilities.TypeExtensions::GetAttribute(System.Type,System.Boolean)
// 0x0000019B System.Boolean Sirenix.Utilities.TypeExtensions::ImplementsOrInherits(System.Type,System.Type)
extern void TypeExtensions_ImplementsOrInherits_mE14EC3D52E1F1029666B799F1333A70BB4EDA868 (void);
// 0x0000019C System.Boolean Sirenix.Utilities.TypeExtensions::ImplementsOpenGenericType(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericType_m1121DC0D840999A914E5574F3F0AD4B26CF8796A (void);
// 0x0000019D System.Boolean Sirenix.Utilities.TypeExtensions::ImplementsOpenGenericInterface(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericInterface_m2F341DAEABDED76559A85A8D20B8550D98305795 (void);
// 0x0000019E System.Boolean Sirenix.Utilities.TypeExtensions::ImplementsOpenGenericClass(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericClass_m0A41ECA8B4244F7ED2BE35D1F971CE6B286F4112 (void);
// 0x0000019F System.Type[] Sirenix.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericType(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m0753CF4AB6F4C0C028B12A66C68035E133CA6825 (void);
// 0x000001A0 System.Type[] Sirenix.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericClass(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m916A7D9B2DD86FAB8764EC572D18A3AE44F1E0B7 (void);
// 0x000001A1 System.Type[] Sirenix.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericInterface(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m465C12014C9C4C4ED14FFA0CA5351375A09D2C32 (void);
// 0x000001A2 System.Reflection.MethodInfo Sirenix.Utilities.TypeExtensions::GetOperatorMethod(System.Type,Sirenix.Utilities.Operator)
extern void TypeExtensions_GetOperatorMethod_m7E05D3B4C3D95C62BA8886DCDC060FEC3EF6C152 (void);
// 0x000001A3 System.Reflection.MethodInfo[] Sirenix.Utilities.TypeExtensions::GetOperatorMethods(System.Type,Sirenix.Utilities.Operator)
extern void TypeExtensions_GetOperatorMethods_mB76C9B9565203292047CF82412370E8FD33F959C (void);
// 0x000001A4 System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Utilities.TypeExtensions::GetAllMembers(System.Type,System.Reflection.BindingFlags)
extern void TypeExtensions_GetAllMembers_mD0E5106F949AD8BC0BC4095BCF4C67050CC859A8 (void);
// 0x000001A5 System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Utilities.TypeExtensions::GetAllMembers(System.Type,System.String,System.Reflection.BindingFlags)
extern void TypeExtensions_GetAllMembers_m3562615FFFE9EE27CC2D5D0193EAD3C6FFC8F9E3 (void);
// 0x000001A6 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.TypeExtensions::GetAllMembers(System.Type,System.Reflection.BindingFlags)
// 0x000001A7 System.Type Sirenix.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type)
extern void TypeExtensions_GetGenericBaseType_m94A62E4BA96C8F4FC5A4F465D14B416468B51F91 (void);
// 0x000001A8 System.Type Sirenix.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type,System.Int32&)
extern void TypeExtensions_GetGenericBaseType_m39F7468A0FF37BB99A5A13025FC2523F0DD1CCB3 (void);
// 0x000001A9 System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Utilities.TypeExtensions::GetBaseTypes(System.Type,System.Boolean)
extern void TypeExtensions_GetBaseTypes_mC637233D7ECF7D9C89B70AA1D0C218844C07A964 (void);
// 0x000001AA System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Utilities.TypeExtensions::GetBaseClasses(System.Type,System.Boolean)
extern void TypeExtensions_GetBaseClasses_m62C3445D92A27770261AF5FA50BFCABAAC2D014D (void);
// 0x000001AB System.String Sirenix.Utilities.TypeExtensions::TypeNameGauntlet(System.Type)
extern void TypeExtensions_TypeNameGauntlet_mED6BB694ABAD8B721B26010774C2AF0F36CEE16C (void);
// 0x000001AC System.String Sirenix.Utilities.TypeExtensions::GetNiceName(System.Type)
extern void TypeExtensions_GetNiceName_mDDEEDB5E0EE8C831C50FF8D56DB2383EE063DCDD (void);
// 0x000001AD System.String Sirenix.Utilities.TypeExtensions::GetNiceFullName(System.Type)
extern void TypeExtensions_GetNiceFullName_mA2DD7054D6BB866DAE63AFAE786D93BA6DF76DEC (void);
// 0x000001AE System.String Sirenix.Utilities.TypeExtensions::GetCompilableNiceName(System.Type)
extern void TypeExtensions_GetCompilableNiceName_m339A581D1A09CE239F39F0B892C3AA46DA92574C (void);
// 0x000001AF System.String Sirenix.Utilities.TypeExtensions::GetCompilableNiceFullName(System.Type)
extern void TypeExtensions_GetCompilableNiceFullName_m14D75762F69A7E7753EFDAA2DE4A71F7CF2CA868 (void);
// 0x000001B0 T Sirenix.Utilities.TypeExtensions::GetCustomAttribute(System.Type,System.Boolean)
// 0x000001B1 T Sirenix.Utilities.TypeExtensions::GetCustomAttribute(System.Type)
// 0x000001B2 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.TypeExtensions::GetCustomAttributes(System.Type)
// 0x000001B3 System.Collections.Generic.IEnumerable`1<T> Sirenix.Utilities.TypeExtensions::GetCustomAttributes(System.Type,System.Boolean)
// 0x000001B4 System.Boolean Sirenix.Utilities.TypeExtensions::IsDefined(System.Type)
// 0x000001B5 System.Boolean Sirenix.Utilities.TypeExtensions::IsDefined(System.Type,System.Boolean)
// 0x000001B6 System.Boolean Sirenix.Utilities.TypeExtensions::InheritsFrom(System.Type)
// 0x000001B7 System.Boolean Sirenix.Utilities.TypeExtensions::InheritsFrom(System.Type,System.Type)
extern void TypeExtensions_InheritsFrom_m9CA98952CA2EF39E0DF4BD9E68773E46B6501471 (void);
// 0x000001B8 System.Int32 Sirenix.Utilities.TypeExtensions::GetInheritanceDistance(System.Type,System.Type)
extern void TypeExtensions_GetInheritanceDistance_m2BC5795354997E6D0A208131F755C2629F26431B (void);
// 0x000001B9 System.Boolean Sirenix.Utilities.TypeExtensions::HasParamaters(System.Reflection.MethodInfo,System.Collections.Generic.IList`1<System.Type>,System.Boolean)
extern void TypeExtensions_HasParamaters_m71D6B60C5F128516E7DD8CA2F9FA4C81988E4659 (void);
// 0x000001BA System.Type Sirenix.Utilities.TypeExtensions::GetReturnType(System.Reflection.MemberInfo)
extern void TypeExtensions_GetReturnType_mEFB54C016DF12930DE9D99E351D0A67F4C0A872E (void);
// 0x000001BB System.Object Sirenix.Utilities.TypeExtensions::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern void TypeExtensions_GetMemberValue_m92007B8251F5EFAD94286371D20C634600BD7253 (void);
// 0x000001BC System.Void Sirenix.Utilities.TypeExtensions::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern void TypeExtensions_SetMemberValue_m19EA7B3165678F36D133C6877A405938BCE728ED (void);
// 0x000001BD System.Boolean Sirenix.Utilities.TypeExtensions::TryInferGenericParameters(System.Type,System.Type[]&,System.Type[])
extern void TypeExtensions_TryInferGenericParameters_m8B86E7145860BD05F5BA87566D034CAEF046E46F (void);
// 0x000001BE System.Boolean Sirenix.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type,System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_m5979A23DB5D7BB878F43406689A1D1E222A84182 (void);
// 0x000001BF System.Boolean Sirenix.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Reflection.MethodBase,System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_m68E1A8190F47993BDD581D414D13C509579F4F43 (void);
// 0x000001C0 System.Boolean Sirenix.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type[],System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_mA8571C660CC62428F444FB79A2C61725B0CA201E (void);
// 0x000001C1 System.Boolean Sirenix.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type)
extern void TypeExtensions_GenericParameterIsFulfilledBy_m6F871B1B8B2427ED49D7FD1A4E0BBC13E0125750 (void);
// 0x000001C2 System.Boolean Sirenix.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Type>,System.Collections.Generic.HashSet`1<System.Type>)
extern void TypeExtensions_GenericParameterIsFulfilledBy_mBB6572A82E174FF80D5A9AE454EE8CA6FFCF777F (void);
// 0x000001C3 System.String Sirenix.Utilities.TypeExtensions::GetGenericConstraintsString(System.Type,System.Boolean)
extern void TypeExtensions_GetGenericConstraintsString_m74F940F9A005E758932D975A47318666A68B2215 (void);
// 0x000001C4 System.String Sirenix.Utilities.TypeExtensions::GetGenericParameterConstraintsString(System.Type,System.Boolean)
extern void TypeExtensions_GetGenericParameterConstraintsString_m6C4C94289FAA168F8C83862ED43A80276BD4E1B3 (void);
// 0x000001C5 System.Boolean Sirenix.Utilities.TypeExtensions::GenericArgumentsContainsTypes(System.Type,System.Type[])
extern void TypeExtensions_GenericArgumentsContainsTypes_mB5405BEBB46D9D14834B1FEFD389AAD72052552E (void);
// 0x000001C6 System.Boolean Sirenix.Utilities.TypeExtensions::IsFullyConstructedGenericType(System.Type)
extern void TypeExtensions_IsFullyConstructedGenericType_m941C7E4175FEB639347AD717A85AD04D6A53CE38 (void);
// 0x000001C7 System.Boolean Sirenix.Utilities.TypeExtensions::IsNullableType(System.Type)
extern void TypeExtensions_IsNullableType_m28FB5D9FB77956D303CF4009FE8E4C62D896822B (void);
// 0x000001C8 System.UInt64 Sirenix.Utilities.TypeExtensions::GetEnumBitmask(System.Object,System.Type)
extern void TypeExtensions_GetEnumBitmask_mD9D4FB7B88E81C06793063E7DBC7FD27D127F1EE (void);
// 0x000001C9 System.Boolean Sirenix.Utilities.TypeExtensions::IsCSharpKeyword(System.String)
extern void TypeExtensions_IsCSharpKeyword_m40A516DF6A056550F38E3785C0E7D8254E38DD26 (void);
// 0x000001CA System.Type[] Sirenix.Utilities.TypeExtensions::SafeGetTypes(System.Reflection.Assembly)
extern void TypeExtensions_SafeGetTypes_mB12AC54B4D81C926F95EABC7EABB6622A57C98F8 (void);
// 0x000001CB System.Boolean Sirenix.Utilities.TypeExtensions::SafeIsDefined(System.Reflection.Assembly,System.Type,System.Boolean)
extern void TypeExtensions_SafeIsDefined_m278C59D57D1127B3783333879C5298B6DFDC2F0A (void);
// 0x000001CC System.Object[] Sirenix.Utilities.TypeExtensions::SafeGetCustomAttributes(System.Reflection.Assembly,System.Type,System.Boolean)
extern void TypeExtensions_SafeGetCustomAttributes_m23EC650659FE27E327171260426F92C9CD5EBF8C (void);
// 0x000001CD System.Void Sirenix.Utilities.TypeExtensions::.cctor()
extern void TypeExtensions__cctor_m26D3724DDE253A689F3136C88B81B28A33860F2C (void);
// 0x000001CE System.Void Sirenix.Utilities.TypeExtensions/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_mDF9AE9AAADF55D825811BA0DD1F8D3588FF619A4 (void);
// 0x000001CF System.Object Sirenix.Utilities.TypeExtensions/<>c__DisplayClass22_0::<GetCastMethodDelegate>b__0(System.Object)
extern void U3CU3Ec__DisplayClass22_0_U3CGetCastMethodDelegateU3Eb__0_m21622DFE58C7ABF4C3930CCCAB57EC7DB1DE3503 (void);
// 0x000001D0 System.Void Sirenix.Utilities.TypeExtensions/<>c__25`1::.cctor()
// 0x000001D1 System.Void Sirenix.Utilities.TypeExtensions/<>c__25`1::.ctor()
// 0x000001D2 System.Boolean Sirenix.Utilities.TypeExtensions/<>c__25`1::<GetEqualityComparerDelegate>b__25_0(System.Reflection.MethodInfo)
// 0x000001D3 System.Boolean Sirenix.Utilities.TypeExtensions/<>c__25`1::<GetEqualityComparerDelegate>b__25_1(UnityEngine.Quaternion,UnityEngine.Quaternion)
// 0x000001D4 System.Boolean Sirenix.Utilities.TypeExtensions/<>c__25`1::<GetEqualityComparerDelegate>b__25_2(T,T)
// 0x000001D5 System.Boolean Sirenix.Utilities.TypeExtensions/<>c__25`1::<GetEqualityComparerDelegate>b__25_3(T,T)
// 0x000001D6 System.Void Sirenix.Utilities.TypeExtensions/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m5305362E4FC2ACA20EF345E4D2916A7ACC726DC1 (void);
// 0x000001D7 System.Boolean Sirenix.Utilities.TypeExtensions/<>c__DisplayClass29_0::<ImplementsOpenGenericInterface>b__0(System.Type)
extern void U3CU3Ec__DisplayClass29_0_U3CImplementsOpenGenericInterfaceU3Eb__0_m1D8CE446643405FA6A9979A20CA7BFE19E0A444F (void);
// 0x000001D8 System.Void Sirenix.Utilities.TypeExtensions/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mB285E443BBE7E2E9F04F0731124D8AF23857C22E (void);
// 0x000001D9 System.Boolean Sirenix.Utilities.TypeExtensions/<>c__DisplayClass34_0::<GetOperatorMethod>b__0(System.Reflection.MethodInfo)
extern void U3CU3Ec__DisplayClass34_0_U3CGetOperatorMethodU3Eb__0_m68FE6B0B00C3875D84A4D6F028C8D67BC03AA612 (void);
// 0x000001DA System.Void Sirenix.Utilities.TypeExtensions/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m640B8F0B7744ACC6F6351B1C003DF77F3074675A (void);
// 0x000001DB System.Boolean Sirenix.Utilities.TypeExtensions/<>c__DisplayClass35_0::<GetOperatorMethods>b__0(System.Reflection.MethodInfo)
extern void U3CU3Ec__DisplayClass35_0_U3CGetOperatorMethodsU3Eb__0_m9B7A6AC703BB914AD88D46239BCCEBC866FB9B29 (void);
// 0x000001DC System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::.ctor(System.Int32)
extern void U3CGetAllMembersU3Ed__36__ctor_m93329B6BB738335150EA8633AC9A62064DE2AE5B (void);
// 0x000001DD System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::System.IDisposable.Dispose()
extern void U3CGetAllMembersU3Ed__36_System_IDisposable_Dispose_m696D89A5F3311080407B13BFD1C6131D488FA40F (void);
// 0x000001DE System.Boolean Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::MoveNext()
extern void U3CGetAllMembersU3Ed__36_MoveNext_m4FCF8C5BEA63510C0183FEA7BE58D0327621C63E (void);
// 0x000001DF System.Reflection.MemberInfo Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::System.Collections.Generic.IEnumerator<System.Reflection.MemberInfo>.get_Current()
extern void U3CGetAllMembersU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mC6C31374620C0677C696479727C2B46974130EE4 (void);
// 0x000001E0 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::System.Collections.IEnumerator.Reset()
extern void U3CGetAllMembersU3Ed__36_System_Collections_IEnumerator_Reset_m7A9247214D92CC94632DF7CBC99660DDCDB73CC3 (void);
// 0x000001E1 System.Object Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllMembersU3Ed__36_System_Collections_IEnumerator_get_Current_m33DEDC31C961C26AA43C6CD836286AE5C2EB1809 (void);
// 0x000001E2 System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern void U3CGetAllMembersU3Ed__36_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mC498D8F9D92C86C5CB7CB8EFA91B0A1B4FBF4B2A (void);
// 0x000001E3 System.Collections.IEnumerator Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__36::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllMembersU3Ed__36_System_Collections_IEnumerable_GetEnumerator_mEF0676FE290DE50AA21460A696AC5585D6959DB3 (void);
// 0x000001E4 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::.ctor(System.Int32)
extern void U3CGetAllMembersU3Ed__37__ctor_m5C0B1BB7706836620920BF0378663D60442ACED9 (void);
// 0x000001E5 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::System.IDisposable.Dispose()
extern void U3CGetAllMembersU3Ed__37_System_IDisposable_Dispose_m122701CE24AE448A557DA668F815115D765395F5 (void);
// 0x000001E6 System.Boolean Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::MoveNext()
extern void U3CGetAllMembersU3Ed__37_MoveNext_mB9A482F8A1B5A1E75080E571609CA9A336DAE46C (void);
// 0x000001E7 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::<>m__Finally1()
extern void U3CGetAllMembersU3Ed__37_U3CU3Em__Finally1_m8207B7B15172021158A4B28C61209EB8D2AA2E40 (void);
// 0x000001E8 System.Reflection.MemberInfo Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::System.Collections.Generic.IEnumerator<System.Reflection.MemberInfo>.get_Current()
extern void U3CGetAllMembersU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m033546A216735300A541C93881903001625A7A27 (void);
// 0x000001E9 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::System.Collections.IEnumerator.Reset()
extern void U3CGetAllMembersU3Ed__37_System_Collections_IEnumerator_Reset_mCECE57E55D6393810F72C45E36309904E806F9ED (void);
// 0x000001EA System.Object Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllMembersU3Ed__37_System_Collections_IEnumerator_get_Current_mBDFA5AAE955CF87CFDC232725522A37BFC16CDCE (void);
// 0x000001EB System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern void U3CGetAllMembersU3Ed__37_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mF7674D670646237FF8B332360649656FE5395034 (void);
// 0x000001EC System.Collections.IEnumerator Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__37::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllMembersU3Ed__37_System_Collections_IEnumerable_GetEnumerator_mFE43504BBBC411400F6B50970E44F082B2FF5B17 (void);
// 0x000001ED System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__38`1::.ctor(System.Int32)
// 0x000001EE System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__38`1::System.IDisposable.Dispose()
// 0x000001EF System.Boolean Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__38`1::MoveNext()
// 0x000001F0 T Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__38`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000001F1 System.Void Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__38`1::System.Collections.IEnumerator.Reset()
// 0x000001F2 System.Object Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__38`1::System.Collections.IEnumerator.get_Current()
// 0x000001F3 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__38`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000001F4 System.Collections.IEnumerator Sirenix.Utilities.TypeExtensions/<GetAllMembers>d__38`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000001F5 System.Void Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::.ctor(System.Int32)
extern void U3CGetBaseClassesU3Ed__42__ctor_mD2A7BF94397412C36C8F81C32F7608F4DD5481AB (void);
// 0x000001F6 System.Void Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::System.IDisposable.Dispose()
extern void U3CGetBaseClassesU3Ed__42_System_IDisposable_Dispose_m4BA19A27C774612AA6AAD5AE4A740F3665F113B5 (void);
// 0x000001F7 System.Boolean Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::MoveNext()
extern void U3CGetBaseClassesU3Ed__42_MoveNext_mA92B84163AF60E2B8D9B25E916BF3F3080AE0CDC (void);
// 0x000001F8 System.Type Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern void U3CGetBaseClassesU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m739E1E100840478C780CD1C037EBE5DC5ED381A4 (void);
// 0x000001F9 System.Void Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::System.Collections.IEnumerator.Reset()
extern void U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerator_Reset_m322C061B956B87A6E42700C1BDDC602F29CC2469 (void);
// 0x000001FA System.Object Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerator_get_Current_mEFAE5547493200A0CF71A7EDC76005CC5FF74D16 (void);
// 0x000001FB System.Collections.Generic.IEnumerator`1<System.Type> Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern void U3CGetBaseClassesU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_mE8BF5192BF980759DFDDBE1DA4AD0CF2C5C03DA7 (void);
// 0x000001FC System.Collections.IEnumerator Sirenix.Utilities.TypeExtensions/<GetBaseClasses>d__42::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerable_GetEnumerator_m6578B926D0606CA6B4A8A0FED583288405B3EBAF (void);
// 0x000001FD System.Void Sirenix.Utilities.UnityExtensions::.cctor()
extern void UnityExtensions__cctor_mBBF8D69E0EF646AC4DF9161FE4A98EB10EAD7AD7 (void);
// 0x000001FE System.Boolean Sirenix.Utilities.UnityExtensions::SafeIsUnityNull(UnityEngine.Object)
extern void UnityExtensions_SafeIsUnityNull_m9609D5559427508D80B7A9FFAE9DE8DA1C36A3E6 (void);
// 0x000001FF T[] Sirenix.Utilities.ArrayUtilities::CreateNewArrayWithAddedElement(T[],T)
// 0x00000200 T[] Sirenix.Utilities.ArrayUtilities::CreateNewArrayWithInsertedElement(T[],System.Int32,T)
// 0x00000201 T[] Sirenix.Utilities.ArrayUtilities::CreateNewArrayWithRemovedElement(T[],System.Int32)
// 0x00000202 System.Int32 Sirenix.Utilities.Cache`1::get_MaxCacheSize()
// 0x00000203 System.Void Sirenix.Utilities.Cache`1::set_MaxCacheSize(System.Int32)
// 0x00000204 System.Void Sirenix.Utilities.Cache`1::.ctor()
// 0x00000205 System.Boolean Sirenix.Utilities.Cache`1::get_IsFree()
// 0x00000206 Sirenix.Utilities.Cache`1<T> Sirenix.Utilities.Cache`1::Claim()
// 0x00000207 System.Void Sirenix.Utilities.Cache`1::Release(Sirenix.Utilities.Cache`1<T>)
// 0x00000208 T Sirenix.Utilities.Cache`1::op_Implicit(Sirenix.Utilities.Cache`1<T>)
// 0x00000209 System.Void Sirenix.Utilities.Cache`1::Release()
// 0x0000020A System.Void Sirenix.Utilities.Cache`1::System.IDisposable.Dispose()
// 0x0000020B System.Void Sirenix.Utilities.Cache`1::.cctor()
// 0x0000020C System.Func`1<System.Object> Sirenix.Utilities.DeepReflection::CreateWeakStaticValueGetter(System.Type,System.Type,System.String,System.Boolean)
extern void DeepReflection_CreateWeakStaticValueGetter_m9E29876353579969056AD7A924B953B358B27085 (void);
// 0x0000020D System.Func`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateWeakInstanceValueGetter(System.Type,System.Type,System.String,System.Boolean)
extern void DeepReflection_CreateWeakInstanceValueGetter_m1AE87BCB3800E63A1AA3DC8D4C7B0A58B7A3B437 (void);
// 0x0000020E System.Action`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateWeakInstanceValueSetter(System.Type,System.Type,System.String,System.Boolean)
extern void DeepReflection_CreateWeakInstanceValueSetter_mFF808ADEE7A3B1EF1A351067BCDEB71A100CFCC6 (void);
// 0x0000020F System.Func`2<System.Object,TResult> Sirenix.Utilities.DeepReflection::CreateWeakInstanceValueGetter(System.Type,System.String,System.Boolean)
// 0x00000210 System.Func`1<TResult> Sirenix.Utilities.DeepReflection::CreateValueGetter(System.Type,System.String,System.Boolean)
// 0x00000211 System.Func`2<TTarget,TResult> Sirenix.Utilities.DeepReflection::CreateValueGetter(System.String,System.Boolean)
// 0x00000212 System.Func`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceGetDelegate1(System.Func`2<TTarget,TResult>)
// 0x00000213 System.Func`2<System.Object,TResult> Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceGetDelegate2(System.Func`2<TTarget,TResult>)
// 0x00000214 System.Func`1<System.Object> Sirenix.Utilities.DeepReflection::CreateWeakAliasForStaticGetDelegate(System.Func`1<TResult>)
// 0x00000215 System.Action`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceSetDelegate1(System.Action`2<TTarget,TArg1>)
// 0x00000216 System.Action`2<System.Object,TArg1> Sirenix.Utilities.DeepReflection::CreateWeakAliasForInstanceSetDelegate2(System.Action`2<TTarget,TArg1>)
// 0x00000217 System.Action`1<System.Object> Sirenix.Utilities.DeepReflection::CreateWeakAliasForStaticSetDelegate(System.Action`1<TArg1>)
// 0x00000218 System.Delegate Sirenix.Utilities.DeepReflection::CreateEmittedDeepValueGetterDelegate(System.String,System.Type,System.Type,System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep>,System.Boolean)
extern void DeepReflection_CreateEmittedDeepValueGetterDelegate_mFF689FCD50A5FBB229D1BF2D50A90B6CBBB40884 (void);
// 0x00000219 System.Func`1<System.Object> Sirenix.Utilities.DeepReflection::CreateSlowDeepStaticValueGetterDelegate(System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep>)
extern void DeepReflection_CreateSlowDeepStaticValueGetterDelegate_mF38DBBD010C6A2BCD55D5CA3A6A6361647AE3312 (void);
// 0x0000021A System.Func`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateSlowDeepInstanceValueGetterDelegate(System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep>)
extern void DeepReflection_CreateSlowDeepInstanceValueGetterDelegate_mEE74497E8303E28B7318D06A4BAF01C889775757 (void);
// 0x0000021B System.Action`2<System.Object,System.Object> Sirenix.Utilities.DeepReflection::CreateSlowDeepInstanceValueSetterDelegate(System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep>)
extern void DeepReflection_CreateSlowDeepInstanceValueSetterDelegate_mD50C6511D6AE5152B2C4FA7338627FDEB69F6154 (void);
// 0x0000021C System.Object Sirenix.Utilities.DeepReflection::SlowGetMemberValue(Sirenix.Utilities.DeepReflection/PathStep,System.Object)
extern void DeepReflection_SlowGetMemberValue_m26CC015099061413CDEF889DD94E47BBBD207016 (void);
// 0x0000021D System.Void Sirenix.Utilities.DeepReflection::SlowSetMemberValue(Sirenix.Utilities.DeepReflection/PathStep,System.Object,System.Object)
extern void DeepReflection_SlowSetMemberValue_m73DC204731C29C010BB4079417E8C11DC9785629 (void);
// 0x0000021E System.Collections.Generic.List`1<Sirenix.Utilities.DeepReflection/PathStep> Sirenix.Utilities.DeepReflection::GetMemberPath(System.Type,System.Type&,System.String,System.Boolean&,System.Boolean)
extern void DeepReflection_GetMemberPath_m2A2FAF730F7B086CA6A5FB47E6F4E83A64778A42 (void);
// 0x0000021F System.Reflection.MemberInfo Sirenix.Utilities.DeepReflection::GetStepMember(System.Type,System.String,System.Boolean)
extern void DeepReflection_GetStepMember_m76C214F64F184C8F2E07AA048B568A05F4146B5D (void);
// 0x00000220 System.Void Sirenix.Utilities.DeepReflection::.cctor()
extern void DeepReflection__cctor_m20523DCA6AFD21927B9C503F25BA2B6176AD2E07 (void);
// 0x00000221 System.Void Sirenix.Utilities.DeepReflection/PathStep::.ctor(System.Reflection.MemberInfo)
extern void PathStep__ctor_m2BC57C1154B7159F6A09789BFD93793E05D78C79_AdjustorThunk (void);
// 0x00000222 System.Void Sirenix.Utilities.DeepReflection/PathStep::.ctor(System.Int32)
extern void PathStep__ctor_mB4EA450A87475972D4F86C8DD2B765208447FA8F_AdjustorThunk (void);
// 0x00000223 System.Void Sirenix.Utilities.DeepReflection/PathStep::.ctor(System.Int32,System.Type,System.Boolean)
extern void PathStep__ctor_mC572D4729D575556442308729E1279799FCDA53B_AdjustorThunk (void);
// 0x00000224 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass11_0`1::.ctor()
// 0x00000225 TResult Sirenix.Utilities.DeepReflection/<>c__DisplayClass11_0`1::<CreateWeakInstanceValueGetter>b__0(System.Object)
// 0x00000226 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass12_0`1::.ctor()
// 0x00000227 TResult Sirenix.Utilities.DeepReflection/<>c__DisplayClass12_0`1::<CreateValueGetter>b__0()
// 0x00000228 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass13_0`2::.ctor()
// 0x00000229 TResult Sirenix.Utilities.DeepReflection/<>c__DisplayClass13_0`2::<CreateValueGetter>b__0(TTarget)
// 0x0000022A System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass14_0`2::.ctor()
// 0x0000022B System.Object Sirenix.Utilities.DeepReflection/<>c__DisplayClass14_0`2::<CreateWeakAliasForInstanceGetDelegate1>b__0(System.Object)
// 0x0000022C System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass15_0`2::.ctor()
// 0x0000022D TResult Sirenix.Utilities.DeepReflection/<>c__DisplayClass15_0`2::<CreateWeakAliasForInstanceGetDelegate2>b__0(System.Object)
// 0x0000022E System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass16_0`1::.ctor()
// 0x0000022F System.Object Sirenix.Utilities.DeepReflection/<>c__DisplayClass16_0`1::<CreateWeakAliasForStaticGetDelegate>b__0()
// 0x00000230 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass17_0`2::.ctor()
// 0x00000231 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass17_0`2::<CreateWeakAliasForInstanceSetDelegate1>b__0(System.Object,System.Object)
// 0x00000232 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass18_0`2::.ctor()
// 0x00000233 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass18_0`2::<CreateWeakAliasForInstanceSetDelegate2>b__0(System.Object,TArg1)
// 0x00000234 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass19_0`1::.ctor()
// 0x00000235 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass19_0`1::<CreateWeakAliasForStaticSetDelegate>b__0(System.Object)
// 0x00000236 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m326BEA86A72EFDF74C923DC57EFDC17BA7D348C5 (void);
// 0x00000237 System.Object Sirenix.Utilities.DeepReflection/<>c__DisplayClass21_0::<CreateSlowDeepStaticValueGetterDelegate>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CCreateSlowDeepStaticValueGetterDelegateU3Eb__0_m835B4ABB788D4A44854B18F602861C97B4254F5F (void);
// 0x00000238 System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m0F5A5768B9013489AB743A64BAC61DEED4096383 (void);
// 0x00000239 System.Object Sirenix.Utilities.DeepReflection/<>c__DisplayClass22_0::<CreateSlowDeepInstanceValueGetterDelegate>b__0(System.Object)
extern void U3CU3Ec__DisplayClass22_0_U3CCreateSlowDeepInstanceValueGetterDelegateU3Eb__0_m4CCEDACB659C316DCA75197FD6FBDB6331805BDB (void);
// 0x0000023A System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mB930061FD4DE76C35FDA2E03E963C13AB7BB9458 (void);
// 0x0000023B System.Void Sirenix.Utilities.DeepReflection/<>c__DisplayClass23_0::<CreateSlowDeepInstanceValueSetterDelegate>b__0(System.Object,System.Object)
extern void U3CU3Ec__DisplayClass23_0_U3CCreateSlowDeepInstanceValueSetterDelegateU3Eb__0_m6444863B8002BB520DA75B61B5485DA7C899F2DE (void);
// 0x0000023C System.Void Sirenix.Utilities.DoubleLookupDictionary`3::.ctor()
// 0x0000023D System.Void Sirenix.Utilities.DoubleLookupDictionary`3::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirstKey>,System.Collections.Generic.IEqualityComparer`1<TSecondKey>)
// 0x0000023E System.Collections.Generic.Dictionary`2<TSecondKey,TValue> Sirenix.Utilities.DoubleLookupDictionary`3::get_Item(TFirstKey)
// 0x0000023F System.Int32 Sirenix.Utilities.DoubleLookupDictionary`3::InnerCount(TFirstKey)
// 0x00000240 System.Int32 Sirenix.Utilities.DoubleLookupDictionary`3::TotalInnerCount()
// 0x00000241 System.Boolean Sirenix.Utilities.DoubleLookupDictionary`3::ContainsKeys(TFirstKey,TSecondKey)
// 0x00000242 System.Boolean Sirenix.Utilities.DoubleLookupDictionary`3::TryGetInnerValue(TFirstKey,TSecondKey,TValue&)
// 0x00000243 TValue Sirenix.Utilities.DoubleLookupDictionary`3::AddInner(TFirstKey,TSecondKey,TValue)
// 0x00000244 System.Boolean Sirenix.Utilities.DoubleLookupDictionary`3::RemoveInner(TFirstKey,TSecondKey)
// 0x00000245 System.Void Sirenix.Utilities.DoubleLookupDictionary`3::RemoveWhere(System.Func`2<TValue,System.Boolean>)
// 0x00000246 System.Void Sirenix.Utilities.WeakValueGetter::.ctor(System.Object,System.IntPtr)
extern void WeakValueGetter__ctor_mCF0428BEC5349653D7E71B478AC65D80B1FA350D (void);
// 0x00000247 System.Object Sirenix.Utilities.WeakValueGetter::Invoke(System.Object&)
extern void WeakValueGetter_Invoke_m0A7E1880F1C7A0D758E5B9A3E242CA701D4E45F3 (void);
// 0x00000248 System.IAsyncResult Sirenix.Utilities.WeakValueGetter::BeginInvoke(System.Object&,System.AsyncCallback,System.Object)
extern void WeakValueGetter_BeginInvoke_m713888D810ECDD3FBE40D6891FC63863F4181F50 (void);
// 0x00000249 System.Object Sirenix.Utilities.WeakValueGetter::EndInvoke(System.Object&,System.IAsyncResult)
extern void WeakValueGetter_EndInvoke_m191201DBBFA1901AD71996713F6F9385EB0EFCE3 (void);
// 0x0000024A System.Void Sirenix.Utilities.WeakValueSetter::.ctor(System.Object,System.IntPtr)
extern void WeakValueSetter__ctor_mF8E82670751C0974BC53C7C95BE27D98F0CD682F (void);
// 0x0000024B System.Void Sirenix.Utilities.WeakValueSetter::Invoke(System.Object&,System.Object)
extern void WeakValueSetter_Invoke_m965159ED7A86D5269F5C19F8F7A37F470C3767DD (void);
// 0x0000024C System.IAsyncResult Sirenix.Utilities.WeakValueSetter::BeginInvoke(System.Object&,System.Object,System.AsyncCallback,System.Object)
extern void WeakValueSetter_BeginInvoke_m36C5647D16BBF4D5D42D2F30B1DEBFFAF76F5142 (void);
// 0x0000024D System.Void Sirenix.Utilities.WeakValueSetter::EndInvoke(System.Object&,System.IAsyncResult)
extern void WeakValueSetter_EndInvoke_mE76BD25B432FF9C6E3A9D9803419619EBBB571C3 (void);
// 0x0000024E System.Void Sirenix.Utilities.WeakValueGetter`1::.ctor(System.Object,System.IntPtr)
// 0x0000024F FieldType Sirenix.Utilities.WeakValueGetter`1::Invoke(System.Object&)
// 0x00000250 System.IAsyncResult Sirenix.Utilities.WeakValueGetter`1::BeginInvoke(System.Object&,System.AsyncCallback,System.Object)
// 0x00000251 FieldType Sirenix.Utilities.WeakValueGetter`1::EndInvoke(System.Object&,System.IAsyncResult)
// 0x00000252 System.Void Sirenix.Utilities.WeakValueSetter`1::.ctor(System.Object,System.IntPtr)
// 0x00000253 System.Void Sirenix.Utilities.WeakValueSetter`1::Invoke(System.Object&,FieldType)
// 0x00000254 System.IAsyncResult Sirenix.Utilities.WeakValueSetter`1::BeginInvoke(System.Object&,FieldType,System.AsyncCallback,System.Object)
// 0x00000255 System.Void Sirenix.Utilities.WeakValueSetter`1::EndInvoke(System.Object&,System.IAsyncResult)
// 0x00000256 System.Void Sirenix.Utilities.ValueGetter`2::.ctor(System.Object,System.IntPtr)
// 0x00000257 FieldType Sirenix.Utilities.ValueGetter`2::Invoke(InstanceType&)
// 0x00000258 System.IAsyncResult Sirenix.Utilities.ValueGetter`2::BeginInvoke(InstanceType&,System.AsyncCallback,System.Object)
// 0x00000259 FieldType Sirenix.Utilities.ValueGetter`2::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x0000025A System.Void Sirenix.Utilities.ValueSetter`2::.ctor(System.Object,System.IntPtr)
// 0x0000025B System.Void Sirenix.Utilities.ValueSetter`2::Invoke(InstanceType&,FieldType)
// 0x0000025C System.IAsyncResult Sirenix.Utilities.ValueSetter`2::BeginInvoke(InstanceType&,FieldType,System.AsyncCallback,System.Object)
// 0x0000025D System.Void Sirenix.Utilities.ValueSetter`2::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x0000025E System.Boolean Sirenix.Utilities.EmitUtilities::get_CanEmit()
extern void EmitUtilities_get_CanEmit_m831F45D116F130774C81E2281CC48A66E6D031E1 (void);
// 0x0000025F System.Func`1<FieldType> Sirenix.Utilities.EmitUtilities::CreateStaticFieldGetter(System.Reflection.FieldInfo)
// 0x00000260 System.Func`1<System.Object> Sirenix.Utilities.EmitUtilities::CreateWeakStaticFieldGetter(System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakStaticFieldGetter_m590B8963414FF8ED20402CF81290DCB75378E0B6 (void);
// 0x00000261 System.Action`1<FieldType> Sirenix.Utilities.EmitUtilities::CreateStaticFieldSetter(System.Reflection.FieldInfo)
// 0x00000262 System.Action`1<System.Object> Sirenix.Utilities.EmitUtilities::CreateWeakStaticFieldSetter(System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakStaticFieldSetter_m3691FE02BC70F8F2D560E31FFE7A2A5DAD26AD76 (void);
// 0x00000263 Sirenix.Utilities.ValueGetter`2<InstanceType,FieldType> Sirenix.Utilities.EmitUtilities::CreateInstanceFieldGetter(System.Reflection.FieldInfo)
// 0x00000264 Sirenix.Utilities.WeakValueGetter`1<FieldType> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceFieldGetter(System.Type,System.Reflection.FieldInfo)
// 0x00000265 Sirenix.Utilities.WeakValueGetter Sirenix.Utilities.EmitUtilities::CreateWeakInstanceFieldGetter(System.Type,System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakInstanceFieldGetter_mB0C0450066FCF1737A61EF7DEC132CB2CF88E391 (void);
// 0x00000266 Sirenix.Utilities.ValueSetter`2<InstanceType,FieldType> Sirenix.Utilities.EmitUtilities::CreateInstanceFieldSetter(System.Reflection.FieldInfo)
// 0x00000267 Sirenix.Utilities.WeakValueSetter`1<FieldType> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceFieldSetter(System.Type,System.Reflection.FieldInfo)
// 0x00000268 Sirenix.Utilities.WeakValueSetter Sirenix.Utilities.EmitUtilities::CreateWeakInstanceFieldSetter(System.Type,System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakInstanceFieldSetter_mBA836299B3D5C427760871875AB6116D24DCBC83 (void);
// 0x00000269 Sirenix.Utilities.WeakValueGetter Sirenix.Utilities.EmitUtilities::CreateWeakInstancePropertyGetter(System.Type,System.Reflection.PropertyInfo)
extern void EmitUtilities_CreateWeakInstancePropertyGetter_mB2F6169975D95EAAA26D2C99E73A1990CF586207 (void);
// 0x0000026A Sirenix.Utilities.WeakValueSetter Sirenix.Utilities.EmitUtilities::CreateWeakInstancePropertySetter(System.Type,System.Reflection.PropertyInfo)
extern void EmitUtilities_CreateWeakInstancePropertySetter_m08F9D0D8EF1F896D3F2CFA2E42B1E94A5CA8D021 (void);
// 0x0000026B System.Action`1<PropType> Sirenix.Utilities.EmitUtilities::CreateStaticPropertySetter(System.Reflection.PropertyInfo)
// 0x0000026C System.Func`1<PropType> Sirenix.Utilities.EmitUtilities::CreateStaticPropertyGetter(System.Reflection.PropertyInfo)
// 0x0000026D Sirenix.Utilities.ValueSetter`2<InstanceType,PropType> Sirenix.Utilities.EmitUtilities::CreateInstancePropertySetter(System.Reflection.PropertyInfo)
// 0x0000026E Sirenix.Utilities.ValueGetter`2<InstanceType,PropType> Sirenix.Utilities.EmitUtilities::CreateInstancePropertyGetter(System.Reflection.PropertyInfo)
// 0x0000026F System.Func`2<InstanceType,ReturnType> Sirenix.Utilities.EmitUtilities::CreateMethodReturner(System.Reflection.MethodInfo)
// 0x00000270 System.Action Sirenix.Utilities.EmitUtilities::CreateStaticMethodCaller(System.Reflection.MethodInfo)
extern void EmitUtilities_CreateStaticMethodCaller_mC7372959C42FA3246EFF8F187B288301DE7E0458 (void);
// 0x00000271 System.Action`2<System.Object,TArg1> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x00000272 System.Action`1<System.Object> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
extern void EmitUtilities_CreateWeakInstanceMethodCaller_m6FE17D0A6A23374135174753FF048E8287C35D4D (void);
// 0x00000273 System.Func`3<System.Object,TArg1,TResult> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x00000274 System.Func`2<System.Object,TResult> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceMethodCallerFunc(System.Reflection.MethodInfo)
// 0x00000275 System.Func`3<System.Object,TArg,TResult> Sirenix.Utilities.EmitUtilities::CreateWeakInstanceMethodCallerFunc(System.Reflection.MethodInfo)
// 0x00000276 System.Action`1<InstanceType> Sirenix.Utilities.EmitUtilities::CreateInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x00000277 System.Action`2<InstanceType,Arg1> Sirenix.Utilities.EmitUtilities::CreateInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x00000278 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass2_0`1::.ctor()
// 0x00000279 FieldType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass2_0`1::<CreateStaticFieldGetter>b__0()
// 0x0000027A System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass2_1`1::.ctor()
// 0x0000027B FieldType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass2_1`1::<CreateStaticFieldGetter>b__1()
// 0x0000027C System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB9B335E229E5DE3D4A9456E6C98610498AC2CB76 (void);
// 0x0000027D System.Object Sirenix.Utilities.EmitUtilities/<>c__DisplayClass3_0::<CreateWeakStaticFieldGetter>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CCreateWeakStaticFieldGetterU3Eb__0_m10E424384DD20AAFF821563E0EF52925B6A1B6C9 (void);
// 0x0000027E System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass4_0`1::.ctor()
// 0x0000027F System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass4_0`1::<CreateStaticFieldSetter>b__0(FieldType)
// 0x00000280 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m5EF22D44DB96EF2A055FE8D70AA33D747A99F961 (void);
// 0x00000281 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass5_0::<CreateWeakStaticFieldSetter>b__0(System.Object)
extern void U3CU3Ec__DisplayClass5_0_U3CCreateWeakStaticFieldSetterU3Eb__0_m24F92E5D14E6947C5B792E6BAE5432A8D003A81E (void);
// 0x00000282 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass6_0`2::.ctor()
// 0x00000283 FieldType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass6_0`2::<CreateInstanceFieldGetter>b__0(InstanceType&)
// 0x00000284 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass7_0`1::.ctor()
// 0x00000285 FieldType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass7_0`1::<CreateWeakInstanceFieldGetter>b__0(System.Object&)
// 0x00000286 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m7A54C06418E3AA81FC4083D9E49EB32C825AAF0B (void);
// 0x00000287 System.Object Sirenix.Utilities.EmitUtilities/<>c__DisplayClass8_0::<CreateWeakInstanceFieldGetter>b__0(System.Object&)
extern void U3CU3Ec__DisplayClass8_0_U3CCreateWeakInstanceFieldGetterU3Eb__0_m210C7902B0325C3D098FB545319F1BDD915A9CFF (void);
// 0x00000288 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass9_0`2::.ctor()
// 0x00000289 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass9_0`2::<CreateInstanceFieldSetter>b__0(InstanceType&,FieldType)
// 0x0000028A System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass10_0`1::.ctor()
// 0x0000028B System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass10_0`1::<CreateWeakInstanceFieldSetter>b__0(System.Object&,FieldType)
// 0x0000028C System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m5DFC9A0E842CD030F03952EB2914B9726E4723AD (void);
// 0x0000028D System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass11_0::<CreateWeakInstanceFieldSetter>b__0(System.Object&,System.Object)
extern void U3CU3Ec__DisplayClass11_0_U3CCreateWeakInstanceFieldSetterU3Eb__0_m1D1BC4E6AAC385CD4EA32C5F91D7E72A129E8EDC (void);
// 0x0000028E System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m3861E9A645F9AFA6125A39F2279CEA82ED9E9BD1 (void);
// 0x0000028F System.Object Sirenix.Utilities.EmitUtilities/<>c__DisplayClass12_0::<CreateWeakInstancePropertyGetter>b__0(System.Object&)
extern void U3CU3Ec__DisplayClass12_0_U3CCreateWeakInstancePropertyGetterU3Eb__0_m7B15E6DE814E5A07D6BA2701DD41533FF65C8185 (void);
// 0x00000290 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mF4F9ABDE00AAB0DBDF8B5437F4B870B19DF83049 (void);
// 0x00000291 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass13_0::<CreateWeakInstancePropertySetter>b__0(System.Object&,System.Object)
extern void U3CU3Ec__DisplayClass13_0_U3CCreateWeakInstancePropertySetterU3Eb__0_mD6DE6ED471F42C506ACDBF8B767B84281925CC03 (void);
// 0x00000292 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass14_0`1::.ctor()
// 0x00000293 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass14_0`1::<CreateStaticPropertySetter>b__0(PropType)
// 0x00000294 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass15_0`1::.ctor()
// 0x00000295 PropType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass15_0`1::<CreateStaticPropertyGetter>b__0()
// 0x00000296 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass16_0`2::.ctor()
// 0x00000297 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass16_0`2::<CreateInstancePropertySetter>b__0(InstanceType&,PropType)
// 0x00000298 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass17_0`2::.ctor()
// 0x00000299 PropType Sirenix.Utilities.EmitUtilities/<>c__DisplayClass17_0`2::<CreateInstancePropertyGetter>b__0(InstanceType&)
// 0x0000029A System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass20_0`1::.ctor()
// 0x0000029B System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass20_0`1::<CreateWeakInstanceMethodCaller>b__0(System.Object,TArg1)
// 0x0000029C System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m2A449559E198BACEDF8695063003000996AD8667 (void);
// 0x0000029D System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass21_0::<CreateWeakInstanceMethodCaller>b__0(System.Object)
extern void U3CU3Ec__DisplayClass21_0_U3CCreateWeakInstanceMethodCallerU3Eb__0_m14B7A3F705494452C9FFAD7BA1D8D11223E4997D (void);
// 0x0000029E System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass22_0`2::.ctor()
// 0x0000029F TResult Sirenix.Utilities.EmitUtilities/<>c__DisplayClass22_0`2::<CreateWeakInstanceMethodCaller>b__0(System.Object,TArg1)
// 0x000002A0 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass23_0`1::.ctor()
// 0x000002A1 TResult Sirenix.Utilities.EmitUtilities/<>c__DisplayClass23_0`1::<CreateWeakInstanceMethodCallerFunc>b__0(System.Object)
// 0x000002A2 System.Void Sirenix.Utilities.EmitUtilities/<>c__DisplayClass24_0`2::.ctor()
// 0x000002A3 TResult Sirenix.Utilities.EmitUtilities/<>c__DisplayClass24_0`2::<CreateWeakInstanceMethodCallerFunc>b__0(System.Object,TArg)
// 0x000002A4 Sirenix.Utilities.GlobalConfigAttribute Sirenix.Utilities.GlobalConfig`1::get_ConfigAttribute()
// 0x000002A5 System.Boolean Sirenix.Utilities.GlobalConfig`1::get_HasInstanceLoaded()
// 0x000002A6 T Sirenix.Utilities.GlobalConfig`1::get_Instance()
// 0x000002A7 System.Void Sirenix.Utilities.GlobalConfig`1::LoadInstanceIfAssetExists()
// 0x000002A8 System.Void Sirenix.Utilities.GlobalConfig`1::OpenInEditor()
// 0x000002A9 System.Void Sirenix.Utilities.GlobalConfig`1::OnConfigAutoCreated()
// 0x000002AA System.Void Sirenix.Utilities.GlobalConfig`1::.ctor()
// 0x000002AB System.String Sirenix.Utilities.GlobalConfigAttribute::get_FullPath()
extern void GlobalConfigAttribute_get_FullPath_m56136FEF1B3488B8A53049E84006E71B9386A31F (void);
// 0x000002AC System.String Sirenix.Utilities.GlobalConfigAttribute::get_AssetPath()
extern void GlobalConfigAttribute_get_AssetPath_m24AD80D23594B2E1811CF9E991359AAB8D0E5639 (void);
// 0x000002AD System.String Sirenix.Utilities.GlobalConfigAttribute::get_AssetPathWithAssetsPrefix()
extern void GlobalConfigAttribute_get_AssetPathWithAssetsPrefix_m8672F9E844036A985D11791D1AA1E4D0F8FE8D8D (void);
// 0x000002AE System.String Sirenix.Utilities.GlobalConfigAttribute::get_AssetPathWithoutAssetsPrefix()
extern void GlobalConfigAttribute_get_AssetPathWithoutAssetsPrefix_mCDE6F775DB99CF4A1E88CCC090C4D072B4E36487 (void);
// 0x000002AF System.String Sirenix.Utilities.GlobalConfigAttribute::get_ResourcesPath()
extern void GlobalConfigAttribute_get_ResourcesPath_m143963C31BEE08369F9EEA4FC7EABB7C96A8749A (void);
// 0x000002B0 System.Boolean Sirenix.Utilities.GlobalConfigAttribute::get_UseAsset()
extern void GlobalConfigAttribute_get_UseAsset_m6DE933FA318D235196AAD7C3A182DB581BFB526C (void);
// 0x000002B1 System.Void Sirenix.Utilities.GlobalConfigAttribute::set_UseAsset(System.Boolean)
extern void GlobalConfigAttribute_set_UseAsset_m98E3100E2A2BFBE9C311C1DDA29DEBEE438F73C7 (void);
// 0x000002B2 System.Boolean Sirenix.Utilities.GlobalConfigAttribute::get_IsInResourcesFolder()
extern void GlobalConfigAttribute_get_IsInResourcesFolder_mC40EB7730EAC10B07754825D5B1072BDDB035E6D (void);
// 0x000002B3 System.Void Sirenix.Utilities.GlobalConfigAttribute::.ctor()
extern void GlobalConfigAttribute__ctor_mB2985C6FEF081EF9B750E38966D7ACE652EE7875 (void);
// 0x000002B4 System.Void Sirenix.Utilities.GlobalConfigAttribute::.ctor(System.String)
extern void GlobalConfigAttribute__ctor_m339998A57E405779FA1C91A99DAFDC88A7216956 (void);
// 0x000002B5 System.Void Sirenix.Utilities.GUILayoutOptions::.cctor()
extern void GUILayoutOptions__cctor_mD785819DD9B2E37FB089ED9F7F50E4E2EBCFF14A (void);
// 0x000002B6 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::Width(System.Single)
extern void GUILayoutOptions_Width_mB6165E9E9DE8E5AA5BFA7A38241532A4B1144EAB (void);
// 0x000002B7 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::Height(System.Single)
extern void GUILayoutOptions_Height_mFEE3D67D07DDF96C52B9BB615D6382C931A6EE33 (void);
// 0x000002B8 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::MaxHeight(System.Single)
extern void GUILayoutOptions_MaxHeight_mC775EB75874DCA1BFEF222FB5EEC0B3FA0FF33D7 (void);
// 0x000002B9 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::MaxWidth(System.Single)
extern void GUILayoutOptions_MaxWidth_m9AC74528ECD413E457482EF1826B876A84EAB2E1 (void);
// 0x000002BA Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::MinWidth(System.Single)
extern void GUILayoutOptions_MinWidth_m09A60F7BDEA20AFBB11CD4CBE319395E6DAFEEBF (void);
// 0x000002BB Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::MinHeight(System.Single)
extern void GUILayoutOptions_MinHeight_m3DE57D9BB5F890F35351044C9AD440ABBF1E50F0 (void);
// 0x000002BC Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::ExpandHeight(System.Boolean)
extern void GUILayoutOptions_ExpandHeight_m09B6463BDF36A2517FCB52C983CCB6527A9E2B4C (void);
// 0x000002BD Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions::ExpandWidth(System.Boolean)
extern void GUILayoutOptions_ExpandWidth_m6D2FD838A01289CBC26B918B14B505D09E51549F (void);
// 0x000002BE UnityEngine.GUILayoutOption[] Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::GetCachedOptions()
extern void GUILayoutOptionsInstance_GetCachedOptions_mEEF48175DAA16E14CBA97B51405611B8920169A3 (void);
// 0x000002BF UnityEngine.GUILayoutOption[] Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::op_Implicit(Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance)
extern void GUILayoutOptionsInstance_op_Implicit_mAB100A4821FAB6820406C0ADB7CCEF354E77F595 (void);
// 0x000002C0 UnityEngine.GUILayoutOption[] Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::CreateOptionsArary()
extern void GUILayoutOptionsInstance_CreateOptionsArary_m0B31D88A8BFF2DA52A675AE9857B633AAC60AD9A (void);
// 0x000002C1 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::Clone()
extern void GUILayoutOptionsInstance_Clone_m3EFA9A2E9822DCBF20A117118BEE3F4C066FFEAC (void);
// 0x000002C2 System.Void Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::.ctor()
extern void GUILayoutOptionsInstance__ctor_m5DFC01FE3A4CFEF16A3019B81A863701B1A3AD8A (void);
// 0x000002C3 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::Width(System.Single)
extern void GUILayoutOptionsInstance_Width_m85CC356D425BA379404829E4FE72E3F26D7A468C (void);
// 0x000002C4 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::Height(System.Single)
extern void GUILayoutOptionsInstance_Height_m13712C3BDC953B1FFDC750F823C2DA24D10725F7 (void);
// 0x000002C5 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::MaxHeight(System.Single)
extern void GUILayoutOptionsInstance_MaxHeight_mC887BE759283C4D02CD7C3692F92E31E6700F643 (void);
// 0x000002C6 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::MaxWidth(System.Single)
extern void GUILayoutOptionsInstance_MaxWidth_mF632FF1F7AE7F79EE4A79EEE272BE5E8614EB5EC (void);
// 0x000002C7 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::MinHeight(System.Single)
extern void GUILayoutOptionsInstance_MinHeight_mF8F6A50F008400329D30E8BC3EEABDFD8CDF91EA (void);
// 0x000002C8 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::MinWidth(System.Single)
extern void GUILayoutOptionsInstance_MinWidth_m0D14EBEE1B546132792DC5D467D6ABE2DE44AFD3 (void);
// 0x000002C9 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::ExpandHeight(System.Boolean)
extern void GUILayoutOptionsInstance_ExpandHeight_mC84470587A8CA7B1B44E653FF782525BEE5E32DE (void);
// 0x000002CA Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::ExpandWidth(System.Boolean)
extern void GUILayoutOptionsInstance_ExpandWidth_m1C87C0C6EEF46B3B002029722E292F828FD9C005 (void);
// 0x000002CB System.Void Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::SetValue(Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionType,System.Single)
extern void GUILayoutOptionsInstance_SetValue_m27B465B91BA7B5C75EEC41BC91D051C990BA7DEA (void);
// 0x000002CC System.Void Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::SetValue(Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionType,System.Boolean)
extern void GUILayoutOptionsInstance_SetValue_m28E473A986E3B648CCBE637630F97A0B8C1452BA (void);
// 0x000002CD System.Boolean Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::Equals(Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance)
extern void GUILayoutOptionsInstance_Equals_mE1A35AFEDADF232034198E494F897534613020EF (void);
// 0x000002CE System.Int32 Sirenix.Utilities.GUILayoutOptions/GUILayoutOptionsInstance::GetHashCode()
extern void GUILayoutOptionsInstance_GetHashCode_mD5E640741B42B6497310376AF108321D6CD43B19 (void);
// 0x000002CF System.Void Sirenix.Utilities.ICacheNotificationReceiver::OnFreed()
// 0x000002D0 System.Void Sirenix.Utilities.ICacheNotificationReceiver::OnClaimed()
// 0x000002D1 System.Void Sirenix.Utilities.ImmutableHashSet`1::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000002D2 System.Boolean Sirenix.Utilities.ImmutableHashSet`1::Contains(T)
// 0x000002D3 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.ImmutableHashSet`1::GetEnumerator()
// 0x000002D4 System.Collections.IEnumerator Sirenix.Utilities.ImmutableHashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000002D5 T Sirenix.Utilities.IImmutableList`1::get_Item(System.Int32)
// 0x000002D6 System.Void Sirenix.Utilities.ImmutableList::.ctor(System.Collections.IList)
extern void ImmutableList__ctor_m39E18EC34C30F4D0D843ADF08E31EE3D180FE93E (void);
// 0x000002D7 System.Int32 Sirenix.Utilities.ImmutableList::get_Count()
extern void ImmutableList_get_Count_m5FBCEA74E297F5642FBFB00D223521C513528A06 (void);
// 0x000002D8 System.Boolean Sirenix.Utilities.ImmutableList::get_IsFixedSize()
extern void ImmutableList_get_IsFixedSize_m4DF4081E30B8BAB12EDF20EF081478591D0C16D8 (void);
// 0x000002D9 System.Boolean Sirenix.Utilities.ImmutableList::get_IsReadOnly()
extern void ImmutableList_get_IsReadOnly_m75101378E544D6EF75181C615D1DDDB2FB1277B5 (void);
// 0x000002DA System.Boolean Sirenix.Utilities.ImmutableList::get_IsSynchronized()
extern void ImmutableList_get_IsSynchronized_m8CDEB5875D93BE84EFAE735970EB900A2649B248 (void);
// 0x000002DB System.Object Sirenix.Utilities.ImmutableList::get_SyncRoot()
extern void ImmutableList_get_SyncRoot_mA0B584308DBFDC0C4E2830D76F759F97CD3EDA8F (void);
// 0x000002DC System.Object Sirenix.Utilities.ImmutableList::System.Collections.IList.get_Item(System.Int32)
extern void ImmutableList_System_Collections_IList_get_Item_m55D326F5A70AEE224DA4D7D514539B9740FAE931 (void);
// 0x000002DD System.Void Sirenix.Utilities.ImmutableList::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void ImmutableList_System_Collections_IList_set_Item_m1E617DC22474DA5D9A686A315E1E359DD9044399 (void);
// 0x000002DE System.Object Sirenix.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.get_Item(System.Int32)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_get_Item_mF2A632CD4E30C7EC4ECD2E3E879D3497EE847828 (void);
// 0x000002DF System.Void Sirenix.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.set_Item(System.Int32,System.Object)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_set_Item_m007828EFF133BC523DF4ACBA8084E3787F3E61FA (void);
// 0x000002E0 System.Object Sirenix.Utilities.ImmutableList::get_Item(System.Int32)
extern void ImmutableList_get_Item_m787FB54661CABD3F78D8504A1002EE27F86FE80E (void);
// 0x000002E1 System.Boolean Sirenix.Utilities.ImmutableList::Contains(System.Object)
extern void ImmutableList_Contains_mB083BE1CB81A28F60B43DB6F430DCDAF03436DF9 (void);
// 0x000002E2 System.Void Sirenix.Utilities.ImmutableList::CopyTo(System.Object[],System.Int32)
extern void ImmutableList_CopyTo_m05DD1848FBFB6CAD73892A0ED6DD4A893E99E0E4 (void);
// 0x000002E3 System.Void Sirenix.Utilities.ImmutableList::CopyTo(System.Array,System.Int32)
extern void ImmutableList_CopyTo_mC44E04AAC0C145317F40DC8D1A3C0F1F9CA9A037 (void);
// 0x000002E4 System.Collections.IEnumerator Sirenix.Utilities.ImmutableList::GetEnumerator()
extern void ImmutableList_GetEnumerator_mC79EA52A900E5F7B337A7D11AA2AD9F0F1F4959C (void);
// 0x000002E5 System.Collections.IEnumerator Sirenix.Utilities.ImmutableList::System.Collections.IEnumerable.GetEnumerator()
extern void ImmutableList_System_Collections_IEnumerable_GetEnumerator_mC6D564CB551420271658CB179A3A2733B1DA26E5 (void);
// 0x000002E6 System.Collections.Generic.IEnumerator`1<System.Object> Sirenix.Utilities.ImmutableList::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
extern void ImmutableList_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m1010893F030A95F0EEB87BA7C63B01ACB9B668DB (void);
// 0x000002E7 System.Int32 Sirenix.Utilities.ImmutableList::System.Collections.IList.Add(System.Object)
extern void ImmutableList_System_Collections_IList_Add_m98C464AA93D9950892BBC7B131DF5C66843B8A78 (void);
// 0x000002E8 System.Void Sirenix.Utilities.ImmutableList::System.Collections.IList.Clear()
extern void ImmutableList_System_Collections_IList_Clear_m108D9F519719557FAF1CBD94BC83785800DEF018 (void);
// 0x000002E9 System.Void Sirenix.Utilities.ImmutableList::System.Collections.IList.Insert(System.Int32,System.Object)
extern void ImmutableList_System_Collections_IList_Insert_m6B42020078E798621501ECBB63425D8E2E2876E6 (void);
// 0x000002EA System.Void Sirenix.Utilities.ImmutableList::System.Collections.IList.Remove(System.Object)
extern void ImmutableList_System_Collections_IList_Remove_mF2194611D723AE664D5215D7A7815A8284D2BF78 (void);
// 0x000002EB System.Void Sirenix.Utilities.ImmutableList::System.Collections.IList.RemoveAt(System.Int32)
extern void ImmutableList_System_Collections_IList_RemoveAt_mDCEFF0BF6D57EEEFAB0C4A422076228F2F9C83A2 (void);
// 0x000002EC System.Int32 Sirenix.Utilities.ImmutableList::IndexOf(System.Object)
extern void ImmutableList_IndexOf_m0162CC7BA84711C8450B6559CE45E613CD881B65 (void);
// 0x000002ED System.Void Sirenix.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.RemoveAt(System.Int32)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_RemoveAt_m4C292994C2ED7563B62F9CF70002D3B08D888172 (void);
// 0x000002EE System.Void Sirenix.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.Insert(System.Int32,System.Object)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_Insert_m3114EAE42C28413944710D5C11075C8D2137A1E6 (void);
// 0x000002EF System.Void Sirenix.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Add(System.Object)
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Add_m9AD5516FCE1091EF3ED319374B8887F5EE471D2E (void);
// 0x000002F0 System.Void Sirenix.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Clear()
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Clear_m3058E54B8F188B94072325E458D9B0A2CBD33C54 (void);
// 0x000002F1 System.Boolean Sirenix.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Remove(System.Object)
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Remove_m24426B9BDB5E3BC79635A5C284B9CF5CDBD3CAA5 (void);
// 0x000002F2 System.Void Sirenix.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::.ctor(System.Int32)
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_mF71247C0669D13A3CCBEB9DDC43CB91FB13D7D2F (void);
// 0x000002F3 System.Void Sirenix.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::System.IDisposable.Dispose()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m55A31EA0E7CD9D1B866BF0EC395E0D0523335C36 (void);
// 0x000002F4 System.Boolean Sirenix.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::MoveNext()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_m731B20D72279F09A16AF1CB093A5A5A89C7E6282 (void);
// 0x000002F5 System.Void Sirenix.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::<>m__Finally1()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_mACE1DFA00F386EE79CBB7EC031E251C18D6FED17 (void);
// 0x000002F6 System.Object Sirenix.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5986C856A9B9449B39B542589647440252B70DAD (void);
// 0x000002F7 System.Void Sirenix.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::System.Collections.IEnumerator.Reset()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m67F6137A2C821A8BF580B221DFAEF24E71C3B334 (void);
// 0x000002F8 System.Object Sirenix.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m81047B92D0044C262E5451C3215F00295380FA9F (void);
// 0x000002F9 System.Void Sirenix.Utilities.ImmutableList`1::.ctor(System.Collections.Generic.IList`1<T>)
// 0x000002FA System.Int32 Sirenix.Utilities.ImmutableList`1::get_Count()
// 0x000002FB System.Boolean Sirenix.Utilities.ImmutableList`1::System.Collections.ICollection.get_IsSynchronized()
// 0x000002FC System.Object Sirenix.Utilities.ImmutableList`1::System.Collections.ICollection.get_SyncRoot()
// 0x000002FD System.Boolean Sirenix.Utilities.ImmutableList`1::System.Collections.IList.get_IsFixedSize()
// 0x000002FE System.Boolean Sirenix.Utilities.ImmutableList`1::System.Collections.IList.get_IsReadOnly()
// 0x000002FF System.Boolean Sirenix.Utilities.ImmutableList`1::get_IsReadOnly()
// 0x00000300 System.Object Sirenix.Utilities.ImmutableList`1::System.Collections.IList.get_Item(System.Int32)
// 0x00000301 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x00000302 T Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.get_Item(System.Int32)
// 0x00000303 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
// 0x00000304 T Sirenix.Utilities.ImmutableList`1::get_Item(System.Int32)
// 0x00000305 System.Boolean Sirenix.Utilities.ImmutableList`1::Contains(T)
// 0x00000306 System.Void Sirenix.Utilities.ImmutableList`1::CopyTo(T[],System.Int32)
// 0x00000307 System.Collections.Generic.IEnumerator`1<T> Sirenix.Utilities.ImmutableList`1::GetEnumerator()
// 0x00000308 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000309 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000030A System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Clear()
// 0x0000030B System.Boolean Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Remove(T)
// 0x0000030C System.Collections.IEnumerator Sirenix.Utilities.ImmutableList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000030D System.Int32 Sirenix.Utilities.ImmutableList`1::System.Collections.IList.Add(System.Object)
// 0x0000030E System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.IList.Clear()
// 0x0000030F System.Boolean Sirenix.Utilities.ImmutableList`1::System.Collections.IList.Contains(System.Object)
// 0x00000310 System.Int32 Sirenix.Utilities.ImmutableList`1::System.Collections.IList.IndexOf(System.Object)
// 0x00000311 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x00000312 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.IList.Remove(System.Object)
// 0x00000313 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
// 0x00000314 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.IList.RemoveAt(System.Int32)
// 0x00000315 System.Int32 Sirenix.Utilities.ImmutableList`1::IndexOf(T)
// 0x00000316 System.Void Sirenix.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
// 0x00000317 System.Void Sirenix.Utilities.ImmutableList`2::.ctor(TList)
// 0x00000318 System.Int32 Sirenix.Utilities.ImmutableList`2::get_Count()
// 0x00000319 System.Boolean Sirenix.Utilities.ImmutableList`2::System.Collections.ICollection.get_IsSynchronized()
// 0x0000031A System.Object Sirenix.Utilities.ImmutableList`2::System.Collections.ICollection.get_SyncRoot()
// 0x0000031B System.Boolean Sirenix.Utilities.ImmutableList`2::System.Collections.IList.get_IsFixedSize()
// 0x0000031C System.Boolean Sirenix.Utilities.ImmutableList`2::System.Collections.IList.get_IsReadOnly()
// 0x0000031D System.Boolean Sirenix.Utilities.ImmutableList`2::get_IsReadOnly()
// 0x0000031E System.Object Sirenix.Utilities.ImmutableList`2::System.Collections.IList.get_Item(System.Int32)
// 0x0000031F System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x00000320 TElement Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x00000321 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x00000322 TElement Sirenix.Utilities.ImmutableList`2::get_Item(System.Int32)
// 0x00000323 System.Boolean Sirenix.Utilities.ImmutableList`2::Contains(TElement)
// 0x00000324 System.Void Sirenix.Utilities.ImmutableList`2::CopyTo(TElement[],System.Int32)
// 0x00000325 System.Collections.Generic.IEnumerator`1<TElement> Sirenix.Utilities.ImmutableList`2::GetEnumerator()
// 0x00000326 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000327 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x00000328 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x00000329 System.Boolean Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x0000032A System.Collections.IEnumerator Sirenix.Utilities.ImmutableList`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000032B System.Int32 Sirenix.Utilities.ImmutableList`2::System.Collections.IList.Add(System.Object)
// 0x0000032C System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.IList.Clear()
// 0x0000032D System.Boolean Sirenix.Utilities.ImmutableList`2::System.Collections.IList.Contains(System.Object)
// 0x0000032E System.Int32 Sirenix.Utilities.ImmutableList`2::System.Collections.IList.IndexOf(System.Object)
// 0x0000032F System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x00000330 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.IList.Remove(System.Object)
// 0x00000331 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x00000332 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.IList.RemoveAt(System.Int32)
// 0x00000333 System.Int32 Sirenix.Utilities.ImmutableList`2::IndexOf(TElement)
// 0x00000334 System.Void Sirenix.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x00000335 System.Void Sirenix.Utilities.ListExtensions::SetLength(System.Collections.Generic.IList`1<T>&,System.Int32)
// 0x00000336 System.Void Sirenix.Utilities.ListExtensions::SetLength(System.Collections.Generic.IList`1<T>&,System.Int32,System.Func`1<T>)
// 0x00000337 System.Void Sirenix.Utilities.ListExtensions::SetLength(System.Collections.Generic.IList`1<T>,System.Int32)
// 0x00000338 System.Void Sirenix.Utilities.ListExtensions::SetLength(System.Collections.Generic.IList`1<T>,System.Int32,System.Func`1<T>)
// 0x00000339 System.Collections.Generic.ICollection`1<TKey> Sirenix.Utilities.IndexedDictionary`2::get_Keys()
// 0x0000033A System.Collections.Generic.ICollection`1<TValue> Sirenix.Utilities.IndexedDictionary`2::get_Values()
// 0x0000033B System.Int32 Sirenix.Utilities.IndexedDictionary`2::get_Count()
// 0x0000033C System.Boolean Sirenix.Utilities.IndexedDictionary`2::get_IsReadOnly()
// 0x0000033D System.Boolean Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.get_IsFixedSize()
// 0x0000033E System.Boolean Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.get_IsReadOnly()
// 0x0000033F System.Collections.ICollection Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.get_Keys()
// 0x00000340 System.Collections.ICollection Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.get_Values()
// 0x00000341 System.Int32 Sirenix.Utilities.IndexedDictionary`2::System.Collections.ICollection.get_Count()
// 0x00000342 System.Boolean Sirenix.Utilities.IndexedDictionary`2::System.Collections.ICollection.get_IsSynchronized()
// 0x00000343 System.Object Sirenix.Utilities.IndexedDictionary`2::System.Collections.ICollection.get_SyncRoot()
// 0x00000344 System.Object Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.get_Item(System.Object)
// 0x00000345 System.Void Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.set_Item(System.Object,System.Object)
// 0x00000346 TValue Sirenix.Utilities.IndexedDictionary`2::get_Item(TKey)
// 0x00000347 System.Void Sirenix.Utilities.IndexedDictionary`2::set_Item(TKey,TValue)
// 0x00000348 System.Void Sirenix.Utilities.IndexedDictionary`2::.ctor()
// 0x00000349 System.Void Sirenix.Utilities.IndexedDictionary`2::.ctor(System.Int32)
// 0x0000034A System.Collections.Generic.KeyValuePair`2<TKey,TValue> Sirenix.Utilities.IndexedDictionary`2::Get(System.Int32)
// 0x0000034B TKey Sirenix.Utilities.IndexedDictionary`2::GetKey(System.Int32)
// 0x0000034C TValue Sirenix.Utilities.IndexedDictionary`2::GetValue(System.Int32)
// 0x0000034D System.Int32 Sirenix.Utilities.IndexedDictionary`2::IndexOf(TKey)
// 0x0000034E System.Void Sirenix.Utilities.IndexedDictionary`2::Add(TKey,TValue)
// 0x0000034F System.Void Sirenix.Utilities.IndexedDictionary`2::Clear()
// 0x00000350 System.Boolean Sirenix.Utilities.IndexedDictionary`2::Remove(TKey)
// 0x00000351 System.Void Sirenix.Utilities.IndexedDictionary`2::RemoveAt(System.Int32)
// 0x00000352 System.Boolean Sirenix.Utilities.IndexedDictionary`2::ContainsKey(TKey)
// 0x00000353 System.Boolean Sirenix.Utilities.IndexedDictionary`2::TryGetValue(TKey,TValue&)
// 0x00000354 System.Void Sirenix.Utilities.IndexedDictionary`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000355 System.Boolean Sirenix.Utilities.IndexedDictionary`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000356 System.Void Sirenix.Utilities.IndexedDictionary`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x00000357 System.Boolean Sirenix.Utilities.IndexedDictionary`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000358 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Sirenix.Utilities.IndexedDictionary`2::GetEnumerator()
// 0x00000359 System.Collections.IEnumerator Sirenix.Utilities.IndexedDictionary`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000035A System.Void Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.Add(System.Object,System.Object)
// 0x0000035B System.Void Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.Clear()
// 0x0000035C System.Boolean Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.Contains(System.Object)
// 0x0000035D System.Collections.IDictionaryEnumerator Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.GetEnumerator()
// 0x0000035E System.Void Sirenix.Utilities.IndexedDictionary`2::System.Collections.IDictionary.Remove(System.Object)
// 0x0000035F System.Void Sirenix.Utilities.IndexedDictionary`2::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000360 System.Single Sirenix.Utilities.MathUtilities::PointDistanceToLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MathUtilities_PointDistanceToLine_m3F554EC70E178606D2CF582897B3415A62594D61 (void);
// 0x00000361 System.Single Sirenix.Utilities.MathUtilities::Hermite(System.Single,System.Single,System.Single)
extern void MathUtilities_Hermite_m0D70F719A680774029563E749582A021A497C4E1 (void);
// 0x00000362 System.Single Sirenix.Utilities.MathUtilities::StackHermite(System.Single,System.Single,System.Single,System.Int32)
extern void MathUtilities_StackHermite_m4B92CCA398BF837A923D8160E413B657E244A187 (void);
// 0x00000363 System.Single Sirenix.Utilities.MathUtilities::Fract(System.Single)
extern void MathUtilities_Fract_mE517258AEBE08DDB37603A3F1AAA3539AFA3D4CA (void);
// 0x00000364 UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::Fract(UnityEngine.Vector2)
extern void MathUtilities_Fract_m5F92E5CE4D517AB3FCA4A10492EBA0BD6EA57365 (void);
// 0x00000365 UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::Fract(UnityEngine.Vector3)
extern void MathUtilities_Fract_mFE9C0746BB5D12A21999CCF5EE9B732A11FE1483 (void);
// 0x00000366 System.Single Sirenix.Utilities.MathUtilities::BounceEaseInFastOut(System.Single)
extern void MathUtilities_BounceEaseInFastOut_m3EADC42038524125F048E6198D8F0D33628B6805 (void);
// 0x00000367 System.Single Sirenix.Utilities.MathUtilities::Hermite01(System.Single)
extern void MathUtilities_Hermite01_mB4D592A73B9AC0FCA325F942D25765E4C1120BF1 (void);
// 0x00000368 System.Single Sirenix.Utilities.MathUtilities::StackHermite01(System.Single,System.Int32)
extern void MathUtilities_StackHermite01_mE8C0C1131FF1DA2A0248E911AD92F19FA5DA4D5F (void);
// 0x00000369 UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::LerpUnclamped(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void MathUtilities_LerpUnclamped_mBE76F49534B8F3B138A2C2151F0F079E89312BD4 (void);
// 0x0000036A UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::LerpUnclamped(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void MathUtilities_LerpUnclamped_mCF23D00760B283A52BC8C353860EB93CEDFC1AA8 (void);
// 0x0000036B System.Single Sirenix.Utilities.MathUtilities::Bounce(System.Single)
extern void MathUtilities_Bounce_m5CCECE6C207736CFDD2058C8C3FA7E9F5F343D6E (void);
// 0x0000036C System.Single Sirenix.Utilities.MathUtilities::EaseInElastic(System.Single,System.Single,System.Single)
extern void MathUtilities_EaseInElastic_m410D85600ADD89A0126359436BB788DCCC637E99 (void);
// 0x0000036D UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::Pow(UnityEngine.Vector3,System.Single)
extern void MathUtilities_Pow_mC169808695BB5B77859857E6BCBD620A9FEC4CD4 (void);
// 0x0000036E UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::Abs(UnityEngine.Vector3)
extern void MathUtilities_Abs_m550FC9B831F3A442BA2C40A369CA77288DDDDC72 (void);
// 0x0000036F UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::Sign(UnityEngine.Vector3)
extern void MathUtilities_Sign_m94D2E6964F9EDEE8BAE9CBCE6BB33BE61F5FBD2F (void);
// 0x00000370 System.Single Sirenix.Utilities.MathUtilities::EaseOutElastic(System.Single,System.Single,System.Single)
extern void MathUtilities_EaseOutElastic_mE7015FC8F2F5162AF5D7159B470DD19BEC3B0B60 (void);
// 0x00000371 System.Single Sirenix.Utilities.MathUtilities::EaseInOut(System.Single)
extern void MathUtilities_EaseInOut_m135BBA69AB4672EBCAAE67701BFCE06B74ED7462 (void);
// 0x00000372 UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::Clamp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MathUtilities_Clamp_mE99524BFDD189E0F3773572F7CFCC895B616C478 (void);
// 0x00000373 UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::Clamp(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void MathUtilities_Clamp_m284B89E4861746A1509A44273C4F3282FDA6244F (void);
// 0x00000374 System.Int32 Sirenix.Utilities.MathUtilities::ComputeByteArrayHash(System.Byte[])
extern void MathUtilities_ComputeByteArrayHash_m4824945708DF79A71584BB8649144C26166C4947 (void);
// 0x00000375 UnityEngine.Vector3 Sirenix.Utilities.MathUtilities::InterpolatePoints(UnityEngine.Vector3[],System.Single)
extern void MathUtilities_InterpolatePoints_m245F80A9A411407C79BC3F3CB619133EB14FCD30 (void);
// 0x00000376 System.Boolean Sirenix.Utilities.MathUtilities::LineIntersectsLine(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2&)
extern void MathUtilities_LineIntersectsLine_m9F8CA7DBBE814DB2837518DF14F3AC4E32D033D7 (void);
// 0x00000377 UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::InfiniteLineIntersect(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void MathUtilities_InfiniteLineIntersect_m4B711DD7013033D94B18A614ED72F64796D071E4 (void);
// 0x00000378 System.Single Sirenix.Utilities.MathUtilities::LineDistToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MathUtilities_LineDistToPlane_m35DB57E189AE76C3CF1C0D6A891559FC8D99A095 (void);
// 0x00000379 System.Single Sirenix.Utilities.MathUtilities::RayDistToPlane(UnityEngine.Ray,UnityEngine.Plane)
extern void MathUtilities_RayDistToPlane_mABFCB75B12BA48DBFD403604747BBABBBC82129C (void);
// 0x0000037A UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::RotatePoint(UnityEngine.Vector2,System.Single)
extern void MathUtilities_RotatePoint_m3253CF8A6E71D2DAC872D644054F6963690C5861 (void);
// 0x0000037B UnityEngine.Vector2 Sirenix.Utilities.MathUtilities::RotatePoint(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void MathUtilities_RotatePoint_mE700DC535AA7E22EA16F5ECBCF0C8ADA0ED3BAF0 (void);
// 0x0000037C System.Single Sirenix.Utilities.MathUtilities::SmoothStep(System.Single,System.Single,System.Single)
extern void MathUtilities_SmoothStep_m59E460497C38E96DE6EB7C7096B3A4E4F29467D6 (void);
// 0x0000037D System.Single Sirenix.Utilities.MathUtilities::LinearStep(System.Single,System.Single,System.Single)
extern void MathUtilities_LinearStep_m0CB3CCFC198A1DD8332A13878D1110A6993E5058 (void);
// 0x0000037E System.Double Sirenix.Utilities.MathUtilities::Wrap(System.Double,System.Double,System.Double)
extern void MathUtilities_Wrap_mD735D6F28AE2B21C6E5D19988D887511B10F98F8 (void);
// 0x0000037F System.Single Sirenix.Utilities.MathUtilities::Wrap(System.Single,System.Single,System.Single)
extern void MathUtilities_Wrap_mE0E213A0AEF97B1F63ABA4946D7CE93C3490ED5C (void);
// 0x00000380 System.Int32 Sirenix.Utilities.MathUtilities::Wrap(System.Int32,System.Int32,System.Int32)
extern void MathUtilities_Wrap_m463BB6C601F15E09AD6BF312F00210FDE7ABC936 (void);
// 0x00000381 System.Double Sirenix.Utilities.MathUtilities::RoundBasedOnMinimumDifference(System.Double,System.Double)
extern void MathUtilities_RoundBasedOnMinimumDifference_m2C50FB893F79415626106E7BF7B2E7E07840D809 (void);
// 0x00000382 System.Double Sirenix.Utilities.MathUtilities::DiscardLeastSignificantDecimal(System.Double)
extern void MathUtilities_DiscardLeastSignificantDecimal_m94AEF71D8B171CB0A3AA58F2D38C7DB42B47506F (void);
// 0x00000383 System.Single Sirenix.Utilities.MathUtilities::ClampWrapAngle(System.Single,System.Single,System.Single)
extern void MathUtilities_ClampWrapAngle_m61C38865D587300A672113CD6CDF437C3571E417 (void);
// 0x00000384 System.Int32 Sirenix.Utilities.MathUtilities::GetNumberOfDecimalsForMinimumDifference(System.Double)
extern void MathUtilities_GetNumberOfDecimalsForMinimumDifference_mF03ACD71B58F28AC8EE1885CBD49F6B2A25B8AB0 (void);
// 0x00000385 TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::InsertOneColumnLeft(TElement[0...,0...],System.Int32)
// 0x00000386 TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::InsertOneColumnRight(TElement[0...,0...],System.Int32)
// 0x00000387 TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::InsertOneRowAbove(TElement[0...,0...],System.Int32)
// 0x00000388 TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::InsertOneRowBelow(TElement[0...,0...],System.Int32)
// 0x00000389 TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::DuplicateColumn(TElement[0...,0...],System.Int32)
// 0x0000038A TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::DuplicateRow(TElement[0...,0...],System.Int32)
// 0x0000038B TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::MoveColumn(TElement[0...,0...],System.Int32,System.Int32)
// 0x0000038C TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::MoveRow(TElement[0...,0...],System.Int32,System.Int32)
// 0x0000038D TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::DeleteColumn(TElement[0...,0...],System.Int32)
// 0x0000038E TElement[0...,0...] Sirenix.Utilities.MultiDimArrayUtilities::DeleteRow(TElement[0...,0...],System.Int32)
// 0x0000038F System.Void Sirenix.Utilities.PersistentAssemblyAttribute::.ctor()
extern void PersistentAssemblyAttribute__ctor_mB4B2296B1BA32B7F368DCAEF5B49740894E9E9A9 (void);
// 0x00000390 System.Void Sirenix.Utilities.MemberAliasFieldInfo::.ctor(System.Reflection.FieldInfo,System.String)
extern void MemberAliasFieldInfo__ctor_mB40B94A27B797989B6799216FDE198D1CAF0DD8C (void);
// 0x00000391 System.Void Sirenix.Utilities.MemberAliasFieldInfo::.ctor(System.Reflection.FieldInfo,System.String,System.String)
extern void MemberAliasFieldInfo__ctor_m2B58D97EDF42FC0467155E297A87E5B8CA41C111 (void);
// 0x00000392 System.Reflection.FieldInfo Sirenix.Utilities.MemberAliasFieldInfo::get_AliasedField()
extern void MemberAliasFieldInfo_get_AliasedField_m7DDA1AB57676F8357EFB968A0DE75D74EC39686F (void);
// 0x00000393 System.Reflection.Module Sirenix.Utilities.MemberAliasFieldInfo::get_Module()
extern void MemberAliasFieldInfo_get_Module_m2D9A35414C033D89BE42774A870E6155EFD45574 (void);
// 0x00000394 System.Int32 Sirenix.Utilities.MemberAliasFieldInfo::get_MetadataToken()
extern void MemberAliasFieldInfo_get_MetadataToken_mDFED6E7B70F50B7D5E5F62D594FF2C6BEBF45CB8 (void);
// 0x00000395 System.String Sirenix.Utilities.MemberAliasFieldInfo::get_Name()
extern void MemberAliasFieldInfo_get_Name_mD9DCAE9401F3552138D22D26A9DBC2B20F6F9469 (void);
// 0x00000396 System.Type Sirenix.Utilities.MemberAliasFieldInfo::get_DeclaringType()
extern void MemberAliasFieldInfo_get_DeclaringType_m92A34CC84A9A2733E5AE0A24A9B931B731FF62E9 (void);
// 0x00000397 System.Type Sirenix.Utilities.MemberAliasFieldInfo::get_ReflectedType()
extern void MemberAliasFieldInfo_get_ReflectedType_m30F5CC156F02778034EA240F69DE29CCD44D8AC7 (void);
// 0x00000398 System.Type Sirenix.Utilities.MemberAliasFieldInfo::get_FieldType()
extern void MemberAliasFieldInfo_get_FieldType_mB065F56D91197BC967ECE3CB619FF38960A3F426 (void);
// 0x00000399 System.RuntimeFieldHandle Sirenix.Utilities.MemberAliasFieldInfo::get_FieldHandle()
extern void MemberAliasFieldInfo_get_FieldHandle_m013F2C3396273A684043CEDC663DB0906DAC7DD0 (void);
// 0x0000039A System.Reflection.FieldAttributes Sirenix.Utilities.MemberAliasFieldInfo::get_Attributes()
extern void MemberAliasFieldInfo_get_Attributes_m0A70DECCBB0537039B7D1D15EF640531005BD1EB (void);
// 0x0000039B System.Object[] Sirenix.Utilities.MemberAliasFieldInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasFieldInfo_GetCustomAttributes_m99C9EC518382663BD7FCF7F19DE969DBECA16A35 (void);
// 0x0000039C System.Object[] Sirenix.Utilities.MemberAliasFieldInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasFieldInfo_GetCustomAttributes_mA385F71A3A3E27327E3A330D46494D95B51739B3 (void);
// 0x0000039D System.Boolean Sirenix.Utilities.MemberAliasFieldInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasFieldInfo_IsDefined_mAB0C1E86683B91C6083EE022873B17ABD1ED4335 (void);
// 0x0000039E System.Object Sirenix.Utilities.MemberAliasFieldInfo::GetValue(System.Object)
extern void MemberAliasFieldInfo_GetValue_mA3797C3E58CDFFC606AD0D04D5C3A3E001D088AE (void);
// 0x0000039F System.Void Sirenix.Utilities.MemberAliasFieldInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern void MemberAliasFieldInfo_SetValue_m9335DBBC0AAC64070C73D49A5699085E211B85D9 (void);
// 0x000003A0 System.Void Sirenix.Utilities.MemberAliasMethodInfo::.ctor(System.Reflection.MethodInfo,System.String)
extern void MemberAliasMethodInfo__ctor_m5C120BB77C445D56129B080BABE64ACE35978F7C (void);
// 0x000003A1 System.Void Sirenix.Utilities.MemberAliasMethodInfo::.ctor(System.Reflection.MethodInfo,System.String,System.String)
extern void MemberAliasMethodInfo__ctor_m0E48B94FB60F47B4494591EE0966929EAC5CA8F6 (void);
// 0x000003A2 System.Reflection.MethodInfo Sirenix.Utilities.MemberAliasMethodInfo::get_AliasedMethod()
extern void MemberAliasMethodInfo_get_AliasedMethod_m686D08A6E01223A8464E8FA3B49CFEF464D47FDA (void);
// 0x000003A3 System.Reflection.ICustomAttributeProvider Sirenix.Utilities.MemberAliasMethodInfo::get_ReturnTypeCustomAttributes()
extern void MemberAliasMethodInfo_get_ReturnTypeCustomAttributes_m44B87420DE3635422CBDBDEB0444DB40DF0626BD (void);
// 0x000003A4 System.RuntimeMethodHandle Sirenix.Utilities.MemberAliasMethodInfo::get_MethodHandle()
extern void MemberAliasMethodInfo_get_MethodHandle_m865E9D79713CD191E21A6A435B6031D764CD9CEE (void);
// 0x000003A5 System.Reflection.MethodAttributes Sirenix.Utilities.MemberAliasMethodInfo::get_Attributes()
extern void MemberAliasMethodInfo_get_Attributes_m5DB3EE822C4236B0B466C10F02CE8E0BDD033132 (void);
// 0x000003A6 System.Type Sirenix.Utilities.MemberAliasMethodInfo::get_ReturnType()
extern void MemberAliasMethodInfo_get_ReturnType_m64B77507200DD592408B07E9453C20BF37F36CC1 (void);
// 0x000003A7 System.Type Sirenix.Utilities.MemberAliasMethodInfo::get_DeclaringType()
extern void MemberAliasMethodInfo_get_DeclaringType_mD82F53D6B718FC6B941B909A2B202F367B756F96 (void);
// 0x000003A8 System.String Sirenix.Utilities.MemberAliasMethodInfo::get_Name()
extern void MemberAliasMethodInfo_get_Name_m154D685F08F6DF50C6BC2F99DD31B0E2DA5F264C (void);
// 0x000003A9 System.Type Sirenix.Utilities.MemberAliasMethodInfo::get_ReflectedType()
extern void MemberAliasMethodInfo_get_ReflectedType_m18318ECE8A12CCBBD10677D7FDCBBDD4B2B049FB (void);
// 0x000003AA System.Reflection.MethodInfo Sirenix.Utilities.MemberAliasMethodInfo::GetBaseDefinition()
extern void MemberAliasMethodInfo_GetBaseDefinition_mA7181BE13BC69C97E5742A20D52BC010292FB4B0 (void);
// 0x000003AB System.Object[] Sirenix.Utilities.MemberAliasMethodInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasMethodInfo_GetCustomAttributes_mF98EDB155F705488811172B5F060534F970EDF56 (void);
// 0x000003AC System.Object[] Sirenix.Utilities.MemberAliasMethodInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasMethodInfo_GetCustomAttributes_mAFD26E349D475AE49939BAC959E35AD285B902CA (void);
// 0x000003AD System.Reflection.MethodImplAttributes Sirenix.Utilities.MemberAliasMethodInfo::GetMethodImplementationFlags()
extern void MemberAliasMethodInfo_GetMethodImplementationFlags_m7770FD10FB38079EF71F179BF869C3082470AE8D (void);
// 0x000003AE System.Reflection.ParameterInfo[] Sirenix.Utilities.MemberAliasMethodInfo::GetParameters()
extern void MemberAliasMethodInfo_GetParameters_mE65642E0A794518EE0E78EF30A3C3FB0DF8CA518 (void);
// 0x000003AF System.Object Sirenix.Utilities.MemberAliasMethodInfo::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasMethodInfo_Invoke_m78238ABAC6928977EF9836F7FCB39F641A3004FB (void);
// 0x000003B0 System.Boolean Sirenix.Utilities.MemberAliasMethodInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasMethodInfo_IsDefined_mD7FFB50281B1C166CE9DE3318350C2A115E983C9 (void);
// 0x000003B1 System.Void Sirenix.Utilities.MemberAliasPropertyInfo::.ctor(System.Reflection.PropertyInfo,System.String)
extern void MemberAliasPropertyInfo__ctor_mEE4490D36B7678F6EE76EC18C08652F0B8DE847F (void);
// 0x000003B2 System.Void Sirenix.Utilities.MemberAliasPropertyInfo::.ctor(System.Reflection.PropertyInfo,System.String,System.String)
extern void MemberAliasPropertyInfo__ctor_m8EA045F034717576708CC9CF453CE3FA7C480BF4 (void);
// 0x000003B3 System.Reflection.PropertyInfo Sirenix.Utilities.MemberAliasPropertyInfo::get_AliasedProperty()
extern void MemberAliasPropertyInfo_get_AliasedProperty_mC9E7814DDB1C26687E30EA73572DBEAD55CAA7D2 (void);
// 0x000003B4 System.Reflection.Module Sirenix.Utilities.MemberAliasPropertyInfo::get_Module()
extern void MemberAliasPropertyInfo_get_Module_mAEACC738E3D9983AF76EDFAF63027BA4B2F0A78A (void);
// 0x000003B5 System.Int32 Sirenix.Utilities.MemberAliasPropertyInfo::get_MetadataToken()
extern void MemberAliasPropertyInfo_get_MetadataToken_m44E2F1C9EB8C49253700F7CB700A978B3538E66D (void);
// 0x000003B6 System.String Sirenix.Utilities.MemberAliasPropertyInfo::get_Name()
extern void MemberAliasPropertyInfo_get_Name_m9F94DB2BF9C3867642EB3CDBF95EA65A2CCAC93C (void);
// 0x000003B7 System.Type Sirenix.Utilities.MemberAliasPropertyInfo::get_DeclaringType()
extern void MemberAliasPropertyInfo_get_DeclaringType_m0AEC61E0920D1BF7756AF92C63F3A13DE3043223 (void);
// 0x000003B8 System.Type Sirenix.Utilities.MemberAliasPropertyInfo::get_ReflectedType()
extern void MemberAliasPropertyInfo_get_ReflectedType_m3CFAF3C575722B04A84B7F24F97EE26AD71B4B99 (void);
// 0x000003B9 System.Type Sirenix.Utilities.MemberAliasPropertyInfo::get_PropertyType()
extern void MemberAliasPropertyInfo_get_PropertyType_m3B0AE172590EF484AE85F212357110025707DFA6 (void);
// 0x000003BA System.Reflection.PropertyAttributes Sirenix.Utilities.MemberAliasPropertyInfo::get_Attributes()
extern void MemberAliasPropertyInfo_get_Attributes_m6F42A06887A0D60BF2EE10E14725D9E0DEC8D097 (void);
// 0x000003BB System.Boolean Sirenix.Utilities.MemberAliasPropertyInfo::get_CanRead()
extern void MemberAliasPropertyInfo_get_CanRead_mE7C796925EE5EAE6E8663FD17DBEFD73C1987126 (void);
// 0x000003BC System.Boolean Sirenix.Utilities.MemberAliasPropertyInfo::get_CanWrite()
extern void MemberAliasPropertyInfo_get_CanWrite_mDF8119040E2F33B3152DDF5D6E43A328973953ED (void);
// 0x000003BD System.Object[] Sirenix.Utilities.MemberAliasPropertyInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasPropertyInfo_GetCustomAttributes_m69C98922100A23C115975FF7FB9F1D3A435146FE (void);
// 0x000003BE System.Object[] Sirenix.Utilities.MemberAliasPropertyInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasPropertyInfo_GetCustomAttributes_mDEAA3C030D8AFC1333496A951FF23DF931DBB0E9 (void);
// 0x000003BF System.Boolean Sirenix.Utilities.MemberAliasPropertyInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasPropertyInfo_IsDefined_m1FCE2DFF438F0B5F3FA976F9BD57A906047AC546 (void);
// 0x000003C0 System.Reflection.MethodInfo[] Sirenix.Utilities.MemberAliasPropertyInfo::GetAccessors(System.Boolean)
extern void MemberAliasPropertyInfo_GetAccessors_mCCA638E597DB6CD5A8C2300D24757611845392DF (void);
// 0x000003C1 System.Reflection.MethodInfo Sirenix.Utilities.MemberAliasPropertyInfo::GetGetMethod(System.Boolean)
extern void MemberAliasPropertyInfo_GetGetMethod_mF99889ED5EDCBE8C15D098511D4C08CE19FAC046 (void);
// 0x000003C2 System.Reflection.ParameterInfo[] Sirenix.Utilities.MemberAliasPropertyInfo::GetIndexParameters()
extern void MemberAliasPropertyInfo_GetIndexParameters_m26581A6AAAA3239A2AF86178CF6257BA8923EA45 (void);
// 0x000003C3 System.Reflection.MethodInfo Sirenix.Utilities.MemberAliasPropertyInfo::GetSetMethod(System.Boolean)
extern void MemberAliasPropertyInfo_GetSetMethod_mFF2F3511464A2B75132562D492418E095994577B (void);
// 0x000003C4 System.Object Sirenix.Utilities.MemberAliasPropertyInfo::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasPropertyInfo_GetValue_m78A1D3E99E3E1879B2158888DF8D34784D5611D5 (void);
// 0x000003C5 System.Void Sirenix.Utilities.MemberAliasPropertyInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasPropertyInfo_SetValue_mC34BA6631E92FB0CA41CCB23CFF21DB54FFE6E60 (void);
// 0x000003C6 System.Void Sirenix.Utilities.ProjectPathFinder::.ctor()
extern void ProjectPathFinder__ctor_mD719533FB64B579ACA432D3E4B5765509E37D913 (void);
// 0x000003C7 System.Void Sirenix.Utilities.SirenixAssetPaths::.cctor()
extern void SirenixAssetPaths__cctor_m8CEAB5B2079CBA000DF1CC17A07408A24BCEBE8F (void);
// 0x000003C8 System.String Sirenix.Utilities.SirenixAssetPaths::ToPathSafeString(System.String,System.Char)
extern void SirenixAssetPaths_ToPathSafeString_mA0D65164C62C9304FF1E2ABDEAD8C739481DD002 (void);
// 0x000003C9 System.Void Sirenix.Utilities.SirenixAssetPaths/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m91AC12760A968BFE56A3CC485478EA0C4F719B5B (void);
// 0x000003CA System.Char Sirenix.Utilities.SirenixAssetPaths/<>c__DisplayClass12_0::<ToPathSafeString>b__0(System.Char)
extern void U3CU3Ec__DisplayClass12_0_U3CToPathSafeStringU3Eb__0_m13A0C3A16E21CBABA119BDF15C630DA52D51A1EE (void);
// 0x000003CB System.String Sirenix.Utilities.SirenixBuildNameAttribute::get_BuildName()
extern void SirenixBuildNameAttribute_get_BuildName_mF45EE0EB16B5E298B342C707C4BD5E00D24ECE8E (void);
// 0x000003CC System.Void Sirenix.Utilities.SirenixBuildNameAttribute::set_BuildName(System.String)
extern void SirenixBuildNameAttribute_set_BuildName_mA4D04890D1D89B974C22E1B19FD9268570B8E078 (void);
// 0x000003CD System.Void Sirenix.Utilities.SirenixBuildNameAttribute::.ctor(System.String)
extern void SirenixBuildNameAttribute__ctor_mDC1ABF132213764E8BB295319A20B8CC6417622D (void);
// 0x000003CE System.String Sirenix.Utilities.SirenixBuildVersionAttribute::get_Version()
extern void SirenixBuildVersionAttribute_get_Version_m44D7DCC05E81DEC96439A765FAEC8DD2E883C28F (void);
// 0x000003CF System.Void Sirenix.Utilities.SirenixBuildVersionAttribute::set_Version(System.String)
extern void SirenixBuildVersionAttribute_set_Version_mD6B19A36745950F52774F9EEF74E021C9E04249A (void);
// 0x000003D0 System.Void Sirenix.Utilities.SirenixBuildVersionAttribute::.ctor(System.String)
extern void SirenixBuildVersionAttribute__ctor_m288B5DD4D3A90D77C5325CCA6ECEC479502AE8C8 (void);
// 0x000003D1 System.Void Sirenix.Utilities.SirenixEditorConfigAttribute::.ctor()
extern void SirenixEditorConfigAttribute__ctor_m196D21A3E2AAA2EB38E9032E1102774FF6A8DD54 (void);
// 0x000003D2 System.Void Sirenix.Utilities.SirenixGlobalConfigAttribute::.ctor()
extern void SirenixGlobalConfigAttribute__ctor_mA553D13722B6480120C37FC17EE0478676C7D317 (void);
// 0x000003D3 System.String Sirenix.Utilities.StringUtilities::NicifyByteSize(System.Int32,System.Int32)
extern void StringUtilities_NicifyByteSize_mEE8617CE865EA091761E7C48FB093A93AFE46ED6 (void);
// 0x000003D4 System.Boolean Sirenix.Utilities.StringUtilities::FastEndsWith(System.String,System.String)
extern void StringUtilities_FastEndsWith_mA9B2FEC333CBFAF6EFADAABC1E6E27E4B8134D68 (void);
// 0x000003D5 System.Void Sirenix.Utilities.UnityVersion::.cctor()
extern void UnityVersion__cctor_m7BE53E133480B4A3D722E17B103DB4C4B85E7C6F (void);
// 0x000003D6 System.Void Sirenix.Utilities.UnityVersion::EnsureLoaded()
extern void UnityVersion_EnsureLoaded_m35CA01CA335803318DE771A95E8C214B3A841345 (void);
// 0x000003D7 System.Boolean Sirenix.Utilities.UnityVersion::IsVersionOrGreater(System.Int32,System.Int32)
extern void UnityVersion_IsVersionOrGreater_mD26669BFBE793B8AD5DBF4E38B43AC489F72CABD (void);
// 0x000003D8 System.Boolean Sirenix.Utilities.ReferenceEqualityComparer`1::Equals(T,T)
// 0x000003D9 System.Int32 Sirenix.Utilities.ReferenceEqualityComparer`1::GetHashCode(T)
// 0x000003DA System.Void Sirenix.Utilities.ReferenceEqualityComparer`1::.ctor()
// 0x000003DB System.Void Sirenix.Utilities.ReferenceEqualityComparer`1::.cctor()
// 0x000003DC T[] Sirenix.Utilities.Unsafe.UnsafeUtilities::StructArrayFromBytes(System.Byte[],System.Int32)
// 0x000003DD T[] Sirenix.Utilities.Unsafe.UnsafeUtilities::StructArrayFromBytes(System.Byte[],System.Int32,System.Int32)
// 0x000003DE System.Byte[] Sirenix.Utilities.Unsafe.UnsafeUtilities::StructArrayToBytes(T[])
// 0x000003DF System.Byte[] Sirenix.Utilities.Unsafe.UnsafeUtilities::StructArrayToBytes(T[],System.Byte[]&,System.Int32)
// 0x000003E0 System.String Sirenix.Utilities.Unsafe.UnsafeUtilities::StringFromBytes(System.Byte[],System.Int32,System.Boolean)
extern void UnsafeUtilities_StringFromBytes_mBFF855E4F6AABB7E23FA4332C9DE93CCA7DB1F06 (void);
// 0x000003E1 System.Int32 Sirenix.Utilities.Unsafe.UnsafeUtilities::StringToBytes(System.Byte[],System.String,System.Boolean)
extern void UnsafeUtilities_StringToBytes_m978FC7DEDB3ED485314FF1F0C56C7FCBB48531E2 (void);
// 0x000003E2 System.Void Sirenix.Utilities.Unsafe.UnsafeUtilities::MemoryCopy(System.Object,System.Object,System.Int32,System.Int32,System.Int32)
extern void UnsafeUtilities_MemoryCopy_mB7D2CB3BE64A52881964A72902ABEFC147697269 (void);
static Il2CppMethodPointer s_methodPointers[994] = 
{
	ColorExtensions_Lerp_m9889C6B728D39820E6B193DBFF1E3F7B094167C4,
	ColorExtensions_MoveTowards_mA4FC1B1A0D4E73C9582A603442CF90F303688987,
	ColorExtensions_TryParseString_mEF98BBAA6B1B6083DBF029FB450B3974127E8C30,
	ColorExtensions_ToCSharpColor_mAB3E86EFCCD979A2A9EDD3A7CC909003D12F16DB,
	ColorExtensions_Pow_mAA9F009DF4579636872558B8C413716B5A699F1C,
	ColorExtensions_NormalizeRGB_mA7D50C50232CDB4E393640BC60C4AE1CE71DC2B1,
	ColorExtensions_TrimFloat_mBA88D820423EF634F4F2902B8DE1D225DDD6EDF7,
	ColorExtensions__cctor_mFD96C17056331628FF551669C4DF46DBBDEE9A65,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FieldInfoExtensions_IsAliasField_m6845C750241C884522A12B3DA22744346CCA3E46,
	FieldInfoExtensions_DeAliasField_m0E168E8E55F57E758C73B014393C579F393AB179,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MemberInfoExtensions_GetAttributes_m15AAE26BE285B269CAE126FB926279E0A826E987,
	MemberInfoExtensions_GetAttributes_m2FFD9512045ED25E441863CA105281FB2C5E0B9F,
	MemberInfoExtensions_GetNiceName_m233C26DA9440A5BD00027679176E7A1207811170,
	MemberInfoExtensions_IsStatic_m0B82DFA6FA5B65EF48F2D3B2799A342F48353F7E,
	MemberInfoExtensions_IsAlias_mA690365F38B68B1AB5041F68D0A6995FD6DADD57,
	MemberInfoExtensions_DeAlias_m6C0863F27B3C8362850BF8B960A7AE376D425D68,
	MemberInfoExtensions_SignaturesAreEqual_mFFAA6FB976110B9E1B5CFB81EC23FFA528363A26,
	MethodInfoExtensions_GetFullName_m2B1D1CF37601A3B322FEA19473EEAB83BE9322C8,
	MethodInfoExtensions_GetParamsNames_mE7B9162DDC988D41D65DD34E54A7C29839C38F58,
	MethodInfoExtensions_GetFullName_m5324B40A2D0B23F9F4989566B50AB7EE99F88A47,
	MethodInfoExtensions_IsExtensionMethod_m7796506EB32D9061808BF565AEDC681C66EDDBED,
	MethodInfoExtensions_IsAliasMethod_mB15831FE197849EA54CA5E0826EE7EE29275A01E,
	MethodInfoExtensions_DeAliasMethod_mF8824D0D8741B82891672059C62A31876EF1C900,
	PathUtilities_GetDirectoryName_m4D874AE3875B253A3B44DEA2763CC2D260901890,
	PathUtilities_HasSubDirectory_mD034F326CD0D7AF99E6F0EFE93550D684CB0A652,
	PathUtilities_FindParentDirectoryWithName_mE67A19F3CB8AA227659DDD7C5F7958C36E1378A3,
	PathUtilities_CanMakeRelative_m45418D5B6BDD5E886719A845076E2C70DA3A60F5,
	PathUtilities_MakeRelative_mAF0FD824193DACF09321255C6E76E89554327178,
	PathUtilities_TryMakeRelative_m0A7838E666713CBEAAB3691E0162FDFFF4CA824E,
	PathUtilities_Combine_m45D096DDDF2FD232BD42283DE55F968A2B699D8B,
	PropertyInfoExtensions_IsAutoProperty_m17D6F1D04D64078B40A4DFC8DB2678E0C88B4863,
	PropertyInfoExtensions_IsAliasProperty_m26C4A48FCE446591C97B7BF2BE9F4716559AB645,
	PropertyInfoExtensions_DeAliasProperty_m5420C600E36D84EB08C57C666C3080A09C191CED,
	RectExtensions_SetWidth_mC853B7063708E8C35014ACD1210681C80A1BCB47,
	RectExtensions_SetHeight_mF6D4E394172A8BF3335A949463B4D7031E0BD5BB,
	RectExtensions_SetSize_mD39716901BD09A67A29FF813F535B0415F09DD3D,
	RectExtensions_SetSize_mA7B1826F31962FB667C60F5291929BD2D779411F,
	RectExtensions_HorizontalPadding_m8E8E3A615990471E3586A9BB20DDB018DB909EB0,
	RectExtensions_HorizontalPadding_m89B3727FB917838AF9D05AFF14D389517798BDA0,
	RectExtensions_VerticalPadding_m5C4CCF230E193C556128BF8481F1FBBC167D67AF,
	RectExtensions_VerticalPadding_m590B82183DB97091EDA9E0C994F54C7D64613C05,
	RectExtensions_Padding_mFB407178B4A570B3E8AD035E7C25C1E1F5431F55,
	RectExtensions_Padding_mDA5649C8E3559871C74BA5D72FA1B690F7107486,
	RectExtensions_Padding_m32E39A56FBB4E5707FB0B008A1E0E59CAB187C79,
	RectExtensions_AlignLeft_m614D187B42334A09858A7F7D393538FE7FEB0889,
	RectExtensions_AlignCenter_m3E956AC4B8A25F9474FCA34A34E53F84D45CEEB2,
	RectExtensions_AlignCenter_m9B01538E9E37E34EE70A05A6F776BFE9B8DE6184,
	RectExtensions_AlignRight_m574DBF16789091EAA5DBA75D1D3763EF34C046BA,
	RectExtensions_AlignRight_m05BE19656017535D2F9E004CB946C36C94FB98F9,
	RectExtensions_AlignTop_m6D8F9DE46371856A5B061230B35C6571CE5A617C,
	RectExtensions_AlignMiddle_mC5E340F841C52962FCBA450A79F86B76C5AE2886,
	RectExtensions_AlignBottom_mBEACA9FC17D0F71F94305F734A21582880214A9B,
	RectExtensions_AlignCenterX_m69C778A0DABE4398737D79D19860AA1FCF428037,
	RectExtensions_AlignCenterY_m7641F9F0AA5704BD40BDDA8B2096C4252686FB08,
	RectExtensions_AlignCenterXY_mF1D830A1A24F00608669BA7ACE783E0C407FE6A4,
	RectExtensions_AlignCenterXY_m3F714CE8FECD8AAD7E1D6CDF276850C7E5FF95C8,
	RectExtensions_Expand_mABE02EAE258191B576ECFADFABDE0DFFF8B9177C,
	RectExtensions_Expand_m81781A1F595C5F5CA55814EE85555D0B2A912233,
	RectExtensions_Expand_m72D5AC60DDE7B7C771FDFD9663F927DC7D91C08B,
	RectExtensions_Split_m97F4A1E73113DB65F4C04D542747DF3E92DB10A8,
	RectExtensions_SplitVertical_m939E05A9013EE2CD669EC050750E468957F50BB0,
	RectExtensions_SplitGrid_m6916D7314A9130D72E3DFBE3D9DC81A69218E90C,
	RectExtensions_SplitTableGrid_mEE23BBAEBD316E42C0731267C95488D9381BDD5A,
	RectExtensions_SetCenterX_m8654D5E6F3D90DFAB618CDE1ABFC4BFB34349A5D,
	RectExtensions_SetCenterY_mD217F8DB58C73B90DF3D0519F3E21387898B0E33,
	RectExtensions_SetCenter_m5D32A84E8B2B0A5AB1190ECD3D2362104E12836E,
	RectExtensions_SetCenter_m9B56A386F413A3ABCD25D840E2E1990D1CEAB2AD,
	RectExtensions_SetPosition_m35513C3D84420597C326DB9F71B1E53ADE6B86F7,
	RectExtensions_ResetPosition_mB818EF964F8D1E78385E0D479D7286DD1A53C140,
	RectExtensions_AddPosition_m1D21D830B6D383D35D05666220FEF082A13A9AFD,
	RectExtensions_AddPosition_m029ADEE6AAC59E615BB356040108B633E2E4F60F,
	RectExtensions_SetX_m00477B241A08B949553D77C69BBCA5A322F22A35,
	RectExtensions_AddX_mAC5D747A732603B30BE1BA06C3BB209FB542B266,
	RectExtensions_SubX_m0AC8AB5C0B438C5D03CD0E51895858EC0B890718,
	RectExtensions_SetY_m08E7C979CC840E57214E979DF30C08F5ADD24B02,
	RectExtensions_AddY_mC0A509D0023FC36DF2AAA3702E463850B73C0415,
	RectExtensions_SubY_m0776E2852F6DACCBA11D7472D7D1AA76F51C6057,
	RectExtensions_SetMin_m68BC6C0D21F9F0558B957515260B08CFE44832B5,
	RectExtensions_AddMin_m2860A1F53A2FF827A6EAAC51570CB198DA547564,
	RectExtensions_SubMin_mE28419D690ADD2516463FA44BCB7C86EAA04346B,
	RectExtensions_SetMax_m855BDDE7EAEC02DB394244E15E5F54A045FAE316,
	RectExtensions_AddMax_mD7B96050B949AFA58E950D9EEF27E722B013ADA4,
	RectExtensions_SubMax_m8B41E31D6702D6E3F91DED8E2AD8D707EA1B882C,
	RectExtensions_SetXMin_mEB4BB8CCD0C42D6078F892475907D18190174627,
	RectExtensions_AddXMin_mBF12C4963D04484010D0B3039FB59E19ABD3F39F,
	RectExtensions_SubXMin_m834AF0100D5BF61C3D7FC980C4B85A63D5C04CFE,
	RectExtensions_SetXMax_m3079E8E87F725074D0C38FB308C7205E0AB03985,
	RectExtensions_AddXMax_m00BCE6D7A36B8B9D5CFEC8AAFCE6032917A6DB31,
	RectExtensions_SubXMax_m60AF370F697312E645E8F30F0CF99B8BB93E851C,
	RectExtensions_SetYMin_m11A869747613438D095F20A504AA0989F6C0718A,
	RectExtensions_AddYMin_m4124C8B31491D21DF6B930295B712C60AE2E32A6,
	RectExtensions_SubYMin_mBFFAC829D2AD00C17749B310642E403BFFD63CC6,
	RectExtensions_SetYMax_mC70AF8DCBFB45C4FC72CEE1E8085DF5F5DB7B61B,
	RectExtensions_AddYMax_mA63EBA978081E7C2CED937F2B563AD1F28CBDA2A,
	RectExtensions_SubYMax_m51DB920309335CD0C3A7D629016E7C48418DAFDF,
	RectExtensions_MinWidth_m77CAE2D37F322D97EE565DBD6F33224ABCAD0DBE,
	RectExtensions_MaxWidth_m6D39F9B0FC8CE7ACFDE83CD74805C15ABFEB1AB6,
	RectExtensions_MinHeight_m8B4B10F23FBA6A890FE5B8F5B6154FE7BF3AF330,
	RectExtensions_MaxHeight_mE30E285546FD1051FCD4B826B0A539447E022252,
	RectExtensions_ExpandTo_mB98847B1A65AF6FEB80FE423BE4F29707004647D,
	StringExtensions_ToTitleCase_mA8A1408F983DDE057FF2DC283C24A931599C25C0,
	StringExtensions_Contains_m8AB548F34460F21E93D1F70FF99B15246F4A8D2E,
	StringExtensions_SplitPascalCase_m5539F652FCECED016F2AC168F3FB04FD54205163,
	StringExtensions_IsNullOrWhitespace_mEE67BC64AC520011BDC8485A9CF67819CEDC6F2D,
	TypeExtensions_GetCachedNiceName_m5117FF5BB8BEE5CECB334268738338EB459CC3BC,
	TypeExtensions_CreateNiceName_m1A225A46571812F45ED50EA207016068A0A8223D,
	TypeExtensions_HasCastDefined_m2119B5754B8D819AA028BCD56C3AAF744B4F4960,
	TypeExtensions_IsValidIdentifier_mDE74E73508BEAAEE954948E88D01BEBA5A6FB424,
	TypeExtensions_IsValidIdentifierStartCharacter_m02CD46B4D1CD9661EA0117D032FD260ACE80941B,
	TypeExtensions_IsValidIdentifierPartCharacter_m1246D10BE585C5D7C17F8CDF089C2867472ABF78,
	TypeExtensions_IsCastableTo_mD75B14DD15DD5B1CC04062319CFBC91E22CDC85A,
	TypeExtensions_GetCastMethodDelegate_m2861121E8A7BB7BB3761C01E8FAD5F1FF94D7C51,
	NULL,
	TypeExtensions_GetCastMethod_mF07C9F8F7ED2C41152EB2C564246358694A4F587,
	NULL,
	NULL,
	TypeExtensions_ImplementsOrInherits_mE14EC3D52E1F1029666B799F1333A70BB4EDA868,
	TypeExtensions_ImplementsOpenGenericType_m1121DC0D840999A914E5574F3F0AD4B26CF8796A,
	TypeExtensions_ImplementsOpenGenericInterface_m2F341DAEABDED76559A85A8D20B8550D98305795,
	TypeExtensions_ImplementsOpenGenericClass_m0A41ECA8B4244F7ED2BE35D1F971CE6B286F4112,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m0753CF4AB6F4C0C028B12A66C68035E133CA6825,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m916A7D9B2DD86FAB8764EC572D18A3AE44F1E0B7,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m465C12014C9C4C4ED14FFA0CA5351375A09D2C32,
	TypeExtensions_GetOperatorMethod_m7E05D3B4C3D95C62BA8886DCDC060FEC3EF6C152,
	TypeExtensions_GetOperatorMethods_mB76C9B9565203292047CF82412370E8FD33F959C,
	TypeExtensions_GetAllMembers_mD0E5106F949AD8BC0BC4095BCF4C67050CC859A8,
	TypeExtensions_GetAllMembers_m3562615FFFE9EE27CC2D5D0193EAD3C6FFC8F9E3,
	NULL,
	TypeExtensions_GetGenericBaseType_m94A62E4BA96C8F4FC5A4F465D14B416468B51F91,
	TypeExtensions_GetGenericBaseType_m39F7468A0FF37BB99A5A13025FC2523F0DD1CCB3,
	TypeExtensions_GetBaseTypes_mC637233D7ECF7D9C89B70AA1D0C218844C07A964,
	TypeExtensions_GetBaseClasses_m62C3445D92A27770261AF5FA50BFCABAAC2D014D,
	TypeExtensions_TypeNameGauntlet_mED6BB694ABAD8B721B26010774C2AF0F36CEE16C,
	TypeExtensions_GetNiceName_mDDEEDB5E0EE8C831C50FF8D56DB2383EE063DCDD,
	TypeExtensions_GetNiceFullName_mA2DD7054D6BB866DAE63AFAE786D93BA6DF76DEC,
	TypeExtensions_GetCompilableNiceName_m339A581D1A09CE239F39F0B892C3AA46DA92574C,
	TypeExtensions_GetCompilableNiceFullName_m14D75762F69A7E7753EFDAA2DE4A71F7CF2CA868,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeExtensions_InheritsFrom_m9CA98952CA2EF39E0DF4BD9E68773E46B6501471,
	TypeExtensions_GetInheritanceDistance_m2BC5795354997E6D0A208131F755C2629F26431B,
	TypeExtensions_HasParamaters_m71D6B60C5F128516E7DD8CA2F9FA4C81988E4659,
	TypeExtensions_GetReturnType_mEFB54C016DF12930DE9D99E351D0A67F4C0A872E,
	TypeExtensions_GetMemberValue_m92007B8251F5EFAD94286371D20C634600BD7253,
	TypeExtensions_SetMemberValue_m19EA7B3165678F36D133C6877A405938BCE728ED,
	TypeExtensions_TryInferGenericParameters_m8B86E7145860BD05F5BA87566D034CAEF046E46F,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_m5979A23DB5D7BB878F43406689A1D1E222A84182,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_m68E1A8190F47993BDD581D414D13C509579F4F43,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_mA8571C660CC62428F444FB79A2C61725B0CA201E,
	TypeExtensions_GenericParameterIsFulfilledBy_m6F871B1B8B2427ED49D7FD1A4E0BBC13E0125750,
	TypeExtensions_GenericParameterIsFulfilledBy_mBB6572A82E174FF80D5A9AE454EE8CA6FFCF777F,
	TypeExtensions_GetGenericConstraintsString_m74F940F9A005E758932D975A47318666A68B2215,
	TypeExtensions_GetGenericParameterConstraintsString_m6C4C94289FAA168F8C83862ED43A80276BD4E1B3,
	TypeExtensions_GenericArgumentsContainsTypes_mB5405BEBB46D9D14834B1FEFD389AAD72052552E,
	TypeExtensions_IsFullyConstructedGenericType_m941C7E4175FEB639347AD717A85AD04D6A53CE38,
	TypeExtensions_IsNullableType_m28FB5D9FB77956D303CF4009FE8E4C62D896822B,
	TypeExtensions_GetEnumBitmask_mD9D4FB7B88E81C06793063E7DBC7FD27D127F1EE,
	TypeExtensions_IsCSharpKeyword_m40A516DF6A056550F38E3785C0E7D8254E38DD26,
	TypeExtensions_SafeGetTypes_mB12AC54B4D81C926F95EABC7EABB6622A57C98F8,
	TypeExtensions_SafeIsDefined_m278C59D57D1127B3783333879C5298B6DFDC2F0A,
	TypeExtensions_SafeGetCustomAttributes_m23EC650659FE27E327171260426F92C9CD5EBF8C,
	TypeExtensions__cctor_m26D3724DDE253A689F3136C88B81B28A33860F2C,
	U3CU3Ec__DisplayClass22_0__ctor_mDF9AE9AAADF55D825811BA0DD1F8D3588FF619A4,
	U3CU3Ec__DisplayClass22_0_U3CGetCastMethodDelegateU3Eb__0_m21622DFE58C7ABF4C3930CCCAB57EC7DB1DE3503,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass29_0__ctor_m5305362E4FC2ACA20EF345E4D2916A7ACC726DC1,
	U3CU3Ec__DisplayClass29_0_U3CImplementsOpenGenericInterfaceU3Eb__0_m1D8CE446643405FA6A9979A20CA7BFE19E0A444F,
	U3CU3Ec__DisplayClass34_0__ctor_mB285E443BBE7E2E9F04F0731124D8AF23857C22E,
	U3CU3Ec__DisplayClass34_0_U3CGetOperatorMethodU3Eb__0_m68FE6B0B00C3875D84A4D6F028C8D67BC03AA612,
	U3CU3Ec__DisplayClass35_0__ctor_m640B8F0B7744ACC6F6351B1C003DF77F3074675A,
	U3CU3Ec__DisplayClass35_0_U3CGetOperatorMethodsU3Eb__0_m9B7A6AC703BB914AD88D46239BCCEBC866FB9B29,
	U3CGetAllMembersU3Ed__36__ctor_m93329B6BB738335150EA8633AC9A62064DE2AE5B,
	U3CGetAllMembersU3Ed__36_System_IDisposable_Dispose_m696D89A5F3311080407B13BFD1C6131D488FA40F,
	U3CGetAllMembersU3Ed__36_MoveNext_m4FCF8C5BEA63510C0183FEA7BE58D0327621C63E,
	U3CGetAllMembersU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mC6C31374620C0677C696479727C2B46974130EE4,
	U3CGetAllMembersU3Ed__36_System_Collections_IEnumerator_Reset_m7A9247214D92CC94632DF7CBC99660DDCDB73CC3,
	U3CGetAllMembersU3Ed__36_System_Collections_IEnumerator_get_Current_m33DEDC31C961C26AA43C6CD836286AE5C2EB1809,
	U3CGetAllMembersU3Ed__36_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mC498D8F9D92C86C5CB7CB8EFA91B0A1B4FBF4B2A,
	U3CGetAllMembersU3Ed__36_System_Collections_IEnumerable_GetEnumerator_mEF0676FE290DE50AA21460A696AC5585D6959DB3,
	U3CGetAllMembersU3Ed__37__ctor_m5C0B1BB7706836620920BF0378663D60442ACED9,
	U3CGetAllMembersU3Ed__37_System_IDisposable_Dispose_m122701CE24AE448A557DA668F815115D765395F5,
	U3CGetAllMembersU3Ed__37_MoveNext_mB9A482F8A1B5A1E75080E571609CA9A336DAE46C,
	U3CGetAllMembersU3Ed__37_U3CU3Em__Finally1_m8207B7B15172021158A4B28C61209EB8D2AA2E40,
	U3CGetAllMembersU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m033546A216735300A541C93881903001625A7A27,
	U3CGetAllMembersU3Ed__37_System_Collections_IEnumerator_Reset_mCECE57E55D6393810F72C45E36309904E806F9ED,
	U3CGetAllMembersU3Ed__37_System_Collections_IEnumerator_get_Current_mBDFA5AAE955CF87CFDC232725522A37BFC16CDCE,
	U3CGetAllMembersU3Ed__37_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mF7674D670646237FF8B332360649656FE5395034,
	U3CGetAllMembersU3Ed__37_System_Collections_IEnumerable_GetEnumerator_mFE43504BBBC411400F6B50970E44F082B2FF5B17,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CGetBaseClassesU3Ed__42__ctor_mD2A7BF94397412C36C8F81C32F7608F4DD5481AB,
	U3CGetBaseClassesU3Ed__42_System_IDisposable_Dispose_m4BA19A27C774612AA6AAD5AE4A740F3665F113B5,
	U3CGetBaseClassesU3Ed__42_MoveNext_mA92B84163AF60E2B8D9B25E916BF3F3080AE0CDC,
	U3CGetBaseClassesU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m739E1E100840478C780CD1C037EBE5DC5ED381A4,
	U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerator_Reset_m322C061B956B87A6E42700C1BDDC602F29CC2469,
	U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerator_get_Current_mEFAE5547493200A0CF71A7EDC76005CC5FF74D16,
	U3CGetBaseClassesU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_mE8BF5192BF980759DFDDBE1DA4AD0CF2C5C03DA7,
	U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerable_GetEnumerator_m6578B926D0606CA6B4A8A0FED583288405B3EBAF,
	UnityExtensions__cctor_mBBF8D69E0EF646AC4DF9161FE4A98EB10EAD7AD7,
	UnityExtensions_SafeIsUnityNull_m9609D5559427508D80B7A9FFAE9DE8DA1C36A3E6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DeepReflection_CreateWeakStaticValueGetter_m9E29876353579969056AD7A924B953B358B27085,
	DeepReflection_CreateWeakInstanceValueGetter_m1AE87BCB3800E63A1AA3DC8D4C7B0A58B7A3B437,
	DeepReflection_CreateWeakInstanceValueSetter_mFF808ADEE7A3B1EF1A351067BCDEB71A100CFCC6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DeepReflection_CreateEmittedDeepValueGetterDelegate_mFF689FCD50A5FBB229D1BF2D50A90B6CBBB40884,
	DeepReflection_CreateSlowDeepStaticValueGetterDelegate_mF38DBBD010C6A2BCD55D5CA3A6A6361647AE3312,
	DeepReflection_CreateSlowDeepInstanceValueGetterDelegate_mEE74497E8303E28B7318D06A4BAF01C889775757,
	DeepReflection_CreateSlowDeepInstanceValueSetterDelegate_mD50C6511D6AE5152B2C4FA7338627FDEB69F6154,
	DeepReflection_SlowGetMemberValue_m26CC015099061413CDEF889DD94E47BBBD207016,
	DeepReflection_SlowSetMemberValue_m73DC204731C29C010BB4079417E8C11DC9785629,
	DeepReflection_GetMemberPath_m2A2FAF730F7B086CA6A5FB47E6F4E83A64778A42,
	DeepReflection_GetStepMember_m76C214F64F184C8F2E07AA048B568A05F4146B5D,
	DeepReflection__cctor_m20523DCA6AFD21927B9C503F25BA2B6176AD2E07,
	PathStep__ctor_m2BC57C1154B7159F6A09789BFD93793E05D78C79_AdjustorThunk,
	PathStep__ctor_mB4EA450A87475972D4F86C8DD2B765208447FA8F_AdjustorThunk,
	PathStep__ctor_mC572D4729D575556442308729E1279799FCDA53B_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass21_0__ctor_m326BEA86A72EFDF74C923DC57EFDC17BA7D348C5,
	U3CU3Ec__DisplayClass21_0_U3CCreateSlowDeepStaticValueGetterDelegateU3Eb__0_m835B4ABB788D4A44854B18F602861C97B4254F5F,
	U3CU3Ec__DisplayClass22_0__ctor_m0F5A5768B9013489AB743A64BAC61DEED4096383,
	U3CU3Ec__DisplayClass22_0_U3CCreateSlowDeepInstanceValueGetterDelegateU3Eb__0_m4CCEDACB659C316DCA75197FD6FBDB6331805BDB,
	U3CU3Ec__DisplayClass23_0__ctor_mB930061FD4DE76C35FDA2E03E963C13AB7BB9458,
	U3CU3Ec__DisplayClass23_0_U3CCreateSlowDeepInstanceValueSetterDelegateU3Eb__0_m6444863B8002BB520DA75B61B5485DA7C899F2DE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WeakValueGetter__ctor_mCF0428BEC5349653D7E71B478AC65D80B1FA350D,
	WeakValueGetter_Invoke_m0A7E1880F1C7A0D758E5B9A3E242CA701D4E45F3,
	WeakValueGetter_BeginInvoke_m713888D810ECDD3FBE40D6891FC63863F4181F50,
	WeakValueGetter_EndInvoke_m191201DBBFA1901AD71996713F6F9385EB0EFCE3,
	WeakValueSetter__ctor_mF8E82670751C0974BC53C7C95BE27D98F0CD682F,
	WeakValueSetter_Invoke_m965159ED7A86D5269F5C19F8F7A37F470C3767DD,
	WeakValueSetter_BeginInvoke_m36C5647D16BBF4D5D42D2F30B1DEBFFAF76F5142,
	WeakValueSetter_EndInvoke_mE76BD25B432FF9C6E3A9D9803419619EBBB571C3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmitUtilities_get_CanEmit_m831F45D116F130774C81E2281CC48A66E6D031E1,
	NULL,
	EmitUtilities_CreateWeakStaticFieldGetter_m590B8963414FF8ED20402CF81290DCB75378E0B6,
	NULL,
	EmitUtilities_CreateWeakStaticFieldSetter_m3691FE02BC70F8F2D560E31FFE7A2A5DAD26AD76,
	NULL,
	NULL,
	EmitUtilities_CreateWeakInstanceFieldGetter_mB0C0450066FCF1737A61EF7DEC132CB2CF88E391,
	NULL,
	NULL,
	EmitUtilities_CreateWeakInstanceFieldSetter_mBA836299B3D5C427760871875AB6116D24DCBC83,
	EmitUtilities_CreateWeakInstancePropertyGetter_mB2F6169975D95EAAA26D2C99E73A1990CF586207,
	EmitUtilities_CreateWeakInstancePropertySetter_m08F9D0D8EF1F896D3F2CFA2E42B1E94A5CA8D021,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmitUtilities_CreateStaticMethodCaller_mC7372959C42FA3246EFF8F187B288301DE7E0458,
	NULL,
	EmitUtilities_CreateWeakInstanceMethodCaller_m6FE17D0A6A23374135174753FF048E8287C35D4D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass3_0__ctor_mB9B335E229E5DE3D4A9456E6C98610498AC2CB76,
	U3CU3Ec__DisplayClass3_0_U3CCreateWeakStaticFieldGetterU3Eb__0_m10E424384DD20AAFF821563E0EF52925B6A1B6C9,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass5_0__ctor_m5EF22D44DB96EF2A055FE8D70AA33D747A99F961,
	U3CU3Ec__DisplayClass5_0_U3CCreateWeakStaticFieldSetterU3Eb__0_m24F92E5D14E6947C5B792E6BAE5432A8D003A81E,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass8_0__ctor_m7A54C06418E3AA81FC4083D9E49EB32C825AAF0B,
	U3CU3Ec__DisplayClass8_0_U3CCreateWeakInstanceFieldGetterU3Eb__0_m210C7902B0325C3D098FB545319F1BDD915A9CFF,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass11_0__ctor_m5DFC9A0E842CD030F03952EB2914B9726E4723AD,
	U3CU3Ec__DisplayClass11_0_U3CCreateWeakInstanceFieldSetterU3Eb__0_m1D1BC4E6AAC385CD4EA32C5F91D7E72A129E8EDC,
	U3CU3Ec__DisplayClass12_0__ctor_m3861E9A645F9AFA6125A39F2279CEA82ED9E9BD1,
	U3CU3Ec__DisplayClass12_0_U3CCreateWeakInstancePropertyGetterU3Eb__0_m7B15E6DE814E5A07D6BA2701DD41533FF65C8185,
	U3CU3Ec__DisplayClass13_0__ctor_mF4F9ABDE00AAB0DBDF8B5437F4B870B19DF83049,
	U3CU3Ec__DisplayClass13_0_U3CCreateWeakInstancePropertySetterU3Eb__0_mD6DE6ED471F42C506ACDBF8B767B84281925CC03,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass21_0__ctor_m2A449559E198BACEDF8695063003000996AD8667,
	U3CU3Ec__DisplayClass21_0_U3CCreateWeakInstanceMethodCallerU3Eb__0_m14B7A3F705494452C9FFAD7BA1D8D11223E4997D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GlobalConfigAttribute_get_FullPath_m56136FEF1B3488B8A53049E84006E71B9386A31F,
	GlobalConfigAttribute_get_AssetPath_m24AD80D23594B2E1811CF9E991359AAB8D0E5639,
	GlobalConfigAttribute_get_AssetPathWithAssetsPrefix_m8672F9E844036A985D11791D1AA1E4D0F8FE8D8D,
	GlobalConfigAttribute_get_AssetPathWithoutAssetsPrefix_mCDE6F775DB99CF4A1E88CCC090C4D072B4E36487,
	GlobalConfigAttribute_get_ResourcesPath_m143963C31BEE08369F9EEA4FC7EABB7C96A8749A,
	GlobalConfigAttribute_get_UseAsset_m6DE933FA318D235196AAD7C3A182DB581BFB526C,
	GlobalConfigAttribute_set_UseAsset_m98E3100E2A2BFBE9C311C1DDA29DEBEE438F73C7,
	GlobalConfigAttribute_get_IsInResourcesFolder_mC40EB7730EAC10B07754825D5B1072BDDB035E6D,
	GlobalConfigAttribute__ctor_mB2985C6FEF081EF9B750E38966D7ACE652EE7875,
	GlobalConfigAttribute__ctor_m339998A57E405779FA1C91A99DAFDC88A7216956,
	GUILayoutOptions__cctor_mD785819DD9B2E37FB089ED9F7F50E4E2EBCFF14A,
	GUILayoutOptions_Width_mB6165E9E9DE8E5AA5BFA7A38241532A4B1144EAB,
	GUILayoutOptions_Height_mFEE3D67D07DDF96C52B9BB615D6382C931A6EE33,
	GUILayoutOptions_MaxHeight_mC775EB75874DCA1BFEF222FB5EEC0B3FA0FF33D7,
	GUILayoutOptions_MaxWidth_m9AC74528ECD413E457482EF1826B876A84EAB2E1,
	GUILayoutOptions_MinWidth_m09A60F7BDEA20AFBB11CD4CBE319395E6DAFEEBF,
	GUILayoutOptions_MinHeight_m3DE57D9BB5F890F35351044C9AD440ABBF1E50F0,
	GUILayoutOptions_ExpandHeight_m09B6463BDF36A2517FCB52C983CCB6527A9E2B4C,
	GUILayoutOptions_ExpandWidth_m6D2FD838A01289CBC26B918B14B505D09E51549F,
	GUILayoutOptionsInstance_GetCachedOptions_mEEF48175DAA16E14CBA97B51405611B8920169A3,
	GUILayoutOptionsInstance_op_Implicit_mAB100A4821FAB6820406C0ADB7CCEF354E77F595,
	GUILayoutOptionsInstance_CreateOptionsArary_m0B31D88A8BFF2DA52A675AE9857B633AAC60AD9A,
	GUILayoutOptionsInstance_Clone_m3EFA9A2E9822DCBF20A117118BEE3F4C066FFEAC,
	GUILayoutOptionsInstance__ctor_m5DFC01FE3A4CFEF16A3019B81A863701B1A3AD8A,
	GUILayoutOptionsInstance_Width_m85CC356D425BA379404829E4FE72E3F26D7A468C,
	GUILayoutOptionsInstance_Height_m13712C3BDC953B1FFDC750F823C2DA24D10725F7,
	GUILayoutOptionsInstance_MaxHeight_mC887BE759283C4D02CD7C3692F92E31E6700F643,
	GUILayoutOptionsInstance_MaxWidth_mF632FF1F7AE7F79EE4A79EEE272BE5E8614EB5EC,
	GUILayoutOptionsInstance_MinHeight_mF8F6A50F008400329D30E8BC3EEABDFD8CDF91EA,
	GUILayoutOptionsInstance_MinWidth_m0D14EBEE1B546132792DC5D467D6ABE2DE44AFD3,
	GUILayoutOptionsInstance_ExpandHeight_mC84470587A8CA7B1B44E653FF782525BEE5E32DE,
	GUILayoutOptionsInstance_ExpandWidth_m1C87C0C6EEF46B3B002029722E292F828FD9C005,
	GUILayoutOptionsInstance_SetValue_m27B465B91BA7B5C75EEC41BC91D051C990BA7DEA,
	GUILayoutOptionsInstance_SetValue_m28E473A986E3B648CCBE637630F97A0B8C1452BA,
	GUILayoutOptionsInstance_Equals_mE1A35AFEDADF232034198E494F897534613020EF,
	GUILayoutOptionsInstance_GetHashCode_mD5E640741B42B6497310376AF108321D6CD43B19,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ImmutableList__ctor_m39E18EC34C30F4D0D843ADF08E31EE3D180FE93E,
	ImmutableList_get_Count_m5FBCEA74E297F5642FBFB00D223521C513528A06,
	ImmutableList_get_IsFixedSize_m4DF4081E30B8BAB12EDF20EF081478591D0C16D8,
	ImmutableList_get_IsReadOnly_m75101378E544D6EF75181C615D1DDDB2FB1277B5,
	ImmutableList_get_IsSynchronized_m8CDEB5875D93BE84EFAE735970EB900A2649B248,
	ImmutableList_get_SyncRoot_mA0B584308DBFDC0C4E2830D76F759F97CD3EDA8F,
	ImmutableList_System_Collections_IList_get_Item_m55D326F5A70AEE224DA4D7D514539B9740FAE931,
	ImmutableList_System_Collections_IList_set_Item_m1E617DC22474DA5D9A686A315E1E359DD9044399,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_get_Item_mF2A632CD4E30C7EC4ECD2E3E879D3497EE847828,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_set_Item_m007828EFF133BC523DF4ACBA8084E3787F3E61FA,
	ImmutableList_get_Item_m787FB54661CABD3F78D8504A1002EE27F86FE80E,
	ImmutableList_Contains_mB083BE1CB81A28F60B43DB6F430DCDAF03436DF9,
	ImmutableList_CopyTo_m05DD1848FBFB6CAD73892A0ED6DD4A893E99E0E4,
	ImmutableList_CopyTo_mC44E04AAC0C145317F40DC8D1A3C0F1F9CA9A037,
	ImmutableList_GetEnumerator_mC79EA52A900E5F7B337A7D11AA2AD9F0F1F4959C,
	ImmutableList_System_Collections_IEnumerable_GetEnumerator_mC6D564CB551420271658CB179A3A2733B1DA26E5,
	ImmutableList_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m1010893F030A95F0EEB87BA7C63B01ACB9B668DB,
	ImmutableList_System_Collections_IList_Add_m98C464AA93D9950892BBC7B131DF5C66843B8A78,
	ImmutableList_System_Collections_IList_Clear_m108D9F519719557FAF1CBD94BC83785800DEF018,
	ImmutableList_System_Collections_IList_Insert_m6B42020078E798621501ECBB63425D8E2E2876E6,
	ImmutableList_System_Collections_IList_Remove_mF2194611D723AE664D5215D7A7815A8284D2BF78,
	ImmutableList_System_Collections_IList_RemoveAt_mDCEFF0BF6D57EEEFAB0C4A422076228F2F9C83A2,
	ImmutableList_IndexOf_m0162CC7BA84711C8450B6559CE45E613CD881B65,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_RemoveAt_m4C292994C2ED7563B62F9CF70002D3B08D888172,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_Insert_m3114EAE42C28413944710D5C11075C8D2137A1E6,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Add_m9AD5516FCE1091EF3ED319374B8887F5EE471D2E,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Clear_m3058E54B8F188B94072325E458D9B0A2CBD33C54,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Remove_m24426B9BDB5E3BC79635A5C284B9CF5CDBD3CAA5,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_mF71247C0669D13A3CCBEB9DDC43CB91FB13D7D2F,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m55A31EA0E7CD9D1B866BF0EC395E0D0523335C36,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_m731B20D72279F09A16AF1CB093A5A5A89C7E6282,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_mACE1DFA00F386EE79CBB7EC031E251C18D6FED17,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5986C856A9B9449B39B542589647440252B70DAD,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m67F6137A2C821A8BF580B221DFAEF24E71C3B334,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m81047B92D0044C262E5451C3215F00295380FA9F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MathUtilities_PointDistanceToLine_m3F554EC70E178606D2CF582897B3415A62594D61,
	MathUtilities_Hermite_m0D70F719A680774029563E749582A021A497C4E1,
	MathUtilities_StackHermite_m4B92CCA398BF837A923D8160E413B657E244A187,
	MathUtilities_Fract_mE517258AEBE08DDB37603A3F1AAA3539AFA3D4CA,
	MathUtilities_Fract_m5F92E5CE4D517AB3FCA4A10492EBA0BD6EA57365,
	MathUtilities_Fract_mFE9C0746BB5D12A21999CCF5EE9B732A11FE1483,
	MathUtilities_BounceEaseInFastOut_m3EADC42038524125F048E6198D8F0D33628B6805,
	MathUtilities_Hermite01_mB4D592A73B9AC0FCA325F942D25765E4C1120BF1,
	MathUtilities_StackHermite01_mE8C0C1131FF1DA2A0248E911AD92F19FA5DA4D5F,
	MathUtilities_LerpUnclamped_mBE76F49534B8F3B138A2C2151F0F079E89312BD4,
	MathUtilities_LerpUnclamped_mCF23D00760B283A52BC8C353860EB93CEDFC1AA8,
	MathUtilities_Bounce_m5CCECE6C207736CFDD2058C8C3FA7E9F5F343D6E,
	MathUtilities_EaseInElastic_m410D85600ADD89A0126359436BB788DCCC637E99,
	MathUtilities_Pow_mC169808695BB5B77859857E6BCBD620A9FEC4CD4,
	MathUtilities_Abs_m550FC9B831F3A442BA2C40A369CA77288DDDDC72,
	MathUtilities_Sign_m94D2E6964F9EDEE8BAE9CBCE6BB33BE61F5FBD2F,
	MathUtilities_EaseOutElastic_mE7015FC8F2F5162AF5D7159B470DD19BEC3B0B60,
	MathUtilities_EaseInOut_m135BBA69AB4672EBCAAE67701BFCE06B74ED7462,
	MathUtilities_Clamp_mE99524BFDD189E0F3773572F7CFCC895B616C478,
	MathUtilities_Clamp_m284B89E4861746A1509A44273C4F3282FDA6244F,
	MathUtilities_ComputeByteArrayHash_m4824945708DF79A71584BB8649144C26166C4947,
	MathUtilities_InterpolatePoints_m245F80A9A411407C79BC3F3CB619133EB14FCD30,
	MathUtilities_LineIntersectsLine_m9F8CA7DBBE814DB2837518DF14F3AC4E32D033D7,
	MathUtilities_InfiniteLineIntersect_m4B711DD7013033D94B18A614ED72F64796D071E4,
	MathUtilities_LineDistToPlane_m35DB57E189AE76C3CF1C0D6A891559FC8D99A095,
	MathUtilities_RayDistToPlane_mABFCB75B12BA48DBFD403604747BBABBBC82129C,
	MathUtilities_RotatePoint_m3253CF8A6E71D2DAC872D644054F6963690C5861,
	MathUtilities_RotatePoint_mE700DC535AA7E22EA16F5ECBCF0C8ADA0ED3BAF0,
	MathUtilities_SmoothStep_m59E460497C38E96DE6EB7C7096B3A4E4F29467D6,
	MathUtilities_LinearStep_m0CB3CCFC198A1DD8332A13878D1110A6993E5058,
	MathUtilities_Wrap_mD735D6F28AE2B21C6E5D19988D887511B10F98F8,
	MathUtilities_Wrap_mE0E213A0AEF97B1F63ABA4946D7CE93C3490ED5C,
	MathUtilities_Wrap_m463BB6C601F15E09AD6BF312F00210FDE7ABC936,
	MathUtilities_RoundBasedOnMinimumDifference_m2C50FB893F79415626106E7BF7B2E7E07840D809,
	MathUtilities_DiscardLeastSignificantDecimal_m94AEF71D8B171CB0A3AA58F2D38C7DB42B47506F,
	MathUtilities_ClampWrapAngle_m61C38865D587300A672113CD6CDF437C3571E417,
	MathUtilities_GetNumberOfDecimalsForMinimumDifference_mF03ACD71B58F28AC8EE1885CBD49F6B2A25B8AB0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PersistentAssemblyAttribute__ctor_mB4B2296B1BA32B7F368DCAEF5B49740894E9E9A9,
	MemberAliasFieldInfo__ctor_mB40B94A27B797989B6799216FDE198D1CAF0DD8C,
	MemberAliasFieldInfo__ctor_m2B58D97EDF42FC0467155E297A87E5B8CA41C111,
	MemberAliasFieldInfo_get_AliasedField_m7DDA1AB57676F8357EFB968A0DE75D74EC39686F,
	MemberAliasFieldInfo_get_Module_m2D9A35414C033D89BE42774A870E6155EFD45574,
	MemberAliasFieldInfo_get_MetadataToken_mDFED6E7B70F50B7D5E5F62D594FF2C6BEBF45CB8,
	MemberAliasFieldInfo_get_Name_mD9DCAE9401F3552138D22D26A9DBC2B20F6F9469,
	MemberAliasFieldInfo_get_DeclaringType_m92A34CC84A9A2733E5AE0A24A9B931B731FF62E9,
	MemberAliasFieldInfo_get_ReflectedType_m30F5CC156F02778034EA240F69DE29CCD44D8AC7,
	MemberAliasFieldInfo_get_FieldType_mB065F56D91197BC967ECE3CB619FF38960A3F426,
	MemberAliasFieldInfo_get_FieldHandle_m013F2C3396273A684043CEDC663DB0906DAC7DD0,
	MemberAliasFieldInfo_get_Attributes_m0A70DECCBB0537039B7D1D15EF640531005BD1EB,
	MemberAliasFieldInfo_GetCustomAttributes_m99C9EC518382663BD7FCF7F19DE969DBECA16A35,
	MemberAliasFieldInfo_GetCustomAttributes_mA385F71A3A3E27327E3A330D46494D95B51739B3,
	MemberAliasFieldInfo_IsDefined_mAB0C1E86683B91C6083EE022873B17ABD1ED4335,
	MemberAliasFieldInfo_GetValue_mA3797C3E58CDFFC606AD0D04D5C3A3E001D088AE,
	MemberAliasFieldInfo_SetValue_m9335DBBC0AAC64070C73D49A5699085E211B85D9,
	MemberAliasMethodInfo__ctor_m5C120BB77C445D56129B080BABE64ACE35978F7C,
	MemberAliasMethodInfo__ctor_m0E48B94FB60F47B4494591EE0966929EAC5CA8F6,
	MemberAliasMethodInfo_get_AliasedMethod_m686D08A6E01223A8464E8FA3B49CFEF464D47FDA,
	MemberAliasMethodInfo_get_ReturnTypeCustomAttributes_m44B87420DE3635422CBDBDEB0444DB40DF0626BD,
	MemberAliasMethodInfo_get_MethodHandle_m865E9D79713CD191E21A6A435B6031D764CD9CEE,
	MemberAliasMethodInfo_get_Attributes_m5DB3EE822C4236B0B466C10F02CE8E0BDD033132,
	MemberAliasMethodInfo_get_ReturnType_m64B77507200DD592408B07E9453C20BF37F36CC1,
	MemberAliasMethodInfo_get_DeclaringType_mD82F53D6B718FC6B941B909A2B202F367B756F96,
	MemberAliasMethodInfo_get_Name_m154D685F08F6DF50C6BC2F99DD31B0E2DA5F264C,
	MemberAliasMethodInfo_get_ReflectedType_m18318ECE8A12CCBBD10677D7FDCBBDD4B2B049FB,
	MemberAliasMethodInfo_GetBaseDefinition_mA7181BE13BC69C97E5742A20D52BC010292FB4B0,
	MemberAliasMethodInfo_GetCustomAttributes_mF98EDB155F705488811172B5F060534F970EDF56,
	MemberAliasMethodInfo_GetCustomAttributes_mAFD26E349D475AE49939BAC959E35AD285B902CA,
	MemberAliasMethodInfo_GetMethodImplementationFlags_m7770FD10FB38079EF71F179BF869C3082470AE8D,
	MemberAliasMethodInfo_GetParameters_mE65642E0A794518EE0E78EF30A3C3FB0DF8CA518,
	MemberAliasMethodInfo_Invoke_m78238ABAC6928977EF9836F7FCB39F641A3004FB,
	MemberAliasMethodInfo_IsDefined_mD7FFB50281B1C166CE9DE3318350C2A115E983C9,
	MemberAliasPropertyInfo__ctor_mEE4490D36B7678F6EE76EC18C08652F0B8DE847F,
	MemberAliasPropertyInfo__ctor_m8EA045F034717576708CC9CF453CE3FA7C480BF4,
	MemberAliasPropertyInfo_get_AliasedProperty_mC9E7814DDB1C26687E30EA73572DBEAD55CAA7D2,
	MemberAliasPropertyInfo_get_Module_mAEACC738E3D9983AF76EDFAF63027BA4B2F0A78A,
	MemberAliasPropertyInfo_get_MetadataToken_m44E2F1C9EB8C49253700F7CB700A978B3538E66D,
	MemberAliasPropertyInfo_get_Name_m9F94DB2BF9C3867642EB3CDBF95EA65A2CCAC93C,
	MemberAliasPropertyInfo_get_DeclaringType_m0AEC61E0920D1BF7756AF92C63F3A13DE3043223,
	MemberAliasPropertyInfo_get_ReflectedType_m3CFAF3C575722B04A84B7F24F97EE26AD71B4B99,
	MemberAliasPropertyInfo_get_PropertyType_m3B0AE172590EF484AE85F212357110025707DFA6,
	MemberAliasPropertyInfo_get_Attributes_m6F42A06887A0D60BF2EE10E14725D9E0DEC8D097,
	MemberAliasPropertyInfo_get_CanRead_mE7C796925EE5EAE6E8663FD17DBEFD73C1987126,
	MemberAliasPropertyInfo_get_CanWrite_mDF8119040E2F33B3152DDF5D6E43A328973953ED,
	MemberAliasPropertyInfo_GetCustomAttributes_m69C98922100A23C115975FF7FB9F1D3A435146FE,
	MemberAliasPropertyInfo_GetCustomAttributes_mDEAA3C030D8AFC1333496A951FF23DF931DBB0E9,
	MemberAliasPropertyInfo_IsDefined_m1FCE2DFF438F0B5F3FA976F9BD57A906047AC546,
	MemberAliasPropertyInfo_GetAccessors_mCCA638E597DB6CD5A8C2300D24757611845392DF,
	MemberAliasPropertyInfo_GetGetMethod_mF99889ED5EDCBE8C15D098511D4C08CE19FAC046,
	MemberAliasPropertyInfo_GetIndexParameters_m26581A6AAAA3239A2AF86178CF6257BA8923EA45,
	MemberAliasPropertyInfo_GetSetMethod_mFF2F3511464A2B75132562D492418E095994577B,
	MemberAliasPropertyInfo_GetValue_m78A1D3E99E3E1879B2158888DF8D34784D5611D5,
	MemberAliasPropertyInfo_SetValue_mC34BA6631E92FB0CA41CCB23CFF21DB54FFE6E60,
	ProjectPathFinder__ctor_mD719533FB64B579ACA432D3E4B5765509E37D913,
	SirenixAssetPaths__cctor_m8CEAB5B2079CBA000DF1CC17A07408A24BCEBE8F,
	SirenixAssetPaths_ToPathSafeString_mA0D65164C62C9304FF1E2ABDEAD8C739481DD002,
	U3CU3Ec__DisplayClass12_0__ctor_m91AC12760A968BFE56A3CC485478EA0C4F719B5B,
	U3CU3Ec__DisplayClass12_0_U3CToPathSafeStringU3Eb__0_m13A0C3A16E21CBABA119BDF15C630DA52D51A1EE,
	SirenixBuildNameAttribute_get_BuildName_mF45EE0EB16B5E298B342C707C4BD5E00D24ECE8E,
	SirenixBuildNameAttribute_set_BuildName_mA4D04890D1D89B974C22E1B19FD9268570B8E078,
	SirenixBuildNameAttribute__ctor_mDC1ABF132213764E8BB295319A20B8CC6417622D,
	SirenixBuildVersionAttribute_get_Version_m44D7DCC05E81DEC96439A765FAEC8DD2E883C28F,
	SirenixBuildVersionAttribute_set_Version_mD6B19A36745950F52774F9EEF74E021C9E04249A,
	SirenixBuildVersionAttribute__ctor_m288B5DD4D3A90D77C5325CCA6ECEC479502AE8C8,
	SirenixEditorConfigAttribute__ctor_m196D21A3E2AAA2EB38E9032E1102774FF6A8DD54,
	SirenixGlobalConfigAttribute__ctor_mA553D13722B6480120C37FC17EE0478676C7D317,
	StringUtilities_NicifyByteSize_mEE8617CE865EA091761E7C48FB093A93AFE46ED6,
	StringUtilities_FastEndsWith_mA9B2FEC333CBFAF6EFADAABC1E6E27E4B8134D68,
	UnityVersion__cctor_m7BE53E133480B4A3D722E17B103DB4C4B85E7C6F,
	UnityVersion_EnsureLoaded_m35CA01CA335803318DE771A95E8C214B3A841345,
	UnityVersion_IsVersionOrGreater_mD26669BFBE793B8AD5DBF4E38B43AC489F72CABD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnsafeUtilities_StringFromBytes_mBFF855E4F6AABB7E23FA4332C9DE93CCA7DB1F06,
	UnsafeUtilities_StringToBytes_m978FC7DEDB3ED485314FF1F0C56C7FCBB48531E2,
	UnsafeUtilities_MemoryCopy_mB7D2CB3BE64A52881964A72902ABEFC147697269,
};
static const int32_t s_InvokerIndices[994] = 
{
	2050,
	1846,
	2173,
	2354,
	2048,
	2280,
	2369,
	2458,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2385,
	2127,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2363,
	2127,
	2363,
	2385,
	2385,
	2127,
	2176,
	2126,
	2363,
	2363,
	2385,
	2385,
	2127,
	2363,
	2176,
	2126,
	2176,
	2126,
	1953,
	2126,
	2385,
	2385,
	2127,
	2142,
	2142,
	1935,
	2143,
	2142,
	1935,
	2142,
	1935,
	2142,
	1935,
	1639,
	2142,
	2142,
	1935,
	2142,
	1934,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	1935,
	2142,
	1935,
	1639,
	1933,
	1933,
	1768,
	1767,
	2142,
	2142,
	1935,
	2143,
	2143,
	2376,
	2143,
	1935,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2143,
	2143,
	2143,
	2143,
	2143,
	2143,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2142,
	2143,
	2363,
	1954,
	2363,
	2385,
	2363,
	2363,
	1956,
	2385,
	2381,
	2381,
	1956,
	1922,
	-1,
	1922,
	-1,
	-1,
	2176,
	2176,
	2176,
	2176,
	2126,
	2126,
	2126,
	2123,
	2123,
	2123,
	1919,
	-1,
	2126,
	1918,
	2127,
	2127,
	2363,
	2363,
	2363,
	2363,
	2363,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2176,
	2092,
	1956,
	2363,
	2126,
	2032,
	1947,
	2176,
	2176,
	2176,
	2176,
	1794,
	2127,
	2127,
	2176,
	2385,
	2385,
	2102,
	2385,
	2363,
	1956,
	1922,
	2458,
	1476,
	1000,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1476,
	1101,
	1476,
	1101,
	1476,
	1101,
	1231,
	1476,
	1459,
	1440,
	1476,
	1440,
	1440,
	1440,
	1231,
	1476,
	1459,
	1476,
	1440,
	1476,
	1440,
	1440,
	1440,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1231,
	1476,
	1459,
	1440,
	1476,
	1440,
	1440,
	1440,
	2458,
	2385,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1758,
	1758,
	1758,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1633,
	2363,
	2363,
	2363,
	2135,
	2040,
	1628,
	1922,
	2458,
	1241,
	1231,
	454,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1476,
	1440,
	1476,
	1000,
	1476,
	789,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	788,
	993,
	362,
	565,
	788,
	663,
	208,
	663,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2452,
	-1,
	2363,
	-1,
	2363,
	-1,
	-1,
	2126,
	-1,
	-1,
	2126,
	2126,
	2126,
	-1,
	-1,
	-1,
	-1,
	-1,
	2363,
	-1,
	2363,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1476,
	1440,
	-1,
	-1,
	1476,
	1241,
	-1,
	-1,
	-1,
	-1,
	1476,
	993,
	-1,
	-1,
	-1,
	-1,
	1476,
	663,
	1476,
	993,
	1476,
	663,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1476,
	1241,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1440,
	1440,
	1440,
	1440,
	1440,
	1459,
	1257,
	1459,
	1476,
	1241,
	2458,
	2369,
	2369,
	2369,
	2369,
	2369,
	2369,
	2368,
	2368,
	1440,
	2363,
	1440,
	1440,
	1476,
	1002,
	1002,
	1002,
	1002,
	1002,
	1002,
	1001,
	1001,
	747,
	745,
	1101,
	1428,
	1476,
	1476,
	-1,
	-1,
	-1,
	-1,
	-1,
	1241,
	1428,
	1459,
	1459,
	1459,
	1440,
	997,
	736,
	997,
	736,
	997,
	1101,
	786,
	786,
	1440,
	1440,
	1440,
	945,
	1476,
	736,
	1241,
	1231,
	945,
	1231,
	736,
	1241,
	1476,
	1101,
	1231,
	1476,
	1459,
	1476,
	1440,
	1476,
	1440,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1971,
	1970,
	1805,
	2401,
	2408,
	2412,
	2401,
	2401,
	2201,
	1977,
	1974,
	2401,
	1970,
	2225,
	2412,
	2412,
	1970,
	2401,
	1978,
	1975,
	2325,
	2221,
	1654,
	1808,
	1806,
	2200,
	2217,
	1974,
	1970,
	1970,
	1853,
	1970,
	1865,
	2062,
	2296,
	1970,
	2319,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1476,
	789,
	488,
	1440,
	1440,
	1428,
	1440,
	1440,
	1440,
	1440,
	1456,
	1428,
	1001,
	581,
	620,
	1000,
	151,
	789,
	488,
	1440,
	1440,
	1457,
	1428,
	1440,
	1440,
	1440,
	1440,
	1440,
	1001,
	581,
	1428,
	1440,
	119,
	620,
	789,
	488,
	1440,
	1440,
	1428,
	1440,
	1440,
	1440,
	1440,
	1428,
	1459,
	1459,
	1001,
	581,
	620,
	1001,
	1001,
	1440,
	1001,
	119,
	91,
	1476,
	2458,
	2122,
	1476,
	890,
	1440,
	1241,
	1241,
	1440,
	1241,
	1241,
	1476,
	1476,
	2117,
	2176,
	2458,
	2458,
	2166,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1916,
	1876,
	1675,
};
static const Il2CppTokenRangePair s_rgctxIndices[168] = 
{
	{ 0x02000004, { 12, 1 } },
	{ 0x02000005, { 13, 3 } },
	{ 0x02000008, { 24, 4 } },
	{ 0x02000009, { 28, 4 } },
	{ 0x0200000A, { 32, 4 } },
	{ 0x0200000B, { 36, 5 } },
	{ 0x0200000D, { 126, 9 } },
	{ 0x0200000E, { 135, 7 } },
	{ 0x0200000F, { 142, 9 } },
	{ 0x02000010, { 151, 8 } },
	{ 0x02000011, { 159, 9 } },
	{ 0x02000012, { 168, 9 } },
	{ 0x02000013, { 177, 8 } },
	{ 0x02000014, { 185, 9 } },
	{ 0x02000015, { 194, 9 } },
	{ 0x02000016, { 203, 8 } },
	{ 0x02000017, { 211, 9 } },
	{ 0x02000018, { 220, 10 } },
	{ 0x02000019, { 230, 9 } },
	{ 0x0200001A, { 239, 10 } },
	{ 0x0200001B, { 249, 9 } },
	{ 0x0200001C, { 258, 8 } },
	{ 0x0200001D, { 266, 9 } },
	{ 0x0200001E, { 275, 9 } },
	{ 0x0200001F, { 284, 8 } },
	{ 0x02000020, { 292, 9 } },
	{ 0x02000021, { 301, 9 } },
	{ 0x02000022, { 310, 8 } },
	{ 0x02000023, { 318, 9 } },
	{ 0x02000024, { 327, 6 } },
	{ 0x0200002E, { 373, 6 } },
	{ 0x02000034, { 379, 4 } },
	{ 0x02000038, { 386, 8 } },
	{ 0x0200003C, { 435, 1 } },
	{ 0x0200003D, { 436, 1 } },
	{ 0x0200003E, { 437, 2 } },
	{ 0x0200003F, { 439, 3 } },
	{ 0x02000040, { 442, 2 } },
	{ 0x02000041, { 444, 2 } },
	{ 0x02000042, { 446, 3 } },
	{ 0x02000043, { 449, 2 } },
	{ 0x02000044, { 451, 2 } },
	{ 0x02000048, { 453, 48 } },
	{ 0x02000051, { 587, 1 } },
	{ 0x02000053, { 588, 1 } },
	{ 0x02000055, { 589, 2 } },
	{ 0x02000056, { 591, 1 } },
	{ 0x02000058, { 592, 3 } },
	{ 0x02000059, { 595, 1 } },
	{ 0x0200005D, { 596, 1 } },
	{ 0x0200005E, { 597, 1 } },
	{ 0x0200005F, { 598, 3 } },
	{ 0x02000060, { 601, 2 } },
	{ 0x02000061, { 603, 1 } },
	{ 0x02000063, { 604, 2 } },
	{ 0x02000064, { 606, 1 } },
	{ 0x02000065, { 607, 2 } },
	{ 0x02000067, { 609, 7 } },
	{ 0x0200006D, { 616, 3 } },
	{ 0x02000072, { 619, 7 } },
	{ 0x02000073, { 626, 14 } },
	{ 0x02000075, { 654, 36 } },
	{ 0x02000085, { 700, 4 } },
	{ 0x06000009, { 0, 5 } },
	{ 0x0600000A, { 5, 7 } },
	{ 0x06000011, { 16, 2 } },
	{ 0x06000012, { 18, 2 } },
	{ 0x06000013, { 20, 2 } },
	{ 0x06000014, { 22, 2 } },
	{ 0x06000029, { 41, 2 } },
	{ 0x0600002A, { 43, 3 } },
	{ 0x0600002B, { 46, 3 } },
	{ 0x0600002C, { 49, 2 } },
	{ 0x0600002D, { 51, 2 } },
	{ 0x0600002E, { 53, 2 } },
	{ 0x0600002F, { 55, 4 } },
	{ 0x06000030, { 59, 2 } },
	{ 0x06000031, { 61, 2 } },
	{ 0x06000032, { 63, 2 } },
	{ 0x06000033, { 65, 2 } },
	{ 0x06000034, { 67, 2 } },
	{ 0x06000035, { 69, 2 } },
	{ 0x06000036, { 71, 2 } },
	{ 0x06000037, { 73, 2 } },
	{ 0x06000038, { 75, 2 } },
	{ 0x06000039, { 77, 2 } },
	{ 0x0600003A, { 79, 2 } },
	{ 0x0600003B, { 81, 2 } },
	{ 0x0600003C, { 83, 2 } },
	{ 0x0600003D, { 85, 2 } },
	{ 0x0600003E, { 87, 2 } },
	{ 0x0600003F, { 89, 2 } },
	{ 0x06000040, { 91, 2 } },
	{ 0x06000041, { 93, 2 } },
	{ 0x06000042, { 95, 2 } },
	{ 0x06000043, { 97, 2 } },
	{ 0x06000044, { 99, 2 } },
	{ 0x06000045, { 101, 2 } },
	{ 0x06000046, { 103, 3 } },
	{ 0x06000047, { 106, 1 } },
	{ 0x06000048, { 107, 2 } },
	{ 0x06000049, { 109, 5 } },
	{ 0x0600004A, { 114, 6 } },
	{ 0x0600004B, { 120, 6 } },
	{ 0x0600012B, { 333, 1 } },
	{ 0x0600012C, { 334, 1 } },
	{ 0x0600012D, { 335, 2 } },
	{ 0x0600012E, { 337, 1 } },
	{ 0x0600012F, { 338, 1 } },
	{ 0x06000130, { 339, 3 } },
	{ 0x06000197, { 342, 4 } },
	{ 0x06000199, { 346, 13 } },
	{ 0x0600019A, { 359, 2 } },
	{ 0x060001A6, { 361, 2 } },
	{ 0x060001B0, { 363, 3 } },
	{ 0x060001B1, { 366, 1 } },
	{ 0x060001B2, { 367, 1 } },
	{ 0x060001B3, { 368, 2 } },
	{ 0x060001B4, { 370, 1 } },
	{ 0x060001B5, { 371, 1 } },
	{ 0x060001B6, { 372, 1 } },
	{ 0x060001FF, { 383, 1 } },
	{ 0x06000200, { 384, 1 } },
	{ 0x06000201, { 385, 1 } },
	{ 0x0600020F, { 394, 6 } },
	{ 0x06000210, { 400, 6 } },
	{ 0x06000211, { 406, 7 } },
	{ 0x06000212, { 413, 3 } },
	{ 0x06000213, { 416, 5 } },
	{ 0x06000214, { 421, 3 } },
	{ 0x06000215, { 424, 3 } },
	{ 0x06000216, { 427, 5 } },
	{ 0x06000217, { 432, 3 } },
	{ 0x0600025F, { 501, 9 } },
	{ 0x06000261, { 510, 5 } },
	{ 0x06000263, { 515, 5 } },
	{ 0x06000264, { 520, 5 } },
	{ 0x06000266, { 525, 5 } },
	{ 0x06000267, { 530, 5 } },
	{ 0x0600026B, { 535, 5 } },
	{ 0x0600026C, { 540, 5 } },
	{ 0x0600026D, { 545, 5 } },
	{ 0x0600026E, { 550, 5 } },
	{ 0x0600026F, { 555, 2 } },
	{ 0x06000271, { 557, 6 } },
	{ 0x06000273, { 563, 7 } },
	{ 0x06000274, { 570, 6 } },
	{ 0x06000275, { 576, 7 } },
	{ 0x06000276, { 583, 2 } },
	{ 0x06000277, { 585, 2 } },
	{ 0x06000335, { 640, 4 } },
	{ 0x06000336, { 644, 5 } },
	{ 0x06000337, { 649, 2 } },
	{ 0x06000338, { 651, 3 } },
	{ 0x06000385, { 690, 1 } },
	{ 0x06000386, { 691, 1 } },
	{ 0x06000387, { 692, 1 } },
	{ 0x06000388, { 693, 1 } },
	{ 0x06000389, { 694, 1 } },
	{ 0x0600038A, { 695, 1 } },
	{ 0x0600038B, { 696, 1 } },
	{ 0x0600038C, { 697, 1 } },
	{ 0x0600038D, { 698, 1 } },
	{ 0x0600038E, { 699, 1 } },
	{ 0x060003DC, { 704, 1 } },
	{ 0x060003DD, { 705, 2 } },
	{ 0x060003DE, { 707, 1 } },
	{ 0x060003DF, { 708, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[709] = 
{
	{ (Il2CppRGCTXDataType)2, 901 },
	{ (Il2CppRGCTXDataType)3, 50 },
	{ (Il2CppRGCTXDataType)3, 51 },
	{ (Il2CppRGCTXDataType)2, 1541 },
	{ (Il2CppRGCTXDataType)3, 4456 },
	{ (Il2CppRGCTXDataType)2, 922 },
	{ (Il2CppRGCTXDataType)3, 145 },
	{ (Il2CppRGCTXDataType)2, 1243 },
	{ (Il2CppRGCTXDataType)3, 2141 },
	{ (Il2CppRGCTXDataType)3, 146 },
	{ (Il2CppRGCTXDataType)2, 1611 },
	{ (Il2CppRGCTXDataType)3, 4550 },
	{ (Il2CppRGCTXDataType)3, 4468 },
	{ (Il2CppRGCTXDataType)3, 2253 },
	{ (Il2CppRGCTXDataType)3, 4557 },
	{ (Il2CppRGCTXDataType)3, 2252 },
	{ (Il2CppRGCTXDataType)2, 2557 },
	{ (Il2CppRGCTXDataType)3, 6283 },
	{ (Il2CppRGCTXDataType)2, 1232 },
	{ (Il2CppRGCTXDataType)3, 2101 },
	{ (Il2CppRGCTXDataType)2, 1240 },
	{ (Il2CppRGCTXDataType)3, 2130 },
	{ (Il2CppRGCTXDataType)2, 1794 },
	{ (Il2CppRGCTXDataType)3, 5041 },
	{ (Il2CppRGCTXDataType)3, 6392 },
	{ (Il2CppRGCTXDataType)3, 3615 },
	{ (Il2CppRGCTXDataType)3, 3614 },
	{ (Il2CppRGCTXDataType)3, 3613 },
	{ (Il2CppRGCTXDataType)3, 4919 },
	{ (Il2CppRGCTXDataType)3, 3612 },
	{ (Il2CppRGCTXDataType)3, 3611 },
	{ (Il2CppRGCTXDataType)3, 3610 },
	{ (Il2CppRGCTXDataType)3, 2260 },
	{ (Il2CppRGCTXDataType)3, 3938 },
	{ (Il2CppRGCTXDataType)3, 3937 },
	{ (Il2CppRGCTXDataType)3, 3936 },
	{ (Il2CppRGCTXDataType)3, 2261 },
	{ (Il2CppRGCTXDataType)3, 3941 },
	{ (Il2CppRGCTXDataType)3, 6176 },
	{ (Il2CppRGCTXDataType)3, 3940 },
	{ (Il2CppRGCTXDataType)3, 3939 },
	{ (Il2CppRGCTXDataType)2, 980 },
	{ (Il2CppRGCTXDataType)3, 468 },
	{ (Il2CppRGCTXDataType)2, 1943 },
	{ (Il2CppRGCTXDataType)2, 2135 },
	{ (Il2CppRGCTXDataType)3, 752 },
	{ (Il2CppRGCTXDataType)2, 1944 },
	{ (Il2CppRGCTXDataType)2, 2136 },
	{ (Il2CppRGCTXDataType)3, 779 },
	{ (Il2CppRGCTXDataType)2, 978 },
	{ (Il2CppRGCTXDataType)3, 454 },
	{ (Il2CppRGCTXDataType)2, 1765 },
	{ (Il2CppRGCTXDataType)3, 4894 },
	{ (Il2CppRGCTXDataType)2, 1766 },
	{ (Il2CppRGCTXDataType)3, 4895 },
	{ (Il2CppRGCTXDataType)2, 2421 },
	{ (Il2CppRGCTXDataType)3, 11018 },
	{ (Il2CppRGCTXDataType)2, 2491 },
	{ (Il2CppRGCTXDataType)3, 5141 },
	{ (Il2CppRGCTXDataType)2, 989 },
	{ (Il2CppRGCTXDataType)3, 525 },
	{ (Il2CppRGCTXDataType)2, 991 },
	{ (Il2CppRGCTXDataType)3, 539 },
	{ (Il2CppRGCTXDataType)2, 993 },
	{ (Il2CppRGCTXDataType)3, 553 },
	{ (Il2CppRGCTXDataType)2, 995 },
	{ (Il2CppRGCTXDataType)3, 569 },
	{ (Il2CppRGCTXDataType)2, 997 },
	{ (Il2CppRGCTXDataType)3, 583 },
	{ (Il2CppRGCTXDataType)2, 999 },
	{ (Il2CppRGCTXDataType)3, 597 },
	{ (Il2CppRGCTXDataType)2, 1001 },
	{ (Il2CppRGCTXDataType)3, 613 },
	{ (Il2CppRGCTXDataType)2, 1003 },
	{ (Il2CppRGCTXDataType)3, 627 },
	{ (Il2CppRGCTXDataType)2, 1005 },
	{ (Il2CppRGCTXDataType)3, 641 },
	{ (Il2CppRGCTXDataType)2, 1007 },
	{ (Il2CppRGCTXDataType)3, 657 },
	{ (Il2CppRGCTXDataType)2, 1009 },
	{ (Il2CppRGCTXDataType)3, 671 },
	{ (Il2CppRGCTXDataType)2, 1011 },
	{ (Il2CppRGCTXDataType)3, 685 },
	{ (Il2CppRGCTXDataType)2, 968 },
	{ (Il2CppRGCTXDataType)3, 380 },
	{ (Il2CppRGCTXDataType)2, 970 },
	{ (Il2CppRGCTXDataType)3, 394 },
	{ (Il2CppRGCTXDataType)2, 972 },
	{ (Il2CppRGCTXDataType)3, 408 },
	{ (Il2CppRGCTXDataType)2, 956 },
	{ (Il2CppRGCTXDataType)3, 292 },
	{ (Il2CppRGCTXDataType)2, 958 },
	{ (Il2CppRGCTXDataType)3, 306 },
	{ (Il2CppRGCTXDataType)2, 960 },
	{ (Il2CppRGCTXDataType)3, 320 },
	{ (Il2CppRGCTXDataType)2, 962 },
	{ (Il2CppRGCTXDataType)3, 336 },
	{ (Il2CppRGCTXDataType)2, 964 },
	{ (Il2CppRGCTXDataType)3, 350 },
	{ (Il2CppRGCTXDataType)2, 966 },
	{ (Il2CppRGCTXDataType)3, 364 },
	{ (Il2CppRGCTXDataType)2, 982 },
	{ (Il2CppRGCTXDataType)3, 482 },
	{ (Il2CppRGCTXDataType)2, 1957 },
	{ (Il2CppRGCTXDataType)2, 2137 },
	{ (Il2CppRGCTXDataType)3, 4896 },
	{ (Il2CppRGCTXDataType)2, 1801 },
	{ (Il2CppRGCTXDataType)2, 1803 },
	{ (Il2CppRGCTXDataType)2, 2424 },
	{ (Il2CppRGCTXDataType)2, 2581 },
	{ (Il2CppRGCTXDataType)3, 6328 },
	{ (Il2CppRGCTXDataType)2, 1958 },
	{ (Il2CppRGCTXDataType)2, 2138 },
	{ (Il2CppRGCTXDataType)2, 1802 },
	{ (Il2CppRGCTXDataType)2, 2583 },
	{ (Il2CppRGCTXDataType)3, 6333 },
	{ (Il2CppRGCTXDataType)3, 6332 },
	{ (Il2CppRGCTXDataType)3, 6334 },
	{ (Il2CppRGCTXDataType)2, 2426 },
	{ (Il2CppRGCTXDataType)2, 1805 },
	{ (Il2CppRGCTXDataType)2, 2582 },
	{ (Il2CppRGCTXDataType)3, 6330 },
	{ (Il2CppRGCTXDataType)3, 6329 },
	{ (Il2CppRGCTXDataType)3, 6331 },
	{ (Il2CppRGCTXDataType)2, 2425 },
	{ (Il2CppRGCTXDataType)2, 1804 },
	{ (Il2CppRGCTXDataType)3, 470 },
	{ (Il2CppRGCTXDataType)2, 2050 },
	{ (Il2CppRGCTXDataType)2, 2195 },
	{ (Il2CppRGCTXDataType)3, 754 },
	{ (Il2CppRGCTXDataType)3, 472 },
	{ (Il2CppRGCTXDataType)2, 656 },
	{ (Il2CppRGCTXDataType)2, 981 },
	{ (Il2CppRGCTXDataType)3, 469 },
	{ (Il2CppRGCTXDataType)3, 471 },
	{ (Il2CppRGCTXDataType)3, 456 },
	{ (Il2CppRGCTXDataType)3, 4603 },
	{ (Il2CppRGCTXDataType)3, 458 },
	{ (Il2CppRGCTXDataType)2, 654 },
	{ (Il2CppRGCTXDataType)2, 979 },
	{ (Il2CppRGCTXDataType)3, 455 },
	{ (Il2CppRGCTXDataType)3, 457 },
	{ (Il2CppRGCTXDataType)3, 527 },
	{ (Il2CppRGCTXDataType)3, 4472 },
	{ (Il2CppRGCTXDataType)2, 2054 },
	{ (Il2CppRGCTXDataType)2, 2198 },
	{ (Il2CppRGCTXDataType)3, 529 },
	{ (Il2CppRGCTXDataType)2, 660 },
	{ (Il2CppRGCTXDataType)2, 990 },
	{ (Il2CppRGCTXDataType)3, 526 },
	{ (Il2CppRGCTXDataType)3, 528 },
	{ (Il2CppRGCTXDataType)3, 541 },
	{ (Il2CppRGCTXDataType)2, 2057 },
	{ (Il2CppRGCTXDataType)2, 2200 },
	{ (Il2CppRGCTXDataType)3, 543 },
	{ (Il2CppRGCTXDataType)2, 662 },
	{ (Il2CppRGCTXDataType)2, 992 },
	{ (Il2CppRGCTXDataType)3, 540 },
	{ (Il2CppRGCTXDataType)3, 542 },
	{ (Il2CppRGCTXDataType)3, 555 },
	{ (Il2CppRGCTXDataType)3, 556 },
	{ (Il2CppRGCTXDataType)2, 2060 },
	{ (Il2CppRGCTXDataType)2, 2202 },
	{ (Il2CppRGCTXDataType)3, 558 },
	{ (Il2CppRGCTXDataType)2, 665 },
	{ (Il2CppRGCTXDataType)2, 994 },
	{ (Il2CppRGCTXDataType)3, 554 },
	{ (Il2CppRGCTXDataType)3, 557 },
	{ (Il2CppRGCTXDataType)3, 571 },
	{ (Il2CppRGCTXDataType)3, 4473 },
	{ (Il2CppRGCTXDataType)2, 2063 },
	{ (Il2CppRGCTXDataType)2, 2204 },
	{ (Il2CppRGCTXDataType)3, 573 },
	{ (Il2CppRGCTXDataType)2, 667 },
	{ (Il2CppRGCTXDataType)2, 996 },
	{ (Il2CppRGCTXDataType)3, 570 },
	{ (Il2CppRGCTXDataType)3, 572 },
	{ (Il2CppRGCTXDataType)3, 585 },
	{ (Il2CppRGCTXDataType)2, 2066 },
	{ (Il2CppRGCTXDataType)2, 2206 },
	{ (Il2CppRGCTXDataType)3, 587 },
	{ (Il2CppRGCTXDataType)2, 669 },
	{ (Il2CppRGCTXDataType)2, 998 },
	{ (Il2CppRGCTXDataType)3, 584 },
	{ (Il2CppRGCTXDataType)3, 586 },
	{ (Il2CppRGCTXDataType)3, 599 },
	{ (Il2CppRGCTXDataType)3, 600 },
	{ (Il2CppRGCTXDataType)2, 2069 },
	{ (Il2CppRGCTXDataType)2, 2208 },
	{ (Il2CppRGCTXDataType)3, 602 },
	{ (Il2CppRGCTXDataType)2, 672 },
	{ (Il2CppRGCTXDataType)2, 1000 },
	{ (Il2CppRGCTXDataType)3, 598 },
	{ (Il2CppRGCTXDataType)3, 601 },
	{ (Il2CppRGCTXDataType)3, 615 },
	{ (Il2CppRGCTXDataType)3, 4474 },
	{ (Il2CppRGCTXDataType)2, 2072 },
	{ (Il2CppRGCTXDataType)2, 2210 },
	{ (Il2CppRGCTXDataType)3, 617 },
	{ (Il2CppRGCTXDataType)2, 674 },
	{ (Il2CppRGCTXDataType)2, 1002 },
	{ (Il2CppRGCTXDataType)3, 614 },
	{ (Il2CppRGCTXDataType)3, 616 },
	{ (Il2CppRGCTXDataType)3, 629 },
	{ (Il2CppRGCTXDataType)2, 2075 },
	{ (Il2CppRGCTXDataType)2, 2212 },
	{ (Il2CppRGCTXDataType)3, 631 },
	{ (Il2CppRGCTXDataType)2, 676 },
	{ (Il2CppRGCTXDataType)2, 1004 },
	{ (Il2CppRGCTXDataType)3, 628 },
	{ (Il2CppRGCTXDataType)3, 630 },
	{ (Il2CppRGCTXDataType)3, 643 },
	{ (Il2CppRGCTXDataType)3, 644 },
	{ (Il2CppRGCTXDataType)2, 2078 },
	{ (Il2CppRGCTXDataType)2, 2214 },
	{ (Il2CppRGCTXDataType)3, 646 },
	{ (Il2CppRGCTXDataType)2, 679 },
	{ (Il2CppRGCTXDataType)2, 1006 },
	{ (Il2CppRGCTXDataType)3, 642 },
	{ (Il2CppRGCTXDataType)3, 645 },
	{ (Il2CppRGCTXDataType)3, 659 },
	{ (Il2CppRGCTXDataType)3, 4574 },
	{ (Il2CppRGCTXDataType)3, 4475 },
	{ (Il2CppRGCTXDataType)2, 2081 },
	{ (Il2CppRGCTXDataType)2, 2216 },
	{ (Il2CppRGCTXDataType)3, 661 },
	{ (Il2CppRGCTXDataType)2, 681 },
	{ (Il2CppRGCTXDataType)2, 1008 },
	{ (Il2CppRGCTXDataType)3, 658 },
	{ (Il2CppRGCTXDataType)3, 660 },
	{ (Il2CppRGCTXDataType)3, 673 },
	{ (Il2CppRGCTXDataType)3, 4575 },
	{ (Il2CppRGCTXDataType)2, 2084 },
	{ (Il2CppRGCTXDataType)2, 2218 },
	{ (Il2CppRGCTXDataType)3, 675 },
	{ (Il2CppRGCTXDataType)2, 683 },
	{ (Il2CppRGCTXDataType)2, 1010 },
	{ (Il2CppRGCTXDataType)3, 672 },
	{ (Il2CppRGCTXDataType)3, 674 },
	{ (Il2CppRGCTXDataType)3, 687 },
	{ (Il2CppRGCTXDataType)3, 688 },
	{ (Il2CppRGCTXDataType)3, 4576 },
	{ (Il2CppRGCTXDataType)2, 2087 },
	{ (Il2CppRGCTXDataType)2, 2220 },
	{ (Il2CppRGCTXDataType)3, 690 },
	{ (Il2CppRGCTXDataType)2, 686 },
	{ (Il2CppRGCTXDataType)2, 1012 },
	{ (Il2CppRGCTXDataType)3, 686 },
	{ (Il2CppRGCTXDataType)3, 689 },
	{ (Il2CppRGCTXDataType)3, 382 },
	{ (Il2CppRGCTXDataType)2, 2040 },
	{ (Il2CppRGCTXDataType)2, 2188 },
	{ (Il2CppRGCTXDataType)3, 4471 },
	{ (Il2CppRGCTXDataType)3, 384 },
	{ (Il2CppRGCTXDataType)2, 647 },
	{ (Il2CppRGCTXDataType)2, 969 },
	{ (Il2CppRGCTXDataType)3, 381 },
	{ (Il2CppRGCTXDataType)3, 383 },
	{ (Il2CppRGCTXDataType)3, 396 },
	{ (Il2CppRGCTXDataType)2, 2043 },
	{ (Il2CppRGCTXDataType)2, 2190 },
	{ (Il2CppRGCTXDataType)3, 398 },
	{ (Il2CppRGCTXDataType)2, 649 },
	{ (Il2CppRGCTXDataType)2, 971 },
	{ (Il2CppRGCTXDataType)3, 395 },
	{ (Il2CppRGCTXDataType)3, 397 },
	{ (Il2CppRGCTXDataType)3, 410 },
	{ (Il2CppRGCTXDataType)3, 411 },
	{ (Il2CppRGCTXDataType)2, 2046 },
	{ (Il2CppRGCTXDataType)2, 2192 },
	{ (Il2CppRGCTXDataType)3, 413 },
	{ (Il2CppRGCTXDataType)2, 652 },
	{ (Il2CppRGCTXDataType)2, 973 },
	{ (Il2CppRGCTXDataType)3, 409 },
	{ (Il2CppRGCTXDataType)3, 412 },
	{ (Il2CppRGCTXDataType)3, 294 },
	{ (Il2CppRGCTXDataType)2, 2022 },
	{ (Il2CppRGCTXDataType)2, 2176 },
	{ (Il2CppRGCTXDataType)3, 4469 },
	{ (Il2CppRGCTXDataType)3, 296 },
	{ (Il2CppRGCTXDataType)2, 633 },
	{ (Il2CppRGCTXDataType)2, 957 },
	{ (Il2CppRGCTXDataType)3, 293 },
	{ (Il2CppRGCTXDataType)3, 295 },
	{ (Il2CppRGCTXDataType)3, 308 },
	{ (Il2CppRGCTXDataType)2, 2025 },
	{ (Il2CppRGCTXDataType)2, 2178 },
	{ (Il2CppRGCTXDataType)3, 310 },
	{ (Il2CppRGCTXDataType)2, 635 },
	{ (Il2CppRGCTXDataType)2, 959 },
	{ (Il2CppRGCTXDataType)3, 307 },
	{ (Il2CppRGCTXDataType)3, 309 },
	{ (Il2CppRGCTXDataType)3, 322 },
	{ (Il2CppRGCTXDataType)3, 323 },
	{ (Il2CppRGCTXDataType)2, 2028 },
	{ (Il2CppRGCTXDataType)2, 2180 },
	{ (Il2CppRGCTXDataType)3, 325 },
	{ (Il2CppRGCTXDataType)2, 638 },
	{ (Il2CppRGCTXDataType)2, 961 },
	{ (Il2CppRGCTXDataType)3, 321 },
	{ (Il2CppRGCTXDataType)3, 324 },
	{ (Il2CppRGCTXDataType)3, 338 },
	{ (Il2CppRGCTXDataType)2, 2031 },
	{ (Il2CppRGCTXDataType)2, 2182 },
	{ (Il2CppRGCTXDataType)3, 4470 },
	{ (Il2CppRGCTXDataType)3, 340 },
	{ (Il2CppRGCTXDataType)2, 640 },
	{ (Il2CppRGCTXDataType)2, 963 },
	{ (Il2CppRGCTXDataType)3, 337 },
	{ (Il2CppRGCTXDataType)3, 339 },
	{ (Il2CppRGCTXDataType)3, 352 },
	{ (Il2CppRGCTXDataType)2, 2034 },
	{ (Il2CppRGCTXDataType)2, 2184 },
	{ (Il2CppRGCTXDataType)3, 354 },
	{ (Il2CppRGCTXDataType)2, 642 },
	{ (Il2CppRGCTXDataType)2, 965 },
	{ (Il2CppRGCTXDataType)3, 351 },
	{ (Il2CppRGCTXDataType)3, 353 },
	{ (Il2CppRGCTXDataType)3, 366 },
	{ (Il2CppRGCTXDataType)3, 367 },
	{ (Il2CppRGCTXDataType)2, 2037 },
	{ (Il2CppRGCTXDataType)2, 2186 },
	{ (Il2CppRGCTXDataType)3, 369 },
	{ (Il2CppRGCTXDataType)2, 645 },
	{ (Il2CppRGCTXDataType)2, 967 },
	{ (Il2CppRGCTXDataType)3, 365 },
	{ (Il2CppRGCTXDataType)3, 368 },
	{ (Il2CppRGCTXDataType)3, 484 },
	{ (Il2CppRGCTXDataType)2, 658 },
	{ (Il2CppRGCTXDataType)3, 486 },
	{ (Il2CppRGCTXDataType)2, 983 },
	{ (Il2CppRGCTXDataType)3, 483 },
	{ (Il2CppRGCTXDataType)3, 485 },
	{ (Il2CppRGCTXDataType)1, 223 },
	{ (Il2CppRGCTXDataType)3, 11252 },
	{ (Il2CppRGCTXDataType)3, 11248 },
	{ (Il2CppRGCTXDataType)3, 11020 },
	{ (Il2CppRGCTXDataType)3, 11243 },
	{ (Il2CppRGCTXDataType)3, 11247 },
	{ (Il2CppRGCTXDataType)1, 225 },
	{ (Il2CppRGCTXDataType)3, 10967 },
	{ (Il2CppRGCTXDataType)2, 3480 },
	{ (Il2CppRGCTXDataType)1, 370 },
	{ (Il2CppRGCTXDataType)1, 761 },
	{ (Il2CppRGCTXDataType)1, 1618 },
	{ (Il2CppRGCTXDataType)2, 1618 },
	{ (Il2CppRGCTXDataType)1, 298 },
	{ (Il2CppRGCTXDataType)2, 896 },
	{ (Il2CppRGCTXDataType)3, 32 },
	{ (Il2CppRGCTXDataType)3, 33 },
	{ (Il2CppRGCTXDataType)2, 1726 },
	{ (Il2CppRGCTXDataType)1, 1726 },
	{ (Il2CppRGCTXDataType)1, 2291 },
	{ (Il2CppRGCTXDataType)3, 34 },
	{ (Il2CppRGCTXDataType)3, 4635 },
	{ (Il2CppRGCTXDataType)3, 35 },
	{ (Il2CppRGCTXDataType)3, 4134 },
	{ (Il2CppRGCTXDataType)2, 1474 },
	{ (Il2CppRGCTXDataType)3, 4133 },
	{ (Il2CppRGCTXDataType)1, 299 },
	{ (Il2CppRGCTXDataType)2, 299 },
	{ (Il2CppRGCTXDataType)2, 984 },
	{ (Il2CppRGCTXDataType)3, 496 },
	{ (Il2CppRGCTXDataType)3, 11490 },
	{ (Il2CppRGCTXDataType)3, 11022 },
	{ (Il2CppRGCTXDataType)3, 11193 },
	{ (Il2CppRGCTXDataType)3, 11486 },
	{ (Il2CppRGCTXDataType)3, 11489 },
	{ (Il2CppRGCTXDataType)1, 297 },
	{ (Il2CppRGCTXDataType)3, 10969 },
	{ (Il2CppRGCTXDataType)1, 293 },
	{ (Il2CppRGCTXDataType)1, 294 },
	{ (Il2CppRGCTXDataType)1, 292 },
	{ (Il2CppRGCTXDataType)2, 899 },
	{ (Il2CppRGCTXDataType)3, 37 },
	{ (Il2CppRGCTXDataType)2, 899 },
	{ (Il2CppRGCTXDataType)1, 710 },
	{ (Il2CppRGCTXDataType)2, 710 },
	{ (Il2CppRGCTXDataType)2, 2296 },
	{ (Il2CppRGCTXDataType)2, 711 },
	{ (Il2CppRGCTXDataType)2, 985 },
	{ (Il2CppRGCTXDataType)3, 497 },
	{ (Il2CppRGCTXDataType)3, 498 },
	{ (Il2CppRGCTXDataType)2, 3456 },
	{ (Il2CppRGCTXDataType)2, 3457 },
	{ (Il2CppRGCTXDataType)2, 3458 },
	{ (Il2CppRGCTXDataType)2, 1165 },
	{ (Il2CppRGCTXDataType)3, 9635 },
	{ (Il2CppRGCTXDataType)2, 1165 },
	{ (Il2CppRGCTXDataType)3, 1671 },
	{ (Il2CppRGCTXDataType)2, 390 },
	{ (Il2CppRGCTXDataType)3, 1673 },
	{ (Il2CppRGCTXDataType)3, 1672 },
	{ (Il2CppRGCTXDataType)1, 390 },
	{ (Il2CppRGCTXDataType)2, 905 },
	{ (Il2CppRGCTXDataType)3, 77 },
	{ (Il2CppRGCTXDataType)1, 97 },
	{ (Il2CppRGCTXDataType)3, 78 },
	{ (Il2CppRGCTXDataType)2, 1696 },
	{ (Il2CppRGCTXDataType)3, 4599 },
	{ (Il2CppRGCTXDataType)2, 906 },
	{ (Il2CppRGCTXDataType)3, 81 },
	{ (Il2CppRGCTXDataType)1, 96 },
	{ (Il2CppRGCTXDataType)3, 82 },
	{ (Il2CppRGCTXDataType)2, 1540 },
	{ (Il2CppRGCTXDataType)3, 4455 },
	{ (Il2CppRGCTXDataType)2, 907 },
	{ (Il2CppRGCTXDataType)3, 85 },
	{ (Il2CppRGCTXDataType)1, 732 },
	{ (Il2CppRGCTXDataType)1, 341 },
	{ (Il2CppRGCTXDataType)3, 86 },
	{ (Il2CppRGCTXDataType)2, 1610 },
	{ (Il2CppRGCTXDataType)3, 4549 },
	{ (Il2CppRGCTXDataType)2, 910 },
	{ (Il2CppRGCTXDataType)3, 97 },
	{ (Il2CppRGCTXDataType)3, 98 },
	{ (Il2CppRGCTXDataType)2, 913 },
	{ (Il2CppRGCTXDataType)3, 109 },
	{ (Il2CppRGCTXDataType)3, 110 },
	{ (Il2CppRGCTXDataType)2, 1703 },
	{ (Il2CppRGCTXDataType)3, 4604 },
	{ (Il2CppRGCTXDataType)2, 914 },
	{ (Il2CppRGCTXDataType)3, 113 },
	{ (Il2CppRGCTXDataType)3, 114 },
	{ (Il2CppRGCTXDataType)2, 917 },
	{ (Il2CppRGCTXDataType)3, 125 },
	{ (Il2CppRGCTXDataType)3, 126 },
	{ (Il2CppRGCTXDataType)2, 920 },
	{ (Il2CppRGCTXDataType)3, 137 },
	{ (Il2CppRGCTXDataType)3, 138 },
	{ (Il2CppRGCTXDataType)2, 1068 },
	{ (Il2CppRGCTXDataType)3, 785 },
	{ (Il2CppRGCTXDataType)2, 921 },
	{ (Il2CppRGCTXDataType)3, 141 },
	{ (Il2CppRGCTXDataType)3, 142 },
	{ (Il2CppRGCTXDataType)2, 544 },
	{ (Il2CppRGCTXDataType)2, 545 },
	{ (Il2CppRGCTXDataType)2, 546 },
	{ (Il2CppRGCTXDataType)2, 819 },
	{ (Il2CppRGCTXDataType)2, 547 },
	{ (Il2CppRGCTXDataType)3, 4555 },
	{ (Il2CppRGCTXDataType)2, 820 },
	{ (Il2CppRGCTXDataType)2, 548 },
	{ (Il2CppRGCTXDataType)3, 4556 },
	{ (Il2CppRGCTXDataType)3, 4467 },
	{ (Il2CppRGCTXDataType)2, 549 },
	{ (Il2CppRGCTXDataType)2, 550 },
	{ (Il2CppRGCTXDataType)2, 822 },
	{ (Il2CppRGCTXDataType)3, 780 },
	{ (Il2CppRGCTXDataType)2, 551 },
	{ (Il2CppRGCTXDataType)3, 781 },
	{ (Il2CppRGCTXDataType)2, 552 },
	{ (Il2CppRGCTXDataType)3, 753 },
	{ (Il2CppRGCTXDataType)3, 2199 },
	{ (Il2CppRGCTXDataType)2, 1251 },
	{ (Il2CppRGCTXDataType)3, 4178 },
	{ (Il2CppRGCTXDataType)2, 1492 },
	{ (Il2CppRGCTXDataType)3, 2200 },
	{ (Il2CppRGCTXDataType)3, 2227 },
	{ (Il2CppRGCTXDataType)2, 1280 },
	{ (Il2CppRGCTXDataType)3, 2274 },
	{ (Il2CppRGCTXDataType)3, 2201 },
	{ (Il2CppRGCTXDataType)3, 2278 },
	{ (Il2CppRGCTXDataType)3, 2228 },
	{ (Il2CppRGCTXDataType)3, 2230 },
	{ (Il2CppRGCTXDataType)3, 9381 },
	{ (Il2CppRGCTXDataType)3, 3927 },
	{ (Il2CppRGCTXDataType)3, 3926 },
	{ (Il2CppRGCTXDataType)2, 1458 },
	{ (Il2CppRGCTXDataType)3, 2275 },
	{ (Il2CppRGCTXDataType)3, 2277 },
	{ (Il2CppRGCTXDataType)3, 2893 },
	{ (Il2CppRGCTXDataType)3, 2895 },
	{ (Il2CppRGCTXDataType)3, 2279 },
	{ (Il2CppRGCTXDataType)3, 2276 },
	{ (Il2CppRGCTXDataType)3, 2206 },
	{ (Il2CppRGCTXDataType)2, 2596 },
	{ (Il2CppRGCTXDataType)3, 6348 },
	{ (Il2CppRGCTXDataType)2, 2616 },
	{ (Il2CppRGCTXDataType)3, 6398 },
	{ (Il2CppRGCTXDataType)3, 11137 },
	{ (Il2CppRGCTXDataType)3, 2105 },
	{ (Il2CppRGCTXDataType)3, 2107 },
	{ (Il2CppRGCTXDataType)3, 6160 },
	{ (Il2CppRGCTXDataType)3, 11138 },
	{ (Il2CppRGCTXDataType)3, 2111 },
	{ (Il2CppRGCTXDataType)3, 2113 },
	{ (Il2CppRGCTXDataType)3, 6182 },
	{ (Il2CppRGCTXDataType)3, 4572 },
	{ (Il2CppRGCTXDataType)3, 6159 },
	{ (Il2CppRGCTXDataType)3, 6349 },
	{ (Il2CppRGCTXDataType)3, 6181 },
	{ (Il2CppRGCTXDataType)3, 6399 },
	{ (Il2CppRGCTXDataType)3, 2112 },
	{ (Il2CppRGCTXDataType)2, 1238 },
	{ (Il2CppRGCTXDataType)3, 2106 },
	{ (Il2CppRGCTXDataType)2, 1234 },
	{ (Il2CppRGCTXDataType)3, 6351 },
	{ (Il2CppRGCTXDataType)3, 6400 },
	{ (Il2CppRGCTXDataType)3, 2894 },
	{ (Il2CppRGCTXDataType)3, 6350 },
	{ (Il2CppRGCTXDataType)2, 936 },
	{ (Il2CppRGCTXDataType)3, 199 },
	{ (Il2CppRGCTXDataType)2, 934 },
	{ (Il2CppRGCTXDataType)3, 191 },
	{ (Il2CppRGCTXDataType)2, 116 },
	{ (Il2CppRGCTXDataType)3, 192 },
	{ (Il2CppRGCTXDataType)2, 1544 },
	{ (Il2CppRGCTXDataType)3, 4459 },
	{ (Il2CppRGCTXDataType)3, 200 },
	{ (Il2CppRGCTXDataType)2, 939 },
	{ (Il2CppRGCTXDataType)3, 211 },
	{ (Il2CppRGCTXDataType)3, 212 },
	{ (Il2CppRGCTXDataType)2, 1024 },
	{ (Il2CppRGCTXDataType)3, 749 },
	{ (Il2CppRGCTXDataType)2, 944 },
	{ (Il2CppRGCTXDataType)3, 227 },
	{ (Il2CppRGCTXDataType)3, 228 },
	{ (Il2CppRGCTXDataType)2, 3075 },
	{ (Il2CppRGCTXDataType)3, 9516 },
	{ (Il2CppRGCTXDataType)2, 948 },
	{ (Il2CppRGCTXDataType)3, 247 },
	{ (Il2CppRGCTXDataType)3, 248 },
	{ (Il2CppRGCTXDataType)2, 3083 },
	{ (Il2CppRGCTXDataType)3, 9544 },
	{ (Il2CppRGCTXDataType)2, 951 },
	{ (Il2CppRGCTXDataType)3, 261 },
	{ (Il2CppRGCTXDataType)3, 262 },
	{ (Il2CppRGCTXDataType)2, 3080 },
	{ (Il2CppRGCTXDataType)3, 9533 },
	{ (Il2CppRGCTXDataType)2, 903 },
	{ (Il2CppRGCTXDataType)3, 56 },
	{ (Il2CppRGCTXDataType)3, 57 },
	{ (Il2CppRGCTXDataType)2, 3085 },
	{ (Il2CppRGCTXDataType)3, 9554 },
	{ (Il2CppRGCTXDataType)2, 909 },
	{ (Il2CppRGCTXDataType)3, 91 },
	{ (Il2CppRGCTXDataType)3, 92 },
	{ (Il2CppRGCTXDataType)2, 1026 },
	{ (Il2CppRGCTXDataType)3, 750 },
	{ (Il2CppRGCTXDataType)2, 912 },
	{ (Il2CppRGCTXDataType)3, 103 },
	{ (Il2CppRGCTXDataType)3, 104 },
	{ (Il2CppRGCTXDataType)2, 1545 },
	{ (Il2CppRGCTXDataType)3, 4460 },
	{ (Il2CppRGCTXDataType)2, 916 },
	{ (Il2CppRGCTXDataType)3, 119 },
	{ (Il2CppRGCTXDataType)3, 120 },
	{ (Il2CppRGCTXDataType)2, 3081 },
	{ (Il2CppRGCTXDataType)3, 9534 },
	{ (Il2CppRGCTXDataType)2, 919 },
	{ (Il2CppRGCTXDataType)3, 129 },
	{ (Il2CppRGCTXDataType)3, 130 },
	{ (Il2CppRGCTXDataType)2, 3076 },
	{ (Il2CppRGCTXDataType)3, 9517 },
	{ (Il2CppRGCTXDataType)1, 1613 },
	{ (Il2CppRGCTXDataType)2, 1613 },
	{ (Il2CppRGCTXDataType)2, 924 },
	{ (Il2CppRGCTXDataType)3, 151 },
	{ (Il2CppRGCTXDataType)1, 115 },
	{ (Il2CppRGCTXDataType)3, 152 },
	{ (Il2CppRGCTXDataType)2, 1067 },
	{ (Il2CppRGCTXDataType)3, 784 },
	{ (Il2CppRGCTXDataType)2, 926 },
	{ (Il2CppRGCTXDataType)3, 159 },
	{ (Il2CppRGCTXDataType)1, 359 },
	{ (Il2CppRGCTXDataType)1, 750 },
	{ (Il2CppRGCTXDataType)3, 160 },
	{ (Il2CppRGCTXDataType)2, 1748 },
	{ (Il2CppRGCTXDataType)3, 4658 },
	{ (Il2CppRGCTXDataType)2, 928 },
	{ (Il2CppRGCTXDataType)3, 167 },
	{ (Il2CppRGCTXDataType)1, 118 },
	{ (Il2CppRGCTXDataType)3, 168 },
	{ (Il2CppRGCTXDataType)2, 1698 },
	{ (Il2CppRGCTXDataType)3, 4601 },
	{ (Il2CppRGCTXDataType)2, 930 },
	{ (Il2CppRGCTXDataType)3, 175 },
	{ (Il2CppRGCTXDataType)1, 749 },
	{ (Il2CppRGCTXDataType)1, 358 },
	{ (Il2CppRGCTXDataType)3, 176 },
	{ (Il2CppRGCTXDataType)2, 1746 },
	{ (Il2CppRGCTXDataType)3, 4656 },
	{ (Il2CppRGCTXDataType)1, 1025 },
	{ (Il2CppRGCTXDataType)2, 1025 },
	{ (Il2CppRGCTXDataType)1, 1062 },
	{ (Il2CppRGCTXDataType)2, 1062 },
	{ (Il2CppRGCTXDataType)2, 595 },
	{ (Il2CppRGCTXDataType)2, 596 },
	{ (Il2CppRGCTXDataType)2, 597 },
	{ (Il2CppRGCTXDataType)2, 841 },
	{ (Il2CppRGCTXDataType)2, 598 },
	{ (Il2CppRGCTXDataType)1, 599 },
	{ (Il2CppRGCTXDataType)2, 599 },
	{ (Il2CppRGCTXDataType)2, 842 },
	{ (Il2CppRGCTXDataType)2, 584 },
	{ (Il2CppRGCTXDataType)2, 585 },
	{ (Il2CppRGCTXDataType)2, 586 },
	{ (Il2CppRGCTXDataType)1, 587 },
	{ (Il2CppRGCTXDataType)2, 587 },
	{ (Il2CppRGCTXDataType)2, 837 },
	{ (Il2CppRGCTXDataType)2, 588 },
	{ (Il2CppRGCTXDataType)2, 838 },
	{ (Il2CppRGCTXDataType)2, 589 },
	{ (Il2CppRGCTXDataType)2, 839 },
	{ (Il2CppRGCTXDataType)2, 590 },
	{ (Il2CppRGCTXDataType)2, 591 },
	{ (Il2CppRGCTXDataType)2, 592 },
	{ (Il2CppRGCTXDataType)2, 840 },
	{ (Il2CppRGCTXDataType)2, 1759 },
	{ (Il2CppRGCTXDataType)1, 425 },
	{ (Il2CppRGCTXDataType)2, 425 },
	{ (Il2CppRGCTXDataType)3, 4871 },
	{ (Il2CppRGCTXDataType)3, 11339 },
	{ (Il2CppRGCTXDataType)3, 4870 },
	{ (Il2CppRGCTXDataType)3, 11331 },
	{ (Il2CppRGCTXDataType)3, 4916 },
	{ (Il2CppRGCTXDataType)3, 4917 },
	{ (Il2CppRGCTXDataType)2, 1440 },
	{ (Il2CppRGCTXDataType)2, 1817 },
	{ (Il2CppRGCTXDataType)3, 5143 },
	{ (Il2CppRGCTXDataType)2, 447 },
	{ (Il2CppRGCTXDataType)2, 2436 },
	{ (Il2CppRGCTXDataType)2, 1993 },
	{ (Il2CppRGCTXDataType)2, 3515 },
	{ (Il2CppRGCTXDataType)3, 5142 },
	{ (Il2CppRGCTXDataType)2, 450 },
	{ (Il2CppRGCTXDataType)2, 1825 },
	{ (Il2CppRGCTXDataType)3, 5059 },
	{ (Il2CppRGCTXDataType)3, 5207 },
	{ (Il2CppRGCTXDataType)2, 786 },
	{ (Il2CppRGCTXDataType)2, 2443 },
	{ (Il2CppRGCTXDataType)3, 5096 },
	{ (Il2CppRGCTXDataType)3, 5057 },
	{ (Il2CppRGCTXDataType)3, 5058 },
	{ (Il2CppRGCTXDataType)2, 2097 },
	{ (Il2CppRGCTXDataType)3, 5081 },
	{ (Il2CppRGCTXDataType)2, 3548 },
	{ (Il2CppRGCTXDataType)3, 5206 },
	{ (Il2CppRGCTXDataType)3, 5095 },
	{ (Il2CppRGCTXDataType)2, 1806 },
	{ (Il2CppRGCTXDataType)2, 3476 },
	{ (Il2CppRGCTXDataType)3, 10723 },
	{ (Il2CppRGCTXDataType)2, 2427 },
	{ (Il2CppRGCTXDataType)2, 1807 },
	{ (Il2CppRGCTXDataType)2, 3477 },
	{ (Il2CppRGCTXDataType)3, 10724 },
	{ (Il2CppRGCTXDataType)3, 4463 },
	{ (Il2CppRGCTXDataType)2, 2428 },
	{ (Il2CppRGCTXDataType)2, 1808 },
	{ (Il2CppRGCTXDataType)2, 2429 },
	{ (Il2CppRGCTXDataType)3, 4464 },
	{ (Il2CppRGCTXDataType)2, 1809 },
	{ (Il2CppRGCTXDataType)2, 2430 },
	{ (Il2CppRGCTXDataType)3, 2242 },
	{ (Il2CppRGCTXDataType)3, 2243 },
	{ (Il2CppRGCTXDataType)3, 2240 },
	{ (Il2CppRGCTXDataType)2, 452 },
	{ (Il2CppRGCTXDataType)3, 5243 },
	{ (Il2CppRGCTXDataType)2, 787 },
	{ (Il2CppRGCTXDataType)3, 5244 },
	{ (Il2CppRGCTXDataType)3, 2241 },
	{ (Il2CppRGCTXDataType)3, 2235 },
	{ (Il2CppRGCTXDataType)3, 2244 },
	{ (Il2CppRGCTXDataType)3, 5238 },
	{ (Il2CppRGCTXDataType)2, 1252 },
	{ (Il2CppRGCTXDataType)3, 2232 },
	{ (Il2CppRGCTXDataType)2, 2597 },
	{ (Il2CppRGCTXDataType)3, 6352 },
	{ (Il2CppRGCTXDataType)3, 6358 },
	{ (Il2CppRGCTXDataType)2, 2527 },
	{ (Il2CppRGCTXDataType)3, 6161 },
	{ (Il2CppRGCTXDataType)3, 6355 },
	{ (Il2CppRGCTXDataType)3, 2233 },
	{ (Il2CppRGCTXDataType)3, 6353 },
	{ (Il2CppRGCTXDataType)3, 6354 },
	{ (Il2CppRGCTXDataType)3, 2234 },
	{ (Il2CppRGCTXDataType)3, 6356 },
	{ (Il2CppRGCTXDataType)3, 2238 },
	{ (Il2CppRGCTXDataType)3, 5242 },
	{ (Il2CppRGCTXDataType)3, 6357 },
	{ (Il2CppRGCTXDataType)3, 2239 },
	{ (Il2CppRGCTXDataType)3, 6162 },
	{ (Il2CppRGCTXDataType)3, 6163 },
	{ (Il2CppRGCTXDataType)3, 2236 },
	{ (Il2CppRGCTXDataType)3, 2237 },
	{ (Il2CppRGCTXDataType)2, 1459 },
	{ (Il2CppRGCTXDataType)3, 5241 },
	{ (Il2CppRGCTXDataType)3, 5239 },
	{ (Il2CppRGCTXDataType)3, 5240 },
	{ (Il2CppRGCTXDataType)2, 3853 },
	{ (Il2CppRGCTXDataType)2, 3854 },
	{ (Il2CppRGCTXDataType)2, 3855 },
	{ (Il2CppRGCTXDataType)2, 3856 },
	{ (Il2CppRGCTXDataType)2, 3851 },
	{ (Il2CppRGCTXDataType)2, 3852 },
	{ (Il2CppRGCTXDataType)2, 3857 },
	{ (Il2CppRGCTXDataType)2, 3858 },
	{ (Il2CppRGCTXDataType)2, 3849 },
	{ (Il2CppRGCTXDataType)2, 3850 },
	{ (Il2CppRGCTXDataType)2, 485 },
	{ (Il2CppRGCTXDataType)2, 2901 },
	{ (Il2CppRGCTXDataType)3, 8753 },
	{ (Il2CppRGCTXDataType)2, 2901 },
	{ (Il2CppRGCTXDataType)3, 11498 },
	{ (Il2CppRGCTXDataType)1, 309 },
	{ (Il2CppRGCTXDataType)2, 3492 },
	{ (Il2CppRGCTXDataType)3, 11499 },
	{ (Il2CppRGCTXDataType)1, 307 },
};
extern const CustomAttributesCacheGenerator g_Sirenix_Utilities_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Sirenix_Utilities_CodeGenModule;
const Il2CppCodeGenModule g_Sirenix_Utilities_CodeGenModule = 
{
	"Sirenix.Utilities.dll",
	994,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	168,
	s_rgctxIndices,
	709,
	s_rgctxValues,
	NULL,
	g_Sirenix_Utilities_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
