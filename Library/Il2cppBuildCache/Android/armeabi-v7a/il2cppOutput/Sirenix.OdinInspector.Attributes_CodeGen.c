﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Sirenix.OdinInspector.AssetListAttribute::.ctor()
extern void AssetListAttribute__ctor_mD7BEF52C5E192D3C71CDD7E151B5C69C05FF7544 (void);
// 0x00000002 System.Void Sirenix.OdinInspector.AssetSelectorAttribute::set_Paths(System.String)
extern void AssetSelectorAttribute_set_Paths_m26DA529BF216F8FF20C13B7346D81BBBFF7C7DE5 (void);
// 0x00000003 System.String Sirenix.OdinInspector.AssetSelectorAttribute::get_Paths()
extern void AssetSelectorAttribute_get_Paths_m539145D9BC2B1F42338AF9F327F54E9B055D752C (void);
// 0x00000004 System.Void Sirenix.OdinInspector.AssetSelectorAttribute::.ctor()
extern void AssetSelectorAttribute__ctor_m4040E0B057B335CE1478EFA6EB099118FDFC0CC2 (void);
// 0x00000005 System.Void Sirenix.OdinInspector.AssetSelectorAttribute/<>c::.cctor()
extern void U3CU3Ec__cctor_m7094437B7D37D1E6FF4DD391765535E524599282 (void);
// 0x00000006 System.Void Sirenix.OdinInspector.AssetSelectorAttribute/<>c::.ctor()
extern void U3CU3Ec__ctor_mEF4D05798CE3F378E5E36DF88F14353C7CF9E182 (void);
// 0x00000007 System.String Sirenix.OdinInspector.AssetSelectorAttribute/<>c::<set_Paths>b__12_0(System.String)
extern void U3CU3Ec_U3Cset_PathsU3Eb__12_0_mB1598E3D172430978C835A9B4E1A1268891DF76D (void);
// 0x00000008 System.Void Sirenix.OdinInspector.AssetsOnlyAttribute::.ctor()
extern void AssetsOnlyAttribute__ctor_m33B3ADA2CD2AB296AA0FC62AA4BCAA8CA56D6DC6 (void);
// 0x00000009 System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor(System.String,System.Boolean,System.Boolean,System.Int32)
extern void BoxGroupAttribute__ctor_mD170E60B450AABCFDC796021EE2DD187F55E6500 (void);
// 0x0000000A System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor()
extern void BoxGroupAttribute__ctor_m6B421EB7331331EB7D1B56C34DE47AE46266C2C3 (void);
// 0x0000000B System.Void Sirenix.OdinInspector.BoxGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void BoxGroupAttribute_CombineValuesWith_mE5E414B3F475B56507C8FC5A2B20A60261CA2EEF (void);
// 0x0000000C System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor()
extern void ButtonAttribute__ctor_mA189093060719DE77084CF59BFAA6CBBA9AE5BBA (void);
// 0x0000000D System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonSizes)
extern void ButtonAttribute__ctor_m54AC64264E1D03E7A49DA7BAF9BFFDE89C45C9F1 (void);
// 0x0000000E System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.Int32)
extern void ButtonAttribute__ctor_mF2F44124675D1722CFD80947CA439B6CA6D9B61E (void);
// 0x0000000F System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String)
extern void ButtonAttribute__ctor_m83C5D44DAF14B09929680979D9E25DFF2F980A30 (void);
// 0x00000010 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonSizes)
extern void ButtonAttribute__ctor_mFDCA00462D14D14FF5EF79B7AF3D602C8592244A (void);
// 0x00000011 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,System.Int32)
extern void ButtonAttribute__ctor_mAD43B91B8D486F4C6DFD046E5B3EA15999F8DF19 (void);
// 0x00000012 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_m40DDBADB260CF6641F37A7816F40D28D505A72C5 (void);
// 0x00000013 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.Int32,Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_m7BE4CA206B38073753DB64895530A4E95732DEF4 (void);
// 0x00000014 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonSizes,Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_mA3C5891F8246BF6A351C2A083391F60BCC6B24FF (void);
// 0x00000015 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_m8C9AB7A45AE05F9214E955FD582DD1B6570F65C7 (void);
// 0x00000016 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonSizes,Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_mC75E79D918637EE750DEFA31D00792CC5BBA8B7D (void);
// 0x00000017 System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,System.Int32,Sirenix.OdinInspector.ButtonStyle)
extern void ButtonAttribute__ctor_mA5567E1BCD7D54A2EAC16AA5FEC0575A1A532EDB (void);
// 0x00000018 System.Void Sirenix.OdinInspector.ButtonGroupAttribute::.ctor(System.String,System.Int32)
extern void ButtonGroupAttribute__ctor_m34D3B7CC10A78BA26C227283792D56AE2DFBD04A (void);
// 0x00000019 System.Void Sirenix.OdinInspector.ChildGameObjectsOnlyAttribute::.ctor()
extern void ChildGameObjectsOnlyAttribute__ctor_mDDEA4F0844C178B7145CBABE91827E784862CEB7 (void);
// 0x0000001A System.Void Sirenix.OdinInspector.CustomValueDrawerAttribute::.ctor(System.String)
extern void CustomValueDrawerAttribute__ctor_m1AD80BD54925742FECC79F2BAF67F844EBBF83A3 (void);
// 0x0000001B System.Void Sirenix.OdinInspector.DisableInInlineEditorsAttribute::.ctor()
extern void DisableInInlineEditorsAttribute__ctor_m80B5355794D7D985E91C1407E0710BA1D37B943A (void);
// 0x0000001C System.Void Sirenix.OdinInspector.DisableInNonPrefabsAttribute::.ctor()
extern void DisableInNonPrefabsAttribute__ctor_m76759AE55534012C1DD807020329CC7796DBAA7C (void);
// 0x0000001D System.Void Sirenix.OdinInspector.DisableInPrefabAssetsAttribute::.ctor()
extern void DisableInPrefabAssetsAttribute__ctor_m43BC5A861668B7A9AEC41E63C3841D138177284F (void);
// 0x0000001E System.Void Sirenix.OdinInspector.DisableInPrefabInstancesAttribute::.ctor()
extern void DisableInPrefabInstancesAttribute__ctor_m874C04BDC1F94EC2270E7789A634CA654EDD54FE (void);
// 0x0000001F System.Void Sirenix.OdinInspector.DisableInPrefabsAttribute::.ctor()
extern void DisableInPrefabsAttribute__ctor_mBE3358BF4C37BE9CA4CFD07FFAEB75646772B260 (void);
// 0x00000020 System.Void Sirenix.OdinInspector.DoNotDrawAsReferenceAttribute::.ctor()
extern void DoNotDrawAsReferenceAttribute__ctor_m47147F6256197B0FDB1F2E13D9C65DF661E18653 (void);
// 0x00000021 System.Void Sirenix.OdinInspector.EnableGUIAttribute::.ctor()
extern void EnableGUIAttribute__ctor_m11708B222C12CFD9CAB97B0919A83C0A68A846B1 (void);
// 0x00000022 System.Void Sirenix.OdinInspector.EnumPagingAttribute::.ctor()
extern void EnumPagingAttribute__ctor_mCDBD71F100FE496FEA9D1E64F5D9B77AE1621272 (void);
// 0x00000023 System.Boolean Sirenix.OdinInspector.FilePathAttribute::get_ReadOnly()
extern void FilePathAttribute_get_ReadOnly_m3A6314406C2D58C15799C01B671EE88CC4399D70 (void);
// 0x00000024 System.Void Sirenix.OdinInspector.FilePathAttribute::set_ReadOnly(System.Boolean)
extern void FilePathAttribute_set_ReadOnly_mC86951B771DBB611C650516E350D4DA4EADA3ECD (void);
// 0x00000025 System.Void Sirenix.OdinInspector.FilePathAttribute::.ctor()
extern void FilePathAttribute__ctor_m9ED4FCDC100FF01750AD869DB5EDB9060171DAF2 (void);
// 0x00000026 System.Void Sirenix.OdinInspector.FolderPathAttribute::.ctor()
extern void FolderPathAttribute__ctor_m66C482346C8479FE547313BA678A83E9A9495FAC (void);
// 0x00000027 System.Void Sirenix.OdinInspector.HideDuplicateReferenceBoxAttribute::.ctor()
extern void HideDuplicateReferenceBoxAttribute__ctor_m2C45CE67CEC79FD4DCB816BF86E95AD95CADB89E (void);
// 0x00000028 System.String Sirenix.OdinInspector.HideIfGroupAttribute::get_MemberName()
extern void HideIfGroupAttribute_get_MemberName_m2F189429E0B3943DFEA543D0E205E6F5022682BF (void);
// 0x00000029 System.Void Sirenix.OdinInspector.HideIfGroupAttribute::set_MemberName(System.String)
extern void HideIfGroupAttribute_set_MemberName_m6839D915223254EE1B9F9A24AFDF46A8CCBB8A1E (void);
// 0x0000002A System.Void Sirenix.OdinInspector.HideIfGroupAttribute::.ctor(System.String,System.Boolean)
extern void HideIfGroupAttribute__ctor_m986B873A48DE6DDCB2E6D6F1F304704ADACF9501 (void);
// 0x0000002B System.Void Sirenix.OdinInspector.HideIfGroupAttribute::.ctor(System.String,System.Object,System.Boolean)
extern void HideIfGroupAttribute__ctor_m9B6851A6ACEFCA54B488B2E3FB8D59CE3A660B04 (void);
// 0x0000002C System.Void Sirenix.OdinInspector.HideIfGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void HideIfGroupAttribute_CombineValuesWith_mB19848141AE80E205B5C72A3439CFA60C2A5FE52 (void);
// 0x0000002D System.Void Sirenix.OdinInspector.HideInInlineEditorsAttribute::.ctor()
extern void HideInInlineEditorsAttribute__ctor_mBB67C06431BCD0DB7796A4B8EE94A1C5681D64BF (void);
// 0x0000002E System.Void Sirenix.OdinInspector.HideInNonPrefabsAttribute::.ctor()
extern void HideInNonPrefabsAttribute__ctor_mBD588D03AB995CC3ABFBC1C67D35DA9740FD1AF2 (void);
// 0x0000002F System.Void Sirenix.OdinInspector.HideInPrefabAssetsAttribute::.ctor()
extern void HideInPrefabAssetsAttribute__ctor_m7C32BAA642A467D80B5395E6B137E1CBB08952FB (void);
// 0x00000030 System.Void Sirenix.OdinInspector.HideInPrefabInstancesAttribute::.ctor()
extern void HideInPrefabInstancesAttribute__ctor_m24FCF4C7A203AC9BEE774ADF1E99A7A328157A5F (void);
// 0x00000031 System.Void Sirenix.OdinInspector.HideInPrefabsAttribute::.ctor()
extern void HideInPrefabsAttribute__ctor_mCAA14A340A0EA0F4B76DA56B295BD084806C5EB5 (void);
// 0x00000032 System.Void Sirenix.OdinInspector.HideInTablesAttribute::.ctor()
extern void HideInTablesAttribute__ctor_m7C2FA177C1B63C744039018A1B9631740FDCFB69 (void);
// 0x00000033 System.Void Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute::.ctor()
extern void HideNetworkBehaviourFieldsAttribute__ctor_mF14C2E8AC55D25A6755F85402C9F9D16EFD86571 (void);
// 0x00000034 System.Void Sirenix.OdinInspector.InlinePropertyAttribute::.ctor()
extern void InlinePropertyAttribute__ctor_m89DE665FBEDCA7C8719E3A71D152F42C57F99F14 (void);
// 0x00000035 System.Void Sirenix.OdinInspector.LabelWidthAttribute::.ctor(System.Single)
extern void LabelWidthAttribute__ctor_m50D992D5EF45F1422D89592679A4A00370B38269 (void);
// 0x00000036 System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor()
extern void PreviewFieldAttribute__ctor_mC099E62E5FAC38708BC45556F8C6E6233D689740 (void);
// 0x00000037 System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(System.Single)
extern void PreviewFieldAttribute__ctor_mFEDC314B35C519267808A58D7E9264027E418660 (void);
// 0x00000038 System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(System.Single,Sirenix.OdinInspector.ObjectFieldAlignment)
extern void PreviewFieldAttribute__ctor_m9AC2A039712B5273C77B62CE22D0147500E0EC1D (void);
// 0x00000039 System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(Sirenix.OdinInspector.ObjectFieldAlignment)
extern void PreviewFieldAttribute__ctor_mA69C62B5B596B6BE59A938C40FDBDDD10E3A4294 (void);
// 0x0000003A System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.Double,System.Double,System.Single,System.Single,System.Single)
extern void ProgressBarAttribute__ctor_m6798524CD0FD36BF262D07CE49731D29C6034C98 (void);
// 0x0000003B System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.String,System.Double,System.Single,System.Single,System.Single)
extern void ProgressBarAttribute__ctor_m2D683B9926AD93ED85E99E6A982245C37FFFAE62 (void);
// 0x0000003C System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.Double,System.String,System.Single,System.Single,System.Single)
extern void ProgressBarAttribute__ctor_m548557B8A039AF4A656F9CF1849E312DB3EEA95F (void);
// 0x0000003D System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.String,System.String,System.Single,System.Single,System.Single)
extern void ProgressBarAttribute__ctor_m449BBE33DABF66BD8E56261E35A71C58E0A6C7F2 (void);
// 0x0000003E System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_DrawValueLabel()
extern void ProgressBarAttribute_get_DrawValueLabel_m8755D428C189B82B8423283EAC1ECE16E828812E (void);
// 0x0000003F System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabel(System.Boolean)
extern void ProgressBarAttribute_set_DrawValueLabel_mB76F6D2A75922450777B1FC0BA8E5E39315EA968 (void);
// 0x00000040 System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_DrawValueLabelHasValue()
extern void ProgressBarAttribute_get_DrawValueLabelHasValue_m06BCD66013F03A68A6C08FF5D7A49AEF316F71D9 (void);
// 0x00000041 System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabelHasValue(System.Boolean)
extern void ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624 (void);
// 0x00000042 UnityEngine.TextAlignment Sirenix.OdinInspector.ProgressBarAttribute::get_ValueLabelAlignment()
extern void ProgressBarAttribute_get_ValueLabelAlignment_mB6B666D410FFE91353B85A24A53AE60782CB3E2A (void);
// 0x00000043 System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignment(UnityEngine.TextAlignment)
extern void ProgressBarAttribute_set_ValueLabelAlignment_mE8366B95F522B01603C426704B9047FFE3BCF742 (void);
// 0x00000044 System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_ValueLabelAlignmentHasValue()
extern void ProgressBarAttribute_get_ValueLabelAlignmentHasValue_m7DC1402D717D0D4EE8E72DF81C941D244509A447 (void);
// 0x00000045 System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignmentHasValue(System.Boolean)
extern void ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0 (void);
// 0x00000046 System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.Double,System.Double)
extern void PropertyRangeAttribute__ctor_m31FCD91543E6D7E0AE3B00A9B2BB744CCA2725AF (void);
// 0x00000047 System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.String,System.Double)
extern void PropertyRangeAttribute__ctor_m442057690400645107E37F5C12D7D976269A15EE (void);
// 0x00000048 System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.Double,System.String)
extern void PropertyRangeAttribute__ctor_mBC695897F24EF4544EC25B5E9876165B8ED1FA77 (void);
// 0x00000049 System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.String,System.String)
extern void PropertyRangeAttribute__ctor_m9EBF44FAF1CFE1EDC513180C6EBB28B6B9E786BE (void);
// 0x0000004A System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor()
extern void PropertySpaceAttribute__ctor_m83A768EB9D318E84B668055F808E5F392D7C32EB (void);
// 0x0000004B System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor(System.Single)
extern void PropertySpaceAttribute__ctor_m76955C7DE89CAC865AA6055D4DB629D46FF7CAA0 (void);
// 0x0000004C System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor(System.Single,System.Single)
extern void PropertySpaceAttribute__ctor_mB343CE0B6B517B4A78002E95B34A5278629A8285 (void);
// 0x0000004D System.Void Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::.ctor(System.String)
extern void ResponsiveButtonGroupAttribute__ctor_m4F03C6D6CC22A5B4586F993A4E4BBC8CE4320D8F (void);
// 0x0000004E System.Void Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void ResponsiveButtonGroupAttribute_CombineValuesWith_mE025C2906C58CCD9A5A71A2758B0B014C1A00910 (void);
// 0x0000004F System.String Sirenix.OdinInspector.ShowIfGroupAttribute::get_MemberName()
extern void ShowIfGroupAttribute_get_MemberName_mB9AD99DB8A8B5360638026E9922C9D9FCB1F0F37 (void);
// 0x00000050 System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::set_MemberName(System.String)
extern void ShowIfGroupAttribute_set_MemberName_m89AC56CE0BC24BA6AE556D56B861693188FBE53D (void);
// 0x00000051 System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::.ctor(System.String,System.Boolean)
extern void ShowIfGroupAttribute__ctor_m13D173D317B53E20CF189BA7C556CF0D3AF1D6B6 (void);
// 0x00000052 System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::.ctor(System.String,System.Object,System.Boolean)
extern void ShowIfGroupAttribute__ctor_m41FD43D087BA9CA6EDD5D8358DF71C64072E2E64 (void);
// 0x00000053 System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void ShowIfGroupAttribute_CombineValuesWith_mD91213E356F6BD31308F03EBBCACE8F0D16865AF (void);
// 0x00000054 System.Void Sirenix.OdinInspector.ShowInInlineEditorsAttribute::.ctor()
extern void ShowInInlineEditorsAttribute__ctor_mE8C56C367F6E705FFE50ADDB98F406F49DA90543 (void);
// 0x00000055 System.Void Sirenix.OdinInspector.ShowPropertyResolverAttribute::.ctor()
extern void ShowPropertyResolverAttribute__ctor_m065342D6D4663EFB8CFCCFCA806B323D1E7F7586 (void);
// 0x00000056 System.Void Sirenix.OdinInspector.SuffixLabelAttribute::.ctor(System.String,System.Boolean)
extern void SuffixLabelAttribute__ctor_mE9AC15983814E1C792988828B1892520062B1A8F (void);
// 0x00000057 System.Void Sirenix.OdinInspector.TableColumnWidthAttribute::.ctor(System.Int32,System.Boolean)
extern void TableColumnWidthAttribute__ctor_mE43C5003B86E8E2A70D71C549C3DE40D9F94CBC0 (void);
// 0x00000058 System.Boolean Sirenix.OdinInspector.TableListAttribute::get_ShowPaging()
extern void TableListAttribute_get_ShowPaging_m4D907C1FCB384775DEBB760E88D10BB129776DAF (void);
// 0x00000059 System.Void Sirenix.OdinInspector.TableListAttribute::set_ShowPaging(System.Boolean)
extern void TableListAttribute_set_ShowPaging_m3827B6C698B94CE02171869DD8E9F1C8F4647688 (void);
// 0x0000005A System.Boolean Sirenix.OdinInspector.TableListAttribute::get_ShowPagingHasValue()
extern void TableListAttribute_get_ShowPagingHasValue_mDFE223AB05B1B0E12517F5E7306273EB0EC6ED67 (void);
// 0x0000005B System.Int32 Sirenix.OdinInspector.TableListAttribute::get_ScrollViewHeight()
extern void TableListAttribute_get_ScrollViewHeight_m9832A51C2B33CAFE93578C379313EC9CC0F98312 (void);
// 0x0000005C System.Void Sirenix.OdinInspector.TableListAttribute::set_ScrollViewHeight(System.Int32)
extern void TableListAttribute_set_ScrollViewHeight_mB27783D51812DBD474A5804E9FD91183735F7166 (void);
// 0x0000005D System.Void Sirenix.OdinInspector.TableListAttribute::.ctor()
extern void TableListAttribute__ctor_mA5BD0E2E7D0A0633559E1908447483EF6A265CA2 (void);
// 0x0000005E System.Void Sirenix.OdinInspector.TableMatrixAttribute::.ctor()
extern void TableMatrixAttribute__ctor_m66477FB1BF84EAFF3518CF83B2CA47CCC1D221E1 (void);
// 0x0000005F System.Void Sirenix.OdinInspector.TypeFilterAttribute::.ctor(System.String)
extern void TypeFilterAttribute__ctor_m6A29E4806E3CEDF292BF06AEF8D0817686E4BF3A (void);
// 0x00000060 System.Void Sirenix.OdinInspector.TypeInfoBoxAttribute::.ctor(System.String)
extern void TypeInfoBoxAttribute__ctor_mF44207CB9379DE5CCBA2598235C57141B55D9221 (void);
// 0x00000061 System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.String,System.Int32)
extern void VerticalGroupAttribute__ctor_m95C002377FFF8A8BFFCE1F1E1C9179A471E5CE06 (void);
// 0x00000062 System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.Int32)
extern void VerticalGroupAttribute__ctor_mD2074E6027586EE28B3E9B869A1A45F74DA01841 (void);
// 0x00000063 System.Void Sirenix.OdinInspector.VerticalGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void VerticalGroupAttribute_CombineValuesWith_mD0C5B314858254CC72D5783B6C644F91D8A80820 (void);
// 0x00000064 System.Void Sirenix.OdinInspector.IncludeMyAttributesAttribute::.ctor()
extern void IncludeMyAttributesAttribute__ctor_m791A833A21BD2049C497C812D1A1DE4F8532187E (void);
// 0x00000065 System.Void Sirenix.OdinInspector.OdinRegisterAttributeAttribute::.ctor(System.Type,System.String,System.String)
extern void OdinRegisterAttributeAttribute__ctor_m8E0FA6955933F971939209DFEC3ED94A48CAD251 (void);
// 0x00000066 System.Void Sirenix.OdinInspector.OdinRegisterAttributeAttribute::.ctor(System.Type,System.String,System.String,System.String)
extern void OdinRegisterAttributeAttribute__ctor_m5E496415C227140A2F40B9CAEB84A2776F549523 (void);
// 0x00000067 System.Void Sirenix.OdinInspector.TitleGroupAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.TitleAlignments,System.Boolean,System.Boolean,System.Boolean,System.Int32)
extern void TitleGroupAttribute__ctor_mF5CAD9A96B1403DA735CDB0DCF9655492EEA329D (void);
// 0x00000068 System.Void Sirenix.OdinInspector.TitleGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void TitleGroupAttribute_CombineValuesWith_mC9C34C1AFDA9BCA6F89BC30EBC3F4488F5B2BE1F (void);
// 0x00000069 System.Void Sirenix.OdinInspector.ColorPaletteAttribute::.ctor()
extern void ColorPaletteAttribute__ctor_mF55AD4A715288F20744E62E0FC19478F2DA6F7DC (void);
// 0x0000006A System.Void Sirenix.OdinInspector.ColorPaletteAttribute::.ctor(System.String)
extern void ColorPaletteAttribute__ctor_mC63330FD86282DBFD27C7B150C7BC288AA5BC099 (void);
// 0x0000006B System.Void Sirenix.OdinInspector.CustomContextMenuAttribute::.ctor(System.String,System.String)
extern void CustomContextMenuAttribute__ctor_mD899BC52D7DC9BA3D03DF6B350FA377D637BA294 (void);
// 0x0000006C System.Void Sirenix.OdinInspector.DelayedPropertyAttribute::.ctor()
extern void DelayedPropertyAttribute__ctor_m8C207C4888FE128658B5E3DA401BFA4A5C5EFB8B (void);
// 0x0000006D System.Void Sirenix.OdinInspector.DetailedInfoBoxAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType,System.String)
extern void DetailedInfoBoxAttribute__ctor_mEAD2D8A6DE093F1CAD461DBD308F8F9B02475A7E (void);
// 0x0000006E System.Void Sirenix.OdinInspector.DictionaryDrawerSettings::.ctor()
extern void DictionaryDrawerSettings__ctor_mFCE0FED8F84669CC694CB10353F5DDFAACBE9A24 (void);
// 0x0000006F System.Void Sirenix.OdinInspector.DisableContextMenuAttribute::.ctor(System.Boolean,System.Boolean)
extern void DisableContextMenuAttribute__ctor_m6D48381D7DDE1005D85BF6DA1880BDD8F86FF302 (void);
// 0x00000070 System.Void Sirenix.OdinInspector.DisableIfAttribute::.ctor(System.String)
extern void DisableIfAttribute__ctor_m7AB2E5AF098216CD048EF0DB08A33B754DA8C9D5 (void);
// 0x00000071 System.Void Sirenix.OdinInspector.DisableIfAttribute::.ctor(System.String,System.Object)
extern void DisableIfAttribute__ctor_m9DB7922AC1CCF5F478C36C54CFDBF6493BC1D270 (void);
// 0x00000072 System.Void Sirenix.OdinInspector.DisableInEditorModeAttribute::.ctor()
extern void DisableInEditorModeAttribute__ctor_mABA48A11350EF0AFB369D01BFC4062D87CD97619 (void);
// 0x00000073 System.Void Sirenix.OdinInspector.DisableInPlayModeAttribute::.ctor()
extern void DisableInPlayModeAttribute__ctor_mDBF6AAD9D639C4AFA15671A02FD76AAA05335A65 (void);
// 0x00000074 System.Void Sirenix.OdinInspector.DisplayAsStringAttribute::.ctor()
extern void DisplayAsStringAttribute__ctor_mC5C746490926B2D0E1ECD5C98307BF6BC584AC91 (void);
// 0x00000075 System.Void Sirenix.OdinInspector.DisplayAsStringAttribute::.ctor(System.Boolean)
extern void DisplayAsStringAttribute__ctor_mE07DDA6D8DC4557144E1A022DE5E4BAA8E46F609 (void);
// 0x00000076 System.Void Sirenix.OdinInspector.DontApplyToListElementsAttribute::.ctor()
extern void DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4 (void);
// 0x00000077 System.Void Sirenix.OdinInspector.DrawWithUnityAttribute::.ctor()
extern void DrawWithUnityAttribute__ctor_mCC3AE3ED0F2537183EEFE4E96675703CBDF9504E (void);
// 0x00000078 System.Void Sirenix.OdinInspector.InlineButtonAttribute::.ctor(System.String,System.String)
extern void InlineButtonAttribute__ctor_m03193A02CCD5D30E075F82C727BF6B7EDE1E86D4 (void);
// 0x00000079 System.String Sirenix.OdinInspector.InlineButtonAttribute::get_MemberMethod()
extern void InlineButtonAttribute_get_MemberMethod_mECE123FE218739415AE473D16E36D3FCF36042EE (void);
// 0x0000007A System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_MemberMethod(System.String)
extern void InlineButtonAttribute_set_MemberMethod_mF0C22FEE372BFE43D98A7B8FDD6BD29365CEDAE8 (void);
// 0x0000007B System.String Sirenix.OdinInspector.InlineButtonAttribute::get_Label()
extern void InlineButtonAttribute_get_Label_m06C931996AADA45AD653F209A5713A4AB09135E0 (void);
// 0x0000007C System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_Label(System.String)
extern void InlineButtonAttribute_set_Label_m24E6BE08859E432C9819B26B95BED6F00C3DC0D0 (void);
// 0x0000007D System.Void Sirenix.OdinInspector.ShowForPrefabOnlyAttribute::.ctor()
extern void ShowForPrefabOnlyAttribute__ctor_m2F3666A81E25D93DA70D60C45DC60E0013833E63 (void);
// 0x0000007E System.Void Sirenix.OdinInspector.EnableForPrefabOnlyAttribute::.ctor()
extern void EnableForPrefabOnlyAttribute__ctor_mE9234FA3C5FA64E15B740F718B6F1F70F519A2C3 (void);
// 0x0000007F System.Void Sirenix.OdinInspector.EnableIfAttribute::.ctor(System.String)
extern void EnableIfAttribute__ctor_m9A122D7791A2EE11AD822E9F4916D84456A38638 (void);
// 0x00000080 System.Void Sirenix.OdinInspector.EnableIfAttribute::.ctor(System.String,System.Object)
extern void EnableIfAttribute__ctor_m146B9D17703655B7BDAB4533643F99AFBC1D9954 (void);
// 0x00000081 System.Void Sirenix.OdinInspector.EnumToggleButtonsAttribute::.ctor()
extern void EnumToggleButtonsAttribute__ctor_m15D3C4BA2574E11C8A3EDCEF7DECF8347B13D7EA (void);
// 0x00000082 System.Void Sirenix.OdinInspector.HideIfAttribute::.ctor(System.String,System.Boolean)
extern void HideIfAttribute__ctor_m962D0D53FCBC0B38020E72D7E853C370B407CBBC (void);
// 0x00000083 System.Void Sirenix.OdinInspector.HideIfAttribute::.ctor(System.String,System.Object,System.Boolean)
extern void HideIfAttribute__ctor_m2C6BD6C12C7977150A6649FE4DFED494356860A1 (void);
// 0x00000084 System.Void Sirenix.OdinInspector.HideInPlayModeAttribute::.ctor()
extern void HideInPlayModeAttribute__ctor_mEE897C3BB786574921534D6E844AEF081F4FB626 (void);
// 0x00000085 System.Void Sirenix.OdinInspector.HideInEditorModeAttribute::.ctor()
extern void HideInEditorModeAttribute__ctor_m17A5C7A0282FF0400356659396C700515E02B396 (void);
// 0x00000086 System.Void Sirenix.OdinInspector.HideMonoScriptAttribute::.ctor()
extern void HideMonoScriptAttribute__ctor_m9BFB340F3A8244073A6474DEFE975F5345F972E5 (void);
// 0x00000087 System.Void Sirenix.OdinInspector.HideReferenceObjectPickerAttribute::.ctor()
extern void HideReferenceObjectPickerAttribute__ctor_mAC0A3B16BB2D6556939A9FF716AB46DFD8EC0B85 (void);
// 0x00000088 System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.String,System.Single,System.Int32,System.Int32,System.Int32)
extern void HorizontalGroupAttribute__ctor_m804D5778400B0BE529750A2E66F7EB85E993BAB5 (void);
// 0x00000089 System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.Single,System.Int32,System.Int32,System.Int32)
extern void HorizontalGroupAttribute__ctor_m765B7BD79095475543818FDB16C39307C98B8FD4 (void);
// 0x0000008A System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void HorizontalGroupAttribute_CombineValuesWith_mC240A69FB2055CD24D60C9C5601D2C688EACD103 (void);
// 0x0000008B System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorModes,Sirenix.OdinInspector.InlineEditorObjectFieldModes)
extern void InlineEditorAttribute__ctor_mA7C55B884AE2BFFA8538A0E8676FB42C0686A34B (void);
// 0x0000008C System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorObjectFieldModes)
extern void InlineEditorAttribute__ctor_m561C13A489F0CFB071822CF18F134842B72B4BFA (void);
// 0x0000008D System.Void Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute::.ctor()
extern void SuppressInvalidAttributeErrorAttribute__ctor_m19653C86F9E551600A888E146263C67DB1B12421 (void);
// 0x0000008E System.Void Sirenix.OdinInspector.ShowDrawerChainAttribute::.ctor()
extern void ShowDrawerChainAttribute__ctor_m0A63FAFA0F1F0430D816601D128666692AAC5BE2 (void);
// 0x0000008F System.Void Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute::.ctor()
extern void ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m6F540B2A92D616275408AEB55BC97B1500F6C9B3 (void);
// 0x00000090 System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::.ctor(System.String,System.Int32)
extern void FoldoutGroupAttribute__ctor_mE7F35B6748804620264F9B7F3BB39542FF93DE25 (void);
// 0x00000091 System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::.ctor(System.String,System.Boolean,System.Int32)
extern void FoldoutGroupAttribute__ctor_m2F82C5310E63CC2154A8C29AB1EA67571C19596B (void);
// 0x00000092 System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_HasDefinedExpanded()
extern void FoldoutGroupAttribute_get_HasDefinedExpanded_mF65E7E54B706CCDE7EA9DB0C3606F2B10DCFCB25 (void);
// 0x00000093 System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_HasDefinedExpanded(System.Boolean)
extern void FoldoutGroupAttribute_set_HasDefinedExpanded_mABAA99EDDE953EEF83E4F04F80DAC78D2E3851B3 (void);
// 0x00000094 System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void FoldoutGroupAttribute_CombineValuesWith_m771FE13B370A0AFF2B33E8D4A265CA77AAFDBA1F (void);
// 0x00000095 System.Void Sirenix.OdinInspector.GUIColorAttribute::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void GUIColorAttribute__ctor_mAF8F90539752BCB9D2DC4B9CD83ED40871593D76 (void);
// 0x00000096 System.Void Sirenix.OdinInspector.GUIColorAttribute::.ctor(System.String)
extern void GUIColorAttribute__ctor_m5CB8EFCA3A75F75FBC971CE083DF3090FADCA25E (void);
// 0x00000097 System.Void Sirenix.OdinInspector.HideLabelAttribute::.ctor()
extern void HideLabelAttribute__ctor_mCEC15AA42376E412DFA3661F34D4B38F5DA377D0 (void);
// 0x00000098 System.Void Sirenix.OdinInspector.IndentAttribute::.ctor(System.Int32)
extern void IndentAttribute__ctor_m4C4EA7CF24D165E19FDBF449EDE0BDE2EE0875FF (void);
// 0x00000099 System.Void Sirenix.OdinInspector.InfoBoxAttribute::.ctor(System.String,Sirenix.OdinInspector.InfoMessageType,System.String)
extern void InfoBoxAttribute__ctor_mEA63EA4FAF8364F7F5861E800232E06B027055CD (void);
// 0x0000009A System.Void Sirenix.OdinInspector.InfoBoxAttribute::.ctor(System.String,System.String)
extern void InfoBoxAttribute__ctor_mE6C3029629BB3B188D62C9D3A42688B2877281D6 (void);
// 0x0000009B System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.Single,System.Single,System.Boolean)
extern void MinMaxSliderAttribute__ctor_m73C32546B8D44A35C3BB658A1AB05EB9EF2AB504 (void);
// 0x0000009C System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.Single,System.Boolean)
extern void MinMaxSliderAttribute__ctor_mA60851FC601385F5021FB7A6C9C8201E465031D9 (void);
// 0x0000009D System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.Single,System.String,System.Boolean)
extern void MinMaxSliderAttribute__ctor_m97121D0D3A9907DE58A2B1B80BDCF7EAE25FC2BD (void);
// 0x0000009E System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.String,System.Boolean)
extern void MinMaxSliderAttribute__ctor_m2B0197624446EE2E82BA984F419B1F0A8A1313FA (void);
// 0x0000009F System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.Boolean)
extern void MinMaxSliderAttribute__ctor_mBB9822C30540B63E70E4BA2DBB29D998BC2BF5AB (void);
// 0x000000A0 System.Void Sirenix.OdinInspector.ToggleLeftAttribute::.ctor()
extern void ToggleLeftAttribute__ctor_mF3B2E5B94E8017E707C11B8A24245E466A41B370 (void);
// 0x000000A1 System.Void Sirenix.OdinInspector.LabelTextAttribute::.ctor(System.String)
extern void LabelTextAttribute__ctor_m120AFF29F6C78A1294923EF793C32D1535C6C202 (void);
// 0x000000A2 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowPaging()
extern void ListDrawerSettingsAttribute_get_ShowPaging_m04B50A37D720C75A3C2E44102538E3C1F6C78B4A (void);
// 0x000000A3 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowPaging(System.Boolean)
extern void ListDrawerSettingsAttribute_set_ShowPaging_mA98F384C28AEB5CF37229A9467E3D714703BB775 (void);
// 0x000000A4 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_DraggableItems()
extern void ListDrawerSettingsAttribute_get_DraggableItems_mB17C6C50B8BD7EA78D57725B196706F3CADF6D45 (void);
// 0x000000A5 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_DraggableItems(System.Boolean)
extern void ListDrawerSettingsAttribute_set_DraggableItems_m8B0B96997D2F66F64D75FE1FF189822B36A083EE (void);
// 0x000000A6 System.Int32 Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_NumberOfItemsPerPage()
extern void ListDrawerSettingsAttribute_get_NumberOfItemsPerPage_m0AD386F6554043FBDD495AA7DC702C77BE4EDE14 (void);
// 0x000000A7 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_NumberOfItemsPerPage(System.Int32)
extern void ListDrawerSettingsAttribute_set_NumberOfItemsPerPage_m8993312EAEF84406A3DF92FC426E5103FF28F3B9 (void);
// 0x000000A8 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_IsReadOnly()
extern void ListDrawerSettingsAttribute_get_IsReadOnly_mF9F88EA01F82CEE70F43077211CE1F04EB90F7F1 (void);
// 0x000000A9 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_IsReadOnly(System.Boolean)
extern void ListDrawerSettingsAttribute_set_IsReadOnly_m083597FF5318279936B6F201BE6CF39394F5879C (void);
// 0x000000AA System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowItemCount()
extern void ListDrawerSettingsAttribute_get_ShowItemCount_mD1E9AC4E9DE4F9B3770092433EA5CFDCE13BF991 (void);
// 0x000000AB System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowItemCount(System.Boolean)
extern void ListDrawerSettingsAttribute_set_ShowItemCount_m665258C5382FD7E1644B0D2C0CDB091E5BC24233 (void);
// 0x000000AC System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_Expanded()
extern void ListDrawerSettingsAttribute_get_Expanded_mE02779BAA3ED21A4496BBC18FDD943AD547BA13B (void);
// 0x000000AD System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_Expanded(System.Boolean)
extern void ListDrawerSettingsAttribute_set_Expanded_m8F83C911B15D8D27A52E21ECCE51E19B8D9320B4 (void);
// 0x000000AE System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowIndexLabels()
extern void ListDrawerSettingsAttribute_get_ShowIndexLabels_m4AAEB41041F7C12CADDE07667D18FCD5C61EF5EB (void);
// 0x000000AF System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowIndexLabels(System.Boolean)
extern void ListDrawerSettingsAttribute_set_ShowIndexLabels_m133A99569AC66462AFA79DFDD9E252939E623830 (void);
// 0x000000B0 System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_OnTitleBarGUI()
extern void ListDrawerSettingsAttribute_get_OnTitleBarGUI_m9E6161CFD836FD472193734DDE1597ED6CB171B6 (void);
// 0x000000B1 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_OnTitleBarGUI(System.String)
extern void ListDrawerSettingsAttribute_set_OnTitleBarGUI_m424220A303257C85E1792B9A1423C42574224EBD (void);
// 0x000000B2 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_PagingHasValue()
extern void ListDrawerSettingsAttribute_get_PagingHasValue_m9550E5B462570C1C522622F2D5C03310A6FE8843 (void);
// 0x000000B3 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowItemCountHasValue()
extern void ListDrawerSettingsAttribute_get_ShowItemCountHasValue_mB1DFCCC7B480597A8451D731BC3543891DBC39DC (void);
// 0x000000B4 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_NumberOfItemsPerPageHasValue()
extern void ListDrawerSettingsAttribute_get_NumberOfItemsPerPageHasValue_mAD690824656C5003CFC3F2E76C1EAA916BCDEBA6 (void);
// 0x000000B5 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_DraggableHasValue()
extern void ListDrawerSettingsAttribute_get_DraggableHasValue_m664E10C5C14E9DC9496343C5AA725E0371E029E5 (void);
// 0x000000B6 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_IsReadOnlyHasValue()
extern void ListDrawerSettingsAttribute_get_IsReadOnlyHasValue_m6006332D62E945FE658FB12805B036CCACD3FA58 (void);
// 0x000000B7 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ExpandedHasValue()
extern void ListDrawerSettingsAttribute_get_ExpandedHasValue_m0B6F2D13EA28AE62EB962846B74AB7859E6C8CFA (void);
// 0x000000B8 System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowIndexLabelsHasValue()
extern void ListDrawerSettingsAttribute_get_ShowIndexLabelsHasValue_m2257AF08FAA5E5A21D4E27DF92FA787A969002E8 (void);
// 0x000000B9 System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::.ctor()
extern void ListDrawerSettingsAttribute__ctor_m89C7EEDBDF4AC27489757F0E307486830B47E9ED (void);
// 0x000000BA System.Void Sirenix.OdinInspector.MaxValueAttribute::.ctor(System.Double)
extern void MaxValueAttribute__ctor_m0F7C5417770DC83172C27B6622E3A2CA1942C3DA (void);
// 0x000000BB System.Void Sirenix.OdinInspector.MaxValueAttribute::.ctor(System.String)
extern void MaxValueAttribute__ctor_m2DECAE60D96504B2869870AEC5A9277A3C155D3A (void);
// 0x000000BC System.Void Sirenix.OdinInspector.MinValueAttribute::.ctor(System.Double)
extern void MinValueAttribute__ctor_m655347F068916BDDCB0DB745A3EF3F6B7462F9C8 (void);
// 0x000000BD System.Void Sirenix.OdinInspector.MinValueAttribute::.ctor(System.String)
extern void MinValueAttribute__ctor_m78683ABAE34F7DDBA6D6CB08132F4AF61AA1669A (void);
// 0x000000BE System.Void Sirenix.OdinInspector.MultiLinePropertyAttribute::.ctor(System.Int32)
extern void MultiLinePropertyAttribute__ctor_m53CC75198F796D0C231E74BACD67F2E7A57B3EE0 (void);
// 0x000000BF System.Void Sirenix.OdinInspector.PropertyTooltipAttribute::.ctor(System.String)
extern void PropertyTooltipAttribute__ctor_m71942469D93E0D7ACEA1B3038C933AAA232C741C (void);
// 0x000000C0 System.Void Sirenix.OdinInspector.ReadOnlyAttribute::.ctor()
extern void ReadOnlyAttribute__ctor_m6C0720DC3F32C28CA3D686B112F3CA7405B08CBA (void);
// 0x000000C1 System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor()
extern void OnInspectorGUIAttribute__ctor_mDDC5EEE9089AF4C1AD2F72C23E3432DE38D7C881 (void);
// 0x000000C2 System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor(System.String,System.Boolean)
extern void OnInspectorGUIAttribute__ctor_mC4B82BFF792E2DC9774313DF104F6229A9BA68AA (void);
// 0x000000C3 System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor(System.String,System.String)
extern void OnInspectorGUIAttribute__ctor_mAC47FD49A86EF8BEED1271E1372E011341BE3936 (void);
// 0x000000C4 System.Void Sirenix.OdinInspector.OnValueChangedAttribute::.ctor(System.String,System.Boolean)
extern void OnValueChangedAttribute__ctor_mABB73C7E36822F4B7FFFCBB1BC4527D4A738093D (void);
// 0x000000C5 System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String,System.Int32)
extern void PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA (void);
// 0x000000C6 System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String)
extern void PropertyGroupAttribute__ctor_m97E8952CDF6B230904482158001DAB88CA24F819 (void);
// 0x000000C7 Sirenix.OdinInspector.PropertyGroupAttribute Sirenix.OdinInspector.PropertyGroupAttribute::Combine(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void PropertyGroupAttribute_Combine_m428F9CF2608153498BE167A9BDE7F139668B237F (void);
// 0x000000C8 System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void PropertyGroupAttribute_CombineValuesWith_mE807B73DCF4C6A9C88AA9BCD40E881733AA03AE0 (void);
// 0x000000C9 System.Void Sirenix.OdinInspector.PropertyOrderAttribute::.ctor()
extern void PropertyOrderAttribute__ctor_m75C3FDE0E6665079956F631BFB910048CEBE3F70 (void);
// 0x000000CA System.Void Sirenix.OdinInspector.PropertyOrderAttribute::.ctor(System.Int32)
extern void PropertyOrderAttribute__ctor_m2807F2D02F63E9E1B6AE80787BB8F72719D68812 (void);
// 0x000000CB System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor()
extern void RequiredAttribute__ctor_m3A7A9539E19CA3D8154225B63AE8E0F624CA0517 (void);
// 0x000000CC System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(System.String,Sirenix.OdinInspector.InfoMessageType)
extern void RequiredAttribute__ctor_m6A2BBE37D802335237FE8CF510CD725AAD609D10 (void);
// 0x000000CD System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(System.String)
extern void RequiredAttribute__ctor_m4DF877C8136908A9C54783F10C4D6DAB2D440AF2 (void);
// 0x000000CE System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(Sirenix.OdinInspector.InfoMessageType)
extern void RequiredAttribute__ctor_mB9C90488A06DD41E9D39D3EBEA88510CBD0E826C (void);
// 0x000000CF System.Void Sirenix.OdinInspector.SceneObjectsOnlyAttribute::.ctor()
extern void SceneObjectsOnlyAttribute__ctor_m6FC75F3244D4F334145DCCDB287B70227988AEA8 (void);
// 0x000000D0 System.Void Sirenix.OdinInspector.ValueDropdownAttribute::.ctor(System.String)
extern void ValueDropdownAttribute__ctor_mD364F567BD9B8DFA8169C42D6256502B253ED3B8 (void);
// 0x000000D1 System.String Sirenix.OdinInspector.IValueDropdownItem::GetText()
// 0x000000D2 System.Object Sirenix.OdinInspector.IValueDropdownItem::GetValue()
// 0x000000D3 System.Void Sirenix.OdinInspector.ValueDropdownList`1::Add(System.String,T)
// 0x000000D4 System.Void Sirenix.OdinInspector.ValueDropdownList`1::Add(T)
// 0x000000D5 System.Void Sirenix.OdinInspector.ValueDropdownList`1::.ctor()
// 0x000000D6 System.Void Sirenix.OdinInspector.ValueDropdownItem::.ctor(System.String,System.Object)
extern void ValueDropdownItem__ctor_mCCB87C57D5FF6019771592E2E52B910C13590ACC_AdjustorThunk (void);
// 0x000000D7 System.String Sirenix.OdinInspector.ValueDropdownItem::ToString()
extern void ValueDropdownItem_ToString_m809FC023CF5BE24E964513E064156CC3E405F300_AdjustorThunk (void);
// 0x000000D8 System.String Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetText()
extern void ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mDE36C60532902791E5E6C2AF39297AD60F3ED9B1_AdjustorThunk (void);
// 0x000000D9 System.Object Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetValue()
extern void ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m8FB6D5B472D5D524135DE9A0DBB78CACC7D617A2_AdjustorThunk (void);
// 0x000000DA System.Void Sirenix.OdinInspector.ValueDropdownItem`1::.ctor(System.String,T)
// 0x000000DB System.String Sirenix.OdinInspector.ValueDropdownItem`1::Sirenix.OdinInspector.IValueDropdownItem.GetText()
// 0x000000DC System.Object Sirenix.OdinInspector.ValueDropdownItem`1::Sirenix.OdinInspector.IValueDropdownItem.GetValue()
// 0x000000DD System.String Sirenix.OdinInspector.ValueDropdownItem`1::ToString()
// 0x000000DE System.Void Sirenix.OdinInspector.ShowInInspectorAttribute::.ctor()
extern void ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233 (void);
// 0x000000DF System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.Boolean,System.Int32)
extern void TabGroupAttribute__ctor_mF8DF0A25262FF59F9CAFB79B49032DDABBA68A32 (void);
// 0x000000E0 System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.String,System.Boolean,System.Int32)
extern void TabGroupAttribute__ctor_m962E789C945DC9AC4EA826DCF7979A2A5733090B (void);
// 0x000000E1 System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::get_Tabs()
extern void TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25 (void);
// 0x000000E2 System.Void Sirenix.OdinInspector.TabGroupAttribute::set_Tabs(System.Collections.Generic.List`1<System.String>)
extern void TabGroupAttribute_set_Tabs_m5BB49FEBE6589933B4F2B96B581E9EE4DC5838B4 (void);
// 0x000000E3 System.Void Sirenix.OdinInspector.TabGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void TabGroupAttribute_CombineValuesWith_mAF271825C72EF38F95673C2A30D695F3C6CB5C96 (void);
// 0x000000E4 System.Collections.Generic.IList`1<Sirenix.OdinInspector.PropertyGroupAttribute> Sirenix.OdinInspector.TabGroupAttribute::Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute.GetSubGroupAttributes()
extern void TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_mB8B9EE5668DBC7DDE649C740D59FE0BBA177659B (void);
// 0x000000E5 System.String Sirenix.OdinInspector.TabGroupAttribute::Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute.RepathMemberAttribute(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_mB501706A09619041158432FBA3B9758F841C01E5 (void);
// 0x000000E6 System.Void Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute::.ctor(System.String,System.Int32)
extern void TabSubGroupAttribute__ctor_mBD891E55D898A286A39966CAB7BD0D9BF4E18F39 (void);
// 0x000000E7 System.Void Sirenix.OdinInspector.TitleAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.TitleAlignments,System.Boolean,System.Boolean)
extern void TitleAttribute__ctor_m2EEB321011887FAD4345C6DE1C9540114AF6765C (void);
// 0x000000E8 System.Void Sirenix.OdinInspector.ToggleAttribute::.ctor(System.String)
extern void ToggleAttribute__ctor_m04A2021D61D0895881BB99A0958EDED31ED2C12A (void);
// 0x000000E9 System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Int32,System.String)
extern void ToggleGroupAttribute__ctor_m02F6C8A534512612492AB9C3C29227776F25CC66 (void);
// 0x000000EA System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.String)
extern void ToggleGroupAttribute__ctor_m9B9BE888CACC61FEB298262544065950F500F85E (void);
// 0x000000EB System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Int32,System.String,System.String)
extern void ToggleGroupAttribute__ctor_mCE42334D8F057E33C717935C60446068B17B1AE8 (void);
// 0x000000EC System.String Sirenix.OdinInspector.ToggleGroupAttribute::get_ToggleMemberName()
extern void ToggleGroupAttribute_get_ToggleMemberName_m04044CD0EAD5C59A69AAC83ED7C8DCF6D4C0A261 (void);
// 0x000000ED System.String Sirenix.OdinInspector.ToggleGroupAttribute::get_TitleStringMemberName()
extern void ToggleGroupAttribute_get_TitleStringMemberName_m7A12EF988A3F9A85FAAAEF843B62E52C0BF10D20 (void);
// 0x000000EE System.Void Sirenix.OdinInspector.ToggleGroupAttribute::set_TitleStringMemberName(System.String)
extern void ToggleGroupAttribute_set_TitleStringMemberName_m3354817FD279639CAE69FF7C3B431854763B55CF (void);
// 0x000000EF System.Void Sirenix.OdinInspector.ToggleGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
extern void ToggleGroupAttribute_CombineValuesWith_mF7D00256747558CF46D44456114681365FF5DAC4 (void);
// 0x000000F0 System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::get_ContiniousValidationCheck()
extern void ValidateInputAttribute_get_ContiniousValidationCheck_m57675CD7DD7EFACDD1242343307F36739A70ADD8 (void);
// 0x000000F1 System.Void Sirenix.OdinInspector.ValidateInputAttribute::set_ContiniousValidationCheck(System.Boolean)
extern void ValidateInputAttribute_set_ContiniousValidationCheck_m7EF3D2DB874DACA93D72468D6F5B6E371B832468 (void);
// 0x000000F2 System.Void Sirenix.OdinInspector.ValidateInputAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType)
extern void ValidateInputAttribute__ctor_m8515D985050435F347600891E06340B5AE18C879 (void);
// 0x000000F3 System.Void Sirenix.OdinInspector.ValidateInputAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType,System.Boolean)
extern void ValidateInputAttribute__ctor_m64BCF6EB7B972FD8563BA5A7E14CFEA80D122038 (void);
// 0x000000F4 System.Void Sirenix.OdinInspector.ShowIfAttribute::.ctor(System.String,System.Boolean)
extern void ShowIfAttribute__ctor_m04695ED0E3854D2DCB01DE13F17900053A70F3D6 (void);
// 0x000000F5 System.Void Sirenix.OdinInspector.ShowIfAttribute::.ctor(System.String,System.Object,System.Boolean)
extern void ShowIfAttribute__ctor_m6D02DEC959A55EF7135A0E1AF02681D4AE43419E (void);
// 0x000000F6 System.Void Sirenix.OdinInspector.WrapAttribute::.ctor(System.Double,System.Double)
extern void WrapAttribute__ctor_mCBFEB1652B6B6062E9D5620E2352B6305F591802 (void);
// 0x000000F7 System.Collections.Generic.IList`1<Sirenix.OdinInspector.PropertyGroupAttribute> Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute::GetSubGroupAttributes()
// 0x000000F8 System.String Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute::RepathMemberAttribute(Sirenix.OdinInspector.PropertyGroupAttribute)
static Il2CppMethodPointer s_methodPointers[248] = 
{
	AssetListAttribute__ctor_mD7BEF52C5E192D3C71CDD7E151B5C69C05FF7544,
	AssetSelectorAttribute_set_Paths_m26DA529BF216F8FF20C13B7346D81BBBFF7C7DE5,
	AssetSelectorAttribute_get_Paths_m539145D9BC2B1F42338AF9F327F54E9B055D752C,
	AssetSelectorAttribute__ctor_m4040E0B057B335CE1478EFA6EB099118FDFC0CC2,
	U3CU3Ec__cctor_m7094437B7D37D1E6FF4DD391765535E524599282,
	U3CU3Ec__ctor_mEF4D05798CE3F378E5E36DF88F14353C7CF9E182,
	U3CU3Ec_U3Cset_PathsU3Eb__12_0_mB1598E3D172430978C835A9B4E1A1268891DF76D,
	AssetsOnlyAttribute__ctor_m33B3ADA2CD2AB296AA0FC62AA4BCAA8CA56D6DC6,
	BoxGroupAttribute__ctor_mD170E60B450AABCFDC796021EE2DD187F55E6500,
	BoxGroupAttribute__ctor_m6B421EB7331331EB7D1B56C34DE47AE46266C2C3,
	BoxGroupAttribute_CombineValuesWith_mE5E414B3F475B56507C8FC5A2B20A60261CA2EEF,
	ButtonAttribute__ctor_mA189093060719DE77084CF59BFAA6CBBA9AE5BBA,
	ButtonAttribute__ctor_m54AC64264E1D03E7A49DA7BAF9BFFDE89C45C9F1,
	ButtonAttribute__ctor_mF2F44124675D1722CFD80947CA439B6CA6D9B61E,
	ButtonAttribute__ctor_m83C5D44DAF14B09929680979D9E25DFF2F980A30,
	ButtonAttribute__ctor_mFDCA00462D14D14FF5EF79B7AF3D602C8592244A,
	ButtonAttribute__ctor_mAD43B91B8D486F4C6DFD046E5B3EA15999F8DF19,
	ButtonAttribute__ctor_m40DDBADB260CF6641F37A7816F40D28D505A72C5,
	ButtonAttribute__ctor_m7BE4CA206B38073753DB64895530A4E95732DEF4,
	ButtonAttribute__ctor_mA3C5891F8246BF6A351C2A083391F60BCC6B24FF,
	ButtonAttribute__ctor_m8C9AB7A45AE05F9214E955FD582DD1B6570F65C7,
	ButtonAttribute__ctor_mC75E79D918637EE750DEFA31D00792CC5BBA8B7D,
	ButtonAttribute__ctor_mA5567E1BCD7D54A2EAC16AA5FEC0575A1A532EDB,
	ButtonGroupAttribute__ctor_m34D3B7CC10A78BA26C227283792D56AE2DFBD04A,
	ChildGameObjectsOnlyAttribute__ctor_mDDEA4F0844C178B7145CBABE91827E784862CEB7,
	CustomValueDrawerAttribute__ctor_m1AD80BD54925742FECC79F2BAF67F844EBBF83A3,
	DisableInInlineEditorsAttribute__ctor_m80B5355794D7D985E91C1407E0710BA1D37B943A,
	DisableInNonPrefabsAttribute__ctor_m76759AE55534012C1DD807020329CC7796DBAA7C,
	DisableInPrefabAssetsAttribute__ctor_m43BC5A861668B7A9AEC41E63C3841D138177284F,
	DisableInPrefabInstancesAttribute__ctor_m874C04BDC1F94EC2270E7789A634CA654EDD54FE,
	DisableInPrefabsAttribute__ctor_mBE3358BF4C37BE9CA4CFD07FFAEB75646772B260,
	DoNotDrawAsReferenceAttribute__ctor_m47147F6256197B0FDB1F2E13D9C65DF661E18653,
	EnableGUIAttribute__ctor_m11708B222C12CFD9CAB97B0919A83C0A68A846B1,
	EnumPagingAttribute__ctor_mCDBD71F100FE496FEA9D1E64F5D9B77AE1621272,
	FilePathAttribute_get_ReadOnly_m3A6314406C2D58C15799C01B671EE88CC4399D70,
	FilePathAttribute_set_ReadOnly_mC86951B771DBB611C650516E350D4DA4EADA3ECD,
	FilePathAttribute__ctor_m9ED4FCDC100FF01750AD869DB5EDB9060171DAF2,
	FolderPathAttribute__ctor_m66C482346C8479FE547313BA678A83E9A9495FAC,
	HideDuplicateReferenceBoxAttribute__ctor_m2C45CE67CEC79FD4DCB816BF86E95AD95CADB89E,
	HideIfGroupAttribute_get_MemberName_m2F189429E0B3943DFEA543D0E205E6F5022682BF,
	HideIfGroupAttribute_set_MemberName_m6839D915223254EE1B9F9A24AFDF46A8CCBB8A1E,
	HideIfGroupAttribute__ctor_m986B873A48DE6DDCB2E6D6F1F304704ADACF9501,
	HideIfGroupAttribute__ctor_m9B6851A6ACEFCA54B488B2E3FB8D59CE3A660B04,
	HideIfGroupAttribute_CombineValuesWith_mB19848141AE80E205B5C72A3439CFA60C2A5FE52,
	HideInInlineEditorsAttribute__ctor_mBB67C06431BCD0DB7796A4B8EE94A1C5681D64BF,
	HideInNonPrefabsAttribute__ctor_mBD588D03AB995CC3ABFBC1C67D35DA9740FD1AF2,
	HideInPrefabAssetsAttribute__ctor_m7C32BAA642A467D80B5395E6B137E1CBB08952FB,
	HideInPrefabInstancesAttribute__ctor_m24FCF4C7A203AC9BEE774ADF1E99A7A328157A5F,
	HideInPrefabsAttribute__ctor_mCAA14A340A0EA0F4B76DA56B295BD084806C5EB5,
	HideInTablesAttribute__ctor_m7C2FA177C1B63C744039018A1B9631740FDCFB69,
	HideNetworkBehaviourFieldsAttribute__ctor_mF14C2E8AC55D25A6755F85402C9F9D16EFD86571,
	InlinePropertyAttribute__ctor_m89DE665FBEDCA7C8719E3A71D152F42C57F99F14,
	LabelWidthAttribute__ctor_m50D992D5EF45F1422D89592679A4A00370B38269,
	PreviewFieldAttribute__ctor_mC099E62E5FAC38708BC45556F8C6E6233D689740,
	PreviewFieldAttribute__ctor_mFEDC314B35C519267808A58D7E9264027E418660,
	PreviewFieldAttribute__ctor_m9AC2A039712B5273C77B62CE22D0147500E0EC1D,
	PreviewFieldAttribute__ctor_mA69C62B5B596B6BE59A938C40FDBDDD10E3A4294,
	ProgressBarAttribute__ctor_m6798524CD0FD36BF262D07CE49731D29C6034C98,
	ProgressBarAttribute__ctor_m2D683B9926AD93ED85E99E6A982245C37FFFAE62,
	ProgressBarAttribute__ctor_m548557B8A039AF4A656F9CF1849E312DB3EEA95F,
	ProgressBarAttribute__ctor_m449BBE33DABF66BD8E56261E35A71C58E0A6C7F2,
	ProgressBarAttribute_get_DrawValueLabel_m8755D428C189B82B8423283EAC1ECE16E828812E,
	ProgressBarAttribute_set_DrawValueLabel_mB76F6D2A75922450777B1FC0BA8E5E39315EA968,
	ProgressBarAttribute_get_DrawValueLabelHasValue_m06BCD66013F03A68A6C08FF5D7A49AEF316F71D9,
	ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624,
	ProgressBarAttribute_get_ValueLabelAlignment_mB6B666D410FFE91353B85A24A53AE60782CB3E2A,
	ProgressBarAttribute_set_ValueLabelAlignment_mE8366B95F522B01603C426704B9047FFE3BCF742,
	ProgressBarAttribute_get_ValueLabelAlignmentHasValue_m7DC1402D717D0D4EE8E72DF81C941D244509A447,
	ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0,
	PropertyRangeAttribute__ctor_m31FCD91543E6D7E0AE3B00A9B2BB744CCA2725AF,
	PropertyRangeAttribute__ctor_m442057690400645107E37F5C12D7D976269A15EE,
	PropertyRangeAttribute__ctor_mBC695897F24EF4544EC25B5E9876165B8ED1FA77,
	PropertyRangeAttribute__ctor_m9EBF44FAF1CFE1EDC513180C6EBB28B6B9E786BE,
	PropertySpaceAttribute__ctor_m83A768EB9D318E84B668055F808E5F392D7C32EB,
	PropertySpaceAttribute__ctor_m76955C7DE89CAC865AA6055D4DB629D46FF7CAA0,
	PropertySpaceAttribute__ctor_mB343CE0B6B517B4A78002E95B34A5278629A8285,
	ResponsiveButtonGroupAttribute__ctor_m4F03C6D6CC22A5B4586F993A4E4BBC8CE4320D8F,
	ResponsiveButtonGroupAttribute_CombineValuesWith_mE025C2906C58CCD9A5A71A2758B0B014C1A00910,
	ShowIfGroupAttribute_get_MemberName_mB9AD99DB8A8B5360638026E9922C9D9FCB1F0F37,
	ShowIfGroupAttribute_set_MemberName_m89AC56CE0BC24BA6AE556D56B861693188FBE53D,
	ShowIfGroupAttribute__ctor_m13D173D317B53E20CF189BA7C556CF0D3AF1D6B6,
	ShowIfGroupAttribute__ctor_m41FD43D087BA9CA6EDD5D8358DF71C64072E2E64,
	ShowIfGroupAttribute_CombineValuesWith_mD91213E356F6BD31308F03EBBCACE8F0D16865AF,
	ShowInInlineEditorsAttribute__ctor_mE8C56C367F6E705FFE50ADDB98F406F49DA90543,
	ShowPropertyResolverAttribute__ctor_m065342D6D4663EFB8CFCCFCA806B323D1E7F7586,
	SuffixLabelAttribute__ctor_mE9AC15983814E1C792988828B1892520062B1A8F,
	TableColumnWidthAttribute__ctor_mE43C5003B86E8E2A70D71C549C3DE40D9F94CBC0,
	TableListAttribute_get_ShowPaging_m4D907C1FCB384775DEBB760E88D10BB129776DAF,
	TableListAttribute_set_ShowPaging_m3827B6C698B94CE02171869DD8E9F1C8F4647688,
	TableListAttribute_get_ShowPagingHasValue_mDFE223AB05B1B0E12517F5E7306273EB0EC6ED67,
	TableListAttribute_get_ScrollViewHeight_m9832A51C2B33CAFE93578C379313EC9CC0F98312,
	TableListAttribute_set_ScrollViewHeight_mB27783D51812DBD474A5804E9FD91183735F7166,
	TableListAttribute__ctor_mA5BD0E2E7D0A0633559E1908447483EF6A265CA2,
	TableMatrixAttribute__ctor_m66477FB1BF84EAFF3518CF83B2CA47CCC1D221E1,
	TypeFilterAttribute__ctor_m6A29E4806E3CEDF292BF06AEF8D0817686E4BF3A,
	TypeInfoBoxAttribute__ctor_mF44207CB9379DE5CCBA2598235C57141B55D9221,
	VerticalGroupAttribute__ctor_m95C002377FFF8A8BFFCE1F1E1C9179A471E5CE06,
	VerticalGroupAttribute__ctor_mD2074E6027586EE28B3E9B869A1A45F74DA01841,
	VerticalGroupAttribute_CombineValuesWith_mD0C5B314858254CC72D5783B6C644F91D8A80820,
	IncludeMyAttributesAttribute__ctor_m791A833A21BD2049C497C812D1A1DE4F8532187E,
	OdinRegisterAttributeAttribute__ctor_m8E0FA6955933F971939209DFEC3ED94A48CAD251,
	OdinRegisterAttributeAttribute__ctor_m5E496415C227140A2F40B9CAEB84A2776F549523,
	TitleGroupAttribute__ctor_mF5CAD9A96B1403DA735CDB0DCF9655492EEA329D,
	TitleGroupAttribute_CombineValuesWith_mC9C34C1AFDA9BCA6F89BC30EBC3F4488F5B2BE1F,
	ColorPaletteAttribute__ctor_mF55AD4A715288F20744E62E0FC19478F2DA6F7DC,
	ColorPaletteAttribute__ctor_mC63330FD86282DBFD27C7B150C7BC288AA5BC099,
	CustomContextMenuAttribute__ctor_mD899BC52D7DC9BA3D03DF6B350FA377D637BA294,
	DelayedPropertyAttribute__ctor_m8C207C4888FE128658B5E3DA401BFA4A5C5EFB8B,
	DetailedInfoBoxAttribute__ctor_mEAD2D8A6DE093F1CAD461DBD308F8F9B02475A7E,
	DictionaryDrawerSettings__ctor_mFCE0FED8F84669CC694CB10353F5DDFAACBE9A24,
	DisableContextMenuAttribute__ctor_m6D48381D7DDE1005D85BF6DA1880BDD8F86FF302,
	DisableIfAttribute__ctor_m7AB2E5AF098216CD048EF0DB08A33B754DA8C9D5,
	DisableIfAttribute__ctor_m9DB7922AC1CCF5F478C36C54CFDBF6493BC1D270,
	DisableInEditorModeAttribute__ctor_mABA48A11350EF0AFB369D01BFC4062D87CD97619,
	DisableInPlayModeAttribute__ctor_mDBF6AAD9D639C4AFA15671A02FD76AAA05335A65,
	DisplayAsStringAttribute__ctor_mC5C746490926B2D0E1ECD5C98307BF6BC584AC91,
	DisplayAsStringAttribute__ctor_mE07DDA6D8DC4557144E1A022DE5E4BAA8E46F609,
	DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4,
	DrawWithUnityAttribute__ctor_mCC3AE3ED0F2537183EEFE4E96675703CBDF9504E,
	InlineButtonAttribute__ctor_m03193A02CCD5D30E075F82C727BF6B7EDE1E86D4,
	InlineButtonAttribute_get_MemberMethod_mECE123FE218739415AE473D16E36D3FCF36042EE,
	InlineButtonAttribute_set_MemberMethod_mF0C22FEE372BFE43D98A7B8FDD6BD29365CEDAE8,
	InlineButtonAttribute_get_Label_m06C931996AADA45AD653F209A5713A4AB09135E0,
	InlineButtonAttribute_set_Label_m24E6BE08859E432C9819B26B95BED6F00C3DC0D0,
	ShowForPrefabOnlyAttribute__ctor_m2F3666A81E25D93DA70D60C45DC60E0013833E63,
	EnableForPrefabOnlyAttribute__ctor_mE9234FA3C5FA64E15B740F718B6F1F70F519A2C3,
	EnableIfAttribute__ctor_m9A122D7791A2EE11AD822E9F4916D84456A38638,
	EnableIfAttribute__ctor_m146B9D17703655B7BDAB4533643F99AFBC1D9954,
	EnumToggleButtonsAttribute__ctor_m15D3C4BA2574E11C8A3EDCEF7DECF8347B13D7EA,
	HideIfAttribute__ctor_m962D0D53FCBC0B38020E72D7E853C370B407CBBC,
	HideIfAttribute__ctor_m2C6BD6C12C7977150A6649FE4DFED494356860A1,
	HideInPlayModeAttribute__ctor_mEE897C3BB786574921534D6E844AEF081F4FB626,
	HideInEditorModeAttribute__ctor_m17A5C7A0282FF0400356659396C700515E02B396,
	HideMonoScriptAttribute__ctor_m9BFB340F3A8244073A6474DEFE975F5345F972E5,
	HideReferenceObjectPickerAttribute__ctor_mAC0A3B16BB2D6556939A9FF716AB46DFD8EC0B85,
	HorizontalGroupAttribute__ctor_m804D5778400B0BE529750A2E66F7EB85E993BAB5,
	HorizontalGroupAttribute__ctor_m765B7BD79095475543818FDB16C39307C98B8FD4,
	HorizontalGroupAttribute_CombineValuesWith_mC240A69FB2055CD24D60C9C5601D2C688EACD103,
	InlineEditorAttribute__ctor_mA7C55B884AE2BFFA8538A0E8676FB42C0686A34B,
	InlineEditorAttribute__ctor_m561C13A489F0CFB071822CF18F134842B72B4BFA,
	SuppressInvalidAttributeErrorAttribute__ctor_m19653C86F9E551600A888E146263C67DB1B12421,
	ShowDrawerChainAttribute__ctor_m0A63FAFA0F1F0430D816601D128666692AAC5BE2,
	ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m6F540B2A92D616275408AEB55BC97B1500F6C9B3,
	FoldoutGroupAttribute__ctor_mE7F35B6748804620264F9B7F3BB39542FF93DE25,
	FoldoutGroupAttribute__ctor_m2F82C5310E63CC2154A8C29AB1EA67571C19596B,
	FoldoutGroupAttribute_get_HasDefinedExpanded_mF65E7E54B706CCDE7EA9DB0C3606F2B10DCFCB25,
	FoldoutGroupAttribute_set_HasDefinedExpanded_mABAA99EDDE953EEF83E4F04F80DAC78D2E3851B3,
	FoldoutGroupAttribute_CombineValuesWith_m771FE13B370A0AFF2B33E8D4A265CA77AAFDBA1F,
	GUIColorAttribute__ctor_mAF8F90539752BCB9D2DC4B9CD83ED40871593D76,
	GUIColorAttribute__ctor_m5CB8EFCA3A75F75FBC971CE083DF3090FADCA25E,
	HideLabelAttribute__ctor_mCEC15AA42376E412DFA3661F34D4B38F5DA377D0,
	IndentAttribute__ctor_m4C4EA7CF24D165E19FDBF449EDE0BDE2EE0875FF,
	InfoBoxAttribute__ctor_mEA63EA4FAF8364F7F5861E800232E06B027055CD,
	InfoBoxAttribute__ctor_mE6C3029629BB3B188D62C9D3A42688B2877281D6,
	MinMaxSliderAttribute__ctor_m73C32546B8D44A35C3BB658A1AB05EB9EF2AB504,
	MinMaxSliderAttribute__ctor_mA60851FC601385F5021FB7A6C9C8201E465031D9,
	MinMaxSliderAttribute__ctor_m97121D0D3A9907DE58A2B1B80BDCF7EAE25FC2BD,
	MinMaxSliderAttribute__ctor_m2B0197624446EE2E82BA984F419B1F0A8A1313FA,
	MinMaxSliderAttribute__ctor_mBB9822C30540B63E70E4BA2DBB29D998BC2BF5AB,
	ToggleLeftAttribute__ctor_mF3B2E5B94E8017E707C11B8A24245E466A41B370,
	LabelTextAttribute__ctor_m120AFF29F6C78A1294923EF793C32D1535C6C202,
	ListDrawerSettingsAttribute_get_ShowPaging_m04B50A37D720C75A3C2E44102538E3C1F6C78B4A,
	ListDrawerSettingsAttribute_set_ShowPaging_mA98F384C28AEB5CF37229A9467E3D714703BB775,
	ListDrawerSettingsAttribute_get_DraggableItems_mB17C6C50B8BD7EA78D57725B196706F3CADF6D45,
	ListDrawerSettingsAttribute_set_DraggableItems_m8B0B96997D2F66F64D75FE1FF189822B36A083EE,
	ListDrawerSettingsAttribute_get_NumberOfItemsPerPage_m0AD386F6554043FBDD495AA7DC702C77BE4EDE14,
	ListDrawerSettingsAttribute_set_NumberOfItemsPerPage_m8993312EAEF84406A3DF92FC426E5103FF28F3B9,
	ListDrawerSettingsAttribute_get_IsReadOnly_mF9F88EA01F82CEE70F43077211CE1F04EB90F7F1,
	ListDrawerSettingsAttribute_set_IsReadOnly_m083597FF5318279936B6F201BE6CF39394F5879C,
	ListDrawerSettingsAttribute_get_ShowItemCount_mD1E9AC4E9DE4F9B3770092433EA5CFDCE13BF991,
	ListDrawerSettingsAttribute_set_ShowItemCount_m665258C5382FD7E1644B0D2C0CDB091E5BC24233,
	ListDrawerSettingsAttribute_get_Expanded_mE02779BAA3ED21A4496BBC18FDD943AD547BA13B,
	ListDrawerSettingsAttribute_set_Expanded_m8F83C911B15D8D27A52E21ECCE51E19B8D9320B4,
	ListDrawerSettingsAttribute_get_ShowIndexLabels_m4AAEB41041F7C12CADDE07667D18FCD5C61EF5EB,
	ListDrawerSettingsAttribute_set_ShowIndexLabels_m133A99569AC66462AFA79DFDD9E252939E623830,
	ListDrawerSettingsAttribute_get_OnTitleBarGUI_m9E6161CFD836FD472193734DDE1597ED6CB171B6,
	ListDrawerSettingsAttribute_set_OnTitleBarGUI_m424220A303257C85E1792B9A1423C42574224EBD,
	ListDrawerSettingsAttribute_get_PagingHasValue_m9550E5B462570C1C522622F2D5C03310A6FE8843,
	ListDrawerSettingsAttribute_get_ShowItemCountHasValue_mB1DFCCC7B480597A8451D731BC3543891DBC39DC,
	ListDrawerSettingsAttribute_get_NumberOfItemsPerPageHasValue_mAD690824656C5003CFC3F2E76C1EAA916BCDEBA6,
	ListDrawerSettingsAttribute_get_DraggableHasValue_m664E10C5C14E9DC9496343C5AA725E0371E029E5,
	ListDrawerSettingsAttribute_get_IsReadOnlyHasValue_m6006332D62E945FE658FB12805B036CCACD3FA58,
	ListDrawerSettingsAttribute_get_ExpandedHasValue_m0B6F2D13EA28AE62EB962846B74AB7859E6C8CFA,
	ListDrawerSettingsAttribute_get_ShowIndexLabelsHasValue_m2257AF08FAA5E5A21D4E27DF92FA787A969002E8,
	ListDrawerSettingsAttribute__ctor_m89C7EEDBDF4AC27489757F0E307486830B47E9ED,
	MaxValueAttribute__ctor_m0F7C5417770DC83172C27B6622E3A2CA1942C3DA,
	MaxValueAttribute__ctor_m2DECAE60D96504B2869870AEC5A9277A3C155D3A,
	MinValueAttribute__ctor_m655347F068916BDDCB0DB745A3EF3F6B7462F9C8,
	MinValueAttribute__ctor_m78683ABAE34F7DDBA6D6CB08132F4AF61AA1669A,
	MultiLinePropertyAttribute__ctor_m53CC75198F796D0C231E74BACD67F2E7A57B3EE0,
	PropertyTooltipAttribute__ctor_m71942469D93E0D7ACEA1B3038C933AAA232C741C,
	ReadOnlyAttribute__ctor_m6C0720DC3F32C28CA3D686B112F3CA7405B08CBA,
	OnInspectorGUIAttribute__ctor_mDDC5EEE9089AF4C1AD2F72C23E3432DE38D7C881,
	OnInspectorGUIAttribute__ctor_mC4B82BFF792E2DC9774313DF104F6229A9BA68AA,
	OnInspectorGUIAttribute__ctor_mAC47FD49A86EF8BEED1271E1372E011341BE3936,
	OnValueChangedAttribute__ctor_mABB73C7E36822F4B7FFFCBB1BC4527D4A738093D,
	PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA,
	PropertyGroupAttribute__ctor_m97E8952CDF6B230904482158001DAB88CA24F819,
	PropertyGroupAttribute_Combine_m428F9CF2608153498BE167A9BDE7F139668B237F,
	PropertyGroupAttribute_CombineValuesWith_mE807B73DCF4C6A9C88AA9BCD40E881733AA03AE0,
	PropertyOrderAttribute__ctor_m75C3FDE0E6665079956F631BFB910048CEBE3F70,
	PropertyOrderAttribute__ctor_m2807F2D02F63E9E1B6AE80787BB8F72719D68812,
	RequiredAttribute__ctor_m3A7A9539E19CA3D8154225B63AE8E0F624CA0517,
	RequiredAttribute__ctor_m6A2BBE37D802335237FE8CF510CD725AAD609D10,
	RequiredAttribute__ctor_m4DF877C8136908A9C54783F10C4D6DAB2D440AF2,
	RequiredAttribute__ctor_mB9C90488A06DD41E9D39D3EBEA88510CBD0E826C,
	SceneObjectsOnlyAttribute__ctor_m6FC75F3244D4F334145DCCDB287B70227988AEA8,
	ValueDropdownAttribute__ctor_mD364F567BD9B8DFA8169C42D6256502B253ED3B8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ValueDropdownItem__ctor_mCCB87C57D5FF6019771592E2E52B910C13590ACC_AdjustorThunk,
	ValueDropdownItem_ToString_m809FC023CF5BE24E964513E064156CC3E405F300_AdjustorThunk,
	ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mDE36C60532902791E5E6C2AF39297AD60F3ED9B1_AdjustorThunk,
	ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m8FB6D5B472D5D524135DE9A0DBB78CACC7D617A2_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233,
	TabGroupAttribute__ctor_mF8DF0A25262FF59F9CAFB79B49032DDABBA68A32,
	TabGroupAttribute__ctor_m962E789C945DC9AC4EA826DCF7979A2A5733090B,
	TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25,
	TabGroupAttribute_set_Tabs_m5BB49FEBE6589933B4F2B96B581E9EE4DC5838B4,
	TabGroupAttribute_CombineValuesWith_mAF271825C72EF38F95673C2A30D695F3C6CB5C96,
	TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_mB8B9EE5668DBC7DDE649C740D59FE0BBA177659B,
	TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_mB501706A09619041158432FBA3B9758F841C01E5,
	TabSubGroupAttribute__ctor_mBD891E55D898A286A39966CAB7BD0D9BF4E18F39,
	TitleAttribute__ctor_m2EEB321011887FAD4345C6DE1C9540114AF6765C,
	ToggleAttribute__ctor_m04A2021D61D0895881BB99A0958EDED31ED2C12A,
	ToggleGroupAttribute__ctor_m02F6C8A534512612492AB9C3C29227776F25CC66,
	ToggleGroupAttribute__ctor_m9B9BE888CACC61FEB298262544065950F500F85E,
	ToggleGroupAttribute__ctor_mCE42334D8F057E33C717935C60446068B17B1AE8,
	ToggleGroupAttribute_get_ToggleMemberName_m04044CD0EAD5C59A69AAC83ED7C8DCF6D4C0A261,
	ToggleGroupAttribute_get_TitleStringMemberName_m7A12EF988A3F9A85FAAAEF843B62E52C0BF10D20,
	ToggleGroupAttribute_set_TitleStringMemberName_m3354817FD279639CAE69FF7C3B431854763B55CF,
	ToggleGroupAttribute_CombineValuesWith_mF7D00256747558CF46D44456114681365FF5DAC4,
	ValidateInputAttribute_get_ContiniousValidationCheck_m57675CD7DD7EFACDD1242343307F36739A70ADD8,
	ValidateInputAttribute_set_ContiniousValidationCheck_m7EF3D2DB874DACA93D72468D6F5B6E371B832468,
	ValidateInputAttribute__ctor_m8515D985050435F347600891E06340B5AE18C879,
	ValidateInputAttribute__ctor_m64BCF6EB7B972FD8563BA5A7E14CFEA80D122038,
	ShowIfAttribute__ctor_m04695ED0E3854D2DCB01DE13F17900053A70F3D6,
	ShowIfAttribute__ctor_m6D02DEC959A55EF7135A0E1AF02681D4AE43419E,
	WrapAttribute__ctor_mCBFEB1652B6B6062E9D5620E2352B6305F591802,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[248] = 
{
	1476,
	1241,
	1440,
	1476,
	2458,
	1476,
	1000,
	1476,
	321,
	1476,
	1241,
	1476,
	1231,
	1231,
	1241,
	786,
	786,
	1231,
	727,
	727,
	786,
	477,
	477,
	786,
	1476,
	1241,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1459,
	1257,
	1476,
	1476,
	1476,
	1440,
	1241,
	791,
	489,
	1241,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1261,
	1476,
	1261,
	805,
	1231,
	133,
	144,
	134,
	156,
	1459,
	1257,
	1459,
	1257,
	1428,
	1231,
	1459,
	1257,
	676,
	783,
	677,
	789,
	1476,
	1261,
	808,
	1241,
	1241,
	1440,
	1241,
	791,
	489,
	1241,
	1476,
	1476,
	791,
	745,
	1459,
	1257,
	1459,
	1428,
	1231,
	1476,
	1476,
	1241,
	1241,
	786,
	1231,
	1241,
	1476,
	488,
	314,
	44,
	1241,
	1476,
	1241,
	789,
	1476,
	309,
	1476,
	801,
	1241,
	789,
	1476,
	1476,
	1476,
	1257,
	1476,
	1476,
	789,
	1440,
	1241,
	1440,
	1241,
	1476,
	1476,
	1241,
	789,
	1476,
	791,
	489,
	1476,
	1476,
	1476,
	1476,
	159,
	333,
	1241,
	727,
	1231,
	1476,
	1476,
	1476,
	786,
	494,
	1459,
	1257,
	1241,
	335,
	1241,
	1476,
	1231,
	479,
	789,
	508,
	498,
	507,
	489,
	791,
	1476,
	1241,
	1459,
	1257,
	1459,
	1257,
	1428,
	1231,
	1459,
	1257,
	1459,
	1257,
	1459,
	1257,
	1459,
	1257,
	1440,
	1241,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1476,
	1223,
	1241,
	1223,
	1241,
	1231,
	1241,
	1476,
	1476,
	791,
	789,
	791,
	786,
	1241,
	1000,
	1241,
	1476,
	1231,
	1476,
	786,
	1241,
	1231,
	1476,
	1241,
	1440,
	1440,
	-1,
	-1,
	-1,
	789,
	1440,
	1440,
	1440,
	-1,
	-1,
	-1,
	-1,
	1476,
	494,
	317,
	1440,
	1241,
	1241,
	1440,
	1000,
	786,
	152,
	1241,
	479,
	789,
	301,
	1440,
	1440,
	1241,
	1241,
	1459,
	1257,
	487,
	310,
	791,
	489,
	676,
	1440,
	1000,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x0200006B, { 0, 6 } },
	{ 0x0200006D, { 6, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[7] = 
{
	{ (Il2CppRGCTXDataType)2, 3070 },
	{ (Il2CppRGCTXDataType)3, 9497 },
	{ (Il2CppRGCTXDataType)3, 6468 },
	{ (Il2CppRGCTXDataType)2, 523 },
	{ (Il2CppRGCTXDataType)3, 6467 },
	{ (Il2CppRGCTXDataType)2, 2624 },
	{ (Il2CppRGCTXDataType)2, 521 },
};
extern const CustomAttributesCacheGenerator g_Sirenix_OdinInspector_Attributes_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Sirenix_OdinInspector_Attributes_CodeGenModule;
const Il2CppCodeGenModule g_Sirenix_OdinInspector_Attributes_CodeGenModule = 
{
	"Sirenix.OdinInspector.Attributes.dll",
	248,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	7,
	s_rgctxValues,
	NULL,
	g_Sirenix_OdinInspector_Attributes_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
