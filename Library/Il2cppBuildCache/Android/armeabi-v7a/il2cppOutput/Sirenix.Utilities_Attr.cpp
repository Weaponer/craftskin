﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct  AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct  AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct  AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct  AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct  AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.CLSCompliantAttribute
struct  CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.CLSCompliantAttribute::m_compliant
	bool ___m_compliant_0;

public:
	inline static int32_t get_offset_of_m_compliant_0() { return static_cast<int32_t>(offsetof(CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249, ___m_compliant_0)); }
	inline bool get_m_compliant_0() const { return ___m_compliant_0; }
	inline bool* get_address_of_m_compliant_0() { return &___m_compliant_0; }
	inline void set_m_compliant_0(bool value)
	{
		___m_compliant_0 = value;
	}
};


// System.Runtime.InteropServices.ComVisibleAttribute
struct  ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::_val
	bool ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A, ____val_0)); }
	inline bool get__val_0() const { return ____val_0; }
	inline bool* get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(bool value)
	{
		____val_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct  DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.InteropServices.GuidAttribute
struct  GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.InteropServices.GuidAttribute::_val
	String_t* ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063, ____val_0)); }
	inline String_t* get__val_0() const { return ____val_0; }
	inline String_t** get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(String_t* value)
	{
		____val_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____val_0), (void*)value);
	}
};


// System.ObsoleteAttribute
struct  ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.ParamArrayAttribute
struct  ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Scripting.PreserveAttribute
struct  PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct  AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RuntimeInitializeLoadType
struct  RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct  AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct  RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172 (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * __this, bool ___visibility0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270 (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * __this, bool ___isCompliant0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * __this, String_t* ___guid0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
static void Sirenix_Utilities_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * tmp = (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A *)cache->attributes[0];
		ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172(tmp, false, NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[1];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[2];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x32\x30\x31\x37"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[3];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x72\x65\x6E\x69\x78\x2E\x55\x74\x69\x6C\x69\x74\x69\x65\x73"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[4];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x72\x65\x6E\x69\x78\x20\x49\x56\x53"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[5];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[6];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[7];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x72\x65\x6E\x69\x78\x2E\x55\x74\x69\x6C\x69\x74\x69\x65\x73"), NULL);
	}
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[8];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[9];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[10];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[11];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[12];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * tmp = (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 *)cache->attributes[13];
		GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA(tmp, il2cpp_codegen_string_new_wrapper("\x32\x32\x37\x33\x31\x62\x30\x31\x2D\x31\x66\x35\x64\x2D\x34\x37\x37\x61\x2D\x38\x31\x31\x63\x2D\x64\x66\x36\x66\x31\x65\x38\x37\x33\x61\x32\x61"), NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[14];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x32\x2E\x31\x2E\x36"), NULL);
	}
}
static void ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator_ColorExtensions_Lerp_m9889C6B728D39820E6B193DBFF1E3F7B094167C4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator_ColorExtensions_MoveTowards_mA4FC1B1A0D4E73C9582A603442CF90F303688987(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator_ColorExtensions_ToCSharpColor_mAB3E86EFCCD979A2A9EDD3A7CC909003D12F16DB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator_ColorExtensions_Pow_mAA9F009DF4579636872558B8C413716B5A699F1C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator_ColorExtensions_NormalizeRGB_mA7D50C50232CDB4E393640BC60C4AE1CE71DC2B1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DelegateExtensions_tC12EB1BA8174ABF92B33663CC84CE81CC15F73EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DelegateExtensions_tC12EB1BA8174ABF92B33663CC84CE81CC15F73EF_CustomAttributesCacheGenerator_DelegateExtensions_Memoize_m4C3B10402AE28FDB2D6E6D6CFC62AB3683EE6734(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DelegateExtensions_tC12EB1BA8174ABF92B33663CC84CE81CC15F73EF_CustomAttributesCacheGenerator_DelegateExtensions_Memoize_m7CCE5A213884A224FECB9C959830CE42C7FA857D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_1_t84FD7F2B762987911F7A7DD357C50853DF46FDBF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_2_tC9D77AC175109F0685ACC75E1A59545F27378587_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldInfoExtensions_tA1D80608B0901949CE0A720C6F2082A26048627A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FieldInfoExtensions_tA1D80608B0901949CE0A720C6F2082A26048627A_CustomAttributesCacheGenerator_FieldInfoExtensions_IsAliasField_m6845C750241C884522A12B3DA22744346CCA3E46(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FieldInfoExtensions_tA1D80608B0901949CE0A720C6F2082A26048627A_CustomAttributesCacheGenerator_FieldInfoExtensions_DeAliasField_m0E168E8E55F57E758C73B014393C579F393AB179(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GarbageFreeIterators_t204682AE6F1C54453D207E271856B5F1FBDFED8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GarbageFreeIterators_t204682AE6F1C54453D207E271856B5F1FBDFED8F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_mA3B6B3149200CACAA93A0D6F7AB93E1094B0067F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GarbageFreeIterators_t204682AE6F1C54453D207E271856B5F1FBDFED8F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_mC3B094CCF3B098BBE20B3F5CB5FD53AE0DB2C7E6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GarbageFreeIterators_t204682AE6F1C54453D207E271856B5F1FBDFED8F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFValueIterator_m786D757BAD9EDB92ABDF5812B5B526168F39D684(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GarbageFreeIterators_t204682AE6F1C54453D207E271856B5F1FBDFED8F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_m306C3F0A9A30D1D539F9B894B559FDB5864D7051(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Examine_m4A8A9E7CCDB237223A822F7864A3F067A6E414FE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_ForEach_mFDCA586AE2178A9FBE288BFA3AAAA9385288480B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_ForEach_m7BC9C671C2CE0488FB4747C88605B2F3AF78A222(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Convert_m008CB5D52FE69558AE092ACD1C940891AC41036D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_ToHashSet_mEC8F33951CBF5419706728B1B3A7B14DBAB7A4C0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_ToHashSet_m1A8AD5BE47781DF55FBD94864812D3DB0156241E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_ToImmutableList_mB8F513CB48B8E4E019681A9263565F420C15AF26(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Prepend_m4F2FF56A82DF3B7C387D22096FAFD4D257F09AF3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Prepend_m9B68AB5EE3D010CD955C3F429F0F34467497966F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Prepend_m48DAFA937E3DFEFEC07046CA28F666A2C1A67F24(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m2233E3BC51FEA933C83489C8F29A7497BA93510D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m730A42177FDA921A15AD43F95E9D523FC32DC39A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_mE5B612C3D9F2B50E7DD0E21AC3CC953C86A1FCB3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m63A5EB8A4722239D095CB27CFF45AB561ADD9071(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m9EC545BB51EB45794B30392019FF875811841F75(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_mF6C8FBA11106B155637BFC5E4B74D4673427E288(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m2C56B55DA06796E75DD829E52FB51C897CAED49A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m5E291D29A182FD8A59BD8ACE20F639187C04BE1D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m396045EC84534F71B900521110504934DFF2836F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendWith_mD9C964D33B14E1A26465D45AEBD0E5E9B77825A7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendWith_m11ABFB9EEF918F0E7959B82E21CD8213E92FF59C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendWith_mD61C9BBB576A102DC0D0B7DAAF8C636572D6C383(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_mE4A774A5F56BD9DD466108580CAE9E83EB41EF49(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_m9A6718618A6569D6DEDAB0DA6812390AB74BE0E2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_m9AF95550DE0A6F426731E801175997FE22075FD4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_mDBE1FB7809D7D2B1617E11D77108F9B768284D84(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_m17840B5E8C3F431692C5DCD78E1D3399E568B831(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_mC9F220AE210BC2B2D8E82BA74666F2FBDB8716BE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_FilterCast_m7E5A9F4B594B01726744E3A4F31477791610D10E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AddRange_m67C2BED6D7F43EEC6413EC43286E804AC343C380(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_IsNullOrEmpty_m57C20C49B35F4875EC61FE3E7561E6223B93A2C8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Populate_mE029AA25F3EFE9E0704464406EDDAF3A27E54635(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AddRange_mC77AF9E9B1EC01E772742CD43D322FA7663C87F4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Sort_m9110BEDDEF0F25E4744C6506FD9B80979553AE7E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Sort_mEB6AC3699E7D177D4B68C65DAF38D6794D2C6AFC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1__ctor_m28C9CEFBE1A0D5DBB913D720E903725D790B0F3A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_IDisposable_Dispose_mAE84E5AF12A4C57BFECB35D1C339A52E4B251291(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m5308FA974EB21604816921C5932E8ACFA3CE8740(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_Collections_IEnumerator_Reset_m4CF073091A7E2E25012BED1E8682C8787D734B90(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_Collections_IEnumerator_get_Current_mB58B1B3CF79790C936C66737B2F5B0215C4BE043(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mEEDAF43AE4F45E8F2B2132A48235AF28562A7FFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_Collections_IEnumerable_GetEnumerator_mF0C38ADCCCE8E1C6E81E03084C1A8AD1FEE5B9BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1__ctor_mBB536416F131D3F745765FD172C0A8CB04FC4547(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_IDisposable_Dispose_m7258E978B8FEC75122CC2421EED2824927711F79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m0138DFAA43EB92B889997D83B6F8CA92BB22DA66(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_Collections_IEnumerator_Reset_m2D69FABE2907C9225CD9AC327F935DBDBFB50A09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_Collections_IEnumerator_get_Current_mE3A24BA3A5CDDEDBD6978B850FDC5AD5493A897C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mC2450E0E66A67CD4A216EABBDE5FAB1FB2FE057B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_Collections_IEnumerable_GetEnumerator_m6B14E440F0F9216D1B7AFAC8E7F75CD54F568708(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1__ctor_m4B32E8E47DFBAE2B02C0D1359F40478BD7331773(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_IDisposable_Dispose_mC91D3DCBF9384B68A6FE7E134945894B4F7647EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m93BCDA002725B8F88251863120A41F554B7DD885(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_Collections_IEnumerator_Reset_m9D17C7772ECBD8D5357C5DAA454F7A2D5C4CCA89(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_Collections_IEnumerator_get_Current_m8A2370FF5EA66712367575CC1E046695FEE63A28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m656200D514881A35DA40B4F0096C366C1110CED2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_Collections_IEnumerable_GetEnumerator_mF5DD1FC956A6AAB9972A7FE42C9F38DFE3C7EC12(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1__ctor_m8E9031BAA4D54EDFC28D8CD016826A91079ECEE4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_IDisposable_Dispose_m80ECB642C64CB0FE4C2A08BD97EF6735A16D804E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mCE6903C5B925D7DFB2A9452355DF81E01E341E33(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_Collections_IEnumerator_Reset_m1BDD6DC6398D1A34F43916C7F858AB0C9F98C6EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_Collections_IEnumerator_get_Current_mE7866B0EC54FA41DB1ABDC84D672571F9F648EE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m672C327B7BB42260ED4C894C989C979E0C9A5E72(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_Collections_IEnumerable_GetEnumerator_m63F22DA9A5B43D28AF222D77DB5247FB44E4163C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1__ctor_mC891923536B77B0A6DB290231A8A61502BECBA0D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_IDisposable_Dispose_m5005082BA371F69B480786817424A9A460958AA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mC6B0B0631DEF274F3326E809FAB01774AAE64D8B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_Collections_IEnumerator_Reset_m59D946D256CFD43C0D57619DBEEB899DF1D0783D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_Collections_IEnumerator_get_Current_m8E49399F51E18A69F917021319EFDF53B2DB6504(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1E6AC42CF9575583B63ADD30B4B16FBC06EF384E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_Collections_IEnumerable_GetEnumerator_m2C9B47F6FC6490FF7AAE3C0B62ACD8C3D47B94A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1__ctor_mEB1CAB552B3EB0AF4AE9FC497F5823E3D7259211(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_IDisposable_Dispose_m28D1ECD420E2166D484675128ECED4016DA42D71(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mC636CD114AC43B0B2C514658D153615323AE3459(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_Collections_IEnumerator_Reset_mD737295CB4874C3666C2A8DF141D991953D6A639(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_Collections_IEnumerator_get_Current_mBEDF0698EA14F468E8FB1DF29163F10FC01E21F5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mE665EA218704AD3AC886CE933317328906A4AAF1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_Collections_IEnumerable_GetEnumerator_mB35FA74CBA01805A1FB3B1E534AFC2E05C1E0FD2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1__ctor_mB34BC2B5B4B408D2BBABFA74A7D6B750CDEC5D94(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_IDisposable_Dispose_m3A8B2D72558FF21654DA4088DF867A556C78558E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1940336AF9AD17D93410EBDEA6AD71F94972C37F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_Collections_IEnumerator_Reset_mF53214FD27DDA876479589D96A2651B7003D617B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_Collections_IEnumerator_get_Current_m89DC52E1584BB1DC475EAE3ABCE6335016D6C9B0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m010F50E5EDD1513CC283125CAB9D892A7B0B6E7C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_Collections_IEnumerable_GetEnumerator_m8EE72E94E5F7A491EDFBF2AA1767BEF0EFD4A568(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1__ctor_mA2BC3E5F24D8BE6E301541B203DD1E680395F748(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_IDisposable_Dispose_mE505461409F9BEF046B4E3533F389156D0EF7ECC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mDD2BF2B1B3F3943ACBE31E9F66C715583B875459(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_Collections_IEnumerator_Reset_mCCF9C854ED135F00206BCD3842A54926C32C1D78(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_Collections_IEnumerator_get_Current_mCD6572D3BF4852CC1B460A4277060CDAFA431CBC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mD4DDEE529A6ED7CC52216362865D2E55D2D338B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_Collections_IEnumerable_GetEnumerator_m0DB5B02D2FFA640EE6349FE5264C5A5484CB7881(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1__ctor_m4E25BAC7D98CEEE59173734BACBBD5F39DC89E4E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_IDisposable_Dispose_m325ACD31949E07142C8C761B213CDDD9A93C7BE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mFF48BC102A62E106A8FB2D1C9C957F9455EF745C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_Collections_IEnumerator_Reset_m9505F99566D8B075235F58F683F7F0AF1B2C823A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_Collections_IEnumerator_get_Current_m92337527F3B18EAA002171D991336953D48BFA2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2DE6CD183149D9BF62B4A14CE1382181834C18D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_Collections_IEnumerable_GetEnumerator_m19FAD7060D1B4B08E6D1BE63D297D11C45C2F453(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1__ctor_m7E5B1679FB0450BA8AC4C93B8A792854AECBB8C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_IDisposable_Dispose_m9A8CF92E97796EB5774E305BC682579F202FD7AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m8F8C076FAF765264505F7E2326DC1DF9A00B7FA2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_Collections_IEnumerator_Reset_m2FE8290D930F3D2A6B1E7E23453810F0D7A3A8CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_Collections_IEnumerator_get_Current_m30485953A9095CA963AC42B5BA4EDB64B5E9FFAD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m211D6A5EDE7E5D67C44FAA115EF78333A68D52A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_Collections_IEnumerable_GetEnumerator_mC355FC9586EBBCF13C46636E81BA374B0AF495AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1__ctor_m81A3B193993A1BA4F834DB4F91BDCDB670CC8178(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_IDisposable_Dispose_m52481EAEA9D4D25647AD2945A6A96DE112120A4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2A396FB9281D722A7179A98076DE6B05DC7EDB49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_Collections_IEnumerator_Reset_mF89773F61F87393DDD750787B9E48D1D8029A912(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_Collections_IEnumerator_get_Current_m33BD879DBCEB10BE318449B25EAF97A6A03C7998(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5BBA0F55B07A2A8C6CF05767BBBB13FFEE9CB211(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_Collections_IEnumerable_GetEnumerator_mE69A6806918FDB41A9B46F36A577EDA6C2D0A2AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1__ctor_m37883249807FC5A5D81FF8AE30E082CBE6086CF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_IDisposable_Dispose_m553279E85BDFD2A5402F2203E9ED0EDDBBA830FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mEBE118017130DFEB39F74B7D5B87E71D30670B67(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_Collections_IEnumerator_Reset_mA7F6E158F8D663EE6A62FE599771748DBAA8135B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_Collections_IEnumerator_get_Current_mF5DD5F1C259368589CD640C1F966FD457CD33481(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mB921B3820FDD393AADEDC43AEEA9013B52EDB63E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_Collections_IEnumerable_GetEnumerator_m39210121B5FEE445D4545D234778A0BE71240F9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1__ctor_mC3BD0D897959C60FB90E139FFF6619D52D05997E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_IDisposable_Dispose_mDF272AFDC581CCCF78FC8E21300C99FB8769ACDB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mE32EFFD67FAD27655EC15D6474B507843F75CD0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_Collections_IEnumerator_Reset_m2246C445D17EA445D74219A31EDBF819B96D5A1B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_Collections_IEnumerator_get_Current_m502FDA0AD271D773B6FD68F8852AA4171E8E5D1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3ECB65F0161DED569F53C158A8B21031ABE8D1C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_Collections_IEnumerable_GetEnumerator_m9C54FCE096DEEABB871CC617514AE3D5A55D02E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1__ctor_m6D4F3DC494073D778340AEB1E72A035C507F54F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_IDisposable_Dispose_m478564692E000CC3B76A1F391865B8C19E78A378(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mB05E3EF866D5AE540BD325CE88ECAC721D430B49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_Collections_IEnumerator_Reset_m303C81C9104E8328AE3B33EC3121EB869E45F083(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_Collections_IEnumerator_get_Current_m9DA1E43666530CA2EDC8C3106EBCCEA6A8A6F4A7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mEA8E8804920289637B216903FFEC80F43F9AE43A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_Collections_IEnumerable_GetEnumerator_m7C5021FC365833F6050D97810286DDF9E8D75E13(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1__ctor_m4C02B46D6E60E23FCAB794DD24A04BB2542A47F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_IDisposable_Dispose_m28047C36017B782AC07FB34213B0C76D89F49DC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m397E2A7040D417BFF1F5403670107BC4165DE9EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_Collections_IEnumerator_Reset_mE3542B9A108BB3D93053BE06650CE1076E4D2F6C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_Collections_IEnumerator_get_Current_m0C0835CFBDC67A750462A8D637D2DD94AF71F74B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mC64796CD6AB6894BA6F94DF140B105F0539D841E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_Collections_IEnumerable_GetEnumerator_m96C7FA5C72763C81251B67E3A186EA16C16131CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1__ctor_m3FF9E472DFFA87F67C6B4A2B34F92AF053075144(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_IDisposable_Dispose_m169E082A5B36189D4C18E55A4FC59B7F574463DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m04F694FAD7454B06932D31B02BF11EDBCD610BD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_Collections_IEnumerator_Reset_mCC00B2E81237862F829E35BD70215B03C8A5AE88(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_Collections_IEnumerator_get_Current_mFF7FCDFB338D3FFD81DC3C97DC6070774E305CCD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mC891764E86772D06B76FB43900EE2C52FF3AD231(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_Collections_IEnumerable_GetEnumerator_mBB0B0416D88B8386B0D4E8C4A49801D92F787088(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1__ctor_mC704E6A2E38F643E01C18D0E2A33910F451A54BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_IDisposable_Dispose_mFF3B8BB49DED97D5D2D41DAA9855C9481D61BC31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m8EE9474F8352298A69CCCA0B443D2E7C90F47AE5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_Collections_IEnumerator_Reset_mBD03819B8B1FDEEE65A62A20FA099ED247AD7625(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_Collections_IEnumerator_get_Current_m52E1E2983E16DF723C8BE2EE6C30B9BC0E9D4FDD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m8838EF68F16A2BB06D39AF2080F1D61A0EDAC9EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_Collections_IEnumerable_GetEnumerator_m1925AB55821D802D8CBE61C0A54FC420F3BA7EA5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1__ctor_m79090569A237807B75DD8E807BE7AD1B52812AF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_IDisposable_Dispose_mDEDC2CB802CF176A2C8D3DC902C50267AA91E70B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m42E2442609FC61478533A9BCB4A9BE7500E29913(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_Collections_IEnumerator_Reset_mB5172CEA7E75183CD507D7183CD2360143A61785(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_Collections_IEnumerator_get_Current_m70969F9028EC816086C92606A30F4A4F79B314B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mA8467099FDA4BE8FEEE45BB9D4E8F582EC8362A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_Collections_IEnumerable_GetEnumerator_mFE2AE0B91BD917991D7961D084A546C0BE232C1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1__ctor_mBBCCB5B2AE7A9F61A9CAD2AE1380A54BDB467C97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_IDisposable_Dispose_m5E95447499C25CA1D13896AF7AE26147487767DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3F598E452BDD0113941CB53A5706B1E073405BF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_Collections_IEnumerator_Reset_m85FF226CD7567DA86E505D978E1D74F0B89F5C75(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_Collections_IEnumerator_get_Current_m2E19FB33AE036DF5C28035CF046B84CF4ABF0E2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25912AEEA751480F735E98229737FC72136E852A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_Collections_IEnumerable_GetEnumerator_m135A55EAD0EEDC57B5CAFE04B0F42D48C64B94EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1__ctor_m770A93895D0A6E35F1D9021C13DDDFCE5E265403(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_IDisposable_Dispose_mB6B9CC65335719DEC231E3B964804375E7AF61D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mC187A8EDCC5319574A8767B522920AB4D9A91AFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_Collections_IEnumerator_Reset_m0EE3B1241D648AA01FEF9E82027BA003C741914F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_Collections_IEnumerator_get_Current_mC2397FB190EF906289423289028E877EFDEED3FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mD46734FD61413A944AD1CBB596D34B0541B46D94(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_Collections_IEnumerable_GetEnumerator_m0B68AD1245FB26B983A19CD925CAF82CDB5E6297(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1__ctor_m2895D05215E1D3E80789175BEA1D90B14FDB0342(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_IDisposable_Dispose_mF3D7B66A181CFA186796DABFDF455C65562A9E24(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m84CE24B58A9740BFCC68CED618E908FCF9E6E188(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_Collections_IEnumerator_Reset_mC52F6CBDDA80560018B03CDEC7610126623EE64F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_Collections_IEnumerator_get_Current_m1F3E2B37C87E359D96C7522E99C473E97D91602E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mBDAAF964103DF49A1166DB5A12AA1B7799F369BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_Collections_IEnumerable_GetEnumerator_m84A1D6DF612A4296555AAFD6B5C65BBECD69AC0D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1__ctor_m10376BA34A68F5A9C4485EF461B5915B97C5E3FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_IDisposable_Dispose_mDC9811AB95140E585F74A0A1282354E69863B0B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m42301BADA9E41F135B7685E875EFC1626E368AF6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_Collections_IEnumerator_Reset_m37BF1318910E389409DFDEF9AF28F719214C8B54(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_Collections_IEnumerator_get_Current_mBC3481184F84834D4F44371E49A7FDFBB0EFD15D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mCFE18B909C863F207934851059BC2242549E56C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_Collections_IEnumerable_GetEnumerator_mE3C1F68D136F0CCF7645CA9D264AB172309050B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1__ctor_m9EEA630CF0EF3D3C670A50C5B38E256D81EF6A4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_IDisposable_Dispose_m27488063FCD722F90D245034A8488BD3F088068A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2A093D599C7AF540B0AB347E59FA6F9628F5A3B6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_Collections_IEnumerator_Reset_mE70A9DEA2643019A9126D70BF76AF2008A09FC68(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_Collections_IEnumerator_get_Current_m6087D07BC2AF20A166A32AB3E3EF5A2E7D63E4E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mD46BE45CDE2AE546102283A9711CA2A41E92444A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_Collections_IEnumerable_GetEnumerator_m35350602E2AF8F2468D6BF4FF6D7EB57535D7E91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1__ctor_m790B8F4A5AD61EA55E9FE5BDC1490EA95F344DC5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_IDisposable_Dispose_m4B9BA567C19438B2275F25DE65FF226A774B41BE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mA2FEEDD6C794181BC31776A52966FC03BB1F7EED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_Collections_IEnumerator_Reset_m82432AE9E4180AA6737F217F0FCBC2E51C5F4387(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_Collections_IEnumerator_get_Current_mA062D57A1B74E398EDD66C9435CE9160FD944F34(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mB34F50C6FDA2613133CCC0B263448D2E211A4AE2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_Collections_IEnumerable_GetEnumerator_mFF0321109979E20D657B613408D6632426217310(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_IsDefined_m3B3989946BCCCD73866295030F1FDF0245912703(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_IsDefined_mC821E92ACDD5A9732B7C0722555E574EA34354F6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttribute_mB1DDC06945BB44059A1966EAFA4E166D22449598(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttribute_m7D24BDEE3F2CCB6625F33375B999EF1F890CA4AE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m857B498AC5F5BE45EFE70386FB0F3E176F709A78(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m9A5476B6768136AF61DA383FA040EC27C26C9A71(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m15AAE26BE285B269CAE126FB926279E0A826E987(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m2FFD9512045ED25E441863CA105281FB2C5E0B9F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetNiceName_m233C26DA9440A5BD00027679176E7A1207811170(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_IsStatic_m0B82DFA6FA5B65EF48F2D3B2799A342F48353F7E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_IsAlias_mA690365F38B68B1AB5041F68D0A6995FD6DADD57(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_DeAlias_m6C0863F27B3C8362850BF8B960A7AE376D425D68(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_SignaturesAreEqual_mFFAA6FB976110B9E1B5CFB81EC23FFA528363A26(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_GetFullName_m2B1D1CF37601A3B322FEA19473EEAB83BE9322C8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_GetParamsNames_mE7B9162DDC988D41D65DD34E54A7C29839C38F58(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_GetFullName_m5324B40A2D0B23F9F4989566B50AB7EE99F88A47(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_IsExtensionMethod_m7796506EB32D9061808BF565AEDC681C66EDDBED(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_IsAliasMethod_mB15831FE197849EA54CA5E0826EE7EE29275A01E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_DeAliasMethod_mF8824D0D8741B82891672059C62A31876EF1C900(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PathUtilities_tE2C12117076D2C8243234A91CE5D992E38F6D335_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PathUtilities_tE2C12117076D2C8243234A91CE5D992E38F6D335_CustomAttributesCacheGenerator_PathUtilities_HasSubDirectory_mD034F326CD0D7AF99E6F0EFE93550D684CB0A652(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PathUtilities_tE2C12117076D2C8243234A91CE5D992E38F6D335_CustomAttributesCacheGenerator_PathUtilities_FindParentDirectoryWithName_mE67A19F3CB8AA227659DDD7C5F7958C36E1378A3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PropertyInfoExtensions_tD214C155D903AA7C382DD73C95C8558BB63AA3FB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PropertyInfoExtensions_tD214C155D903AA7C382DD73C95C8558BB63AA3FB_CustomAttributesCacheGenerator_PropertyInfoExtensions_IsAutoProperty_m17D6F1D04D64078B40A4DFC8DB2678E0C88B4863(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PropertyInfoExtensions_tD214C155D903AA7C382DD73C95C8558BB63AA3FB_CustomAttributesCacheGenerator_PropertyInfoExtensions_IsAliasProperty_m26C4A48FCE446591C97B7BF2BE9F4716559AB645(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PropertyInfoExtensions_tD214C155D903AA7C382DD73C95C8558BB63AA3FB_CustomAttributesCacheGenerator_PropertyInfoExtensions_DeAliasProperty_m5420C600E36D84EB08C57C666C3080A09C191CED(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetWidth_mC853B7063708E8C35014ACD1210681C80A1BCB47(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetHeight_mF6D4E394172A8BF3335A949463B4D7031E0BD5BB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetSize_mD39716901BD09A67A29FF813F535B0415F09DD3D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetSize_mA7B1826F31962FB667C60F5291929BD2D779411F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_HorizontalPadding_m8E8E3A615990471E3586A9BB20DDB018DB909EB0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_HorizontalPadding_m89B3727FB917838AF9D05AFF14D389517798BDA0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_VerticalPadding_m5C4CCF230E193C556128BF8481F1FBBC167D67AF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_VerticalPadding_m590B82183DB97091EDA9E0C994F54C7D64613C05(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Padding_mFB407178B4A570B3E8AD035E7C25C1E1F5431F55(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Padding_mDA5649C8E3559871C74BA5D72FA1B690F7107486(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Padding_m32E39A56FBB4E5707FB0B008A1E0E59CAB187C79(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignLeft_m614D187B42334A09858A7F7D393538FE7FEB0889(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenter_m3E956AC4B8A25F9474FCA34A34E53F84D45CEEB2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenter_m9B01538E9E37E34EE70A05A6F776BFE9B8DE6184(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignRight_m574DBF16789091EAA5DBA75D1D3763EF34C046BA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignRight_m05BE19656017535D2F9E004CB946C36C94FB98F9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignTop_m6D8F9DE46371856A5B061230B35C6571CE5A617C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignMiddle_mC5E340F841C52962FCBA450A79F86B76C5AE2886(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignBottom_mBEACA9FC17D0F71F94305F734A21582880214A9B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenterX_m69C778A0DABE4398737D79D19860AA1FCF428037(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenterY_m7641F9F0AA5704BD40BDDA8B2096C4252686FB08(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenterXY_mF1D830A1A24F00608669BA7ACE783E0C407FE6A4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenterXY_m3F714CE8FECD8AAD7E1D6CDF276850C7E5FF95C8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Expand_mABE02EAE258191B576ECFADFABDE0DFFF8B9177C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Expand_m81781A1F595C5F5CA55814EE85555D0B2A912233(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Expand_m72D5AC60DDE7B7C771FDFD9663F927DC7D91C08B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Split_m97F4A1E73113DB65F4C04D542747DF3E92DB10A8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SplitVertical_m939E05A9013EE2CD669EC050750E468957F50BB0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SplitGrid_m6916D7314A9130D72E3DFBE3D9DC81A69218E90C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SplitTableGrid_mEE23BBAEBD316E42C0731267C95488D9381BDD5A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetCenterX_m8654D5E6F3D90DFAB618CDE1ABFC4BFB34349A5D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetCenterY_mD217F8DB58C73B90DF3D0519F3E21387898B0E33(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetCenter_m5D32A84E8B2B0A5AB1190ECD3D2362104E12836E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetCenter_m9B56A386F413A3ABCD25D840E2E1990D1CEAB2AD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetPosition_m35513C3D84420597C326DB9F71B1E53ADE6B86F7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_ResetPosition_mB818EF964F8D1E78385E0D479D7286DD1A53C140(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddPosition_m1D21D830B6D383D35D05666220FEF082A13A9AFD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddPosition_m029ADEE6AAC59E615BB356040108B633E2E4F60F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetX_m00477B241A08B949553D77C69BBCA5A322F22A35(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddX_mAC5D747A732603B30BE1BA06C3BB209FB542B266(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubX_m0AC8AB5C0B438C5D03CD0E51895858EC0B890718(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetY_m08E7C979CC840E57214E979DF30C08F5ADD24B02(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddY_mC0A509D0023FC36DF2AAA3702E463850B73C0415(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubY_m0776E2852F6DACCBA11D7472D7D1AA76F51C6057(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetMin_m68BC6C0D21F9F0558B957515260B08CFE44832B5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddMin_m2860A1F53A2FF827A6EAAC51570CB198DA547564(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubMin_mE28419D690ADD2516463FA44BCB7C86EAA04346B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetMax_m855BDDE7EAEC02DB394244E15E5F54A045FAE316(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddMax_mD7B96050B949AFA58E950D9EEF27E722B013ADA4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubMax_m8B41E31D6702D6E3F91DED8E2AD8D707EA1B882C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetXMin_mEB4BB8CCD0C42D6078F892475907D18190174627(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddXMin_mBF12C4963D04484010D0B3039FB59E19ABD3F39F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubXMin_m834AF0100D5BF61C3D7FC980C4B85A63D5C04CFE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetXMax_m3079E8E87F725074D0C38FB308C7205E0AB03985(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddXMax_m00BCE6D7A36B8B9D5CFEC8AAFCE6032917A6DB31(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubXMax_m60AF370F697312E645E8F30F0CF99B8BB93E851C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetYMin_m11A869747613438D095F20A504AA0989F6C0718A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddYMin_m4124C8B31491D21DF6B930295B712C60AE2E32A6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubYMin_mBFFAC829D2AD00C17749B310642E403BFFD63CC6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetYMax_mC70AF8DCBFB45C4FC72CEE1E8085DF5F5DB7B61B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddYMax_mA63EBA978081E7C2CED937F2B563AD1F28CBDA2A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubYMax_m51DB920309335CD0C3A7D629016E7C48418DAFDF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_MinWidth_m77CAE2D37F322D97EE565DBD6F33224ABCAD0DBE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_MaxWidth_m6D39F9B0FC8CE7ACFDE83CD74805C15ABFEB1AB6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_MinHeight_m8B4B10F23FBA6A890FE5B8F5B6154FE7BF3AF330(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_MaxHeight_mE30E285546FD1051FCD4B826B0A539447E022252(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_ExpandTo_mB98847B1A65AF6FEB80FE423BE4F29707004647D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t32B32D9752767D336798A0E94A1EC9D3F58E6704_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t32B32D9752767D336798A0E94A1EC9D3F58E6704_CustomAttributesCacheGenerator_StringExtensions_ToTitleCase_mA8A1408F983DDE057FF2DC283C24A931599C25C0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t32B32D9752767D336798A0E94A1EC9D3F58E6704_CustomAttributesCacheGenerator_StringExtensions_Contains_m8AB548F34460F21E93D1F70FF99B15246F4A8D2E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t32B32D9752767D336798A0E94A1EC9D3F58E6704_CustomAttributesCacheGenerator_StringExtensions_SplitPascalCase_m5539F652FCECED016F2AC168F3FB04FD54205163(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t32B32D9752767D336798A0E94A1EC9D3F58E6704_CustomAttributesCacheGenerator_StringExtensions_IsNullOrWhitespace_mEE67BC64AC520011BDC8485A9CF67819CEDC6F2D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_HasCastDefined_m2119B5754B8D819AA028BCD56C3AAF744B4F4960(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_IsCastableTo_mD75B14DD15DD5B1CC04062319CFBC91E22CDC85A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCastMethodDelegate_m2861121E8A7BB7BB3761C01E8FAD5F1FF94D7C51(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCastMethod_mF07C9F8F7ED2C41152EB2C564246358694A4F587(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetAttribute_mC67735A2238DCFE832D8F16C3941BF0143FC995D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOrInherits_mE14EC3D52E1F1029666B799F1333A70BB4EDA868(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericType_m1121DC0D840999A914E5574F3F0AD4B26CF8796A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericInterface_m2F341DAEABDED76559A85A8D20B8550D98305795(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericClass_m0A41ECA8B4244F7ED2BE35D1F971CE6B286F4112(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m0753CF4AB6F4C0C028B12A66C68035E133CA6825(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m916A7D9B2DD86FAB8764EC572D18A3AE44F1E0B7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m465C12014C9C4C4ED14FFA0CA5351375A09D2C32(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetOperatorMethod_m7E05D3B4C3D95C62BA8886DCDC060FEC3EF6C152(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetOperatorMethods_mB76C9B9565203292047CF82412370E8FD33F959C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_mD0E5106F949AD8BC0BC4095BCF4C67050CC859A8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_m3562615FFFE9EE27CC2D5D0193EAD3C6FFC8F9E3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_m92335B5FDC7C089A57FA4401D0C0E6BCD44BD580(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetGenericBaseType_m94A62E4BA96C8F4FC5A4F465D14B416468B51F91(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetGenericBaseType_m39F7468A0FF37BB99A5A13025FC2523F0DD1CCB3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetBaseTypes_mC637233D7ECF7D9C89B70AA1D0C218844C07A964(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetBaseClasses_m62C3445D92A27770261AF5FA50BFCABAAC2D014D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_TypeNameGauntlet_mED6BB694ABAD8B721B26010774C2AF0F36CEE16C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetNiceName_mDDEEDB5E0EE8C831C50FF8D56DB2383EE063DCDD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetNiceFullName_mA2DD7054D6BB866DAE63AFAE786D93BA6DF76DEC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCompilableNiceName_m339A581D1A09CE239F39F0B892C3AA46DA92574C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCompilableNiceFullName_m14D75762F69A7E7753EFDAA2DE4A71F7CF2CA868(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttribute_m878AB4F5EFEE7E7C1C0975ADA8867FE5D0DCF237(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttribute_m0743729CC72D695D45AE2D351685FC89EADA5130(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttributes_mA27D2AB317348FBA619A5D630EA60ABCED74A664(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttributes_m411C857A8BB49F74A0BF6F6E1FCB67C6EC86FB0D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_IsDefined_m68B1F8D45C6BFF6CBDA6B56DE0954E01F0C0133A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_IsDefined_m1311C286FCA3E48BCFA44154EAC42466741BFB9A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_InheritsFrom_m4811C1F4003F6D1597DF695C5FE93BB8B32F9D49(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_InheritsFrom_m9CA98952CA2EF39E0DF4BD9E68773E46B6501471(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetInheritanceDistance_m2BC5795354997E6D0A208131F755C2629F26431B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_HasParamaters_m71D6B60C5F128516E7DD8CA2F9FA4C81988E4659(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetReturnType_mEFB54C016DF12930DE9D99E351D0A67F4C0A872E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetMemberValue_m92007B8251F5EFAD94286371D20C634600BD7253(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_SetMemberValue_m19EA7B3165678F36D133C6877A405938BCE728ED(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_TryInferGenericParameters_m8B86E7145860BD05F5BA87566D034CAEF046E46F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_TryInferGenericParameters_m8B86E7145860BD05F5BA87566D034CAEF046E46F____knownParameters2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m5979A23DB5D7BB878F43406689A1D1E222A84182(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m5979A23DB5D7BB878F43406689A1D1E222A84182____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m68E1A8190F47993BDD581D414D13C509579F4F43(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m68E1A8190F47993BDD581D414D13C509579F4F43____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GenericParameterIsFulfilledBy_m6F871B1B8B2427ED49D7FD1A4E0BBC13E0125750(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GenericParameterIsFulfilledBy_mBB6572A82E174FF80D5A9AE454EE8CA6FFCF777F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetGenericConstraintsString_m74F940F9A005E758932D975A47318666A68B2215(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetGenericParameterConstraintsString_m6C4C94289FAA168F8C83862ED43A80276BD4E1B3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GenericArgumentsContainsTypes_mB5405BEBB46D9D14834B1FEFD389AAD72052552E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GenericArgumentsContainsTypes_mB5405BEBB46D9D14834B1FEFD389AAD72052552E____types1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_IsFullyConstructedGenericType_m941C7E4175FEB639347AD717A85AD04D6A53CE38(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_IsNullableType_m28FB5D9FB77956D303CF4009FE8E4C62D896822B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_SafeGetTypes_mB12AC54B4D81C926F95EABC7EABB6622A57C98F8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_SafeIsDefined_m278C59D57D1127B3783333879C5298B6DFDC2F0A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_SafeGetCustomAttributes_m23EC650659FE27E327171260426F92C9CD5EBF8C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_t9303AE7132ED494071086287CAEB1462CFB3B6E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__25_1_t4F2E737EB50FE5C38F489C2E5221868768FC324F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_tEAD70FF851758143764FEAF289F1995E271464CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass34_0_t5A596E24D8ED1B469CBDB10CEBDA0AC63AB318DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass35_0_t639A7EBA788055862A10E4D3E447197715AC43A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36__ctor_m93329B6BB738335150EA8633AC9A62064DE2AE5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_IDisposable_Dispose_m696D89A5F3311080407B13BFD1C6131D488FA40F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mC6C31374620C0677C696479727C2B46974130EE4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_Collections_IEnumerator_Reset_m7A9247214D92CC94632DF7CBC99660DDCDB73CC3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_Collections_IEnumerator_get_Current_m33DEDC31C961C26AA43C6CD836286AE5C2EB1809(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mC498D8F9D92C86C5CB7CB8EFA91B0A1B4FBF4B2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_Collections_IEnumerable_GetEnumerator_mEF0676FE290DE50AA21460A696AC5585D6959DB3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37__ctor_m5C0B1BB7706836620920BF0378663D60442ACED9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_IDisposable_Dispose_m122701CE24AE448A557DA668F815115D765395F5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m033546A216735300A541C93881903001625A7A27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_Collections_IEnumerator_Reset_mCECE57E55D6393810F72C45E36309904E806F9ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_Collections_IEnumerator_get_Current_mBDFA5AAE955CF87CFDC232725522A37BFC16CDCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mF7674D670646237FF8B332360649656FE5395034(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_Collections_IEnumerable_GetEnumerator_mFE43504BBBC411400F6B50970E44F082B2FF5B17(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1__ctor_mBD6E46486AECB01846A1CDD7F9A655B3D3FAE51A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_IDisposable_Dispose_m584F3C90E648E7F580E5DA81056CDE1527CAB1C3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m84F7CE806A076197F0BF43FEE44EC3E807B547BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_Collections_IEnumerator_Reset_mA37AF27BD2DD1C3C88838DFB46DC5E68464D4DD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_Collections_IEnumerator_get_Current_mE1C13076C88FD97184CDF71355F5A14AC62C5D5D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1229BF38DE86CAB2A87E72B91417FF89FAE6931E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_Collections_IEnumerable_GetEnumerator_m42815FFD80C190C0BAEA2D82F9360F726ED41C2D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42__ctor_mD2A7BF94397412C36C8F81C32F7608F4DD5481AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_IDisposable_Dispose_m4BA19A27C774612AA6AAD5AE4A740F3665F113B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m739E1E100840478C780CD1C037EBE5DC5ED381A4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerator_Reset_m322C061B956B87A6E42700C1BDDC602F29CC2469(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerator_get_Current_mEFAE5547493200A0CF71A7EDC76005CC5FF74D16(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_mE8BF5192BF980759DFDDBE1DA4AD0CF2C5C03DA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerable_GetEnumerator_m6578B926D0606CA6B4A8A0FED583288405B3EBAF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UnityExtensions_tD3EFA04258F9C647E2C1D20C616D5C3991C000A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UnityExtensions_tD3EFA04258F9C647E2C1D20C616D5C3991C000A2_CustomAttributesCacheGenerator_UnityExtensions_SafeIsUnityNull_m9609D5559427508D80B7A9FFAE9DE8DA1C36A3E6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_1_t6BEDE50B0B8D613C0C70C5782161CC5DEB55B504_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_1_t9D35061C753A9238A5E0CF0FD2499E1C22024FDF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_2_tF6D37C166E1B6C94E34DBBDA4FA34C6F5B91FC14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_2_tF5FF3077923F71466B7D55A2A9C2C43426AAFA9A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_2_t62FFFC9705F04BE6A75D16A443AFC3B6F6588B8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_1_t83A49D7F615431D5E39BFCB398B96AB64BDA4199_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_2_tDC2C26BCFC97CDC5F3BA5E0C19CAECE49EC993FA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass18_0_2_t6BC4702979992F0DE82801D1D68C9657C41C9F59_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass19_0_1_tE581833927451E946F41064DD8BFAE421C385FC6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t75729FD56ABA0B98917619AC7F77A97BFCD0EE46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_tF812986FE8492E826F6999E79A41A8EA8C950895_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_tE962850B6BA008EA33B6F2D15D5957F1DE470B9B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DoubleLookupDictionary_3_t913197955594243A3726430930E1906D28CDE556_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_1_t1A573CADC1E792CDFF89D3BEAA7F39CDA46CA3D6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_1_1_tE1579A8FA288839CC0CDEEE1B72DC6B97A7E94EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tA4A7A3E24273266A0E627EC9A0C076495B966581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_1_tA03AEBEF02A739AD8061E922D82CCB1F2B756C2B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tC4782CB7998308866A013948B73003D8A303C434_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_2_t7726FD61CF0E97F877A62EDE557A992F20E8C40A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_1_tE14FF4D7B3C2BF3648BA4A93732124857EDB8A44_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_tE78C237336171DFAEDEAC1FB8CCA738B48847840_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_2_tBCB0DE1E2F262B4C6F454084E0AF9CA94B177F8D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_1_t2541637D794E09E89B2082464E3A04F88086A71A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_t562D56E42ED912CF095D7B5D55FD766C019AD8BD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t18E2F87BD90000BED545B3BCD55F85DB6DFAE7C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_t141B667427E87F84B8BAC5E9363EE5C2EC8E7917_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_1_tAF59221F597F41D3845BEECB88AF93841256724A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_1_tE4B4EB18EDABD9F69DBCE32FE8CAEB6442FDA9F1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_2_t25591E98AED7932DD79C0326B71CE2A3B4085F11_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_2_tAE63B212838B9A2C28C039204512B71CA22324E7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_1_tF8FFDF12D94C2D8B4CF9D5A21CEA8FFD212D8AE4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t8F35DECB5FA5111963937705845DF71817621700_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_2_tE5A61DE88712303199D6F987671F4E8F04345116_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_1_t821DE62CBF1D7984C35E09D7DACA6D157CC082D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_2_tEBED323A92B0881EB9F8769746272E76D7F6DAF5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
}
static void GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02_CustomAttributesCacheGenerator_U3CUseAssetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02_CustomAttributesCacheGenerator_GlobalConfigAttribute_get_UseAsset_m6DE933FA318D235196AAD7C3A182DB581BFB526C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02_CustomAttributesCacheGenerator_GlobalConfigAttribute_set_UseAsset_m98E3100E2A2BFBE9C311C1DDA29DEBEE438F73C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02_CustomAttributesCacheGenerator_GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02____FullPath_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x27\x73\x20\x61\x20\x62\x69\x74\x20\x6D\x6F\x72\x65\x20\x63\x6F\x6D\x70\x6C\x69\x63\x61\x74\x65\x64\x20\x74\x68\x61\x6E\x20\x74\x68\x61\x74\x20\x61\x73\x20\x69\x74\x27\x73\x20\x6E\x6F\x74\x20\x61\x6C\x77\x61\x79\x73\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x20\x74\x6F\x20\x6B\x6E\x6F\x77\x20\x74\x68\x65\x20\x66\x75\x6C\x6C\x20\x70\x61\x74\x68\x2C\x20\x73\x6F\x20\x74\x72\x79\x20\x61\x6E\x64\x20\x6D\x61\x6B\x65\x20\x64\x75\x65\x20\x77\x69\x74\x68\x6F\x75\x74\x20\x69\x74\x20\x69\x66\x20\x79\x6F\x75\x20\x63\x61\x6E\x2C\x20\x6F\x6E\x6C\x79\x20\x75\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x41\x73\x73\x65\x74\x44\x61\x74\x61\x62\x61\x73\x65\x2E"), NULL);
	}
}
static void IImmutableList_1_tF0ACA080D52BD259073B9A5BD0C24B51042AD498_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ImmutableList_t49F27F3327CCD3A8BA9D533BB610022C4A9312DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ImmutableList_t49F27F3327CCD3A8BA9D533BB610022C4A9312DA_CustomAttributesCacheGenerator_innerList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_mF71247C0669D13A3CCBEB9DDC43CB91FB13D7D2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m55A31EA0E7CD9D1B866BF0EC395E0D0523335C36(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5986C856A9B9449B39B542589647440252B70DAD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m67F6137A2C821A8BF580B221DFAEF24E71C3B334(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m81047B92D0044C262E5451C3215F00295380FA9F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ImmutableList_1_t50D68A24605228295D545DA4AA84B5D9A25F9729_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ImmutableList_1_t50D68A24605228295D545DA4AA84B5D9A25F9729_CustomAttributesCacheGenerator_innerList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ImmutableList_2_tD588EADF657CF70731C80E78760AF63D00AFE5A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ListExtensions_t4CDFCD31F6080E2522A133523E87A05FC418AE6D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t4CDFCD31F6080E2522A133523E87A05FC418AE6D_CustomAttributesCacheGenerator_ListExtensions_SetLength_m9C776B6460315564F68246097670424620DCED85(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t4CDFCD31F6080E2522A133523E87A05FC418AE6D_CustomAttributesCacheGenerator_ListExtensions_SetLength_mCAEE073DDF8FBC79213BE64FCCA3B80567DFA765(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IndexedDictionary_2_t43ED11262F2048E13F66C3A8B445B03E0DD7B475_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator_MathUtilities_Pow_mC169808695BB5B77859857E6BCBD620A9FEC4CD4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator_MathUtilities_Abs_m550FC9B831F3A442BA2C40A369CA77288DDDDC72(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator_MathUtilities_Sign_m94D2E6964F9EDEE8BAE9CBCE6BB33BE61F5FBD2F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator_MathUtilities_Clamp_mE99524BFDD189E0F3773572F7CFCC895B616C478(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator_MathUtilities_Clamp_m284B89E4861746A1509A44273C4F3282FDA6244F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PersistentAssemblyAttribute_tE4D7BDB9117A1E22B2EAEEF4A3A976AC4F935854_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1LL, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t7CC972480E7F6CD842E14F6007767B4C36184C19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SirenixBuildNameAttribute_tD2F7F06A523D6D952301778DAEDEDFA8AFF30175_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void SirenixBuildNameAttribute_tD2F7F06A523D6D952301778DAEDEDFA8AFF30175_CustomAttributesCacheGenerator_U3CBuildNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SirenixBuildNameAttribute_tD2F7F06A523D6D952301778DAEDEDFA8AFF30175_CustomAttributesCacheGenerator_SirenixBuildNameAttribute_get_BuildName_mF45EE0EB16B5E298B342C707C4BD5E00D24ECE8E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SirenixBuildNameAttribute_tD2F7F06A523D6D952301778DAEDEDFA8AFF30175_CustomAttributesCacheGenerator_SirenixBuildNameAttribute_set_BuildName_mA4D04890D1D89B974C22E1B19FD9268570B8E078(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SirenixBuildVersionAttribute_tEC151875E0945B59C210D365DDA385F743A3C964_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void SirenixBuildVersionAttribute_tEC151875E0945B59C210D365DDA385F743A3C964_CustomAttributesCacheGenerator_U3CVersionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SirenixBuildVersionAttribute_tEC151875E0945B59C210D365DDA385F743A3C964_CustomAttributesCacheGenerator_SirenixBuildVersionAttribute_get_Version_m44D7DCC05E81DEC96439A765FAEC8DD2E883C28F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SirenixBuildVersionAttribute_tEC151875E0945B59C210D365DDA385F743A3C964_CustomAttributesCacheGenerator_SirenixBuildVersionAttribute_set_Version_mD6B19A36745950F52774F9EEF74E021C9E04249A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StringUtilities_tB788381F3751AF3EE17C33AD1078C828C8B8F958_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringUtilities_tB788381F3751AF3EE17C33AD1078C828C8B8F958_CustomAttributesCacheGenerator_StringUtilities_FastEndsWith_mA9B2FEC333CBFAF6EFADAABC1E6E27E4B8134D68(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UnityVersion_tE4A3A4D950BC8D7D4C4E368E8D7126E7D207E410_CustomAttributesCacheGenerator_UnityVersion_EnsureLoaded_m35CA01CA335803318DE771A95E8C214B3A841345(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_tC69422E79DA752D8A3B421C5CE82507C8F7E18EA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Sirenix_Utilities_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Sirenix_Utilities_AttributeGenerators[521] = 
{
	ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator,
	DelegateExtensions_tC12EB1BA8174ABF92B33663CC84CE81CC15F73EF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_1_t84FD7F2B762987911F7A7DD357C50853DF46FDBF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_2_tC9D77AC175109F0685ACC75E1A59545F27378587_CustomAttributesCacheGenerator,
	FieldInfoExtensions_tA1D80608B0901949CE0A720C6F2082A26048627A_CustomAttributesCacheGenerator,
	GarbageFreeIterators_t204682AE6F1C54453D207E271856B5F1FBDFED8F_CustomAttributesCacheGenerator,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator,
	U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator,
	U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator,
	U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator,
	U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator,
	U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator,
	U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator,
	U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator,
	U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator,
	U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator,
	U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator,
	U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator,
	U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator,
	U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator,
	U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator,
	U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator,
	U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator,
	U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator,
	U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator,
	U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator,
	U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator,
	U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator,
	U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator,
	U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator,
	U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator,
	MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator,
	PathUtilities_tE2C12117076D2C8243234A91CE5D992E38F6D335_CustomAttributesCacheGenerator,
	PropertyInfoExtensions_tD214C155D903AA7C382DD73C95C8558BB63AA3FB_CustomAttributesCacheGenerator,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator,
	StringExtensions_t32B32D9752767D336798A0E94A1EC9D3F58E6704_CustomAttributesCacheGenerator,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_t9303AE7132ED494071086287CAEB1462CFB3B6E8_CustomAttributesCacheGenerator,
	U3CU3Ec__25_1_t4F2E737EB50FE5C38F489C2E5221868768FC324F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_tEAD70FF851758143764FEAF289F1995E271464CB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass34_0_t5A596E24D8ED1B469CBDB10CEBDA0AC63AB318DD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass35_0_t639A7EBA788055862A10E4D3E447197715AC43A0_CustomAttributesCacheGenerator,
	U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator,
	U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator,
	U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator,
	U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator,
	UnityExtensions_tD3EFA04258F9C647E2C1D20C616D5C3991C000A2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_1_t6BEDE50B0B8D613C0C70C5782161CC5DEB55B504_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_1_t9D35061C753A9238A5E0CF0FD2499E1C22024FDF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_2_tF6D37C166E1B6C94E34DBBDA4FA34C6F5B91FC14_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_2_tF5FF3077923F71466B7D55A2A9C2C43426AAFA9A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_2_t62FFFC9705F04BE6A75D16A443AFC3B6F6588B8F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_1_t83A49D7F615431D5E39BFCB398B96AB64BDA4199_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_2_tDC2C26BCFC97CDC5F3BA5E0C19CAECE49EC993FA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass18_0_2_t6BC4702979992F0DE82801D1D68C9657C41C9F59_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_0_1_tE581833927451E946F41064DD8BFAE421C385FC6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t75729FD56ABA0B98917619AC7F77A97BFCD0EE46_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_tF812986FE8492E826F6999E79A41A8EA8C950895_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_tE962850B6BA008EA33B6F2D15D5957F1DE470B9B_CustomAttributesCacheGenerator,
	DoubleLookupDictionary_3_t913197955594243A3726430930E1906D28CDE556_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_1_t1A573CADC1E792CDFF89D3BEAA7F39CDA46CA3D6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_1_1_tE1579A8FA288839CC0CDEEE1B72DC6B97A7E94EE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tA4A7A3E24273266A0E627EC9A0C076495B966581_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_1_tA03AEBEF02A739AD8061E922D82CCB1F2B756C2B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tC4782CB7998308866A013948B73003D8A303C434_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_2_t7726FD61CF0E97F877A62EDE557A992F20E8C40A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_1_tE14FF4D7B3C2BF3648BA4A93732124857EDB8A44_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_tE78C237336171DFAEDEAC1FB8CCA738B48847840_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_2_tBCB0DE1E2F262B4C6F454084E0AF9CA94B177F8D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_1_t2541637D794E09E89B2082464E3A04F88086A71A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_t562D56E42ED912CF095D7B5D55FD766C019AD8BD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t18E2F87BD90000BED545B3BCD55F85DB6DFAE7C8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_t141B667427E87F84B8BAC5E9363EE5C2EC8E7917_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_1_tAF59221F597F41D3845BEECB88AF93841256724A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_1_tE4B4EB18EDABD9F69DBCE32FE8CAEB6442FDA9F1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_2_t25591E98AED7932DD79C0326B71CE2A3B4085F11_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_2_tAE63B212838B9A2C28C039204512B71CA22324E7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_1_tF8FFDF12D94C2D8B4CF9D5A21CEA8FFD212D8AE4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t8F35DECB5FA5111963937705845DF71817621700_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_2_tE5A61DE88712303199D6F987671F4E8F04345116_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_1_t821DE62CBF1D7984C35E09D7DACA6D157CC082D5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_2_tEBED323A92B0881EB9F8769746272E76D7F6DAF5_CustomAttributesCacheGenerator,
	GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02_CustomAttributesCacheGenerator,
	IImmutableList_1_tF0ACA080D52BD259073B9A5BD0C24B51042AD498_CustomAttributesCacheGenerator,
	ImmutableList_t49F27F3327CCD3A8BA9D533BB610022C4A9312DA_CustomAttributesCacheGenerator,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator,
	ImmutableList_1_t50D68A24605228295D545DA4AA84B5D9A25F9729_CustomAttributesCacheGenerator,
	ImmutableList_2_tD588EADF657CF70731C80E78760AF63D00AFE5A5_CustomAttributesCacheGenerator,
	ListExtensions_t4CDFCD31F6080E2522A133523E87A05FC418AE6D_CustomAttributesCacheGenerator,
	IndexedDictionary_2_t43ED11262F2048E13F66C3A8B445B03E0DD7B475_CustomAttributesCacheGenerator,
	MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator,
	PersistentAssemblyAttribute_tE4D7BDB9117A1E22B2EAEEF4A3A976AC4F935854_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t7CC972480E7F6CD842E14F6007767B4C36184C19_CustomAttributesCacheGenerator,
	SirenixBuildNameAttribute_tD2F7F06A523D6D952301778DAEDEDFA8AFF30175_CustomAttributesCacheGenerator,
	SirenixBuildVersionAttribute_tEC151875E0945B59C210D365DDA385F743A3C964_CustomAttributesCacheGenerator,
	StringUtilities_tB788381F3751AF3EE17C33AD1078C828C8B8F958_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_tC69422E79DA752D8A3B421C5CE82507C8F7E18EA_CustomAttributesCacheGenerator,
	GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02_CustomAttributesCacheGenerator_U3CUseAssetU3Ek__BackingField,
	ImmutableList_t49F27F3327CCD3A8BA9D533BB610022C4A9312DA_CustomAttributesCacheGenerator_innerList,
	ImmutableList_1_t50D68A24605228295D545DA4AA84B5D9A25F9729_CustomAttributesCacheGenerator_innerList,
	SirenixBuildNameAttribute_tD2F7F06A523D6D952301778DAEDEDFA8AFF30175_CustomAttributesCacheGenerator_U3CBuildNameU3Ek__BackingField,
	SirenixBuildVersionAttribute_tEC151875E0945B59C210D365DDA385F743A3C964_CustomAttributesCacheGenerator_U3CVersionU3Ek__BackingField,
	ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator_ColorExtensions_Lerp_m9889C6B728D39820E6B193DBFF1E3F7B094167C4,
	ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator_ColorExtensions_MoveTowards_mA4FC1B1A0D4E73C9582A603442CF90F303688987,
	ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator_ColorExtensions_ToCSharpColor_mAB3E86EFCCD979A2A9EDD3A7CC909003D12F16DB,
	ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator_ColorExtensions_Pow_mAA9F009DF4579636872558B8C413716B5A699F1C,
	ColorExtensions_t65440A3E71BB1459882FC305586381C826FB3EA7_CustomAttributesCacheGenerator_ColorExtensions_NormalizeRGB_mA7D50C50232CDB4E393640BC60C4AE1CE71DC2B1,
	DelegateExtensions_tC12EB1BA8174ABF92B33663CC84CE81CC15F73EF_CustomAttributesCacheGenerator_DelegateExtensions_Memoize_m4C3B10402AE28FDB2D6E6D6CFC62AB3683EE6734,
	DelegateExtensions_tC12EB1BA8174ABF92B33663CC84CE81CC15F73EF_CustomAttributesCacheGenerator_DelegateExtensions_Memoize_m7CCE5A213884A224FECB9C959830CE42C7FA857D,
	FieldInfoExtensions_tA1D80608B0901949CE0A720C6F2082A26048627A_CustomAttributesCacheGenerator_FieldInfoExtensions_IsAliasField_m6845C750241C884522A12B3DA22744346CCA3E46,
	FieldInfoExtensions_tA1D80608B0901949CE0A720C6F2082A26048627A_CustomAttributesCacheGenerator_FieldInfoExtensions_DeAliasField_m0E168E8E55F57E758C73B014393C579F393AB179,
	GarbageFreeIterators_t204682AE6F1C54453D207E271856B5F1FBDFED8F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_mA3B6B3149200CACAA93A0D6F7AB93E1094B0067F,
	GarbageFreeIterators_t204682AE6F1C54453D207E271856B5F1FBDFED8F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_mC3B094CCF3B098BBE20B3F5CB5FD53AE0DB2C7E6,
	GarbageFreeIterators_t204682AE6F1C54453D207E271856B5F1FBDFED8F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFValueIterator_m786D757BAD9EDB92ABDF5812B5B526168F39D684,
	GarbageFreeIterators_t204682AE6F1C54453D207E271856B5F1FBDFED8F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_m306C3F0A9A30D1D539F9B894B559FDB5864D7051,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Examine_m4A8A9E7CCDB237223A822F7864A3F067A6E414FE,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_ForEach_mFDCA586AE2178A9FBE288BFA3AAAA9385288480B,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_ForEach_m7BC9C671C2CE0488FB4747C88605B2F3AF78A222,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Convert_m008CB5D52FE69558AE092ACD1C940891AC41036D,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_ToHashSet_mEC8F33951CBF5419706728B1B3A7B14DBAB7A4C0,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_ToHashSet_m1A8AD5BE47781DF55FBD94864812D3DB0156241E,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_ToImmutableList_mB8F513CB48B8E4E019681A9263565F420C15AF26,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Prepend_m4F2FF56A82DF3B7C387D22096FAFD4D257F09AF3,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Prepend_m9B68AB5EE3D010CD955C3F429F0F34467497966F,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Prepend_m48DAFA937E3DFEFEC07046CA28F666A2C1A67F24,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m2233E3BC51FEA933C83489C8F29A7497BA93510D,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m730A42177FDA921A15AD43F95E9D523FC32DC39A,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_mE5B612C3D9F2B50E7DD0E21AC3CC953C86A1FCB3,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m63A5EB8A4722239D095CB27CFF45AB561ADD9071,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m9EC545BB51EB45794B30392019FF875811841F75,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_mF6C8FBA11106B155637BFC5E4B74D4673427E288,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m2C56B55DA06796E75DD829E52FB51C897CAED49A,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m5E291D29A182FD8A59BD8ACE20F639187C04BE1D,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_PrependIf_m396045EC84534F71B900521110504934DFF2836F,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendWith_mD9C964D33B14E1A26465D45AEBD0E5E9B77825A7,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendWith_m11ABFB9EEF918F0E7959B82E21CD8213E92FF59C,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendWith_mD61C9BBB576A102DC0D0B7DAAF8C636572D6C383,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_mE4A774A5F56BD9DD466108580CAE9E83EB41EF49,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_m9A6718618A6569D6DEDAB0DA6812390AB74BE0E2,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_m9AF95550DE0A6F426731E801175997FE22075FD4,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_mDBE1FB7809D7D2B1617E11D77108F9B768284D84,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_m17840B5E8C3F431692C5DCD78E1D3399E568B831,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AppendIf_mC9F220AE210BC2B2D8E82BA74666F2FBDB8716BE,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_FilterCast_m7E5A9F4B594B01726744E3A4F31477791610D10E,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AddRange_m67C2BED6D7F43EEC6413EC43286E804AC343C380,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_IsNullOrEmpty_m57C20C49B35F4875EC61FE3E7561E6223B93A2C8,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Populate_mE029AA25F3EFE9E0704464406EDDAF3A27E54635,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_AddRange_mC77AF9E9B1EC01E772742CD43D322FA7663C87F4,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Sort_m9110BEDDEF0F25E4744C6506FD9B80979553AE7E,
	LinqExtensions_t6505286184A8CE2FA19DC9F6098D4CE10B750593_CustomAttributesCacheGenerator_LinqExtensions_Sort_mEB6AC3699E7D177D4B68C65DAF38D6794D2C6AFC,
	U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1__ctor_m28C9CEFBE1A0D5DBB913D720E903725D790B0F3A,
	U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_IDisposable_Dispose_mAE84E5AF12A4C57BFECB35D1C339A52E4B251291,
	U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m5308FA974EB21604816921C5932E8ACFA3CE8740,
	U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_Collections_IEnumerator_Reset_m4CF073091A7E2E25012BED1E8682C8787D734B90,
	U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_Collections_IEnumerator_get_Current_mB58B1B3CF79790C936C66737B2F5B0215C4BE043,
	U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mEEDAF43AE4F45E8F2B2132A48235AF28562A7FFA,
	U3CExamineU3Ed__0_1_t93F13A6363EAAE2F1D31C7064BB2A2A55EECFABD_CustomAttributesCacheGenerator_U3CExamineU3Ed__0_1_System_Collections_IEnumerable_GetEnumerator_mF0C38ADCCCE8E1C6E81E03084C1A8AD1FEE5B9BD,
	U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1__ctor_mBB536416F131D3F745765FD172C0A8CB04FC4547,
	U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_IDisposable_Dispose_m7258E978B8FEC75122CC2421EED2824927711F79,
	U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m0138DFAA43EB92B889997D83B6F8CA92BB22DA66,
	U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_Collections_IEnumerator_Reset_m2D69FABE2907C9225CD9AC327F935DBDBFB50A09,
	U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_Collections_IEnumerator_get_Current_mE3A24BA3A5CDDEDBD6978B850FDC5AD5493A897C,
	U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mC2450E0E66A67CD4A216EABBDE5FAB1FB2FE057B,
	U3CConvertU3Ed__3_1_tF76E78704B468D54E1E72A5D5972B039F17FBDD5_CustomAttributesCacheGenerator_U3CConvertU3Ed__3_1_System_Collections_IEnumerable_GetEnumerator_m6B14E440F0F9216D1B7AFAC8E7F75CD54F568708,
	U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1__ctor_m4B32E8E47DFBAE2B02C0D1359F40478BD7331773,
	U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_IDisposable_Dispose_mC91D3DCBF9384B68A6FE7E134945894B4F7647EE,
	U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m93BCDA002725B8F88251863120A41F554B7DD885,
	U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_Collections_IEnumerator_Reset_m9D17C7772ECBD8D5357C5DAA454F7A2D5C4CCA89,
	U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_Collections_IEnumerator_get_Current_m8A2370FF5EA66712367575CC1E046695FEE63A28,
	U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m656200D514881A35DA40B4F0096C366C1110CED2,
	U3CPrependU3Ed__7_1_tCA07D65773DD7AFB971B138498F0CB508490BB4F_CustomAttributesCacheGenerator_U3CPrependU3Ed__7_1_System_Collections_IEnumerable_GetEnumerator_mF5DD1FC956A6AAB9972A7FE42C9F38DFE3C7EC12,
	U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1__ctor_m8E9031BAA4D54EDFC28D8CD016826A91079ECEE4,
	U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_IDisposable_Dispose_m80ECB642C64CB0FE4C2A08BD97EF6735A16D804E,
	U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mCE6903C5B925D7DFB2A9452355DF81E01E341E33,
	U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_Collections_IEnumerator_Reset_m1BDD6DC6398D1A34F43916C7F858AB0C9F98C6EC,
	U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_Collections_IEnumerator_get_Current_mE7866B0EC54FA41DB1ABDC84D672571F9F648EE3,
	U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m672C327B7BB42260ED4C894C989C979E0C9A5E72,
	U3CPrependU3Ed__8_1_t35BA6B9497416EE8C20A2B70DB6903B0FF66694D_CustomAttributesCacheGenerator_U3CPrependU3Ed__8_1_System_Collections_IEnumerable_GetEnumerator_m63F22DA9A5B43D28AF222D77DB5247FB44E4163C,
	U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1__ctor_mC891923536B77B0A6DB290231A8A61502BECBA0D,
	U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_IDisposable_Dispose_m5005082BA371F69B480786817424A9A460958AA9,
	U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mC6B0B0631DEF274F3326E809FAB01774AAE64D8B,
	U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_Collections_IEnumerator_Reset_m59D946D256CFD43C0D57619DBEEB899DF1D0783D,
	U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_Collections_IEnumerator_get_Current_m8E49399F51E18A69F917021319EFDF53B2DB6504,
	U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1E6AC42CF9575583B63ADD30B4B16FBC06EF384E,
	U3CPrependU3Ed__9_1_t43ED6223F9D247817E7100064C6CC2DB612F6003_CustomAttributesCacheGenerator_U3CPrependU3Ed__9_1_System_Collections_IEnumerable_GetEnumerator_m2C9B47F6FC6490FF7AAE3C0B62ACD8C3D47B94A3,
	U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1__ctor_mEB1CAB552B3EB0AF4AE9FC497F5823E3D7259211,
	U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_IDisposable_Dispose_m28D1ECD420E2166D484675128ECED4016DA42D71,
	U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mC636CD114AC43B0B2C514658D153615323AE3459,
	U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_Collections_IEnumerator_Reset_mD737295CB4874C3666C2A8DF141D991953D6A639,
	U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_Collections_IEnumerator_get_Current_mBEDF0698EA14F468E8FB1DF29163F10FC01E21F5,
	U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mE665EA218704AD3AC886CE933317328906A4AAF1,
	U3CPrependIfU3Ed__10_1_tE6AFB9F66F911F7870E753C6567A2AB1F7A3DFAA_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__10_1_System_Collections_IEnumerable_GetEnumerator_mB35FA74CBA01805A1FB3B1E534AFC2E05C1E0FD2,
	U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1__ctor_mB34BC2B5B4B408D2BBABFA74A7D6B750CDEC5D94,
	U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_IDisposable_Dispose_m3A8B2D72558FF21654DA4088DF867A556C78558E,
	U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1940336AF9AD17D93410EBDEA6AD71F94972C37F,
	U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_Collections_IEnumerator_Reset_mF53214FD27DDA876479589D96A2651B7003D617B,
	U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_Collections_IEnumerator_get_Current_m89DC52E1584BB1DC475EAE3ABCE6335016D6C9B0,
	U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m010F50E5EDD1513CC283125CAB9D892A7B0B6E7C,
	U3CPrependIfU3Ed__11_1_t1CD40A644999E2CF56078D90B9159E126B922FD8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__11_1_System_Collections_IEnumerable_GetEnumerator_m8EE72E94E5F7A491EDFBF2AA1767BEF0EFD4A568,
	U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1__ctor_mA2BC3E5F24D8BE6E301541B203DD1E680395F748,
	U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_IDisposable_Dispose_mE505461409F9BEF046B4E3533F389156D0EF7ECC,
	U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mDD2BF2B1B3F3943ACBE31E9F66C715583B875459,
	U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_Collections_IEnumerator_Reset_mCCF9C854ED135F00206BCD3842A54926C32C1D78,
	U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_Collections_IEnumerator_get_Current_mCD6572D3BF4852CC1B460A4277060CDAFA431CBC,
	U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mD4DDEE529A6ED7CC52216362865D2E55D2D338B2,
	U3CPrependIfU3Ed__12_1_t559EEE727171E0A14C368B073A7EAB025EB7258B_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__12_1_System_Collections_IEnumerable_GetEnumerator_m0DB5B02D2FFA640EE6349FE5264C5A5484CB7881,
	U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1__ctor_m4E25BAC7D98CEEE59173734BACBBD5F39DC89E4E,
	U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_IDisposable_Dispose_m325ACD31949E07142C8C761B213CDDD9A93C7BE0,
	U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mFF48BC102A62E106A8FB2D1C9C957F9455EF745C,
	U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_Collections_IEnumerator_Reset_m9505F99566D8B075235F58F683F7F0AF1B2C823A,
	U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_Collections_IEnumerator_get_Current_m92337527F3B18EAA002171D991336953D48BFA2A,
	U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2DE6CD183149D9BF62B4A14CE1382181834C18D0,
	U3CPrependIfU3Ed__13_1_t24F8BEAE3640EDFE9326BDD137B399BC8C5AADC6_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__13_1_System_Collections_IEnumerable_GetEnumerator_m19FAD7060D1B4B08E6D1BE63D297D11C45C2F453,
	U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1__ctor_m7E5B1679FB0450BA8AC4C93B8A792854AECBB8C2,
	U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_IDisposable_Dispose_m9A8CF92E97796EB5774E305BC682579F202FD7AD,
	U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m8F8C076FAF765264505F7E2326DC1DF9A00B7FA2,
	U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_Collections_IEnumerator_Reset_m2FE8290D930F3D2A6B1E7E23453810F0D7A3A8CC,
	U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_Collections_IEnumerator_get_Current_m30485953A9095CA963AC42B5BA4EDB64B5E9FFAD,
	U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m211D6A5EDE7E5D67C44FAA115EF78333A68D52A3,
	U3CPrependIfU3Ed__14_1_t0E9340071335D999F81D85A5BDE7D015C4D9F9D8_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__14_1_System_Collections_IEnumerable_GetEnumerator_mC355FC9586EBBCF13C46636E81BA374B0AF495AB,
	U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1__ctor_m81A3B193993A1BA4F834DB4F91BDCDB670CC8178,
	U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_IDisposable_Dispose_m52481EAEA9D4D25647AD2945A6A96DE112120A4A,
	U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2A396FB9281D722A7179A98076DE6B05DC7EDB49,
	U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_Collections_IEnumerator_Reset_mF89773F61F87393DDD750787B9E48D1D8029A912,
	U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_Collections_IEnumerator_get_Current_m33BD879DBCEB10BE318449B25EAF97A6A03C7998,
	U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m5BBA0F55B07A2A8C6CF05767BBBB13FFEE9CB211,
	U3CPrependIfU3Ed__15_1_tD6DC099B9FF367E5DE8FBE52A40735E4A967A063_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__15_1_System_Collections_IEnumerable_GetEnumerator_mE69A6806918FDB41A9B46F36A577EDA6C2D0A2AB,
	U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1__ctor_m37883249807FC5A5D81FF8AE30E082CBE6086CF3,
	U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_IDisposable_Dispose_m553279E85BDFD2A5402F2203E9ED0EDDBBA830FC,
	U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mEBE118017130DFEB39F74B7D5B87E71D30670B67,
	U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_Collections_IEnumerator_Reset_mA7F6E158F8D663EE6A62FE599771748DBAA8135B,
	U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_Collections_IEnumerator_get_Current_mF5DD5F1C259368589CD640C1F966FD457CD33481,
	U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mB921B3820FDD393AADEDC43AEEA9013B52EDB63E,
	U3CPrependIfU3Ed__16_1_t0A3F1047DF75F13873F8F26C15C0A7B81035E956_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__16_1_System_Collections_IEnumerable_GetEnumerator_m39210121B5FEE445D4545D234778A0BE71240F9D,
	U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1__ctor_mC3BD0D897959C60FB90E139FFF6619D52D05997E,
	U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_IDisposable_Dispose_mDF272AFDC581CCCF78FC8E21300C99FB8769ACDB,
	U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mE32EFFD67FAD27655EC15D6474B507843F75CD0F,
	U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_Collections_IEnumerator_Reset_m2246C445D17EA445D74219A31EDBF819B96D5A1B,
	U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_Collections_IEnumerator_get_Current_m502FDA0AD271D773B6FD68F8852AA4171E8E5D1E,
	U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3ECB65F0161DED569F53C158A8B21031ABE8D1C8,
	U3CPrependIfU3Ed__17_1_t856C4D70B129645176A8FE98F2F1ADF430668177_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__17_1_System_Collections_IEnumerable_GetEnumerator_m9C54FCE096DEEABB871CC617514AE3D5A55D02E1,
	U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1__ctor_m6D4F3DC494073D778340AEB1E72A035C507F54F3,
	U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_IDisposable_Dispose_m478564692E000CC3B76A1F391865B8C19E78A378,
	U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mB05E3EF866D5AE540BD325CE88ECAC721D430B49,
	U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_Collections_IEnumerator_Reset_m303C81C9104E8328AE3B33EC3121EB869E45F083,
	U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_Collections_IEnumerator_get_Current_m9DA1E43666530CA2EDC8C3106EBCCEA6A8A6F4A7,
	U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mEA8E8804920289637B216903FFEC80F43F9AE43A,
	U3CPrependIfU3Ed__18_1_t751E284FA11625D24D4B975B09C110D74933C2B0_CustomAttributesCacheGenerator_U3CPrependIfU3Ed__18_1_System_Collections_IEnumerable_GetEnumerator_m7C5021FC365833F6050D97810286DDF9E8D75E13,
	U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1__ctor_m4C02B46D6E60E23FCAB794DD24A04BB2542A47F3,
	U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_IDisposable_Dispose_m28047C36017B782AC07FB34213B0C76D89F49DC0,
	U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m397E2A7040D417BFF1F5403670107BC4165DE9EE,
	U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_Collections_IEnumerator_Reset_mE3542B9A108BB3D93053BE06650CE1076E4D2F6C,
	U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_Collections_IEnumerator_get_Current_m0C0835CFBDC67A750462A8D637D2DD94AF71F74B,
	U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mC64796CD6AB6894BA6F94DF140B105F0539D841E,
	U3CAppendWithU3Ed__19_1_t83A8A41E423DE6DFB90D3525ACF5EB3B415959BE_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__19_1_System_Collections_IEnumerable_GetEnumerator_m96C7FA5C72763C81251B67E3A186EA16C16131CC,
	U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1__ctor_m3FF9E472DFFA87F67C6B4A2B34F92AF053075144,
	U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_IDisposable_Dispose_m169E082A5B36189D4C18E55A4FC59B7F574463DD,
	U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m04F694FAD7454B06932D31B02BF11EDBCD610BD3,
	U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_Collections_IEnumerator_Reset_mCC00B2E81237862F829E35BD70215B03C8A5AE88,
	U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_Collections_IEnumerator_get_Current_mFF7FCDFB338D3FFD81DC3C97DC6070774E305CCD,
	U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mC891764E86772D06B76FB43900EE2C52FF3AD231,
	U3CAppendWithU3Ed__20_1_t7ECDD25051FD11EDE41A6B71CF3F935CB84F82A3_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__20_1_System_Collections_IEnumerable_GetEnumerator_mBB0B0416D88B8386B0D4E8C4A49801D92F787088,
	U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1__ctor_mC704E6A2E38F643E01C18D0E2A33910F451A54BF,
	U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_IDisposable_Dispose_mFF3B8BB49DED97D5D2D41DAA9855C9481D61BC31,
	U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m8EE9474F8352298A69CCCA0B443D2E7C90F47AE5,
	U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_Collections_IEnumerator_Reset_mBD03819B8B1FDEEE65A62A20FA099ED247AD7625,
	U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_Collections_IEnumerator_get_Current_m52E1E2983E16DF723C8BE2EE6C30B9BC0E9D4FDD,
	U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m8838EF68F16A2BB06D39AF2080F1D61A0EDAC9EA,
	U3CAppendWithU3Ed__21_1_t59DCFDA0F0445754F85A763243E2B4D2BA992D4E_CustomAttributesCacheGenerator_U3CAppendWithU3Ed__21_1_System_Collections_IEnumerable_GetEnumerator_m1925AB55821D802D8CBE61C0A54FC420F3BA7EA5,
	U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1__ctor_m79090569A237807B75DD8E807BE7AD1B52812AF3,
	U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_IDisposable_Dispose_mDEDC2CB802CF176A2C8D3DC902C50267AA91E70B,
	U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m42E2442609FC61478533A9BCB4A9BE7500E29913,
	U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_Collections_IEnumerator_Reset_mB5172CEA7E75183CD507D7183CD2360143A61785,
	U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_Collections_IEnumerator_get_Current_m70969F9028EC816086C92606A30F4A4F79B314B9,
	U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mA8467099FDA4BE8FEEE45BB9D4E8F582EC8362A9,
	U3CAppendIfU3Ed__22_1_t41C6BA9EA8237CB69194089E051CD1DA1863D90C_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__22_1_System_Collections_IEnumerable_GetEnumerator_mFE2AE0B91BD917991D7961D084A546C0BE232C1E,
	U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1__ctor_mBBCCB5B2AE7A9F61A9CAD2AE1380A54BDB467C97,
	U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_IDisposable_Dispose_m5E95447499C25CA1D13896AF7AE26147487767DA,
	U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3F598E452BDD0113941CB53A5706B1E073405BF9,
	U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_Collections_IEnumerator_Reset_m85FF226CD7567DA86E505D978E1D74F0B89F5C75,
	U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_Collections_IEnumerator_get_Current_m2E19FB33AE036DF5C28035CF046B84CF4ABF0E2F,
	U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25912AEEA751480F735E98229737FC72136E852A,
	U3CAppendIfU3Ed__23_1_t496C1B2E1DF9846A78105AC5337B7CB99D27B3B4_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__23_1_System_Collections_IEnumerable_GetEnumerator_m135A55EAD0EEDC57B5CAFE04B0F42D48C64B94EF,
	U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1__ctor_m770A93895D0A6E35F1D9021C13DDDFCE5E265403,
	U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_IDisposable_Dispose_mB6B9CC65335719DEC231E3B964804375E7AF61D7,
	U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mC187A8EDCC5319574A8767B522920AB4D9A91AFA,
	U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_Collections_IEnumerator_Reset_m0EE3B1241D648AA01FEF9E82027BA003C741914F,
	U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_Collections_IEnumerator_get_Current_mC2397FB190EF906289423289028E877EFDEED3FF,
	U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mD46734FD61413A944AD1CBB596D34B0541B46D94,
	U3CAppendIfU3Ed__24_1_t7C2058296D6807A66218DA392B4CC203DC9CB103_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__24_1_System_Collections_IEnumerable_GetEnumerator_m0B68AD1245FB26B983A19CD925CAF82CDB5E6297,
	U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1__ctor_m2895D05215E1D3E80789175BEA1D90B14FDB0342,
	U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_IDisposable_Dispose_mF3D7B66A181CFA186796DABFDF455C65562A9E24,
	U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m84CE24B58A9740BFCC68CED618E908FCF9E6E188,
	U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_Collections_IEnumerator_Reset_mC52F6CBDDA80560018B03CDEC7610126623EE64F,
	U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_Collections_IEnumerator_get_Current_m1F3E2B37C87E359D96C7522E99C473E97D91602E,
	U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mBDAAF964103DF49A1166DB5A12AA1B7799F369BB,
	U3CAppendIfU3Ed__25_1_tE9B3967E7E3ED502AC833A1B6499539C67D7DEBA_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__25_1_System_Collections_IEnumerable_GetEnumerator_m84A1D6DF612A4296555AAFD6B5C65BBECD69AC0D,
	U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1__ctor_m10376BA34A68F5A9C4485EF461B5915B97C5E3FF,
	U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_IDisposable_Dispose_mDC9811AB95140E585F74A0A1282354E69863B0B2,
	U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m42301BADA9E41F135B7685E875EFC1626E368AF6,
	U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_Collections_IEnumerator_Reset_m37BF1318910E389409DFDEF9AF28F719214C8B54,
	U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_Collections_IEnumerator_get_Current_mBC3481184F84834D4F44371E49A7FDFBB0EFD15D,
	U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mCFE18B909C863F207934851059BC2242549E56C6,
	U3CAppendIfU3Ed__26_1_t39C392219F8A316787DE261F8975290DF972E800_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__26_1_System_Collections_IEnumerable_GetEnumerator_mE3C1F68D136F0CCF7645CA9D264AB172309050B9,
	U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1__ctor_m9EEA630CF0EF3D3C670A50C5B38E256D81EF6A4A,
	U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_IDisposable_Dispose_m27488063FCD722F90D245034A8488BD3F088068A,
	U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2A093D599C7AF540B0AB347E59FA6F9628F5A3B6,
	U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_Collections_IEnumerator_Reset_mE70A9DEA2643019A9126D70BF76AF2008A09FC68,
	U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_Collections_IEnumerator_get_Current_m6087D07BC2AF20A166A32AB3E3EF5A2E7D63E4E4,
	U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mD46BE45CDE2AE546102283A9711CA2A41E92444A,
	U3CAppendIfU3Ed__27_1_t389F2962214D8B95A4BE20B152DFC52C96770FC2_CustomAttributesCacheGenerator_U3CAppendIfU3Ed__27_1_System_Collections_IEnumerable_GetEnumerator_m35350602E2AF8F2468D6BF4FF6D7EB57535D7E91,
	U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1__ctor_m790B8F4A5AD61EA55E9FE5BDC1490EA95F344DC5,
	U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_IDisposable_Dispose_m4B9BA567C19438B2275F25DE65FF226A774B41BE,
	U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mA2FEEDD6C794181BC31776A52966FC03BB1F7EED,
	U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_Collections_IEnumerator_Reset_m82432AE9E4180AA6737F217F0FCBC2E51C5F4387,
	U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_Collections_IEnumerator_get_Current_mA062D57A1B74E398EDD66C9435CE9160FD944F34,
	U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mB34F50C6FDA2613133CCC0B263448D2E211A4AE2,
	U3CFilterCastU3Ed__28_1_tEB6E0E6A505C833DB45A259C5C5C1ED09BFCD162_CustomAttributesCacheGenerator_U3CFilterCastU3Ed__28_1_System_Collections_IEnumerable_GetEnumerator_mFF0321109979E20D657B613408D6632426217310,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_IsDefined_m3B3989946BCCCD73866295030F1FDF0245912703,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_IsDefined_mC821E92ACDD5A9732B7C0722555E574EA34354F6,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttribute_mB1DDC06945BB44059A1966EAFA4E166D22449598,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttribute_m7D24BDEE3F2CCB6625F33375B999EF1F890CA4AE,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m857B498AC5F5BE45EFE70386FB0F3E176F709A78,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m9A5476B6768136AF61DA383FA040EC27C26C9A71,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m15AAE26BE285B269CAE126FB926279E0A826E987,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m2FFD9512045ED25E441863CA105281FB2C5E0B9F,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_GetNiceName_m233C26DA9440A5BD00027679176E7A1207811170,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_IsStatic_m0B82DFA6FA5B65EF48F2D3B2799A342F48353F7E,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_IsAlias_mA690365F38B68B1AB5041F68D0A6995FD6DADD57,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_DeAlias_m6C0863F27B3C8362850BF8B960A7AE376D425D68,
	MemberInfoExtensions_t4A34C241899F0A10EBEF403113DD4D4BC190524C_CustomAttributesCacheGenerator_MemberInfoExtensions_SignaturesAreEqual_mFFAA6FB976110B9E1B5CFB81EC23FFA528363A26,
	MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_GetFullName_m2B1D1CF37601A3B322FEA19473EEAB83BE9322C8,
	MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_GetParamsNames_mE7B9162DDC988D41D65DD34E54A7C29839C38F58,
	MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_GetFullName_m5324B40A2D0B23F9F4989566B50AB7EE99F88A47,
	MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_IsExtensionMethod_m7796506EB32D9061808BF565AEDC681C66EDDBED,
	MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_IsAliasMethod_mB15831FE197849EA54CA5E0826EE7EE29275A01E,
	MethodInfoExtensions_tAD2C93B7AEB570BF0D0F0FA524BD38CCECBB1AFD_CustomAttributesCacheGenerator_MethodInfoExtensions_DeAliasMethod_mF8824D0D8741B82891672059C62A31876EF1C900,
	PathUtilities_tE2C12117076D2C8243234A91CE5D992E38F6D335_CustomAttributesCacheGenerator_PathUtilities_HasSubDirectory_mD034F326CD0D7AF99E6F0EFE93550D684CB0A652,
	PathUtilities_tE2C12117076D2C8243234A91CE5D992E38F6D335_CustomAttributesCacheGenerator_PathUtilities_FindParentDirectoryWithName_mE67A19F3CB8AA227659DDD7C5F7958C36E1378A3,
	PropertyInfoExtensions_tD214C155D903AA7C382DD73C95C8558BB63AA3FB_CustomAttributesCacheGenerator_PropertyInfoExtensions_IsAutoProperty_m17D6F1D04D64078B40A4DFC8DB2678E0C88B4863,
	PropertyInfoExtensions_tD214C155D903AA7C382DD73C95C8558BB63AA3FB_CustomAttributesCacheGenerator_PropertyInfoExtensions_IsAliasProperty_m26C4A48FCE446591C97B7BF2BE9F4716559AB645,
	PropertyInfoExtensions_tD214C155D903AA7C382DD73C95C8558BB63AA3FB_CustomAttributesCacheGenerator_PropertyInfoExtensions_DeAliasProperty_m5420C600E36D84EB08C57C666C3080A09C191CED,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetWidth_mC853B7063708E8C35014ACD1210681C80A1BCB47,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetHeight_mF6D4E394172A8BF3335A949463B4D7031E0BD5BB,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetSize_mD39716901BD09A67A29FF813F535B0415F09DD3D,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetSize_mA7B1826F31962FB667C60F5291929BD2D779411F,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_HorizontalPadding_m8E8E3A615990471E3586A9BB20DDB018DB909EB0,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_HorizontalPadding_m89B3727FB917838AF9D05AFF14D389517798BDA0,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_VerticalPadding_m5C4CCF230E193C556128BF8481F1FBBC167D67AF,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_VerticalPadding_m590B82183DB97091EDA9E0C994F54C7D64613C05,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Padding_mFB407178B4A570B3E8AD035E7C25C1E1F5431F55,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Padding_mDA5649C8E3559871C74BA5D72FA1B690F7107486,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Padding_m32E39A56FBB4E5707FB0B008A1E0E59CAB187C79,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignLeft_m614D187B42334A09858A7F7D393538FE7FEB0889,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenter_m3E956AC4B8A25F9474FCA34A34E53F84D45CEEB2,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenter_m9B01538E9E37E34EE70A05A6F776BFE9B8DE6184,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignRight_m574DBF16789091EAA5DBA75D1D3763EF34C046BA,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignRight_m05BE19656017535D2F9E004CB946C36C94FB98F9,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignTop_m6D8F9DE46371856A5B061230B35C6571CE5A617C,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignMiddle_mC5E340F841C52962FCBA450A79F86B76C5AE2886,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignBottom_mBEACA9FC17D0F71F94305F734A21582880214A9B,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenterX_m69C778A0DABE4398737D79D19860AA1FCF428037,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenterY_m7641F9F0AA5704BD40BDDA8B2096C4252686FB08,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenterXY_mF1D830A1A24F00608669BA7ACE783E0C407FE6A4,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AlignCenterXY_m3F714CE8FECD8AAD7E1D6CDF276850C7E5FF95C8,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Expand_mABE02EAE258191B576ECFADFABDE0DFFF8B9177C,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Expand_m81781A1F595C5F5CA55814EE85555D0B2A912233,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Expand_m72D5AC60DDE7B7C771FDFD9663F927DC7D91C08B,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_Split_m97F4A1E73113DB65F4C04D542747DF3E92DB10A8,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SplitVertical_m939E05A9013EE2CD669EC050750E468957F50BB0,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SplitGrid_m6916D7314A9130D72E3DFBE3D9DC81A69218E90C,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SplitTableGrid_mEE23BBAEBD316E42C0731267C95488D9381BDD5A,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetCenterX_m8654D5E6F3D90DFAB618CDE1ABFC4BFB34349A5D,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetCenterY_mD217F8DB58C73B90DF3D0519F3E21387898B0E33,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetCenter_m5D32A84E8B2B0A5AB1190ECD3D2362104E12836E,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetCenter_m9B56A386F413A3ABCD25D840E2E1990D1CEAB2AD,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetPosition_m35513C3D84420597C326DB9F71B1E53ADE6B86F7,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_ResetPosition_mB818EF964F8D1E78385E0D479D7286DD1A53C140,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddPosition_m1D21D830B6D383D35D05666220FEF082A13A9AFD,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddPosition_m029ADEE6AAC59E615BB356040108B633E2E4F60F,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetX_m00477B241A08B949553D77C69BBCA5A322F22A35,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddX_mAC5D747A732603B30BE1BA06C3BB209FB542B266,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubX_m0AC8AB5C0B438C5D03CD0E51895858EC0B890718,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetY_m08E7C979CC840E57214E979DF30C08F5ADD24B02,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddY_mC0A509D0023FC36DF2AAA3702E463850B73C0415,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubY_m0776E2852F6DACCBA11D7472D7D1AA76F51C6057,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetMin_m68BC6C0D21F9F0558B957515260B08CFE44832B5,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddMin_m2860A1F53A2FF827A6EAAC51570CB198DA547564,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubMin_mE28419D690ADD2516463FA44BCB7C86EAA04346B,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetMax_m855BDDE7EAEC02DB394244E15E5F54A045FAE316,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddMax_mD7B96050B949AFA58E950D9EEF27E722B013ADA4,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubMax_m8B41E31D6702D6E3F91DED8E2AD8D707EA1B882C,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetXMin_mEB4BB8CCD0C42D6078F892475907D18190174627,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddXMin_mBF12C4963D04484010D0B3039FB59E19ABD3F39F,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubXMin_m834AF0100D5BF61C3D7FC980C4B85A63D5C04CFE,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetXMax_m3079E8E87F725074D0C38FB308C7205E0AB03985,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddXMax_m00BCE6D7A36B8B9D5CFEC8AAFCE6032917A6DB31,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubXMax_m60AF370F697312E645E8F30F0CF99B8BB93E851C,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetYMin_m11A869747613438D095F20A504AA0989F6C0718A,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddYMin_m4124C8B31491D21DF6B930295B712C60AE2E32A6,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubYMin_mBFFAC829D2AD00C17749B310642E403BFFD63CC6,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SetYMax_mC70AF8DCBFB45C4FC72CEE1E8085DF5F5DB7B61B,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_AddYMax_mA63EBA978081E7C2CED937F2B563AD1F28CBDA2A,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_SubYMax_m51DB920309335CD0C3A7D629016E7C48418DAFDF,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_MinWidth_m77CAE2D37F322D97EE565DBD6F33224ABCAD0DBE,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_MaxWidth_m6D39F9B0FC8CE7ACFDE83CD74805C15ABFEB1AB6,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_MinHeight_m8B4B10F23FBA6A890FE5B8F5B6154FE7BF3AF330,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_MaxHeight_mE30E285546FD1051FCD4B826B0A539447E022252,
	RectExtensions_tB8996F6AAF61B58682985F9895945810367F497E_CustomAttributesCacheGenerator_RectExtensions_ExpandTo_mB98847B1A65AF6FEB80FE423BE4F29707004647D,
	StringExtensions_t32B32D9752767D336798A0E94A1EC9D3F58E6704_CustomAttributesCacheGenerator_StringExtensions_ToTitleCase_mA8A1408F983DDE057FF2DC283C24A931599C25C0,
	StringExtensions_t32B32D9752767D336798A0E94A1EC9D3F58E6704_CustomAttributesCacheGenerator_StringExtensions_Contains_m8AB548F34460F21E93D1F70FF99B15246F4A8D2E,
	StringExtensions_t32B32D9752767D336798A0E94A1EC9D3F58E6704_CustomAttributesCacheGenerator_StringExtensions_SplitPascalCase_m5539F652FCECED016F2AC168F3FB04FD54205163,
	StringExtensions_t32B32D9752767D336798A0E94A1EC9D3F58E6704_CustomAttributesCacheGenerator_StringExtensions_IsNullOrWhitespace_mEE67BC64AC520011BDC8485A9CF67819CEDC6F2D,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_HasCastDefined_m2119B5754B8D819AA028BCD56C3AAF744B4F4960,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_IsCastableTo_mD75B14DD15DD5B1CC04062319CFBC91E22CDC85A,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCastMethodDelegate_m2861121E8A7BB7BB3761C01E8FAD5F1FF94D7C51,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCastMethod_mF07C9F8F7ED2C41152EB2C564246358694A4F587,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetAttribute_mC67735A2238DCFE832D8F16C3941BF0143FC995D,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOrInherits_mE14EC3D52E1F1029666B799F1333A70BB4EDA868,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericType_m1121DC0D840999A914E5574F3F0AD4B26CF8796A,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericInterface_m2F341DAEABDED76559A85A8D20B8550D98305795,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericClass_m0A41ECA8B4244F7ED2BE35D1F971CE6B286F4112,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m0753CF4AB6F4C0C028B12A66C68035E133CA6825,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_m916A7D9B2DD86FAB8764EC572D18A3AE44F1E0B7,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m465C12014C9C4C4ED14FFA0CA5351375A09D2C32,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetOperatorMethod_m7E05D3B4C3D95C62BA8886DCDC060FEC3EF6C152,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetOperatorMethods_mB76C9B9565203292047CF82412370E8FD33F959C,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_mD0E5106F949AD8BC0BC4095BCF4C67050CC859A8,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_m3562615FFFE9EE27CC2D5D0193EAD3C6FFC8F9E3,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_m92335B5FDC7C089A57FA4401D0C0E6BCD44BD580,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetGenericBaseType_m94A62E4BA96C8F4FC5A4F465D14B416468B51F91,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetGenericBaseType_m39F7468A0FF37BB99A5A13025FC2523F0DD1CCB3,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetBaseTypes_mC637233D7ECF7D9C89B70AA1D0C218844C07A964,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetBaseClasses_m62C3445D92A27770261AF5FA50BFCABAAC2D014D,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_TypeNameGauntlet_mED6BB694ABAD8B721B26010774C2AF0F36CEE16C,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetNiceName_mDDEEDB5E0EE8C831C50FF8D56DB2383EE063DCDD,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetNiceFullName_mA2DD7054D6BB866DAE63AFAE786D93BA6DF76DEC,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCompilableNiceName_m339A581D1A09CE239F39F0B892C3AA46DA92574C,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCompilableNiceFullName_m14D75762F69A7E7753EFDAA2DE4A71F7CF2CA868,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttribute_m878AB4F5EFEE7E7C1C0975ADA8867FE5D0DCF237,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttribute_m0743729CC72D695D45AE2D351685FC89EADA5130,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttributes_mA27D2AB317348FBA619A5D630EA60ABCED74A664,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttributes_m411C857A8BB49F74A0BF6F6E1FCB67C6EC86FB0D,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_IsDefined_m68B1F8D45C6BFF6CBDA6B56DE0954E01F0C0133A,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_IsDefined_m1311C286FCA3E48BCFA44154EAC42466741BFB9A,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_InheritsFrom_m4811C1F4003F6D1597DF695C5FE93BB8B32F9D49,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_InheritsFrom_m9CA98952CA2EF39E0DF4BD9E68773E46B6501471,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetInheritanceDistance_m2BC5795354997E6D0A208131F755C2629F26431B,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_HasParamaters_m71D6B60C5F128516E7DD8CA2F9FA4C81988E4659,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetReturnType_mEFB54C016DF12930DE9D99E351D0A67F4C0A872E,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetMemberValue_m92007B8251F5EFAD94286371D20C634600BD7253,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_SetMemberValue_m19EA7B3165678F36D133C6877A405938BCE728ED,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_TryInferGenericParameters_m8B86E7145860BD05F5BA87566D034CAEF046E46F,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m5979A23DB5D7BB878F43406689A1D1E222A84182,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m68E1A8190F47993BDD581D414D13C509579F4F43,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GenericParameterIsFulfilledBy_m6F871B1B8B2427ED49D7FD1A4E0BBC13E0125750,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GenericParameterIsFulfilledBy_mBB6572A82E174FF80D5A9AE454EE8CA6FFCF777F,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetGenericConstraintsString_m74F940F9A005E758932D975A47318666A68B2215,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GetGenericParameterConstraintsString_m6C4C94289FAA168F8C83862ED43A80276BD4E1B3,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GenericArgumentsContainsTypes_mB5405BEBB46D9D14834B1FEFD389AAD72052552E,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_IsFullyConstructedGenericType_m941C7E4175FEB639347AD717A85AD04D6A53CE38,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_IsNullableType_m28FB5D9FB77956D303CF4009FE8E4C62D896822B,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_SafeGetTypes_mB12AC54B4D81C926F95EABC7EABB6622A57C98F8,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_SafeIsDefined_m278C59D57D1127B3783333879C5298B6DFDC2F0A,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_SafeGetCustomAttributes_m23EC650659FE27E327171260426F92C9CD5EBF8C,
	U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36__ctor_m93329B6BB738335150EA8633AC9A62064DE2AE5B,
	U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_IDisposable_Dispose_m696D89A5F3311080407B13BFD1C6131D488FA40F,
	U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mC6C31374620C0677C696479727C2B46974130EE4,
	U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_Collections_IEnumerator_Reset_m7A9247214D92CC94632DF7CBC99660DDCDB73CC3,
	U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_Collections_IEnumerator_get_Current_m33DEDC31C961C26AA43C6CD836286AE5C2EB1809,
	U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mC498D8F9D92C86C5CB7CB8EFA91B0A1B4FBF4B2A,
	U3CGetAllMembersU3Ed__36_tD609CB95C017662F66CF933D8A093700091DBB1F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__36_System_Collections_IEnumerable_GetEnumerator_mEF0676FE290DE50AA21460A696AC5585D6959DB3,
	U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37__ctor_m5C0B1BB7706836620920BF0378663D60442ACED9,
	U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_IDisposable_Dispose_m122701CE24AE448A557DA668F815115D765395F5,
	U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m033546A216735300A541C93881903001625A7A27,
	U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_Collections_IEnumerator_Reset_mCECE57E55D6393810F72C45E36309904E806F9ED,
	U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_Collections_IEnumerator_get_Current_mBDFA5AAE955CF87CFDC232725522A37BFC16CDCE,
	U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mF7674D670646237FF8B332360649656FE5395034,
	U3CGetAllMembersU3Ed__37_t372B783B473A76D61A1969DA1581963578C86A5F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__37_System_Collections_IEnumerable_GetEnumerator_mFE43504BBBC411400F6B50970E44F082B2FF5B17,
	U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1__ctor_mBD6E46486AECB01846A1CDD7F9A655B3D3FAE51A,
	U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_IDisposable_Dispose_m584F3C90E648E7F580E5DA81056CDE1527CAB1C3,
	U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m84F7CE806A076197F0BF43FEE44EC3E807B547BF,
	U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_Collections_IEnumerator_Reset_mA37AF27BD2DD1C3C88838DFB46DC5E68464D4DD5,
	U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_Collections_IEnumerator_get_Current_mE1C13076C88FD97184CDF71355F5A14AC62C5D5D,
	U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1229BF38DE86CAB2A87E72B91417FF89FAE6931E,
	U3CGetAllMembersU3Ed__38_1_t29FCAFE715E4466CCAC699C19D9C2A5F611F980F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__38_1_System_Collections_IEnumerable_GetEnumerator_m42815FFD80C190C0BAEA2D82F9360F726ED41C2D,
	U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42__ctor_mD2A7BF94397412C36C8F81C32F7608F4DD5481AB,
	U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_IDisposable_Dispose_m4BA19A27C774612AA6AAD5AE4A740F3665F113B5,
	U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m739E1E100840478C780CD1C037EBE5DC5ED381A4,
	U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerator_Reset_m322C061B956B87A6E42700C1BDDC602F29CC2469,
	U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerator_get_Current_mEFAE5547493200A0CF71A7EDC76005CC5FF74D16,
	U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_mE8BF5192BF980759DFDDBE1DA4AD0CF2C5C03DA7,
	U3CGetBaseClassesU3Ed__42_t8002E4240E8083A560464AFAD24B848FA3805528_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__42_System_Collections_IEnumerable_GetEnumerator_m6578B926D0606CA6B4A8A0FED583288405B3EBAF,
	UnityExtensions_tD3EFA04258F9C647E2C1D20C616D5C3991C000A2_CustomAttributesCacheGenerator_UnityExtensions_SafeIsUnityNull_m9609D5559427508D80B7A9FFAE9DE8DA1C36A3E6,
	GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02_CustomAttributesCacheGenerator_GlobalConfigAttribute_get_UseAsset_m6DE933FA318D235196AAD7C3A182DB581BFB526C,
	GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02_CustomAttributesCacheGenerator_GlobalConfigAttribute_set_UseAsset_m98E3100E2A2BFBE9C311C1DDA29DEBEE438F73C7,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_mF71247C0669D13A3CCBEB9DDC43CB91FB13D7D2F,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m55A31EA0E7CD9D1B866BF0EC395E0D0523335C36,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5986C856A9B9449B39B542589647440252B70DAD,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m67F6137A2C821A8BF580B221DFAEF24E71C3B334,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tBAC96A287AEBCA0FC0E1D0A85E5A4ECB91D1C5A8_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m81047B92D0044C262E5451C3215F00295380FA9F,
	ListExtensions_t4CDFCD31F6080E2522A133523E87A05FC418AE6D_CustomAttributesCacheGenerator_ListExtensions_SetLength_m9C776B6460315564F68246097670424620DCED85,
	ListExtensions_t4CDFCD31F6080E2522A133523E87A05FC418AE6D_CustomAttributesCacheGenerator_ListExtensions_SetLength_mCAEE073DDF8FBC79213BE64FCCA3B80567DFA765,
	MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator_MathUtilities_Pow_mC169808695BB5B77859857E6BCBD620A9FEC4CD4,
	MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator_MathUtilities_Abs_m550FC9B831F3A442BA2C40A369CA77288DDDDC72,
	MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator_MathUtilities_Sign_m94D2E6964F9EDEE8BAE9CBCE6BB33BE61F5FBD2F,
	MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator_MathUtilities_Clamp_mE99524BFDD189E0F3773572F7CFCC895B616C478,
	MathUtilities_tBBD7245B52B4237AFFCCB31F974010E81DF5ADA8_CustomAttributesCacheGenerator_MathUtilities_Clamp_m284B89E4861746A1509A44273C4F3282FDA6244F,
	SirenixBuildNameAttribute_tD2F7F06A523D6D952301778DAEDEDFA8AFF30175_CustomAttributesCacheGenerator_SirenixBuildNameAttribute_get_BuildName_mF45EE0EB16B5E298B342C707C4BD5E00D24ECE8E,
	SirenixBuildNameAttribute_tD2F7F06A523D6D952301778DAEDEDFA8AFF30175_CustomAttributesCacheGenerator_SirenixBuildNameAttribute_set_BuildName_mA4D04890D1D89B974C22E1B19FD9268570B8E078,
	SirenixBuildVersionAttribute_tEC151875E0945B59C210D365DDA385F743A3C964_CustomAttributesCacheGenerator_SirenixBuildVersionAttribute_get_Version_m44D7DCC05E81DEC96439A765FAEC8DD2E883C28F,
	SirenixBuildVersionAttribute_tEC151875E0945B59C210D365DDA385F743A3C964_CustomAttributesCacheGenerator_SirenixBuildVersionAttribute_set_Version_mD6B19A36745950F52774F9EEF74E021C9E04249A,
	StringUtilities_tB788381F3751AF3EE17C33AD1078C828C8B8F958_CustomAttributesCacheGenerator_StringUtilities_FastEndsWith_mA9B2FEC333CBFAF6EFADAABC1E6E27E4B8134D68,
	UnityVersion_tE4A3A4D950BC8D7D4C4E368E8D7126E7D207E410_CustomAttributesCacheGenerator_UnityVersion_EnsureLoaded_m35CA01CA335803318DE771A95E8C214B3A841345,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_TryInferGenericParameters_m8B86E7145860BD05F5BA87566D034CAEF046E46F____knownParameters2,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m5979A23DB5D7BB878F43406689A1D1E222A84182____parameters1,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m68E1A8190F47993BDD581D414D13C509579F4F43____parameters1,
	TypeExtensions_t287F29AF4874293036EC1E4E1424A65DEFC6A6F3_CustomAttributesCacheGenerator_TypeExtensions_GenericArgumentsContainsTypes_mB5405BEBB46D9D14834B1FEFD389AAD72052552E____types1,
	GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02_CustomAttributesCacheGenerator_GlobalConfigAttribute_t8F6705E712F2B80F3B9DCC20DDCB583C3FF5FB02____FullPath_PropertyInfo,
	Sirenix_Utilities_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_allowMultiple_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_inherited_2(L_0);
		return;
	}
}
