﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436;
// System.Func`2<System.String,System.String>
struct Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_tBD60400523D840591A17E4CBBACC79397F68FAA2;
// System.Collections.Generic.IList`1<Sirenix.OdinInspector.PropertyGroupAttribute>
struct IList_1_t9B1A513FF1B4B4D2A5CA67E5C664DD49913ACFD3;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>
struct List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// Sirenix.OdinInspector.PropertyGroupAttribute[]
struct PropertyGroupAttributeU5BU5D_tA49CF528999CC76172E8A6BBB23AE96F6FB3035F;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00;
// System.ArgumentNullException
struct ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB;
// Sirenix.OdinInspector.AssetListAttribute
struct AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7;
// Sirenix.OdinInspector.AssetSelectorAttribute
struct AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622;
// Sirenix.OdinInspector.AssetsOnlyAttribute
struct AssetsOnlyAttribute_tCC6A1D3E6DBF4014A229FFADCF481275DC8CE8EE;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// Sirenix.OdinInspector.BoxGroupAttribute
struct BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C;
// Sirenix.OdinInspector.ButtonAttribute
struct ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782;
// Sirenix.OdinInspector.ButtonGroupAttribute
struct ButtonGroupAttribute_t20631D3282E17CD252851E3A40703BA4F5EB4A86;
// Sirenix.OdinInspector.ChildGameObjectsOnlyAttribute
struct ChildGameObjectsOnlyAttribute_t6CF8E838A1AE35CF0E136161BD89949F76E22BF3;
// Sirenix.OdinInspector.ColorPaletteAttribute
struct ColorPaletteAttribute_t89BD14DE6C9A17B37A64A8E188A49AB7621D759C;
// Sirenix.OdinInspector.CustomContextMenuAttribute
struct CustomContextMenuAttribute_tE9AFF948A16686A5D0C8831B5383270F7134AB49;
// Sirenix.OdinInspector.CustomValueDrawerAttribute
struct CustomValueDrawerAttribute_t7F42DE8AE9C58EB688259D42B09473C0DEE98A31;
// Sirenix.OdinInspector.DelayedPropertyAttribute
struct DelayedPropertyAttribute_t18B4307EE458D335803442517336AE1208186A88;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// Sirenix.OdinInspector.DetailedInfoBoxAttribute
struct DetailedInfoBoxAttribute_t8CEBCE93D61675CB38CA659984F4C121381C565E;
// Sirenix.OdinInspector.DictionaryDrawerSettings
struct DictionaryDrawerSettings_tD59605DC70F69FF09FC833CCC887105A966463F7;
// Sirenix.OdinInspector.DisableContextMenuAttribute
struct DisableContextMenuAttribute_t1EC42DF3D6D965B29C1578354756628012429BC8;
// Sirenix.OdinInspector.DisableIfAttribute
struct DisableIfAttribute_t4938E088E077FE7ABA2C514DBF3B3CF42BE41771;
// Sirenix.OdinInspector.DisableInEditorModeAttribute
struct DisableInEditorModeAttribute_t50BED704C92D71F81B99C7F72118468D918FF96D;
// Sirenix.OdinInspector.DisableInInlineEditorsAttribute
struct DisableInInlineEditorsAttribute_t2EB18DF22DC1A708520C4AD34151B95A7D570744;
// Sirenix.OdinInspector.DisableInNonPrefabsAttribute
struct DisableInNonPrefabsAttribute_tE0BD101B759B56A43490981CAC42FC73F4AB81CE;
// Sirenix.OdinInspector.DisableInPlayModeAttribute
struct DisableInPlayModeAttribute_t62870C62E5C7F02C470B68549E1323500520604C;
// Sirenix.OdinInspector.DisableInPrefabAssetsAttribute
struct DisableInPrefabAssetsAttribute_t5A0A0810282C07F9A30A66E2B829A080B9B97BC1;
// Sirenix.OdinInspector.DisableInPrefabInstancesAttribute
struct DisableInPrefabInstancesAttribute_t88BB1DCC0C1B7F899C6C07C65B1CA463D2506DDC;
// Sirenix.OdinInspector.DisableInPrefabsAttribute
struct DisableInPrefabsAttribute_t2612557CCF5BD1035F1E8990029DDD1A3D2A33F7;
// Sirenix.OdinInspector.DisplayAsStringAttribute
struct DisplayAsStringAttribute_tC0C89AF4B51CB834ED7AD9AB028D617487E7297F;
// Sirenix.OdinInspector.DoNotDrawAsReferenceAttribute
struct DoNotDrawAsReferenceAttribute_tB1F70750A3599CE5DFFF4136AAD2C64FB41A6121;
// Sirenix.OdinInspector.DontApplyToListElementsAttribute
struct DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143;
// Sirenix.OdinInspector.DrawWithUnityAttribute
struct DrawWithUnityAttribute_t22A1203943EDA03AB38CF29B28743CB94753A153;
// Sirenix.OdinInspector.EnableForPrefabOnlyAttribute
struct EnableForPrefabOnlyAttribute_t179CCCCF9D183ACF7AAAB884F147EEAB89BBFFDB;
// Sirenix.OdinInspector.EnableGUIAttribute
struct EnableGUIAttribute_tC43B92B9971628498DF2B4F91C1667F357265CB0;
// Sirenix.OdinInspector.EnableIfAttribute
struct EnableIfAttribute_t3B8036F797E23325323E2064C7CCEA71BAAF7BE9;
// Sirenix.OdinInspector.EnumPagingAttribute
struct EnumPagingAttribute_tF54A51875E6BA517B1D30C52BA323866A891FB1F;
// Sirenix.OdinInspector.EnumToggleButtonsAttribute
struct EnumToggleButtonsAttribute_tCF9348ACFED9D214C966105CBA027843D5ABE1C0;
// Sirenix.OdinInspector.FilePathAttribute
struct FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458;
// Sirenix.OdinInspector.FolderPathAttribute
struct FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389;
// Sirenix.OdinInspector.FoldoutGroupAttribute
struct FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D;
// Sirenix.OdinInspector.GUIColorAttribute
struct GUIColorAttribute_t7E1FEBC8732A8232FE82C68019C98709FD244DB4;
// Sirenix.OdinInspector.HideDuplicateReferenceBoxAttribute
struct HideDuplicateReferenceBoxAttribute_t23219A744C229500EA4BE2F8939515E05BA7A8D1;
// Sirenix.OdinInspector.HideIfAttribute
struct HideIfAttribute_t11B1419645C36889523DAB5156404E47D08167B0;
// Sirenix.OdinInspector.HideIfGroupAttribute
struct HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4;
// Sirenix.OdinInspector.HideInEditorModeAttribute
struct HideInEditorModeAttribute_tA80C5951E867A0D27A1C64176166E457B9C4C69F;
// Sirenix.OdinInspector.HideInInlineEditorsAttribute
struct HideInInlineEditorsAttribute_t30CAEEF70FDB341465674DA00FB39A43320F868B;
// Sirenix.OdinInspector.HideInNonPrefabsAttribute
struct HideInNonPrefabsAttribute_t0BF8A36CF5FAE4EF1217ED0B4F1285A3098A594B;
// Sirenix.OdinInspector.HideInPlayModeAttribute
struct HideInPlayModeAttribute_t5A71EEB1100BED2A35EF076EF6B99B628C7E32A7;
// Sirenix.OdinInspector.HideInPrefabAssetsAttribute
struct HideInPrefabAssetsAttribute_tB9D268E4438857EE5032A747B4629682092E69D6;
// Sirenix.OdinInspector.HideInPrefabInstancesAttribute
struct HideInPrefabInstancesAttribute_t439BFC26506199B446892DFB6C329D8DE37DA492;
// Sirenix.OdinInspector.HideInPrefabsAttribute
struct HideInPrefabsAttribute_tB539C9027320E92429C7F183A8D0B488213D7CF4;
// Sirenix.OdinInspector.HideInTablesAttribute
struct HideInTablesAttribute_t110DFA0CFAC26EBCE8B6C9086B6DC345A0D8B47B;
// Sirenix.OdinInspector.HideLabelAttribute
struct HideLabelAttribute_t8CFE493B307FE0E07EA75618A406700A44FD2B00;
// Sirenix.OdinInspector.HideMonoScriptAttribute
struct HideMonoScriptAttribute_tF0E9B439B2C4AF5DF40AA4408216484CBC611A18;
// Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute
struct HideNetworkBehaviourFieldsAttribute_tF2720B20E003A5A3947466638AC7081930321BD3;
// Sirenix.OdinInspector.HideReferenceObjectPickerAttribute
struct HideReferenceObjectPickerAttribute_t904F2AE6C1E46F3630F27B5717DAA2A078240400;
// Sirenix.OdinInspector.HorizontalGroupAttribute
struct HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// Sirenix.OdinInspector.IncludeMyAttributesAttribute
struct IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833;
// Sirenix.OdinInspector.IndentAttribute
struct IndentAttribute_tE8DFFDF7D9778532690A56BC8145CC86AA6C74EE;
// Sirenix.OdinInspector.InfoBoxAttribute
struct InfoBoxAttribute_t8D2A3D9022D85EC4424CAB78C71C24C8BF8AEFD5;
// Sirenix.OdinInspector.InlineButtonAttribute
struct InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18;
// Sirenix.OdinInspector.InlineEditorAttribute
struct InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76;
// Sirenix.OdinInspector.InlinePropertyAttribute
struct InlinePropertyAttribute_tC4F6EE65AF6B4B9F3D0995C379732DA61E459158;
// Sirenix.OdinInspector.LabelTextAttribute
struct LabelTextAttribute_tAC01C34A3856E3C62B39EA8A4412BFE522E35D27;
// Sirenix.OdinInspector.LabelWidthAttribute
struct LabelWidthAttribute_t2E4C87DC106B9C9E3D7E47E038D3161026726A8B;
// Sirenix.OdinInspector.ListDrawerSettingsAttribute
struct ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181;
// Sirenix.OdinInspector.MaxValueAttribute
struct MaxValueAttribute_tFC98EB9F62599D96C7A430775FE8543EC98309BC;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Sirenix.OdinInspector.MinMaxSliderAttribute
struct MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41;
// Sirenix.OdinInspector.MinValueAttribute
struct MinValueAttribute_t45E8A787D2858D90D37F7A87C0582C0BF131FA34;
// Sirenix.OdinInspector.MultiLinePropertyAttribute
struct MultiLinePropertyAttribute_tD35BC318DFDFAC8469DA2D276235CCF0E59B319D;
// System.NotImplementedException
struct NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6;
// Sirenix.OdinInspector.OdinRegisterAttributeAttribute
struct OdinRegisterAttributeAttribute_t8A82A1C5F996F02644E959C5EAD6533EBF268FFA;
// Sirenix.OdinInspector.OnInspectorGUIAttribute
struct OnInspectorGUIAttribute_t1CCCB842BCEBEB2AB07C1298B7FFBAB7571B0F2F;
// Sirenix.OdinInspector.OnValueChangedAttribute
struct OnValueChangedAttribute_tFE13CD46B1B5466F8AA8C351562C9D3BB9FB7623;
// Sirenix.OdinInspector.PreviewFieldAttribute
struct PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4;
// Sirenix.OdinInspector.ProgressBarAttribute
struct ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F;
// Sirenix.OdinInspector.PropertyGroupAttribute
struct PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1;
// Sirenix.OdinInspector.PropertyOrderAttribute
struct PropertyOrderAttribute_t2F83A60EEA2D6D2C91E10D2A83E22326F5F71936;
// Sirenix.OdinInspector.PropertyRangeAttribute
struct PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0;
// Sirenix.OdinInspector.PropertySpaceAttribute
struct PropertySpaceAttribute_tDF729E8EB760A22841021D50F961B59FAB282048;
// Sirenix.OdinInspector.PropertyTooltipAttribute
struct PropertyTooltipAttribute_t7BC0BF969C21340C1CA2B7180CFD04350B79204B;
// Sirenix.OdinInspector.ReadOnlyAttribute
struct ReadOnlyAttribute_t4A1BE30F9257C5E0187EFB8FC659A42F4B61472C;
// Sirenix.OdinInspector.RequiredAttribute
struct RequiredAttribute_t0941212FA1DC5F51F3309F0EB27016EE914366D4;
// Sirenix.OdinInspector.ResponsiveButtonGroupAttribute
struct ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// Sirenix.OdinInspector.SceneObjectsOnlyAttribute
struct SceneObjectsOnlyAttribute_t74D00B9886B272BE983F0AE1F447B56E222402A2;
// Sirenix.OdinInspector.ShowDrawerChainAttribute
struct ShowDrawerChainAttribute_tAF05CAAEC33491EA7281A449128A45877A66A7CC;
// Sirenix.OdinInspector.ShowForPrefabOnlyAttribute
struct ShowForPrefabOnlyAttribute_tF680D3BAFA9AF8C0C8BE3A0EC1DB9380BCB319D4;
// Sirenix.OdinInspector.ShowIfAttribute
struct ShowIfAttribute_tDF7F785B12F1489ED80C78FC2695E7363CC5B8AE;
// Sirenix.OdinInspector.ShowIfGroupAttribute
struct ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34;
// Sirenix.OdinInspector.ShowInInlineEditorsAttribute
struct ShowInInlineEditorsAttribute_t074163930680127467E651ECE68C8A44528E93D2;
// Sirenix.OdinInspector.ShowInInspectorAttribute
struct ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41;
// Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute
struct ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D;
// Sirenix.OdinInspector.ShowPropertyResolverAttribute
struct ShowPropertyResolverAttribute_t38E4D018FDE433172E3184B2B8BB77CF841B8A9E;
// System.String
struct String_t;
// Sirenix.OdinInspector.SuffixLabelAttribute
struct SuffixLabelAttribute_t990C9A8E87740E342FB14097884866F5D3A9869C;
// Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute
struct SuppressInvalidAttributeErrorAttribute_t8CCDC91E2DECC329BCDF59102809D220CCF7794E;
// Sirenix.OdinInspector.TabGroupAttribute
struct TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D;
// Sirenix.OdinInspector.TableColumnWidthAttribute
struct TableColumnWidthAttribute_t62FFBE33C21C9BCA7419C73D67C20BBF291671BF;
// Sirenix.OdinInspector.TableListAttribute
struct TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9;
// Sirenix.OdinInspector.TableMatrixAttribute
struct TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E;
// Sirenix.OdinInspector.TitleAttribute
struct TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44;
// Sirenix.OdinInspector.TitleGroupAttribute
struct TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED;
// Sirenix.OdinInspector.ToggleAttribute
struct ToggleAttribute_tE3BA633AE4C4A847529A9FEB83DFF3C17CB83F2B;
// Sirenix.OdinInspector.ToggleGroupAttribute
struct ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00;
// Sirenix.OdinInspector.ToggleLeftAttribute
struct ToggleLeftAttribute_t4CC8B95C528CF91FEA46AABBC6237B2258FD77C9;
// System.Type
struct Type_t;
// Sirenix.OdinInspector.TypeFilterAttribute
struct TypeFilterAttribute_t7989849079B6440606CBDA93E5AE67B4877A56FE;
// Sirenix.OdinInspector.TypeInfoBoxAttribute
struct TypeInfoBoxAttribute_t43B3E84724467282A1E3696E35325EF8C66E4BBD;
// Sirenix.OdinInspector.ValidateInputAttribute
struct ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE;
// Sirenix.OdinInspector.ValueDropdownAttribute
struct ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671;
// Sirenix.OdinInspector.VerticalGroupAttribute
struct VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Sirenix.OdinInspector.WrapAttribute
struct WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930;
// Sirenix.OdinInspector.AssetSelectorAttribute/<>c
struct U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6;
// Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute
struct TabSubGroupAttribute_tDEC43BA5824DF3FC46C494AC92DD32CD26D8B22C;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TabSubGroupAttribute_tDEC43BA5824DF3FC46C494AC92DD32CD26D8B22C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0B13B52780344FB2960F902E01BC99A1B7E5A4B5;
IL2CPP_EXTERN_C String_t* _stringLiteral43E3531262B01D6352A62A076F3CA58EB20D257F;
IL2CPP_EXTERN_C String_t* _stringLiteral5BEFD8CC60A79699B5BB00E37BAC5B62D371E174;
IL2CPP_EXTERN_C String_t* _stringLiteral6E9E619437A54F36D60249F4E555066130AFDC3C;
IL2CPP_EXTERN_C String_t* _stringLiteral7F87C5EE8E2AF3096FEB1F359E7640DC822F9ACD;
IL2CPP_EXTERN_C String_t* _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1;
IL2CPP_EXTERN_C String_t* _stringLiteral86FADB31129B6F40C720A97600D69389EA3567E3;
IL2CPP_EXTERN_C String_t* _stringLiteralA46F01FBD653440FE1C9E92E2A2D40AAB3645653;
IL2CPP_EXTERN_C String_t* _stringLiteralB720A9AE58815DFF5576319E5228D318E7899C07;
IL2CPP_EXTERN_C String_t* _stringLiteralC18C9BB6DF0D5C60CE5A5D2D3D6111BEB6F8CCEB;
IL2CPP_EXTERN_C String_t* _stringLiteralD13C29EFE248E598FB27D17F13DBD3AF1D765FB8;
IL2CPP_EXTERN_C String_t* _stringLiteralF7933083B6BA56CBC6D7BCA0F30688A30D0368F6;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisString_t_TisString_t_m5E54BE921E960B9AB33FE013CF7ECB8D1CF24A1A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisString_t_mE824E1F8EB2A50DC8E24291957CBEED8C356E582_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m22403E6E9EC24A3D8103D29D9D66B5EEEA0AC69E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InlineEditorAttribute__ctor_mA7C55B884AE2BFFA8538A0E8676FB42C0686A34B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mAE53CD7A7307ABC7A7AC23A32795DD076B4C0CB5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m2EAD2DADA0478175052301E48FCE772ECD9A6F5F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m468E89F534D7F4463B96A099275295DF689B2323_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mAB3E3E1DD0571D6A301A9D1DF30EC3C8C49766AD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyGroupAttribute_Combine_m428F9CF2608153498BE167A9BDE7F139668B237F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3Cset_PathsU3Eb__12_0_mB1598E3D172430978C835A9B4E1A1268891DF76D_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tAF2E653BDCC26D3871D6045939A08474474C4E5D 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>
struct  List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PropertyGroupAttributeU5BU5D_tA49CF528999CC76172E8A6BBB23AE96F6FB3035F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D, ____items_1)); }
	inline PropertyGroupAttributeU5BU5D_tA49CF528999CC76172E8A6BBB23AE96F6FB3035F* get__items_1() const { return ____items_1; }
	inline PropertyGroupAttributeU5BU5D_tA49CF528999CC76172E8A6BBB23AE96F6FB3035F** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PropertyGroupAttributeU5BU5D_tA49CF528999CC76172E8A6BBB23AE96F6FB3035F* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	PropertyGroupAttributeU5BU5D_tA49CF528999CC76172E8A6BBB23AE96F6FB3035F* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D_StaticFields, ____emptyArray_5)); }
	inline PropertyGroupAttributeU5BU5D_tA49CF528999CC76172E8A6BBB23AE96F6FB3035F* get__emptyArray_5() const { return ____emptyArray_5; }
	inline PropertyGroupAttributeU5BU5D_tA49CF528999CC76172E8A6BBB23AE96F6FB3035F** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(PropertyGroupAttributeU5BU5D_tA49CF528999CC76172E8A6BBB23AE96F6FB3035F* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct  List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____items_1)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Sirenix.OdinInspector.AssetSelectorAttribute/<>c
struct  U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_StaticFields
{
public:
	// Sirenix.OdinInspector.AssetSelectorAttribute/<>c Sirenix.OdinInspector.AssetSelectorAttribute/<>c::<>9
	U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> Sirenix.OdinInspector.AssetSelectorAttribute/<>c::<>9__12_0
	Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * ___U3CU3E9__12_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__12_0_1), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.String>
struct  Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___list_0)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_list_0() const { return ___list_0; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// Sirenix.OdinInspector.AssetListAttribute
struct  AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean Sirenix.OdinInspector.AssetListAttribute::AutoPopulate
	bool ___AutoPopulate_0;
	// System.String Sirenix.OdinInspector.AssetListAttribute::Tags
	String_t* ___Tags_1;
	// System.String Sirenix.OdinInspector.AssetListAttribute::LayerNames
	String_t* ___LayerNames_2;
	// System.String Sirenix.OdinInspector.AssetListAttribute::AssetNamePrefix
	String_t* ___AssetNamePrefix_3;
	// System.String Sirenix.OdinInspector.AssetListAttribute::Path
	String_t* ___Path_4;
	// System.String Sirenix.OdinInspector.AssetListAttribute::CustomFilterMethod
	String_t* ___CustomFilterMethod_5;

public:
	inline static int32_t get_offset_of_AutoPopulate_0() { return static_cast<int32_t>(offsetof(AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7, ___AutoPopulate_0)); }
	inline bool get_AutoPopulate_0() const { return ___AutoPopulate_0; }
	inline bool* get_address_of_AutoPopulate_0() { return &___AutoPopulate_0; }
	inline void set_AutoPopulate_0(bool value)
	{
		___AutoPopulate_0 = value;
	}

	inline static int32_t get_offset_of_Tags_1() { return static_cast<int32_t>(offsetof(AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7, ___Tags_1)); }
	inline String_t* get_Tags_1() const { return ___Tags_1; }
	inline String_t** get_address_of_Tags_1() { return &___Tags_1; }
	inline void set_Tags_1(String_t* value)
	{
		___Tags_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Tags_1), (void*)value);
	}

	inline static int32_t get_offset_of_LayerNames_2() { return static_cast<int32_t>(offsetof(AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7, ___LayerNames_2)); }
	inline String_t* get_LayerNames_2() const { return ___LayerNames_2; }
	inline String_t** get_address_of_LayerNames_2() { return &___LayerNames_2; }
	inline void set_LayerNames_2(String_t* value)
	{
		___LayerNames_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LayerNames_2), (void*)value);
	}

	inline static int32_t get_offset_of_AssetNamePrefix_3() { return static_cast<int32_t>(offsetof(AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7, ___AssetNamePrefix_3)); }
	inline String_t* get_AssetNamePrefix_3() const { return ___AssetNamePrefix_3; }
	inline String_t** get_address_of_AssetNamePrefix_3() { return &___AssetNamePrefix_3; }
	inline void set_AssetNamePrefix_3(String_t* value)
	{
		___AssetNamePrefix_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AssetNamePrefix_3), (void*)value);
	}

	inline static int32_t get_offset_of_Path_4() { return static_cast<int32_t>(offsetof(AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7, ___Path_4)); }
	inline String_t* get_Path_4() const { return ___Path_4; }
	inline String_t** get_address_of_Path_4() { return &___Path_4; }
	inline void set_Path_4(String_t* value)
	{
		___Path_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Path_4), (void*)value);
	}

	inline static int32_t get_offset_of_CustomFilterMethod_5() { return static_cast<int32_t>(offsetof(AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7, ___CustomFilterMethod_5)); }
	inline String_t* get_CustomFilterMethod_5() const { return ___CustomFilterMethod_5; }
	inline String_t** get_address_of_CustomFilterMethod_5() { return &___CustomFilterMethod_5; }
	inline void set_CustomFilterMethod_5(String_t* value)
	{
		___CustomFilterMethod_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CustomFilterMethod_5), (void*)value);
	}
};


// Sirenix.OdinInspector.AssetSelectorAttribute
struct  AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::IsUniqueList
	bool ___IsUniqueList_0;
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::DrawDropdownForListElements
	bool ___DrawDropdownForListElements_1;
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::DisableListAddButtonBehaviour
	bool ___DisableListAddButtonBehaviour_2;
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::ExcludeExistingValuesInList
	bool ___ExcludeExistingValuesInList_3;
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::ExpandAllMenuItems
	bool ___ExpandAllMenuItems_4;
	// System.Boolean Sirenix.OdinInspector.AssetSelectorAttribute::FlattenTreeView
	bool ___FlattenTreeView_5;
	// System.Int32 Sirenix.OdinInspector.AssetSelectorAttribute::DropdownWidth
	int32_t ___DropdownWidth_6;
	// System.Int32 Sirenix.OdinInspector.AssetSelectorAttribute::DropdownHeight
	int32_t ___DropdownHeight_7;
	// System.String Sirenix.OdinInspector.AssetSelectorAttribute::DropdownTitle
	String_t* ___DropdownTitle_8;
	// System.String[] Sirenix.OdinInspector.AssetSelectorAttribute::SearchInFolders
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___SearchInFolders_9;
	// System.String Sirenix.OdinInspector.AssetSelectorAttribute::Filter
	String_t* ___Filter_10;

public:
	inline static int32_t get_offset_of_IsUniqueList_0() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___IsUniqueList_0)); }
	inline bool get_IsUniqueList_0() const { return ___IsUniqueList_0; }
	inline bool* get_address_of_IsUniqueList_0() { return &___IsUniqueList_0; }
	inline void set_IsUniqueList_0(bool value)
	{
		___IsUniqueList_0 = value;
	}

	inline static int32_t get_offset_of_DrawDropdownForListElements_1() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___DrawDropdownForListElements_1)); }
	inline bool get_DrawDropdownForListElements_1() const { return ___DrawDropdownForListElements_1; }
	inline bool* get_address_of_DrawDropdownForListElements_1() { return &___DrawDropdownForListElements_1; }
	inline void set_DrawDropdownForListElements_1(bool value)
	{
		___DrawDropdownForListElements_1 = value;
	}

	inline static int32_t get_offset_of_DisableListAddButtonBehaviour_2() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___DisableListAddButtonBehaviour_2)); }
	inline bool get_DisableListAddButtonBehaviour_2() const { return ___DisableListAddButtonBehaviour_2; }
	inline bool* get_address_of_DisableListAddButtonBehaviour_2() { return &___DisableListAddButtonBehaviour_2; }
	inline void set_DisableListAddButtonBehaviour_2(bool value)
	{
		___DisableListAddButtonBehaviour_2 = value;
	}

	inline static int32_t get_offset_of_ExcludeExistingValuesInList_3() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___ExcludeExistingValuesInList_3)); }
	inline bool get_ExcludeExistingValuesInList_3() const { return ___ExcludeExistingValuesInList_3; }
	inline bool* get_address_of_ExcludeExistingValuesInList_3() { return &___ExcludeExistingValuesInList_3; }
	inline void set_ExcludeExistingValuesInList_3(bool value)
	{
		___ExcludeExistingValuesInList_3 = value;
	}

	inline static int32_t get_offset_of_ExpandAllMenuItems_4() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___ExpandAllMenuItems_4)); }
	inline bool get_ExpandAllMenuItems_4() const { return ___ExpandAllMenuItems_4; }
	inline bool* get_address_of_ExpandAllMenuItems_4() { return &___ExpandAllMenuItems_4; }
	inline void set_ExpandAllMenuItems_4(bool value)
	{
		___ExpandAllMenuItems_4 = value;
	}

	inline static int32_t get_offset_of_FlattenTreeView_5() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___FlattenTreeView_5)); }
	inline bool get_FlattenTreeView_5() const { return ___FlattenTreeView_5; }
	inline bool* get_address_of_FlattenTreeView_5() { return &___FlattenTreeView_5; }
	inline void set_FlattenTreeView_5(bool value)
	{
		___FlattenTreeView_5 = value;
	}

	inline static int32_t get_offset_of_DropdownWidth_6() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___DropdownWidth_6)); }
	inline int32_t get_DropdownWidth_6() const { return ___DropdownWidth_6; }
	inline int32_t* get_address_of_DropdownWidth_6() { return &___DropdownWidth_6; }
	inline void set_DropdownWidth_6(int32_t value)
	{
		___DropdownWidth_6 = value;
	}

	inline static int32_t get_offset_of_DropdownHeight_7() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___DropdownHeight_7)); }
	inline int32_t get_DropdownHeight_7() const { return ___DropdownHeight_7; }
	inline int32_t* get_address_of_DropdownHeight_7() { return &___DropdownHeight_7; }
	inline void set_DropdownHeight_7(int32_t value)
	{
		___DropdownHeight_7 = value;
	}

	inline static int32_t get_offset_of_DropdownTitle_8() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___DropdownTitle_8)); }
	inline String_t* get_DropdownTitle_8() const { return ___DropdownTitle_8; }
	inline String_t** get_address_of_DropdownTitle_8() { return &___DropdownTitle_8; }
	inline void set_DropdownTitle_8(String_t* value)
	{
		___DropdownTitle_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DropdownTitle_8), (void*)value);
	}

	inline static int32_t get_offset_of_SearchInFolders_9() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___SearchInFolders_9)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_SearchInFolders_9() const { return ___SearchInFolders_9; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_SearchInFolders_9() { return &___SearchInFolders_9; }
	inline void set_SearchInFolders_9(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___SearchInFolders_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SearchInFolders_9), (void*)value);
	}

	inline static int32_t get_offset_of_Filter_10() { return static_cast<int32_t>(offsetof(AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622, ___Filter_10)); }
	inline String_t* get_Filter_10() const { return ___Filter_10; }
	inline String_t** get_address_of_Filter_10() { return &___Filter_10; }
	inline void set_Filter_10(String_t* value)
	{
		___Filter_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Filter_10), (void*)value);
	}
};


// Sirenix.OdinInspector.AssetsOnlyAttribute
struct  AssetsOnlyAttribute_tCC6A1D3E6DBF4014A229FFADCF481275DC8CE8EE  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct  Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// Sirenix.OdinInspector.ChildGameObjectsOnlyAttribute
struct  ChildGameObjectsOnlyAttribute_t6CF8E838A1AE35CF0E136161BD89949F76E22BF3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean Sirenix.OdinInspector.ChildGameObjectsOnlyAttribute::IncludeSelf
	bool ___IncludeSelf_0;

public:
	inline static int32_t get_offset_of_IncludeSelf_0() { return static_cast<int32_t>(offsetof(ChildGameObjectsOnlyAttribute_t6CF8E838A1AE35CF0E136161BD89949F76E22BF3, ___IncludeSelf_0)); }
	inline bool get_IncludeSelf_0() const { return ___IncludeSelf_0; }
	inline bool* get_address_of_IncludeSelf_0() { return &___IncludeSelf_0; }
	inline void set_IncludeSelf_0(bool value)
	{
		___IncludeSelf_0 = value;
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// Sirenix.OdinInspector.ColorPaletteAttribute
struct  ColorPaletteAttribute_t89BD14DE6C9A17B37A64A8E188A49AB7621D759C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.ColorPaletteAttribute::PaletteName
	String_t* ___PaletteName_0;
	// System.Boolean Sirenix.OdinInspector.ColorPaletteAttribute::ShowAlpha
	bool ___ShowAlpha_1;

public:
	inline static int32_t get_offset_of_PaletteName_0() { return static_cast<int32_t>(offsetof(ColorPaletteAttribute_t89BD14DE6C9A17B37A64A8E188A49AB7621D759C, ___PaletteName_0)); }
	inline String_t* get_PaletteName_0() const { return ___PaletteName_0; }
	inline String_t** get_address_of_PaletteName_0() { return &___PaletteName_0; }
	inline void set_PaletteName_0(String_t* value)
	{
		___PaletteName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PaletteName_0), (void*)value);
	}

	inline static int32_t get_offset_of_ShowAlpha_1() { return static_cast<int32_t>(offsetof(ColorPaletteAttribute_t89BD14DE6C9A17B37A64A8E188A49AB7621D759C, ___ShowAlpha_1)); }
	inline bool get_ShowAlpha_1() const { return ___ShowAlpha_1; }
	inline bool* get_address_of_ShowAlpha_1() { return &___ShowAlpha_1; }
	inline void set_ShowAlpha_1(bool value)
	{
		___ShowAlpha_1 = value;
	}
};


// Sirenix.OdinInspector.CustomContextMenuAttribute
struct  CustomContextMenuAttribute_tE9AFF948A16686A5D0C8831B5383270F7134AB49  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.CustomContextMenuAttribute::MenuItem
	String_t* ___MenuItem_0;
	// System.String Sirenix.OdinInspector.CustomContextMenuAttribute::MethodName
	String_t* ___MethodName_1;

public:
	inline static int32_t get_offset_of_MenuItem_0() { return static_cast<int32_t>(offsetof(CustomContextMenuAttribute_tE9AFF948A16686A5D0C8831B5383270F7134AB49, ___MenuItem_0)); }
	inline String_t* get_MenuItem_0() const { return ___MenuItem_0; }
	inline String_t** get_address_of_MenuItem_0() { return &___MenuItem_0; }
	inline void set_MenuItem_0(String_t* value)
	{
		___MenuItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MenuItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_MethodName_1() { return static_cast<int32_t>(offsetof(CustomContextMenuAttribute_tE9AFF948A16686A5D0C8831B5383270F7134AB49, ___MethodName_1)); }
	inline String_t* get_MethodName_1() const { return ___MethodName_1; }
	inline String_t** get_address_of_MethodName_1() { return &___MethodName_1; }
	inline void set_MethodName_1(String_t* value)
	{
		___MethodName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MethodName_1), (void*)value);
	}
};


// Sirenix.OdinInspector.CustomValueDrawerAttribute
struct  CustomValueDrawerAttribute_t7F42DE8AE9C58EB688259D42B09473C0DEE98A31  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.CustomValueDrawerAttribute::MethodName
	String_t* ___MethodName_0;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(CustomValueDrawerAttribute_t7F42DE8AE9C58EB688259D42B09473C0DEE98A31, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MethodName_0), (void*)value);
	}
};


// Sirenix.OdinInspector.DelayedPropertyAttribute
struct  DelayedPropertyAttribute_t18B4307EE458D335803442517336AE1208186A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.DisableContextMenuAttribute
struct  DisableContextMenuAttribute_t1EC42DF3D6D965B29C1578354756628012429BC8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean Sirenix.OdinInspector.DisableContextMenuAttribute::DisableForMember
	bool ___DisableForMember_0;
	// System.Boolean Sirenix.OdinInspector.DisableContextMenuAttribute::DisableForCollectionElements
	bool ___DisableForCollectionElements_1;

public:
	inline static int32_t get_offset_of_DisableForMember_0() { return static_cast<int32_t>(offsetof(DisableContextMenuAttribute_t1EC42DF3D6D965B29C1578354756628012429BC8, ___DisableForMember_0)); }
	inline bool get_DisableForMember_0() const { return ___DisableForMember_0; }
	inline bool* get_address_of_DisableForMember_0() { return &___DisableForMember_0; }
	inline void set_DisableForMember_0(bool value)
	{
		___DisableForMember_0 = value;
	}

	inline static int32_t get_offset_of_DisableForCollectionElements_1() { return static_cast<int32_t>(offsetof(DisableContextMenuAttribute_t1EC42DF3D6D965B29C1578354756628012429BC8, ___DisableForCollectionElements_1)); }
	inline bool get_DisableForCollectionElements_1() const { return ___DisableForCollectionElements_1; }
	inline bool* get_address_of_DisableForCollectionElements_1() { return &___DisableForCollectionElements_1; }
	inline void set_DisableForCollectionElements_1(bool value)
	{
		___DisableForCollectionElements_1 = value;
	}
};


// Sirenix.OdinInspector.DisableIfAttribute
struct  DisableIfAttribute_t4938E088E077FE7ABA2C514DBF3B3CF42BE41771  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.DisableIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Object Sirenix.OdinInspector.DisableIfAttribute::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(DisableIfAttribute_t4938E088E077FE7ABA2C514DBF3B3CF42BE41771, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberName_0), (void*)value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(DisableIfAttribute_t4938E088E077FE7ABA2C514DBF3B3CF42BE41771, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_1), (void*)value);
	}
};


// Sirenix.OdinInspector.DisableInEditorModeAttribute
struct  DisableInEditorModeAttribute_t50BED704C92D71F81B99C7F72118468D918FF96D  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.DisableInInlineEditorsAttribute
struct  DisableInInlineEditorsAttribute_t2EB18DF22DC1A708520C4AD34151B95A7D570744  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.DisableInNonPrefabsAttribute
struct  DisableInNonPrefabsAttribute_tE0BD101B759B56A43490981CAC42FC73F4AB81CE  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.DisableInPlayModeAttribute
struct  DisableInPlayModeAttribute_t62870C62E5C7F02C470B68549E1323500520604C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.DisableInPrefabAssetsAttribute
struct  DisableInPrefabAssetsAttribute_t5A0A0810282C07F9A30A66E2B829A080B9B97BC1  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.DisableInPrefabInstancesAttribute
struct  DisableInPrefabInstancesAttribute_t88BB1DCC0C1B7F899C6C07C65B1CA463D2506DDC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.DisableInPrefabsAttribute
struct  DisableInPrefabsAttribute_t2612557CCF5BD1035F1E8990029DDD1A3D2A33F7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.DisplayAsStringAttribute
struct  DisplayAsStringAttribute_tC0C89AF4B51CB834ED7AD9AB028D617487E7297F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean Sirenix.OdinInspector.DisplayAsStringAttribute::Overflow
	bool ___Overflow_0;

public:
	inline static int32_t get_offset_of_Overflow_0() { return static_cast<int32_t>(offsetof(DisplayAsStringAttribute_tC0C89AF4B51CB834ED7AD9AB028D617487E7297F, ___Overflow_0)); }
	inline bool get_Overflow_0() const { return ___Overflow_0; }
	inline bool* get_address_of_Overflow_0() { return &___Overflow_0; }
	inline void set_Overflow_0(bool value)
	{
		___Overflow_0 = value;
	}
};


// Sirenix.OdinInspector.DoNotDrawAsReferenceAttribute
struct  DoNotDrawAsReferenceAttribute_tB1F70750A3599CE5DFFF4136AAD2C64FB41A6121  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.DontApplyToListElementsAttribute
struct  DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Double
struct  Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// Sirenix.OdinInspector.DrawWithUnityAttribute
struct  DrawWithUnityAttribute_t22A1203943EDA03AB38CF29B28743CB94753A153  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.EnableForPrefabOnlyAttribute
struct  EnableForPrefabOnlyAttribute_t179CCCCF9D183ACF7AAAB884F147EEAB89BBFFDB  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.EnableGUIAttribute
struct  EnableGUIAttribute_tC43B92B9971628498DF2B4F91C1667F357265CB0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.EnableIfAttribute
struct  EnableIfAttribute_t3B8036F797E23325323E2064C7CCEA71BAAF7BE9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.EnableIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Object Sirenix.OdinInspector.EnableIfAttribute::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(EnableIfAttribute_t3B8036F797E23325323E2064C7CCEA71BAAF7BE9, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberName_0), (void*)value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(EnableIfAttribute_t3B8036F797E23325323E2064C7CCEA71BAAF7BE9, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_1), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// Sirenix.OdinInspector.EnumPagingAttribute
struct  EnumPagingAttribute_tF54A51875E6BA517B1D30C52BA323866A891FB1F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.EnumToggleButtonsAttribute
struct  EnumToggleButtonsAttribute_tCF9348ACFED9D214C966105CBA027843D5ABE1C0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.FilePathAttribute
struct  FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::AbsolutePath
	bool ___AbsolutePath_0;
	// System.String Sirenix.OdinInspector.FilePathAttribute::Extensions
	String_t* ___Extensions_1;
	// System.String Sirenix.OdinInspector.FilePathAttribute::ParentFolder
	String_t* ___ParentFolder_2;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::RequireValidPath
	bool ___RequireValidPath_3;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::RequireExistingPath
	bool ___RequireExistingPath_4;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::UseBackslashes
	bool ___UseBackslashes_5;
	// System.Boolean Sirenix.OdinInspector.FilePathAttribute::<ReadOnly>k__BackingField
	bool ___U3CReadOnlyU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_AbsolutePath_0() { return static_cast<int32_t>(offsetof(FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458, ___AbsolutePath_0)); }
	inline bool get_AbsolutePath_0() const { return ___AbsolutePath_0; }
	inline bool* get_address_of_AbsolutePath_0() { return &___AbsolutePath_0; }
	inline void set_AbsolutePath_0(bool value)
	{
		___AbsolutePath_0 = value;
	}

	inline static int32_t get_offset_of_Extensions_1() { return static_cast<int32_t>(offsetof(FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458, ___Extensions_1)); }
	inline String_t* get_Extensions_1() const { return ___Extensions_1; }
	inline String_t** get_address_of_Extensions_1() { return &___Extensions_1; }
	inline void set_Extensions_1(String_t* value)
	{
		___Extensions_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Extensions_1), (void*)value);
	}

	inline static int32_t get_offset_of_ParentFolder_2() { return static_cast<int32_t>(offsetof(FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458, ___ParentFolder_2)); }
	inline String_t* get_ParentFolder_2() const { return ___ParentFolder_2; }
	inline String_t** get_address_of_ParentFolder_2() { return &___ParentFolder_2; }
	inline void set_ParentFolder_2(String_t* value)
	{
		___ParentFolder_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ParentFolder_2), (void*)value);
	}

	inline static int32_t get_offset_of_RequireValidPath_3() { return static_cast<int32_t>(offsetof(FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458, ___RequireValidPath_3)); }
	inline bool get_RequireValidPath_3() const { return ___RequireValidPath_3; }
	inline bool* get_address_of_RequireValidPath_3() { return &___RequireValidPath_3; }
	inline void set_RequireValidPath_3(bool value)
	{
		___RequireValidPath_3 = value;
	}

	inline static int32_t get_offset_of_RequireExistingPath_4() { return static_cast<int32_t>(offsetof(FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458, ___RequireExistingPath_4)); }
	inline bool get_RequireExistingPath_4() const { return ___RequireExistingPath_4; }
	inline bool* get_address_of_RequireExistingPath_4() { return &___RequireExistingPath_4; }
	inline void set_RequireExistingPath_4(bool value)
	{
		___RequireExistingPath_4 = value;
	}

	inline static int32_t get_offset_of_UseBackslashes_5() { return static_cast<int32_t>(offsetof(FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458, ___UseBackslashes_5)); }
	inline bool get_UseBackslashes_5() const { return ___UseBackslashes_5; }
	inline bool* get_address_of_UseBackslashes_5() { return &___UseBackslashes_5; }
	inline void set_UseBackslashes_5(bool value)
	{
		___UseBackslashes_5 = value;
	}

	inline static int32_t get_offset_of_U3CReadOnlyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458, ___U3CReadOnlyU3Ek__BackingField_6)); }
	inline bool get_U3CReadOnlyU3Ek__BackingField_6() const { return ___U3CReadOnlyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CReadOnlyU3Ek__BackingField_6() { return &___U3CReadOnlyU3Ek__BackingField_6; }
	inline void set_U3CReadOnlyU3Ek__BackingField_6(bool value)
	{
		___U3CReadOnlyU3Ek__BackingField_6 = value;
	}
};


// Sirenix.OdinInspector.FolderPathAttribute
struct  FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::AbsolutePath
	bool ___AbsolutePath_0;
	// System.String Sirenix.OdinInspector.FolderPathAttribute::ParentFolder
	String_t* ___ParentFolder_1;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::RequireValidPath
	bool ___RequireValidPath_2;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::RequireExistingPath
	bool ___RequireExistingPath_3;
	// System.Boolean Sirenix.OdinInspector.FolderPathAttribute::UseBackslashes
	bool ___UseBackslashes_4;

public:
	inline static int32_t get_offset_of_AbsolutePath_0() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389, ___AbsolutePath_0)); }
	inline bool get_AbsolutePath_0() const { return ___AbsolutePath_0; }
	inline bool* get_address_of_AbsolutePath_0() { return &___AbsolutePath_0; }
	inline void set_AbsolutePath_0(bool value)
	{
		___AbsolutePath_0 = value;
	}

	inline static int32_t get_offset_of_ParentFolder_1() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389, ___ParentFolder_1)); }
	inline String_t* get_ParentFolder_1() const { return ___ParentFolder_1; }
	inline String_t** get_address_of_ParentFolder_1() { return &___ParentFolder_1; }
	inline void set_ParentFolder_1(String_t* value)
	{
		___ParentFolder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ParentFolder_1), (void*)value);
	}

	inline static int32_t get_offset_of_RequireValidPath_2() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389, ___RequireValidPath_2)); }
	inline bool get_RequireValidPath_2() const { return ___RequireValidPath_2; }
	inline bool* get_address_of_RequireValidPath_2() { return &___RequireValidPath_2; }
	inline void set_RequireValidPath_2(bool value)
	{
		___RequireValidPath_2 = value;
	}

	inline static int32_t get_offset_of_RequireExistingPath_3() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389, ___RequireExistingPath_3)); }
	inline bool get_RequireExistingPath_3() const { return ___RequireExistingPath_3; }
	inline bool* get_address_of_RequireExistingPath_3() { return &___RequireExistingPath_3; }
	inline void set_RequireExistingPath_3(bool value)
	{
		___RequireExistingPath_3 = value;
	}

	inline static int32_t get_offset_of_UseBackslashes_4() { return static_cast<int32_t>(offsetof(FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389, ___UseBackslashes_4)); }
	inline bool get_UseBackslashes_4() const { return ___UseBackslashes_4; }
	inline bool* get_address_of_UseBackslashes_4() { return &___UseBackslashes_4; }
	inline void set_UseBackslashes_4(bool value)
	{
		___UseBackslashes_4 = value;
	}
};


// Sirenix.OdinInspector.HideDuplicateReferenceBoxAttribute
struct  HideDuplicateReferenceBoxAttribute_t23219A744C229500EA4BE2F8939515E05BA7A8D1  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideIfAttribute
struct  HideIfAttribute_t11B1419645C36889523DAB5156404E47D08167B0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.HideIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Object Sirenix.OdinInspector.HideIfAttribute::Value
	RuntimeObject * ___Value_1;
	// System.Boolean Sirenix.OdinInspector.HideIfAttribute::Animate
	bool ___Animate_2;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(HideIfAttribute_t11B1419645C36889523DAB5156404E47D08167B0, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberName_0), (void*)value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(HideIfAttribute_t11B1419645C36889523DAB5156404E47D08167B0, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_1), (void*)value);
	}

	inline static int32_t get_offset_of_Animate_2() { return static_cast<int32_t>(offsetof(HideIfAttribute_t11B1419645C36889523DAB5156404E47D08167B0, ___Animate_2)); }
	inline bool get_Animate_2() const { return ___Animate_2; }
	inline bool* get_address_of_Animate_2() { return &___Animate_2; }
	inline void set_Animate_2(bool value)
	{
		___Animate_2 = value;
	}
};


// Sirenix.OdinInspector.HideInEditorModeAttribute
struct  HideInEditorModeAttribute_tA80C5951E867A0D27A1C64176166E457B9C4C69F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideInInlineEditorsAttribute
struct  HideInInlineEditorsAttribute_t30CAEEF70FDB341465674DA00FB39A43320F868B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideInNonPrefabsAttribute
struct  HideInNonPrefabsAttribute_t0BF8A36CF5FAE4EF1217ED0B4F1285A3098A594B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideInPlayModeAttribute
struct  HideInPlayModeAttribute_t5A71EEB1100BED2A35EF076EF6B99B628C7E32A7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideInPrefabAssetsAttribute
struct  HideInPrefabAssetsAttribute_tB9D268E4438857EE5032A747B4629682092E69D6  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideInPrefabInstancesAttribute
struct  HideInPrefabInstancesAttribute_t439BFC26506199B446892DFB6C329D8DE37DA492  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideInPrefabsAttribute
struct  HideInPrefabsAttribute_tB539C9027320E92429C7F183A8D0B488213D7CF4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideInTablesAttribute
struct  HideInTablesAttribute_t110DFA0CFAC26EBCE8B6C9086B6DC345A0D8B47B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideLabelAttribute
struct  HideLabelAttribute_t8CFE493B307FE0E07EA75618A406700A44FD2B00  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideMonoScriptAttribute
struct  HideMonoScriptAttribute_tF0E9B439B2C4AF5DF40AA4408216484CBC611A18  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute
struct  HideNetworkBehaviourFieldsAttribute_tF2720B20E003A5A3947466638AC7081930321BD3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.HideReferenceObjectPickerAttribute
struct  HideReferenceObjectPickerAttribute_t904F2AE6C1E46F3630F27B5717DAA2A078240400  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.IncludeMyAttributesAttribute
struct  IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.IndentAttribute
struct  IndentAttribute_tE8DFFDF7D9778532690A56BC8145CC86AA6C74EE  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 Sirenix.OdinInspector.IndentAttribute::IndentLevel
	int32_t ___IndentLevel_0;

public:
	inline static int32_t get_offset_of_IndentLevel_0() { return static_cast<int32_t>(offsetof(IndentAttribute_tE8DFFDF7D9778532690A56BC8145CC86AA6C74EE, ___IndentLevel_0)); }
	inline int32_t get_IndentLevel_0() const { return ___IndentLevel_0; }
	inline int32_t* get_address_of_IndentLevel_0() { return &___IndentLevel_0; }
	inline void set_IndentLevel_0(int32_t value)
	{
		___IndentLevel_0 = value;
	}
};


// Sirenix.OdinInspector.InlineButtonAttribute
struct  InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.InlineButtonAttribute::<MemberMethod>k__BackingField
	String_t* ___U3CMemberMethodU3Ek__BackingField_0;
	// System.String Sirenix.OdinInspector.InlineButtonAttribute::<Label>k__BackingField
	String_t* ___U3CLabelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMemberMethodU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18, ___U3CMemberMethodU3Ek__BackingField_0)); }
	inline String_t* get_U3CMemberMethodU3Ek__BackingField_0() const { return ___U3CMemberMethodU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMemberMethodU3Ek__BackingField_0() { return &___U3CMemberMethodU3Ek__BackingField_0; }
	inline void set_U3CMemberMethodU3Ek__BackingField_0(String_t* value)
	{
		___U3CMemberMethodU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMemberMethodU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLabelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18, ___U3CLabelU3Ek__BackingField_1)); }
	inline String_t* get_U3CLabelU3Ek__BackingField_1() const { return ___U3CLabelU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CLabelU3Ek__BackingField_1() { return &___U3CLabelU3Ek__BackingField_1; }
	inline void set_U3CLabelU3Ek__BackingField_1(String_t* value)
	{
		___U3CLabelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLabelU3Ek__BackingField_1), (void*)value);
	}
};


// Sirenix.OdinInspector.InlinePropertyAttribute
struct  InlinePropertyAttribute_tC4F6EE65AF6B4B9F3D0995C379732DA61E459158  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 Sirenix.OdinInspector.InlinePropertyAttribute::LabelWidth
	int32_t ___LabelWidth_0;

public:
	inline static int32_t get_offset_of_LabelWidth_0() { return static_cast<int32_t>(offsetof(InlinePropertyAttribute_tC4F6EE65AF6B4B9F3D0995C379732DA61E459158, ___LabelWidth_0)); }
	inline int32_t get_LabelWidth_0() const { return ___LabelWidth_0; }
	inline int32_t* get_address_of_LabelWidth_0() { return &___LabelWidth_0; }
	inline void set_LabelWidth_0(int32_t value)
	{
		___LabelWidth_0 = value;
	}
};


// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// Sirenix.OdinInspector.LabelTextAttribute
struct  LabelTextAttribute_tAC01C34A3856E3C62B39EA8A4412BFE522E35D27  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.LabelTextAttribute::Text
	String_t* ___Text_0;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(LabelTextAttribute_tAC01C34A3856E3C62B39EA8A4412BFE522E35D27, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text_0), (void*)value);
	}
};


// Sirenix.OdinInspector.LabelWidthAttribute
struct  LabelWidthAttribute_t2E4C87DC106B9C9E3D7E47E038D3161026726A8B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Single Sirenix.OdinInspector.LabelWidthAttribute::Width
	float ___Width_0;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(LabelWidthAttribute_t2E4C87DC106B9C9E3D7E47E038D3161026726A8B, ___Width_0)); }
	inline float get_Width_0() const { return ___Width_0; }
	inline float* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(float value)
	{
		___Width_0 = value;
	}
};


// Sirenix.OdinInspector.ListDrawerSettingsAttribute
struct  ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::HideAddButton
	bool ___HideAddButton_0;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::HideRemoveButton
	bool ___HideRemoveButton_1;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::ListElementLabelName
	String_t* ___ListElementLabelName_2;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomAddFunction
	String_t* ___CustomAddFunction_3;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomRemoveIndexFunction
	String_t* ___CustomRemoveIndexFunction_4;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::CustomRemoveElementFunction
	String_t* ___CustomRemoveElementFunction_5;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::OnBeginListElementGUI
	String_t* ___OnBeginListElementGUI_6;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::OnEndListElementGUI
	String_t* ___OnEndListElementGUI_7;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::AlwaysAddDefaultValue
	bool ___AlwaysAddDefaultValue_8;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::AddCopiesLastElement
	bool ___AddCopiesLastElement_9;
	// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::onTitleBarGUI
	String_t* ___onTitleBarGUI_10;
	// System.Int32 Sirenix.OdinInspector.ListDrawerSettingsAttribute::numberOfItemsPerPage
	int32_t ___numberOfItemsPerPage_11;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::paging
	bool ___paging_12;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::draggable
	bool ___draggable_13;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::isReadOnly
	bool ___isReadOnly_14;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showItemCount
	bool ___showItemCount_15;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::pagingHasValue
	bool ___pagingHasValue_16;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::draggableHasValue
	bool ___draggableHasValue_17;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::isReadOnlyHasValue
	bool ___isReadOnlyHasValue_18;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showItemCountHasValue
	bool ___showItemCountHasValue_19;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::expanded
	bool ___expanded_20;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::expandedHasValue
	bool ___expandedHasValue_21;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::numberOfItemsPerPageHasValue
	bool ___numberOfItemsPerPageHasValue_22;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showIndexLabels
	bool ___showIndexLabels_23;
	// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::showIndexLabelsHasValue
	bool ___showIndexLabelsHasValue_24;

public:
	inline static int32_t get_offset_of_HideAddButton_0() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___HideAddButton_0)); }
	inline bool get_HideAddButton_0() const { return ___HideAddButton_0; }
	inline bool* get_address_of_HideAddButton_0() { return &___HideAddButton_0; }
	inline void set_HideAddButton_0(bool value)
	{
		___HideAddButton_0 = value;
	}

	inline static int32_t get_offset_of_HideRemoveButton_1() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___HideRemoveButton_1)); }
	inline bool get_HideRemoveButton_1() const { return ___HideRemoveButton_1; }
	inline bool* get_address_of_HideRemoveButton_1() { return &___HideRemoveButton_1; }
	inline void set_HideRemoveButton_1(bool value)
	{
		___HideRemoveButton_1 = value;
	}

	inline static int32_t get_offset_of_ListElementLabelName_2() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___ListElementLabelName_2)); }
	inline String_t* get_ListElementLabelName_2() const { return ___ListElementLabelName_2; }
	inline String_t** get_address_of_ListElementLabelName_2() { return &___ListElementLabelName_2; }
	inline void set_ListElementLabelName_2(String_t* value)
	{
		___ListElementLabelName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ListElementLabelName_2), (void*)value);
	}

	inline static int32_t get_offset_of_CustomAddFunction_3() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___CustomAddFunction_3)); }
	inline String_t* get_CustomAddFunction_3() const { return ___CustomAddFunction_3; }
	inline String_t** get_address_of_CustomAddFunction_3() { return &___CustomAddFunction_3; }
	inline void set_CustomAddFunction_3(String_t* value)
	{
		___CustomAddFunction_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CustomAddFunction_3), (void*)value);
	}

	inline static int32_t get_offset_of_CustomRemoveIndexFunction_4() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___CustomRemoveIndexFunction_4)); }
	inline String_t* get_CustomRemoveIndexFunction_4() const { return ___CustomRemoveIndexFunction_4; }
	inline String_t** get_address_of_CustomRemoveIndexFunction_4() { return &___CustomRemoveIndexFunction_4; }
	inline void set_CustomRemoveIndexFunction_4(String_t* value)
	{
		___CustomRemoveIndexFunction_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CustomRemoveIndexFunction_4), (void*)value);
	}

	inline static int32_t get_offset_of_CustomRemoveElementFunction_5() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___CustomRemoveElementFunction_5)); }
	inline String_t* get_CustomRemoveElementFunction_5() const { return ___CustomRemoveElementFunction_5; }
	inline String_t** get_address_of_CustomRemoveElementFunction_5() { return &___CustomRemoveElementFunction_5; }
	inline void set_CustomRemoveElementFunction_5(String_t* value)
	{
		___CustomRemoveElementFunction_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CustomRemoveElementFunction_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnBeginListElementGUI_6() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___OnBeginListElementGUI_6)); }
	inline String_t* get_OnBeginListElementGUI_6() const { return ___OnBeginListElementGUI_6; }
	inline String_t** get_address_of_OnBeginListElementGUI_6() { return &___OnBeginListElementGUI_6; }
	inline void set_OnBeginListElementGUI_6(String_t* value)
	{
		___OnBeginListElementGUI_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnBeginListElementGUI_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnEndListElementGUI_7() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___OnEndListElementGUI_7)); }
	inline String_t* get_OnEndListElementGUI_7() const { return ___OnEndListElementGUI_7; }
	inline String_t** get_address_of_OnEndListElementGUI_7() { return &___OnEndListElementGUI_7; }
	inline void set_OnEndListElementGUI_7(String_t* value)
	{
		___OnEndListElementGUI_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEndListElementGUI_7), (void*)value);
	}

	inline static int32_t get_offset_of_AlwaysAddDefaultValue_8() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___AlwaysAddDefaultValue_8)); }
	inline bool get_AlwaysAddDefaultValue_8() const { return ___AlwaysAddDefaultValue_8; }
	inline bool* get_address_of_AlwaysAddDefaultValue_8() { return &___AlwaysAddDefaultValue_8; }
	inline void set_AlwaysAddDefaultValue_8(bool value)
	{
		___AlwaysAddDefaultValue_8 = value;
	}

	inline static int32_t get_offset_of_AddCopiesLastElement_9() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___AddCopiesLastElement_9)); }
	inline bool get_AddCopiesLastElement_9() const { return ___AddCopiesLastElement_9; }
	inline bool* get_address_of_AddCopiesLastElement_9() { return &___AddCopiesLastElement_9; }
	inline void set_AddCopiesLastElement_9(bool value)
	{
		___AddCopiesLastElement_9 = value;
	}

	inline static int32_t get_offset_of_onTitleBarGUI_10() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___onTitleBarGUI_10)); }
	inline String_t* get_onTitleBarGUI_10() const { return ___onTitleBarGUI_10; }
	inline String_t** get_address_of_onTitleBarGUI_10() { return &___onTitleBarGUI_10; }
	inline void set_onTitleBarGUI_10(String_t* value)
	{
		___onTitleBarGUI_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onTitleBarGUI_10), (void*)value);
	}

	inline static int32_t get_offset_of_numberOfItemsPerPage_11() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___numberOfItemsPerPage_11)); }
	inline int32_t get_numberOfItemsPerPage_11() const { return ___numberOfItemsPerPage_11; }
	inline int32_t* get_address_of_numberOfItemsPerPage_11() { return &___numberOfItemsPerPage_11; }
	inline void set_numberOfItemsPerPage_11(int32_t value)
	{
		___numberOfItemsPerPage_11 = value;
	}

	inline static int32_t get_offset_of_paging_12() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___paging_12)); }
	inline bool get_paging_12() const { return ___paging_12; }
	inline bool* get_address_of_paging_12() { return &___paging_12; }
	inline void set_paging_12(bool value)
	{
		___paging_12 = value;
	}

	inline static int32_t get_offset_of_draggable_13() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___draggable_13)); }
	inline bool get_draggable_13() const { return ___draggable_13; }
	inline bool* get_address_of_draggable_13() { return &___draggable_13; }
	inline void set_draggable_13(bool value)
	{
		___draggable_13 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_14() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___isReadOnly_14)); }
	inline bool get_isReadOnly_14() const { return ___isReadOnly_14; }
	inline bool* get_address_of_isReadOnly_14() { return &___isReadOnly_14; }
	inline void set_isReadOnly_14(bool value)
	{
		___isReadOnly_14 = value;
	}

	inline static int32_t get_offset_of_showItemCount_15() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___showItemCount_15)); }
	inline bool get_showItemCount_15() const { return ___showItemCount_15; }
	inline bool* get_address_of_showItemCount_15() { return &___showItemCount_15; }
	inline void set_showItemCount_15(bool value)
	{
		___showItemCount_15 = value;
	}

	inline static int32_t get_offset_of_pagingHasValue_16() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___pagingHasValue_16)); }
	inline bool get_pagingHasValue_16() const { return ___pagingHasValue_16; }
	inline bool* get_address_of_pagingHasValue_16() { return &___pagingHasValue_16; }
	inline void set_pagingHasValue_16(bool value)
	{
		___pagingHasValue_16 = value;
	}

	inline static int32_t get_offset_of_draggableHasValue_17() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___draggableHasValue_17)); }
	inline bool get_draggableHasValue_17() const { return ___draggableHasValue_17; }
	inline bool* get_address_of_draggableHasValue_17() { return &___draggableHasValue_17; }
	inline void set_draggableHasValue_17(bool value)
	{
		___draggableHasValue_17 = value;
	}

	inline static int32_t get_offset_of_isReadOnlyHasValue_18() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___isReadOnlyHasValue_18)); }
	inline bool get_isReadOnlyHasValue_18() const { return ___isReadOnlyHasValue_18; }
	inline bool* get_address_of_isReadOnlyHasValue_18() { return &___isReadOnlyHasValue_18; }
	inline void set_isReadOnlyHasValue_18(bool value)
	{
		___isReadOnlyHasValue_18 = value;
	}

	inline static int32_t get_offset_of_showItemCountHasValue_19() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___showItemCountHasValue_19)); }
	inline bool get_showItemCountHasValue_19() const { return ___showItemCountHasValue_19; }
	inline bool* get_address_of_showItemCountHasValue_19() { return &___showItemCountHasValue_19; }
	inline void set_showItemCountHasValue_19(bool value)
	{
		___showItemCountHasValue_19 = value;
	}

	inline static int32_t get_offset_of_expanded_20() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___expanded_20)); }
	inline bool get_expanded_20() const { return ___expanded_20; }
	inline bool* get_address_of_expanded_20() { return &___expanded_20; }
	inline void set_expanded_20(bool value)
	{
		___expanded_20 = value;
	}

	inline static int32_t get_offset_of_expandedHasValue_21() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___expandedHasValue_21)); }
	inline bool get_expandedHasValue_21() const { return ___expandedHasValue_21; }
	inline bool* get_address_of_expandedHasValue_21() { return &___expandedHasValue_21; }
	inline void set_expandedHasValue_21(bool value)
	{
		___expandedHasValue_21 = value;
	}

	inline static int32_t get_offset_of_numberOfItemsPerPageHasValue_22() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___numberOfItemsPerPageHasValue_22)); }
	inline bool get_numberOfItemsPerPageHasValue_22() const { return ___numberOfItemsPerPageHasValue_22; }
	inline bool* get_address_of_numberOfItemsPerPageHasValue_22() { return &___numberOfItemsPerPageHasValue_22; }
	inline void set_numberOfItemsPerPageHasValue_22(bool value)
	{
		___numberOfItemsPerPageHasValue_22 = value;
	}

	inline static int32_t get_offset_of_showIndexLabels_23() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___showIndexLabels_23)); }
	inline bool get_showIndexLabels_23() const { return ___showIndexLabels_23; }
	inline bool* get_address_of_showIndexLabels_23() { return &___showIndexLabels_23; }
	inline void set_showIndexLabels_23(bool value)
	{
		___showIndexLabels_23 = value;
	}

	inline static int32_t get_offset_of_showIndexLabelsHasValue_24() { return static_cast<int32_t>(offsetof(ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181, ___showIndexLabelsHasValue_24)); }
	inline bool get_showIndexLabelsHasValue_24() const { return ___showIndexLabelsHasValue_24; }
	inline bool* get_address_of_showIndexLabelsHasValue_24() { return &___showIndexLabelsHasValue_24; }
	inline void set_showIndexLabelsHasValue_24(bool value)
	{
		___showIndexLabelsHasValue_24 = value;
	}
};


// Sirenix.OdinInspector.MaxValueAttribute
struct  MaxValueAttribute_tFC98EB9F62599D96C7A430775FE8543EC98309BC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Double Sirenix.OdinInspector.MaxValueAttribute::MaxValue
	double ___MaxValue_0;
	// System.String Sirenix.OdinInspector.MaxValueAttribute::Expression
	String_t* ___Expression_1;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(MaxValueAttribute_tFC98EB9F62599D96C7A430775FE8543EC98309BC, ___MaxValue_0)); }
	inline double get_MaxValue_0() const { return ___MaxValue_0; }
	inline double* get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(double value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_Expression_1() { return static_cast<int32_t>(offsetof(MaxValueAttribute_tFC98EB9F62599D96C7A430775FE8543EC98309BC, ___Expression_1)); }
	inline String_t* get_Expression_1() const { return ___Expression_1; }
	inline String_t** get_address_of_Expression_1() { return &___Expression_1; }
	inline void set_Expression_1(String_t* value)
	{
		___Expression_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Expression_1), (void*)value);
	}
};


// Sirenix.OdinInspector.MinMaxSliderAttribute
struct  MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Single Sirenix.OdinInspector.MinMaxSliderAttribute::MinValue
	float ___MinValue_0;
	// System.Single Sirenix.OdinInspector.MinMaxSliderAttribute::MaxValue
	float ___MaxValue_1;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MinMember
	String_t* ___MinMember_2;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MaxMember
	String_t* ___MaxMember_3;
	// System.String Sirenix.OdinInspector.MinMaxSliderAttribute::MinMaxMember
	String_t* ___MinMaxMember_4;
	// System.Boolean Sirenix.OdinInspector.MinMaxSliderAttribute::ShowFields
	bool ___ShowFields_5;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41, ___MinValue_0)); }
	inline float get_MinValue_0() const { return ___MinValue_0; }
	inline float* get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(float value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41, ___MaxValue_1)); }
	inline float get_MaxValue_1() const { return ___MaxValue_1; }
	inline float* get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(float value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinMember_2() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41, ___MinMember_2)); }
	inline String_t* get_MinMember_2() const { return ___MinMember_2; }
	inline String_t** get_address_of_MinMember_2() { return &___MinMember_2; }
	inline void set_MinMember_2(String_t* value)
	{
		___MinMember_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MinMember_2), (void*)value);
	}

	inline static int32_t get_offset_of_MaxMember_3() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41, ___MaxMember_3)); }
	inline String_t* get_MaxMember_3() const { return ___MaxMember_3; }
	inline String_t** get_address_of_MaxMember_3() { return &___MaxMember_3; }
	inline void set_MaxMember_3(String_t* value)
	{
		___MaxMember_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MaxMember_3), (void*)value);
	}

	inline static int32_t get_offset_of_MinMaxMember_4() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41, ___MinMaxMember_4)); }
	inline String_t* get_MinMaxMember_4() const { return ___MinMaxMember_4; }
	inline String_t** get_address_of_MinMaxMember_4() { return &___MinMaxMember_4; }
	inline void set_MinMaxMember_4(String_t* value)
	{
		___MinMaxMember_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MinMaxMember_4), (void*)value);
	}

	inline static int32_t get_offset_of_ShowFields_5() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41, ___ShowFields_5)); }
	inline bool get_ShowFields_5() const { return ___ShowFields_5; }
	inline bool* get_address_of_ShowFields_5() { return &___ShowFields_5; }
	inline void set_ShowFields_5(bool value)
	{
		___ShowFields_5 = value;
	}
};


// Sirenix.OdinInspector.MinValueAttribute
struct  MinValueAttribute_t45E8A787D2858D90D37F7A87C0582C0BF131FA34  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Double Sirenix.OdinInspector.MinValueAttribute::MinValue
	double ___MinValue_0;
	// System.String Sirenix.OdinInspector.MinValueAttribute::Expression
	String_t* ___Expression_1;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(MinValueAttribute_t45E8A787D2858D90D37F7A87C0582C0BF131FA34, ___MinValue_0)); }
	inline double get_MinValue_0() const { return ___MinValue_0; }
	inline double* get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(double value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_Expression_1() { return static_cast<int32_t>(offsetof(MinValueAttribute_t45E8A787D2858D90D37F7A87C0582C0BF131FA34, ___Expression_1)); }
	inline String_t* get_Expression_1() const { return ___Expression_1; }
	inline String_t** get_address_of_Expression_1() { return &___Expression_1; }
	inline void set_Expression_1(String_t* value)
	{
		___Expression_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Expression_1), (void*)value);
	}
};


// Sirenix.OdinInspector.MultiLinePropertyAttribute
struct  MultiLinePropertyAttribute_tD35BC318DFDFAC8469DA2D276235CCF0E59B319D  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 Sirenix.OdinInspector.MultiLinePropertyAttribute::Lines
	int32_t ___Lines_0;

public:
	inline static int32_t get_offset_of_Lines_0() { return static_cast<int32_t>(offsetof(MultiLinePropertyAttribute_tD35BC318DFDFAC8469DA2D276235CCF0E59B319D, ___Lines_0)); }
	inline int32_t get_Lines_0() const { return ___Lines_0; }
	inline int32_t* get_address_of_Lines_0() { return &___Lines_0; }
	inline void set_Lines_0(int32_t value)
	{
		___Lines_0 = value;
	}
};


// Sirenix.OdinInspector.OdinRegisterAttributeAttribute
struct  OdinRegisterAttributeAttribute_t8A82A1C5F996F02644E959C5EAD6533EBF268FFA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type Sirenix.OdinInspector.OdinRegisterAttributeAttribute::AttributeType
	Type_t * ___AttributeType_0;
	// System.String Sirenix.OdinInspector.OdinRegisterAttributeAttribute::Categories
	String_t* ___Categories_1;
	// System.String Sirenix.OdinInspector.OdinRegisterAttributeAttribute::Description
	String_t* ___Description_2;
	// System.String Sirenix.OdinInspector.OdinRegisterAttributeAttribute::DocumentationUrl
	String_t* ___DocumentationUrl_3;

public:
	inline static int32_t get_offset_of_AttributeType_0() { return static_cast<int32_t>(offsetof(OdinRegisterAttributeAttribute_t8A82A1C5F996F02644E959C5EAD6533EBF268FFA, ___AttributeType_0)); }
	inline Type_t * get_AttributeType_0() const { return ___AttributeType_0; }
	inline Type_t ** get_address_of_AttributeType_0() { return &___AttributeType_0; }
	inline void set_AttributeType_0(Type_t * value)
	{
		___AttributeType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AttributeType_0), (void*)value);
	}

	inline static int32_t get_offset_of_Categories_1() { return static_cast<int32_t>(offsetof(OdinRegisterAttributeAttribute_t8A82A1C5F996F02644E959C5EAD6533EBF268FFA, ___Categories_1)); }
	inline String_t* get_Categories_1() const { return ___Categories_1; }
	inline String_t** get_address_of_Categories_1() { return &___Categories_1; }
	inline void set_Categories_1(String_t* value)
	{
		___Categories_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Categories_1), (void*)value);
	}

	inline static int32_t get_offset_of_Description_2() { return static_cast<int32_t>(offsetof(OdinRegisterAttributeAttribute_t8A82A1C5F996F02644E959C5EAD6533EBF268FFA, ___Description_2)); }
	inline String_t* get_Description_2() const { return ___Description_2; }
	inline String_t** get_address_of_Description_2() { return &___Description_2; }
	inline void set_Description_2(String_t* value)
	{
		___Description_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Description_2), (void*)value);
	}

	inline static int32_t get_offset_of_DocumentationUrl_3() { return static_cast<int32_t>(offsetof(OdinRegisterAttributeAttribute_t8A82A1C5F996F02644E959C5EAD6533EBF268FFA, ___DocumentationUrl_3)); }
	inline String_t* get_DocumentationUrl_3() const { return ___DocumentationUrl_3; }
	inline String_t** get_address_of_DocumentationUrl_3() { return &___DocumentationUrl_3; }
	inline void set_DocumentationUrl_3(String_t* value)
	{
		___DocumentationUrl_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DocumentationUrl_3), (void*)value);
	}
};


// Sirenix.OdinInspector.OnValueChangedAttribute
struct  OnValueChangedAttribute_tFE13CD46B1B5466F8AA8C351562C9D3BB9FB7623  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.OnValueChangedAttribute::MethodName
	String_t* ___MethodName_0;
	// System.Boolean Sirenix.OdinInspector.OnValueChangedAttribute::IncludeChildren
	bool ___IncludeChildren_1;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(OnValueChangedAttribute_tFE13CD46B1B5466F8AA8C351562C9D3BB9FB7623, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MethodName_0), (void*)value);
	}

	inline static int32_t get_offset_of_IncludeChildren_1() { return static_cast<int32_t>(offsetof(OnValueChangedAttribute_tFE13CD46B1B5466F8AA8C351562C9D3BB9FB7623, ___IncludeChildren_1)); }
	inline bool get_IncludeChildren_1() const { return ___IncludeChildren_1; }
	inline bool* get_address_of_IncludeChildren_1() { return &___IncludeChildren_1; }
	inline void set_IncludeChildren_1(bool value)
	{
		___IncludeChildren_1 = value;
	}
};


// Sirenix.OdinInspector.PropertyGroupAttribute
struct  PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::GroupID
	String_t* ___GroupID_0;
	// System.String Sirenix.OdinInspector.PropertyGroupAttribute::GroupName
	String_t* ___GroupName_1;
	// System.Int32 Sirenix.OdinInspector.PropertyGroupAttribute::Order
	int32_t ___Order_2;

public:
	inline static int32_t get_offset_of_GroupID_0() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1, ___GroupID_0)); }
	inline String_t* get_GroupID_0() const { return ___GroupID_0; }
	inline String_t** get_address_of_GroupID_0() { return &___GroupID_0; }
	inline void set_GroupID_0(String_t* value)
	{
		___GroupID_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GroupID_0), (void*)value);
	}

	inline static int32_t get_offset_of_GroupName_1() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1, ___GroupName_1)); }
	inline String_t* get_GroupName_1() const { return ___GroupName_1; }
	inline String_t** get_address_of_GroupName_1() { return &___GroupName_1; }
	inline void set_GroupName_1(String_t* value)
	{
		___GroupName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GroupName_1), (void*)value);
	}

	inline static int32_t get_offset_of_Order_2() { return static_cast<int32_t>(offsetof(PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1, ___Order_2)); }
	inline int32_t get_Order_2() const { return ___Order_2; }
	inline int32_t* get_address_of_Order_2() { return &___Order_2; }
	inline void set_Order_2(int32_t value)
	{
		___Order_2 = value;
	}
};


// Sirenix.OdinInspector.PropertyOrderAttribute
struct  PropertyOrderAttribute_t2F83A60EEA2D6D2C91E10D2A83E22326F5F71936  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 Sirenix.OdinInspector.PropertyOrderAttribute::Order
	int32_t ___Order_0;

public:
	inline static int32_t get_offset_of_Order_0() { return static_cast<int32_t>(offsetof(PropertyOrderAttribute_t2F83A60EEA2D6D2C91E10D2A83E22326F5F71936, ___Order_0)); }
	inline int32_t get_Order_0() const { return ___Order_0; }
	inline int32_t* get_address_of_Order_0() { return &___Order_0; }
	inline void set_Order_0(int32_t value)
	{
		___Order_0 = value;
	}
};


// Sirenix.OdinInspector.PropertyRangeAttribute
struct  PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Double Sirenix.OdinInspector.PropertyRangeAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.PropertyRangeAttribute::Max
	double ___Max_1;
	// System.String Sirenix.OdinInspector.PropertyRangeAttribute::MinMember
	String_t* ___MinMember_2;
	// System.String Sirenix.OdinInspector.PropertyRangeAttribute::MaxMember
	String_t* ___MaxMember_3;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}

	inline static int32_t get_offset_of_MinMember_2() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0, ___MinMember_2)); }
	inline String_t* get_MinMember_2() const { return ___MinMember_2; }
	inline String_t** get_address_of_MinMember_2() { return &___MinMember_2; }
	inline void set_MinMember_2(String_t* value)
	{
		___MinMember_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MinMember_2), (void*)value);
	}

	inline static int32_t get_offset_of_MaxMember_3() { return static_cast<int32_t>(offsetof(PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0, ___MaxMember_3)); }
	inline String_t* get_MaxMember_3() const { return ___MaxMember_3; }
	inline String_t** get_address_of_MaxMember_3() { return &___MaxMember_3; }
	inline void set_MaxMember_3(String_t* value)
	{
		___MaxMember_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MaxMember_3), (void*)value);
	}
};


// Sirenix.OdinInspector.PropertySpaceAttribute
struct  PropertySpaceAttribute_tDF729E8EB760A22841021D50F961B59FAB282048  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Single Sirenix.OdinInspector.PropertySpaceAttribute::SpaceBefore
	float ___SpaceBefore_0;
	// System.Single Sirenix.OdinInspector.PropertySpaceAttribute::SpaceAfter
	float ___SpaceAfter_1;

public:
	inline static int32_t get_offset_of_SpaceBefore_0() { return static_cast<int32_t>(offsetof(PropertySpaceAttribute_tDF729E8EB760A22841021D50F961B59FAB282048, ___SpaceBefore_0)); }
	inline float get_SpaceBefore_0() const { return ___SpaceBefore_0; }
	inline float* get_address_of_SpaceBefore_0() { return &___SpaceBefore_0; }
	inline void set_SpaceBefore_0(float value)
	{
		___SpaceBefore_0 = value;
	}

	inline static int32_t get_offset_of_SpaceAfter_1() { return static_cast<int32_t>(offsetof(PropertySpaceAttribute_tDF729E8EB760A22841021D50F961B59FAB282048, ___SpaceAfter_1)); }
	inline float get_SpaceAfter_1() const { return ___SpaceAfter_1; }
	inline float* get_address_of_SpaceAfter_1() { return &___SpaceAfter_1; }
	inline void set_SpaceAfter_1(float value)
	{
		___SpaceAfter_1 = value;
	}
};


// Sirenix.OdinInspector.PropertyTooltipAttribute
struct  PropertyTooltipAttribute_t7BC0BF969C21340C1CA2B7180CFD04350B79204B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.PropertyTooltipAttribute::Tooltip
	String_t* ___Tooltip_0;

public:
	inline static int32_t get_offset_of_Tooltip_0() { return static_cast<int32_t>(offsetof(PropertyTooltipAttribute_t7BC0BF969C21340C1CA2B7180CFD04350B79204B, ___Tooltip_0)); }
	inline String_t* get_Tooltip_0() const { return ___Tooltip_0; }
	inline String_t** get_address_of_Tooltip_0() { return &___Tooltip_0; }
	inline void set_Tooltip_0(String_t* value)
	{
		___Tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Tooltip_0), (void*)value);
	}
};


// Sirenix.OdinInspector.ReadOnlyAttribute
struct  ReadOnlyAttribute_t4A1BE30F9257C5E0187EFB8FC659A42F4B61472C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.SceneObjectsOnlyAttribute
struct  SceneObjectsOnlyAttribute_t74D00B9886B272BE983F0AE1F447B56E222402A2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.ShowDrawerChainAttribute
struct  ShowDrawerChainAttribute_tAF05CAAEC33491EA7281A449128A45877A66A7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.ShowForPrefabOnlyAttribute
struct  ShowForPrefabOnlyAttribute_tF680D3BAFA9AF8C0C8BE3A0EC1DB9380BCB319D4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.ShowIfAttribute
struct  ShowIfAttribute_tDF7F785B12F1489ED80C78FC2695E7363CC5B8AE  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.ShowIfAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Boolean Sirenix.OdinInspector.ShowIfAttribute::Animate
	bool ___Animate_1;
	// System.Object Sirenix.OdinInspector.ShowIfAttribute::Value
	RuntimeObject * ___Value_2;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(ShowIfAttribute_tDF7F785B12F1489ED80C78FC2695E7363CC5B8AE, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberName_0), (void*)value);
	}

	inline static int32_t get_offset_of_Animate_1() { return static_cast<int32_t>(offsetof(ShowIfAttribute_tDF7F785B12F1489ED80C78FC2695E7363CC5B8AE, ___Animate_1)); }
	inline bool get_Animate_1() const { return ___Animate_1; }
	inline bool* get_address_of_Animate_1() { return &___Animate_1; }
	inline void set_Animate_1(bool value)
	{
		___Animate_1 = value;
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(ShowIfAttribute_tDF7F785B12F1489ED80C78FC2695E7363CC5B8AE, ___Value_2)); }
	inline RuntimeObject * get_Value_2() const { return ___Value_2; }
	inline RuntimeObject ** get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(RuntimeObject * value)
	{
		___Value_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_2), (void*)value);
	}
};


// Sirenix.OdinInspector.ShowInInlineEditorsAttribute
struct  ShowInInlineEditorsAttribute_t074163930680127467E651ECE68C8A44528E93D2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.ShowInInspectorAttribute
struct  ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute
struct  ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.ShowPropertyResolverAttribute
struct  ShowPropertyResolverAttribute_t38E4D018FDE433172E3184B2B8BB77CF841B8A9E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// Sirenix.OdinInspector.SuffixLabelAttribute
struct  SuffixLabelAttribute_t990C9A8E87740E342FB14097884866F5D3A9869C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.SuffixLabelAttribute::Label
	String_t* ___Label_0;
	// System.Boolean Sirenix.OdinInspector.SuffixLabelAttribute::Overlay
	bool ___Overlay_1;

public:
	inline static int32_t get_offset_of_Label_0() { return static_cast<int32_t>(offsetof(SuffixLabelAttribute_t990C9A8E87740E342FB14097884866F5D3A9869C, ___Label_0)); }
	inline String_t* get_Label_0() const { return ___Label_0; }
	inline String_t** get_address_of_Label_0() { return &___Label_0; }
	inline void set_Label_0(String_t* value)
	{
		___Label_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Label_0), (void*)value);
	}

	inline static int32_t get_offset_of_Overlay_1() { return static_cast<int32_t>(offsetof(SuffixLabelAttribute_t990C9A8E87740E342FB14097884866F5D3A9869C, ___Overlay_1)); }
	inline bool get_Overlay_1() const { return ___Overlay_1; }
	inline bool* get_address_of_Overlay_1() { return &___Overlay_1; }
	inline void set_Overlay_1(bool value)
	{
		___Overlay_1 = value;
	}
};


// Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute
struct  SuppressInvalidAttributeErrorAttribute_t8CCDC91E2DECC329BCDF59102809D220CCF7794E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.TableColumnWidthAttribute
struct  TableColumnWidthAttribute_t62FFBE33C21C9BCA7419C73D67C20BBF291671BF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 Sirenix.OdinInspector.TableColumnWidthAttribute::Width
	int32_t ___Width_0;
	// System.Boolean Sirenix.OdinInspector.TableColumnWidthAttribute::Resizable
	bool ___Resizable_1;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(TableColumnWidthAttribute_t62FFBE33C21C9BCA7419C73D67C20BBF291671BF, ___Width_0)); }
	inline int32_t get_Width_0() const { return ___Width_0; }
	inline int32_t* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(int32_t value)
	{
		___Width_0 = value;
	}

	inline static int32_t get_offset_of_Resizable_1() { return static_cast<int32_t>(offsetof(TableColumnWidthAttribute_t62FFBE33C21C9BCA7419C73D67C20BBF291671BF, ___Resizable_1)); }
	inline bool get_Resizable_1() const { return ___Resizable_1; }
	inline bool* get_address_of_Resizable_1() { return &___Resizable_1; }
	inline void set_Resizable_1(bool value)
	{
		___Resizable_1 = value;
	}
};


// Sirenix.OdinInspector.TableListAttribute
struct  TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::NumberOfItemsPerPage
	int32_t ___NumberOfItemsPerPage_0;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::IsReadOnly
	bool ___IsReadOnly_1;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::DefaultMinColumnWidth
	int32_t ___DefaultMinColumnWidth_2;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::ShowIndexLabels
	bool ___ShowIndexLabels_3;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::DrawScrollView
	bool ___DrawScrollView_4;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::MinScrollViewHeight
	int32_t ___MinScrollViewHeight_5;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::MaxScrollViewHeight
	int32_t ___MaxScrollViewHeight_6;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::AlwaysExpanded
	bool ___AlwaysExpanded_7;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::HideToolbar
	bool ___HideToolbar_8;
	// System.Int32 Sirenix.OdinInspector.TableListAttribute::CellPadding
	int32_t ___CellPadding_9;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::showPagingHasValue
	bool ___showPagingHasValue_10;
	// System.Boolean Sirenix.OdinInspector.TableListAttribute::showPaging
	bool ___showPaging_11;

public:
	inline static int32_t get_offset_of_NumberOfItemsPerPage_0() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___NumberOfItemsPerPage_0)); }
	inline int32_t get_NumberOfItemsPerPage_0() const { return ___NumberOfItemsPerPage_0; }
	inline int32_t* get_address_of_NumberOfItemsPerPage_0() { return &___NumberOfItemsPerPage_0; }
	inline void set_NumberOfItemsPerPage_0(int32_t value)
	{
		___NumberOfItemsPerPage_0 = value;
	}

	inline static int32_t get_offset_of_IsReadOnly_1() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___IsReadOnly_1)); }
	inline bool get_IsReadOnly_1() const { return ___IsReadOnly_1; }
	inline bool* get_address_of_IsReadOnly_1() { return &___IsReadOnly_1; }
	inline void set_IsReadOnly_1(bool value)
	{
		___IsReadOnly_1 = value;
	}

	inline static int32_t get_offset_of_DefaultMinColumnWidth_2() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___DefaultMinColumnWidth_2)); }
	inline int32_t get_DefaultMinColumnWidth_2() const { return ___DefaultMinColumnWidth_2; }
	inline int32_t* get_address_of_DefaultMinColumnWidth_2() { return &___DefaultMinColumnWidth_2; }
	inline void set_DefaultMinColumnWidth_2(int32_t value)
	{
		___DefaultMinColumnWidth_2 = value;
	}

	inline static int32_t get_offset_of_ShowIndexLabels_3() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___ShowIndexLabels_3)); }
	inline bool get_ShowIndexLabels_3() const { return ___ShowIndexLabels_3; }
	inline bool* get_address_of_ShowIndexLabels_3() { return &___ShowIndexLabels_3; }
	inline void set_ShowIndexLabels_3(bool value)
	{
		___ShowIndexLabels_3 = value;
	}

	inline static int32_t get_offset_of_DrawScrollView_4() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___DrawScrollView_4)); }
	inline bool get_DrawScrollView_4() const { return ___DrawScrollView_4; }
	inline bool* get_address_of_DrawScrollView_4() { return &___DrawScrollView_4; }
	inline void set_DrawScrollView_4(bool value)
	{
		___DrawScrollView_4 = value;
	}

	inline static int32_t get_offset_of_MinScrollViewHeight_5() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___MinScrollViewHeight_5)); }
	inline int32_t get_MinScrollViewHeight_5() const { return ___MinScrollViewHeight_5; }
	inline int32_t* get_address_of_MinScrollViewHeight_5() { return &___MinScrollViewHeight_5; }
	inline void set_MinScrollViewHeight_5(int32_t value)
	{
		___MinScrollViewHeight_5 = value;
	}

	inline static int32_t get_offset_of_MaxScrollViewHeight_6() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___MaxScrollViewHeight_6)); }
	inline int32_t get_MaxScrollViewHeight_6() const { return ___MaxScrollViewHeight_6; }
	inline int32_t* get_address_of_MaxScrollViewHeight_6() { return &___MaxScrollViewHeight_6; }
	inline void set_MaxScrollViewHeight_6(int32_t value)
	{
		___MaxScrollViewHeight_6 = value;
	}

	inline static int32_t get_offset_of_AlwaysExpanded_7() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___AlwaysExpanded_7)); }
	inline bool get_AlwaysExpanded_7() const { return ___AlwaysExpanded_7; }
	inline bool* get_address_of_AlwaysExpanded_7() { return &___AlwaysExpanded_7; }
	inline void set_AlwaysExpanded_7(bool value)
	{
		___AlwaysExpanded_7 = value;
	}

	inline static int32_t get_offset_of_HideToolbar_8() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___HideToolbar_8)); }
	inline bool get_HideToolbar_8() const { return ___HideToolbar_8; }
	inline bool* get_address_of_HideToolbar_8() { return &___HideToolbar_8; }
	inline void set_HideToolbar_8(bool value)
	{
		___HideToolbar_8 = value;
	}

	inline static int32_t get_offset_of_CellPadding_9() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___CellPadding_9)); }
	inline int32_t get_CellPadding_9() const { return ___CellPadding_9; }
	inline int32_t* get_address_of_CellPadding_9() { return &___CellPadding_9; }
	inline void set_CellPadding_9(int32_t value)
	{
		___CellPadding_9 = value;
	}

	inline static int32_t get_offset_of_showPagingHasValue_10() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___showPagingHasValue_10)); }
	inline bool get_showPagingHasValue_10() const { return ___showPagingHasValue_10; }
	inline bool* get_address_of_showPagingHasValue_10() { return &___showPagingHasValue_10; }
	inline void set_showPagingHasValue_10(bool value)
	{
		___showPagingHasValue_10 = value;
	}

	inline static int32_t get_offset_of_showPaging_11() { return static_cast<int32_t>(offsetof(TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9, ___showPaging_11)); }
	inline bool get_showPaging_11() const { return ___showPaging_11; }
	inline bool* get_address_of_showPaging_11() { return &___showPaging_11; }
	inline void set_showPaging_11(bool value)
	{
		___showPaging_11 = value;
	}
};


// Sirenix.OdinInspector.TableMatrixAttribute
struct  TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::IsReadOnly
	bool ___IsReadOnly_0;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::ResizableColumns
	bool ___ResizableColumns_1;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::VerticalTitle
	String_t* ___VerticalTitle_2;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::HorizontalTitle
	String_t* ___HorizontalTitle_3;
	// System.String Sirenix.OdinInspector.TableMatrixAttribute::DrawElementMethod
	String_t* ___DrawElementMethod_4;
	// System.Int32 Sirenix.OdinInspector.TableMatrixAttribute::RowHeight
	int32_t ___RowHeight_5;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::SquareCells
	bool ___SquareCells_6;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::HideColumnIndices
	bool ___HideColumnIndices_7;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::HideRowIndices
	bool ___HideRowIndices_8;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::RespectIndentLevel
	bool ___RespectIndentLevel_9;
	// System.Boolean Sirenix.OdinInspector.TableMatrixAttribute::Transpose
	bool ___Transpose_10;

public:
	inline static int32_t get_offset_of_IsReadOnly_0() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___IsReadOnly_0)); }
	inline bool get_IsReadOnly_0() const { return ___IsReadOnly_0; }
	inline bool* get_address_of_IsReadOnly_0() { return &___IsReadOnly_0; }
	inline void set_IsReadOnly_0(bool value)
	{
		___IsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_ResizableColumns_1() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___ResizableColumns_1)); }
	inline bool get_ResizableColumns_1() const { return ___ResizableColumns_1; }
	inline bool* get_address_of_ResizableColumns_1() { return &___ResizableColumns_1; }
	inline void set_ResizableColumns_1(bool value)
	{
		___ResizableColumns_1 = value;
	}

	inline static int32_t get_offset_of_VerticalTitle_2() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___VerticalTitle_2)); }
	inline String_t* get_VerticalTitle_2() const { return ___VerticalTitle_2; }
	inline String_t** get_address_of_VerticalTitle_2() { return &___VerticalTitle_2; }
	inline void set_VerticalTitle_2(String_t* value)
	{
		___VerticalTitle_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VerticalTitle_2), (void*)value);
	}

	inline static int32_t get_offset_of_HorizontalTitle_3() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___HorizontalTitle_3)); }
	inline String_t* get_HorizontalTitle_3() const { return ___HorizontalTitle_3; }
	inline String_t** get_address_of_HorizontalTitle_3() { return &___HorizontalTitle_3; }
	inline void set_HorizontalTitle_3(String_t* value)
	{
		___HorizontalTitle_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HorizontalTitle_3), (void*)value);
	}

	inline static int32_t get_offset_of_DrawElementMethod_4() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___DrawElementMethod_4)); }
	inline String_t* get_DrawElementMethod_4() const { return ___DrawElementMethod_4; }
	inline String_t** get_address_of_DrawElementMethod_4() { return &___DrawElementMethod_4; }
	inline void set_DrawElementMethod_4(String_t* value)
	{
		___DrawElementMethod_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DrawElementMethod_4), (void*)value);
	}

	inline static int32_t get_offset_of_RowHeight_5() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___RowHeight_5)); }
	inline int32_t get_RowHeight_5() const { return ___RowHeight_5; }
	inline int32_t* get_address_of_RowHeight_5() { return &___RowHeight_5; }
	inline void set_RowHeight_5(int32_t value)
	{
		___RowHeight_5 = value;
	}

	inline static int32_t get_offset_of_SquareCells_6() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___SquareCells_6)); }
	inline bool get_SquareCells_6() const { return ___SquareCells_6; }
	inline bool* get_address_of_SquareCells_6() { return &___SquareCells_6; }
	inline void set_SquareCells_6(bool value)
	{
		___SquareCells_6 = value;
	}

	inline static int32_t get_offset_of_HideColumnIndices_7() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___HideColumnIndices_7)); }
	inline bool get_HideColumnIndices_7() const { return ___HideColumnIndices_7; }
	inline bool* get_address_of_HideColumnIndices_7() { return &___HideColumnIndices_7; }
	inline void set_HideColumnIndices_7(bool value)
	{
		___HideColumnIndices_7 = value;
	}

	inline static int32_t get_offset_of_HideRowIndices_8() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___HideRowIndices_8)); }
	inline bool get_HideRowIndices_8() const { return ___HideRowIndices_8; }
	inline bool* get_address_of_HideRowIndices_8() { return &___HideRowIndices_8; }
	inline void set_HideRowIndices_8(bool value)
	{
		___HideRowIndices_8 = value;
	}

	inline static int32_t get_offset_of_RespectIndentLevel_9() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___RespectIndentLevel_9)); }
	inline bool get_RespectIndentLevel_9() const { return ___RespectIndentLevel_9; }
	inline bool* get_address_of_RespectIndentLevel_9() { return &___RespectIndentLevel_9; }
	inline void set_RespectIndentLevel_9(bool value)
	{
		___RespectIndentLevel_9 = value;
	}

	inline static int32_t get_offset_of_Transpose_10() { return static_cast<int32_t>(offsetof(TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E, ___Transpose_10)); }
	inline bool get_Transpose_10() const { return ___Transpose_10; }
	inline bool* get_address_of_Transpose_10() { return &___Transpose_10; }
	inline void set_Transpose_10(bool value)
	{
		___Transpose_10 = value;
	}
};


// Sirenix.OdinInspector.ToggleAttribute
struct  ToggleAttribute_tE3BA633AE4C4A847529A9FEB83DFF3C17CB83F2B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.ToggleAttribute::ToggleMemberName
	String_t* ___ToggleMemberName_0;
	// System.Boolean Sirenix.OdinInspector.ToggleAttribute::CollapseOthersOnExpand
	bool ___CollapseOthersOnExpand_1;

public:
	inline static int32_t get_offset_of_ToggleMemberName_0() { return static_cast<int32_t>(offsetof(ToggleAttribute_tE3BA633AE4C4A847529A9FEB83DFF3C17CB83F2B, ___ToggleMemberName_0)); }
	inline String_t* get_ToggleMemberName_0() const { return ___ToggleMemberName_0; }
	inline String_t** get_address_of_ToggleMemberName_0() { return &___ToggleMemberName_0; }
	inline void set_ToggleMemberName_0(String_t* value)
	{
		___ToggleMemberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ToggleMemberName_0), (void*)value);
	}

	inline static int32_t get_offset_of_CollapseOthersOnExpand_1() { return static_cast<int32_t>(offsetof(ToggleAttribute_tE3BA633AE4C4A847529A9FEB83DFF3C17CB83F2B, ___CollapseOthersOnExpand_1)); }
	inline bool get_CollapseOthersOnExpand_1() const { return ___CollapseOthersOnExpand_1; }
	inline bool* get_address_of_CollapseOthersOnExpand_1() { return &___CollapseOthersOnExpand_1; }
	inline void set_CollapseOthersOnExpand_1(bool value)
	{
		___CollapseOthersOnExpand_1 = value;
	}
};


// Sirenix.OdinInspector.ToggleLeftAttribute
struct  ToggleLeftAttribute_t4CC8B95C528CF91FEA46AABBC6237B2258FD77C9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.TypeFilterAttribute
struct  TypeFilterAttribute_t7989849079B6440606CBDA93E5AE67B4877A56FE  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.TypeFilterAttribute::MemberName
	String_t* ___MemberName_0;
	// System.String Sirenix.OdinInspector.TypeFilterAttribute::DropdownTitle
	String_t* ___DropdownTitle_1;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(TypeFilterAttribute_t7989849079B6440606CBDA93E5AE67B4877A56FE, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberName_0), (void*)value);
	}

	inline static int32_t get_offset_of_DropdownTitle_1() { return static_cast<int32_t>(offsetof(TypeFilterAttribute_t7989849079B6440606CBDA93E5AE67B4877A56FE, ___DropdownTitle_1)); }
	inline String_t* get_DropdownTitle_1() const { return ___DropdownTitle_1; }
	inline String_t** get_address_of_DropdownTitle_1() { return &___DropdownTitle_1; }
	inline void set_DropdownTitle_1(String_t* value)
	{
		___DropdownTitle_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DropdownTitle_1), (void*)value);
	}
};


// Sirenix.OdinInspector.TypeInfoBoxAttribute
struct  TypeInfoBoxAttribute_t43B3E84724467282A1E3696E35325EF8C66E4BBD  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.TypeInfoBoxAttribute::Message
	String_t* ___Message_0;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(TypeInfoBoxAttribute_t43B3E84724467282A1E3696E35325EF8C66E4BBD, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Message_0), (void*)value);
	}
};


// Sirenix.OdinInspector.ValueDropdownAttribute
struct  ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.ValueDropdownAttribute::MemberName
	String_t* ___MemberName_0;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::NumberOfItemsBeforeEnablingSearch
	int32_t ___NumberOfItemsBeforeEnablingSearch_1;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::IsUniqueList
	bool ___IsUniqueList_2;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DrawDropdownForListElements
	bool ___DrawDropdownForListElements_3;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DisableListAddButtonBehaviour
	bool ___DisableListAddButtonBehaviour_4;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::ExcludeExistingValuesInList
	bool ___ExcludeExistingValuesInList_5;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::ExpandAllMenuItems
	bool ___ExpandAllMenuItems_6;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::AppendNextDrawer
	bool ___AppendNextDrawer_7;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DisableGUIInAppendedDrawer
	bool ___DisableGUIInAppendedDrawer_8;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::DoubleClickToConfirm
	bool ___DoubleClickToConfirm_9;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::FlattenTreeView
	bool ___FlattenTreeView_10;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::DropdownWidth
	int32_t ___DropdownWidth_11;
	// System.Int32 Sirenix.OdinInspector.ValueDropdownAttribute::DropdownHeight
	int32_t ___DropdownHeight_12;
	// System.String Sirenix.OdinInspector.ValueDropdownAttribute::DropdownTitle
	String_t* ___DropdownTitle_13;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::SortDropdownItems
	bool ___SortDropdownItems_14;
	// System.Boolean Sirenix.OdinInspector.ValueDropdownAttribute::HideChildProperties
	bool ___HideChildProperties_15;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberName_0), (void*)value);
	}

	inline static int32_t get_offset_of_NumberOfItemsBeforeEnablingSearch_1() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___NumberOfItemsBeforeEnablingSearch_1)); }
	inline int32_t get_NumberOfItemsBeforeEnablingSearch_1() const { return ___NumberOfItemsBeforeEnablingSearch_1; }
	inline int32_t* get_address_of_NumberOfItemsBeforeEnablingSearch_1() { return &___NumberOfItemsBeforeEnablingSearch_1; }
	inline void set_NumberOfItemsBeforeEnablingSearch_1(int32_t value)
	{
		___NumberOfItemsBeforeEnablingSearch_1 = value;
	}

	inline static int32_t get_offset_of_IsUniqueList_2() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___IsUniqueList_2)); }
	inline bool get_IsUniqueList_2() const { return ___IsUniqueList_2; }
	inline bool* get_address_of_IsUniqueList_2() { return &___IsUniqueList_2; }
	inline void set_IsUniqueList_2(bool value)
	{
		___IsUniqueList_2 = value;
	}

	inline static int32_t get_offset_of_DrawDropdownForListElements_3() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___DrawDropdownForListElements_3)); }
	inline bool get_DrawDropdownForListElements_3() const { return ___DrawDropdownForListElements_3; }
	inline bool* get_address_of_DrawDropdownForListElements_3() { return &___DrawDropdownForListElements_3; }
	inline void set_DrawDropdownForListElements_3(bool value)
	{
		___DrawDropdownForListElements_3 = value;
	}

	inline static int32_t get_offset_of_DisableListAddButtonBehaviour_4() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___DisableListAddButtonBehaviour_4)); }
	inline bool get_DisableListAddButtonBehaviour_4() const { return ___DisableListAddButtonBehaviour_4; }
	inline bool* get_address_of_DisableListAddButtonBehaviour_4() { return &___DisableListAddButtonBehaviour_4; }
	inline void set_DisableListAddButtonBehaviour_4(bool value)
	{
		___DisableListAddButtonBehaviour_4 = value;
	}

	inline static int32_t get_offset_of_ExcludeExistingValuesInList_5() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___ExcludeExistingValuesInList_5)); }
	inline bool get_ExcludeExistingValuesInList_5() const { return ___ExcludeExistingValuesInList_5; }
	inline bool* get_address_of_ExcludeExistingValuesInList_5() { return &___ExcludeExistingValuesInList_5; }
	inline void set_ExcludeExistingValuesInList_5(bool value)
	{
		___ExcludeExistingValuesInList_5 = value;
	}

	inline static int32_t get_offset_of_ExpandAllMenuItems_6() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___ExpandAllMenuItems_6)); }
	inline bool get_ExpandAllMenuItems_6() const { return ___ExpandAllMenuItems_6; }
	inline bool* get_address_of_ExpandAllMenuItems_6() { return &___ExpandAllMenuItems_6; }
	inline void set_ExpandAllMenuItems_6(bool value)
	{
		___ExpandAllMenuItems_6 = value;
	}

	inline static int32_t get_offset_of_AppendNextDrawer_7() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___AppendNextDrawer_7)); }
	inline bool get_AppendNextDrawer_7() const { return ___AppendNextDrawer_7; }
	inline bool* get_address_of_AppendNextDrawer_7() { return &___AppendNextDrawer_7; }
	inline void set_AppendNextDrawer_7(bool value)
	{
		___AppendNextDrawer_7 = value;
	}

	inline static int32_t get_offset_of_DisableGUIInAppendedDrawer_8() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___DisableGUIInAppendedDrawer_8)); }
	inline bool get_DisableGUIInAppendedDrawer_8() const { return ___DisableGUIInAppendedDrawer_8; }
	inline bool* get_address_of_DisableGUIInAppendedDrawer_8() { return &___DisableGUIInAppendedDrawer_8; }
	inline void set_DisableGUIInAppendedDrawer_8(bool value)
	{
		___DisableGUIInAppendedDrawer_8 = value;
	}

	inline static int32_t get_offset_of_DoubleClickToConfirm_9() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___DoubleClickToConfirm_9)); }
	inline bool get_DoubleClickToConfirm_9() const { return ___DoubleClickToConfirm_9; }
	inline bool* get_address_of_DoubleClickToConfirm_9() { return &___DoubleClickToConfirm_9; }
	inline void set_DoubleClickToConfirm_9(bool value)
	{
		___DoubleClickToConfirm_9 = value;
	}

	inline static int32_t get_offset_of_FlattenTreeView_10() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___FlattenTreeView_10)); }
	inline bool get_FlattenTreeView_10() const { return ___FlattenTreeView_10; }
	inline bool* get_address_of_FlattenTreeView_10() { return &___FlattenTreeView_10; }
	inline void set_FlattenTreeView_10(bool value)
	{
		___FlattenTreeView_10 = value;
	}

	inline static int32_t get_offset_of_DropdownWidth_11() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___DropdownWidth_11)); }
	inline int32_t get_DropdownWidth_11() const { return ___DropdownWidth_11; }
	inline int32_t* get_address_of_DropdownWidth_11() { return &___DropdownWidth_11; }
	inline void set_DropdownWidth_11(int32_t value)
	{
		___DropdownWidth_11 = value;
	}

	inline static int32_t get_offset_of_DropdownHeight_12() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___DropdownHeight_12)); }
	inline int32_t get_DropdownHeight_12() const { return ___DropdownHeight_12; }
	inline int32_t* get_address_of_DropdownHeight_12() { return &___DropdownHeight_12; }
	inline void set_DropdownHeight_12(int32_t value)
	{
		___DropdownHeight_12 = value;
	}

	inline static int32_t get_offset_of_DropdownTitle_13() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___DropdownTitle_13)); }
	inline String_t* get_DropdownTitle_13() const { return ___DropdownTitle_13; }
	inline String_t** get_address_of_DropdownTitle_13() { return &___DropdownTitle_13; }
	inline void set_DropdownTitle_13(String_t* value)
	{
		___DropdownTitle_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DropdownTitle_13), (void*)value);
	}

	inline static int32_t get_offset_of_SortDropdownItems_14() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___SortDropdownItems_14)); }
	inline bool get_SortDropdownItems_14() const { return ___SortDropdownItems_14; }
	inline bool* get_address_of_SortDropdownItems_14() { return &___SortDropdownItems_14; }
	inline void set_SortDropdownItems_14(bool value)
	{
		___SortDropdownItems_14 = value;
	}

	inline static int32_t get_offset_of_HideChildProperties_15() { return static_cast<int32_t>(offsetof(ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671, ___HideChildProperties_15)); }
	inline bool get_HideChildProperties_15() const { return ___HideChildProperties_15; }
	inline bool* get_address_of_HideChildProperties_15() { return &___HideChildProperties_15; }
	inline void set_HideChildProperties_15(bool value)
	{
		___HideChildProperties_15 = value;
	}
};


// Sirenix.OdinInspector.ValueDropdownItem
struct  ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E 
{
public:
	// System.String Sirenix.OdinInspector.ValueDropdownItem::Text
	String_t* ___Text_0;
	// System.Object Sirenix.OdinInspector.ValueDropdownItem::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text_0), (void*)value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Sirenix.OdinInspector.ValueDropdownItem
struct ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshaled_pinvoke
{
	char* ___Text_0;
	Il2CppIUnknown* ___Value_1;
};
// Native definition for COM marshalling of Sirenix.OdinInspector.ValueDropdownItem
struct ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshaled_com
{
	Il2CppChar* ___Text_0;
	Il2CppIUnknown* ___Value_1;
};

// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Sirenix.OdinInspector.WrapAttribute
struct  WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Double Sirenix.OdinInspector.WrapAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.WrapAttribute::Max
	double ___Max_1;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}
};


// System.AttributeTargets
struct  AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.BoxGroupAttribute
struct  BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:
	// System.Boolean Sirenix.OdinInspector.BoxGroupAttribute::ShowLabel
	bool ___ShowLabel_3;
	// System.Boolean Sirenix.OdinInspector.BoxGroupAttribute::CenterLabel
	bool ___CenterLabel_4;

public:
	inline static int32_t get_offset_of_ShowLabel_3() { return static_cast<int32_t>(offsetof(BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C, ___ShowLabel_3)); }
	inline bool get_ShowLabel_3() const { return ___ShowLabel_3; }
	inline bool* get_address_of_ShowLabel_3() { return &___ShowLabel_3; }
	inline void set_ShowLabel_3(bool value)
	{
		___ShowLabel_3 = value;
	}

	inline static int32_t get_offset_of_CenterLabel_4() { return static_cast<int32_t>(offsetof(BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C, ___CenterLabel_4)); }
	inline bool get_CenterLabel_4() const { return ___CenterLabel_4; }
	inline bool* get_address_of_CenterLabel_4() { return &___CenterLabel_4; }
	inline void set_CenterLabel_4(bool value)
	{
		___CenterLabel_4 = value;
	}
};


// Sirenix.OdinInspector.ButtonGroupAttribute
struct  ButtonGroupAttribute_t20631D3282E17CD252851E3A40703BA4F5EB4A86  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:

public:
};


// Sirenix.OdinInspector.ButtonSizes
struct  ButtonSizes_tAE6FC6D828BF464A0793BD9C531FBA911B11CA68 
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonSizes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonSizes_tAE6FC6D828BF464A0793BD9C531FBA911B11CA68, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.ButtonStyle
struct  ButtonStyle_tD45E72B92484EF544CCC92C9DC8A43F909035902 
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStyle_tD45E72B92484EF544CCC92C9DC8A43F909035902, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// Sirenix.OdinInspector.DictionaryDisplayOptions
struct  DictionaryDisplayOptions_t60D174EBC2E1FAB06D0768D12F9CCA011782C690 
{
public:
	// System.Int32 Sirenix.OdinInspector.DictionaryDisplayOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DictionaryDisplayOptions_t60D174EBC2E1FAB06D0768D12F9CCA011782C690, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// Sirenix.OdinInspector.FoldoutGroupAttribute
struct  FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:
	// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::Expanded
	bool ___Expanded_3;
	// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::<HasDefinedExpanded>k__BackingField
	bool ___U3CHasDefinedExpandedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_Expanded_3() { return static_cast<int32_t>(offsetof(FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D, ___Expanded_3)); }
	inline bool get_Expanded_3() const { return ___Expanded_3; }
	inline bool* get_address_of_Expanded_3() { return &___Expanded_3; }
	inline void set_Expanded_3(bool value)
	{
		___Expanded_3 = value;
	}

	inline static int32_t get_offset_of_U3CHasDefinedExpandedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D, ___U3CHasDefinedExpandedU3Ek__BackingField_4)); }
	inline bool get_U3CHasDefinedExpandedU3Ek__BackingField_4() const { return ___U3CHasDefinedExpandedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CHasDefinedExpandedU3Ek__BackingField_4() { return &___U3CHasDefinedExpandedU3Ek__BackingField_4; }
	inline void set_U3CHasDefinedExpandedU3Ek__BackingField_4(bool value)
	{
		___U3CHasDefinedExpandedU3Ek__BackingField_4 = value;
	}
};


// Sirenix.OdinInspector.GUIColorAttribute
struct  GUIColorAttribute_t7E1FEBC8732A8232FE82C68019C98709FD244DB4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// UnityEngine.Color Sirenix.OdinInspector.GUIColorAttribute::Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Color_0;
	// System.String Sirenix.OdinInspector.GUIColorAttribute::GetColor
	String_t* ___GetColor_1;

public:
	inline static int32_t get_offset_of_Color_0() { return static_cast<int32_t>(offsetof(GUIColorAttribute_t7E1FEBC8732A8232FE82C68019C98709FD244DB4, ___Color_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Color_0() const { return ___Color_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Color_0() { return &___Color_0; }
	inline void set_Color_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Color_0 = value;
	}

	inline static int32_t get_offset_of_GetColor_1() { return static_cast<int32_t>(offsetof(GUIColorAttribute_t7E1FEBC8732A8232FE82C68019C98709FD244DB4, ___GetColor_1)); }
	inline String_t* get_GetColor_1() const { return ___GetColor_1; }
	inline String_t** get_address_of_GetColor_1() { return &___GetColor_1; }
	inline void set_GetColor_1(String_t* value)
	{
		___GetColor_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GetColor_1), (void*)value);
	}
};


// Sirenix.OdinInspector.HideIfGroupAttribute
struct  HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:
	// System.String Sirenix.OdinInspector.HideIfGroupAttribute::memberName
	String_t* ___memberName_3;
	// System.Boolean Sirenix.OdinInspector.HideIfGroupAttribute::Animate
	bool ___Animate_4;
	// System.Object Sirenix.OdinInspector.HideIfGroupAttribute::Value
	RuntimeObject * ___Value_5;

public:
	inline static int32_t get_offset_of_memberName_3() { return static_cast<int32_t>(offsetof(HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4, ___memberName_3)); }
	inline String_t* get_memberName_3() const { return ___memberName_3; }
	inline String_t** get_address_of_memberName_3() { return &___memberName_3; }
	inline void set_memberName_3(String_t* value)
	{
		___memberName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___memberName_3), (void*)value);
	}

	inline static int32_t get_offset_of_Animate_4() { return static_cast<int32_t>(offsetof(HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4, ___Animate_4)); }
	inline bool get_Animate_4() const { return ___Animate_4; }
	inline bool* get_address_of_Animate_4() { return &___Animate_4; }
	inline void set_Animate_4(bool value)
	{
		___Animate_4 = value;
	}

	inline static int32_t get_offset_of_Value_5() { return static_cast<int32_t>(offsetof(HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4, ___Value_5)); }
	inline RuntimeObject * get_Value_5() const { return ___Value_5; }
	inline RuntimeObject ** get_address_of_Value_5() { return &___Value_5; }
	inline void set_Value_5(RuntimeObject * value)
	{
		___Value_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_5), (void*)value);
	}
};


// Sirenix.OdinInspector.HorizontalGroupAttribute
struct  HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::Width
	float ___Width_3;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MarginLeft
	float ___MarginLeft_4;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MarginRight
	float ___MarginRight_5;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::PaddingLeft
	float ___PaddingLeft_6;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::PaddingRight
	float ___PaddingRight_7;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MinWidth
	float ___MinWidth_8;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::MaxWidth
	float ___MaxWidth_9;
	// System.String Sirenix.OdinInspector.HorizontalGroupAttribute::Title
	String_t* ___Title_10;
	// System.Single Sirenix.OdinInspector.HorizontalGroupAttribute::LabelWidth
	float ___LabelWidth_11;

public:
	inline static int32_t get_offset_of_Width_3() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3, ___Width_3)); }
	inline float get_Width_3() const { return ___Width_3; }
	inline float* get_address_of_Width_3() { return &___Width_3; }
	inline void set_Width_3(float value)
	{
		___Width_3 = value;
	}

	inline static int32_t get_offset_of_MarginLeft_4() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3, ___MarginLeft_4)); }
	inline float get_MarginLeft_4() const { return ___MarginLeft_4; }
	inline float* get_address_of_MarginLeft_4() { return &___MarginLeft_4; }
	inline void set_MarginLeft_4(float value)
	{
		___MarginLeft_4 = value;
	}

	inline static int32_t get_offset_of_MarginRight_5() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3, ___MarginRight_5)); }
	inline float get_MarginRight_5() const { return ___MarginRight_5; }
	inline float* get_address_of_MarginRight_5() { return &___MarginRight_5; }
	inline void set_MarginRight_5(float value)
	{
		___MarginRight_5 = value;
	}

	inline static int32_t get_offset_of_PaddingLeft_6() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3, ___PaddingLeft_6)); }
	inline float get_PaddingLeft_6() const { return ___PaddingLeft_6; }
	inline float* get_address_of_PaddingLeft_6() { return &___PaddingLeft_6; }
	inline void set_PaddingLeft_6(float value)
	{
		___PaddingLeft_6 = value;
	}

	inline static int32_t get_offset_of_PaddingRight_7() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3, ___PaddingRight_7)); }
	inline float get_PaddingRight_7() const { return ___PaddingRight_7; }
	inline float* get_address_of_PaddingRight_7() { return &___PaddingRight_7; }
	inline void set_PaddingRight_7(float value)
	{
		___PaddingRight_7 = value;
	}

	inline static int32_t get_offset_of_MinWidth_8() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3, ___MinWidth_8)); }
	inline float get_MinWidth_8() const { return ___MinWidth_8; }
	inline float* get_address_of_MinWidth_8() { return &___MinWidth_8; }
	inline void set_MinWidth_8(float value)
	{
		___MinWidth_8 = value;
	}

	inline static int32_t get_offset_of_MaxWidth_9() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3, ___MaxWidth_9)); }
	inline float get_MaxWidth_9() const { return ___MaxWidth_9; }
	inline float* get_address_of_MaxWidth_9() { return &___MaxWidth_9; }
	inline void set_MaxWidth_9(float value)
	{
		___MaxWidth_9 = value;
	}

	inline static int32_t get_offset_of_Title_10() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3, ___Title_10)); }
	inline String_t* get_Title_10() const { return ___Title_10; }
	inline String_t** get_address_of_Title_10() { return &___Title_10; }
	inline void set_Title_10(String_t* value)
	{
		___Title_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Title_10), (void*)value);
	}

	inline static int32_t get_offset_of_LabelWidth_11() { return static_cast<int32_t>(offsetof(HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3, ___LabelWidth_11)); }
	inline float get_LabelWidth_11() const { return ___LabelWidth_11; }
	inline float* get_address_of_LabelWidth_11() { return &___LabelWidth_11; }
	inline void set_LabelWidth_11(float value)
	{
		___LabelWidth_11 = value;
	}
};


// Sirenix.OdinInspector.InfoMessageType
struct  InfoMessageType_tD9E2C7E48DC121059E99FAE4521AFD17BAA5AD16 
{
public:
	// System.Int32 Sirenix.OdinInspector.InfoMessageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InfoMessageType_tD9E2C7E48DC121059E99FAE4521AFD17BAA5AD16, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.InlineEditorModes
struct  InlineEditorModes_tA3375520732979F67E155488E79989790885A142 
{
public:
	// System.Int32 Sirenix.OdinInspector.InlineEditorModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InlineEditorModes_tA3375520732979F67E155488E79989790885A142, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.InlineEditorObjectFieldModes
struct  InlineEditorObjectFieldModes_t65FABDED2B38164F51EE43C600EC66D196C4459B 
{
public:
	// System.Int32 Sirenix.OdinInspector.InlineEditorObjectFieldModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InlineEditorObjectFieldModes_t65FABDED2B38164F51EE43C600EC66D196C4459B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.ObjectFieldAlignment
struct  ObjectFieldAlignment_t443EF71509B7A1B73D14570FA291F36700509CC5 
{
public:
	// System.Int32 Sirenix.OdinInspector.ObjectFieldAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectFieldAlignment_t443EF71509B7A1B73D14570FA291F36700509CC5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.OnInspectorGUIAttribute
struct  OnInspectorGUIAttribute_t1CCCB842BCEBEB2AB07C1298B7FFBAB7571B0F2F  : public ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41
{
public:
	// System.String Sirenix.OdinInspector.OnInspectorGUIAttribute::PrependMethodName
	String_t* ___PrependMethodName_0;
	// System.String Sirenix.OdinInspector.OnInspectorGUIAttribute::AppendMethodName
	String_t* ___AppendMethodName_1;

public:
	inline static int32_t get_offset_of_PrependMethodName_0() { return static_cast<int32_t>(offsetof(OnInspectorGUIAttribute_t1CCCB842BCEBEB2AB07C1298B7FFBAB7571B0F2F, ___PrependMethodName_0)); }
	inline String_t* get_PrependMethodName_0() const { return ___PrependMethodName_0; }
	inline String_t** get_address_of_PrependMethodName_0() { return &___PrependMethodName_0; }
	inline void set_PrependMethodName_0(String_t* value)
	{
		___PrependMethodName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PrependMethodName_0), (void*)value);
	}

	inline static int32_t get_offset_of_AppendMethodName_1() { return static_cast<int32_t>(offsetof(OnInspectorGUIAttribute_t1CCCB842BCEBEB2AB07C1298B7FFBAB7571B0F2F, ___AppendMethodName_1)); }
	inline String_t* get_AppendMethodName_1() const { return ___AppendMethodName_1; }
	inline String_t** get_address_of_AppendMethodName_1() { return &___AppendMethodName_1; }
	inline void set_AppendMethodName_1(String_t* value)
	{
		___AppendMethodName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppendMethodName_1), (void*)value);
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// Sirenix.OdinInspector.ShowIfGroupAttribute
struct  ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:
	// System.String Sirenix.OdinInspector.ShowIfGroupAttribute::memberName
	String_t* ___memberName_3;
	// System.Boolean Sirenix.OdinInspector.ShowIfGroupAttribute::Animate
	bool ___Animate_4;
	// System.Object Sirenix.OdinInspector.ShowIfGroupAttribute::Value
	RuntimeObject * ___Value_5;

public:
	inline static int32_t get_offset_of_memberName_3() { return static_cast<int32_t>(offsetof(ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34, ___memberName_3)); }
	inline String_t* get_memberName_3() const { return ___memberName_3; }
	inline String_t** get_address_of_memberName_3() { return &___memberName_3; }
	inline void set_memberName_3(String_t* value)
	{
		___memberName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___memberName_3), (void*)value);
	}

	inline static int32_t get_offset_of_Animate_4() { return static_cast<int32_t>(offsetof(ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34, ___Animate_4)); }
	inline bool get_Animate_4() const { return ___Animate_4; }
	inline bool* get_address_of_Animate_4() { return &___Animate_4; }
	inline void set_Animate_4(bool value)
	{
		___Animate_4 = value;
	}

	inline static int32_t get_offset_of_Value_5() { return static_cast<int32_t>(offsetof(ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34, ___Value_5)); }
	inline RuntimeObject * get_Value_5() const { return ___Value_5; }
	inline RuntimeObject ** get_address_of_Value_5() { return &___Value_5; }
	inline void set_Value_5(RuntimeObject * value)
	{
		___Value_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_5), (void*)value);
	}
};


// Sirenix.OdinInspector.TabGroupAttribute
struct  TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:
	// System.String Sirenix.OdinInspector.TabGroupAttribute::TabName
	String_t* ___TabName_4;
	// System.Boolean Sirenix.OdinInspector.TabGroupAttribute::UseFixedHeight
	bool ___UseFixedHeight_5;
	// System.Boolean Sirenix.OdinInspector.TabGroupAttribute::Paddingless
	bool ___Paddingless_6;
	// System.Boolean Sirenix.OdinInspector.TabGroupAttribute::HideTabGroupIfTabGroupOnlyHasOneTab
	bool ___HideTabGroupIfTabGroupOnlyHasOneTab_7;
	// System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::<Tabs>k__BackingField
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___U3CTabsU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_TabName_4() { return static_cast<int32_t>(offsetof(TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D, ___TabName_4)); }
	inline String_t* get_TabName_4() const { return ___TabName_4; }
	inline String_t** get_address_of_TabName_4() { return &___TabName_4; }
	inline void set_TabName_4(String_t* value)
	{
		___TabName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TabName_4), (void*)value);
	}

	inline static int32_t get_offset_of_UseFixedHeight_5() { return static_cast<int32_t>(offsetof(TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D, ___UseFixedHeight_5)); }
	inline bool get_UseFixedHeight_5() const { return ___UseFixedHeight_5; }
	inline bool* get_address_of_UseFixedHeight_5() { return &___UseFixedHeight_5; }
	inline void set_UseFixedHeight_5(bool value)
	{
		___UseFixedHeight_5 = value;
	}

	inline static int32_t get_offset_of_Paddingless_6() { return static_cast<int32_t>(offsetof(TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D, ___Paddingless_6)); }
	inline bool get_Paddingless_6() const { return ___Paddingless_6; }
	inline bool* get_address_of_Paddingless_6() { return &___Paddingless_6; }
	inline void set_Paddingless_6(bool value)
	{
		___Paddingless_6 = value;
	}

	inline static int32_t get_offset_of_HideTabGroupIfTabGroupOnlyHasOneTab_7() { return static_cast<int32_t>(offsetof(TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D, ___HideTabGroupIfTabGroupOnlyHasOneTab_7)); }
	inline bool get_HideTabGroupIfTabGroupOnlyHasOneTab_7() const { return ___HideTabGroupIfTabGroupOnlyHasOneTab_7; }
	inline bool* get_address_of_HideTabGroupIfTabGroupOnlyHasOneTab_7() { return &___HideTabGroupIfTabGroupOnlyHasOneTab_7; }
	inline void set_HideTabGroupIfTabGroupOnlyHasOneTab_7(bool value)
	{
		___HideTabGroupIfTabGroupOnlyHasOneTab_7 = value;
	}

	inline static int32_t get_offset_of_U3CTabsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D, ___U3CTabsU3Ek__BackingField_8)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_U3CTabsU3Ek__BackingField_8() const { return ___U3CTabsU3Ek__BackingField_8; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_U3CTabsU3Ek__BackingField_8() { return &___U3CTabsU3Ek__BackingField_8; }
	inline void set_U3CTabsU3Ek__BackingField_8(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___U3CTabsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTabsU3Ek__BackingField_8), (void*)value);
	}
};


// UnityEngine.TextAlignment
struct  TextAlignment_t23BC9C82BA84BEC0BFE7426C26FE96C7DE0EF8F4 
{
public:
	// System.Int32 UnityEngine.TextAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignment_t23BC9C82BA84BEC0BFE7426C26FE96C7DE0EF8F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.TitleAlignments
struct  TitleAlignments_tE15A4C1B6B91AEF6991EF08428A4A27416CB517D 
{
public:
	// System.Int32 Sirenix.OdinInspector.TitleAlignments::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TitleAlignments_tE15A4C1B6B91AEF6991EF08428A4A27416CB517D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Sirenix.OdinInspector.ToggleGroupAttribute
struct  ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:
	// System.String Sirenix.OdinInspector.ToggleGroupAttribute::ToggleGroupTitle
	String_t* ___ToggleGroupTitle_3;
	// System.Boolean Sirenix.OdinInspector.ToggleGroupAttribute::CollapseOthersOnExpand
	bool ___CollapseOthersOnExpand_4;
	// System.String Sirenix.OdinInspector.ToggleGroupAttribute::<TitleStringMemberName>k__BackingField
	String_t* ___U3CTitleStringMemberNameU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_ToggleGroupTitle_3() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00, ___ToggleGroupTitle_3)); }
	inline String_t* get_ToggleGroupTitle_3() const { return ___ToggleGroupTitle_3; }
	inline String_t** get_address_of_ToggleGroupTitle_3() { return &___ToggleGroupTitle_3; }
	inline void set_ToggleGroupTitle_3(String_t* value)
	{
		___ToggleGroupTitle_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ToggleGroupTitle_3), (void*)value);
	}

	inline static int32_t get_offset_of_CollapseOthersOnExpand_4() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00, ___CollapseOthersOnExpand_4)); }
	inline bool get_CollapseOthersOnExpand_4() const { return ___CollapseOthersOnExpand_4; }
	inline bool* get_address_of_CollapseOthersOnExpand_4() { return &___CollapseOthersOnExpand_4; }
	inline void set_CollapseOthersOnExpand_4(bool value)
	{
		___CollapseOthersOnExpand_4 = value;
	}

	inline static int32_t get_offset_of_U3CTitleStringMemberNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00, ___U3CTitleStringMemberNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CTitleStringMemberNameU3Ek__BackingField_5() const { return ___U3CTitleStringMemberNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CTitleStringMemberNameU3Ek__BackingField_5() { return &___U3CTitleStringMemberNameU3Ek__BackingField_5; }
	inline void set_U3CTitleStringMemberNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CTitleStringMemberNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTitleStringMemberNameU3Ek__BackingField_5), (void*)value);
	}
};


// Sirenix.OdinInspector.VerticalGroupAttribute
struct  VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:
	// System.Single Sirenix.OdinInspector.VerticalGroupAttribute::PaddingTop
	float ___PaddingTop_3;
	// System.Single Sirenix.OdinInspector.VerticalGroupAttribute::PaddingBottom
	float ___PaddingBottom_4;

public:
	inline static int32_t get_offset_of_PaddingTop_3() { return static_cast<int32_t>(offsetof(VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF, ___PaddingTop_3)); }
	inline float get_PaddingTop_3() const { return ___PaddingTop_3; }
	inline float* get_address_of_PaddingTop_3() { return &___PaddingTop_3; }
	inline void set_PaddingTop_3(float value)
	{
		___PaddingTop_3 = value;
	}

	inline static int32_t get_offset_of_PaddingBottom_4() { return static_cast<int32_t>(offsetof(VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF, ___PaddingBottom_4)); }
	inline float get_PaddingBottom_4() const { return ___PaddingBottom_4; }
	inline float* get_address_of_PaddingBottom_4() { return &___PaddingBottom_4; }
	inline void set_PaddingBottom_4(float value)
	{
		___PaddingBottom_4 = value;
	}
};


// Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute
struct  TabSubGroupAttribute_tDEC43BA5824DF3FC46C494AC92DD32CD26D8B22C  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:

public:
};


// Sirenix.OdinInspector.AttributeTargetFlags
struct  AttributeTargetFlags_tBFAA02311C5AA4BEF0265AC9AF9BAC4F90FC556B  : public RuntimeObject
{
public:

public:
};


// Sirenix.OdinInspector.ButtonAttribute
struct  ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782  : public ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41
{
public:
	// System.Int32 Sirenix.OdinInspector.ButtonAttribute::ButtonHeight
	int32_t ___ButtonHeight_0;
	// System.String Sirenix.OdinInspector.ButtonAttribute::Name
	String_t* ___Name_1;
	// Sirenix.OdinInspector.ButtonStyle Sirenix.OdinInspector.ButtonAttribute::Style
	int32_t ___Style_2;
	// System.Boolean Sirenix.OdinInspector.ButtonAttribute::Expanded
	bool ___Expanded_3;

public:
	inline static int32_t get_offset_of_ButtonHeight_0() { return static_cast<int32_t>(offsetof(ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782, ___ButtonHeight_0)); }
	inline int32_t get_ButtonHeight_0() const { return ___ButtonHeight_0; }
	inline int32_t* get_address_of_ButtonHeight_0() { return &___ButtonHeight_0; }
	inline void set_ButtonHeight_0(int32_t value)
	{
		___ButtonHeight_0 = value;
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_1), (void*)value);
	}

	inline static int32_t get_offset_of_Style_2() { return static_cast<int32_t>(offsetof(ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782, ___Style_2)); }
	inline int32_t get_Style_2() const { return ___Style_2; }
	inline int32_t* get_address_of_Style_2() { return &___Style_2; }
	inline void set_Style_2(int32_t value)
	{
		___Style_2 = value;
	}

	inline static int32_t get_offset_of_Expanded_3() { return static_cast<int32_t>(offsetof(ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782, ___Expanded_3)); }
	inline bool get_Expanded_3() const { return ___Expanded_3; }
	inline bool* get_address_of_Expanded_3() { return &___Expanded_3; }
	inline void set_Expanded_3(bool value)
	{
		___Expanded_3 = value;
	}
};


// Sirenix.OdinInspector.DetailedInfoBoxAttribute
struct  DetailedInfoBoxAttribute_t8CEBCE93D61675CB38CA659984F4C121381C565E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::Message
	String_t* ___Message_0;
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::Details
	String_t* ___Details_1;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.DetailedInfoBoxAttribute::InfoMessageType
	int32_t ___InfoMessageType_2;
	// System.String Sirenix.OdinInspector.DetailedInfoBoxAttribute::VisibleIf
	String_t* ___VisibleIf_3;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t8CEBCE93D61675CB38CA659984F4C121381C565E, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Message_0), (void*)value);
	}

	inline static int32_t get_offset_of_Details_1() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t8CEBCE93D61675CB38CA659984F4C121381C565E, ___Details_1)); }
	inline String_t* get_Details_1() const { return ___Details_1; }
	inline String_t** get_address_of_Details_1() { return &___Details_1; }
	inline void set_Details_1(String_t* value)
	{
		___Details_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Details_1), (void*)value);
	}

	inline static int32_t get_offset_of_InfoMessageType_2() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t8CEBCE93D61675CB38CA659984F4C121381C565E, ___InfoMessageType_2)); }
	inline int32_t get_InfoMessageType_2() const { return ___InfoMessageType_2; }
	inline int32_t* get_address_of_InfoMessageType_2() { return &___InfoMessageType_2; }
	inline void set_InfoMessageType_2(int32_t value)
	{
		___InfoMessageType_2 = value;
	}

	inline static int32_t get_offset_of_VisibleIf_3() { return static_cast<int32_t>(offsetof(DetailedInfoBoxAttribute_t8CEBCE93D61675CB38CA659984F4C121381C565E, ___VisibleIf_3)); }
	inline String_t* get_VisibleIf_3() const { return ___VisibleIf_3; }
	inline String_t** get_address_of_VisibleIf_3() { return &___VisibleIf_3; }
	inline void set_VisibleIf_3(String_t* value)
	{
		___VisibleIf_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VisibleIf_3), (void*)value);
	}
};


// Sirenix.OdinInspector.DictionaryDrawerSettings
struct  DictionaryDrawerSettings_tD59605DC70F69FF09FC833CCC887105A966463F7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.DictionaryDrawerSettings::KeyLabel
	String_t* ___KeyLabel_0;
	// System.String Sirenix.OdinInspector.DictionaryDrawerSettings::ValueLabel
	String_t* ___ValueLabel_1;
	// Sirenix.OdinInspector.DictionaryDisplayOptions Sirenix.OdinInspector.DictionaryDrawerSettings::DisplayMode
	int32_t ___DisplayMode_2;
	// System.Boolean Sirenix.OdinInspector.DictionaryDrawerSettings::IsReadOnly
	bool ___IsReadOnly_3;

public:
	inline static int32_t get_offset_of_KeyLabel_0() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_tD59605DC70F69FF09FC833CCC887105A966463F7, ___KeyLabel_0)); }
	inline String_t* get_KeyLabel_0() const { return ___KeyLabel_0; }
	inline String_t** get_address_of_KeyLabel_0() { return &___KeyLabel_0; }
	inline void set_KeyLabel_0(String_t* value)
	{
		___KeyLabel_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___KeyLabel_0), (void*)value);
	}

	inline static int32_t get_offset_of_ValueLabel_1() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_tD59605DC70F69FF09FC833CCC887105A966463F7, ___ValueLabel_1)); }
	inline String_t* get_ValueLabel_1() const { return ___ValueLabel_1; }
	inline String_t** get_address_of_ValueLabel_1() { return &___ValueLabel_1; }
	inline void set_ValueLabel_1(String_t* value)
	{
		___ValueLabel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ValueLabel_1), (void*)value);
	}

	inline static int32_t get_offset_of_DisplayMode_2() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_tD59605DC70F69FF09FC833CCC887105A966463F7, ___DisplayMode_2)); }
	inline int32_t get_DisplayMode_2() const { return ___DisplayMode_2; }
	inline int32_t* get_address_of_DisplayMode_2() { return &___DisplayMode_2; }
	inline void set_DisplayMode_2(int32_t value)
	{
		___DisplayMode_2 = value;
	}

	inline static int32_t get_offset_of_IsReadOnly_3() { return static_cast<int32_t>(offsetof(DictionaryDrawerSettings_tD59605DC70F69FF09FC833CCC887105A966463F7, ___IsReadOnly_3)); }
	inline bool get_IsReadOnly_3() const { return ___IsReadOnly_3; }
	inline bool* get_address_of_IsReadOnly_3() { return &___IsReadOnly_3; }
	inline void set_IsReadOnly_3(bool value)
	{
		___IsReadOnly_3 = value;
	}
};


// Sirenix.OdinInspector.InfoBoxAttribute
struct  InfoBoxAttribute_t8D2A3D9022D85EC4424CAB78C71C24C8BF8AEFD5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.InfoBoxAttribute::Message
	String_t* ___Message_0;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.InfoBoxAttribute::InfoMessageType
	int32_t ___InfoMessageType_1;
	// System.String Sirenix.OdinInspector.InfoBoxAttribute::VisibleIf
	String_t* ___VisibleIf_2;
	// System.Boolean Sirenix.OdinInspector.InfoBoxAttribute::GUIAlwaysEnabled
	bool ___GUIAlwaysEnabled_3;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t8D2A3D9022D85EC4424CAB78C71C24C8BF8AEFD5, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Message_0), (void*)value);
	}

	inline static int32_t get_offset_of_InfoMessageType_1() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t8D2A3D9022D85EC4424CAB78C71C24C8BF8AEFD5, ___InfoMessageType_1)); }
	inline int32_t get_InfoMessageType_1() const { return ___InfoMessageType_1; }
	inline int32_t* get_address_of_InfoMessageType_1() { return &___InfoMessageType_1; }
	inline void set_InfoMessageType_1(int32_t value)
	{
		___InfoMessageType_1 = value;
	}

	inline static int32_t get_offset_of_VisibleIf_2() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t8D2A3D9022D85EC4424CAB78C71C24C8BF8AEFD5, ___VisibleIf_2)); }
	inline String_t* get_VisibleIf_2() const { return ___VisibleIf_2; }
	inline String_t** get_address_of_VisibleIf_2() { return &___VisibleIf_2; }
	inline void set_VisibleIf_2(String_t* value)
	{
		___VisibleIf_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VisibleIf_2), (void*)value);
	}

	inline static int32_t get_offset_of_GUIAlwaysEnabled_3() { return static_cast<int32_t>(offsetof(InfoBoxAttribute_t8D2A3D9022D85EC4424CAB78C71C24C8BF8AEFD5, ___GUIAlwaysEnabled_3)); }
	inline bool get_GUIAlwaysEnabled_3() const { return ___GUIAlwaysEnabled_3; }
	inline bool* get_address_of_GUIAlwaysEnabled_3() { return &___GUIAlwaysEnabled_3; }
	inline void set_GUIAlwaysEnabled_3(bool value)
	{
		___GUIAlwaysEnabled_3 = value;
	}
};


// Sirenix.OdinInspector.InlineEditorAttribute
struct  InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::Expanded
	bool ___Expanded_0;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawHeader
	bool ___DrawHeader_1;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawGUI
	bool ___DrawGUI_2;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::DrawPreview
	bool ___DrawPreview_3;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::MaxHeight
	float ___MaxHeight_4;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::PreviewWidth
	float ___PreviewWidth_5;
	// System.Single Sirenix.OdinInspector.InlineEditorAttribute::PreviewHeight
	float ___PreviewHeight_6;
	// System.Boolean Sirenix.OdinInspector.InlineEditorAttribute::IncrementInlineEditorDrawerDepth
	bool ___IncrementInlineEditorDrawerDepth_7;
	// Sirenix.OdinInspector.InlineEditorObjectFieldModes Sirenix.OdinInspector.InlineEditorAttribute::ObjectFieldMode
	int32_t ___ObjectFieldMode_8;

public:
	inline static int32_t get_offset_of_Expanded_0() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76, ___Expanded_0)); }
	inline bool get_Expanded_0() const { return ___Expanded_0; }
	inline bool* get_address_of_Expanded_0() { return &___Expanded_0; }
	inline void set_Expanded_0(bool value)
	{
		___Expanded_0 = value;
	}

	inline static int32_t get_offset_of_DrawHeader_1() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76, ___DrawHeader_1)); }
	inline bool get_DrawHeader_1() const { return ___DrawHeader_1; }
	inline bool* get_address_of_DrawHeader_1() { return &___DrawHeader_1; }
	inline void set_DrawHeader_1(bool value)
	{
		___DrawHeader_1 = value;
	}

	inline static int32_t get_offset_of_DrawGUI_2() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76, ___DrawGUI_2)); }
	inline bool get_DrawGUI_2() const { return ___DrawGUI_2; }
	inline bool* get_address_of_DrawGUI_2() { return &___DrawGUI_2; }
	inline void set_DrawGUI_2(bool value)
	{
		___DrawGUI_2 = value;
	}

	inline static int32_t get_offset_of_DrawPreview_3() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76, ___DrawPreview_3)); }
	inline bool get_DrawPreview_3() const { return ___DrawPreview_3; }
	inline bool* get_address_of_DrawPreview_3() { return &___DrawPreview_3; }
	inline void set_DrawPreview_3(bool value)
	{
		___DrawPreview_3 = value;
	}

	inline static int32_t get_offset_of_MaxHeight_4() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76, ___MaxHeight_4)); }
	inline float get_MaxHeight_4() const { return ___MaxHeight_4; }
	inline float* get_address_of_MaxHeight_4() { return &___MaxHeight_4; }
	inline void set_MaxHeight_4(float value)
	{
		___MaxHeight_4 = value;
	}

	inline static int32_t get_offset_of_PreviewWidth_5() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76, ___PreviewWidth_5)); }
	inline float get_PreviewWidth_5() const { return ___PreviewWidth_5; }
	inline float* get_address_of_PreviewWidth_5() { return &___PreviewWidth_5; }
	inline void set_PreviewWidth_5(float value)
	{
		___PreviewWidth_5 = value;
	}

	inline static int32_t get_offset_of_PreviewHeight_6() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76, ___PreviewHeight_6)); }
	inline float get_PreviewHeight_6() const { return ___PreviewHeight_6; }
	inline float* get_address_of_PreviewHeight_6() { return &___PreviewHeight_6; }
	inline void set_PreviewHeight_6(float value)
	{
		___PreviewHeight_6 = value;
	}

	inline static int32_t get_offset_of_IncrementInlineEditorDrawerDepth_7() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76, ___IncrementInlineEditorDrawerDepth_7)); }
	inline bool get_IncrementInlineEditorDrawerDepth_7() const { return ___IncrementInlineEditorDrawerDepth_7; }
	inline bool* get_address_of_IncrementInlineEditorDrawerDepth_7() { return &___IncrementInlineEditorDrawerDepth_7; }
	inline void set_IncrementInlineEditorDrawerDepth_7(bool value)
	{
		___IncrementInlineEditorDrawerDepth_7 = value;
	}

	inline static int32_t get_offset_of_ObjectFieldMode_8() { return static_cast<int32_t>(offsetof(InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76, ___ObjectFieldMode_8)); }
	inline int32_t get_ObjectFieldMode_8() const { return ___ObjectFieldMode_8; }
	inline int32_t* get_address_of_ObjectFieldMode_8() { return &___ObjectFieldMode_8; }
	inline void set_ObjectFieldMode_8(int32_t value)
	{
		___ObjectFieldMode_8 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// Sirenix.OdinInspector.PreviewFieldAttribute
struct  PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Single Sirenix.OdinInspector.PreviewFieldAttribute::Height
	float ___Height_0;
	// Sirenix.OdinInspector.ObjectFieldAlignment Sirenix.OdinInspector.PreviewFieldAttribute::Alignment
	int32_t ___Alignment_1;
	// System.Boolean Sirenix.OdinInspector.PreviewFieldAttribute::AlignmentHasValue
	bool ___AlignmentHasValue_2;

public:
	inline static int32_t get_offset_of_Height_0() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4, ___Height_0)); }
	inline float get_Height_0() const { return ___Height_0; }
	inline float* get_address_of_Height_0() { return &___Height_0; }
	inline void set_Height_0(float value)
	{
		___Height_0 = value;
	}

	inline static int32_t get_offset_of_Alignment_1() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4, ___Alignment_1)); }
	inline int32_t get_Alignment_1() const { return ___Alignment_1; }
	inline int32_t* get_address_of_Alignment_1() { return &___Alignment_1; }
	inline void set_Alignment_1(int32_t value)
	{
		___Alignment_1 = value;
	}

	inline static int32_t get_offset_of_AlignmentHasValue_2() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4, ___AlignmentHasValue_2)); }
	inline bool get_AlignmentHasValue_2() const { return ___AlignmentHasValue_2; }
	inline bool* get_address_of_AlignmentHasValue_2() { return &___AlignmentHasValue_2; }
	inline void set_AlignmentHasValue_2(bool value)
	{
		___AlignmentHasValue_2 = value;
	}
};


// Sirenix.OdinInspector.ProgressBarAttribute
struct  ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Double Sirenix.OdinInspector.ProgressBarAttribute::Min
	double ___Min_0;
	// System.Double Sirenix.OdinInspector.ProgressBarAttribute::Max
	double ___Max_1;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::MinMember
	String_t* ___MinMember_2;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::MaxMember
	String_t* ___MaxMember_3;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::R
	float ___R_4;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::G
	float ___G_5;
	// System.Single Sirenix.OdinInspector.ProgressBarAttribute::B
	float ___B_6;
	// System.Int32 Sirenix.OdinInspector.ProgressBarAttribute::Height
	int32_t ___Height_7;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::ColorMember
	String_t* ___ColorMember_8;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::BackgroundColorMember
	String_t* ___BackgroundColorMember_9;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::Segmented
	bool ___Segmented_10;
	// System.String Sirenix.OdinInspector.ProgressBarAttribute::CustomValueStringMember
	String_t* ___CustomValueStringMember_11;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::drawValueLabel
	bool ___drawValueLabel_12;
	// UnityEngine.TextAlignment Sirenix.OdinInspector.ProgressBarAttribute::valueLabelAlignment
	int32_t ___valueLabelAlignment_13;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::<DrawValueLabelHasValue>k__BackingField
	bool ___U3CDrawValueLabelHasValueU3Ek__BackingField_14;
	// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::<ValueLabelAlignmentHasValue>k__BackingField
	bool ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___Min_0)); }
	inline double get_Min_0() const { return ___Min_0; }
	inline double* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(double value)
	{
		___Min_0 = value;
	}

	inline static int32_t get_offset_of_Max_1() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___Max_1)); }
	inline double get_Max_1() const { return ___Max_1; }
	inline double* get_address_of_Max_1() { return &___Max_1; }
	inline void set_Max_1(double value)
	{
		___Max_1 = value;
	}

	inline static int32_t get_offset_of_MinMember_2() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___MinMember_2)); }
	inline String_t* get_MinMember_2() const { return ___MinMember_2; }
	inline String_t** get_address_of_MinMember_2() { return &___MinMember_2; }
	inline void set_MinMember_2(String_t* value)
	{
		___MinMember_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MinMember_2), (void*)value);
	}

	inline static int32_t get_offset_of_MaxMember_3() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___MaxMember_3)); }
	inline String_t* get_MaxMember_3() const { return ___MaxMember_3; }
	inline String_t** get_address_of_MaxMember_3() { return &___MaxMember_3; }
	inline void set_MaxMember_3(String_t* value)
	{
		___MaxMember_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MaxMember_3), (void*)value);
	}

	inline static int32_t get_offset_of_R_4() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___R_4)); }
	inline float get_R_4() const { return ___R_4; }
	inline float* get_address_of_R_4() { return &___R_4; }
	inline void set_R_4(float value)
	{
		___R_4 = value;
	}

	inline static int32_t get_offset_of_G_5() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___G_5)); }
	inline float get_G_5() const { return ___G_5; }
	inline float* get_address_of_G_5() { return &___G_5; }
	inline void set_G_5(float value)
	{
		___G_5 = value;
	}

	inline static int32_t get_offset_of_B_6() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___B_6)); }
	inline float get_B_6() const { return ___B_6; }
	inline float* get_address_of_B_6() { return &___B_6; }
	inline void set_B_6(float value)
	{
		___B_6 = value;
	}

	inline static int32_t get_offset_of_Height_7() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___Height_7)); }
	inline int32_t get_Height_7() const { return ___Height_7; }
	inline int32_t* get_address_of_Height_7() { return &___Height_7; }
	inline void set_Height_7(int32_t value)
	{
		___Height_7 = value;
	}

	inline static int32_t get_offset_of_ColorMember_8() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___ColorMember_8)); }
	inline String_t* get_ColorMember_8() const { return ___ColorMember_8; }
	inline String_t** get_address_of_ColorMember_8() { return &___ColorMember_8; }
	inline void set_ColorMember_8(String_t* value)
	{
		___ColorMember_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ColorMember_8), (void*)value);
	}

	inline static int32_t get_offset_of_BackgroundColorMember_9() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___BackgroundColorMember_9)); }
	inline String_t* get_BackgroundColorMember_9() const { return ___BackgroundColorMember_9; }
	inline String_t** get_address_of_BackgroundColorMember_9() { return &___BackgroundColorMember_9; }
	inline void set_BackgroundColorMember_9(String_t* value)
	{
		___BackgroundColorMember_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BackgroundColorMember_9), (void*)value);
	}

	inline static int32_t get_offset_of_Segmented_10() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___Segmented_10)); }
	inline bool get_Segmented_10() const { return ___Segmented_10; }
	inline bool* get_address_of_Segmented_10() { return &___Segmented_10; }
	inline void set_Segmented_10(bool value)
	{
		___Segmented_10 = value;
	}

	inline static int32_t get_offset_of_CustomValueStringMember_11() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___CustomValueStringMember_11)); }
	inline String_t* get_CustomValueStringMember_11() const { return ___CustomValueStringMember_11; }
	inline String_t** get_address_of_CustomValueStringMember_11() { return &___CustomValueStringMember_11; }
	inline void set_CustomValueStringMember_11(String_t* value)
	{
		___CustomValueStringMember_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CustomValueStringMember_11), (void*)value);
	}

	inline static int32_t get_offset_of_drawValueLabel_12() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___drawValueLabel_12)); }
	inline bool get_drawValueLabel_12() const { return ___drawValueLabel_12; }
	inline bool* get_address_of_drawValueLabel_12() { return &___drawValueLabel_12; }
	inline void set_drawValueLabel_12(bool value)
	{
		___drawValueLabel_12 = value;
	}

	inline static int32_t get_offset_of_valueLabelAlignment_13() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___valueLabelAlignment_13)); }
	inline int32_t get_valueLabelAlignment_13() const { return ___valueLabelAlignment_13; }
	inline int32_t* get_address_of_valueLabelAlignment_13() { return &___valueLabelAlignment_13; }
	inline void set_valueLabelAlignment_13(int32_t value)
	{
		___valueLabelAlignment_13 = value;
	}

	inline static int32_t get_offset_of_U3CDrawValueLabelHasValueU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___U3CDrawValueLabelHasValueU3Ek__BackingField_14)); }
	inline bool get_U3CDrawValueLabelHasValueU3Ek__BackingField_14() const { return ___U3CDrawValueLabelHasValueU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CDrawValueLabelHasValueU3Ek__BackingField_14() { return &___U3CDrawValueLabelHasValueU3Ek__BackingField_14; }
	inline void set_U3CDrawValueLabelHasValueU3Ek__BackingField_14(bool value)
	{
		___U3CDrawValueLabelHasValueU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F, ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15)); }
	inline bool get_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() const { return ___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15() { return &___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15; }
	inline void set_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15(bool value)
	{
		___U3CValueLabelAlignmentHasValueU3Ek__BackingField_15 = value;
	}
};


// Sirenix.OdinInspector.RequiredAttribute
struct  RequiredAttribute_t0941212FA1DC5F51F3309F0EB27016EE914366D4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.RequiredAttribute::ErrorMessage
	String_t* ___ErrorMessage_0;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.RequiredAttribute::MessageType
	int32_t ___MessageType_1;

public:
	inline static int32_t get_offset_of_ErrorMessage_0() { return static_cast<int32_t>(offsetof(RequiredAttribute_t0941212FA1DC5F51F3309F0EB27016EE914366D4, ___ErrorMessage_0)); }
	inline String_t* get_ErrorMessage_0() const { return ___ErrorMessage_0; }
	inline String_t** get_address_of_ErrorMessage_0() { return &___ErrorMessage_0; }
	inline void set_ErrorMessage_0(String_t* value)
	{
		___ErrorMessage_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ErrorMessage_0), (void*)value);
	}

	inline static int32_t get_offset_of_MessageType_1() { return static_cast<int32_t>(offsetof(RequiredAttribute_t0941212FA1DC5F51F3309F0EB27016EE914366D4, ___MessageType_1)); }
	inline int32_t get_MessageType_1() const { return ___MessageType_1; }
	inline int32_t* get_address_of_MessageType_1() { return &___MessageType_1; }
	inline void set_MessageType_1(int32_t value)
	{
		___MessageType_1 = value;
	}
};


// Sirenix.OdinInspector.ResponsiveButtonGroupAttribute
struct  ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:
	// Sirenix.OdinInspector.ButtonSizes Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::DefaultButtonSize
	int32_t ___DefaultButtonSize_3;
	// System.Boolean Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::UniformLayout
	bool ___UniformLayout_4;

public:
	inline static int32_t get_offset_of_DefaultButtonSize_3() { return static_cast<int32_t>(offsetof(ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9, ___DefaultButtonSize_3)); }
	inline int32_t get_DefaultButtonSize_3() const { return ___DefaultButtonSize_3; }
	inline int32_t* get_address_of_DefaultButtonSize_3() { return &___DefaultButtonSize_3; }
	inline void set_DefaultButtonSize_3(int32_t value)
	{
		___DefaultButtonSize_3 = value;
	}

	inline static int32_t get_offset_of_UniformLayout_4() { return static_cast<int32_t>(offsetof(ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9, ___UniformLayout_4)); }
	inline bool get_UniformLayout_4() const { return ___UniformLayout_4; }
	inline bool* get_address_of_UniformLayout_4() { return &___UniformLayout_4; }
	inline void set_UniformLayout_4(bool value)
	{
		___UniformLayout_4 = value;
	}
};


// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// Sirenix.OdinInspector.TitleAttribute
struct  TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.TitleAttribute::Title
	String_t* ___Title_0;
	// System.String Sirenix.OdinInspector.TitleAttribute::Subtitle
	String_t* ___Subtitle_1;
	// System.Boolean Sirenix.OdinInspector.TitleAttribute::Bold
	bool ___Bold_2;
	// System.Boolean Sirenix.OdinInspector.TitleAttribute::HorizontalLine
	bool ___HorizontalLine_3;
	// Sirenix.OdinInspector.TitleAlignments Sirenix.OdinInspector.TitleAttribute::TitleAlignment
	int32_t ___TitleAlignment_4;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Title_0), (void*)value);
	}

	inline static int32_t get_offset_of_Subtitle_1() { return static_cast<int32_t>(offsetof(TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44, ___Subtitle_1)); }
	inline String_t* get_Subtitle_1() const { return ___Subtitle_1; }
	inline String_t** get_address_of_Subtitle_1() { return &___Subtitle_1; }
	inline void set_Subtitle_1(String_t* value)
	{
		___Subtitle_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Subtitle_1), (void*)value);
	}

	inline static int32_t get_offset_of_Bold_2() { return static_cast<int32_t>(offsetof(TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44, ___Bold_2)); }
	inline bool get_Bold_2() const { return ___Bold_2; }
	inline bool* get_address_of_Bold_2() { return &___Bold_2; }
	inline void set_Bold_2(bool value)
	{
		___Bold_2 = value;
	}

	inline static int32_t get_offset_of_HorizontalLine_3() { return static_cast<int32_t>(offsetof(TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44, ___HorizontalLine_3)); }
	inline bool get_HorizontalLine_3() const { return ___HorizontalLine_3; }
	inline bool* get_address_of_HorizontalLine_3() { return &___HorizontalLine_3; }
	inline void set_HorizontalLine_3(bool value)
	{
		___HorizontalLine_3 = value;
	}

	inline static int32_t get_offset_of_TitleAlignment_4() { return static_cast<int32_t>(offsetof(TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44, ___TitleAlignment_4)); }
	inline int32_t get_TitleAlignment_4() const { return ___TitleAlignment_4; }
	inline int32_t* get_address_of_TitleAlignment_4() { return &___TitleAlignment_4; }
	inline void set_TitleAlignment_4(int32_t value)
	{
		___TitleAlignment_4 = value;
	}
};


// Sirenix.OdinInspector.TitleGroupAttribute
struct  TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED  : public PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1
{
public:
	// System.String Sirenix.OdinInspector.TitleGroupAttribute::Subtitle
	String_t* ___Subtitle_3;
	// Sirenix.OdinInspector.TitleAlignments Sirenix.OdinInspector.TitleGroupAttribute::Alignment
	int32_t ___Alignment_4;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::HorizontalLine
	bool ___HorizontalLine_5;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::BoldTitle
	bool ___BoldTitle_6;
	// System.Boolean Sirenix.OdinInspector.TitleGroupAttribute::Indent
	bool ___Indent_7;

public:
	inline static int32_t get_offset_of_Subtitle_3() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED, ___Subtitle_3)); }
	inline String_t* get_Subtitle_3() const { return ___Subtitle_3; }
	inline String_t** get_address_of_Subtitle_3() { return &___Subtitle_3; }
	inline void set_Subtitle_3(String_t* value)
	{
		___Subtitle_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Subtitle_3), (void*)value);
	}

	inline static int32_t get_offset_of_Alignment_4() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED, ___Alignment_4)); }
	inline int32_t get_Alignment_4() const { return ___Alignment_4; }
	inline int32_t* get_address_of_Alignment_4() { return &___Alignment_4; }
	inline void set_Alignment_4(int32_t value)
	{
		___Alignment_4 = value;
	}

	inline static int32_t get_offset_of_HorizontalLine_5() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED, ___HorizontalLine_5)); }
	inline bool get_HorizontalLine_5() const { return ___HorizontalLine_5; }
	inline bool* get_address_of_HorizontalLine_5() { return &___HorizontalLine_5; }
	inline void set_HorizontalLine_5(bool value)
	{
		___HorizontalLine_5 = value;
	}

	inline static int32_t get_offset_of_BoldTitle_6() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED, ___BoldTitle_6)); }
	inline bool get_BoldTitle_6() const { return ___BoldTitle_6; }
	inline bool* get_address_of_BoldTitle_6() { return &___BoldTitle_6; }
	inline void set_BoldTitle_6(bool value)
	{
		___BoldTitle_6 = value;
	}

	inline static int32_t get_offset_of_Indent_7() { return static_cast<int32_t>(offsetof(TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED, ___Indent_7)); }
	inline bool get_Indent_7() const { return ___Indent_7; }
	inline bool* get_address_of_Indent_7() { return &___Indent_7; }
	inline void set_Indent_7(bool value)
	{
		___Indent_7 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// Sirenix.OdinInspector.ValidateInputAttribute
struct  ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Sirenix.OdinInspector.ValidateInputAttribute::DefaultMessage
	String_t* ___DefaultMessage_0;
	// System.String Sirenix.OdinInspector.ValidateInputAttribute::MemberName
	String_t* ___MemberName_1;
	// Sirenix.OdinInspector.InfoMessageType Sirenix.OdinInspector.ValidateInputAttribute::MessageType
	int32_t ___MessageType_2;
	// System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::IncludeChildren
	bool ___IncludeChildren_3;
	// System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::ContinuousValidationCheck
	bool ___ContinuousValidationCheck_4;

public:
	inline static int32_t get_offset_of_DefaultMessage_0() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE, ___DefaultMessage_0)); }
	inline String_t* get_DefaultMessage_0() const { return ___DefaultMessage_0; }
	inline String_t** get_address_of_DefaultMessage_0() { return &___DefaultMessage_0; }
	inline void set_DefaultMessage_0(String_t* value)
	{
		___DefaultMessage_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultMessage_0), (void*)value);
	}

	inline static int32_t get_offset_of_MemberName_1() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE, ___MemberName_1)); }
	inline String_t* get_MemberName_1() const { return ___MemberName_1; }
	inline String_t** get_address_of_MemberName_1() { return &___MemberName_1; }
	inline void set_MemberName_1(String_t* value)
	{
		___MemberName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberName_1), (void*)value);
	}

	inline static int32_t get_offset_of_MessageType_2() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE, ___MessageType_2)); }
	inline int32_t get_MessageType_2() const { return ___MessageType_2; }
	inline int32_t* get_address_of_MessageType_2() { return &___MessageType_2; }
	inline void set_MessageType_2(int32_t value)
	{
		___MessageType_2 = value;
	}

	inline static int32_t get_offset_of_IncludeChildren_3() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE, ___IncludeChildren_3)); }
	inline bool get_IncludeChildren_3() const { return ___IncludeChildren_3; }
	inline bool* get_address_of_IncludeChildren_3() { return &___IncludeChildren_3; }
	inline void set_IncludeChildren_3(bool value)
	{
		___IncludeChildren_3 = value;
	}

	inline static int32_t get_offset_of_ContinuousValidationCheck_4() { return static_cast<int32_t>(offsetof(ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE, ___ContinuousValidationCheck_4)); }
	inline bool get_ContinuousValidationCheck_4() const { return ___ContinuousValidationCheck_4; }
	inline bool* get_address_of_ContinuousValidationCheck_4() { return &___ContinuousValidationCheck_4; }
	inline void set_ContinuousValidationCheck_4(bool value)
	{
		___ContinuousValidationCheck_4 = value;
	}
};


// System.Func`2<System.String,System.String>
struct  Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A  : public MulticastDelegate_t
{
public:

public:
};


// System.ArgumentException
struct  ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.NotImplementedException
struct  NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.ArgumentNullException
struct  ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mA7F3C5A0612B84E910DE92E77BA95101FD68EEDB_gshared (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisRuntimeObject_TisRuntimeObject_mC0F1DA980E0433D70A6CF9DD7CD1942BB7FE87C0_gshared (RuntimeObject* ___source0, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Enumerable_ToArray_TisRuntimeObject_m21E15191FE8BDBAE753CC592A1DB55EA3BCE7B5B_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m1D864B65CCD0498EC4BFFBDA8F8D04AE5333195A_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m99C700668AC6D272188471D2D6B784A2B5636C8E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mFEB2301A6F28290A828A979BA9CC847B16B3D538_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___capacity0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1 (Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71 * __this, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B (String_t* __this, CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___separator0, const RuntimeMethod* method);
// System.Void System.Func`2<System.String,System.String>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m22403E6E9EC24A3D8103D29D9D66B5EEEA0AC69E (Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mA7F3C5A0612B84E910DE92E77BA95101FD68EEDB_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.String,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
inline RuntimeObject* Enumerable_Select_TisString_t_TisString_t_m5E54BE921E960B9AB33FE013CF7ECB8D1CF24A1A (RuntimeObject* ___source0, Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A *, const RuntimeMethod*))Enumerable_Select_TisRuntimeObject_TisRuntimeObject_mC0F1DA980E0433D70A6CF9DD7CD1942BB7FE87C0_gshared)(___source0, ___selector1, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* Enumerable_ToArray_TisString_t_mE824E1F8EB2A50DC8E24291957CBEED8C356E582 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m21E15191FE8BDBAE753CC592A1DB55EA3BCE7B5B_gshared)(___source0, method);
}
// System.String System.String::Join(System.String,System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Join_m8846EB11F0A221BDE237DE041D17764B36065404 (String_t* ___separator0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___value1, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA (PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor(System.String,System.Boolean,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxGroupAttribute__ctor_mD170E60B450AABCFDC796021EE2DD187F55E6500 (BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C * __this, String_t* ___group0, bool ___showLabel1, bool ___centerLabel2, int32_t ___order3, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ShowInInspectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233 (ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_HasDefinedExpanded(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_set_HasDefinedExpanded_mABAA99EDDE953EEF83E4F04F80DAC78D2E3851B3_inline (FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_HasDefinedExpanded()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_HasDefinedExpanded_mF65E7E54B706CCDE7EA9DB0C3606F2B10DCFCB25_inline (FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_m97E8952CDF6B230904482158001DAB88CA24F819 (PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * __this, String_t* ___groupId0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.String,System.Single,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HorizontalGroupAttribute__ctor_m804D5778400B0BE529750A2E66F7EB85E993BAB5 (HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3 * __this, String_t* ___group0, float ___width1, int32_t ___marginLeft2, int32_t ___marginRight3, int32_t ___order4, const RuntimeMethod* method);
// System.Single System.Math::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Math_Max_mEB87839DA28310AE4CB81A94D551874CFC2B1247 (float ___val10, float ___val21, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_MemberMethod(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_MemberMethod_mF0C22FEE372BFE43D98A7B8FDD6BD29365CEDAE8_inline (InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_Label(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_Label_m24E6BE08859E432C9819B26B95BED6F00C3DC0D0_inline (InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83 (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorModes,Sirenix.OdinInspector.InlineEditorObjectFieldModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineEditorAttribute__ctor_mA7C55B884AE2BFFA8538A0E8676FB42C0686A34B (InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76 * __this, int32_t ___inlineEditorMode0, int32_t ___objectFieldMode1, const RuntimeMethod* method);
// System.Int32 System.Math::Max(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Max_mD8AA27386BF012C65303FCDEA041B0CC65056E7B (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabelHasValue(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624_inline (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignmentHasValue(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0_inline (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, bool ___value0, const RuntimeMethod* method);
// System.Int32 System.String::LastIndexOf(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_LastIndexOf_m29D788F388576F13C5D522AD008A86859E5BA826 (String_t* __this, Il2CppChar ___value0, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_mB6B87FD76552BBF6D4E2B9F07F857FE051DCE190 (String_t* __this, int32_t ___startIndex0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97 (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Int32 System.Math::Min(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.String,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabGroupAttribute__ctor_m962E789C945DC9AC4EA826DCF7979A2A5733090B (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, String_t* ___group0, String_t* ___tab1, bool ___useFixedHeight2, int32_t ___order3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9 (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::set_Tabs(System.Collections.Generic.List`1<System.String>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TabGroupAttribute_set_Tabs_m5BB49FEBE6589933B4F2B96B581E9EE4DC5838B4_inline (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___value0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::get_Tabs()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25_inline (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, String_t*, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m468E89F534D7F4463B96A099275295DF689B2323 (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m1D864B65CCD0498EC4BFFBDA8F8D04AE5333195A_gshared)(__this, ___collection0, method);
}
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute_CombineValuesWith_mE807B73DCF4C6A9C88AA9BCD40E881733AA03AE0 (PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(!0)
inline bool List_1_Contains_m2EAD2DADA0478175052301E48FCE772ECD9A6F5F (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, String_t*, const RuntimeMethod*))List_1_Contains_m99C700668AC6D272188471D2D6B784A2B5636C8E_gshared)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
inline int32_t List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_inline (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>::.ctor(System.Int32)
inline void List_1__ctor_mAB3E3E1DD0571D6A301A9D1DF30EC3C8C49766AD (List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D *, int32_t, const RuntimeMethod*))List_1__ctor_mFEB2301A6F28290A828A979BA9CC847B16B3D538_gshared)(__this, ___capacity0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.String>::GetEnumerator()
inline Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
inline String_t* Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_inline (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabSubGroupAttribute__ctor_mBD891E55D898A286A39966CAB7BD0D9BF4E18F39 (TabSubGroupAttribute_tDEC43BA5824DF3FC46C494AC92DD32CD26D8B22C * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Sirenix.OdinInspector.PropertyGroupAttribute>::Add(!0)
inline void List_1_Add_mAE53CD7A7307ABC7A7AC23A32795DD076B4C0CB5 (List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D *, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
inline bool Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54 (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
inline void Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7 (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_m02F6C8A534512612492AB9C3C29227776F25CC66 (ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * __this, String_t* ___toggleMemberName0, int32_t ___order1, String_t* ___groupTitle2, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ValueDropdownItem::.ctor(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueDropdownItem__ctor_mCCB87C57D5FF6019771592E2E52B910C13590ACC (ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * __this, String_t* ___text0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mD3809D54FDAC43AA11084A9FE53165D57A6153FF (RuntimeObject * ___arg00, const RuntimeMethod* method);
// System.String Sirenix.OdinInspector.ValueDropdownItem::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_ToString_m809FC023CF5BE24E964513E064156CC3E405F300 (ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * __this, const RuntimeMethod* method);
// System.String Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetText()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mDE36C60532902791E5E6C2AF39297AD60F3ED9B1_inline (ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * __this, const RuntimeMethod* method);
// System.Object Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetValue()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m8FB6D5B472D5D524135DE9A0DBB78CACC7D617A2_inline (ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VerticalGroupAttribute__ctor_m95C002377FFF8A8BFFCE1F1E1C9179A471E5CE06 (VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.AssetSelectorAttribute/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mEF4D05798CE3F378E5E36DF88F14353C7CF9E182 (U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String System.String::Trim()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Trim_m3FEC641D7046124B7F381701903B50B5171DE0A2 (String_t* __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.AssetListAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetListAttribute__ctor_mD7BEF52C5E192D3C71CDD7E151B5C69C05FF7544 (AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		__this->set_AutoPopulate_0((bool)0);
		__this->set_Tags_1((String_t*)NULL);
		__this->set_LayerNames_2((String_t*)NULL);
		__this->set_AssetNamePrefix_3((String_t*)NULL);
		__this->set_CustomFilterMethod_5((String_t*)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.AssetSelectorAttribute::set_Paths(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetSelectorAttribute_set_Paths_m26DA529BF216F8FF20C13B7346D81BBBFF7C7DE5 (AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisString_t_TisString_t_m5E54BE921E960B9AB33FE013CF7ECB8D1CF24A1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisString_t_mE824E1F8EB2A50DC8E24291957CBEED8C356E582_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m22403E6E9EC24A3D8103D29D9D66B5EEEA0AC69E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3Cset_PathsU3Eb__12_0_mB1598E3D172430978C835A9B4E1A1268891DF76D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * G_B2_0 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* G_B2_1 = NULL;
	AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622 * G_B2_2 = NULL;
	Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * G_B1_0 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* G_B1_1 = NULL;
	AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622 * G_B1_2 = NULL;
	{
		String_t* L_0 = ___value0;
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_1 = (CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34*)SZArrayNew(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)124));
		NullCheck(L_0);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3;
		L_3 = String_Split_m2C74DC2B85B322998094BEDE787C378822E1F28B(L_0, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_il2cpp_TypeInfo_var);
		Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * L_4 = ((U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_il2cpp_TypeInfo_var))->get_U3CU3E9__12_0_1();
		Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * L_5 = L_4;
		G_B1_0 = L_5;
		G_B1_1 = L_3;
		G_B1_2 = __this;
		if (L_5)
		{
			G_B2_0 = L_5;
			G_B2_1 = L_3;
			G_B2_2 = __this;
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_il2cpp_TypeInfo_var);
		U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6 * L_6 = ((U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * L_7 = (Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A *)il2cpp_codegen_object_new(Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A_il2cpp_TypeInfo_var);
		Func_2__ctor_m22403E6E9EC24A3D8103D29D9D66B5EEEA0AC69E(L_7, L_6, (intptr_t)((intptr_t)U3CU3Ec_U3Cset_PathsU3Eb__12_0_mB1598E3D172430978C835A9B4E1A1268891DF76D_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m22403E6E9EC24A3D8103D29D9D66B5EEEA0AC69E_RuntimeMethod_var);
		Func_2_t5FF29EF71496B6AFA2C5B7FF601B0EFA1C47A41A * L_8 = L_7;
		((U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_il2cpp_TypeInfo_var))->set_U3CU3E9__12_0_1(L_8);
		G_B2_0 = L_8;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0031:
	{
		RuntimeObject* L_9;
		L_9 = Enumerable_Select_TisString_t_TisString_t_m5E54BE921E960B9AB33FE013CF7ECB8D1CF24A1A((RuntimeObject*)(RuntimeObject*)G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Select_TisString_t_TisString_t_m5E54BE921E960B9AB33FE013CF7ECB8D1CF24A1A_RuntimeMethod_var);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10;
		L_10 = Enumerable_ToArray_TisString_t_mE824E1F8EB2A50DC8E24291957CBEED8C356E582(L_9, /*hidden argument*/Enumerable_ToArray_TisString_t_mE824E1F8EB2A50DC8E24291957CBEED8C356E582_RuntimeMethod_var);
		NullCheck(G_B2_2);
		G_B2_2->set_SearchInFolders_9(L_10);
		return;
	}
}
// System.String Sirenix.OdinInspector.AssetSelectorAttribute::get_Paths()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AssetSelectorAttribute_get_Paths_m539145D9BC2B1F42338AF9F327F54E9B055D752C (AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC18C9BB6DF0D5C60CE5A5D2D3D6111BEB6F8CCEB);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = __this->get_SearchInFolders_9();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = __this->get_SearchInFolders_9();
		String_t* L_2;
		L_2 = String_Join_m8846EB11F0A221BDE237DE041D17764B36065404(_stringLiteralC18C9BB6DF0D5C60CE5A5D2D3D6111BEB6F8CCEB, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0019:
	{
		return (String_t*)NULL;
	}
}
// System.Void Sirenix.OdinInspector.AssetSelectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetSelectorAttribute__ctor_m4040E0B057B335CE1478EFA6EB099118FDFC0CC2 (AssetSelectorAttribute_t76606568CFA22A05C20A0742E02FDF5ED6C1D622 * __this, const RuntimeMethod* method)
{
	{
		__this->set_IsUniqueList_0((bool)1);
		__this->set_DrawDropdownForListElements_1((bool)1);
		__this->set_ExpandAllMenuItems_4((bool)1);
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.AssetsOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetsOnlyAttribute__ctor_m33B3ADA2CD2AB296AA0FC62AA4BCAA8CA56D6DC6 (AssetsOnlyAttribute_tCC6A1D3E6DBF4014A229FFADCF481275DC8CE8EE * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor(System.String,System.Boolean,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxGroupAttribute__ctor_mD170E60B450AABCFDC796021EE2DD187F55E6500 (BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C * __this, String_t* ___group0, bool ___showLabel1, bool ___centerLabel2, int32_t ___order3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___group0;
		int32_t L_1 = ___order3;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ___showLabel1;
		__this->set_ShowLabel_3(L_2);
		bool L_3 = ___centerLabel2;
		__this->set_CenterLabel_4(L_3);
		return;
	}
}
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxGroupAttribute__ctor_m6B421EB7331331EB7D1B56C34DE47AE46266C2C3 (BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F87C5EE8E2AF3096FEB1F359E7640DC822F9ACD);
		s_Il2CppMethodInitialized = true;
	}
	{
		BoxGroupAttribute__ctor_mD170E60B450AABCFDC796021EE2DD187F55E6500(__this, _stringLiteral7F87C5EE8E2AF3096FEB1F359E7640DC822F9ACD, (bool)0, (bool)0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.BoxGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxGroupAttribute_CombineValuesWith_mE5E414B3F475B56507C8FC5A2B20A60261CA2EEF (BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C * V_0 = NULL;
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___other0;
		V_0 = ((BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C *)IsInstClass((RuntimeObject*)L_0, BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C_il2cpp_TypeInfo_var));
		bool L_1 = __this->get_ShowLabel_3();
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = L_2->get_ShowLabel_3();
		if (L_3)
		{
			goto IL_0025;
		}
	}

IL_0017:
	{
		__this->set_ShowLabel_3((bool)0);
		BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C * L_4 = V_0;
		NullCheck(L_4);
		L_4->set_ShowLabel_3((bool)0);
	}

IL_0025:
	{
		bool L_5 = __this->get_CenterLabel_4();
		BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = L_6->get_CenterLabel_4();
		__this->set_CenterLabel_4((bool)((int32_t)((int32_t)L_5|(int32_t)L_7)));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mA189093060719DE77084CF59BFAA6CBBA9AE5BBA (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		__this->set_Name_1((String_t*)NULL);
		__this->set_ButtonHeight_0(0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonSizes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m54AC64264E1D03E7A49DA7BAF9BFFDE89C45C9F1 (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, int32_t ___size0, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		__this->set_Name_1((String_t*)NULL);
		int32_t L_0 = ___size0;
		__this->set_ButtonHeight_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mF2F44124675D1722CFD80947CA439B6CA6D9B61E (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, int32_t ___buttonSize0, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___buttonSize0;
		__this->set_ButtonHeight_0(L_0);
		__this->set_Name_1((String_t*)NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m83C5D44DAF14B09929680979D9E25DFF2F980A30 (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		__this->set_ButtonHeight_0(0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonSizes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mFDCA00462D14D14FF5EF79B7AF3D602C8592244A (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, String_t* ___name0, int32_t ___buttonSize1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mAD43B91B8D486F4C6DFD046E5B3EA15999F8DF19 (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, String_t* ___name0, int32_t ___buttonSize1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m40DDBADB260CF6641F37A7816F40D28D505A72C5 (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, int32_t ___parameterBtnStyle0, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		__this->set_Name_1((String_t*)NULL);
		__this->set_ButtonHeight_0(0);
		int32_t L_0 = ___parameterBtnStyle0;
		__this->set_Style_2(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.Int32,Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m7BE4CA206B38073753DB64895530A4E95732DEF4 (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, int32_t ___buttonSize0, int32_t ___parameterBtnStyle1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___buttonSize0;
		__this->set_ButtonHeight_0(L_0);
		__this->set_Name_1((String_t*)NULL);
		int32_t L_1 = ___parameterBtnStyle1;
		__this->set_Style_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(Sirenix.OdinInspector.ButtonSizes,Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mA3C5891F8246BF6A351C2A083391F60BCC6B24FF (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, int32_t ___size0, int32_t ___parameterBtnStyle1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___size0;
		__this->set_ButtonHeight_0(L_0);
		__this->set_Name_1((String_t*)NULL);
		int32_t L_1 = ___parameterBtnStyle1;
		__this->set_Style_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_m8C9AB7A45AE05F9214E955FD582DD1B6570F65C7 (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, String_t* ___name0, int32_t ___parameterBtnStyle1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		__this->set_ButtonHeight_0(0);
		int32_t L_1 = ___parameterBtnStyle1;
		__this->set_Style_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,Sirenix.OdinInspector.ButtonSizes,Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mC75E79D918637EE750DEFA31D00792CC5BBA8B7D (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, String_t* ___name0, int32_t ___buttonSize1, int32_t ___parameterBtnStyle2, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		int32_t L_2 = ___parameterBtnStyle2;
		__this->set_Style_2(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ButtonAttribute::.ctor(System.String,System.Int32,Sirenix.OdinInspector.ButtonStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonAttribute__ctor_mA5567E1BCD7D54A2EAC16AA5FEC0575A1A532EDB (ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782 * __this, String_t* ___name0, int32_t ___buttonSize1, int32_t ___parameterBtnStyle2, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_Name_1(L_0);
		int32_t L_1 = ___buttonSize1;
		__this->set_ButtonHeight_0(L_1);
		int32_t L_2 = ___parameterBtnStyle2;
		__this->set_Style_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ButtonGroupAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonGroupAttribute__ctor_m34D3B7CC10A78BA26C227283792D56AE2DFBD04A (ButtonGroupAttribute_t20631D3282E17CD252851E3A40703BA4F5EB4A86 * __this, String_t* ___group0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___group0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ChildGameObjectsOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChildGameObjectsOnlyAttribute__ctor_mDDEA4F0844C178B7145CBABE91827E784862CEB7 (ChildGameObjectsOnlyAttribute_t6CF8E838A1AE35CF0E136161BD89949F76E22BF3 * __this, const RuntimeMethod* method)
{
	{
		__this->set_IncludeSelf_0((bool)1);
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ColorPaletteAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorPaletteAttribute__ctor_mF55AD4A715288F20744E62E0FC19478F2DA6F7DC (ColorPaletteAttribute_t89BD14DE6C9A17B37A64A8E188A49AB7621D759C * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		__this->set_PaletteName_0((String_t*)NULL);
		__this->set_ShowAlpha_1((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ColorPaletteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorPaletteAttribute__ctor_mC63330FD86282DBFD27C7B150C7BC288AA5BC099 (ColorPaletteAttribute_t89BD14DE6C9A17B37A64A8E188A49AB7621D759C * __this, String_t* ___paletteName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___paletteName0;
		__this->set_PaletteName_0(L_0);
		__this->set_ShowAlpha_1((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.CustomContextMenuAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomContextMenuAttribute__ctor_mD899BC52D7DC9BA3D03DF6B350FA377D637BA294 (CustomContextMenuAttribute_tE9AFF948A16686A5D0C8831B5383270F7134AB49 * __this, String_t* ___menuItem0, String_t* ___methodName1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuItem0;
		__this->set_MenuItem_0(L_0);
		String_t* L_1 = ___methodName1;
		__this->set_MethodName_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.CustomValueDrawerAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomValueDrawerAttribute__ctor_m1AD80BD54925742FECC79F2BAF67F844EBBF83A3 (CustomValueDrawerAttribute_t7F42DE8AE9C58EB688259D42B09473C0DEE98A31 * __this, String_t* ___methodName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___methodName0;
		__this->set_MethodName_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DelayedPropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DelayedPropertyAttribute__ctor_m8C207C4888FE128658B5E3DA401BFA4A5C5EFB8B (DelayedPropertyAttribute_t18B4307EE458D335803442517336AE1208186A88 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DetailedInfoBoxAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DetailedInfoBoxAttribute__ctor_mEAD2D8A6DE093F1CAD461DBD308F8F9B02475A7E (DetailedInfoBoxAttribute_t8CEBCE93D61675CB38CA659984F4C121381C565E * __this, String_t* ___message0, String_t* ___details1, int32_t ___infoMessageType2, String_t* ___visibleIf3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		String_t* L_1 = ___details1;
		__this->set_Details_1(L_1);
		int32_t L_2 = ___infoMessageType2;
		__this->set_InfoMessageType_2(L_2);
		String_t* L_3 = ___visibleIf3;
		__this->set_VisibleIf_3(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DictionaryDrawerSettings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictionaryDrawerSettings__ctor_mFCE0FED8F84669CC694CB10353F5DDFAACBE9A24 (DictionaryDrawerSettings_tD59605DC70F69FF09FC833CCC887105A966463F7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86FADB31129B6F40C720A97600D69389EA3567E3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB720A9AE58815DFF5576319E5228D318E7899C07);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_KeyLabel_0(_stringLiteralB720A9AE58815DFF5576319E5228D318E7899C07);
		__this->set_ValueLabel_1(_stringLiteral86FADB31129B6F40C720A97600D69389EA3567E3);
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableContextMenuAttribute::.ctor(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableContextMenuAttribute__ctor_m6D48381D7DDE1005D85BF6DA1880BDD8F86FF302 (DisableContextMenuAttribute_t1EC42DF3D6D965B29C1578354756628012429BC8 * __this, bool ___disableForMember0, bool ___disableCollectionElements1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		bool L_0 = ___disableForMember0;
		__this->set_DisableForMember_0(L_0);
		bool L_1 = ___disableCollectionElements1;
		__this->set_DisableForCollectionElements_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableIfAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableIfAttribute__ctor_m7AB2E5AF098216CD048EF0DB08A33B754DA8C9D5 (DisableIfAttribute_t4938E088E077FE7ABA2C514DBF3B3CF42BE41771 * __this, String_t* ___memberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.DisableIfAttribute::.ctor(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableIfAttribute__ctor_m9DB7922AC1CCF5F478C36C54CFDBF6493BC1D270 (DisableIfAttribute_t4938E088E077FE7ABA2C514DBF3B3CF42BE41771 * __this, String_t* ___memberName0, RuntimeObject * ___optionalValue1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInEditorModeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInEditorModeAttribute__ctor_mABA48A11350EF0AFB369D01BFC4062D87CD97619 (DisableInEditorModeAttribute_t50BED704C92D71F81B99C7F72118468D918FF96D * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInInlineEditorsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInInlineEditorsAttribute__ctor_m80B5355794D7D985E91C1407E0710BA1D37B943A (DisableInInlineEditorsAttribute_t2EB18DF22DC1A708520C4AD34151B95A7D570744 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInNonPrefabsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInNonPrefabsAttribute__ctor_m76759AE55534012C1DD807020329CC7796DBAA7C (DisableInNonPrefabsAttribute_tE0BD101B759B56A43490981CAC42FC73F4AB81CE * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPlayModeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInPlayModeAttribute__ctor_mDBF6AAD9D639C4AFA15671A02FD76AAA05335A65 (DisableInPlayModeAttribute_t62870C62E5C7F02C470B68549E1323500520604C * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPrefabAssetsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInPrefabAssetsAttribute__ctor_m43BC5A861668B7A9AEC41E63C3841D138177284F (DisableInPrefabAssetsAttribute_t5A0A0810282C07F9A30A66E2B829A080B9B97BC1 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPrefabInstancesAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInPrefabInstancesAttribute__ctor_m874C04BDC1F94EC2270E7789A634CA654EDD54FE (DisableInPrefabInstancesAttribute_t88BB1DCC0C1B7F899C6C07C65B1CA463D2506DDC * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisableInPrefabsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisableInPrefabsAttribute__ctor_mBE3358BF4C37BE9CA4CFD07FFAEB75646772B260 (DisableInPrefabsAttribute_t2612557CCF5BD1035F1E8990029DDD1A3D2A33F7 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DisplayAsStringAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplayAsStringAttribute__ctor_mC5C746490926B2D0E1ECD5C98307BF6BC584AC91 (DisplayAsStringAttribute_tC0C89AF4B51CB834ED7AD9AB028D617487E7297F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		__this->set_Overflow_0((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.DisplayAsStringAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisplayAsStringAttribute__ctor_mE07DDA6D8DC4557144E1A022DE5E4BAA8E46F609 (DisplayAsStringAttribute_tC0C89AF4B51CB834ED7AD9AB028D617487E7297F * __this, bool ___overflow0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		bool L_0 = ___overflow0;
		__this->set_Overflow_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DoNotDrawAsReferenceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DoNotDrawAsReferenceAttribute__ctor_m47147F6256197B0FDB1F2E13D9C65DF661E18653 (DoNotDrawAsReferenceAttribute_tB1F70750A3599CE5DFFF4136AAD2C64FB41A6121 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DontApplyToListElementsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4 (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.DrawWithUnityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrawWithUnityAttribute__ctor_mCC3AE3ED0F2537183EEFE4E96675703CBDF9504E (DrawWithUnityAttribute_t22A1203943EDA03AB38CF29B28743CB94753A153 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnableForPrefabOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnableForPrefabOnlyAttribute__ctor_mE9234FA3C5FA64E15B740F718B6F1F70F519A2C3 (EnableForPrefabOnlyAttribute_t179CCCCF9D183ACF7AAAB884F147EEAB89BBFFDB * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnableGUIAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnableGUIAttribute__ctor_m11708B222C12CFD9CAB97B0919A83C0A68A846B1 (EnableGUIAttribute_tC43B92B9971628498DF2B4F91C1667F357265CB0 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnableIfAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnableIfAttribute__ctor_m9A122D7791A2EE11AD822E9F4916D84456A38638 (EnableIfAttribute_t3B8036F797E23325323E2064C7CCEA71BAAF7BE9 * __this, String_t* ___memberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.EnableIfAttribute::.ctor(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnableIfAttribute__ctor_m146B9D17703655B7BDAB4533643F99AFBC1D9954 (EnableIfAttribute_t3B8036F797E23325323E2064C7CCEA71BAAF7BE9 * __this, String_t* ___memberName0, RuntimeObject * ___optionalValue1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnumPagingAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumPagingAttribute__ctor_mCDBD71F100FE496FEA9D1E64F5D9B77AE1621272 (EnumPagingAttribute_tF54A51875E6BA517B1D30C52BA323866A891FB1F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.EnumToggleButtonsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumToggleButtonsAttribute__ctor_m15D3C4BA2574E11C8A3EDCEF7DECF8347B13D7EA (EnumToggleButtonsAttribute_tCF9348ACFED9D214C966105CBA027843D5ABE1C0 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.FilePathAttribute::get_ReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FilePathAttribute_get_ReadOnly_m3A6314406C2D58C15799C01B671EE88CC4399D70 (FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CReadOnlyU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.FilePathAttribute::set_ReadOnly(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FilePathAttribute_set_ReadOnly_mC86951B771DBB611C650516E350D4DA4EADA3ECD (FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CReadOnlyU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.FilePathAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FilePathAttribute__ctor_m9ED4FCDC100FF01750AD869DB5EDB9060171DAF2 (FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.FolderPathAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FolderPathAttribute__ctor_m66C482346C8479FE547313BA678A83E9A9495FAC (FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FoldoutGroupAttribute__ctor_mE7F35B6748804620264F9B7F3BB39542FF93DE25 (FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * __this, String_t* ___groupName0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupName0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::.ctor(System.String,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FoldoutGroupAttribute__ctor_m2F82C5310E63CC2154A8C29AB1EA67571C19596B (FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * __this, String_t* ___groupName0, bool ___expanded1, int32_t ___order2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupName0;
		int32_t L_1 = ___order2;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ___expanded1;
		__this->set_Expanded_3(L_2);
		FoldoutGroupAttribute_set_HasDefinedExpanded_mABAA99EDDE953EEF83E4F04F80DAC78D2E3851B3_inline(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.FoldoutGroupAttribute::get_HasDefinedExpanded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_HasDefinedExpanded_mF65E7E54B706CCDE7EA9DB0C3606F2B10DCFCB25 (FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CHasDefinedExpandedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::set_HasDefinedExpanded(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_set_HasDefinedExpanded_mABAA99EDDE953EEF83E4F04F80DAC78D2E3851B3 (FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CHasDefinedExpandedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.FoldoutGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_CombineValuesWith_m771FE13B370A0AFF2B33E8D4A265CA77AAFDBA1F (FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * V_0 = NULL;
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___other0;
		V_0 = ((FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D *)IsInstClass((RuntimeObject*)L_0, FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_il2cpp_TypeInfo_var));
		FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * L_1 = V_0;
		NullCheck(L_1);
		bool L_2;
		L_2 = FoldoutGroupAttribute_get_HasDefinedExpanded_mF65E7E54B706CCDE7EA9DB0C3606F2B10DCFCB25_inline(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		FoldoutGroupAttribute_set_HasDefinedExpanded_mABAA99EDDE953EEF83E4F04F80DAC78D2E3851B3_inline(__this, (bool)1, /*hidden argument*/NULL);
		FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = L_3->get_Expanded_3();
		__this->set_Expanded_3(L_4);
	}

IL_0022:
	{
		bool L_5;
		L_5 = FoldoutGroupAttribute_get_HasDefinedExpanded_mF65E7E54B706CCDE7EA9DB0C3606F2B10DCFCB25_inline(__this, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * L_6 = V_0;
		NullCheck(L_6);
		FoldoutGroupAttribute_set_HasDefinedExpanded_mABAA99EDDE953EEF83E4F04F80DAC78D2E3851B3_inline(L_6, (bool)1, /*hidden argument*/NULL);
		FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * L_7 = V_0;
		bool L_8 = __this->get_Expanded_3();
		NullCheck(L_7);
		L_7->set_Expanded_3(L_8);
	}

IL_003d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.GUIColorAttribute::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUIColorAttribute__ctor_mAF8F90539752BCB9D2DC4B9CD83ED40871593D76 (GUIColorAttribute_t7E1FEBC8732A8232FE82C68019C98709FD244DB4 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		float L_0 = ___r0;
		float L_1 = ___g1;
		float L_2 = ___b2;
		float L_3 = ___a3;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_Color_0(L_4);
		return;
	}
}
// System.Void Sirenix.OdinInspector.GUIColorAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUIColorAttribute__ctor_m5CB8EFCA3A75F75FBC971CE083DF3090FADCA25E (GUIColorAttribute_t7E1FEBC8732A8232FE82C68019C98709FD244DB4 * __this, String_t* ___getColor0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___getColor0;
		__this->set_GetColor_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideDuplicateReferenceBoxAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideDuplicateReferenceBoxAttribute__ctor_m2C45CE67CEC79FD4DCB816BF86E95AD95CADB89E (HideDuplicateReferenceBoxAttribute_t23219A744C229500EA4BE2F8939515E05BA7A8D1 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideIfAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfAttribute__ctor_m962D0D53FCBC0B38020E72D7E853C370B407CBBC (HideIfAttribute_t11B1419645C36889523DAB5156404E47D08167B0 * __this, String_t* ___memberName0, bool ___animate1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		bool L_1 = ___animate1;
		__this->set_Animate_2(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HideIfAttribute::.ctor(System.String,System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfAttribute__ctor_m2C6BD6C12C7977150A6649FE4DFED494356860A1 (HideIfAttribute_t11B1419645C36889523DAB5156404E47D08167B0 * __this, String_t* ___memberName0, RuntimeObject * ___optionalValue1, bool ___animate2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_1(L_1);
		bool L_2 = ___animate2;
		__this->set_Animate_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.HideIfGroupAttribute::get_MemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* HideIfGroupAttribute_get_MemberName_m2F189429E0B3943DFEA543D0E205E6F5022682BF (HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_memberName_3();
		bool L_1;
		L_1 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_2 = __this->get_memberName_3();
		return L_2;
	}

IL_0014:
	{
		String_t* L_3 = ((PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 *)__this)->get_GroupName_1();
		return L_3;
	}
}
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::set_MemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfGroupAttribute_set_MemberName_m6839D915223254EE1B9F9A24AFDF46A8CCBB8A1E (HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_memberName_3(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfGroupAttribute__ctor_m986B873A48DE6DDCB2E6D6F1F304704ADACF9501 (HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4 * __this, String_t* ___path0, bool ___animate1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		PropertyGroupAttribute__ctor_m97E8952CDF6B230904482158001DAB88CA24F819(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___animate1;
		__this->set_Animate_4(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::.ctor(System.String,System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfGroupAttribute__ctor_m9B6851A6ACEFCA54B488B2E3FB8D59CE3A660B04 (HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4 * __this, String_t* ___path0, RuntimeObject * ___value1, bool ___animate2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		PropertyGroupAttribute__ctor_m97E8952CDF6B230904482158001DAB88CA24F819(__this, L_0, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___value1;
		__this->set_Value_5(L_1);
		bool L_2 = ___animate2;
		__this->set_Animate_4(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HideIfGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideIfGroupAttribute_CombineValuesWith_mB19848141AE80E205B5C72A3439CFA60C2A5FE52 (HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4 * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4 * V_0 = NULL;
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___other0;
		V_0 = ((HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4 *)IsInstClass((RuntimeObject*)L_0, HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4_il2cpp_TypeInfo_var));
		String_t* L_1 = __this->get_memberName_3();
		bool L_2;
		L_2 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4 * L_3 = V_0;
		String_t* L_4 = __this->get_memberName_3();
		NullCheck(L_3);
		L_3->set_memberName_3(L_4);
	}

IL_0020:
	{
		bool L_5 = __this->get_Animate_4();
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4 * L_6 = V_0;
		bool L_7 = __this->get_Animate_4();
		NullCheck(L_6);
		L_6->set_Animate_4(L_7);
	}

IL_0034:
	{
		RuntimeObject * L_8 = __this->get_Value_5();
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		HideIfGroupAttribute_tA9E4175D3923CC87F5CF055EF3E45094AB074EF4 * L_9 = V_0;
		RuntimeObject * L_10 = __this->get_Value_5();
		NullCheck(L_9);
		L_9->set_Value_5(L_10);
	}

IL_0048:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInEditorModeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInEditorModeAttribute__ctor_m17A5C7A0282FF0400356659396C700515E02B396 (HideInEditorModeAttribute_tA80C5951E867A0D27A1C64176166E457B9C4C69F * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInInlineEditorsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInlineEditorsAttribute__ctor_mBB67C06431BCD0DB7796A4B8EE94A1C5681D64BF (HideInInlineEditorsAttribute_t30CAEEF70FDB341465674DA00FB39A43320F868B * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInNonPrefabsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInNonPrefabsAttribute__ctor_mBD588D03AB995CC3ABFBC1C67D35DA9740FD1AF2 (HideInNonPrefabsAttribute_t0BF8A36CF5FAE4EF1217ED0B4F1285A3098A594B * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPlayModeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInPlayModeAttribute__ctor_mEE897C3BB786574921534D6E844AEF081F4FB626 (HideInPlayModeAttribute_t5A71EEB1100BED2A35EF076EF6B99B628C7E32A7 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPrefabAssetsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInPrefabAssetsAttribute__ctor_m7C32BAA642A467D80B5395E6B137E1CBB08952FB (HideInPrefabAssetsAttribute_tB9D268E4438857EE5032A747B4629682092E69D6 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPrefabInstancesAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInPrefabInstancesAttribute__ctor_m24FCF4C7A203AC9BEE774ADF1E99A7A328157A5F (HideInPrefabInstancesAttribute_t439BFC26506199B446892DFB6C329D8DE37DA492 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInPrefabsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInPrefabsAttribute__ctor_mCAA14A340A0EA0F4B76DA56B295BD084806C5EB5 (HideInPrefabsAttribute_tB539C9027320E92429C7F183A8D0B488213D7CF4 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideInTablesAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInTablesAttribute__ctor_m7C2FA177C1B63C744039018A1B9631740FDCFB69 (HideInTablesAttribute_t110DFA0CFAC26EBCE8B6C9086B6DC345A0D8B47B * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideLabelAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideLabelAttribute__ctor_mCEC15AA42376E412DFA3661F34D4B38F5DA377D0 (HideLabelAttribute_t8CFE493B307FE0E07EA75618A406700A44FD2B00 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideMonoScriptAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideMonoScriptAttribute__ctor_m9BFB340F3A8244073A6474DEFE975F5345F972E5 (HideMonoScriptAttribute_tF0E9B439B2C4AF5DF40AA4408216484CBC611A18 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideNetworkBehaviourFieldsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideNetworkBehaviourFieldsAttribute__ctor_mF14C2E8AC55D25A6755F85402C9F9D16EFD86571 (HideNetworkBehaviourFieldsAttribute_tF2720B20E003A5A3947466638AC7081930321BD3 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HideReferenceObjectPickerAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideReferenceObjectPickerAttribute__ctor_mAC0A3B16BB2D6556939A9FF716AB46DFD8EC0B85 (HideReferenceObjectPickerAttribute_t904F2AE6C1E46F3630F27B5717DAA2A078240400 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.String,System.Single,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HorizontalGroupAttribute__ctor_m804D5778400B0BE529750A2E66F7EB85E993BAB5 (HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3 * __this, String_t* ___group0, float ___width1, int32_t ___marginLeft2, int32_t ___marginRight3, int32_t ___order4, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___group0;
		int32_t L_1 = ___order4;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		float L_2 = ___width1;
		__this->set_Width_3(L_2);
		int32_t L_3 = ___marginLeft2;
		__this->set_MarginLeft_4(((float)((float)L_3)));
		int32_t L_4 = ___marginRight3;
		__this->set_MarginRight_5(((float)((float)L_4)));
		return;
	}
}
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::.ctor(System.Single,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HorizontalGroupAttribute__ctor_m765B7BD79095475543818FDB16C39307C98B8FD4 (HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3 * __this, float ___width0, int32_t ___marginLeft1, int32_t ___marginRight2, int32_t ___order3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD13C29EFE248E598FB27D17F13DBD3AF1D765FB8);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___width0;
		int32_t L_1 = ___marginLeft1;
		int32_t L_2 = ___marginRight2;
		int32_t L_3 = ___order3;
		HorizontalGroupAttribute__ctor_m804D5778400B0BE529750A2E66F7EB85E993BAB5(__this, _stringLiteralD13C29EFE248E598FB27D17F13DBD3AF1D765FB8, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.HorizontalGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HorizontalGroupAttribute_CombineValuesWith_mC240A69FB2055CD24D60C9C5601D2C688EACD103 (HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3 * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3 * G_B1_1 = NULL;
	{
		String_t* L_0 = __this->get_Title_10();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0016;
		}
	}
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_2 = ___other0;
		NullCheck(((HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3 *)IsInstClass((RuntimeObject*)L_2, HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3_il2cpp_TypeInfo_var)));
		String_t* L_3 = ((HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3 *)IsInstClass((RuntimeObject*)L_2, HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3_il2cpp_TypeInfo_var))->get_Title_10();
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0016:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_Title_10(G_B2_0);
		float L_4 = __this->get_LabelWidth_11();
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_5 = ___other0;
		NullCheck(((HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3 *)IsInstClass((RuntimeObject*)L_5, HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3_il2cpp_TypeInfo_var)));
		float L_6 = ((HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3 *)IsInstClass((RuntimeObject*)L_5, HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3_il2cpp_TypeInfo_var))->get_LabelWidth_11();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		float L_7;
		L_7 = Math_Max_mEB87839DA28310AE4CB81A94D551874CFC2B1247(L_4, L_6, /*hidden argument*/NULL);
		__this->set_LabelWidth_11(L_7);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.IncludeMyAttributesAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IncludeMyAttributesAttribute__ctor_m791A833A21BD2049C497C812D1A1DE4F8532187E (IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.IndentAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndentAttribute__ctor_m4C4EA7CF24D165E19FDBF449EDE0BDE2EE0875FF (IndentAttribute_tE8DFFDF7D9778532690A56BC8145CC86AA6C74EE * __this, int32_t ___indentLevel0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___indentLevel0;
		__this->set_IndentLevel_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.InfoBoxAttribute::.ctor(System.String,Sirenix.OdinInspector.InfoMessageType,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InfoBoxAttribute__ctor_mEA63EA4FAF8364F7F5861E800232E06B027055CD (InfoBoxAttribute_t8D2A3D9022D85EC4424CAB78C71C24C8BF8AEFD5 * __this, String_t* ___message0, int32_t ___infoMessageType1, String_t* ___visibleIfMemberName2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		int32_t L_1 = ___infoMessageType1;
		__this->set_InfoMessageType_1(L_1);
		String_t* L_2 = ___visibleIfMemberName2;
		__this->set_VisibleIf_2(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.InfoBoxAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InfoBoxAttribute__ctor_mE6C3029629BB3B188D62C9D3A42688B2877281D6 (InfoBoxAttribute_t8D2A3D9022D85EC4424CAB78C71C24C8BF8AEFD5 * __this, String_t* ___message0, String_t* ___visibleIfMemberName1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		__this->set_InfoMessageType_1(1);
		String_t* L_1 = ___visibleIfMemberName1;
		__this->set_VisibleIf_2(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineButtonAttribute__ctor_m03193A02CCD5D30E075F82C727BF6B7EDE1E86D4 (InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18 * __this, String_t* ___memberMethod0, String_t* ___label1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberMethod0;
		InlineButtonAttribute_set_MemberMethod_mF0C22FEE372BFE43D98A7B8FDD6BD29365CEDAE8_inline(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___label1;
		InlineButtonAttribute_set_Label_m24E6BE08859E432C9819B26B95BED6F00C3DC0D0_inline(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String Sirenix.OdinInspector.InlineButtonAttribute::get_MemberMethod()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InlineButtonAttribute_get_MemberMethod_mECE123FE218739415AE473D16E36D3FCF36042EE (InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CMemberMethodU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_MemberMethod(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_MemberMethod_mF0C22FEE372BFE43D98A7B8FDD6BD29365CEDAE8 (InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CMemberMethodU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String Sirenix.OdinInspector.InlineButtonAttribute::get_Label()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InlineButtonAttribute_get_Label_m06C931996AADA45AD653F209A5713A4AB09135E0 (InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CLabelU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.InlineButtonAttribute::set_Label(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_Label_m24E6BE08859E432C9819B26B95BED6F00C3DC0D0 (InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CLabelU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorModes,Sirenix.OdinInspector.InlineEditorObjectFieldModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineEditorAttribute__ctor_mA7C55B884AE2BFFA8538A0E8676FB42C0686A34B (InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76 * __this, int32_t ___inlineEditorMode0, int32_t ___objectFieldMode1, const RuntimeMethod* method)
{
	{
		__this->set_PreviewWidth_5((100.0f));
		__this->set_PreviewHeight_6((35.0f));
		__this->set_IncrementInlineEditorDrawerDepth_7((bool)1);
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___objectFieldMode1;
		__this->set_ObjectFieldMode_8(L_0);
		int32_t L_1 = ___inlineEditorMode0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_004a;
			}
			case 1:
			{
				goto IL_0052;
			}
			case 2:
			{
				goto IL_0061;
			}
			case 3:
			{
				goto IL_0070;
			}
			case 4:
			{
				goto IL_007f;
			}
			case 5:
			{
				goto IL_0099;
			}
		}
	}
	{
		goto IL_00af;
	}

IL_004a:
	{
		__this->set_DrawGUI_2((bool)1);
		return;
	}

IL_0052:
	{
		__this->set_DrawGUI_2((bool)1);
		__this->set_DrawHeader_1((bool)1);
		return;
	}

IL_0061:
	{
		__this->set_DrawGUI_2((bool)1);
		__this->set_DrawPreview_3((bool)1);
		return;
	}

IL_0070:
	{
		__this->set_Expanded_0((bool)1);
		__this->set_DrawPreview_3((bool)1);
		return;
	}

IL_007f:
	{
		__this->set_Expanded_0((bool)1);
		__this->set_DrawPreview_3((bool)1);
		__this->set_PreviewHeight_6((170.0f));
		return;
	}

IL_0099:
	{
		__this->set_DrawGUI_2((bool)1);
		__this->set_DrawHeader_1((bool)1);
		__this->set_DrawPreview_3((bool)1);
		return;
	}

IL_00af:
	{
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_2 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InlineEditorAttribute__ctor_mA7C55B884AE2BFFA8538A0E8676FB42C0686A34B_RuntimeMethod_var)));
	}
}
// System.Void Sirenix.OdinInspector.InlineEditorAttribute::.ctor(Sirenix.OdinInspector.InlineEditorObjectFieldModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlineEditorAttribute__ctor_m561C13A489F0CFB071822CF18F134842B72B4BFA (InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76 * __this, int32_t ___objectFieldMode0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___objectFieldMode0;
		InlineEditorAttribute__ctor_mA7C55B884AE2BFFA8538A0E8676FB42C0686A34B(__this, 0, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.InlinePropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InlinePropertyAttribute__ctor_m89DE665FBEDCA7C8719E3A71D152F42C57F99F14 (InlinePropertyAttribute_tC4F6EE65AF6B4B9F3D0995C379732DA61E459158 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.LabelTextAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LabelTextAttribute__ctor_m120AFF29F6C78A1294923EF793C32D1535C6C202 (LabelTextAttribute_tAC01C34A3856E3C62B39EA8A4412BFE522E35D27 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___text0;
		__this->set_Text_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.LabelWidthAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LabelWidthAttribute__ctor_m50D992D5EF45F1422D89592679A4A00370B38269 (LabelWidthAttribute_t2E4C87DC106B9C9E3D7E47E038D3161026726A8B * __this, float ___width0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		float L_0 = ___width0;
		__this->set_Width_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowPaging()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowPaging_m04B50A37D720C75A3C2E44102538E3C1F6C78B4A (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_paging_12();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowPaging(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_ShowPaging_mA98F384C28AEB5CF37229A9467E3D714703BB775 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_paging_12(L_0);
		__this->set_pagingHasValue_16((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_DraggableItems()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_DraggableItems_mB17C6C50B8BD7EA78D57725B196706F3CADF6D45 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_draggable_13();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_DraggableItems(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_DraggableItems_m8B0B96997D2F66F64D75FE1FF189822B36A083EE (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_draggable_13(L_0);
		__this->set_draggableHasValue_17((bool)1);
		return;
	}
}
// System.Int32 Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_NumberOfItemsPerPage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ListDrawerSettingsAttribute_get_NumberOfItemsPerPage_m0AD386F6554043FBDD495AA7DC702C77BE4EDE14 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_numberOfItemsPerPage_11();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_NumberOfItemsPerPage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_NumberOfItemsPerPage_m8993312EAEF84406A3DF92FC426E5103FF28F3B9 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_numberOfItemsPerPage_11(L_0);
		__this->set_numberOfItemsPerPageHasValue_22((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_IsReadOnly_mF9F88EA01F82CEE70F43077211CE1F04EB90F7F1 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isReadOnly_14();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_IsReadOnly(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_IsReadOnly_m083597FF5318279936B6F201BE6CF39394F5879C (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isReadOnly_14(L_0);
		__this->set_isReadOnlyHasValue_18((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowItemCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowItemCount_mD1E9AC4E9DE4F9B3770092433EA5CFDCE13BF991 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showItemCount_15();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowItemCount(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_ShowItemCount_m665258C5382FD7E1644B0D2C0CDB091E5BC24233 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_showItemCount_15(L_0);
		__this->set_showItemCountHasValue_19((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_Expanded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_Expanded_mE02779BAA3ED21A4496BBC18FDD943AD547BA13B (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_expanded_20();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_Expanded(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_Expanded_m8F83C911B15D8D27A52E21ECCE51E19B8D9320B4 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_expanded_20(L_0);
		__this->set_expandedHasValue_21((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowIndexLabels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowIndexLabels_m4AAEB41041F7C12CADDE07667D18FCD5C61EF5EB (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showIndexLabels_23();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_ShowIndexLabels(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_ShowIndexLabels_m133A99569AC66462AFA79DFDD9E252939E623830 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_showIndexLabels_23(L_0);
		__this->set_showIndexLabelsHasValue_24((bool)1);
		return;
	}
}
// System.String Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_OnTitleBarGUI()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ListDrawerSettingsAttribute_get_OnTitleBarGUI_m9E6161CFD836FD472193734DDE1597ED6CB171B6 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_onTitleBarGUI_10();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::set_OnTitleBarGUI(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute_set_OnTitleBarGUI_m424220A303257C85E1792B9A1423C42574224EBD (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_onTitleBarGUI_10(L_0);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_PagingHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_PagingHasValue_m9550E5B462570C1C522622F2D5C03310A6FE8843 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_pagingHasValue_16();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowItemCountHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowItemCountHasValue_mB1DFCCC7B480597A8451D731BC3543891DBC39DC (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showItemCountHasValue_19();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_NumberOfItemsPerPageHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_NumberOfItemsPerPageHasValue_mAD690824656C5003CFC3F2E76C1EAA916BCDEBA6 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_numberOfItemsPerPageHasValue_22();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_DraggableHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_DraggableHasValue_m664E10C5C14E9DC9496343C5AA725E0371E029E5 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_draggableHasValue_17();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_IsReadOnlyHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_IsReadOnlyHasValue_m6006332D62E945FE658FB12805B036CCACD3FA58 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isReadOnlyHasValue_18();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ExpandedHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ExpandedHasValue_m0B6F2D13EA28AE62EB962846B74AB7859E6C8CFA (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_expandedHasValue_21();
		return L_0;
	}
}
// System.Boolean Sirenix.OdinInspector.ListDrawerSettingsAttribute::get_ShowIndexLabelsHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ListDrawerSettingsAttribute_get_ShowIndexLabelsHasValue_m2257AF08FAA5E5A21D4E27DF92FA787A969002E8 (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showIndexLabelsHasValue_24();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ListDrawerSettingsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListDrawerSettingsAttribute__ctor_m89C7EEDBDF4AC27489757F0E307486830B47E9ED (ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MaxValueAttribute::.ctor(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaxValueAttribute__ctor_m0F7C5417770DC83172C27B6622E3A2CA1942C3DA (MaxValueAttribute_tFC98EB9F62599D96C7A430775FE8543EC98309BC * __this, double ___maxValue0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		double L_0 = ___maxValue0;
		__this->set_MaxValue_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MaxValueAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaxValueAttribute__ctor_m2DECAE60D96504B2869870AEC5A9277A3C155D3A (MaxValueAttribute_tFC98EB9F62599D96C7A430775FE8543EC98309BC * __this, String_t* ___expression0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___expression0;
		__this->set_Expression_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m73C32546B8D44A35C3BB658A1AB05EB9EF2AB504 (MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41 * __this, float ___minValue0, float ___maxValue1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		float L_0 = ___minValue0;
		__this->set_MinValue_0(L_0);
		float L_1 = ___maxValue1;
		__this->set_MaxValue_1(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_mA60851FC601385F5021FB7A6C9C8201E465031D9 (MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41 * __this, String_t* ___minMember0, float ___maxValue1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		float L_1 = ___maxValue1;
		__this->set_MaxValue_1(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.Single,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m97121D0D3A9907DE58A2B1B80BDCF7EAE25FC2BD (MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41 * __this, float ___minValue0, String_t* ___maxMember1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		float L_0 = ___minValue0;
		__this->set_MinValue_0(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_m2B0197624446EE2E82BA984F419B1F0A8A1313FA (MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41 * __this, String_t* ___minMember0, String_t* ___maxMember1, bool ___showFields2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		bool L_2 = ___showFields2;
		__this->set_ShowFields_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinMaxSliderAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_mBB9822C30540B63E70E4BA2DBB29D998BC2BF5AB (MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41 * __this, String_t* ___minMaxMember0, bool ___showFields1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMaxMember0;
		__this->set_MinMaxMember_4(L_0);
		bool L_1 = ___showFields1;
		__this->set_ShowFields_5(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MinValueAttribute::.ctor(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinValueAttribute__ctor_m655347F068916BDDCB0DB745A3EF3F6B7462F9C8 (MinValueAttribute_t45E8A787D2858D90D37F7A87C0582C0BF131FA34 * __this, double ___minValue0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		double L_0 = ___minValue0;
		__this->set_MinValue_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.MinValueAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinValueAttribute__ctor_m78683ABAE34F7DDBA6D6CB08132F4AF61AA1669A (MinValueAttribute_t45E8A787D2858D90D37F7A87C0582C0BF131FA34 * __this, String_t* ___expression0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___expression0;
		__this->set_Expression_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.MultiLinePropertyAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiLinePropertyAttribute__ctor_m53CC75198F796D0C231E74BACD67F2E7A57B3EE0 (MultiLinePropertyAttribute_tD35BC318DFDFAC8469DA2D276235CCF0E59B319D * __this, int32_t ___lines0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___lines0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = Math_Max_mD8AA27386BF012C65303FCDEA041B0CC65056E7B(1, L_0, /*hidden argument*/NULL);
		__this->set_Lines_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OdinRegisterAttributeAttribute::.ctor(System.Type,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OdinRegisterAttributeAttribute__ctor_m8E0FA6955933F971939209DFEC3ED94A48CAD251 (OdinRegisterAttributeAttribute_t8A82A1C5F996F02644E959C5EAD6533EBF268FFA * __this, Type_t * ___attributeType0, String_t* ___category1, String_t* ___description2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___attributeType0;
		__this->set_AttributeType_0(L_0);
		String_t* L_1 = ___category1;
		__this->set_Categories_1(L_1);
		String_t* L_2 = ___description2;
		__this->set_Description_2(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OdinRegisterAttributeAttribute::.ctor(System.Type,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OdinRegisterAttributeAttribute__ctor_m5E496415C227140A2F40B9CAEB84A2776F549523 (OdinRegisterAttributeAttribute_t8A82A1C5F996F02644E959C5EAD6533EBF268FFA * __this, Type_t * ___attributeType0, String_t* ___category1, String_t* ___description2, String_t* ___url3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___attributeType0;
		__this->set_AttributeType_0(L_0);
		String_t* L_1 = ___category1;
		__this->set_Categories_1(L_1);
		String_t* L_2 = ___description2;
		__this->set_Description_2(L_2);
		String_t* L_3 = ___url3;
		__this->set_DocumentationUrl_3(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnInspectorGUIAttribute__ctor_mDDC5EEE9089AF4C1AD2F72C23E3432DE38D7C881 (OnInspectorGUIAttribute_t1CCCB842BCEBEB2AB07C1298B7FFBAB7571B0F2F * __this, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnInspectorGUIAttribute__ctor_mC4B82BFF792E2DC9774313DF104F6229A9BA68AA (OnInspectorGUIAttribute_t1CCCB842BCEBEB2AB07C1298B7FFBAB7571B0F2F * __this, String_t* ___methodName0, bool ___append1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		bool L_0 = ___append1;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___methodName0;
		__this->set_AppendMethodName_1(L_1);
		return;
	}

IL_0011:
	{
		String_t* L_2 = ___methodName0;
		__this->set_PrependMethodName_0(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.OnInspectorGUIAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnInspectorGUIAttribute__ctor_mAC47FD49A86EF8BEED1271E1372E011341BE3936 (OnInspectorGUIAttribute_t1CCCB842BCEBEB2AB07C1298B7FFBAB7571B0F2F * __this, String_t* ___prependMethodName0, String_t* ___appendMethodName1, const RuntimeMethod* method)
{
	{
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___prependMethodName0;
		__this->set_PrependMethodName_0(L_0);
		String_t* L_1 = ___appendMethodName1;
		__this->set_AppendMethodName_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.OnValueChangedAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnValueChangedAttribute__ctor_mABB73C7E36822F4B7FFFCBB1BC4527D4A738093D (OnValueChangedAttribute_tFE13CD46B1B5466F8AA8C351562C9D3BB9FB7623 * __this, String_t* ___methodName0, bool ___includeChildren1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___methodName0;
		__this->set_MethodName_0(L_0);
		bool L_1 = ___includeChildren1;
		__this->set_IncludeChildren_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_mC099E62E5FAC38708BC45556F8C6E6233D689740 (PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		__this->set_Height_0((0.0f));
		return;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_mFEDC314B35C519267808A58D7E9264027E418660 (PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4 * __this, float ___height0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_Height_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(System.Single,Sirenix.OdinInspector.ObjectFieldAlignment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_m9AC2A039712B5273C77B62CE22D0147500E0EC1D (PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4 * __this, float ___height0, int32_t ___alignment1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_Height_0(L_0);
		int32_t L_1 = ___alignment1;
		__this->set_Alignment_1(L_1);
		__this->set_AlignmentHasValue_2((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PreviewFieldAttribute::.ctor(Sirenix.OdinInspector.ObjectFieldAlignment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreviewFieldAttribute__ctor_mA69C62B5B596B6BE59A938C40FDBDDD10E3A4294 (PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4 * __this, int32_t ___alignment0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___alignment0;
		__this->set_Alignment_1(L_0);
		__this->set_AlignmentHasValue_2((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.Double,System.Double,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m6798524CD0FD36BF262D07CE49731D29C6034C98 (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, double ___min0, double ___max1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		__this->set_Min_0(L_0);
		double L_1 = ___max1;
		__this->set_Max_1(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624_inline(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0_inline(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.String,System.Double,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m2D683B9926AD93ED85E99E6A982245C37FFFAE62 (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, String_t* ___minMember0, double ___max1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		double L_1 = ___max1;
		__this->set_Max_1(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624_inline(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0_inline(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.Double,System.String,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m548557B8A039AF4A656F9CF1849E312DB3EEA95F (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, double ___min0, String_t* ___maxMember1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		__this->set_Min_0(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624_inline(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0_inline(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::.ctor(System.String,System.String,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute__ctor_m449BBE33DABF66BD8E56261E35A71C58E0A6C7F2 (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, String_t* ___minMember0, String_t* ___maxMember1, float ___r2, float ___g3, float ___b4, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		float L_2 = ___r2;
		__this->set_R_4(L_2);
		float L_3 = ___g3;
		__this->set_G_5(L_3);
		float L_4 = ___b4;
		__this->set_B_6(L_4);
		__this->set_Height_7(((int32_t)12));
		__this->set_Segmented_10((bool)0);
		__this->set_drawValueLabel_12((bool)1);
		ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624_inline(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_valueLabelAlignment_13(1);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0_inline(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_DrawValueLabel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProgressBarAttribute_get_DrawValueLabel_m8755D428C189B82B8423283EAC1ECE16E828812E (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_drawValueLabel_12();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabel(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabel_mB76F6D2A75922450777B1FC0BA8E5E39315EA968 (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_drawValueLabel_12(L_0);
		ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624_inline(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_DrawValueLabelHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProgressBarAttribute_get_DrawValueLabelHasValue_m06BCD66013F03A68A6C08FF5D7A49AEF316F71D9 (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CDrawValueLabelHasValueU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_DrawValueLabelHasValue(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624 (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CDrawValueLabelHasValueU3Ek__BackingField_14(L_0);
		return;
	}
}
// UnityEngine.TextAlignment Sirenix.OdinInspector.ProgressBarAttribute::get_ValueLabelAlignment()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ProgressBarAttribute_get_ValueLabelAlignment_mB6B666D410FFE91353B85A24A53AE60782CB3E2A (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_valueLabelAlignment_13();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignment(UnityEngine.TextAlignment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignment_mE8366B95F522B01603C426704B9047FFE3BCF742 (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_valueLabelAlignment_13(L_0);
		ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0_inline(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.ProgressBarAttribute::get_ValueLabelAlignmentHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ProgressBarAttribute_get_ValueLabelAlignmentHasValue_m7DC1402D717D0D4EE8E72DF81C941D244509A447 (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ProgressBarAttribute::set_ValueLabelAlignmentHasValue(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0 (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA (PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * G_B2_0 = NULL;
	PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * G_B1_0 = NULL;
	PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * G_B3_0 = NULL;
	String_t* G_B4_0 = NULL;
	PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * G_B4_1 = NULL;
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___groupId0;
		__this->set_GroupID_0(L_0);
		int32_t L_1 = ___order1;
		__this->set_Order_2(L_1);
		String_t* L_2 = ___groupId0;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = String_LastIndexOf_m29D788F388576F13C5D522AD008A86859E5BA826(L_2, ((int32_t)47), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		G_B1_0 = __this;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			G_B2_0 = __this;
			goto IL_002b;
		}
	}
	{
		int32_t L_5 = V_0;
		String_t* L_6 = ___groupId0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_6, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			G_B3_0 = G_B1_0;
			goto IL_002e;
		}
	}

IL_002b:
	{
		String_t* L_8 = ___groupId0;
		G_B4_0 = L_8;
		G_B4_1 = G_B2_0;
		goto IL_0037;
	}

IL_002e:
	{
		String_t* L_9 = ___groupId0;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11;
		L_11 = String_Substring_mB6B87FD76552BBF6D4E2B9F07F857FE051DCE190(L_9, ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)), /*hidden argument*/NULL);
		G_B4_0 = L_11;
		G_B4_1 = G_B3_0;
	}

IL_0037:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_GroupName_1(G_B4_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute__ctor_m97E8952CDF6B230904482158001DAB88CA24F819 (PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * __this, String_t* ___groupId0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupId0;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// Sirenix.OdinInspector.PropertyGroupAttribute Sirenix.OdinInspector.PropertyGroupAttribute::Combine(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * PropertyGroupAttribute_Combine_m428F9CF2608153498BE167A9BDE7F139668B237F (PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_1 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralF7933083B6BA56CBC6D7BCA0F30688A30D0368F6)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyGroupAttribute_Combine_m428F9CF2608153498BE167A9BDE7F139668B237F_RuntimeMethod_var)));
	}

IL_000e:
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_2 = ___other0;
		NullCheck(L_2);
		Type_t * L_3;
		L_3 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_2, /*hidden argument*/NULL);
		Type_t * L_4;
		L_4 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(__this, /*hidden argument*/NULL);
		if ((((RuntimeObject*)(Type_t *)L_3) == ((RuntimeObject*)(Type_t *)L_4)))
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_5 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_5, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral0B13B52780344FB2960F902E01BC99A1B7E5A4B5)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyGroupAttribute_Combine_m428F9CF2608153498BE167A9BDE7F139668B237F_RuntimeMethod_var)));
	}

IL_0027:
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_6 = ___other0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_GroupID_0();
		String_t* L_8 = __this->get_GroupID_0();
		bool L_9;
		L_9 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_10 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_10, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral43E3531262B01D6352A62A076F3CA58EB20D257F)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyGroupAttribute_Combine_m428F9CF2608153498BE167A9BDE7F139668B237F_RuntimeMethod_var)));
	}

IL_0045:
	{
		int32_t L_11 = __this->get_Order_2();
		if (L_11)
		{
			goto IL_005b;
		}
	}
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_12 = ___other0;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_Order_2();
		__this->set_Order_2(L_13);
		goto IL_007a;
	}

IL_005b:
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_14 = ___other0;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_Order_2();
		if (!L_15)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_16 = __this->get_Order_2();
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_17 = ___other0;
		NullCheck(L_17);
		int32_t L_18 = L_17->get_Order_2();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_19;
		L_19 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(L_16, L_18, /*hidden argument*/NULL);
		__this->set_Order_2(L_19);
	}

IL_007a:
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_20 = ___other0;
		VirtActionInvoker1< PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * >::Invoke(4 /* System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute) */, __this, L_20);
		return __this;
	}
}
// System.Void Sirenix.OdinInspector.PropertyGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyGroupAttribute_CombineValuesWith_mE807B73DCF4C6A9C88AA9BCD40E881733AA03AE0 (PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyOrderAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyOrderAttribute__ctor_m75C3FDE0E6665079956F631BFB910048CEBE3F70 (PropertyOrderAttribute_t2F83A60EEA2D6D2C91E10D2A83E22326F5F71936 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyOrderAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyOrderAttribute__ctor_m2807F2D02F63E9E1B6AE80787BB8F72719D68812 (PropertyOrderAttribute_t2F83A60EEA2D6D2C91E10D2A83E22326F5F71936 * __this, int32_t ___order0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___order0;
		__this->set_Order_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_m31FCD91543E6D7E0AE3B00A9B2BB744CCA2725AF (PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0 * __this, double ___min0, double ___max1, const RuntimeMethod* method)
{
	PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0 * G_B2_0 = NULL;
	PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0 * G_B1_0 = NULL;
	double G_B3_0 = 0.0;
	PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0 * G_B3_1 = NULL;
	PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0 * G_B5_0 = NULL;
	PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0 * G_B4_0 = NULL;
	double G_B6_0 = 0.0;
	PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0 * G_B6_1 = NULL;
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		double L_1 = ___max1;
		G_B1_0 = __this;
		if ((((double)L_0) < ((double)L_1)))
		{
			G_B2_0 = __this;
			goto IL_000e;
		}
	}
	{
		double L_2 = ___max1;
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_000f;
	}

IL_000e:
	{
		double L_3 = ___min0;
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_000f:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_Min_0(G_B3_0);
		double L_4 = ___max1;
		double L_5 = ___min0;
		G_B4_0 = __this;
		if ((((double)L_4) > ((double)L_5)))
		{
			G_B5_0 = __this;
			goto IL_001c;
		}
	}
	{
		double L_6 = ___min0;
		G_B6_0 = L_6;
		G_B6_1 = G_B4_0;
		goto IL_001d;
	}

IL_001c:
	{
		double L_7 = ___max1;
		G_B6_0 = L_7;
		G_B6_1 = G_B5_0;
	}

IL_001d:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_Max_1(G_B6_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.String,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_m442057690400645107E37F5C12D7D976269A15EE (PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0 * __this, String_t* ___minMember0, double ___max1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		double L_1 = ___max1;
		__this->set_Max_1(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.Double,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_mBC695897F24EF4544EC25B5E9876165B8ED1FA77 (PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0 * __this, double ___min0, String_t* ___maxMember1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		__this->set_Min_0(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertyRangeAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyRangeAttribute__ctor_m9EBF44FAF1CFE1EDC513180C6EBB28B6B9E786BE (PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0 * __this, String_t* ___minMember0, String_t* ___maxMember1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___minMember0;
		__this->set_MinMember_2(L_0);
		String_t* L_1 = ___maxMember1;
		__this->set_MaxMember_3(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertySpaceAttribute__ctor_m83A768EB9D318E84B668055F808E5F392D7C32EB (PropertySpaceAttribute_tDF729E8EB760A22841021D50F961B59FAB282048 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		__this->set_SpaceBefore_0((8.0f));
		__this->set_SpaceAfter_1((0.0f));
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertySpaceAttribute__ctor_m76955C7DE89CAC865AA6055D4DB629D46FF7CAA0 (PropertySpaceAttribute_tDF729E8EB760A22841021D50F961B59FAB282048 * __this, float ___spaceBefore0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		float L_0 = ___spaceBefore0;
		__this->set_SpaceBefore_0(L_0);
		__this->set_SpaceAfter_1((0.0f));
		return;
	}
}
// System.Void Sirenix.OdinInspector.PropertySpaceAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertySpaceAttribute__ctor_mB343CE0B6B517B4A78002E95B34A5278629A8285 (PropertySpaceAttribute_tDF729E8EB760A22841021D50F961B59FAB282048 * __this, float ___spaceBefore0, float ___spaceAfter1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		float L_0 = ___spaceBefore0;
		__this->set_SpaceBefore_0(L_0);
		float L_1 = ___spaceAfter1;
		__this->set_SpaceAfter_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.PropertyTooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyTooltipAttribute__ctor_m71942469D93E0D7ACEA1B3038C933AAA232C741C (PropertyTooltipAttribute_t7BC0BF969C21340C1CA2B7180CFD04350B79204B * __this, String_t* ___tooltip0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip0;
		__this->set_Tooltip_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyAttribute__ctor_m6C0720DC3F32C28CA3D686B112F3CA7405B08CBA (ReadOnlyAttribute_t4A1BE30F9257C5E0187EFB8FC659A42F4B61472C * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_m3A7A9539E19CA3D8154225B63AE8E0F624CA0517 (RequiredAttribute_t0941212FA1DC5F51F3309F0EB27016EE914366D4 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		__this->set_MessageType_1(3);
		return;
	}
}
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(System.String,Sirenix.OdinInspector.InfoMessageType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_m6A2BBE37D802335237FE8CF510CD725AAD609D10 (RequiredAttribute_t0941212FA1DC5F51F3309F0EB27016EE914366D4 * __this, String_t* ___errorMessage0, int32_t ___messageType1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___errorMessage0;
		__this->set_ErrorMessage_0(L_0);
		int32_t L_1 = ___messageType1;
		__this->set_MessageType_1(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_m4DF877C8136908A9C54783F10C4D6DAB2D440AF2 (RequiredAttribute_t0941212FA1DC5F51F3309F0EB27016EE914366D4 * __this, String_t* ___errorMessage0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___errorMessage0;
		__this->set_ErrorMessage_0(L_0);
		__this->set_MessageType_1(3);
		return;
	}
}
// System.Void Sirenix.OdinInspector.RequiredAttribute::.ctor(Sirenix.OdinInspector.InfoMessageType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequiredAttribute__ctor_mB9C90488A06DD41E9D39D3EBEA88510CBD0E826C (RequiredAttribute_t0941212FA1DC5F51F3309F0EB27016EE914366D4 * __this, int32_t ___messageType0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___messageType0;
		__this->set_MessageType_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResponsiveButtonGroupAttribute__ctor_m4F03C6D6CC22A5B4586F993A4E4BBC8CE4320D8F (ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 * __this, String_t* ___group0, const RuntimeMethod* method)
{
	{
		__this->set_DefaultButtonSize_3(((int32_t)22));
		String_t* L_0 = ___group0;
		PropertyGroupAttribute__ctor_m97E8952CDF6B230904482158001DAB88CA24F819(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ResponsiveButtonGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResponsiveButtonGroupAttribute_CombineValuesWith_mE025C2906C58CCD9A5A71A2758B0B014C1A00910 (ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 * V_0 = NULL;
	ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 * G_B8_0 = NULL;
	ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 * G_B7_0 = NULL;
	int32_t G_B9_0 = 0;
	ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 * G_B9_1 = NULL;
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___other0;
		V_0 = ((ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 *)IsInstClass((RuntimeObject*)L_0, ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9_il2cpp_TypeInfo_var));
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_1 = ___other0;
		if (L_1)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_DefaultButtonSize_3();
		if ((((int32_t)L_3) == ((int32_t)((int32_t)22))))
		{
			goto IL_0023;
		}
	}
	{
		ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_DefaultButtonSize_3();
		__this->set_DefaultButtonSize_3(L_5);
		goto IL_0039;
	}

IL_0023:
	{
		int32_t L_6 = __this->get_DefaultButtonSize_3();
		if ((((int32_t)L_6) == ((int32_t)((int32_t)22))))
		{
			goto IL_0039;
		}
	}
	{
		ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 * L_7 = V_0;
		int32_t L_8 = __this->get_DefaultButtonSize_3();
		NullCheck(L_7);
		L_7->set_DefaultButtonSize_3(L_8);
	}

IL_0039:
	{
		bool L_9 = __this->get_UniformLayout_4();
		G_B7_0 = __this;
		if (L_9)
		{
			G_B8_0 = __this;
			goto IL_004a;
		}
	}
	{
		ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = L_10->get_UniformLayout_4();
		G_B9_0 = ((int32_t)(L_11));
		G_B9_1 = G_B7_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B9_0 = 1;
		G_B9_1 = G_B8_0;
	}

IL_004b:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_UniformLayout_4((bool)G_B9_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.SceneObjectsOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneObjectsOnlyAttribute__ctor_m6FC75F3244D4F334145DCCDB287B70227988AEA8 (SceneObjectsOnlyAttribute_t74D00B9886B272BE983F0AE1F447B56E222402A2 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowDrawerChainAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowDrawerChainAttribute__ctor_m0A63FAFA0F1F0430D816601D128666692AAC5BE2 (ShowDrawerChainAttribute_tAF05CAAEC33491EA7281A449128A45877A66A7CC * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowForPrefabOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowForPrefabOnlyAttribute__ctor_m2F3666A81E25D93DA70D60C45DC60E0013833E63 (ShowForPrefabOnlyAttribute_tF680D3BAFA9AF8C0C8BE3A0EC1DB9380BCB319D4 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowIfAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfAttribute__ctor_m04695ED0E3854D2DCB01DE13F17900053A70F3D6 (ShowIfAttribute_tDF7F785B12F1489ED80C78FC2695E7363CC5B8AE * __this, String_t* ___memberName0, bool ___animate1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		bool L_1 = ___animate1;
		__this->set_Animate_1(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfAttribute::.ctor(System.String,System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfAttribute__ctor_m6D02DEC959A55EF7135A0E1AF02681D4AE43419E (ShowIfAttribute_tDF7F785B12F1489ED80C78FC2695E7363CC5B8AE * __this, String_t* ___memberName0, RuntimeObject * ___optionalValue1, bool ___animate2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		RuntimeObject * L_1 = ___optionalValue1;
		__this->set_Value_2(L_1);
		bool L_2 = ___animate2;
		__this->set_Animate_1(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Sirenix.OdinInspector.ShowIfGroupAttribute::get_MemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ShowIfGroupAttribute_get_MemberName_mB9AD99DB8A8B5360638026E9922C9D9FCB1F0F37 (ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_memberName_3();
		bool L_1;
		L_1 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_2 = __this->get_memberName_3();
		return L_2;
	}

IL_0014:
	{
		String_t* L_3 = ((PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 *)__this)->get_GroupName_1();
		return L_3;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::set_MemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfGroupAttribute_set_MemberName_m89AC56CE0BC24BA6AE556D56B861693188FBE53D (ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_memberName_3(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfGroupAttribute__ctor_m13D173D317B53E20CF189BA7C556CF0D3AF1D6B6 (ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34 * __this, String_t* ___path0, bool ___animate1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		PropertyGroupAttribute__ctor_m97E8952CDF6B230904482158001DAB88CA24F819(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___animate1;
		__this->set_Animate_4(L_1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::.ctor(System.String,System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfGroupAttribute__ctor_m41FD43D087BA9CA6EDD5D8358DF71C64072E2E64 (ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34 * __this, String_t* ___path0, RuntimeObject * ___value1, bool ___animate2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		PropertyGroupAttribute__ctor_m97E8952CDF6B230904482158001DAB88CA24F819(__this, L_0, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___value1;
		__this->set_Value_5(L_1);
		bool L_2 = ___animate2;
		__this->set_Animate_4(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ShowIfGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowIfGroupAttribute_CombineValuesWith_mD91213E356F6BD31308F03EBBCACE8F0D16865AF (ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34 * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34 * V_0 = NULL;
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___other0;
		V_0 = ((ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34 *)IsInstClass((RuntimeObject*)L_0, ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34_il2cpp_TypeInfo_var));
		String_t* L_1 = __this->get_memberName_3();
		bool L_2;
		L_2 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34 * L_3 = V_0;
		String_t* L_4 = __this->get_memberName_3();
		NullCheck(L_3);
		L_3->set_memberName_3(L_4);
	}

IL_0020:
	{
		bool L_5 = __this->get_Animate_4();
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34 * L_6 = V_0;
		bool L_7 = __this->get_Animate_4();
		NullCheck(L_6);
		L_6->set_Animate_4(L_7);
	}

IL_0034:
	{
		RuntimeObject * L_8 = __this->get_Value_5();
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		ShowIfGroupAttribute_t9366A63CFA75608F2385B52DB4D5493200F26B34 * L_9 = V_0;
		RuntimeObject * L_10 = __this->get_Value_5();
		NullCheck(L_9);
		L_9->set_Value_5(L_10);
	}

IL_0048:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowInInlineEditorsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowInInlineEditorsAttribute__ctor_mE8C56C367F6E705FFE50ADDB98F406F49DA90543 (ShowInInlineEditorsAttribute_t074163930680127467E651ECE68C8A44528E93D2 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowInInspectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233 (ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m6F540B2A92D616275408AEB55BC97B1500F6C9B3 (ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ShowPropertyResolverAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowPropertyResolverAttribute__ctor_m065342D6D4663EFB8CFCCFCA806B323D1E7F7586 (ShowPropertyResolverAttribute_t38E4D018FDE433172E3184B2B8BB77CF841B8A9E * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.SuffixLabelAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuffixLabelAttribute__ctor_mE9AC15983814E1C792988828B1892520062B1A8F (SuffixLabelAttribute_t990C9A8E87740E342FB14097884866F5D3A9869C * __this, String_t* ___label0, bool ___overlay1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___label0;
		__this->set_Label_0(L_0);
		bool L_1 = ___overlay1;
		__this->set_Overlay_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.SuppressInvalidAttributeErrorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuppressInvalidAttributeErrorAttribute__ctor_m19653C86F9E551600A888E146263C67DB1B12421 (SuppressInvalidAttributeErrorAttribute_t8CCDC91E2DECC329BCDF59102809D220CCF7794E * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabGroupAttribute__ctor_mF8DF0A25262FF59F9CAFB79B49032DDABBA68A32 (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, String_t* ___tab0, bool ___useFixedHeight1, int32_t ___order2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6E9E619437A54F36D60249F4E555066130AFDC3C);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___tab0;
		bool L_1 = ___useFixedHeight1;
		int32_t L_2 = ___order2;
		TabGroupAttribute__ctor_m962E789C945DC9AC4EA826DCF7979A2A5733090B(__this, _stringLiteral6E9E619437A54F36D60249F4E555066130AFDC3C, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::.ctor(System.String,System.String,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabGroupAttribute__ctor_m962E789C945DC9AC4EA826DCF7979A2A5733090B (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, String_t* ___group0, String_t* ___tab1, bool ___useFixedHeight2, int32_t ___order3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m468E89F534D7F4463B96A099275295DF689B2323_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___group0;
		int32_t L_1 = ___order3;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___tab1;
		__this->set_TabName_4(L_2);
		bool L_3 = ___useFixedHeight2;
		__this->set_UseFixedHeight_5(L_3);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_4 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_4, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		TabGroupAttribute_set_Tabs_m5BB49FEBE6589933B4F2B96B581E9EE4DC5838B4_inline(__this, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___tab1;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_6;
		L_6 = TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25_inline(__this, /*hidden argument*/NULL);
		String_t* L_7 = ___tab1;
		NullCheck(L_6);
		List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE(L_6, L_7, /*hidden argument*/List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
	}

IL_0031:
	{
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_8;
		L_8 = TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25_inline(__this, /*hidden argument*/NULL);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_9 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m468E89F534D7F4463B96A099275295DF689B2323(L_9, L_8, /*hidden argument*/List_1__ctor_m468E89F534D7F4463B96A099275295DF689B2323_RuntimeMethod_var);
		TabGroupAttribute_set_Tabs_m5BB49FEBE6589933B4F2B96B581E9EE4DC5838B4_inline(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Sirenix.OdinInspector.TabGroupAttribute::get_Tabs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25 (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, const RuntimeMethod* method)
{
	{
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_U3CTabsU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::set_Tabs(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabGroupAttribute_set_Tabs_m5BB49FEBE6589933B4F2B96B581E9EE4DC5838B4 (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = ___value0;
		__this->set_U3CTabsU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TabGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabGroupAttribute_CombineValuesWith_mAF271825C72EF38F95673C2A30D695F3C6CB5C96 (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m2EAD2DADA0478175052301E48FCE772ECD9A6F5F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * V_0 = NULL;
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * G_B3_0 = NULL;
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * G_B4_1 = NULL;
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * G_B6_0 = NULL;
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * G_B7_1 = NULL;
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * G_B9_0 = NULL;
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * G_B8_0 = NULL;
	int32_t G_B10_0 = 0;
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * G_B10_1 = NULL;
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___other0;
		PropertyGroupAttribute_CombineValuesWith_mE807B73DCF4C6A9C88AA9BCD40E881733AA03AE0(__this, L_0, /*hidden argument*/NULL);
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_1 = ___other0;
		V_0 = ((TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D *)IsInstClass((RuntimeObject*)L_1, TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_il2cpp_TypeInfo_var));
		TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_TabName_4();
		if (!L_3)
		{
			goto IL_007f;
		}
	}
	{
		bool L_4 = __this->get_UseFixedHeight_5();
		G_B2_0 = __this;
		if (L_4)
		{
			G_B3_0 = __this;
			goto IL_0027;
		}
	}
	{
		TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = L_5->get_UseFixedHeight_5();
		G_B4_0 = ((int32_t)(L_6));
		G_B4_1 = G_B2_0;
		goto IL_0028;
	}

IL_0027:
	{
		G_B4_0 = 1;
		G_B4_1 = G_B3_0;
	}

IL_0028:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_UseFixedHeight_5((bool)G_B4_0);
		bool L_7 = __this->get_Paddingless_6();
		G_B5_0 = __this;
		if (L_7)
		{
			G_B6_0 = __this;
			goto IL_003e;
		}
	}
	{
		TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = L_8->get_Paddingless_6();
		G_B7_0 = ((int32_t)(L_9));
		G_B7_1 = G_B5_0;
		goto IL_003f;
	}

IL_003e:
	{
		G_B7_0 = 1;
		G_B7_1 = G_B6_0;
	}

IL_003f:
	{
		NullCheck(G_B7_1);
		G_B7_1->set_Paddingless_6((bool)G_B7_0);
		bool L_10 = __this->get_HideTabGroupIfTabGroupOnlyHasOneTab_7();
		G_B8_0 = __this;
		if (L_10)
		{
			G_B9_0 = __this;
			goto IL_0055;
		}
	}
	{
		TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = L_11->get_HideTabGroupIfTabGroupOnlyHasOneTab_7();
		G_B10_0 = ((int32_t)(L_12));
		G_B10_1 = G_B8_0;
		goto IL_0056;
	}

IL_0055:
	{
		G_B10_0 = 1;
		G_B10_1 = G_B9_0;
	}

IL_0056:
	{
		NullCheck(G_B10_1);
		G_B10_1->set_HideTabGroupIfTabGroupOnlyHasOneTab_7((bool)G_B10_0);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_13;
		L_13 = TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25_inline(__this, /*hidden argument*/NULL);
		TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = L_14->get_TabName_4();
		NullCheck(L_13);
		bool L_16;
		L_16 = List_1_Contains_m2EAD2DADA0478175052301E48FCE772ECD9A6F5F(L_13, L_15, /*hidden argument*/List_1_Contains_m2EAD2DADA0478175052301E48FCE772ECD9A6F5F_RuntimeMethod_var);
		if (L_16)
		{
			goto IL_007f;
		}
	}
	{
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_17;
		L_17 = TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25_inline(__this, /*hidden argument*/NULL);
		TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = L_18->get_TabName_4();
		NullCheck(L_17);
		List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE(L_17, L_19, /*hidden argument*/List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
	}

IL_007f:
	{
		return;
	}
}
// System.Collections.Generic.IList`1<Sirenix.OdinInspector.PropertyGroupAttribute> Sirenix.OdinInspector.TabGroupAttribute::Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute.GetSubGroupAttributes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_GetSubGroupAttributes_mB8B9EE5668DBC7DDE649C740D59FE0BBA177659B (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAE53CD7A7307ABC7A7AC23A32795DD076B4C0CB5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mAB3E3E1DD0571D6A301A9D1DF30EC3C8C49766AD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TabSubGroupAttribute_tDEC43BA5824DF3FC46C494AC92DD32CD26D8B22C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D * V_1 = NULL;
	Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  V_2;
	memset((&V_2), 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		V_0 = 0;
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0;
		L_0 = TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_inline(L_0, /*hidden argument*/List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_RuntimeMethod_var);
		List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D * L_2 = (List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D *)il2cpp_codegen_object_new(List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D_il2cpp_TypeInfo_var);
		List_1__ctor_mAB3E3E1DD0571D6A301A9D1DF30EC3C8C49766AD(L_2, L_1, /*hidden argument*/List_1__ctor_mAB3E3E1DD0571D6A301A9D1DF30EC3C8C49766AD_RuntimeMethod_var);
		V_1 = L_2;
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_3;
		L_3 = TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  L_4;
		L_4 = List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF(L_3, /*hidden argument*/List_1_GetEnumerator_m35388695226DE2F7B0B5D0A07016716D6AD9CAEF_RuntimeMethod_var);
		V_2 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004a;
		}

IL_0021:
		{
			String_t* L_5;
			L_5 = Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_inline((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_2), /*hidden argument*/Enumerator_get_Current_m9B0E356FA9FCFB9B1BECC6D7C5DF5C03309251AA_RuntimeMethod_var);
			V_3 = L_5;
			List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D * L_6 = V_1;
			String_t* L_7 = ((PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 *)__this)->get_GroupID_0();
			String_t* L_8 = V_3;
			String_t* L_9;
			L_9 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_7, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, L_8, /*hidden argument*/NULL);
			int32_t L_10 = V_0;
			int32_t L_11 = L_10;
			V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
			TabSubGroupAttribute_tDEC43BA5824DF3FC46C494AC92DD32CD26D8B22C * L_12 = (TabSubGroupAttribute_tDEC43BA5824DF3FC46C494AC92DD32CD26D8B22C *)il2cpp_codegen_object_new(TabSubGroupAttribute_tDEC43BA5824DF3FC46C494AC92DD32CD26D8B22C_il2cpp_TypeInfo_var);
			TabSubGroupAttribute__ctor_mBD891E55D898A286A39966CAB7BD0D9BF4E18F39(L_12, L_9, L_11, /*hidden argument*/NULL);
			NullCheck(L_6);
			List_1_Add_mAE53CD7A7307ABC7A7AC23A32795DD076B4C0CB5(L_6, L_12, /*hidden argument*/List_1_Add_mAE53CD7A7307ABC7A7AC23A32795DD076B4C0CB5_RuntimeMethod_var);
		}

IL_004a:
		{
			bool L_13;
			L_13 = Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_2), /*hidden argument*/Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
			if (L_13)
			{
				goto IL_0021;
			}
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x63, FINALLY_0055);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_2), /*hidden argument*/Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		IL2CPP_END_FINALLY(85)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x63, IL_0063)
	}

IL_0063:
	{
		List_1_t02EA1E971E7C27C092B7F327734AF6C37940985D * L_14 = V_1;
		return L_14;
	}
}
// System.String Sirenix.OdinInspector.TabGroupAttribute::Sirenix.OdinInspector.Internal.ISubGroupProviderAttribute.RepathMemberAttribute(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TabGroupAttribute_Sirenix_OdinInspector_Internal_ISubGroupProviderAttribute_RepathMemberAttribute_mB501706A09619041158432FBA3B9758F841C01E5 (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___attr0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		s_Il2CppMethodInitialized = true;
	}
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * V_0 = NULL;
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___attr0;
		V_0 = ((TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D *)CastclassClass((RuntimeObject*)L_0, TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_il2cpp_TypeInfo_var));
		String_t* L_1 = ((PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 *)__this)->get_GroupID_0();
		TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_TabName_4();
		String_t* L_4;
		L_4 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_1, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TableColumnWidthAttribute::.ctor(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TableColumnWidthAttribute__ctor_mE43C5003B86E8E2A70D71C549C3DE40D9F94CBC0 (TableColumnWidthAttribute_t62FFBE33C21C9BCA7419C73D67C20BBF291671BF * __this, int32_t ___width0, bool ___resizable1, const RuntimeMethod* method)
{
	{
		__this->set_Resizable_1((bool)1);
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		__this->set_Width_0(L_0);
		bool L_1 = ___resizable1;
		__this->set_Resizable_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.TableListAttribute::get_ShowPaging()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TableListAttribute_get_ShowPaging_m4D907C1FCB384775DEBB760E88D10BB129776DAF (TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showPaging_11();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.TableListAttribute::set_ShowPaging(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TableListAttribute_set_ShowPaging_m3827B6C698B94CE02171869DD8E9F1C8F4647688 (TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_showPaging_11(L_0);
		__this->set_showPagingHasValue_10((bool)1);
		return;
	}
}
// System.Boolean Sirenix.OdinInspector.TableListAttribute::get_ShowPagingHasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TableListAttribute_get_ShowPagingHasValue_mDFE223AB05B1B0E12517F5E7306273EB0EC6ED67 (TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_showPagingHasValue_10();
		return L_0;
	}
}
// System.Int32 Sirenix.OdinInspector.TableListAttribute::get_ScrollViewHeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TableListAttribute_get_ScrollViewHeight_m9832A51C2B33CAFE93578C379313EC9CC0F98312 (TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_MinScrollViewHeight_5();
		int32_t L_1 = __this->get_MaxScrollViewHeight_6();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_2;
		L_2 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Sirenix.OdinInspector.TableListAttribute::set_ScrollViewHeight(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TableListAttribute_set_ScrollViewHeight_mB27783D51812DBD474A5804E9FD91183735F7166 (TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = L_0;
		V_0 = L_1;
		__this->set_MaxScrollViewHeight_6(L_1);
		int32_t L_2 = V_0;
		__this->set_MinScrollViewHeight_5(L_2);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TableListAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TableListAttribute__ctor_mA5BD0E2E7D0A0633559E1908447483EF6A265CA2 (TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9 * __this, const RuntimeMethod* method)
{
	{
		__this->set_DefaultMinColumnWidth_2(((int32_t)40));
		__this->set_DrawScrollView_4((bool)1);
		__this->set_MinScrollViewHeight_5(((int32_t)350));
		__this->set_CellPadding_9(2);
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TableMatrixAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TableMatrixAttribute__ctor_m66477FB1BF84EAFF3518CF83B2CA47CCC1D221E1 (TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E * __this, const RuntimeMethod* method)
{
	{
		__this->set_ResizableColumns_1((bool)1);
		__this->set_RespectIndentLevel_9((bool)1);
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TitleAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.TitleAlignments,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TitleAttribute__ctor_m2EEB321011887FAD4345C6DE1C9540114AF6765C (TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44 * __this, String_t* ___title0, String_t* ___subtitle1, int32_t ___titleAlignment2, bool ___horizontalLine3, bool ___bold4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5BEFD8CC60A79699B5BB00E37BAC5B62D371E174);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44 * G_B1_1 = NULL;
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___title0;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0011;
		}
	}
	{
		G_B2_0 = _stringLiteral5BEFD8CC60A79699B5BB00E37BAC5B62D371E174;
		G_B2_1 = G_B1_1;
	}

IL_0011:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_Title_0(G_B2_0);
		String_t* L_2 = ___subtitle1;
		__this->set_Subtitle_1(L_2);
		bool L_3 = ___bold4;
		__this->set_Bold_2(L_3);
		int32_t L_4 = ___titleAlignment2;
		__this->set_TitleAlignment_4(L_4);
		bool L_5 = ___horizontalLine3;
		__this->set_HorizontalLine_3(L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TitleGroupAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.TitleAlignments,System.Boolean,System.Boolean,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TitleGroupAttribute__ctor_mF5CAD9A96B1403DA735CDB0DCF9655492EEA329D (TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * __this, String_t* ___title0, String_t* ___subtitle1, int32_t ___alignment2, bool ___horizontalLine3, bool ___boldTitle4, bool ___indent5, int32_t ___order6, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___title0;
		int32_t L_1 = ___order6;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___subtitle1;
		__this->set_Subtitle_3(L_2);
		int32_t L_3 = ___alignment2;
		__this->set_Alignment_4(L_3);
		bool L_4 = ___horizontalLine3;
		__this->set_HorizontalLine_5(L_4);
		bool L_5 = ___boldTitle4;
		__this->set_BoldTitle_6(L_5);
		bool L_6 = ___indent5;
		__this->set_Indent_7(L_6);
		return;
	}
}
// System.Void Sirenix.OdinInspector.TitleGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TitleGroupAttribute_CombineValuesWith_mC9C34C1AFDA9BCA6F89BC30EBC3F4488F5B2BE1F (TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * V_0 = NULL;
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___other0;
		V_0 = ((TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED *)IsInstSealed((RuntimeObject*)L_0, TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED_il2cpp_TypeInfo_var));
		String_t* L_1 = __this->get_Subtitle_3();
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * L_2 = V_0;
		String_t* L_3 = __this->get_Subtitle_3();
		NullCheck(L_2);
		L_2->set_Subtitle_3(L_3);
		goto IL_0029;
	}

IL_001d:
	{
		TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_Subtitle_3();
		__this->set_Subtitle_3(L_5);
	}

IL_0029:
	{
		int32_t L_6 = __this->get_Alignment_4();
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * L_7 = V_0;
		int32_t L_8 = __this->get_Alignment_4();
		NullCheck(L_7);
		L_7->set_Alignment_4(L_8);
		goto IL_004b;
	}

IL_003f:
	{
		TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_Alignment_4();
		__this->set_Alignment_4(L_10);
	}

IL_004b:
	{
		bool L_11 = __this->get_HorizontalLine_5();
		if (L_11)
		{
			goto IL_0061;
		}
	}
	{
		TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * L_12 = V_0;
		bool L_13 = __this->get_HorizontalLine_5();
		NullCheck(L_12);
		L_12->set_HorizontalLine_5(L_13);
		goto IL_006d;
	}

IL_0061:
	{
		TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * L_14 = V_0;
		NullCheck(L_14);
		bool L_15 = L_14->get_HorizontalLine_5();
		__this->set_HorizontalLine_5(L_15);
	}

IL_006d:
	{
		bool L_16 = __this->get_BoldTitle_6();
		if (L_16)
		{
			goto IL_0083;
		}
	}
	{
		TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * L_17 = V_0;
		bool L_18 = __this->get_BoldTitle_6();
		NullCheck(L_17);
		L_17->set_BoldTitle_6(L_18);
		goto IL_008f;
	}

IL_0083:
	{
		TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = L_19->get_BoldTitle_6();
		__this->set_BoldTitle_6(L_20);
	}

IL_008f:
	{
		bool L_21 = __this->get_Indent_7();
		if (!L_21)
		{
			goto IL_00a4;
		}
	}
	{
		TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * L_22 = V_0;
		bool L_23 = __this->get_Indent_7();
		NullCheck(L_22);
		L_22->set_Indent_7(L_23);
		return;
	}

IL_00a4:
	{
		TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED * L_24 = V_0;
		NullCheck(L_24);
		bool L_25 = L_24->get_Indent_7();
		__this->set_Indent_7(L_25);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ToggleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleAttribute__ctor_m04A2021D61D0895881BB99A0958EDED31ED2C12A (ToggleAttribute_tE3BA633AE4C4A847529A9FEB83DFF3C17CB83F2B * __this, String_t* ___toggleMemberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___toggleMemberName0;
		__this->set_ToggleMemberName_0(L_0);
		__this->set_CollapseOthersOnExpand_1((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_m02F6C8A534512612492AB9C3C29227776F25CC66 (ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * __this, String_t* ___toggleMemberName0, int32_t ___order1, String_t* ___groupTitle2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___toggleMemberName0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___groupTitle2;
		__this->set_ToggleGroupTitle_3(L_2);
		__this->set_CollapseOthersOnExpand_4((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_m9B9BE888CACC61FEB298262544065950F500F85E (ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * __this, String_t* ___toggleMemberName0, String_t* ___groupTitle1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___toggleMemberName0;
		String_t* L_1 = ___groupTitle1;
		ToggleGroupAttribute__ctor_m02F6C8A534512612492AB9C3C29227776F25CC66(__this, L_0, 0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::.ctor(System.String,System.Int32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute__ctor_mCE42334D8F057E33C717935C60446068B17B1AE8 (ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * __this, String_t* ___toggleMemberName0, int32_t ___order1, String_t* ___groupTitle2, String_t* ___titleStringMemberName3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___toggleMemberName0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___groupTitle2;
		__this->set_ToggleGroupTitle_3(L_2);
		__this->set_CollapseOthersOnExpand_4((bool)1);
		return;
	}
}
// System.String Sirenix.OdinInspector.ToggleGroupAttribute::get_ToggleMemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ToggleGroupAttribute_get_ToggleMemberName_m04044CD0EAD5C59A69AAC83ED7C8DCF6D4C0A261 (ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ((PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 *)__this)->get_GroupName_1();
		return L_0;
	}
}
// System.String Sirenix.OdinInspector.ToggleGroupAttribute::get_TitleStringMemberName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ToggleGroupAttribute_get_TitleStringMemberName_m7A12EF988A3F9A85FAAAEF843B62E52C0BF10D20 (ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleStringMemberNameU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::set_TitleStringMemberName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute_set_TitleStringMemberName_m3354817FD279639CAE69FF7C3B431854763B55CF (ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleStringMemberNameU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ToggleGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleGroupAttribute_CombineValuesWith_mF7D00256747558CF46D44456114681365FF5DAC4 (ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * V_0 = NULL;
	ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * G_B6_0 = NULL;
	ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * G_B7_1 = NULL;
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___other0;
		V_0 = ((ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 *)IsInstSealed((RuntimeObject*)L_0, ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_il2cpp_TypeInfo_var));
		String_t* L_1 = __this->get_ToggleGroupTitle_3();
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_ToggleGroupTitle_3();
		__this->set_ToggleGroupTitle_3(L_3);
		goto IL_0031;
	}

IL_001d:
	{
		ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_ToggleGroupTitle_3();
		if (L_5)
		{
			goto IL_0031;
		}
	}
	{
		ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * L_6 = V_0;
		String_t* L_7 = __this->get_ToggleGroupTitle_3();
		NullCheck(L_6);
		L_6->set_ToggleGroupTitle_3(L_7);
	}

IL_0031:
	{
		bool L_8 = __this->get_CollapseOthersOnExpand_4();
		G_B5_0 = __this;
		if (L_8)
		{
			G_B6_0 = __this;
			goto IL_0042;
		}
	}
	{
		ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = L_9->get_CollapseOthersOnExpand_4();
		G_B7_0 = ((int32_t)(L_10));
		G_B7_1 = G_B5_0;
		goto IL_0043;
	}

IL_0042:
	{
		G_B7_0 = 1;
		G_B7_1 = G_B6_0;
	}

IL_0043:
	{
		NullCheck(G_B7_1);
		G_B7_1->set_CollapseOthersOnExpand_4((bool)G_B7_0);
		ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00 * L_11 = V_0;
		bool L_12 = __this->get_CollapseOthersOnExpand_4();
		NullCheck(L_11);
		L_11->set_CollapseOthersOnExpand_4(L_12);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ToggleLeftAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToggleLeftAttribute__ctor_mF3B2E5B94E8017E707C11B8A24245E466A41B370 (ToggleLeftAttribute_t4CC8B95C528CF91FEA46AABBC6237B2258FD77C9 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TypeFilterAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TypeFilterAttribute__ctor_m6A29E4806E3CEDF292BF06AEF8D0817686E4BF3A (TypeFilterAttribute_t7989849079B6440606CBDA93E5AE67B4877A56FE * __this, String_t* ___memberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TypeInfoBoxAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TypeInfoBoxAttribute__ctor_mF44207CB9379DE5CCBA2598235C57141B55D9221 (TypeInfoBoxAttribute_t43B3E84724467282A1E3696E35325EF8C66E4BBD * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		__this->set_Message_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Sirenix.OdinInspector.ValidateInputAttribute::get_ContiniousValidationCheck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ValidateInputAttribute_get_ContiniousValidationCheck_m57675CD7DD7EFACDD1242343307F36739A70ADD8 (ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_ContinuousValidationCheck_4();
		return L_0;
	}
}
// System.Void Sirenix.OdinInspector.ValidateInputAttribute::set_ContiniousValidationCheck(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValidateInputAttribute_set_ContiniousValidationCheck_m7EF3D2DB874DACA93D72468D6F5B6E371B832468 (ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_ContinuousValidationCheck_4(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ValidateInputAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValidateInputAttribute__ctor_m8515D985050435F347600891E06340B5AE18C879 (ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE * __this, String_t* ___memberName0, String_t* ___defaultMessage1, int32_t ___messageType2, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_1(L_0);
		String_t* L_1 = ___defaultMessage1;
		__this->set_DefaultMessage_0(L_1);
		int32_t L_2 = ___messageType2;
		__this->set_MessageType_2(L_2);
		__this->set_IncludeChildren_3((bool)1);
		return;
	}
}
// System.Void Sirenix.OdinInspector.ValidateInputAttribute::.ctor(System.String,System.String,Sirenix.OdinInspector.InfoMessageType,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValidateInputAttribute__ctor_m64BCF6EB7B972FD8563BA5A7E14CFEA80D122038 (ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE * __this, String_t* ___memberName0, String_t* ___message1, int32_t ___messageType2, bool ___rejectedInvalidInput3, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_1(L_0);
		String_t* L_1 = ___message1;
		__this->set_DefaultMessage_0(L_1);
		int32_t L_2 = ___messageType2;
		__this->set_MessageType_2(L_2);
		__this->set_IncludeChildren_3((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.ValueDropdownAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueDropdownAttribute__ctor_mD364F567BD9B8DFA8169C42D6256502B253ED3B8 (ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671 * __this, String_t* ___memberName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		__this->set_NumberOfItemsBeforeEnablingSearch_1(((int32_t)10));
		String_t* L_0 = ___memberName0;
		__this->set_MemberName_0(L_0);
		__this->set_DrawDropdownForListElements_3((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Sirenix.OdinInspector.ValueDropdownItem
IL2CPP_EXTERN_C void ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshal_pinvoke(const ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E& unmarshaled, ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshaled_pinvoke& marshaled)
{
	marshaled.___Text_0 = il2cpp_codegen_marshal_string(unmarshaled.get_Text_0());
	if (unmarshaled.get_Value_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_Value_1()))
		{
			marshaled.___Value_1 = il2cpp_codegen_com_query_interface<Il2CppIUnknown>(static_cast<Il2CppComObject*>(unmarshaled.get_Value_1()));
			(marshaled.___Value_1)->AddRef();
		}
		else
		{
			marshaled.___Value_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_Value_1());
		}
	}
	else
	{
		marshaled.___Value_1 = NULL;
	}
}
IL2CPP_EXTERN_C void ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshal_pinvoke_back(const ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshaled_pinvoke& marshaled, ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Il2CppComObject_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_Text_0(il2cpp_codegen_marshal_string_result(marshaled.___Text_0));
	if (marshaled.___Value_1 != NULL)
	{
		unmarshaled.set_Value_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___Value_1, Il2CppComObject_il2cpp_TypeInfo_var));

		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_Value_1()))
		{
			il2cpp_codegen_com_cache_queried_interface(static_cast<Il2CppComObject*>(unmarshaled.get_Value_1()), Il2CppIUnknown::IID, marshaled.___Value_1);
		}
	}
	else
	{
		unmarshaled.set_Value_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: Sirenix.OdinInspector.ValueDropdownItem
IL2CPP_EXTERN_C void ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshal_pinvoke_cleanup(ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Text_0);
	marshaled.___Text_0 = NULL;
	if (marshaled.___Value_1 != NULL)
	{
		(marshaled.___Value_1)->Release();
		marshaled.___Value_1 = NULL;
	}
}
// Conversion methods for marshalling of: Sirenix.OdinInspector.ValueDropdownItem
IL2CPP_EXTERN_C void ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshal_com(const ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E& unmarshaled, ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshaled_com& marshaled)
{
	marshaled.___Text_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Text_0());
	if (unmarshaled.get_Value_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_Value_1()))
		{
			marshaled.___Value_1 = il2cpp_codegen_com_query_interface<Il2CppIUnknown>(static_cast<Il2CppComObject*>(unmarshaled.get_Value_1()));
			(marshaled.___Value_1)->AddRef();
		}
		else
		{
			marshaled.___Value_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_Value_1());
		}
	}
	else
	{
		marshaled.___Value_1 = NULL;
	}
}
IL2CPP_EXTERN_C void ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshal_com_back(const ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshaled_com& marshaled, ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Il2CppComObject_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.set_Text_0(il2cpp_codegen_marshal_bstring_result(marshaled.___Text_0));
	if (marshaled.___Value_1 != NULL)
	{
		unmarshaled.set_Value_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___Value_1, Il2CppComObject_il2cpp_TypeInfo_var));

		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_Value_1()))
		{
			il2cpp_codegen_com_cache_queried_interface(static_cast<Il2CppComObject*>(unmarshaled.get_Value_1()), Il2CppIUnknown::IID, marshaled.___Value_1);
		}
	}
	else
	{
		unmarshaled.set_Value_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: Sirenix.OdinInspector.ValueDropdownItem
IL2CPP_EXTERN_C void ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshal_com_cleanup(ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___Text_0);
	marshaled.___Text_0 = NULL;
	if (marshaled.___Value_1 != NULL)
	{
		(marshaled.___Value_1)->Release();
		marshaled.___Value_1 = NULL;
	}
}
// System.Void Sirenix.OdinInspector.ValueDropdownItem::.ctor(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueDropdownItem__ctor_mCCB87C57D5FF6019771592E2E52B910C13590ACC (ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * __this, String_t* ___text0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___text0;
		__this->set_Text_0(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_Value_1(L_1);
		return;
	}
}
IL2CPP_EXTERN_C  void ValueDropdownItem__ctor_mCCB87C57D5FF6019771592E2E52B910C13590ACC_AdjustorThunk (RuntimeObject * __this, String_t* ___text0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E *>(__this + _offset);
	ValueDropdownItem__ctor_mCCB87C57D5FF6019771592E2E52B910C13590ACC(_thisAdjusted, ___text0, ___value1, method);
}
// System.String Sirenix.OdinInspector.ValueDropdownItem::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_ToString_m809FC023CF5BE24E964513E064156CC3E405F300 (ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * __this, const RuntimeMethod* method)
{
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get_Text_0();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0015;
		}
	}
	{
		RuntimeObject * L_2 = __this->get_Value_1();
		String_t* L_3;
		L_3 = String_Concat_mD3809D54FDAC43AA11084A9FE53165D57A6153FF(L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
	}

IL_0015:
	{
		return G_B2_0;
	}
}
IL2CPP_EXTERN_C  String_t* ValueDropdownItem_ToString_m809FC023CF5BE24E964513E064156CC3E405F300_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = ValueDropdownItem_ToString_m809FC023CF5BE24E964513E064156CC3E405F300(_thisAdjusted, method);
	return _returnValue;
}
// System.String Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetText()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mDE36C60532902791E5E6C2AF39297AD60F3ED9B1 (ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Text_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C  String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mDE36C60532902791E5E6C2AF39297AD60F3ED9B1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mDE36C60532902791E5E6C2AF39297AD60F3ED9B1_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.Object Sirenix.OdinInspector.ValueDropdownItem::Sirenix.OdinInspector.IValueDropdownItem.GetValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m8FB6D5B472D5D524135DE9A0DBB78CACC7D617A2 (ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_Value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m8FB6D5B472D5D524135DE9A0DBB78CACC7D617A2_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * _thisAdjusted = reinterpret_cast<ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E *>(__this + _offset);
	RuntimeObject * _returnValue;
	_returnValue = ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m8FB6D5B472D5D524135DE9A0DBB78CACC7D617A2_inline(_thisAdjusted, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VerticalGroupAttribute__ctor_m95C002377FFF8A8BFFCE1F1E1C9179A471E5CE06 (VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupId0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VerticalGroupAttribute__ctor_mD2074E6027586EE28B3E9B869A1A45F74DA01841 (VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF * __this, int32_t ___order0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA46F01FBD653440FE1C9E92E2A2D40AAB3645653);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___order0;
		VerticalGroupAttribute__ctor_m95C002377FFF8A8BFFCE1F1E1C9179A471E5CE06(__this, _stringLiteralA46F01FBD653440FE1C9E92E2A2D40AAB3645653, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sirenix.OdinInspector.VerticalGroupAttribute::CombineValuesWith(Sirenix.OdinInspector.PropertyGroupAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VerticalGroupAttribute_CombineValuesWith_mD0C5B314858254CC72D5783B6C644F91D8A80820 (VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF * __this, PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF * V_0 = NULL;
	{
		PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1 * L_0 = ___other0;
		V_0 = ((VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF *)IsInstClass((RuntimeObject*)L_0, VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF_il2cpp_TypeInfo_var));
		VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF * L_1 = V_0;
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF * L_2 = V_0;
		NullCheck(L_2);
		float L_3 = L_2->get_PaddingTop_3();
		if ((((float)L_3) == ((float)(0.0f))))
		{
			goto IL_0023;
		}
	}
	{
		VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF * L_4 = V_0;
		NullCheck(L_4);
		float L_5 = L_4->get_PaddingTop_3();
		__this->set_PaddingTop_3(L_5);
	}

IL_0023:
	{
		VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF * L_6 = V_0;
		NullCheck(L_6);
		float L_7 = L_6->get_PaddingBottom_4();
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_003c;
		}
	}
	{
		VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF * L_8 = V_0;
		NullCheck(L_8);
		float L_9 = L_8->get_PaddingBottom_4();
		__this->set_PaddingBottom_4(L_9);
	}

IL_003c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.WrapAttribute::.ctor(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WrapAttribute__ctor_mCBFEB1652B6B6062E9D5620E2352B6305F591802 (WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930 * __this, double ___min0, double ___max1, const RuntimeMethod* method)
{
	WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930 * G_B2_0 = NULL;
	WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930 * G_B1_0 = NULL;
	double G_B3_0 = 0.0;
	WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930 * G_B3_1 = NULL;
	WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930 * G_B5_0 = NULL;
	WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930 * G_B4_0 = NULL;
	double G_B6_0 = 0.0;
	WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930 * G_B6_1 = NULL;
	{
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		double L_1 = ___max1;
		G_B1_0 = __this;
		if ((((double)L_0) < ((double)L_1)))
		{
			G_B2_0 = __this;
			goto IL_000e;
		}
	}
	{
		double L_2 = ___max1;
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_000f;
	}

IL_000e:
	{
		double L_3 = ___min0;
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_000f:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_Min_0(G_B3_0);
		double L_4 = ___max1;
		double L_5 = ___min0;
		G_B4_0 = __this;
		if ((((double)L_4) > ((double)L_5)))
		{
			G_B5_0 = __this;
			goto IL_001c;
		}
	}
	{
		double L_6 = ___min0;
		G_B6_0 = L_6;
		G_B6_1 = G_B4_0;
		goto IL_001d;
	}

IL_001c:
	{
		double L_7 = ___max1;
		G_B6_0 = L_7;
		G_B6_1 = G_B5_0;
	}

IL_001d:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_Max_1(G_B6_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.AssetSelectorAttribute/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m7094437B7D37D1E6FF4DD391765535E524599282 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6 * L_0 = (U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6 *)il2cpp_codegen_object_new(U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mEF4D05798CE3F378E5E36DF88F14353C7CF9E182(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Sirenix.OdinInspector.AssetSelectorAttribute/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mEF4D05798CE3F378E5E36DF88F14353C7CF9E182 (U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Sirenix.OdinInspector.AssetSelectorAttribute/<>c::<set_Paths>b__12_0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3Cset_PathsU3Eb__12_0_mB1598E3D172430978C835A9B4E1A1268891DF76D (U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6 * __this, String_t* ___x0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = String_Trim_m3FEC641D7046124B7F381701903B50B5171DE0A2(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Sirenix.OdinInspector.TabGroupAttribute/TabSubGroupAttribute::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TabSubGroupAttribute__ctor_mBD891E55D898A286A39966CAB7BD0D9BF4E18F39 (TabSubGroupAttribute_tDEC43BA5824DF3FC46C494AC92DD32CD26D8B22C * __this, String_t* ___groupId0, int32_t ___order1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___groupId0;
		int32_t L_1 = ___order1;
		PropertyGroupAttribute__ctor_mB3B403FE71990F2A611C2B351AEA3439CEC6B0DA(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void FoldoutGroupAttribute_set_HasDefinedExpanded_mABAA99EDDE953EEF83E4F04F80DAC78D2E3851B3_inline (FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CHasDefinedExpandedU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool FoldoutGroupAttribute_get_HasDefinedExpanded_mF65E7E54B706CCDE7EA9DB0C3606F2B10DCFCB25_inline (FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CHasDefinedExpandedU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_MemberMethod_mF0C22FEE372BFE43D98A7B8FDD6BD29365CEDAE8_inline (InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CMemberMethodU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InlineButtonAttribute_set_Label_m24E6BE08859E432C9819B26B95BED6F00C3DC0D0_inline (InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CLabelU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624_inline (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CDrawValueLabelHasValueU3Ek__BackingField_14(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0_inline (ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CValueLabelAlignmentHasValueU3Ek__BackingField_15(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TabGroupAttribute_set_Tabs_m5BB49FEBE6589933B4F2B96B581E9EE4DC5838B4_inline (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = ___value0;
		__this->set_U3CTabsU3Ek__BackingField_8(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25_inline (TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D * __this, const RuntimeMethod* method)
{
	{
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_U3CTabsU3Ek__BackingField_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetText_mDE36C60532902791E5E6C2AF39297AD60F3ED9B1_inline (ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Text_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * ValueDropdownItem_Sirenix_OdinInspector_IValueDropdownItem_GetValue_m8FB6D5B472D5D524135DE9A0DBB78CACC7D617A2_inline (ValueDropdownItem_t450A391FC6A9899FA9FD4AAC8C87F3D3FFE4769E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_Value_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
