﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Sirenix.Serialization.CustomLogger::.ctor(System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.Exception>)
extern void CustomLogger__ctor_mE5413C0D4C53C5785EAC1B009B3E80602729B6F2 (void);
// 0x00000002 System.Void Sirenix.Serialization.CustomLogger::LogWarning(System.String)
extern void CustomLogger_LogWarning_mFE7AFCFA5316978C58DD7C82DCC57B3A0B183ACA (void);
// 0x00000003 System.Void Sirenix.Serialization.CustomLogger::LogError(System.String)
extern void CustomLogger_LogError_m21A8D84479D82AFAE28CD155B9167E4BE5270974 (void);
// 0x00000004 System.Void Sirenix.Serialization.CustomLogger::LogException(System.Exception)
extern void CustomLogger_LogException_mCD689934ED2D13D4EB086519B8D0EB46FCDB666C (void);
// 0x00000005 Sirenix.Serialization.ILogger Sirenix.Serialization.DefaultLoggers::get_DefaultLogger()
extern void DefaultLoggers_get_DefaultLogger_m3B6E58ECECAABC5E115CF85B1984B63DA19FF706 (void);
// 0x00000006 Sirenix.Serialization.ILogger Sirenix.Serialization.DefaultLoggers::get_UnityLogger()
extern void DefaultLoggers_get_UnityLogger_mE41B6564C4A1B4DA935843B517AFFF76D827303C (void);
// 0x00000007 System.Void Sirenix.Serialization.DefaultLoggers::.cctor()
extern void DefaultLoggers__cctor_mE38AB9993471C504A7F659F7DD415B65C738EAAD (void);
// 0x00000008 Sirenix.Serialization.ILogger Sirenix.Serialization.GlobalSerializationConfig::get_Logger()
extern void GlobalSerializationConfig_get_Logger_mF7C61DAB3AFF0C840A8B09984E8E368262EF3CBF (void);
// 0x00000009 Sirenix.Serialization.DataFormat Sirenix.Serialization.GlobalSerializationConfig::get_EditorSerializationFormat()
extern void GlobalSerializationConfig_get_EditorSerializationFormat_m41F7D781601A39D80151EBEFCF91517F47D1979C (void);
// 0x0000000A System.Void Sirenix.Serialization.GlobalSerializationConfig::set_EditorSerializationFormat(Sirenix.Serialization.DataFormat)
extern void GlobalSerializationConfig_set_EditorSerializationFormat_mC4ECD7457044A2753C3A8ED6DD8402C57E8EFCDA (void);
// 0x0000000B Sirenix.Serialization.DataFormat Sirenix.Serialization.GlobalSerializationConfig::get_BuildSerializationFormat()
extern void GlobalSerializationConfig_get_BuildSerializationFormat_m28AB3EA7BB193615F1358A7FE42A43841602D8D3 (void);
// 0x0000000C System.Void Sirenix.Serialization.GlobalSerializationConfig::set_BuildSerializationFormat(Sirenix.Serialization.DataFormat)
extern void GlobalSerializationConfig_set_BuildSerializationFormat_mB68D82918CFE60A107F63572714147A3CEEB6063 (void);
// 0x0000000D Sirenix.Serialization.LoggingPolicy Sirenix.Serialization.GlobalSerializationConfig::get_LoggingPolicy()
extern void GlobalSerializationConfig_get_LoggingPolicy_m18782ADC0023C139DDFEA63C629B9DEE5C1D8B04 (void);
// 0x0000000E System.Void Sirenix.Serialization.GlobalSerializationConfig::set_LoggingPolicy(Sirenix.Serialization.LoggingPolicy)
extern void GlobalSerializationConfig_set_LoggingPolicy_mD9AA73326BDC3589274D32E7DABFF8B3C9792CE6 (void);
// 0x0000000F Sirenix.Serialization.ErrorHandlingPolicy Sirenix.Serialization.GlobalSerializationConfig::get_ErrorHandlingPolicy()
extern void GlobalSerializationConfig_get_ErrorHandlingPolicy_mF66A0B7D9F6B18F90A36A8B51F35C69E52FF96A7 (void);
// 0x00000010 System.Void Sirenix.Serialization.GlobalSerializationConfig::set_ErrorHandlingPolicy(Sirenix.Serialization.ErrorHandlingPolicy)
extern void GlobalSerializationConfig_set_ErrorHandlingPolicy_m9705A4998ECD43616EDBB49695ED5DDDFF571ECC (void);
// 0x00000011 System.Void Sirenix.Serialization.GlobalSerializationConfig::OnInspectorGUI()
extern void GlobalSerializationConfig_OnInspectorGUI_m3FE0EFD4FD9E27D4D05C84AD91A4237895D4DE31 (void);
// 0x00000012 System.Void Sirenix.Serialization.GlobalSerializationConfig::.ctor()
extern void GlobalSerializationConfig__ctor_mE94B3B161307618EFE92FEAC82B2A28EFD2B41BD (void);
// 0x00000013 System.Void Sirenix.Serialization.GlobalSerializationConfig::.cctor()
extern void GlobalSerializationConfig__cctor_m67C47913261A23BB3F395A09E07F369267C4FE7C (void);
// 0x00000014 System.Void Sirenix.Serialization.ILogger::LogWarning(System.String)
// 0x00000015 System.Void Sirenix.Serialization.ILogger::LogError(System.String)
// 0x00000016 System.Void Sirenix.Serialization.ILogger::LogException(System.Exception)
static Il2CppMethodPointer s_methodPointers[22] = 
{
	CustomLogger__ctor_mE5413C0D4C53C5785EAC1B009B3E80602729B6F2,
	CustomLogger_LogWarning_mFE7AFCFA5316978C58DD7C82DCC57B3A0B183ACA,
	CustomLogger_LogError_m21A8D84479D82AFAE28CD155B9167E4BE5270974,
	CustomLogger_LogException_mCD689934ED2D13D4EB086519B8D0EB46FCDB666C,
	DefaultLoggers_get_DefaultLogger_m3B6E58ECECAABC5E115CF85B1984B63DA19FF706,
	DefaultLoggers_get_UnityLogger_mE41B6564C4A1B4DA935843B517AFFF76D827303C,
	DefaultLoggers__cctor_mE38AB9993471C504A7F659F7DD415B65C738EAAD,
	GlobalSerializationConfig_get_Logger_mF7C61DAB3AFF0C840A8B09984E8E368262EF3CBF,
	GlobalSerializationConfig_get_EditorSerializationFormat_m41F7D781601A39D80151EBEFCF91517F47D1979C,
	GlobalSerializationConfig_set_EditorSerializationFormat_mC4ECD7457044A2753C3A8ED6DD8402C57E8EFCDA,
	GlobalSerializationConfig_get_BuildSerializationFormat_m28AB3EA7BB193615F1358A7FE42A43841602D8D3,
	GlobalSerializationConfig_set_BuildSerializationFormat_mB68D82918CFE60A107F63572714147A3CEEB6063,
	GlobalSerializationConfig_get_LoggingPolicy_m18782ADC0023C139DDFEA63C629B9DEE5C1D8B04,
	GlobalSerializationConfig_set_LoggingPolicy_mD9AA73326BDC3589274D32E7DABFF8B3C9792CE6,
	GlobalSerializationConfig_get_ErrorHandlingPolicy_mF66A0B7D9F6B18F90A36A8B51F35C69E52FF96A7,
	GlobalSerializationConfig_set_ErrorHandlingPolicy_m9705A4998ECD43616EDBB49695ED5DDDFF571ECC,
	GlobalSerializationConfig_OnInspectorGUI_m3FE0EFD4FD9E27D4D05C84AD91A4237895D4DE31,
	GlobalSerializationConfig__ctor_mE94B3B161307618EFE92FEAC82B2A28EFD2B41BD,
	GlobalSerializationConfig__cctor_m67C47913261A23BB3F395A09E07F369267C4FE7C,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[22] = 
{
	488,
	1241,
	1241,
	1241,
	2445,
	2445,
	2458,
	1440,
	1428,
	1231,
	1428,
	1231,
	1428,
	1231,
	1428,
	1231,
	1476,
	1476,
	2458,
	1241,
	1241,
	1241,
};
extern const CustomAttributesCacheGenerator g_Sirenix_Serialization_Config_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Sirenix_Serialization_Config_CodeGenModule;
const Il2CppCodeGenModule g_Sirenix_Serialization_Config_CodeGenModule = 
{
	"Sirenix.Serialization.Config.dll",
	22,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Sirenix_Serialization_Config_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
