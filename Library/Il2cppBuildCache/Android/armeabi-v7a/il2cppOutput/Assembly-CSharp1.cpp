﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task>
struct Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8;
// System.Func`1<System.Threading.Tasks.Task/ContingentProperties>
struct Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B;
// System.Func`2<UnityEngine.MeshRenderer,UnityEngine.Mesh>
struct Func_2_t798C577C946B09A44DFDCDFECC6D23AADE937404;
// System.Func`2<Program.Option.OptionObject,System.Object>
struct Func_2_t2B142F97BDB708FE8BDA2E4A9B7B5B2AFC2B0E6A;
// System.Func`2<ProjectPalitra,Palitra>
struct Func_2_t7A18FE33D332E9149ED1B210BB4F36CF337A155A;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<Program.Editor.GetPixels/Triangle>
struct IEnumerable_1_t6C6B4C2A35203B41CBB173E20525BEA8E95F2F4A;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E;
// System.Collections.Generic.List`1<ItemFile>
struct List_1_t2AF58D691E5BC2E9210D1E7E43FBAF6451097E78;
// System.Collections.Generic.List`1<Program.Menu.ItemNews>
struct List_1_t1097B7FA8F0449D8B1D7C2B0B2997E5E22D23310;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<Program.UI.PalitraItem>
struct List_1_t0B6165ABABF6AD736F24984ADFC2EC3B92936B27;
// System.Collections.Generic.List`1<Program.Editor.GetPixels/Triangle>
struct List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923;
// System.Collections.Generic.List`1<Program.Editor.Model/Story>
struct List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD;
// System.Predicate`1<System.Object>
struct Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB;
// System.Predicate`1<System.Threading.Tasks.Task>
struct Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// Program.UI.ColorItem[]
struct ColorItemU5BU5D_t21E2528356087A131BD727F2826714C4CDF04E3C;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// FilerServer.News[]
struct NewsU5BU5D_t7CD4BFBC754A2A366E68BF321543B7DEF517F927;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// FilerServer.Texture[]
struct TextureU5BU5D_tFC397DCC79C0EAD5839F838C9E694ACB8C7C331B;
// TextureFile[]
struct TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B;
// System.UInt16[]
struct UInt16U5BU5D_t42D35C587B07DCDBCFDADF572C6D733AE85B2A67;
// Program.Editor.GetPixels/Triangle[]
struct TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79;
// Program.Editor.Model/Story[]
struct StoryU5BU5D_t814EAE97194FB4969BF0807B637425F16593A40C;
// FilerServer.Texture/Pixel[]
struct PixelU5BU5D_t4C1B73D1643C9DB7566C12B119C443AF0417057E;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// Program.Audio.AudioController
struct AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// Program.Menu.BaseMenu
struct BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475;
// Program.UI.BodyParts
struct BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1;
// Program.UI.ColorItem
struct ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650;
// Program.UI.ColorPalitra
struct ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3;
// Program.UI.ColorPickerPanel
struct ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// System.Threading.ContextCallback
struct ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// Program.UI.DownLayer
struct DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C;
// System.Net.EndPoint
struct EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA;
// System.Exception
struct Exception_t;
// Program.Menu.FileNews
struct FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A;
// Program.Editor.FileTextures
struct FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// Program.UI.HistoryButton
struct HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tAE063F84A60E1058FCA4E3EA9F555D3462641F7D;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.IOAsyncCallback
struct IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E;
// System.Net.IPAddress
struct IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE;
// System.Net.IPEndPoint
struct IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.UI.InputField
struct InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0;
// Program.UI.Instruments
struct Instruments_tF75DA728107310E221C75AD3538952EAEE98442E;
// ItemFile
struct ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8;
// Program.Menu.ItemNews
struct ItemNews_t56EF3225D84E7162C9E47B98493683E32B6C08F1;
// Program.Menu.ListNews
struct ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01;
// Program.UI.LoadLibraryPanel
struct LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394;
// Program.UI.MenuPaltira
struct MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MeshFilter
struct MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A;
// UnityEngine.MeshRenderer
struct MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// FilerServer.News
struct News_tDEA932114B87C23732FC46451449BC249F3EAE2C;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// Program.Option.OptionObject
struct OptionObject_t108F4312E931F36F66EF6A91E95988338600EC8F;
// Palitra
struct Palitra_t41281A13EF2874D486547B9324202C7D112880B3;
// PalitraDataBase
struct PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD;
// Program.UI.PalitraItem
struct PalitraItem_tB7364AC466C6FA3A859EB47095C3B8A563A0AD4A;
// Program.UI.ProgramUI
struct ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7;
// ProjectPalitra
struct ProjectPalitra_tE92E7BFB63915D8633D6B7A34DD7D32D2EEFC847;
// Program.UI.Render
struct Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.Net.Sockets.SafeSocketHandle
struct SafeSocketHandle_t5050671179FB886DA1763A0E4EFB3FCD072363C9;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385;
// Program.UI.SetPalitra
struct SetPalitra_tF9CBA6472FC50BA66C7FE5BB350A36CE8E184A62;
// System.Net.Sockets.Socket
struct Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09;
// System.Threading.Tasks.StackGuard
struct StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D;
// System.String
struct String_t;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069;
// System.Threading.Tasks.Task
struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60;
// System.Threading.Tasks.TaskFactory
struct TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D;
// FilerServer.Texture
struct Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// TextureFile
struct TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// Program.Menu.UIMenu
struct UIMenu_t6A3241472F39D561390A7106B024030062D29EA6;
// Program.UI.UV
struct UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C;
// Program.UI.UVPanel
struct UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40;
// FilerServer.Base.BaseConnect/BaseProcces
struct BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA;
// Program.UI.BodyPanel/<>c
struct U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C;
// Program.Audio.ButtonClick/<>c
struct U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F;
// Program.Audio.ButtonClickColor/<>c
struct U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4;
// Program.UI.ColorItem/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t2104F99A587EAB2260F5C1947DDBF7A6D9C6C62A;
// Program.UI.DefaultCamera/<>c
struct U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522;
// Program.Editor.GetPixels/Triangle
struct Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18;
// Program.Menu.ListNews/<LoadNewsWait>d__8
struct U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9;
// Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11
struct U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E;
// Program.UI.LoadMenu/<>c
struct U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9;
// Program.UI.MenuPaltira/<>c
struct U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE;
// Program.Editor.Model/<>c
struct U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F;
// Program.Editor.Model/History
struct History_tB9376052A6C8C07D431414CC16E050FE0EE9A156;
// Program.Editor.Model/Story
struct Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22;
// Program.Option.OptionController/<>c
struct U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E;
// Program.UI.PalitraItem/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_tD3927C92A4B65FB4EC5119DE2D09429DBBB6B3E9;
// Program.UI.PalitraPanel/<>c
struct U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0;
// System.Threading.Tasks.Task/ContingentProperties
struct ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0;
// FilerServer.Texture/Pixel
struct Pixel_t626CE4860392029BBF83CD2ADA27B179C3B93203;
// Program.Menu.UIMenu/<>c
struct U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D;

IL2CPP_EXTERN_C RuntimeClass* Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral107694947DB47644F8036602F63473486E1ED925;
IL2CPP_EXTERN_C String_t* _stringLiteral1B2D546B68B913141D48C16BD1B6C81F159853D3;
IL2CPP_EXTERN_C String_t* _stringLiteral2753D2982C7E0B6A494B13A85BFA999F0F3C0D17;
IL2CPP_EXTERN_C String_t* _stringLiteral4A261EB7E7319776625F5A015EA18053797E6890;
IL2CPP_EXTERN_C String_t* _stringLiteral5A1278AB54EFEBBD1E0E03AB2677F22D39311C27;
IL2CPP_EXTERN_C String_t* _stringLiteral79275F5DC38DFCA0BC6C5F6A6F0BDFA6B072469D;
IL2CPP_EXTERN_C String_t* _stringLiteralF766F1D50CD96D8870845654104505296E397678;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_m77090A1636BA1001FD6018EED4EEC9FEAE973E0C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncVoidMethodBuilder_Start_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_mB911DC1C3B7DF7B772D17AC5FBAB824F5FF66FF6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* BaseProcces_U3CUpdateU3Eb__8_0_mAD6652D6C6523D25840710F6A2E3C0C01767203F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_m4E31C5D3D0490AEE405B54BE9F61802EA425B9DC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_AddRange_m054C1E7F4F8B20005C0A988D0EB20B6711E58227_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m2FD0A3E3967129689A18B133C0AB1C60674D1BEC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mC5F56B296FBB59F233C39F696B4811C2B3F76A18_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m95A9EDACA9EDF273FB8BBAEE846E7738A872EA00_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m2383E89A18C3A19BFC8BCC1763ADC4321508C883_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveRange_m0266B9170CB4F7A00AFF336AA360766D0F6FAF66_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ToArray_mDE8A2E8CE776F68F8B0D394D1602EEB7B2D06EA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m94FA9771A56F4CCC6087669FB8221EBB6316096F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mB8DE1969DD30A9DD0A0EB797ABE6D6E3AB1AE461_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mC36BC2EDA73A732A891DE47D6E71213DF5544622_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m07689479A4628A308A145509E0858A6A782CD8BF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m34D1481BA2C3E15122493C0DE376BFC24A3732EA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_Reset_m53159E79EBA8B80BAC673EDE60518A4F30D17A98_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_Reset_m1F745D6ACC0C15F9630C12F79F928F663ADC18B7_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct NewsU5BU5D_t7CD4BFBC754A2A366E68BF321543B7DEF517F927;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct TextureU5BU5D_tFC397DCC79C0EAD5839F838C9E694ACB8C7C331B;
struct TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B;
struct TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Program.Editor.GetPixels/Triangle>
struct  List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923, ____items_1)); }
	inline TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* get__items_1() const { return ____items_1; }
	inline TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923_StaticFields, ____emptyArray_5)); }
	inline TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Program.Editor.Model/Story>
struct  List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StoryU5BU5D_t814EAE97194FB4969BF0807B637425F16593A40C* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD, ____items_1)); }
	inline StoryU5BU5D_t814EAE97194FB4969BF0807B637425F16593A40C* get__items_1() const { return ____items_1; }
	inline StoryU5BU5D_t814EAE97194FB4969BF0807B637425F16593A40C** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StoryU5BU5D_t814EAE97194FB4969BF0807B637425F16593A40C* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StoryU5BU5D_t814EAE97194FB4969BF0807B637425F16593A40C* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD_StaticFields, ____emptyArray_5)); }
	inline StoryU5BU5D_t814EAE97194FB4969BF0807B637425F16593A40C* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StoryU5BU5D_t814EAE97194FB4969BF0807B637425F16593A40C** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StoryU5BU5D_t814EAE97194FB4969BF0807B637425F16593A40C* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// FilerServer.Base.BaseConnect
struct  BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03  : public RuntimeObject
{
public:

public:
};

struct BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields
{
public:
	// System.String FilerServer.Base.BaseConnect::IP
	String_t* ___IP_0;
	// System.Net.Sockets.Socket FilerServer.Base.BaseConnect::Socket
	Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * ___Socket_9;
	// System.Net.EndPoint FilerServer.Base.BaseConnect::point
	EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * ___point_10;
	// FilerServer.Base.BaseConnect/BaseProcces FilerServer.Base.BaseConnect::procces
	BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * ___procces_11;

public:
	inline static int32_t get_offset_of_IP_0() { return static_cast<int32_t>(offsetof(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields, ___IP_0)); }
	inline String_t* get_IP_0() const { return ___IP_0; }
	inline String_t** get_address_of_IP_0() { return &___IP_0; }
	inline void set_IP_0(String_t* value)
	{
		___IP_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___IP_0), (void*)value);
	}

	inline static int32_t get_offset_of_Socket_9() { return static_cast<int32_t>(offsetof(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields, ___Socket_9)); }
	inline Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * get_Socket_9() const { return ___Socket_9; }
	inline Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 ** get_address_of_Socket_9() { return &___Socket_9; }
	inline void set_Socket_9(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * value)
	{
		___Socket_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Socket_9), (void*)value);
	}

	inline static int32_t get_offset_of_point_10() { return static_cast<int32_t>(offsetof(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields, ___point_10)); }
	inline EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * get_point_10() const { return ___point_10; }
	inline EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA ** get_address_of_point_10() { return &___point_10; }
	inline void set_point_10(EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * value)
	{
		___point_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___point_10), (void*)value);
	}

	inline static int32_t get_offset_of_procces_11() { return static_cast<int32_t>(offsetof(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields, ___procces_11)); }
	inline BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * get_procces_11() const { return ___procces_11; }
	inline BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA ** get_address_of_procces_11() { return &___procces_11; }
	inline void set_procces_11(BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * value)
	{
		___procces_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___procces_11), (void*)value);
	}
};


// Program.UI.ColorPalitra
struct  ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3  : public RuntimeObject
{
public:
	// System.Action Program.UI.ColorPalitra::TakeItem
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___TakeItem_0;
	// Program.UI.ColorItem[] Program.UI.ColorPalitra::items
	ColorItemU5BU5D_t21E2528356087A131BD727F2826714C4CDF04E3C* ___items_1;
	// Program.UI.ColorItem Program.UI.ColorPalitra::thisItem
	ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650 * ___thisItem_2;

public:
	inline static int32_t get_offset_of_TakeItem_0() { return static_cast<int32_t>(offsetof(ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3, ___TakeItem_0)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_TakeItem_0() const { return ___TakeItem_0; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_TakeItem_0() { return &___TakeItem_0; }
	inline void set_TakeItem_0(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___TakeItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TakeItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3, ___items_1)); }
	inline ColorItemU5BU5D_t21E2528356087A131BD727F2826714C4CDF04E3C* get_items_1() const { return ___items_1; }
	inline ColorItemU5BU5D_t21E2528356087A131BD727F2826714C4CDF04E3C** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(ColorItemU5BU5D_t21E2528356087A131BD727F2826714C4CDF04E3C* value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___items_1), (void*)value);
	}

	inline static int32_t get_offset_of_thisItem_2() { return static_cast<int32_t>(offsetof(ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3, ___thisItem_2)); }
	inline ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650 * get_thisItem_2() const { return ___thisItem_2; }
	inline ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650 ** get_address_of_thisItem_2() { return &___thisItem_2; }
	inline void set_thisItem_2(ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650 * value)
	{
		___thisItem_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thisItem_2), (void*)value);
	}
};


// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7  : public RuntimeObject
{
public:

public:
};


// Program.UI.DownLayer
struct  DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C  : public RuntimeObject
{
public:
	// System.Action Program.UI.DownLayer::EditAction
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___EditAction_0;
	// System.Boolean Program.UI.DownLayer::_enable
	bool ____enable_1;

public:
	inline static int32_t get_offset_of_EditAction_0() { return static_cast<int32_t>(offsetof(DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C, ___EditAction_0)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_EditAction_0() const { return ___EditAction_0; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_EditAction_0() { return &___EditAction_0; }
	inline void set_EditAction_0(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___EditAction_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EditAction_0), (void*)value);
	}

	inline static int32_t get_offset_of__enable_1() { return static_cast<int32_t>(offsetof(DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C, ____enable_1)); }
	inline bool get__enable_1() const { return ____enable_1; }
	inline bool* get_address_of__enable_1() { return &____enable_1; }
	inline void set__enable_1(bool value)
	{
		____enable_1 = value;
	}
};


// System.Net.EndPoint
struct  EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA  : public RuntimeObject
{
public:

public:
};


// Program.Menu.FileNews
struct  FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A  : public RuntimeObject
{
public:
	// System.UInt32 Program.Menu.FileNews::NumUpdate
	uint32_t ___NumUpdate_1;
	// FilerServer.News[] Program.Menu.FileNews::ArrayNews
	NewsU5BU5D_t7CD4BFBC754A2A366E68BF321543B7DEF517F927* ___ArrayNews_2;

public:
	inline static int32_t get_offset_of_NumUpdate_1() { return static_cast<int32_t>(offsetof(FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A, ___NumUpdate_1)); }
	inline uint32_t get_NumUpdate_1() const { return ___NumUpdate_1; }
	inline uint32_t* get_address_of_NumUpdate_1() { return &___NumUpdate_1; }
	inline void set_NumUpdate_1(uint32_t value)
	{
		___NumUpdate_1 = value;
	}

	inline static int32_t get_offset_of_ArrayNews_2() { return static_cast<int32_t>(offsetof(FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A, ___ArrayNews_2)); }
	inline NewsU5BU5D_t7CD4BFBC754A2A366E68BF321543B7DEF517F927* get_ArrayNews_2() const { return ___ArrayNews_2; }
	inline NewsU5BU5D_t7CD4BFBC754A2A366E68BF321543B7DEF517F927** get_address_of_ArrayNews_2() { return &___ArrayNews_2; }
	inline void set_ArrayNews_2(NewsU5BU5D_t7CD4BFBC754A2A366E68BF321543B7DEF517F927* value)
	{
		___ArrayNews_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ArrayNews_2), (void*)value);
	}
};

struct FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A_StaticFields
{
public:
	// System.String Program.Menu.FileNews::path
	String_t* ___path_3;

public:
	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A_StaticFields, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_3), (void*)value);
	}
};


// Program.Editor.FileTextures
struct  FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84  : public RuntimeObject
{
public:
	// System.UInt32 Program.Editor.FileTextures::NumUpdate
	uint32_t ___NumUpdate_1;
	// FilerServer.Texture[] Program.Editor.FileTextures::ArrayTexture
	TextureU5BU5D_tFC397DCC79C0EAD5839F838C9E694ACB8C7C331B* ___ArrayTexture_2;

public:
	inline static int32_t get_offset_of_NumUpdate_1() { return static_cast<int32_t>(offsetof(FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84, ___NumUpdate_1)); }
	inline uint32_t get_NumUpdate_1() const { return ___NumUpdate_1; }
	inline uint32_t* get_address_of_NumUpdate_1() { return &___NumUpdate_1; }
	inline void set_NumUpdate_1(uint32_t value)
	{
		___NumUpdate_1 = value;
	}

	inline static int32_t get_offset_of_ArrayTexture_2() { return static_cast<int32_t>(offsetof(FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84, ___ArrayTexture_2)); }
	inline TextureU5BU5D_tFC397DCC79C0EAD5839F838C9E694ACB8C7C331B* get_ArrayTexture_2() const { return ___ArrayTexture_2; }
	inline TextureU5BU5D_tFC397DCC79C0EAD5839F838C9E694ACB8C7C331B** get_address_of_ArrayTexture_2() { return &___ArrayTexture_2; }
	inline void set_ArrayTexture_2(TextureU5BU5D_tFC397DCC79C0EAD5839F838C9E694ACB8C7C331B* value)
	{
		___ArrayTexture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ArrayTexture_2), (void*)value);
	}
};

struct FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84_StaticFields
{
public:
	// System.String Program.Editor.FileTextures::path
	String_t* ___path_3;

public:
	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84_StaticFields, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_3), (void*)value);
	}
};


// Program.UI.HistoryButton
struct  HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB  : public RuntimeObject
{
public:
	// System.Action Program.UI.HistoryButton::HistoryNext
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___HistoryNext_0;
	// System.Action Program.UI.HistoryButton::HistoryBack
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___HistoryBack_1;

public:
	inline static int32_t get_offset_of_HistoryNext_0() { return static_cast<int32_t>(offsetof(HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB, ___HistoryNext_0)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_HistoryNext_0() const { return ___HistoryNext_0; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_HistoryNext_0() { return &___HistoryNext_0; }
	inline void set_HistoryNext_0(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___HistoryNext_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HistoryNext_0), (void*)value);
	}

	inline static int32_t get_offset_of_HistoryBack_1() { return static_cast<int32_t>(offsetof(HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB, ___HistoryBack_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_HistoryBack_1() const { return ___HistoryBack_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_HistoryBack_1() { return &___HistoryBack_1; }
	inline void set_HistoryBack_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___HistoryBack_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HistoryBack_1), (void*)value);
	}
};


// FilerServer.LoadObject
struct  LoadObject_t20A5930F931AF20BC3929EE825FF16FD2E24CC0E  : public RuntimeObject
{
public:
	// System.Int32 FilerServer.LoadObject::NumIndex
	int32_t ___NumIndex_0;

public:
	inline static int32_t get_offset_of_NumIndex_0() { return static_cast<int32_t>(offsetof(LoadObject_t20A5930F931AF20BC3929EE825FF16FD2E24CC0E, ___NumIndex_0)); }
	inline int32_t get_NumIndex_0() const { return ___NumIndex_0; }
	inline int32_t* get_address_of_NumIndex_0() { return &___NumIndex_0; }
	inline void set_NumIndex_0(int32_t value)
	{
		___NumIndex_0 = value;
	}
};


// Program.Menu.NewsServer
struct  NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6  : public RuntimeObject
{
public:

public:
};

struct NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields
{
public:
	// Program.Menu.FileNews Program.Menu.NewsServer::NewsLoad
	FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A * ___NewsLoad_0;
	// System.Boolean Program.Menu.NewsServer::IsLoadEnd
	bool ___IsLoadEnd_1;
	// System.Boolean Program.Menu.NewsServer::IsLoad
	bool ___IsLoad_2;
	// System.Boolean Program.Menu.NewsServer::isSave
	bool ___isSave_3;

public:
	inline static int32_t get_offset_of_NewsLoad_0() { return static_cast<int32_t>(offsetof(NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields, ___NewsLoad_0)); }
	inline FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A * get_NewsLoad_0() const { return ___NewsLoad_0; }
	inline FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A ** get_address_of_NewsLoad_0() { return &___NewsLoad_0; }
	inline void set_NewsLoad_0(FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A * value)
	{
		___NewsLoad_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NewsLoad_0), (void*)value);
	}

	inline static int32_t get_offset_of_IsLoadEnd_1() { return static_cast<int32_t>(offsetof(NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields, ___IsLoadEnd_1)); }
	inline bool get_IsLoadEnd_1() const { return ___IsLoadEnd_1; }
	inline bool* get_address_of_IsLoadEnd_1() { return &___IsLoadEnd_1; }
	inline void set_IsLoadEnd_1(bool value)
	{
		___IsLoadEnd_1 = value;
	}

	inline static int32_t get_offset_of_IsLoad_2() { return static_cast<int32_t>(offsetof(NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields, ___IsLoad_2)); }
	inline bool get_IsLoad_2() const { return ___IsLoad_2; }
	inline bool* get_address_of_IsLoad_2() { return &___IsLoad_2; }
	inline void set_IsLoad_2(bool value)
	{
		___IsLoad_2 = value;
	}

	inline static int32_t get_offset_of_isSave_3() { return static_cast<int32_t>(offsetof(NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields, ___isSave_3)); }
	inline bool get_isSave_3() const { return ___isSave_3; }
	inline bool* get_address_of_isSave_3() { return &___isSave_3; }
	inline void set_isSave_3(bool value)
	{
		___isSave_3 = value;
	}
};


// Palitra
struct  Palitra_t41281A13EF2874D486547B9324202C7D112880B3  : public RuntimeObject
{
public:
	// System.Single[] Palitra::R
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___R_0;
	// System.Single[] Palitra::G
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___G_1;
	// System.Single[] Palitra::B
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___B_2;
	// System.Single[] Palitra::A
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___A_3;

public:
	inline static int32_t get_offset_of_R_0() { return static_cast<int32_t>(offsetof(Palitra_t41281A13EF2874D486547B9324202C7D112880B3, ___R_0)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_R_0() const { return ___R_0; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_R_0() { return &___R_0; }
	inline void set_R_0(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___R_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___R_0), (void*)value);
	}

	inline static int32_t get_offset_of_G_1() { return static_cast<int32_t>(offsetof(Palitra_t41281A13EF2874D486547B9324202C7D112880B3, ___G_1)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_G_1() const { return ___G_1; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_G_1() { return &___G_1; }
	inline void set_G_1(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___G_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___G_1), (void*)value);
	}

	inline static int32_t get_offset_of_B_2() { return static_cast<int32_t>(offsetof(Palitra_t41281A13EF2874D486547B9324202C7D112880B3, ___B_2)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_B_2() const { return ___B_2; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_B_2() { return &___B_2; }
	inline void set_B_2(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___B_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___B_2), (void*)value);
	}

	inline static int32_t get_offset_of_A_3() { return static_cast<int32_t>(offsetof(Palitra_t41281A13EF2874D486547B9324202C7D112880B3, ___A_3)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_A_3() const { return ___A_3; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_A_3() { return &___A_3; }
	inline void set_A_3(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___A_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___A_3), (void*)value);
	}
};


// ProjectPalitra
struct  ProjectPalitra_tE92E7BFB63915D8633D6B7A34DD7D32D2EEFC847  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Color> ProjectPalitra::Colors
	List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * ___Colors_0;

public:
	inline static int32_t get_offset_of_Colors_0() { return static_cast<int32_t>(offsetof(ProjectPalitra_tE92E7BFB63915D8633D6B7A34DD7D32D2EEFC847, ___Colors_0)); }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * get_Colors_0() const { return ___Colors_0; }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E ** get_address_of_Colors_0() { return &___Colors_0; }
	inline void set_Colors_0(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * value)
	{
		___Colors_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Colors_0), (void*)value);
	}
};


// Program.UI.Render
struct  Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08  : public RuntimeObject
{
public:
	// System.Action Program.UI.Render::StartRenderEvent
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___StartRenderEvent_0;

public:
	inline static int32_t get_offset_of_StartRenderEvent_0() { return static_cast<int32_t>(offsetof(Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08, ___StartRenderEvent_0)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_StartRenderEvent_0() const { return ___StartRenderEvent_0; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_StartRenderEvent_0() { return &___StartRenderEvent_0; }
	inline void set_StartRenderEvent_0(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___StartRenderEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StartRenderEvent_0), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// TextureFile
struct  TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F  : public RuntimeObject
{
public:
	// System.String TextureFile::Name
	String_t* ___Name_0;
	// UnityEngine.Texture2D TextureFile::Texture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___Texture_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_Texture_1() { return static_cast<int32_t>(offsetof(TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F, ___Texture_1)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_Texture_1() const { return ___Texture_1; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_Texture_1() { return &___Texture_1; }
	inline void set_Texture_1(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___Texture_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Texture_1), (void*)value);
	}
};


// Program.Editor.TexturesServer
struct  TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298  : public RuntimeObject
{
public:

public:
};

struct TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields
{
public:
	// Program.Editor.FileTextures Program.Editor.TexturesServer::TextureLoad
	FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84 * ___TextureLoad_0;
	// TextureFile[] Program.Editor.TexturesServer::GenerateTexture
	TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* ___GenerateTexture_1;
	// System.Boolean Program.Editor.TexturesServer::IsLoadEnd
	bool ___IsLoadEnd_2;
	// System.Boolean Program.Editor.TexturesServer::IsLoad
	bool ___IsLoad_3;
	// System.Boolean Program.Editor.TexturesServer::isSave
	bool ___isSave_4;

public:
	inline static int32_t get_offset_of_TextureLoad_0() { return static_cast<int32_t>(offsetof(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields, ___TextureLoad_0)); }
	inline FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84 * get_TextureLoad_0() const { return ___TextureLoad_0; }
	inline FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84 ** get_address_of_TextureLoad_0() { return &___TextureLoad_0; }
	inline void set_TextureLoad_0(FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84 * value)
	{
		___TextureLoad_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextureLoad_0), (void*)value);
	}

	inline static int32_t get_offset_of_GenerateTexture_1() { return static_cast<int32_t>(offsetof(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields, ___GenerateTexture_1)); }
	inline TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* get_GenerateTexture_1() const { return ___GenerateTexture_1; }
	inline TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B** get_address_of_GenerateTexture_1() { return &___GenerateTexture_1; }
	inline void set_GenerateTexture_1(TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* value)
	{
		___GenerateTexture_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GenerateTexture_1), (void*)value);
	}

	inline static int32_t get_offset_of_IsLoadEnd_2() { return static_cast<int32_t>(offsetof(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields, ___IsLoadEnd_2)); }
	inline bool get_IsLoadEnd_2() const { return ___IsLoadEnd_2; }
	inline bool* get_address_of_IsLoadEnd_2() { return &___IsLoadEnd_2; }
	inline void set_IsLoadEnd_2(bool value)
	{
		___IsLoadEnd_2 = value;
	}

	inline static int32_t get_offset_of_IsLoad_3() { return static_cast<int32_t>(offsetof(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields, ___IsLoad_3)); }
	inline bool get_IsLoad_3() const { return ___IsLoad_3; }
	inline bool* get_address_of_IsLoad_3() { return &___IsLoad_3; }
	inline void set_IsLoad_3(bool value)
	{
		___IsLoad_3 = value;
	}

	inline static int32_t get_offset_of_isSave_4() { return static_cast<int32_t>(offsetof(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields, ___isSave_4)); }
	inline bool get_isSave_4() const { return ___isSave_4; }
	inline bool* get_address_of_isSave_4() { return &___isSave_4; }
	inline void set_isSave_4(bool value)
	{
		___isSave_4 = value;
	}
};


// Program.UI.UVPanel
struct  UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9  : public RuntimeObject
{
public:
	// System.Action Program.UI.UVPanel::EditUV
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___EditUV_0;
	// System.Boolean Program.UI.UVPanel::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_1;
	// Program.UI.UV Program.UI.UVPanel::currentUV
	UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C * ___currentUV_2;

public:
	inline static int32_t get_offset_of_EditUV_0() { return static_cast<int32_t>(offsetof(UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9, ___EditUV_0)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_EditUV_0() const { return ___EditUV_0; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_EditUV_0() { return &___EditUV_0; }
	inline void set_EditUV_0(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___EditUV_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EditUV_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9, ___U3CIsActiveU3Ek__BackingField_1)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_1() const { return ___U3CIsActiveU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_1() { return &___U3CIsActiveU3Ek__BackingField_1; }
	inline void set_U3CIsActiveU3Ek__BackingField_1(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_currentUV_2() { return static_cast<int32_t>(offsetof(UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9, ___currentUV_2)); }
	inline UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C * get_currentUV_2() const { return ___currentUV_2; }
	inline UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C ** get_address_of_currentUV_2() { return &___currentUV_2; }
	inline void set_currentUV_2(UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C * value)
	{
		___currentUV_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentUV_2), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// FilerServer.Base.BaseConnect/BaseProcces
struct  BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA  : public RuntimeObject
{
public:
	// System.Boolean FilerServer.Base.BaseConnect/BaseProcces::_is_end
	bool ____is_end_0;
	// System.Action FilerServer.Base.BaseConnect/BaseProcces::start
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___start_1;
	// System.Action FilerServer.Base.BaseConnect/BaseProcces::end
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___end_2;

public:
	inline static int32_t get_offset_of__is_end_0() { return static_cast<int32_t>(offsetof(BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA, ____is_end_0)); }
	inline bool get__is_end_0() const { return ____is_end_0; }
	inline bool* get_address_of__is_end_0() { return &____is_end_0; }
	inline void set__is_end_0(bool value)
	{
		____is_end_0 = value;
	}

	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA, ___start_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_start_1() const { return ___start_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___start_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___start_1), (void*)value);
	}

	inline static int32_t get_offset_of_end_2() { return static_cast<int32_t>(offsetof(BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA, ___end_2)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_end_2() const { return ___end_2; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_end_2() { return &___end_2; }
	inline void set_end_2(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___end_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___end_2), (void*)value);
	}
};


// Program.UI.BodyPanel/<>c
struct  U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_StaticFields
{
public:
	// Program.UI.BodyPanel/<>c Program.UI.BodyPanel/<>c::<>9
	U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction Program.UI.BodyPanel/<>c::<>9__35_0
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__35_0_1;
	// UnityEngine.Events.UnityAction Program.UI.BodyPanel/<>c::<>9__35_1
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__35_1_2;
	// UnityEngine.Events.UnityAction Program.UI.BodyPanel/<>c::<>9__36_0
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__36_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_StaticFields, ___U3CU3E9__35_0_1)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__35_0_1() const { return ___U3CU3E9__35_0_1; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__35_0_1() { return &___U3CU3E9__35_0_1; }
	inline void set_U3CU3E9__35_0_1(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__35_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_StaticFields, ___U3CU3E9__35_1_2)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__35_1_2() const { return ___U3CU3E9__35_1_2; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__35_1_2() { return &___U3CU3E9__35_1_2; }
	inline void set_U3CU3E9__35_1_2(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__35_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__35_1_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_StaticFields, ___U3CU3E9__36_0_3)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__36_0_3() const { return ___U3CU3E9__36_0_3; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__36_0_3() { return &___U3CU3E9__36_0_3; }
	inline void set_U3CU3E9__36_0_3(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__36_0_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__36_0_3), (void*)value);
	}
};


// Program.Audio.ButtonClick/<>c
struct  U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F_StaticFields
{
public:
	// Program.Audio.ButtonClick/<>c Program.Audio.ButtonClick/<>c::<>9
	U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction Program.Audio.ButtonClick/<>c::<>9__1_0
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F_StaticFields, ___U3CU3E9__1_0_1)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__1_0_1), (void*)value);
	}
};


// Program.Audio.ButtonClickColor/<>c
struct  U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4_StaticFields
{
public:
	// Program.Audio.ButtonClickColor/<>c Program.Audio.ButtonClickColor/<>c::<>9
	U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4 * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction Program.Audio.ButtonClickColor/<>c::<>9__1_0
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4_StaticFields, ___U3CU3E9__1_0_1)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__1_0_1), (void*)value);
	}
};


// Program.UI.ColorItem/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t2104F99A587EAB2260F5C1947DDBF7A6D9C6C62A  : public RuntimeObject
{
public:
	// Program.UI.ColorPalitra Program.UI.ColorItem/<>c__DisplayClass2_0::colorTake
	ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3 * ___colorTake_0;
	// Program.UI.ColorItem Program.UI.ColorItem/<>c__DisplayClass2_0::<>4__this
	ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_colorTake_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t2104F99A587EAB2260F5C1947DDBF7A6D9C6C62A, ___colorTake_0)); }
	inline ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3 * get_colorTake_0() const { return ___colorTake_0; }
	inline ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3 ** get_address_of_colorTake_0() { return &___colorTake_0; }
	inline void set_colorTake_0(ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3 * value)
	{
		___colorTake_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorTake_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t2104F99A587EAB2260F5C1947DDBF7A6D9C6C62A, ___U3CU3E4__this_1)); }
	inline ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// Program.UI.DefaultCamera/<>c
struct  U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522_StaticFields
{
public:
	// Program.UI.DefaultCamera/<>c Program.UI.DefaultCamera/<>c::<>9
	U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522 * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction Program.UI.DefaultCamera/<>c::<>9__4_0
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522_StaticFields, ___U3CU3E9__4_0_1)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}
};


// Program.Editor.GetPixels/Triangle
struct  Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18  : public RuntimeObject
{
public:
	// System.Int32 Program.Editor.GetPixels/Triangle::point1
	int32_t ___point1_0;
	// System.Int32 Program.Editor.GetPixels/Triangle::point2
	int32_t ___point2_1;
	// System.Int32 Program.Editor.GetPixels/Triangle::point3
	int32_t ___point3_2;
	// System.Boolean Program.Editor.GetPixels/Triangle::isCheck
	bool ___isCheck_3;
	// System.Collections.Generic.List`1<Program.Editor.GetPixels/Triangle> Program.Editor.GetPixels/Triangle::connect
	List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * ___connect_4;

public:
	inline static int32_t get_offset_of_point1_0() { return static_cast<int32_t>(offsetof(Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18, ___point1_0)); }
	inline int32_t get_point1_0() const { return ___point1_0; }
	inline int32_t* get_address_of_point1_0() { return &___point1_0; }
	inline void set_point1_0(int32_t value)
	{
		___point1_0 = value;
	}

	inline static int32_t get_offset_of_point2_1() { return static_cast<int32_t>(offsetof(Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18, ___point2_1)); }
	inline int32_t get_point2_1() const { return ___point2_1; }
	inline int32_t* get_address_of_point2_1() { return &___point2_1; }
	inline void set_point2_1(int32_t value)
	{
		___point2_1 = value;
	}

	inline static int32_t get_offset_of_point3_2() { return static_cast<int32_t>(offsetof(Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18, ___point3_2)); }
	inline int32_t get_point3_2() const { return ___point3_2; }
	inline int32_t* get_address_of_point3_2() { return &___point3_2; }
	inline void set_point3_2(int32_t value)
	{
		___point3_2 = value;
	}

	inline static int32_t get_offset_of_isCheck_3() { return static_cast<int32_t>(offsetof(Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18, ___isCheck_3)); }
	inline bool get_isCheck_3() const { return ___isCheck_3; }
	inline bool* get_address_of_isCheck_3() { return &___isCheck_3; }
	inline void set_isCheck_3(bool value)
	{
		___isCheck_3 = value;
	}

	inline static int32_t get_offset_of_connect_4() { return static_cast<int32_t>(offsetof(Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18, ___connect_4)); }
	inline List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * get_connect_4() const { return ___connect_4; }
	inline List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 ** get_address_of_connect_4() { return &___connect_4; }
	inline void set_connect_4(List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * value)
	{
		___connect_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___connect_4), (void*)value);
	}
};


// Program.Menu.ListNews/<LoadNewsWait>d__8
struct  U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9  : public RuntimeObject
{
public:
	// System.Int32 Program.Menu.ListNews/<LoadNewsWait>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Program.Menu.ListNews/<LoadNewsWait>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Program.Menu.ListNews Program.Menu.ListNews/<LoadNewsWait>d__8::<>4__this
	ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9, ___U3CU3E4__this_2)); }
	inline ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11
struct  U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E  : public RuntimeObject
{
public:
	// System.Int32 Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Program.UI.LoadLibraryPanel Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::<>4__this
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E, ___U3CU3E4__this_2)); }
	inline LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// Program.UI.LoadMenu/<>c
struct  U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9_StaticFields
{
public:
	// Program.UI.LoadMenu/<>c Program.UI.LoadMenu/<>c::<>9
	U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9 * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction Program.UI.LoadMenu/<>c::<>9__1_0
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9_StaticFields, ___U3CU3E9__1_0_1)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__1_0_1), (void*)value);
	}
};


// Program.UI.MenuPaltira/<>c
struct  U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_StaticFields
{
public:
	// Program.UI.MenuPaltira/<>c Program.UI.MenuPaltira/<>c::<>9
	U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE * ___U3CU3E9_0;
	// System.Func`2<ProjectPalitra,Palitra> Program.UI.MenuPaltira/<>c::<>9__19_0
	Func_2_t7A18FE33D332E9149ED1B210BB4F36CF337A155A * ___U3CU3E9__19_0_1;
	// System.Func`2<ProjectPalitra,Palitra> Program.UI.MenuPaltira/<>c::<>9__24_0
	Func_2_t7A18FE33D332E9149ED1B210BB4F36CF337A155A * ___U3CU3E9__24_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_StaticFields, ___U3CU3E9__19_0_1)); }
	inline Func_2_t7A18FE33D332E9149ED1B210BB4F36CF337A155A * get_U3CU3E9__19_0_1() const { return ___U3CU3E9__19_0_1; }
	inline Func_2_t7A18FE33D332E9149ED1B210BB4F36CF337A155A ** get_address_of_U3CU3E9__19_0_1() { return &___U3CU3E9__19_0_1; }
	inline void set_U3CU3E9__19_0_1(Func_2_t7A18FE33D332E9149ED1B210BB4F36CF337A155A * value)
	{
		___U3CU3E9__19_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__19_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_StaticFields, ___U3CU3E9__24_0_2)); }
	inline Func_2_t7A18FE33D332E9149ED1B210BB4F36CF337A155A * get_U3CU3E9__24_0_2() const { return ___U3CU3E9__24_0_2; }
	inline Func_2_t7A18FE33D332E9149ED1B210BB4F36CF337A155A ** get_address_of_U3CU3E9__24_0_2() { return &___U3CU3E9__24_0_2; }
	inline void set_U3CU3E9__24_0_2(Func_2_t7A18FE33D332E9149ED1B210BB4F36CF337A155A * value)
	{
		___U3CU3E9__24_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__24_0_2), (void*)value);
	}
};


// Program.Editor.Model/<>c
struct  U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F_StaticFields
{
public:
	// Program.Editor.Model/<>c Program.Editor.Model/<>c::<>9
	U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.MeshRenderer,UnityEngine.Mesh> Program.Editor.Model/<>c::<>9__14_0
	Func_2_t798C577C946B09A44DFDCDFECC6D23AADE937404 * ___U3CU3E9__14_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F_StaticFields, ___U3CU3E9__14_0_1)); }
	inline Func_2_t798C577C946B09A44DFDCDFECC6D23AADE937404 * get_U3CU3E9__14_0_1() const { return ___U3CU3E9__14_0_1; }
	inline Func_2_t798C577C946B09A44DFDCDFECC6D23AADE937404 ** get_address_of_U3CU3E9__14_0_1() { return &___U3CU3E9__14_0_1; }
	inline void set_U3CU3E9__14_0_1(Func_2_t798C577C946B09A44DFDCDFECC6D23AADE937404 * value)
	{
		___U3CU3E9__14_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__14_0_1), (void*)value);
	}
};


// Program.Editor.Model/History
struct  History_tB9376052A6C8C07D431414CC16E050FE0EE9A156  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Program.Editor.Model/Story> Program.Editor.Model/History::stories
	List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * ___stories_0;
	// System.Int32 Program.Editor.Model/History::<CurrentStory>k__BackingField
	int32_t ___U3CCurrentStoryU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_stories_0() { return static_cast<int32_t>(offsetof(History_tB9376052A6C8C07D431414CC16E050FE0EE9A156, ___stories_0)); }
	inline List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * get_stories_0() const { return ___stories_0; }
	inline List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD ** get_address_of_stories_0() { return &___stories_0; }
	inline void set_stories_0(List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * value)
	{
		___stories_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stories_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentStoryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(History_tB9376052A6C8C07D431414CC16E050FE0EE9A156, ___U3CCurrentStoryU3Ek__BackingField_1)); }
	inline int32_t get_U3CCurrentStoryU3Ek__BackingField_1() const { return ___U3CCurrentStoryU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCurrentStoryU3Ek__BackingField_1() { return &___U3CCurrentStoryU3Ek__BackingField_1; }
	inline void set_U3CCurrentStoryU3Ek__BackingField_1(int32_t value)
	{
		___U3CCurrentStoryU3Ek__BackingField_1 = value;
	}
};


// Program.Editor.Model/Story
struct  Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D Program.Editor.Model/Story::Save
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___Save_0;

public:
	inline static int32_t get_offset_of_Save_0() { return static_cast<int32_t>(offsetof(Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22, ___Save_0)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_Save_0() const { return ___Save_0; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_Save_0() { return &___Save_0; }
	inline void set_Save_0(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___Save_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Save_0), (void*)value);
	}
};


// Program.Option.OptionController/<>c
struct  U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E_StaticFields
{
public:
	// Program.Option.OptionController/<>c Program.Option.OptionController/<>c::<>9
	U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E * ___U3CU3E9_0;
	// System.Func`2<Program.Option.OptionObject,System.Object> Program.Option.OptionController/<>c::<>9__3_0
	Func_2_t2B142F97BDB708FE8BDA2E4A9B7B5B2AFC2B0E6A * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_t2B142F97BDB708FE8BDA2E4A9B7B5B2AFC2B0E6A * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_t2B142F97BDB708FE8BDA2E4A9B7B5B2AFC2B0E6A ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_t2B142F97BDB708FE8BDA2E4A9B7B5B2AFC2B0E6A * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_0_1), (void*)value);
	}
};


// Program.UI.PalitraItem/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tD3927C92A4B65FB4EC5119DE2D09429DBBB6B3E9  : public RuntimeObject
{
public:
	// Program.UI.MenuPaltira Program.UI.PalitraItem/<>c__DisplayClass2_0::menuPaltira
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * ___menuPaltira_0;
	// Palitra Program.UI.PalitraItem/<>c__DisplayClass2_0::palitra
	Palitra_t41281A13EF2874D486547B9324202C7D112880B3 * ___palitra_1;

public:
	inline static int32_t get_offset_of_menuPaltira_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tD3927C92A4B65FB4EC5119DE2D09429DBBB6B3E9, ___menuPaltira_0)); }
	inline MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * get_menuPaltira_0() const { return ___menuPaltira_0; }
	inline MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A ** get_address_of_menuPaltira_0() { return &___menuPaltira_0; }
	inline void set_menuPaltira_0(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * value)
	{
		___menuPaltira_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuPaltira_0), (void*)value);
	}

	inline static int32_t get_offset_of_palitra_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tD3927C92A4B65FB4EC5119DE2D09429DBBB6B3E9, ___palitra_1)); }
	inline Palitra_t41281A13EF2874D486547B9324202C7D112880B3 * get_palitra_1() const { return ___palitra_1; }
	inline Palitra_t41281A13EF2874D486547B9324202C7D112880B3 ** get_address_of_palitra_1() { return &___palitra_1; }
	inline void set_palitra_1(Palitra_t41281A13EF2874D486547B9324202C7D112880B3 * value)
	{
		___palitra_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___palitra_1), (void*)value);
	}
};


// Program.UI.PalitraPanel/<>c
struct  U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_StaticFields
{
public:
	// Program.UI.PalitraPanel/<>c Program.UI.PalitraPanel/<>c::<>9
	U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction Program.UI.PalitraPanel/<>c::<>9__39_0
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__39_0_1;
	// UnityEngine.Events.UnityAction Program.UI.PalitraPanel/<>c::<>9__39_1
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__39_1_2;
	// UnityEngine.Events.UnityAction Program.UI.PalitraPanel/<>c::<>9__39_2
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__39_2_3;
	// UnityEngine.Events.UnityAction Program.UI.PalitraPanel/<>c::<>9__39_3
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__39_3_4;
	// UnityEngine.Events.UnityAction Program.UI.PalitraPanel/<>c::<>9__39_4
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__39_4_5;
	// UnityEngine.Events.UnityAction Program.UI.PalitraPanel/<>c::<>9__39_5
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__39_5_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_StaticFields, ___U3CU3E9__39_0_1)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__39_0_1() const { return ___U3CU3E9__39_0_1; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__39_0_1() { return &___U3CU3E9__39_0_1; }
	inline void set_U3CU3E9__39_0_1(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__39_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__39_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_StaticFields, ___U3CU3E9__39_1_2)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__39_1_2() const { return ___U3CU3E9__39_1_2; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__39_1_2() { return &___U3CU3E9__39_1_2; }
	inline void set_U3CU3E9__39_1_2(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__39_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__39_1_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_StaticFields, ___U3CU3E9__39_2_3)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__39_2_3() const { return ___U3CU3E9__39_2_3; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__39_2_3() { return &___U3CU3E9__39_2_3; }
	inline void set_U3CU3E9__39_2_3(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__39_2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__39_2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_StaticFields, ___U3CU3E9__39_3_4)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__39_3_4() const { return ___U3CU3E9__39_3_4; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__39_3_4() { return &___U3CU3E9__39_3_4; }
	inline void set_U3CU3E9__39_3_4(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__39_3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__39_3_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_4_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_StaticFields, ___U3CU3E9__39_4_5)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__39_4_5() const { return ___U3CU3E9__39_4_5; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__39_4_5() { return &___U3CU3E9__39_4_5; }
	inline void set_U3CU3E9__39_4_5(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__39_4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__39_4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_5_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_StaticFields, ___U3CU3E9__39_5_6)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__39_5_6() const { return ___U3CU3E9__39_5_6; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__39_5_6() { return &___U3CU3E9__39_5_6; }
	inline void set_U3CU3E9__39_5_6(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__39_5_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__39_5_6), (void*)value);
	}
};


// Program.UI.ProgramUI/CorrectCanvance
struct  CorrectCanvance_t6DA9FDCCF40C4DD86AA8E2B68172AB2765CE26C8  : public RuntimeObject
{
public:

public:
};


// FilerServer.Texture/Pixel
struct  Pixel_t626CE4860392029BBF83CD2ADA27B179C3B93203  : public RuntimeObject
{
public:
	// System.Byte FilerServer.Texture/Pixel::r
	uint8_t ___r_0;
	// System.Byte FilerServer.Texture/Pixel::g
	uint8_t ___g_1;
	// System.Byte FilerServer.Texture/Pixel::b
	uint8_t ___b_2;
	// System.Byte FilerServer.Texture/Pixel::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Pixel_t626CE4860392029BBF83CD2ADA27B179C3B93203, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Pixel_t626CE4860392029BBF83CD2ADA27B179C3B93203, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Pixel_t626CE4860392029BBF83CD2ADA27B179C3B93203, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Pixel_t626CE4860392029BBF83CD2ADA27B179C3B93203, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};


// Program.Menu.UIMenu/<>c
struct  U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_StaticFields
{
public:
	// Program.Menu.UIMenu/<>c Program.Menu.UIMenu/<>c::<>9
	U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction Program.Menu.UIMenu/<>c::<>9__3_0
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__3_0_1;
	// UnityEngine.Events.UnityAction Program.Menu.UIMenu/<>c::<>9__3_1
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__3_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_StaticFields, ___U3CU3E9__3_0_1)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_StaticFields, ___U3CU3E9__3_1_2)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__3_1_2() const { return ___U3CU3E9__3_1_2; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__3_1_2() { return &___U3CU3E9__3_1_2; }
	inline void set_U3CU3E9__3_1_2(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__3_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_1_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateMachine_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34, ___m_defaultContextAction_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_defaultContextAction_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Net.IPEndPoint
struct  IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E  : public EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA
{
public:
	// System.Net.IPAddress System.Net.IPEndPoint::m_Address
	IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * ___m_Address_0;
	// System.Int32 System.Net.IPEndPoint::m_Port
	int32_t ___m_Port_1;

public:
	inline static int32_t get_offset_of_m_Address_0() { return static_cast<int32_t>(offsetof(IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E, ___m_Address_0)); }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * get_m_Address_0() const { return ___m_Address_0; }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE ** get_address_of_m_Address_0() { return &___m_Address_0; }
	inline void set_m_Address_0(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * value)
	{
		___m_Address_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Address_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Port_1() { return static_cast<int32_t>(offsetof(IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E, ___m_Port_1)); }
	inline int32_t get_m_Port_1() const { return ___m_Port_1; }
	inline int32_t* get_address_of_m_Port_1() { return &___m_Port_1; }
	inline void set_m_Port_1(int32_t value)
	{
		___m_Port_1 = value;
	}
};

struct IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E_StaticFields
{
public:
	// System.Net.IPEndPoint System.Net.IPEndPoint::Any
	IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * ___Any_2;
	// System.Net.IPEndPoint System.Net.IPEndPoint::IPv6Any
	IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * ___IPv6Any_3;

public:
	inline static int32_t get_offset_of_Any_2() { return static_cast<int32_t>(offsetof(IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E_StaticFields, ___Any_2)); }
	inline IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * get_Any_2() const { return ___Any_2; }
	inline IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E ** get_address_of_Any_2() { return &___Any_2; }
	inline void set_Any_2(IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * value)
	{
		___Any_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Any_2), (void*)value);
	}

	inline static int32_t get_offset_of_IPv6Any_3() { return static_cast<int32_t>(offsetof(IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E_StaticFields, ___IPv6Any_3)); }
	inline IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * get_IPv6Any_3() const { return ___IPv6Any_3; }
	inline IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E ** get_address_of_IPv6Any_3() { return &___IPv6Any_3; }
	inline void set_IPv6Any_3(IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * value)
	{
		___IPv6Any_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___IPv6Any_3), (void*)value);
	}
};


// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// FilerServer.News
struct  News_tDEA932114B87C23732FC46451449BC249F3EAE2C  : public LoadObject_t20A5930F931AF20BC3929EE825FF16FD2E24CC0E
{
public:
	// System.String FilerServer.News::EnTitle
	String_t* ___EnTitle_1;
	// System.String FilerServer.News::EnContent
	String_t* ___EnContent_2;
	// System.String FilerServer.News::RuTitle
	String_t* ___RuTitle_3;
	// System.String FilerServer.News::RuContent
	String_t* ___RuContent_4;

public:
	inline static int32_t get_offset_of_EnTitle_1() { return static_cast<int32_t>(offsetof(News_tDEA932114B87C23732FC46451449BC249F3EAE2C, ___EnTitle_1)); }
	inline String_t* get_EnTitle_1() const { return ___EnTitle_1; }
	inline String_t** get_address_of_EnTitle_1() { return &___EnTitle_1; }
	inline void set_EnTitle_1(String_t* value)
	{
		___EnTitle_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnTitle_1), (void*)value);
	}

	inline static int32_t get_offset_of_EnContent_2() { return static_cast<int32_t>(offsetof(News_tDEA932114B87C23732FC46451449BC249F3EAE2C, ___EnContent_2)); }
	inline String_t* get_EnContent_2() const { return ___EnContent_2; }
	inline String_t** get_address_of_EnContent_2() { return &___EnContent_2; }
	inline void set_EnContent_2(String_t* value)
	{
		___EnContent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnContent_2), (void*)value);
	}

	inline static int32_t get_offset_of_RuTitle_3() { return static_cast<int32_t>(offsetof(News_tDEA932114B87C23732FC46451449BC249F3EAE2C, ___RuTitle_3)); }
	inline String_t* get_RuTitle_3() const { return ___RuTitle_3; }
	inline String_t** get_address_of_RuTitle_3() { return &___RuTitle_3; }
	inline void set_RuTitle_3(String_t* value)
	{
		___RuTitle_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RuTitle_3), (void*)value);
	}

	inline static int32_t get_offset_of_RuContent_4() { return static_cast<int32_t>(offsetof(News_tDEA932114B87C23732FC46451449BC249F3EAE2C, ___RuContent_4)); }
	inline String_t* get_RuContent_4() const { return ___RuContent_4; }
	inline String_t** get_address_of_RuContent_4() { return &___RuContent_4; }
	inline void set_RuContent_4(String_t* value)
	{
		___RuContent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RuContent_4), (void*)value);
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Runtime.CompilerServices.TaskAwaiter
struct  TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C, ___m_task_0)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_marshaled_pinvoke
{
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_marshaled_com
{
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_0;
};

// FilerServer.Texture
struct  Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D  : public LoadObject_t20A5930F931AF20BC3929EE825FF16FD2E24CC0E
{
public:
	// System.String FilerServer.Texture::Name
	String_t* ___Name_2;
	// FilerServer.Texture/Pixel[] FilerServer.Texture::Pixels
	PixelU5BU5D_t4C1B73D1643C9DB7566C12B119C443AF0417057E* ___Pixels_3;

public:
	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_2), (void*)value);
	}

	inline static int32_t get_offset_of_Pixels_3() { return static_cast<int32_t>(offsetof(Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D, ___Pixels_3)); }
	inline PixelU5BU5D_t4C1B73D1643C9DB7566C12B119C443AF0417057E* get_Pixels_3() const { return ___Pixels_3; }
	inline PixelU5BU5D_t4C1B73D1643C9DB7566C12B119C443AF0417057E** get_address_of_Pixels_3() { return &___Pixels_3; }
	inline void set_Pixels_3(PixelU5BU5D_t4C1B73D1643C9DB7566C12B119C443AF0417057E* value)
	{
		___Pixels_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Pixels_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForSecondsRealtime
struct  WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40  : public CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7
{
public:
	// System.Single UnityEngine.WaitForSecondsRealtime::<waitTime>k__BackingField
	float ___U3CwaitTimeU3Ek__BackingField_0;
	// System.Single UnityEngine.WaitForSecondsRealtime::m_WaitUntilTime
	float ___m_WaitUntilTime_1;

public:
	inline static int32_t get_offset_of_U3CwaitTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40, ___U3CwaitTimeU3Ek__BackingField_0)); }
	inline float get_U3CwaitTimeU3Ek__BackingField_0() const { return ___U3CwaitTimeU3Ek__BackingField_0; }
	inline float* get_address_of_U3CwaitTimeU3Ek__BackingField_0() { return &___U3CwaitTimeU3Ek__BackingField_0; }
	inline void set_U3CwaitTimeU3Ek__BackingField_0(float value)
	{
		___U3CwaitTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_WaitUntilTime_1() { return static_cast<int32_t>(offsetof(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40, ___m_WaitUntilTime_1)); }
	inline float get_m_WaitUntilTime_1() const { return ___m_WaitUntilTime_1; }
	inline float* get_address_of_m_WaitUntilTime_1() { return &___m_WaitUntilTime_1; }
	inline void set_m_WaitUntilTime_1(float value)
	{
		___m_WaitUntilTime_1 = value;
	}
};


// System.Net.Sockets.AddressFamily
struct  AddressFamily_tFCF4C888B95C069AB2D4720EC8C2E19453C28B33 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AddressFamily_tFCF4C888B95C069AB2D4720EC8C2E19453C28B33, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct  AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 
{
public:
	// System.Threading.SynchronizationContext System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_synchronizationContext
	SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * ___m_synchronizationContext_0;
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_coreState
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  ___m_coreState_1;
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_task
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_synchronizationContext_0() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6, ___m_synchronizationContext_0)); }
	inline SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * get_m_synchronizationContext_0() const { return ___m_synchronizationContext_0; }
	inline SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 ** get_address_of_m_synchronizationContext_0() { return &___m_synchronizationContext_0; }
	inline void set_m_synchronizationContext_0(SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * value)
	{
		___m_synchronizationContext_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_synchronizationContext_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  value)
	{
		___m_coreState_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6, ___m_task_2)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_m_task_2() const { return ___m_task_2; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6_marshaled_pinvoke
{
	SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_pinvoke ___m_coreState_1;
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_2;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6_marshaled_com
{
	SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_com ___m_coreState_1;
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_2;
};

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// Program.UI.Instrument
struct  Instrument_tE11EE4668091F733F5853F758FC7F9B103473F0D 
{
public:
	// System.Int32 Program.UI.Instrument::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Instrument_tE11EE4668091F733F5853F758FC7F9B103473F0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.Net.Sockets.ProtocolType
struct  ProtocolType_t07C7AB65B583B132A2D99BC06BB2A909BDDCE156 
{
public:
	// System.Int32 System.Net.Sockets.ProtocolType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProtocolType_t07C7AB65B583B132A2D99BC06BB2A909BDDCE156, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Net.Sockets.SocketType
struct  SocketType_t234FBD298C115F92305ABC40D2E592FC535DFF94 
{
public:
	// System.Int32 System.Net.Sockets.SocketType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketType_t234FBD298C115F92305ABC40D2E592FC535DFF94, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Threading.Tasks.Task
struct  Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60  : public RuntimeObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_taskId
	int32_t ___m_taskId_4;
	// System.Object System.Threading.Tasks.Task::m_action
	RuntimeObject * ___m_action_5;
	// System.Object System.Threading.Tasks.Task::m_stateObject
	RuntimeObject * ___m_stateObject_6;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.Task::m_taskScheduler
	TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * ___m_taskScheduler_7;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::m_parent
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_parent_8;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_stateFlags
	int32_t ___m_stateFlags_9;
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_continuationObject
	RuntimeObject * ___m_continuationObject_10;
	// System.Threading.Tasks.Task/ContingentProperties modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_contingentProperties
	ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * ___m_contingentProperties_15;

public:
	inline static int32_t get_offset_of_m_taskId_4() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskId_4)); }
	inline int32_t get_m_taskId_4() const { return ___m_taskId_4; }
	inline int32_t* get_address_of_m_taskId_4() { return &___m_taskId_4; }
	inline void set_m_taskId_4(int32_t value)
	{
		___m_taskId_4 = value;
	}

	inline static int32_t get_offset_of_m_action_5() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_action_5)); }
	inline RuntimeObject * get_m_action_5() const { return ___m_action_5; }
	inline RuntimeObject ** get_address_of_m_action_5() { return &___m_action_5; }
	inline void set_m_action_5(RuntimeObject * value)
	{
		___m_action_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_action_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateObject_6() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateObject_6)); }
	inline RuntimeObject * get_m_stateObject_6() const { return ___m_stateObject_6; }
	inline RuntimeObject ** get_address_of_m_stateObject_6() { return &___m_stateObject_6; }
	inline void set_m_stateObject_6(RuntimeObject * value)
	{
		___m_stateObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_taskScheduler_7() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskScheduler_7)); }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * get_m_taskScheduler_7() const { return ___m_taskScheduler_7; }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D ** get_address_of_m_taskScheduler_7() { return &___m_taskScheduler_7; }
	inline void set_m_taskScheduler_7(TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * value)
	{
		___m_taskScheduler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_taskScheduler_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_parent_8() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_parent_8)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_m_parent_8() const { return ___m_parent_8; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_m_parent_8() { return &___m_parent_8; }
	inline void set_m_parent_8(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___m_parent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parent_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateFlags_9() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateFlags_9)); }
	inline int32_t get_m_stateFlags_9() const { return ___m_stateFlags_9; }
	inline int32_t* get_address_of_m_stateFlags_9() { return &___m_stateFlags_9; }
	inline void set_m_stateFlags_9(int32_t value)
	{
		___m_stateFlags_9 = value;
	}

	inline static int32_t get_offset_of_m_continuationObject_10() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_continuationObject_10)); }
	inline RuntimeObject * get_m_continuationObject_10() const { return ___m_continuationObject_10; }
	inline RuntimeObject ** get_address_of_m_continuationObject_10() { return &___m_continuationObject_10; }
	inline void set_m_continuationObject_10(RuntimeObject * value)
	{
		___m_continuationObject_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_continuationObject_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_contingentProperties_15() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_contingentProperties_15)); }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * get_m_contingentProperties_15() const { return ___m_contingentProperties_15; }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 ** get_address_of_m_contingentProperties_15() { return &___m_contingentProperties_15; }
	inline void set_m_contingentProperties_15(ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * value)
	{
		___m_contingentProperties_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_contingentProperties_15), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields
{
public:
	// System.Int32 System.Threading.Tasks.Task::s_taskIdCounter
	int32_t ___s_taskIdCounter_2;
	// System.Threading.Tasks.TaskFactory System.Threading.Tasks.Task::s_factory
	TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * ___s_factory_3;
	// System.Object System.Threading.Tasks.Task::s_taskCompletionSentinel
	RuntimeObject * ___s_taskCompletionSentinel_11;
	// System.Boolean System.Threading.Tasks.Task::s_asyncDebuggingEnabled
	bool ___s_asyncDebuggingEnabled_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_currentActiveTasks
	Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * ___s_currentActiveTasks_13;
	// System.Object System.Threading.Tasks.Task::s_activeTasksLock
	RuntimeObject * ___s_activeTasksLock_14;
	// System.Action`1<System.Object> System.Threading.Tasks.Task::s_taskCancelCallback
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___s_taskCancelCallback_16;
	// System.Func`1<System.Threading.Tasks.Task/ContingentProperties> System.Threading.Tasks.Task::s_createContingentProperties
	Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * ___s_createContingentProperties_17;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::s_completedTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___s_completedTask_18;
	// System.Predicate`1<System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_IsExceptionObservedByParentPredicate
	Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * ___s_IsExceptionObservedByParentPredicate_19;
	// System.Threading.ContextCallback System.Threading.Tasks.Task::s_ecCallback
	ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * ___s_ecCallback_20;
	// System.Predicate`1<System.Object> System.Threading.Tasks.Task::s_IsTaskContinuationNullPredicate
	Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * ___s_IsTaskContinuationNullPredicate_21;

public:
	inline static int32_t get_offset_of_s_taskIdCounter_2() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskIdCounter_2)); }
	inline int32_t get_s_taskIdCounter_2() const { return ___s_taskIdCounter_2; }
	inline int32_t* get_address_of_s_taskIdCounter_2() { return &___s_taskIdCounter_2; }
	inline void set_s_taskIdCounter_2(int32_t value)
	{
		___s_taskIdCounter_2 = value;
	}

	inline static int32_t get_offset_of_s_factory_3() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_factory_3)); }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * get_s_factory_3() const { return ___s_factory_3; }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B ** get_address_of_s_factory_3() { return &___s_factory_3; }
	inline void set_s_factory_3(TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * value)
	{
		___s_factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_factory_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCompletionSentinel_11() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCompletionSentinel_11)); }
	inline RuntimeObject * get_s_taskCompletionSentinel_11() const { return ___s_taskCompletionSentinel_11; }
	inline RuntimeObject ** get_address_of_s_taskCompletionSentinel_11() { return &___s_taskCompletionSentinel_11; }
	inline void set_s_taskCompletionSentinel_11(RuntimeObject * value)
	{
		___s_taskCompletionSentinel_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCompletionSentinel_11), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncDebuggingEnabled_12() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_asyncDebuggingEnabled_12)); }
	inline bool get_s_asyncDebuggingEnabled_12() const { return ___s_asyncDebuggingEnabled_12; }
	inline bool* get_address_of_s_asyncDebuggingEnabled_12() { return &___s_asyncDebuggingEnabled_12; }
	inline void set_s_asyncDebuggingEnabled_12(bool value)
	{
		___s_asyncDebuggingEnabled_12 = value;
	}

	inline static int32_t get_offset_of_s_currentActiveTasks_13() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_currentActiveTasks_13)); }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * get_s_currentActiveTasks_13() const { return ___s_currentActiveTasks_13; }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 ** get_address_of_s_currentActiveTasks_13() { return &___s_currentActiveTasks_13; }
	inline void set_s_currentActiveTasks_13(Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * value)
	{
		___s_currentActiveTasks_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_currentActiveTasks_13), (void*)value);
	}

	inline static int32_t get_offset_of_s_activeTasksLock_14() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_activeTasksLock_14)); }
	inline RuntimeObject * get_s_activeTasksLock_14() const { return ___s_activeTasksLock_14; }
	inline RuntimeObject ** get_address_of_s_activeTasksLock_14() { return &___s_activeTasksLock_14; }
	inline void set_s_activeTasksLock_14(RuntimeObject * value)
	{
		___s_activeTasksLock_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_activeTasksLock_14), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCancelCallback_16() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCancelCallback_16)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_s_taskCancelCallback_16() const { return ___s_taskCancelCallback_16; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_s_taskCancelCallback_16() { return &___s_taskCancelCallback_16; }
	inline void set_s_taskCancelCallback_16(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___s_taskCancelCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCancelCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_createContingentProperties_17() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_createContingentProperties_17)); }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * get_s_createContingentProperties_17() const { return ___s_createContingentProperties_17; }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B ** get_address_of_s_createContingentProperties_17() { return &___s_createContingentProperties_17; }
	inline void set_s_createContingentProperties_17(Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * value)
	{
		___s_createContingentProperties_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_createContingentProperties_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_completedTask_18() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_completedTask_18)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_s_completedTask_18() const { return ___s_completedTask_18; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_s_completedTask_18() { return &___s_completedTask_18; }
	inline void set_s_completedTask_18(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___s_completedTask_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_completedTask_18), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsExceptionObservedByParentPredicate_19() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsExceptionObservedByParentPredicate_19)); }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * get_s_IsExceptionObservedByParentPredicate_19() const { return ___s_IsExceptionObservedByParentPredicate_19; }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD ** get_address_of_s_IsExceptionObservedByParentPredicate_19() { return &___s_IsExceptionObservedByParentPredicate_19; }
	inline void set_s_IsExceptionObservedByParentPredicate_19(Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * value)
	{
		___s_IsExceptionObservedByParentPredicate_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsExceptionObservedByParentPredicate_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_ecCallback_20() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_ecCallback_20)); }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * get_s_ecCallback_20() const { return ___s_ecCallback_20; }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B ** get_address_of_s_ecCallback_20() { return &___s_ecCallback_20; }
	inline void set_s_ecCallback_20(ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * value)
	{
		___s_ecCallback_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ecCallback_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsTaskContinuationNullPredicate_21() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsTaskContinuationNullPredicate_21)); }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * get_s_IsTaskContinuationNullPredicate_21() const { return ___s_IsTaskContinuationNullPredicate_21; }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB ** get_address_of_s_IsTaskContinuationNullPredicate_21() { return &___s_IsTaskContinuationNullPredicate_21; }
	inline void set_s_IsTaskContinuationNullPredicate_21(Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * value)
	{
		___s_IsTaskContinuationNullPredicate_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsTaskContinuationNullPredicate_21), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::t_currentTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___t_currentTask_0;
	// System.Threading.Tasks.StackGuard System.Threading.Tasks.Task::t_stackGuard
	StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * ___t_stackGuard_1;

public:
	inline static int32_t get_offset_of_t_currentTask_0() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_currentTask_0)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_t_currentTask_0() const { return ___t_currentTask_0; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_t_currentTask_0() { return &___t_currentTask_0; }
	inline void set_t_currentTask_0(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___t_currentTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_currentTask_0), (void*)value);
	}

	inline static int32_t get_offset_of_t_stackGuard_1() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_stackGuard_1)); }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * get_t_stackGuard_1() const { return ___t_stackGuard_1; }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D ** get_address_of_t_stackGuard_1() { return &___t_stackGuard_1; }
	inline void set_t_stackGuard_1(StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * value)
	{
		___t_stackGuard_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_stackGuard_1), (void*)value);
	}
};


// UnityEngine.UI.CanvasScaler/ScaleMode
struct  ScaleMode_t0CBCB9FD5EB6F84B682D0F5E4203D0925BCDB069 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScaleMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScaleMode_t0CBCB9FD5EB6F84B682D0F5E4203D0925BCDB069, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.CanvasScaler/ScreenMatchMode
struct  ScreenMatchMode_t64D475564756A5C040CC9B7C62D321C7133970DB 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScreenMatchMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenMatchMode_t64D475564756A5C040CC9B7C62D321C7133970DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.CanvasScaler/Unit
struct  Unit_t48D9126E954FB214B48FD2E199CB041FF97CFF80 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/Unit::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Unit_t48D9126E954FB214B48FD2E199CB041FF97CFF80, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.Net.IPAddress
struct  IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE  : public RuntimeObject
{
public:
	// System.Int64 System.Net.IPAddress::m_Address
	int64_t ___m_Address_5;
	// System.String System.Net.IPAddress::m_ToString
	String_t* ___m_ToString_6;
	// System.Net.Sockets.AddressFamily System.Net.IPAddress::m_Family
	int32_t ___m_Family_10;
	// System.UInt16[] System.Net.IPAddress::m_Numbers
	UInt16U5BU5D_t42D35C587B07DCDBCFDADF572C6D733AE85B2A67* ___m_Numbers_11;
	// System.Int64 System.Net.IPAddress::m_ScopeId
	int64_t ___m_ScopeId_12;
	// System.Int32 System.Net.IPAddress::m_HashCode
	int32_t ___m_HashCode_13;

public:
	inline static int32_t get_offset_of_m_Address_5() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE, ___m_Address_5)); }
	inline int64_t get_m_Address_5() const { return ___m_Address_5; }
	inline int64_t* get_address_of_m_Address_5() { return &___m_Address_5; }
	inline void set_m_Address_5(int64_t value)
	{
		___m_Address_5 = value;
	}

	inline static int32_t get_offset_of_m_ToString_6() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE, ___m_ToString_6)); }
	inline String_t* get_m_ToString_6() const { return ___m_ToString_6; }
	inline String_t** get_address_of_m_ToString_6() { return &___m_ToString_6; }
	inline void set_m_ToString_6(String_t* value)
	{
		___m_ToString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ToString_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Family_10() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE, ___m_Family_10)); }
	inline int32_t get_m_Family_10() const { return ___m_Family_10; }
	inline int32_t* get_address_of_m_Family_10() { return &___m_Family_10; }
	inline void set_m_Family_10(int32_t value)
	{
		___m_Family_10 = value;
	}

	inline static int32_t get_offset_of_m_Numbers_11() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE, ___m_Numbers_11)); }
	inline UInt16U5BU5D_t42D35C587B07DCDBCFDADF572C6D733AE85B2A67* get_m_Numbers_11() const { return ___m_Numbers_11; }
	inline UInt16U5BU5D_t42D35C587B07DCDBCFDADF572C6D733AE85B2A67** get_address_of_m_Numbers_11() { return &___m_Numbers_11; }
	inline void set_m_Numbers_11(UInt16U5BU5D_t42D35C587B07DCDBCFDADF572C6D733AE85B2A67* value)
	{
		___m_Numbers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Numbers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_ScopeId_12() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE, ___m_ScopeId_12)); }
	inline int64_t get_m_ScopeId_12() const { return ___m_ScopeId_12; }
	inline int64_t* get_address_of_m_ScopeId_12() { return &___m_ScopeId_12; }
	inline void set_m_ScopeId_12(int64_t value)
	{
		___m_ScopeId_12 = value;
	}

	inline static int32_t get_offset_of_m_HashCode_13() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE, ___m_HashCode_13)); }
	inline int32_t get_m_HashCode_13() const { return ___m_HashCode_13; }
	inline int32_t* get_address_of_m_HashCode_13() { return &___m_HashCode_13; }
	inline void set_m_HashCode_13(int32_t value)
	{
		___m_HashCode_13 = value;
	}
};

struct IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_StaticFields
{
public:
	// System.Net.IPAddress System.Net.IPAddress::Any
	IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * ___Any_0;
	// System.Net.IPAddress System.Net.IPAddress::Loopback
	IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * ___Loopback_1;
	// System.Net.IPAddress System.Net.IPAddress::Broadcast
	IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * ___Broadcast_2;
	// System.Net.IPAddress System.Net.IPAddress::None
	IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * ___None_3;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Any
	IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * ___IPv6Any_7;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Loopback
	IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * ___IPv6Loopback_8;
	// System.Net.IPAddress System.Net.IPAddress::IPv6None
	IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * ___IPv6None_9;

public:
	inline static int32_t get_offset_of_Any_0() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_StaticFields, ___Any_0)); }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * get_Any_0() const { return ___Any_0; }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE ** get_address_of_Any_0() { return &___Any_0; }
	inline void set_Any_0(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * value)
	{
		___Any_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Any_0), (void*)value);
	}

	inline static int32_t get_offset_of_Loopback_1() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_StaticFields, ___Loopback_1)); }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * get_Loopback_1() const { return ___Loopback_1; }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE ** get_address_of_Loopback_1() { return &___Loopback_1; }
	inline void set_Loopback_1(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * value)
	{
		___Loopback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Loopback_1), (void*)value);
	}

	inline static int32_t get_offset_of_Broadcast_2() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_StaticFields, ___Broadcast_2)); }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * get_Broadcast_2() const { return ___Broadcast_2; }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE ** get_address_of_Broadcast_2() { return &___Broadcast_2; }
	inline void set_Broadcast_2(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * value)
	{
		___Broadcast_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Broadcast_2), (void*)value);
	}

	inline static int32_t get_offset_of_None_3() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_StaticFields, ___None_3)); }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * get_None_3() const { return ___None_3; }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE ** get_address_of_None_3() { return &___None_3; }
	inline void set_None_3(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * value)
	{
		___None_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___None_3), (void*)value);
	}

	inline static int32_t get_offset_of_IPv6Any_7() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_StaticFields, ___IPv6Any_7)); }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * get_IPv6Any_7() const { return ___IPv6Any_7; }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE ** get_address_of_IPv6Any_7() { return &___IPv6Any_7; }
	inline void set_IPv6Any_7(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * value)
	{
		___IPv6Any_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___IPv6Any_7), (void*)value);
	}

	inline static int32_t get_offset_of_IPv6Loopback_8() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_StaticFields, ___IPv6Loopback_8)); }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * get_IPv6Loopback_8() const { return ___IPv6Loopback_8; }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE ** get_address_of_IPv6Loopback_8() { return &___IPv6Loopback_8; }
	inline void set_IPv6Loopback_8(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * value)
	{
		___IPv6Loopback_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___IPv6Loopback_8), (void*)value);
	}

	inline static int32_t get_offset_of_IPv6None_9() { return static_cast<int32_t>(offsetof(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_StaticFields, ___IPv6None_9)); }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * get_IPv6None_9() const { return ___IPv6None_9; }
	inline IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE ** get_address_of_IPv6None_9() { return &___IPv6None_9; }
	inline void set_IPv6None_9(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * value)
	{
		___IPv6None_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___IPv6None_9), (void*)value);
	}
};


// Program.UI.Instruments
struct  Instruments_tF75DA728107310E221C75AD3538952EAEE98442E  : public RuntimeObject
{
public:
	// Program.UI.Instrument Program.UI.Instruments::<InstrumentTake>k__BackingField
	int32_t ___U3CInstrumentTakeU3Ek__BackingField_0;
	// System.Action Program.UI.Instruments::TakeInstrument
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___TakeInstrument_1;

public:
	inline static int32_t get_offset_of_U3CInstrumentTakeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Instruments_tF75DA728107310E221C75AD3538952EAEE98442E, ___U3CInstrumentTakeU3Ek__BackingField_0)); }
	inline int32_t get_U3CInstrumentTakeU3Ek__BackingField_0() const { return ___U3CInstrumentTakeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CInstrumentTakeU3Ek__BackingField_0() { return &___U3CInstrumentTakeU3Ek__BackingField_0; }
	inline void set_U3CInstrumentTakeU3Ek__BackingField_0(int32_t value)
	{
		___U3CInstrumentTakeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_TakeInstrument_1() { return static_cast<int32_t>(offsetof(Instruments_tF75DA728107310E221C75AD3538952EAEE98442E, ___TakeInstrument_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_TakeInstrument_1() const { return ___TakeInstrument_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_TakeInstrument_1() { return &___TakeInstrument_1; }
	inline void set_TakeInstrument_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___TakeInstrument_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TakeInstrument_1), (void*)value);
	}
};


// UnityEngine.Mesh
struct  Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Net.Sockets.Socket
struct  Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09  : public RuntimeObject
{
public:
	// System.Boolean System.Net.Sockets.Socket::is_closed
	bool ___is_closed_6;
	// System.Boolean System.Net.Sockets.Socket::is_listening
	bool ___is_listening_7;
	// System.Boolean System.Net.Sockets.Socket::useOverlappedIO
	bool ___useOverlappedIO_8;
	// System.Int32 System.Net.Sockets.Socket::linger_timeout
	int32_t ___linger_timeout_9;
	// System.Net.Sockets.AddressFamily System.Net.Sockets.Socket::addressFamily
	int32_t ___addressFamily_10;
	// System.Net.Sockets.SocketType System.Net.Sockets.Socket::socketType
	int32_t ___socketType_11;
	// System.Net.Sockets.ProtocolType System.Net.Sockets.Socket::protocolType
	int32_t ___protocolType_12;
	// System.Net.Sockets.SafeSocketHandle System.Net.Sockets.Socket::m_Handle
	SafeSocketHandle_t5050671179FB886DA1763A0E4EFB3FCD072363C9 * ___m_Handle_13;
	// System.Net.EndPoint System.Net.Sockets.Socket::seed_endpoint
	EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * ___seed_endpoint_14;
	// System.Threading.SemaphoreSlim System.Net.Sockets.Socket::ReadSem
	SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * ___ReadSem_15;
	// System.Threading.SemaphoreSlim System.Net.Sockets.Socket::WriteSem
	SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * ___WriteSem_16;
	// System.Boolean System.Net.Sockets.Socket::is_blocking
	bool ___is_blocking_17;
	// System.Boolean System.Net.Sockets.Socket::is_bound
	bool ___is_bound_18;
	// System.Boolean System.Net.Sockets.Socket::is_connected
	bool ___is_connected_19;
	// System.Int32 System.Net.Sockets.Socket::m_IntCleanedUp
	int32_t ___m_IntCleanedUp_20;
	// System.Boolean System.Net.Sockets.Socket::connect_in_progress
	bool ___connect_in_progress_21;
	// System.Int32 System.Net.Sockets.Socket::ID
	int32_t ___ID_22;

public:
	inline static int32_t get_offset_of_is_closed_6() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___is_closed_6)); }
	inline bool get_is_closed_6() const { return ___is_closed_6; }
	inline bool* get_address_of_is_closed_6() { return &___is_closed_6; }
	inline void set_is_closed_6(bool value)
	{
		___is_closed_6 = value;
	}

	inline static int32_t get_offset_of_is_listening_7() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___is_listening_7)); }
	inline bool get_is_listening_7() const { return ___is_listening_7; }
	inline bool* get_address_of_is_listening_7() { return &___is_listening_7; }
	inline void set_is_listening_7(bool value)
	{
		___is_listening_7 = value;
	}

	inline static int32_t get_offset_of_useOverlappedIO_8() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___useOverlappedIO_8)); }
	inline bool get_useOverlappedIO_8() const { return ___useOverlappedIO_8; }
	inline bool* get_address_of_useOverlappedIO_8() { return &___useOverlappedIO_8; }
	inline void set_useOverlappedIO_8(bool value)
	{
		___useOverlappedIO_8 = value;
	}

	inline static int32_t get_offset_of_linger_timeout_9() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___linger_timeout_9)); }
	inline int32_t get_linger_timeout_9() const { return ___linger_timeout_9; }
	inline int32_t* get_address_of_linger_timeout_9() { return &___linger_timeout_9; }
	inline void set_linger_timeout_9(int32_t value)
	{
		___linger_timeout_9 = value;
	}

	inline static int32_t get_offset_of_addressFamily_10() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___addressFamily_10)); }
	inline int32_t get_addressFamily_10() const { return ___addressFamily_10; }
	inline int32_t* get_address_of_addressFamily_10() { return &___addressFamily_10; }
	inline void set_addressFamily_10(int32_t value)
	{
		___addressFamily_10 = value;
	}

	inline static int32_t get_offset_of_socketType_11() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___socketType_11)); }
	inline int32_t get_socketType_11() const { return ___socketType_11; }
	inline int32_t* get_address_of_socketType_11() { return &___socketType_11; }
	inline void set_socketType_11(int32_t value)
	{
		___socketType_11 = value;
	}

	inline static int32_t get_offset_of_protocolType_12() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___protocolType_12)); }
	inline int32_t get_protocolType_12() const { return ___protocolType_12; }
	inline int32_t* get_address_of_protocolType_12() { return &___protocolType_12; }
	inline void set_protocolType_12(int32_t value)
	{
		___protocolType_12 = value;
	}

	inline static int32_t get_offset_of_m_Handle_13() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___m_Handle_13)); }
	inline SafeSocketHandle_t5050671179FB886DA1763A0E4EFB3FCD072363C9 * get_m_Handle_13() const { return ___m_Handle_13; }
	inline SafeSocketHandle_t5050671179FB886DA1763A0E4EFB3FCD072363C9 ** get_address_of_m_Handle_13() { return &___m_Handle_13; }
	inline void set_m_Handle_13(SafeSocketHandle_t5050671179FB886DA1763A0E4EFB3FCD072363C9 * value)
	{
		___m_Handle_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Handle_13), (void*)value);
	}

	inline static int32_t get_offset_of_seed_endpoint_14() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___seed_endpoint_14)); }
	inline EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * get_seed_endpoint_14() const { return ___seed_endpoint_14; }
	inline EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA ** get_address_of_seed_endpoint_14() { return &___seed_endpoint_14; }
	inline void set_seed_endpoint_14(EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * value)
	{
		___seed_endpoint_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___seed_endpoint_14), (void*)value);
	}

	inline static int32_t get_offset_of_ReadSem_15() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___ReadSem_15)); }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * get_ReadSem_15() const { return ___ReadSem_15; }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 ** get_address_of_ReadSem_15() { return &___ReadSem_15; }
	inline void set_ReadSem_15(SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * value)
	{
		___ReadSem_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ReadSem_15), (void*)value);
	}

	inline static int32_t get_offset_of_WriteSem_16() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___WriteSem_16)); }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * get_WriteSem_16() const { return ___WriteSem_16; }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 ** get_address_of_WriteSem_16() { return &___WriteSem_16; }
	inline void set_WriteSem_16(SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * value)
	{
		___WriteSem_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WriteSem_16), (void*)value);
	}

	inline static int32_t get_offset_of_is_blocking_17() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___is_blocking_17)); }
	inline bool get_is_blocking_17() const { return ___is_blocking_17; }
	inline bool* get_address_of_is_blocking_17() { return &___is_blocking_17; }
	inline void set_is_blocking_17(bool value)
	{
		___is_blocking_17 = value;
	}

	inline static int32_t get_offset_of_is_bound_18() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___is_bound_18)); }
	inline bool get_is_bound_18() const { return ___is_bound_18; }
	inline bool* get_address_of_is_bound_18() { return &___is_bound_18; }
	inline void set_is_bound_18(bool value)
	{
		___is_bound_18 = value;
	}

	inline static int32_t get_offset_of_is_connected_19() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___is_connected_19)); }
	inline bool get_is_connected_19() const { return ___is_connected_19; }
	inline bool* get_address_of_is_connected_19() { return &___is_connected_19; }
	inline void set_is_connected_19(bool value)
	{
		___is_connected_19 = value;
	}

	inline static int32_t get_offset_of_m_IntCleanedUp_20() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___m_IntCleanedUp_20)); }
	inline int32_t get_m_IntCleanedUp_20() const { return ___m_IntCleanedUp_20; }
	inline int32_t* get_address_of_m_IntCleanedUp_20() { return &___m_IntCleanedUp_20; }
	inline void set_m_IntCleanedUp_20(int32_t value)
	{
		___m_IntCleanedUp_20 = value;
	}

	inline static int32_t get_offset_of_connect_in_progress_21() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___connect_in_progress_21)); }
	inline bool get_connect_in_progress_21() const { return ___connect_in_progress_21; }
	inline bool* get_address_of_connect_in_progress_21() { return &___connect_in_progress_21; }
	inline void set_connect_in_progress_21(bool value)
	{
		___connect_in_progress_21 = value;
	}

	inline static int32_t get_offset_of_ID_22() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09, ___ID_22)); }
	inline int32_t get_ID_22() const { return ___ID_22; }
	inline int32_t* get_address_of_ID_22() { return &___ID_22; }
	inline void set_ID_22(int32_t value)
	{
		___ID_22 = value;
	}
};

struct Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields
{
public:
	// System.Object System.Net.Sockets.Socket::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_0;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_SupportsIPv4
	bool ___s_SupportsIPv4_1;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_SupportsIPv6
	bool ___s_SupportsIPv6_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_OSSupportsIPv6
	bool ___s_OSSupportsIPv6_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_Initialized
	bool ___s_Initialized_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.Sockets.Socket::s_LoggingEnabled
	bool ___s_LoggingEnabled_5;
	// System.AsyncCallback System.Net.Sockets.Socket::AcceptAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___AcceptAsyncCallback_23;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginAcceptCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginAcceptCallback_24;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginAcceptReceiveCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginAcceptReceiveCallback_25;
	// System.AsyncCallback System.Net.Sockets.Socket::ConnectAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___ConnectAsyncCallback_26;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginConnectCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginConnectCallback_27;
	// System.AsyncCallback System.Net.Sockets.Socket::DisconnectAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___DisconnectAsyncCallback_28;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginDisconnectCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginDisconnectCallback_29;
	// System.AsyncCallback System.Net.Sockets.Socket::ReceiveAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___ReceiveAsyncCallback_30;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginReceiveCallback_31;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveGenericCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginReceiveGenericCallback_32;
	// System.AsyncCallback System.Net.Sockets.Socket::ReceiveFromAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___ReceiveFromAsyncCallback_33;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginReceiveFromCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginReceiveFromCallback_34;
	// System.AsyncCallback System.Net.Sockets.Socket::SendAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___SendAsyncCallback_35;
	// System.IOAsyncCallback System.Net.Sockets.Socket::BeginSendGenericCallback
	IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * ___BeginSendGenericCallback_36;
	// System.AsyncCallback System.Net.Sockets.Socket::SendToAsyncCallback
	AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___SendToAsyncCallback_37;

public:
	inline static int32_t get_offset_of_s_InternalSyncObject_0() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_InternalSyncObject_0)); }
	inline RuntimeObject * get_s_InternalSyncObject_0() const { return ___s_InternalSyncObject_0; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_0() { return &___s_InternalSyncObject_0; }
	inline void set_s_InternalSyncObject_0(RuntimeObject * value)
	{
		___s_InternalSyncObject_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_SupportsIPv4_1() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_SupportsIPv4_1)); }
	inline bool get_s_SupportsIPv4_1() const { return ___s_SupportsIPv4_1; }
	inline bool* get_address_of_s_SupportsIPv4_1() { return &___s_SupportsIPv4_1; }
	inline void set_s_SupportsIPv4_1(bool value)
	{
		___s_SupportsIPv4_1 = value;
	}

	inline static int32_t get_offset_of_s_SupportsIPv6_2() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_SupportsIPv6_2)); }
	inline bool get_s_SupportsIPv6_2() const { return ___s_SupportsIPv6_2; }
	inline bool* get_address_of_s_SupportsIPv6_2() { return &___s_SupportsIPv6_2; }
	inline void set_s_SupportsIPv6_2(bool value)
	{
		___s_SupportsIPv6_2 = value;
	}

	inline static int32_t get_offset_of_s_OSSupportsIPv6_3() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_OSSupportsIPv6_3)); }
	inline bool get_s_OSSupportsIPv6_3() const { return ___s_OSSupportsIPv6_3; }
	inline bool* get_address_of_s_OSSupportsIPv6_3() { return &___s_OSSupportsIPv6_3; }
	inline void set_s_OSSupportsIPv6_3(bool value)
	{
		___s_OSSupportsIPv6_3 = value;
	}

	inline static int32_t get_offset_of_s_Initialized_4() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_Initialized_4)); }
	inline bool get_s_Initialized_4() const { return ___s_Initialized_4; }
	inline bool* get_address_of_s_Initialized_4() { return &___s_Initialized_4; }
	inline void set_s_Initialized_4(bool value)
	{
		___s_Initialized_4 = value;
	}

	inline static int32_t get_offset_of_s_LoggingEnabled_5() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___s_LoggingEnabled_5)); }
	inline bool get_s_LoggingEnabled_5() const { return ___s_LoggingEnabled_5; }
	inline bool* get_address_of_s_LoggingEnabled_5() { return &___s_LoggingEnabled_5; }
	inline void set_s_LoggingEnabled_5(bool value)
	{
		___s_LoggingEnabled_5 = value;
	}

	inline static int32_t get_offset_of_AcceptAsyncCallback_23() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___AcceptAsyncCallback_23)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_AcceptAsyncCallback_23() const { return ___AcceptAsyncCallback_23; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_AcceptAsyncCallback_23() { return &___AcceptAsyncCallback_23; }
	inline void set_AcceptAsyncCallback_23(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___AcceptAsyncCallback_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AcceptAsyncCallback_23), (void*)value);
	}

	inline static int32_t get_offset_of_BeginAcceptCallback_24() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginAcceptCallback_24)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginAcceptCallback_24() const { return ___BeginAcceptCallback_24; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginAcceptCallback_24() { return &___BeginAcceptCallback_24; }
	inline void set_BeginAcceptCallback_24(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginAcceptCallback_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginAcceptCallback_24), (void*)value);
	}

	inline static int32_t get_offset_of_BeginAcceptReceiveCallback_25() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginAcceptReceiveCallback_25)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginAcceptReceiveCallback_25() const { return ___BeginAcceptReceiveCallback_25; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginAcceptReceiveCallback_25() { return &___BeginAcceptReceiveCallback_25; }
	inline void set_BeginAcceptReceiveCallback_25(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginAcceptReceiveCallback_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginAcceptReceiveCallback_25), (void*)value);
	}

	inline static int32_t get_offset_of_ConnectAsyncCallback_26() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___ConnectAsyncCallback_26)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_ConnectAsyncCallback_26() const { return ___ConnectAsyncCallback_26; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_ConnectAsyncCallback_26() { return &___ConnectAsyncCallback_26; }
	inline void set_ConnectAsyncCallback_26(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___ConnectAsyncCallback_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConnectAsyncCallback_26), (void*)value);
	}

	inline static int32_t get_offset_of_BeginConnectCallback_27() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginConnectCallback_27)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginConnectCallback_27() const { return ___BeginConnectCallback_27; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginConnectCallback_27() { return &___BeginConnectCallback_27; }
	inline void set_BeginConnectCallback_27(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginConnectCallback_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginConnectCallback_27), (void*)value);
	}

	inline static int32_t get_offset_of_DisconnectAsyncCallback_28() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___DisconnectAsyncCallback_28)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_DisconnectAsyncCallback_28() const { return ___DisconnectAsyncCallback_28; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_DisconnectAsyncCallback_28() { return &___DisconnectAsyncCallback_28; }
	inline void set_DisconnectAsyncCallback_28(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___DisconnectAsyncCallback_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DisconnectAsyncCallback_28), (void*)value);
	}

	inline static int32_t get_offset_of_BeginDisconnectCallback_29() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginDisconnectCallback_29)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginDisconnectCallback_29() const { return ___BeginDisconnectCallback_29; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginDisconnectCallback_29() { return &___BeginDisconnectCallback_29; }
	inline void set_BeginDisconnectCallback_29(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginDisconnectCallback_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginDisconnectCallback_29), (void*)value);
	}

	inline static int32_t get_offset_of_ReceiveAsyncCallback_30() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___ReceiveAsyncCallback_30)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_ReceiveAsyncCallback_30() const { return ___ReceiveAsyncCallback_30; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_ReceiveAsyncCallback_30() { return &___ReceiveAsyncCallback_30; }
	inline void set_ReceiveAsyncCallback_30(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___ReceiveAsyncCallback_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ReceiveAsyncCallback_30), (void*)value);
	}

	inline static int32_t get_offset_of_BeginReceiveCallback_31() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginReceiveCallback_31)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginReceiveCallback_31() const { return ___BeginReceiveCallback_31; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginReceiveCallback_31() { return &___BeginReceiveCallback_31; }
	inline void set_BeginReceiveCallback_31(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginReceiveCallback_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginReceiveCallback_31), (void*)value);
	}

	inline static int32_t get_offset_of_BeginReceiveGenericCallback_32() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginReceiveGenericCallback_32)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginReceiveGenericCallback_32() const { return ___BeginReceiveGenericCallback_32; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginReceiveGenericCallback_32() { return &___BeginReceiveGenericCallback_32; }
	inline void set_BeginReceiveGenericCallback_32(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginReceiveGenericCallback_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginReceiveGenericCallback_32), (void*)value);
	}

	inline static int32_t get_offset_of_ReceiveFromAsyncCallback_33() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___ReceiveFromAsyncCallback_33)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_ReceiveFromAsyncCallback_33() const { return ___ReceiveFromAsyncCallback_33; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_ReceiveFromAsyncCallback_33() { return &___ReceiveFromAsyncCallback_33; }
	inline void set_ReceiveFromAsyncCallback_33(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___ReceiveFromAsyncCallback_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ReceiveFromAsyncCallback_33), (void*)value);
	}

	inline static int32_t get_offset_of_BeginReceiveFromCallback_34() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginReceiveFromCallback_34)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginReceiveFromCallback_34() const { return ___BeginReceiveFromCallback_34; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginReceiveFromCallback_34() { return &___BeginReceiveFromCallback_34; }
	inline void set_BeginReceiveFromCallback_34(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginReceiveFromCallback_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginReceiveFromCallback_34), (void*)value);
	}

	inline static int32_t get_offset_of_SendAsyncCallback_35() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___SendAsyncCallback_35)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_SendAsyncCallback_35() const { return ___SendAsyncCallback_35; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_SendAsyncCallback_35() { return &___SendAsyncCallback_35; }
	inline void set_SendAsyncCallback_35(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___SendAsyncCallback_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SendAsyncCallback_35), (void*)value);
	}

	inline static int32_t get_offset_of_BeginSendGenericCallback_36() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___BeginSendGenericCallback_36)); }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * get_BeginSendGenericCallback_36() const { return ___BeginSendGenericCallback_36; }
	inline IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E ** get_address_of_BeginSendGenericCallback_36() { return &___BeginSendGenericCallback_36; }
	inline void set_BeginSendGenericCallback_36(IOAsyncCallback_tB965FCE75DB2822B784F36808F71EA447D5F977E * value)
	{
		___BeginSendGenericCallback_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BeginSendGenericCallback_36), (void*)value);
	}

	inline static int32_t get_offset_of_SendToAsyncCallback_37() { return static_cast<int32_t>(offsetof(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_StaticFields, ___SendToAsyncCallback_37)); }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * get_SendToAsyncCallback_37() const { return ___SendToAsyncCallback_37; }
	inline AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA ** get_address_of_SendToAsyncCallback_37() { return &___SendToAsyncCallback_37; }
	inline void set_SendToAsyncCallback_37(AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * value)
	{
		___SendToAsyncCallback_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SendToAsyncCallback_37), (void*)value);
	}
};


// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8
struct  U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 
{
public:
	// System.Int32 FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8::<>t__builder
	AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6  ___U3CU3Et__builder_1;
	// FilerServer.Base.BaseConnect/BaseProcces FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8::<>4__this
	BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * ___U3CU3E4__this_2;
	// System.Runtime.CompilerServices.TaskAwaiter FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8::<>u__1
	TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708, ___U3CU3Et__builder_1)); }
	inline AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_synchronizationContext_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708, ___U3CU3E4__this_2)); }
	inline BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708, ___U3CU3Eu__1_3)); }
	inline TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  value)
	{
		___U3CU3Eu__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Eu__1_3))->___m_task_0), (void*)NULL);
	}
};


// System.Action
struct  Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MeshFilter
struct  MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Texture2D
struct  Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.MeshRenderer
struct  MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// Program.Audio.AudioController
struct  AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AudioSource Program.Audio.AudioController::clickButton
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___clickButton_4;
	// UnityEngine.AudioSource Program.Audio.AudioController::clickColorButton
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___clickColorButton_5;
	// UnityEngine.AudioSource Program.Audio.AudioController::paint
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___paint_6;

public:
	inline static int32_t get_offset_of_clickButton_4() { return static_cast<int32_t>(offsetof(AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F, ___clickButton_4)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_clickButton_4() const { return ___clickButton_4; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_clickButton_4() { return &___clickButton_4; }
	inline void set_clickButton_4(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___clickButton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clickButton_4), (void*)value);
	}

	inline static int32_t get_offset_of_clickColorButton_5() { return static_cast<int32_t>(offsetof(AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F, ___clickColorButton_5)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_clickColorButton_5() const { return ___clickColorButton_5; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_clickColorButton_5() { return &___clickColorButton_5; }
	inline void set_clickColorButton_5(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___clickColorButton_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clickColorButton_5), (void*)value);
	}

	inline static int32_t get_offset_of_paint_6() { return static_cast<int32_t>(offsetof(AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F, ___paint_6)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_paint_6() const { return ___paint_6; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_paint_6() { return &___paint_6; }
	inline void set_paint_6(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___paint_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___paint_6), (void*)value);
	}
};

struct AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_StaticFields
{
public:
	// Program.Audio.AudioController Program.Audio.AudioController::<Singleton>k__BackingField
	AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F * ___U3CSingletonU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CSingletonU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_StaticFields, ___U3CSingletonU3Ek__BackingField_7)); }
	inline AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F * get_U3CSingletonU3Ek__BackingField_7() const { return ___U3CSingletonU3Ek__BackingField_7; }
	inline AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F ** get_address_of_U3CSingletonU3Ek__BackingField_7() { return &___U3CSingletonU3Ek__BackingField_7; }
	inline void set_U3CSingletonU3Ek__BackingField_7(AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F * value)
	{
		___U3CSingletonU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSingletonU3Ek__BackingField_7), (void*)value);
	}
};


// Program.Menu.BaseMenu
struct  BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_StaticFields
{
public:
	// Program.Menu.BaseMenu Program.Menu.BaseMenu::<Singleton>k__BackingField
	BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 * ___U3CSingletonU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CSingletonU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_StaticFields, ___U3CSingletonU3Ek__BackingField_4)); }
	inline BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 * get_U3CSingletonU3Ek__BackingField_4() const { return ___U3CSingletonU3Ek__BackingField_4; }
	inline BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 ** get_address_of_U3CSingletonU3Ek__BackingField_4() { return &___U3CSingletonU3Ek__BackingField_4; }
	inline void set_U3CSingletonU3Ek__BackingField_4(BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 * value)
	{
		___U3CSingletonU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSingletonU3Ek__BackingField_4), (void*)value);
	}
};


// Program.UI.BodyPanel
struct  BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Button Program.UI.BodyPanel::renderButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___renderButton_4;
	// UnityEngine.UI.Button Program.UI.BodyPanel::layerDownButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___layerDownButton_5;
	// UnityEngine.UI.Button Program.UI.BodyPanel::openUV
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___openUV_6;
	// UnityEngine.UI.Button Program.UI.BodyPanel::headButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___headButton_7;
	// UnityEngine.UI.Image Program.UI.BodyPanel::headImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___headImage_8;
	// UnityEngine.UI.Button Program.UI.BodyPanel::bodyButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___bodyButton_9;
	// UnityEngine.UI.Image Program.UI.BodyPanel::bodyImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___bodyImage_10;
	// UnityEngine.UI.Button Program.UI.BodyPanel::lhandButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___lhandButton_11;
	// UnityEngine.UI.Image Program.UI.BodyPanel::lhandImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___lhandImage_12;
	// UnityEngine.UI.Button Program.UI.BodyPanel::rhandButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___rhandButton_13;
	// UnityEngine.UI.Image Program.UI.BodyPanel::rhandImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___rhandImage_14;
	// UnityEngine.UI.Button Program.UI.BodyPanel::lfutButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___lfutButton_15;
	// UnityEngine.UI.Image Program.UI.BodyPanel::lfutImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___lfutImage_16;
	// UnityEngine.UI.Button Program.UI.BodyPanel::rfutButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___rfutButton_17;
	// UnityEngine.UI.Image Program.UI.BodyPanel::rfutImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___rfutImage_18;

public:
	inline static int32_t get_offset_of_renderButton_4() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___renderButton_4)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_renderButton_4() const { return ___renderButton_4; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_renderButton_4() { return &___renderButton_4; }
	inline void set_renderButton_4(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___renderButton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___renderButton_4), (void*)value);
	}

	inline static int32_t get_offset_of_layerDownButton_5() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___layerDownButton_5)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_layerDownButton_5() const { return ___layerDownButton_5; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_layerDownButton_5() { return &___layerDownButton_5; }
	inline void set_layerDownButton_5(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___layerDownButton_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layerDownButton_5), (void*)value);
	}

	inline static int32_t get_offset_of_openUV_6() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___openUV_6)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_openUV_6() const { return ___openUV_6; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_openUV_6() { return &___openUV_6; }
	inline void set_openUV_6(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___openUV_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___openUV_6), (void*)value);
	}

	inline static int32_t get_offset_of_headButton_7() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___headButton_7)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_headButton_7() const { return ___headButton_7; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_headButton_7() { return &___headButton_7; }
	inline void set_headButton_7(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___headButton_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___headButton_7), (void*)value);
	}

	inline static int32_t get_offset_of_headImage_8() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___headImage_8)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_headImage_8() const { return ___headImage_8; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_headImage_8() { return &___headImage_8; }
	inline void set_headImage_8(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___headImage_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___headImage_8), (void*)value);
	}

	inline static int32_t get_offset_of_bodyButton_9() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___bodyButton_9)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_bodyButton_9() const { return ___bodyButton_9; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_bodyButton_9() { return &___bodyButton_9; }
	inline void set_bodyButton_9(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___bodyButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bodyButton_9), (void*)value);
	}

	inline static int32_t get_offset_of_bodyImage_10() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___bodyImage_10)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_bodyImage_10() const { return ___bodyImage_10; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_bodyImage_10() { return &___bodyImage_10; }
	inline void set_bodyImage_10(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___bodyImage_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bodyImage_10), (void*)value);
	}

	inline static int32_t get_offset_of_lhandButton_11() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___lhandButton_11)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_lhandButton_11() const { return ___lhandButton_11; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_lhandButton_11() { return &___lhandButton_11; }
	inline void set_lhandButton_11(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___lhandButton_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lhandButton_11), (void*)value);
	}

	inline static int32_t get_offset_of_lhandImage_12() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___lhandImage_12)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_lhandImage_12() const { return ___lhandImage_12; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_lhandImage_12() { return &___lhandImage_12; }
	inline void set_lhandImage_12(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___lhandImage_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lhandImage_12), (void*)value);
	}

	inline static int32_t get_offset_of_rhandButton_13() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___rhandButton_13)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_rhandButton_13() const { return ___rhandButton_13; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_rhandButton_13() { return &___rhandButton_13; }
	inline void set_rhandButton_13(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___rhandButton_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rhandButton_13), (void*)value);
	}

	inline static int32_t get_offset_of_rhandImage_14() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___rhandImage_14)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_rhandImage_14() const { return ___rhandImage_14; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_rhandImage_14() { return &___rhandImage_14; }
	inline void set_rhandImage_14(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___rhandImage_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rhandImage_14), (void*)value);
	}

	inline static int32_t get_offset_of_lfutButton_15() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___lfutButton_15)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_lfutButton_15() const { return ___lfutButton_15; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_lfutButton_15() { return &___lfutButton_15; }
	inline void set_lfutButton_15(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___lfutButton_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lfutButton_15), (void*)value);
	}

	inline static int32_t get_offset_of_lfutImage_16() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___lfutImage_16)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_lfutImage_16() const { return ___lfutImage_16; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_lfutImage_16() { return &___lfutImage_16; }
	inline void set_lfutImage_16(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___lfutImage_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lfutImage_16), (void*)value);
	}

	inline static int32_t get_offset_of_rfutButton_17() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___rfutButton_17)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_rfutButton_17() const { return ___rfutButton_17; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_rfutButton_17() { return &___rfutButton_17; }
	inline void set_rfutButton_17(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___rfutButton_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rfutButton_17), (void*)value);
	}

	inline static int32_t get_offset_of_rfutImage_18() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15, ___rfutImage_18)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_rfutImage_18() const { return ___rfutImage_18; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_rfutImage_18() { return &___rfutImage_18; }
	inline void set_rfutImage_18(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___rfutImage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rfutImage_18), (void*)value);
	}
};

struct BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_StaticFields
{
public:
	// Program.UI.DownLayer Program.UI.BodyPanel::<LayerDown>k__BackingField
	DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * ___U3CLayerDownU3Ek__BackingField_19;
	// Program.UI.BodyParts Program.UI.BodyPanel::<Parts>k__BackingField
	BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3 * ___U3CPartsU3Ek__BackingField_20;
	// Program.UI.Render Program.UI.BodyPanel::<StartRender>k__BackingField
	Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08 * ___U3CStartRenderU3Ek__BackingField_21;
	// Program.UI.UVPanel Program.UI.BodyPanel::<UVEditor>k__BackingField
	UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9 * ___U3CUVEditorU3Ek__BackingField_22;
	// Program.UI.BodyPanel Program.UI.BodyPanel::<Singleton>k__BackingField
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15 * ___U3CSingletonU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_U3CLayerDownU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_StaticFields, ___U3CLayerDownU3Ek__BackingField_19)); }
	inline DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * get_U3CLayerDownU3Ek__BackingField_19() const { return ___U3CLayerDownU3Ek__BackingField_19; }
	inline DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C ** get_address_of_U3CLayerDownU3Ek__BackingField_19() { return &___U3CLayerDownU3Ek__BackingField_19; }
	inline void set_U3CLayerDownU3Ek__BackingField_19(DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * value)
	{
		___U3CLayerDownU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLayerDownU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPartsU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_StaticFields, ___U3CPartsU3Ek__BackingField_20)); }
	inline BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3 * get_U3CPartsU3Ek__BackingField_20() const { return ___U3CPartsU3Ek__BackingField_20; }
	inline BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3 ** get_address_of_U3CPartsU3Ek__BackingField_20() { return &___U3CPartsU3Ek__BackingField_20; }
	inline void set_U3CPartsU3Ek__BackingField_20(BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3 * value)
	{
		___U3CPartsU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPartsU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStartRenderU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_StaticFields, ___U3CStartRenderU3Ek__BackingField_21)); }
	inline Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08 * get_U3CStartRenderU3Ek__BackingField_21() const { return ___U3CStartRenderU3Ek__BackingField_21; }
	inline Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08 ** get_address_of_U3CStartRenderU3Ek__BackingField_21() { return &___U3CStartRenderU3Ek__BackingField_21; }
	inline void set_U3CStartRenderU3Ek__BackingField_21(Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08 * value)
	{
		___U3CStartRenderU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStartRenderU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUVEditorU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_StaticFields, ___U3CUVEditorU3Ek__BackingField_22)); }
	inline UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9 * get_U3CUVEditorU3Ek__BackingField_22() const { return ___U3CUVEditorU3Ek__BackingField_22; }
	inline UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9 ** get_address_of_U3CUVEditorU3Ek__BackingField_22() { return &___U3CUVEditorU3Ek__BackingField_22; }
	inline void set_U3CUVEditorU3Ek__BackingField_22(UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9 * value)
	{
		___U3CUVEditorU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUVEditorU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSingletonU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_StaticFields, ___U3CSingletonU3Ek__BackingField_23)); }
	inline BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15 * get_U3CSingletonU3Ek__BackingField_23() const { return ___U3CSingletonU3Ek__BackingField_23; }
	inline BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15 ** get_address_of_U3CSingletonU3Ek__BackingField_23() { return &___U3CSingletonU3Ek__BackingField_23; }
	inline void set_U3CSingletonU3Ek__BackingField_23(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15 * value)
	{
		___U3CSingletonU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSingletonU3Ek__BackingField_23), (void*)value);
	}
};


// Program.UI.CenterPanel
struct  CenterPanel_t6E95A42E4283947A4575C58DC97DCDA346C8425B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Program.UI.CenterPanel::<IsOpen>k__BackingField
	bool ___U3CIsOpenU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CIsOpenU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CenterPanel_t6E95A42E4283947A4575C58DC97DCDA346C8425B, ___U3CIsOpenU3Ek__BackingField_4)); }
	inline bool get_U3CIsOpenU3Ek__BackingField_4() const { return ___U3CIsOpenU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsOpenU3Ek__BackingField_4() { return &___U3CIsOpenU3Ek__BackingField_4; }
	inline void set_U3CIsOpenU3Ek__BackingField_4(bool value)
	{
		___U3CIsOpenU3Ek__BackingField_4 = value;
	}
};


// Program.UI.ColorItem
struct  ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Button Program.UI.ColorItem::button
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___button_4;
	// UnityEngine.UI.Image Program.UI.ColorItem::image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___image_5;

public:
	inline static int32_t get_offset_of_button_4() { return static_cast<int32_t>(offsetof(ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650, ___button_4)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_button_4() const { return ___button_4; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_button_4() { return &___button_4; }
	inline void set_button_4(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___button_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___button_4), (void*)value);
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650, ___image_5)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_image_5() const { return ___image_5; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___image_5), (void*)value);
	}
};


// Program.UI.DefaultCamera
struct  DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Button Program.UI.DefaultCamera::button
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___button_5;

public:
	inline static int32_t get_offset_of_button_5() { return static_cast<int32_t>(offsetof(DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B, ___button_5)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_button_5() const { return ___button_5; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_button_5() { return &___button_5; }
	inline void set_button_5(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___button_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___button_5), (void*)value);
	}
};

struct DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_StaticFields
{
public:
	// System.Action Program.UI.DefaultCamera::ClickButton
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___ClickButton_4;

public:
	inline static int32_t get_offset_of_ClickButton_4() { return static_cast<int32_t>(offsetof(DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_StaticFields, ___ClickButton_4)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_ClickButton_4() const { return ___ClickButton_4; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_ClickButton_4() { return &___ClickButton_4; }
	inline void set_ClickButton_4(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___ClickButton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ClickButton_4), (void*)value);
	}
};


// Program.Menu.ListNews
struct  ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Program.Menu.UIMenu Program.Menu.ListNews::uIMenu
	UIMenu_t6A3241472F39D561390A7106B024030062D29EA6 * ___uIMenu_5;
	// Program.Menu.ItemNews Program.Menu.ListNews::prefabNews
	ItemNews_t56EF3225D84E7162C9E47B98493683E32B6C08F1 * ___prefabNews_6;
	// System.Collections.Generic.List`1<Program.Menu.ItemNews> Program.Menu.ListNews::items
	List_1_t1097B7FA8F0449D8B1D7C2B0B2997E5E22D23310 * ___items_7;

public:
	inline static int32_t get_offset_of_uIMenu_5() { return static_cast<int32_t>(offsetof(ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01, ___uIMenu_5)); }
	inline UIMenu_t6A3241472F39D561390A7106B024030062D29EA6 * get_uIMenu_5() const { return ___uIMenu_5; }
	inline UIMenu_t6A3241472F39D561390A7106B024030062D29EA6 ** get_address_of_uIMenu_5() { return &___uIMenu_5; }
	inline void set_uIMenu_5(UIMenu_t6A3241472F39D561390A7106B024030062D29EA6 * value)
	{
		___uIMenu_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uIMenu_5), (void*)value);
	}

	inline static int32_t get_offset_of_prefabNews_6() { return static_cast<int32_t>(offsetof(ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01, ___prefabNews_6)); }
	inline ItemNews_t56EF3225D84E7162C9E47B98493683E32B6C08F1 * get_prefabNews_6() const { return ___prefabNews_6; }
	inline ItemNews_t56EF3225D84E7162C9E47B98493683E32B6C08F1 ** get_address_of_prefabNews_6() { return &___prefabNews_6; }
	inline void set_prefabNews_6(ItemNews_t56EF3225D84E7162C9E47B98493683E32B6C08F1 * value)
	{
		___prefabNews_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefabNews_6), (void*)value);
	}

	inline static int32_t get_offset_of_items_7() { return static_cast<int32_t>(offsetof(ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01, ___items_7)); }
	inline List_1_t1097B7FA8F0449D8B1D7C2B0B2997E5E22D23310 * get_items_7() const { return ___items_7; }
	inline List_1_t1097B7FA8F0449D8B1D7C2B0B2997E5E22D23310 ** get_address_of_items_7() { return &___items_7; }
	inline void set_items_7(List_1_t1097B7FA8F0449D8B1D7C2B0B2997E5E22D23310 * value)
	{
		___items_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___items_7), (void*)value);
	}
};

struct ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_StaticFields
{
public:
	// Program.Menu.ListNews Program.Menu.ListNews::<Singleton>k__BackingField
	ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * ___U3CSingletonU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CSingletonU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_StaticFields, ___U3CSingletonU3Ek__BackingField_4)); }
	inline ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * get_U3CSingletonU3Ek__BackingField_4() const { return ___U3CSingletonU3Ek__BackingField_4; }
	inline ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 ** get_address_of_U3CSingletonU3Ek__BackingField_4() { return &___U3CSingletonU3Ek__BackingField_4; }
	inline void set_U3CSingletonU3Ek__BackingField_4(ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * value)
	{
		___U3CSingletonU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSingletonU3Ek__BackingField_4), (void*)value);
	}
};


// Program.Option.OptionObject
struct  OptionObject_t108F4312E931F36F66EF6A91E95988338600EC8F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Program.UI.PalitraPanel
struct  PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Program.UI.ColorPickerPanel Program.UI.PalitraPanel::colorPickerPanel
	ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7 * ___colorPickerPanel_4;
	// UnityEngine.UI.Button Program.UI.PalitraPanel::openPicker
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___openPicker_5;
	// Program.UI.ProgramUI Program.UI.PalitraPanel::programUI
	ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7 * ___programUI_6;
	// PalitraDataBase Program.UI.PalitraPanel::palitraDataBase
	PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD * ___palitraDataBase_7;
	// UnityEngine.UI.Button Program.UI.PalitraPanel::openPalitras
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___openPalitras_8;
	// Program.UI.MenuPaltira Program.UI.PalitraPanel::menuPaltira
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * ___menuPaltira_9;
	// UnityEngine.UI.Button Program.UI.PalitraPanel::paint
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___paint_10;
	// UnityEngine.UI.Image Program.UI.PalitraPanel::paintOn
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___paintOn_11;
	// UnityEngine.UI.Image Program.UI.PalitraPanel::paintOff
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___paintOff_12;
	// UnityEngine.UI.Button Program.UI.PalitraPanel::takeColor
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___takeColor_13;
	// UnityEngine.UI.Image Program.UI.PalitraPanel::takeColorOn
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___takeColorOn_14;
	// UnityEngine.UI.Image Program.UI.PalitraPanel::takeColorOff
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___takeColorOff_15;
	// UnityEngine.UI.Button Program.UI.PalitraPanel::fillColor
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___fillColor_16;
	// UnityEngine.UI.Image Program.UI.PalitraPanel::fillColorOn
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___fillColorOn_17;
	// UnityEngine.UI.Image Program.UI.PalitraPanel::fillColorOff
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___fillColorOff_18;
	// UnityEngine.UI.Button Program.UI.PalitraPanel::erase
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___erase_19;
	// UnityEngine.UI.Image Program.UI.PalitraPanel::eraseOn
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___eraseOn_20;
	// UnityEngine.UI.Image Program.UI.PalitraPanel::eraseOff
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___eraseOff_21;
	// Program.UI.ColorItem[] Program.UI.PalitraPanel::colorItems
	ColorItemU5BU5D_t21E2528356087A131BD727F2826714C4CDF04E3C* ___colorItems_22;
	// UnityEngine.Transform Program.UI.PalitraPanel::pointTake
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___pointTake_23;
	// UnityEngine.UI.Button Program.UI.PalitraPanel::backHistory
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___backHistory_24;
	// UnityEngine.UI.Button Program.UI.PalitraPanel::nextHistory
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___nextHistory_25;

public:
	inline static int32_t get_offset_of_colorPickerPanel_4() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___colorPickerPanel_4)); }
	inline ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7 * get_colorPickerPanel_4() const { return ___colorPickerPanel_4; }
	inline ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7 ** get_address_of_colorPickerPanel_4() { return &___colorPickerPanel_4; }
	inline void set_colorPickerPanel_4(ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7 * value)
	{
		___colorPickerPanel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorPickerPanel_4), (void*)value);
	}

	inline static int32_t get_offset_of_openPicker_5() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___openPicker_5)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_openPicker_5() const { return ___openPicker_5; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_openPicker_5() { return &___openPicker_5; }
	inline void set_openPicker_5(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___openPicker_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___openPicker_5), (void*)value);
	}

	inline static int32_t get_offset_of_programUI_6() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___programUI_6)); }
	inline ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7 * get_programUI_6() const { return ___programUI_6; }
	inline ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7 ** get_address_of_programUI_6() { return &___programUI_6; }
	inline void set_programUI_6(ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7 * value)
	{
		___programUI_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___programUI_6), (void*)value);
	}

	inline static int32_t get_offset_of_palitraDataBase_7() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___palitraDataBase_7)); }
	inline PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD * get_palitraDataBase_7() const { return ___palitraDataBase_7; }
	inline PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD ** get_address_of_palitraDataBase_7() { return &___palitraDataBase_7; }
	inline void set_palitraDataBase_7(PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD * value)
	{
		___palitraDataBase_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___palitraDataBase_7), (void*)value);
	}

	inline static int32_t get_offset_of_openPalitras_8() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___openPalitras_8)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_openPalitras_8() const { return ___openPalitras_8; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_openPalitras_8() { return &___openPalitras_8; }
	inline void set_openPalitras_8(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___openPalitras_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___openPalitras_8), (void*)value);
	}

	inline static int32_t get_offset_of_menuPaltira_9() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___menuPaltira_9)); }
	inline MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * get_menuPaltira_9() const { return ___menuPaltira_9; }
	inline MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A ** get_address_of_menuPaltira_9() { return &___menuPaltira_9; }
	inline void set_menuPaltira_9(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * value)
	{
		___menuPaltira_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuPaltira_9), (void*)value);
	}

	inline static int32_t get_offset_of_paint_10() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___paint_10)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_paint_10() const { return ___paint_10; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_paint_10() { return &___paint_10; }
	inline void set_paint_10(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___paint_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___paint_10), (void*)value);
	}

	inline static int32_t get_offset_of_paintOn_11() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___paintOn_11)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_paintOn_11() const { return ___paintOn_11; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_paintOn_11() { return &___paintOn_11; }
	inline void set_paintOn_11(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___paintOn_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___paintOn_11), (void*)value);
	}

	inline static int32_t get_offset_of_paintOff_12() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___paintOff_12)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_paintOff_12() const { return ___paintOff_12; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_paintOff_12() { return &___paintOff_12; }
	inline void set_paintOff_12(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___paintOff_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___paintOff_12), (void*)value);
	}

	inline static int32_t get_offset_of_takeColor_13() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___takeColor_13)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_takeColor_13() const { return ___takeColor_13; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_takeColor_13() { return &___takeColor_13; }
	inline void set_takeColor_13(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___takeColor_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___takeColor_13), (void*)value);
	}

	inline static int32_t get_offset_of_takeColorOn_14() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___takeColorOn_14)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_takeColorOn_14() const { return ___takeColorOn_14; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_takeColorOn_14() { return &___takeColorOn_14; }
	inline void set_takeColorOn_14(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___takeColorOn_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___takeColorOn_14), (void*)value);
	}

	inline static int32_t get_offset_of_takeColorOff_15() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___takeColorOff_15)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_takeColorOff_15() const { return ___takeColorOff_15; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_takeColorOff_15() { return &___takeColorOff_15; }
	inline void set_takeColorOff_15(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___takeColorOff_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___takeColorOff_15), (void*)value);
	}

	inline static int32_t get_offset_of_fillColor_16() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___fillColor_16)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_fillColor_16() const { return ___fillColor_16; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_fillColor_16() { return &___fillColor_16; }
	inline void set_fillColor_16(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___fillColor_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fillColor_16), (void*)value);
	}

	inline static int32_t get_offset_of_fillColorOn_17() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___fillColorOn_17)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_fillColorOn_17() const { return ___fillColorOn_17; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_fillColorOn_17() { return &___fillColorOn_17; }
	inline void set_fillColorOn_17(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___fillColorOn_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fillColorOn_17), (void*)value);
	}

	inline static int32_t get_offset_of_fillColorOff_18() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___fillColorOff_18)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_fillColorOff_18() const { return ___fillColorOff_18; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_fillColorOff_18() { return &___fillColorOff_18; }
	inline void set_fillColorOff_18(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___fillColorOff_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fillColorOff_18), (void*)value);
	}

	inline static int32_t get_offset_of_erase_19() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___erase_19)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_erase_19() const { return ___erase_19; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_erase_19() { return &___erase_19; }
	inline void set_erase_19(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___erase_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___erase_19), (void*)value);
	}

	inline static int32_t get_offset_of_eraseOn_20() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___eraseOn_20)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_eraseOn_20() const { return ___eraseOn_20; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_eraseOn_20() { return &___eraseOn_20; }
	inline void set_eraseOn_20(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___eraseOn_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eraseOn_20), (void*)value);
	}

	inline static int32_t get_offset_of_eraseOff_21() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___eraseOff_21)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_eraseOff_21() const { return ___eraseOff_21; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_eraseOff_21() { return &___eraseOff_21; }
	inline void set_eraseOff_21(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___eraseOff_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eraseOff_21), (void*)value);
	}

	inline static int32_t get_offset_of_colorItems_22() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___colorItems_22)); }
	inline ColorItemU5BU5D_t21E2528356087A131BD727F2826714C4CDF04E3C* get_colorItems_22() const { return ___colorItems_22; }
	inline ColorItemU5BU5D_t21E2528356087A131BD727F2826714C4CDF04E3C** get_address_of_colorItems_22() { return &___colorItems_22; }
	inline void set_colorItems_22(ColorItemU5BU5D_t21E2528356087A131BD727F2826714C4CDF04E3C* value)
	{
		___colorItems_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorItems_22), (void*)value);
	}

	inline static int32_t get_offset_of_pointTake_23() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___pointTake_23)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_pointTake_23() const { return ___pointTake_23; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_pointTake_23() { return &___pointTake_23; }
	inline void set_pointTake_23(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___pointTake_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointTake_23), (void*)value);
	}

	inline static int32_t get_offset_of_backHistory_24() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___backHistory_24)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_backHistory_24() const { return ___backHistory_24; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_backHistory_24() { return &___backHistory_24; }
	inline void set_backHistory_24(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___backHistory_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___backHistory_24), (void*)value);
	}

	inline static int32_t get_offset_of_nextHistory_25() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD, ___nextHistory_25)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_nextHistory_25() const { return ___nextHistory_25; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_nextHistory_25() { return &___nextHistory_25; }
	inline void set_nextHistory_25(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___nextHistory_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nextHistory_25), (void*)value);
	}
};

struct PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_StaticFields
{
public:
	// Program.UI.Instruments Program.UI.PalitraPanel::<Instrument>k__BackingField
	Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * ___U3CInstrumentU3Ek__BackingField_26;
	// Program.UI.ColorPalitra Program.UI.PalitraPanel::<ColorList>k__BackingField
	ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3 * ___U3CColorListU3Ek__BackingField_27;
	// Program.UI.HistoryButton Program.UI.PalitraPanel::<History>k__BackingField
	HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB * ___U3CHistoryU3Ek__BackingField_28;
	// Program.UI.PalitraPanel Program.UI.PalitraPanel::<Singleton>k__BackingField
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD * ___U3CSingletonU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_U3CInstrumentU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_StaticFields, ___U3CInstrumentU3Ek__BackingField_26)); }
	inline Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * get_U3CInstrumentU3Ek__BackingField_26() const { return ___U3CInstrumentU3Ek__BackingField_26; }
	inline Instruments_tF75DA728107310E221C75AD3538952EAEE98442E ** get_address_of_U3CInstrumentU3Ek__BackingField_26() { return &___U3CInstrumentU3Ek__BackingField_26; }
	inline void set_U3CInstrumentU3Ek__BackingField_26(Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * value)
	{
		___U3CInstrumentU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstrumentU3Ek__BackingField_26), (void*)value);
	}

	inline static int32_t get_offset_of_U3CColorListU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_StaticFields, ___U3CColorListU3Ek__BackingField_27)); }
	inline ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3 * get_U3CColorListU3Ek__BackingField_27() const { return ___U3CColorListU3Ek__BackingField_27; }
	inline ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3 ** get_address_of_U3CColorListU3Ek__BackingField_27() { return &___U3CColorListU3Ek__BackingField_27; }
	inline void set_U3CColorListU3Ek__BackingField_27(ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3 * value)
	{
		___U3CColorListU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CColorListU3Ek__BackingField_27), (void*)value);
	}

	inline static int32_t get_offset_of_U3CHistoryU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_StaticFields, ___U3CHistoryU3Ek__BackingField_28)); }
	inline HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB * get_U3CHistoryU3Ek__BackingField_28() const { return ___U3CHistoryU3Ek__BackingField_28; }
	inline HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB ** get_address_of_U3CHistoryU3Ek__BackingField_28() { return &___U3CHistoryU3Ek__BackingField_28; }
	inline void set_U3CHistoryU3Ek__BackingField_28(HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB * value)
	{
		___U3CHistoryU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CHistoryU3Ek__BackingField_28), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSingletonU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_StaticFields, ___U3CSingletonU3Ek__BackingField_29)); }
	inline PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD * get_U3CSingletonU3Ek__BackingField_29() const { return ___U3CSingletonU3Ek__BackingField_29; }
	inline PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD ** get_address_of_U3CSingletonU3Ek__BackingField_29() { return &___U3CSingletonU3Ek__BackingField_29; }
	inline void set_U3CSingletonU3Ek__BackingField_29(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD * value)
	{
		___U3CSingletonU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSingletonU3Ek__BackingField_29), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.CanvasScaler
struct  CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::m_UiScaleMode
	int32_t ___m_UiScaleMode_4;
	// System.Single UnityEngine.UI.CanvasScaler::m_ReferencePixelsPerUnit
	float ___m_ReferencePixelsPerUnit_5;
	// System.Single UnityEngine.UI.CanvasScaler::m_ScaleFactor
	float ___m_ScaleFactor_6;
	// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::m_ReferenceResolution
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_ReferenceResolution_7;
	// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::m_ScreenMatchMode
	int32_t ___m_ScreenMatchMode_8;
	// System.Single UnityEngine.UI.CanvasScaler::m_MatchWidthOrHeight
	float ___m_MatchWidthOrHeight_9;
	// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::m_PhysicalUnit
	int32_t ___m_PhysicalUnit_11;
	// System.Single UnityEngine.UI.CanvasScaler::m_FallbackScreenDPI
	float ___m_FallbackScreenDPI_12;
	// System.Single UnityEngine.UI.CanvasScaler::m_DefaultSpriteDPI
	float ___m_DefaultSpriteDPI_13;
	// System.Single UnityEngine.UI.CanvasScaler::m_DynamicPixelsPerUnit
	float ___m_DynamicPixelsPerUnit_14;
	// UnityEngine.Canvas UnityEngine.UI.CanvasScaler::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_15;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevScaleFactor
	float ___m_PrevScaleFactor_16;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevReferencePixelsPerUnit
	float ___m_PrevReferencePixelsPerUnit_17;
	// System.Boolean UnityEngine.UI.CanvasScaler::m_PresetInfoIsWorld
	bool ___m_PresetInfoIsWorld_18;

public:
	inline static int32_t get_offset_of_m_UiScaleMode_4() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_UiScaleMode_4)); }
	inline int32_t get_m_UiScaleMode_4() const { return ___m_UiScaleMode_4; }
	inline int32_t* get_address_of_m_UiScaleMode_4() { return &___m_UiScaleMode_4; }
	inline void set_m_UiScaleMode_4(int32_t value)
	{
		___m_UiScaleMode_4 = value;
	}

	inline static int32_t get_offset_of_m_ReferencePixelsPerUnit_5() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_ReferencePixelsPerUnit_5)); }
	inline float get_m_ReferencePixelsPerUnit_5() const { return ___m_ReferencePixelsPerUnit_5; }
	inline float* get_address_of_m_ReferencePixelsPerUnit_5() { return &___m_ReferencePixelsPerUnit_5; }
	inline void set_m_ReferencePixelsPerUnit_5(float value)
	{
		___m_ReferencePixelsPerUnit_5 = value;
	}

	inline static int32_t get_offset_of_m_ScaleFactor_6() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_ScaleFactor_6)); }
	inline float get_m_ScaleFactor_6() const { return ___m_ScaleFactor_6; }
	inline float* get_address_of_m_ScaleFactor_6() { return &___m_ScaleFactor_6; }
	inline void set_m_ScaleFactor_6(float value)
	{
		___m_ScaleFactor_6 = value;
	}

	inline static int32_t get_offset_of_m_ReferenceResolution_7() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_ReferenceResolution_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_ReferenceResolution_7() const { return ___m_ReferenceResolution_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_ReferenceResolution_7() { return &___m_ReferenceResolution_7; }
	inline void set_m_ReferenceResolution_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_ReferenceResolution_7 = value;
	}

	inline static int32_t get_offset_of_m_ScreenMatchMode_8() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_ScreenMatchMode_8)); }
	inline int32_t get_m_ScreenMatchMode_8() const { return ___m_ScreenMatchMode_8; }
	inline int32_t* get_address_of_m_ScreenMatchMode_8() { return &___m_ScreenMatchMode_8; }
	inline void set_m_ScreenMatchMode_8(int32_t value)
	{
		___m_ScreenMatchMode_8 = value;
	}

	inline static int32_t get_offset_of_m_MatchWidthOrHeight_9() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_MatchWidthOrHeight_9)); }
	inline float get_m_MatchWidthOrHeight_9() const { return ___m_MatchWidthOrHeight_9; }
	inline float* get_address_of_m_MatchWidthOrHeight_9() { return &___m_MatchWidthOrHeight_9; }
	inline void set_m_MatchWidthOrHeight_9(float value)
	{
		___m_MatchWidthOrHeight_9 = value;
	}

	inline static int32_t get_offset_of_m_PhysicalUnit_11() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_PhysicalUnit_11)); }
	inline int32_t get_m_PhysicalUnit_11() const { return ___m_PhysicalUnit_11; }
	inline int32_t* get_address_of_m_PhysicalUnit_11() { return &___m_PhysicalUnit_11; }
	inline void set_m_PhysicalUnit_11(int32_t value)
	{
		___m_PhysicalUnit_11 = value;
	}

	inline static int32_t get_offset_of_m_FallbackScreenDPI_12() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_FallbackScreenDPI_12)); }
	inline float get_m_FallbackScreenDPI_12() const { return ___m_FallbackScreenDPI_12; }
	inline float* get_address_of_m_FallbackScreenDPI_12() { return &___m_FallbackScreenDPI_12; }
	inline void set_m_FallbackScreenDPI_12(float value)
	{
		___m_FallbackScreenDPI_12 = value;
	}

	inline static int32_t get_offset_of_m_DefaultSpriteDPI_13() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_DefaultSpriteDPI_13)); }
	inline float get_m_DefaultSpriteDPI_13() const { return ___m_DefaultSpriteDPI_13; }
	inline float* get_address_of_m_DefaultSpriteDPI_13() { return &___m_DefaultSpriteDPI_13; }
	inline void set_m_DefaultSpriteDPI_13(float value)
	{
		___m_DefaultSpriteDPI_13 = value;
	}

	inline static int32_t get_offset_of_m_DynamicPixelsPerUnit_14() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_DynamicPixelsPerUnit_14)); }
	inline float get_m_DynamicPixelsPerUnit_14() const { return ___m_DynamicPixelsPerUnit_14; }
	inline float* get_address_of_m_DynamicPixelsPerUnit_14() { return &___m_DynamicPixelsPerUnit_14; }
	inline void set_m_DynamicPixelsPerUnit_14(float value)
	{
		___m_DynamicPixelsPerUnit_14 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_15() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_Canvas_15)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_15() const { return ___m_Canvas_15; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_15() { return &___m_Canvas_15; }
	inline void set_m_Canvas_15(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_PrevScaleFactor_16() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_PrevScaleFactor_16)); }
	inline float get_m_PrevScaleFactor_16() const { return ___m_PrevScaleFactor_16; }
	inline float* get_address_of_m_PrevScaleFactor_16() { return &___m_PrevScaleFactor_16; }
	inline void set_m_PrevScaleFactor_16(float value)
	{
		___m_PrevScaleFactor_16 = value;
	}

	inline static int32_t get_offset_of_m_PrevReferencePixelsPerUnit_17() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_PrevReferencePixelsPerUnit_17)); }
	inline float get_m_PrevReferencePixelsPerUnit_17() const { return ___m_PrevReferencePixelsPerUnit_17; }
	inline float* get_address_of_m_PrevReferencePixelsPerUnit_17() { return &___m_PrevReferencePixelsPerUnit_17; }
	inline void set_m_PrevReferencePixelsPerUnit_17(float value)
	{
		___m_PrevReferencePixelsPerUnit_17 = value;
	}

	inline static int32_t get_offset_of_m_PresetInfoIsWorld_18() { return static_cast<int32_t>(offsetof(CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1, ___m_PresetInfoIsWorld_18)); }
	inline bool get_m_PresetInfoIsWorld_18() const { return ___m_PresetInfoIsWorld_18; }
	inline bool* get_address_of_m_PresetInfoIsWorld_18() { return &___m_PresetInfoIsWorld_18; }
	inline void set_m_PresetInfoIsWorld_18(bool value)
	{
		___m_PresetInfoIsWorld_18 = value;
	}
};


// Program.UI.LoadLibraryPanel
struct  LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394  : public CenterPanel_t6E95A42E4283947A4575C58DC97DCDA346C8425B
{
public:
	// UnityEngine.GameObject Program.UI.LoadLibraryPanel::panel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___panel_5;
	// Program.UI.ProgramUI Program.UI.LoadLibraryPanel::programUI
	ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7 * ___programUI_6;
	// ItemFile Program.UI.LoadLibraryPanel::takeFile
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8 * ___takeFile_7;
	// UnityEngine.UI.InputField Program.UI.LoadLibraryPanel::inputField
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___inputField_8;
	// UnityEngine.UI.Button Program.UI.LoadLibraryPanel::back
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___back_9;
	// UnityEngine.UI.Button Program.UI.LoadLibraryPanel::load
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___load_10;
	// UnityEngine.Transform Program.UI.LoadLibraryPanel::content
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___content_11;
	// ItemFile Program.UI.LoadLibraryPanel::prefabItemFile
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8 * ___prefabItemFile_12;
	// System.Collections.Generic.List`1<ItemFile> Program.UI.LoadLibraryPanel::itemFiles
	List_1_t2AF58D691E5BC2E9210D1E7E43FBAF6451097E78 * ___itemFiles_13;
	// TextureFile[] Program.UI.LoadLibraryPanel::serverFiles
	TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* ___serverFiles_14;

public:
	inline static int32_t get_offset_of_panel_5() { return static_cast<int32_t>(offsetof(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394, ___panel_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_panel_5() const { return ___panel_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_panel_5() { return &___panel_5; }
	inline void set_panel_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___panel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___panel_5), (void*)value);
	}

	inline static int32_t get_offset_of_programUI_6() { return static_cast<int32_t>(offsetof(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394, ___programUI_6)); }
	inline ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7 * get_programUI_6() const { return ___programUI_6; }
	inline ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7 ** get_address_of_programUI_6() { return &___programUI_6; }
	inline void set_programUI_6(ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7 * value)
	{
		___programUI_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___programUI_6), (void*)value);
	}

	inline static int32_t get_offset_of_takeFile_7() { return static_cast<int32_t>(offsetof(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394, ___takeFile_7)); }
	inline ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8 * get_takeFile_7() const { return ___takeFile_7; }
	inline ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8 ** get_address_of_takeFile_7() { return &___takeFile_7; }
	inline void set_takeFile_7(ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8 * value)
	{
		___takeFile_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___takeFile_7), (void*)value);
	}

	inline static int32_t get_offset_of_inputField_8() { return static_cast<int32_t>(offsetof(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394, ___inputField_8)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_inputField_8() const { return ___inputField_8; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_inputField_8() { return &___inputField_8; }
	inline void set_inputField_8(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___inputField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputField_8), (void*)value);
	}

	inline static int32_t get_offset_of_back_9() { return static_cast<int32_t>(offsetof(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394, ___back_9)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_back_9() const { return ___back_9; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_back_9() { return &___back_9; }
	inline void set_back_9(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___back_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___back_9), (void*)value);
	}

	inline static int32_t get_offset_of_load_10() { return static_cast<int32_t>(offsetof(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394, ___load_10)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_load_10() const { return ___load_10; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_load_10() { return &___load_10; }
	inline void set_load_10(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___load_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___load_10), (void*)value);
	}

	inline static int32_t get_offset_of_content_11() { return static_cast<int32_t>(offsetof(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394, ___content_11)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_content_11() const { return ___content_11; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_content_11() { return &___content_11; }
	inline void set_content_11(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___content_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___content_11), (void*)value);
	}

	inline static int32_t get_offset_of_prefabItemFile_12() { return static_cast<int32_t>(offsetof(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394, ___prefabItemFile_12)); }
	inline ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8 * get_prefabItemFile_12() const { return ___prefabItemFile_12; }
	inline ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8 ** get_address_of_prefabItemFile_12() { return &___prefabItemFile_12; }
	inline void set_prefabItemFile_12(ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8 * value)
	{
		___prefabItemFile_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefabItemFile_12), (void*)value);
	}

	inline static int32_t get_offset_of_itemFiles_13() { return static_cast<int32_t>(offsetof(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394, ___itemFiles_13)); }
	inline List_1_t2AF58D691E5BC2E9210D1E7E43FBAF6451097E78 * get_itemFiles_13() const { return ___itemFiles_13; }
	inline List_1_t2AF58D691E5BC2E9210D1E7E43FBAF6451097E78 ** get_address_of_itemFiles_13() { return &___itemFiles_13; }
	inline void set_itemFiles_13(List_1_t2AF58D691E5BC2E9210D1E7E43FBAF6451097E78 * value)
	{
		___itemFiles_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemFiles_13), (void*)value);
	}

	inline static int32_t get_offset_of_serverFiles_14() { return static_cast<int32_t>(offsetof(LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394, ___serverFiles_14)); }
	inline TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* get_serverFiles_14() const { return ___serverFiles_14; }
	inline TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B** get_address_of_serverFiles_14() { return &___serverFiles_14; }
	inline void set_serverFiles_14(TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* value)
	{
		___serverFiles_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serverFiles_14), (void*)value);
	}
};


// Program.UI.MenuPaltira
struct  MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A  : public CenterPanel_t6E95A42E4283947A4575C58DC97DCDA346C8425B
{
public:
	// UnityEngine.UI.Button Program.UI.MenuPaltira::saveFile
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___saveFile_5;
	// UnityEngine.UI.Button Program.UI.MenuPaltira::deletFile
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___deletFile_6;
	// UnityEngine.GameObject Program.UI.MenuPaltira::panel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___panel_7;
	// UnityEngine.UI.Button Program.UI.MenuPaltira::breakBut
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___breakBut_8;
	// Program.UI.PalitraItem Program.UI.MenuPaltira::prefabItem
	PalitraItem_tB7364AC466C6FA3A859EB47095C3B8A563A0AD4A * ___prefabItem_9;
	// UnityEngine.Transform Program.UI.MenuPaltira::content
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___content_10;
	// PalitraDataBase Program.UI.MenuPaltira::palitraData
	PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD * ___palitraData_11;
	// System.Collections.Generic.List`1<Program.UI.PalitraItem> Program.UI.MenuPaltira::objs
	List_1_t0B6165ABABF6AD736F24984ADFC2EC3B92936B27 * ___objs_12;
	// Program.UI.SetPalitra Program.UI.MenuPaltira::TakePalitra
	SetPalitra_tF9CBA6472FC50BA66C7FE5BB350A36CE8E184A62 * ___TakePalitra_15;

public:
	inline static int32_t get_offset_of_saveFile_5() { return static_cast<int32_t>(offsetof(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A, ___saveFile_5)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_saveFile_5() const { return ___saveFile_5; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_saveFile_5() { return &___saveFile_5; }
	inline void set_saveFile_5(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___saveFile_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___saveFile_5), (void*)value);
	}

	inline static int32_t get_offset_of_deletFile_6() { return static_cast<int32_t>(offsetof(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A, ___deletFile_6)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_deletFile_6() const { return ___deletFile_6; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_deletFile_6() { return &___deletFile_6; }
	inline void set_deletFile_6(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___deletFile_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deletFile_6), (void*)value);
	}

	inline static int32_t get_offset_of_panel_7() { return static_cast<int32_t>(offsetof(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A, ___panel_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_panel_7() const { return ___panel_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_panel_7() { return &___panel_7; }
	inline void set_panel_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___panel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___panel_7), (void*)value);
	}

	inline static int32_t get_offset_of_breakBut_8() { return static_cast<int32_t>(offsetof(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A, ___breakBut_8)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_breakBut_8() const { return ___breakBut_8; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_breakBut_8() { return &___breakBut_8; }
	inline void set_breakBut_8(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___breakBut_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___breakBut_8), (void*)value);
	}

	inline static int32_t get_offset_of_prefabItem_9() { return static_cast<int32_t>(offsetof(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A, ___prefabItem_9)); }
	inline PalitraItem_tB7364AC466C6FA3A859EB47095C3B8A563A0AD4A * get_prefabItem_9() const { return ___prefabItem_9; }
	inline PalitraItem_tB7364AC466C6FA3A859EB47095C3B8A563A0AD4A ** get_address_of_prefabItem_9() { return &___prefabItem_9; }
	inline void set_prefabItem_9(PalitraItem_tB7364AC466C6FA3A859EB47095C3B8A563A0AD4A * value)
	{
		___prefabItem_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefabItem_9), (void*)value);
	}

	inline static int32_t get_offset_of_content_10() { return static_cast<int32_t>(offsetof(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A, ___content_10)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_content_10() const { return ___content_10; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_content_10() { return &___content_10; }
	inline void set_content_10(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___content_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___content_10), (void*)value);
	}

	inline static int32_t get_offset_of_palitraData_11() { return static_cast<int32_t>(offsetof(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A, ___palitraData_11)); }
	inline PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD * get_palitraData_11() const { return ___palitraData_11; }
	inline PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD ** get_address_of_palitraData_11() { return &___palitraData_11; }
	inline void set_palitraData_11(PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD * value)
	{
		___palitraData_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___palitraData_11), (void*)value);
	}

	inline static int32_t get_offset_of_objs_12() { return static_cast<int32_t>(offsetof(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A, ___objs_12)); }
	inline List_1_t0B6165ABABF6AD736F24984ADFC2EC3B92936B27 * get_objs_12() const { return ___objs_12; }
	inline List_1_t0B6165ABABF6AD736F24984ADFC2EC3B92936B27 ** get_address_of_objs_12() { return &___objs_12; }
	inline void set_objs_12(List_1_t0B6165ABABF6AD736F24984ADFC2EC3B92936B27 * value)
	{
		___objs_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objs_12), (void*)value);
	}

	inline static int32_t get_offset_of_TakePalitra_15() { return static_cast<int32_t>(offsetof(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A, ___TakePalitra_15)); }
	inline SetPalitra_tF9CBA6472FC50BA66C7FE5BB350A36CE8E184A62 * get_TakePalitra_15() const { return ___TakePalitra_15; }
	inline SetPalitra_tF9CBA6472FC50BA66C7FE5BB350A36CE8E184A62 ** get_address_of_TakePalitra_15() { return &___TakePalitra_15; }
	inline void set_TakePalitra_15(SetPalitra_tF9CBA6472FC50BA66C7FE5BB350A36CE8E184A62 * value)
	{
		___TakePalitra_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TakePalitra_15), (void*)value);
	}
};

struct MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_StaticFields
{
public:
	// Program.UI.MenuPaltira Program.UI.MenuPaltira::<Singleton>k__BackingField
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * ___U3CSingletonU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CSingletonU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_StaticFields, ___U3CSingletonU3Ek__BackingField_16)); }
	inline MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * get_U3CSingletonU3Ek__BackingField_16() const { return ___U3CSingletonU3Ek__BackingField_16; }
	inline MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A ** get_address_of_U3CSingletonU3Ek__BackingField_16() { return &___U3CSingletonU3Ek__BackingField_16; }
	inline void set_U3CSingletonU3Ek__BackingField_16(MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * value)
	{
		___U3CSingletonU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSingletonU3Ek__BackingField_16), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Program.Editor.GetPixels/Triangle[]
struct TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * m_Items[1];

public:
	inline Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// FilerServer.News[]
struct NewsU5BU5D_t7CD4BFBC754A2A366E68BF321543B7DEF517F927  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) News_tDEA932114B87C23732FC46451449BC249F3EAE2C * m_Items[1];

public:
	inline News_tDEA932114B87C23732FC46451449BC249F3EAE2C * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline News_tDEA932114B87C23732FC46451449BC249F3EAE2C ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, News_tDEA932114B87C23732FC46451449BC249F3EAE2C * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline News_tDEA932114B87C23732FC46451449BC249F3EAE2C * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline News_tDEA932114B87C23732FC46451449BC249F3EAE2C ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, News_tDEA932114B87C23732FC46451449BC249F3EAE2C * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// FilerServer.Texture[]
struct TextureU5BU5D_tFC397DCC79C0EAD5839F838C9E694ACB8C7C331B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D * m_Items[1];

public:
	inline Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Texture_tAEF35C26141E69B4760C3229A41DDF55E801429D * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// TextureFile[]
struct TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F * m_Items[1];

public:
	inline TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TextureFile_t631D031818CB18896B08A3E68A7B8EDAB226360F * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::Start<FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8>(!!0&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncVoidMethodBuilder_Start_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_mB911DC1C3B7DF7B772D17AC5FBAB824F5FF66FF6_gshared (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 * ___stateMachine0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_AddRange_m6465DEF706EB529B4227F2AF79338419D517EDF9_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* List_1_ToArray_mA737986DE6389E9DD8FA8E3D4E222DE4DA34958D_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m66148860899ECCAE9B323372032BFC1C255393D2_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveRange(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveRange_m0BBC3852B9B0719DDA7E6AFEF3C3E3CD165DF5AA_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, int32_t ___count1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.TaskAwaiter,FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8>(!!0&,!!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_m77090A1636BA1001FD6018EED4EEC9FEAE973E0C_gshared (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * ___awaiter0, U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 * ___stateMachine1, const RuntimeMethod* method);

// System.Void FilerServer.Base.BaseConnect/BaseProcces::End()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_End_m1CAF78A6E42FCE331CA98540417F50F507CD356B (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method);
// System.Void System.Action::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, const RuntimeMethod* method);
// System.Void FilerServer.Base.BaseConnect/BaseProcces::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_Update_mBC2C04C98E90C05C6C9347EDC4F03F9C64C1E5BD (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method);
// System.Runtime.CompilerServices.AsyncVoidMethodBuilder System.Runtime.CompilerServices.AsyncVoidMethodBuilder::Create()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6  AsyncVoidMethodBuilder_Create_m878314259623CC47A2EBAEEF2F8E8D6B61560FA5 (const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::Start<FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8>(!!0&)
inline void AsyncVoidMethodBuilder_Start_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_mB911DC1C3B7DF7B772D17AC5FBAB824F5FF66FF6 (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 * ___stateMachine0, const RuntimeMethod* method)
{
	((  void (*) (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *, U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 *, const RuntimeMethod*))AsyncVoidMethodBuilder_Start_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_mB911DC1C3B7DF7B772D17AC5FBAB824F5FF66FF6_gshared)(__this, ___stateMachine0, method);
}
// System.Void System.Net.Sockets.Socket::.ctor(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Socket__ctor_m5A4B335AEC1450ABE31CF1151F3F5A93D9D0280C (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, int32_t ___addressFamily0, int32_t ___socketType1, int32_t ___protocolType2, const RuntimeMethod* method);
// System.Net.IPAddress System.Net.IPAddress::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * IPAddress_Parse_m49C413225AC75DA34D1663559818861CA34C3CB0 (String_t* ___ipString0, const RuntimeMethod* method);
// System.Void System.Net.IPEndPoint::.ctor(System.Net.IPAddress,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IPEndPoint__ctor_m22783A215BA0B38674F6A6CB6803804268561321 (IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * __this, IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * ___address0, int32_t ___port1, const RuntimeMethod* method);
// System.Void System.Net.Sockets.Socket::Connect(System.Net.EndPoint)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Socket_Connect_mA047E173F3E1082B396D018585E0B0B2D6E8E5A8 (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * ___remoteEP0, const RuntimeMethod* method);
// System.Void FilerServer.Base.BaseConnect::RemoveProcces()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseConnect_RemoveProcces_m3D2E114B21E59102C6E9B7D3E832F976BAC9FFD7 (const RuntimeMethod* method);
// System.Boolean System.Net.Sockets.Socket::get_Connected()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Socket_get_Connected_m6E8C88AC69580EF7782514CD48754D4D180D9CE0_inline (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, const RuntimeMethod* method);
// System.Void System.Net.Sockets.Socket::Close()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Socket_Close_m24AB78F5DAC1C39BB7FFB30A9620B2B07E01DEEB (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void FilerServer.Base.BaseConnect/BaseProcces::set_isEnd(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_set_isEnd_mB7801225FC730DD85B549BA70DDA25A1CECBAF04 (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Program.UI.BodyPanel/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m219117CD33D8037CB10012C35FCCFAF38B21B619 (U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C * __this, const RuntimeMethod* method);
// Program.UI.UVPanel Program.UI.BodyPanel::get_UVEditor()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9 * BodyPanel_get_UVEditor_mB67B5BA8BC6C9B3D828712FFE97D6D5E3209E8CE_inline (const RuntimeMethod* method);
// System.Void Program.UI.UVPanel::Open()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UVPanel_Open_mA452877254FF476D6AA0734706B8E329ED6A8665 (UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9 * __this, const RuntimeMethod* method);
// Program.UI.Render Program.UI.BodyPanel::get_StartRender()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08 * BodyPanel_get_StartRender_mAAB7A1D9289510C59929934F34C22C74FD0BE8AF_inline (const RuntimeMethod* method);
// System.Void Program.UI.Render::StartRender()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Render_StartRender_mD3B88E93D93D28AC89F251010D7B5D6ED1A77E91 (Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08 * __this, const RuntimeMethod* method);
// Program.UI.DownLayer Program.UI.BodyPanel::get_LayerDown()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * BodyPanel_get_LayerDown_m29403D6F813A24BCB8A9239FDD6573D75D3C9D7D_inline (const RuntimeMethod* method);
// System.Boolean Program.UI.DownLayer::get_Enable()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool DownLayer_get_Enable_m49AAD772F67A74CE9EA1E2EF80369E7963C9F42A_inline (DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * __this, const RuntimeMethod* method);
// System.Void Program.UI.DownLayer::set_Enable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DownLayer_set_Enable_m0BF1AB18F2A620C840531101B5CC7B33022E150A (DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Program.Audio.ButtonClick/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m40813A2B2E6D775355D3CDFFC065FAD5F1C8F95B (U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F * __this, const RuntimeMethod* method);
// Program.Audio.AudioController Program.Audio.AudioController::get_Singleton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F * AudioController_get_Singleton_m23473DECF654EB8C9EF43BD85282CEFBF4023594_inline (const RuntimeMethod* method);
// System.Void Program.Audio.AudioController::PlayOnClickButton()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioController_PlayOnClickButton_mB766132F4DA6AFD200BC1C832A64AC10B06DA0A4 (AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F * __this, const RuntimeMethod* method);
// System.Void Program.Audio.ButtonClickColor/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mB4922A770A757AC0778C8BA26D0811911F196C4E (U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4 * __this, const RuntimeMethod* method);
// System.Void Program.Audio.AudioController::PlayOnClickColorButton()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioController_PlayOnClickColorButton_m2B6A898AB5F8DFF20BF8747D0C9DCD22A8C72DCC (AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F * __this, const RuntimeMethod* method);
// System.Void Program.UI.ColorPalitra::Take(Program.UI.ColorItem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorPalitra_Take_m915CDF50CB7CE7BCF4583A354CDE2B76F5A041B0 (ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3 * __this, ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650 * ___item0, const RuntimeMethod* method);
// System.Void Program.UI.DefaultCamera/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m6E994BF29BC2D19215C5E3B73A23592ABCBC1463 (U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Program.Editor.GetPixels/Triangle>::.ctor()
inline void List_1__ctor_mB8DE1969DD30A9DD0A0EB797ABE6D6E3AB1AE461 (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Program.Editor.GetPixels/Triangle>::Add(!0)
inline void List_1_Add_m2FD0A3E3967129689A18B133C0AB1C60674D1BEC (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * __this, Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 *, Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// !0 System.Collections.Generic.List`1<Program.Editor.GetPixels/Triangle>::get_Item(System.Int32)
inline Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * List_1_get_Item_m07689479A4628A308A145509E0858A6A782CD8BF_inline (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * (*) (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// Program.Editor.GetPixels/Triangle[] Program.Editor.GetPixels/Triangle::GetConnect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* Triangle_GetConnect_mB0722685A89A197549E71DDEDA17DC7AD119E4DD (Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Program.Editor.GetPixels/Triangle>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1_AddRange_m054C1E7F4F8B20005C0A988D0EB20B6711E58227 (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m6465DEF706EB529B4227F2AF79338419D517EDF9_gshared)(__this, ___collection0, method);
}
// System.Int32 System.Collections.Generic.List`1<Program.Editor.GetPixels/Triangle>::get_Count()
inline int32_t List_1_get_Count_mC36BC2EDA73A732A891DE47D6E71213DF5544622_inline (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// !0[] System.Collections.Generic.List`1<Program.Editor.GetPixels/Triangle>::ToArray()
inline TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* List_1_ToArray_mDE8A2E8CE776F68F8B0D394D1602EEB7B2D06EA1 (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * __this, const RuntimeMethod* method)
{
	return ((  TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* (*) (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 *, const RuntimeMethod*))List_1_ToArray_mA737986DE6389E9DD8FA8E3D4E222DE4DA34958D_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void Program.Menu.NewsServer::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NewsServer_Start_m5A55EA8BB183B715BECD9CFA9132988A9C12C61A (const RuntimeMethod* method);
// System.Void Program.Menu.ListNews::LoadListNews()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListNews_LoadListNews_m9C0155DFB850A4099B7700B0B635580C28BE8999 (ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * __this, const RuntimeMethod* method);
// System.Void Program.Menu.NewsServer::SaveCurrentFile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NewsServer_SaveCurrentFile_mE0D4921FC0E1622C8E3143C5FF9F93D7F36A6F81 (const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSecondsRealtime::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225 (WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * __this, float ___time0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void Program.Editor.TexturesServer::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TexturesServer_Start_mF17ABA42248EE6E7782E1CCD55123CE67C767FCB (const RuntimeMethod* method);
// System.Void Program.Editor.TexturesServer::GenerateUnityTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TexturesServer_GenerateUnityTexture_mFB79B1E2B18B242F87BAD7FC8B4924B6A7576B6F (const RuntimeMethod* method);
// System.Void Program.UI.LoadLibraryPanel::LoadList(TextureFile[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadLibraryPanel_LoadList_mAB71BDBC92DD8B3E5FCE30F951A9AAABD689263B (LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * __this, TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* ___files0, const RuntimeMethod* method);
// System.Void Program.Editor.TexturesServer::SaveCurrentFile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TexturesServer_SaveCurrentFile_m6ED026056AAA606513828662C42F38E530C4BC48 (const RuntimeMethod* method);
// System.Void Program.UI.LoadMenu/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mD73860545FF53EC49CA9184FCAD0FE2FFF78DEA0 (U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5 (int32_t ___sceneBuildIndex0, const RuntimeMethod* method);
// System.Void Program.UI.MenuPaltira/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m7E18D07EDEB57DA53149F9156B637B1C2A1EDD0B (U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE * __this, const RuntimeMethod* method);
// Palitra ProjectPalitra::GetPalitra()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Palitra_t41281A13EF2874D486547B9324202C7D112880B3 * ProjectPalitra_GetPalitra_m426101BEAACA8C9F1E4F78C72028A9716DC1C513 (ProjectPalitra_tE92E7BFB63915D8633D6B7A34DD7D32D2EEFC847 * __this, const RuntimeMethod* method);
// System.Void Program.Editor.Model/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m8AF44807C9189D0DDBEB1F38D11927CD90DC7247 (U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
inline MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * Component_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_m4E31C5D3D0490AEE405B54BE9F61802EA425B9DC (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * MeshFilter_get_mesh_mFC1DF5AFBC1E4269D08628DB83C954882FD2B417 (MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Program.Editor.Model/Story>::.ctor()
inline void List_1__ctor_m94FA9771A56F4CCC6087669FB8221EBB6316096F (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void Program.Editor.Model/History::set_CurrentStory(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2_inline (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 Program.Editor.Model/History::get_CurrentStory()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Program.Editor.Model/Story>::get_Count()
inline int32_t List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_inline (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<Program.Editor.Model/Story>::get_Item(System.Int32)
inline Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 * List_1_get_Item_m34D1481BA2C3E15122493C0DE376BFC24A3732EA_inline (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 * (*) (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ImageConversion_EncodeToPNG_mA598C2969C878ACC5AEA5FDC92C6199EB30D51ED (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___tex0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___tex0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data1, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_m3BB3975288119BA62ED9BE4243F7767EC2F88CA0 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// System.Void Program.Editor.Model/History::ClearStory(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void History_ClearStory_m74055B06CDC0551ACF9A841D7F110EAED6B2DC50 (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, int32_t ___num0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Program.Editor.Model/Story>::Add(!0)
inline void List_1_Add_mC5F56B296FBB59F233C39F696B4811C2B3F76A18 (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * __this, Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD *, Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<Program.Editor.Model/Story>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m2383E89A18C3A19BFC8BCC1763ADC4321508C883 (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m66148860899ECCAE9B323372032BFC1C255393D2_gshared)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<Program.Editor.Model/Story>::Clear()
inline void List_1_Clear_m95A9EDACA9EDF273FB8BBAEE846E7738A872EA00 (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Program.Editor.Model/Story>::RemoveRange(System.Int32,System.Int32)
inline void List_1_RemoveRange_m0266B9170CB4F7A00AFF336AA360766D0F6FAF66 (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * __this, int32_t ___index0, int32_t ___count1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD *, int32_t, int32_t, const RuntimeMethod*))List_1_RemoveRange_m0BBC3852B9B0719DDA7E6AFEF3C3E3CD165DF5AA_gshared)(__this, ___index0, ___count1, method);
}
// System.Void Program.Option.OptionController/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m50BC0DBD2EC29FE4E39FD44C84748386CC5260E9 (U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E * __this, const RuntimeMethod* method);
// System.Void Program.UI.MenuPaltira::NewTakePalitra(Palitra)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuPaltira_NewTakePalitra_m8504090ACB63ECC3B5A1DFD458F6995373A8A5E1 (MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * __this, Palitra_t41281A13EF2874D486547B9324202C7D112880B3 * ___palitra0, const RuntimeMethod* method);
// System.Void Program.UI.PalitraPanel/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mF7D1199BFF2C780D70E9D73848C8D33ADFDA4F72 (U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * __this, const RuntimeMethod* method);
// Program.UI.HistoryButton Program.UI.PalitraPanel::get_History()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB * PalitraPanel_get_History_mD749E9F50722D5B8D5110C265F2CDAEAFDD881AB_inline (const RuntimeMethod* method);
// System.Void Program.UI.HistoryButton::BackHistory()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HistoryButton_BackHistory_m87C5E919DA71B99989BBF0FB67EFFB6DA65C9C8B (HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB * __this, const RuntimeMethod* method);
// System.Void Program.UI.HistoryButton::NextHistory()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HistoryButton_NextHistory_m809836890A22F382AD96F564748FBB68C2A3934F (HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB * __this, const RuntimeMethod* method);
// Program.UI.Instruments Program.UI.PalitraPanel::get_Instrument()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * PalitraPanel_get_Instrument_m7F79D1932B2C71BB7B236D976366BFDC4965020C_inline (const RuntimeMethod* method);
// System.Void Program.UI.Instruments::TakePaint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Instruments_TakePaint_mF5A5660834B59D12BEE85DBECE098C2B9A518867 (Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * __this, const RuntimeMethod* method);
// System.Void Program.UI.Instruments::TakeColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Instruments_TakeColor_m1286AF6ABD47C645C9CA73EE5E37806B59DAF8AA (Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * __this, const RuntimeMethod* method);
// System.Void Program.UI.Instruments::TakeFillColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Instruments_TakeFillColor_mB3AE6CBE53415CD455CF250626B7B7BA72739B40 (Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * __this, const RuntimeMethod* method);
// System.Void Program.UI.Instruments::TakeErase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Instruments_TakeErase_m556D71FCB88F4B8D858379D644A22DABFD6E47BE (Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.CanvasScaler::set_referenceResolution(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CanvasScaler_set_referenceResolution_mD3200E76103B16C8064B6E48595EFC9C5BDCB719 (CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void Program.Menu.UIMenu/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mC295447BB5C8F7D4EE7E0278E80DF0EC99E1264D (U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D * __this, const RuntimeMethod* method);
// Program.Menu.BaseMenu Program.Menu.BaseMenu::get_Singleton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 * BaseMenu_get_Singleton_m069EC972BD638A8FB525054E1611FB620CA75AA5_inline (const RuntimeMethod* method);
// System.Void Program.Menu.BaseMenu::LoadEditor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMenu_LoadEditor_mB6D54A1389D8C9489397FAA0E85E65BB159AA571 (BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 * __this, const RuntimeMethod* method);
// System.Void Program.Menu.BaseMenu::ExitProgram()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMenu_ExitProgram_m35F3098E90075D44F5141D44A15A2A730C27B7B5 (BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 * __this, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Threading.Tasks.Task System.Threading.Tasks.Task::Run(System.Action)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * Task_Run_m77F41B8D89AFD69BE94888BC2CAD5E3183A79B8D (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___action0, const RuntimeMethod* method);
// System.Runtime.CompilerServices.TaskAwaiter System.Threading.Tasks.Task::GetAwaiter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  Task_GetAwaiter_m1FF7528A8FE13F79207DFE970F642078EF6B1260 (Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * __this, const RuntimeMethod* method);
// System.Boolean System.Runtime.CompilerServices.TaskAwaiter::get_IsCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TaskAwaiter_get_IsCompleted_m6F97613C55E505B5664C3C0CFC4677D296EAA8BC (TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.TaskAwaiter,FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8>(!!0&,!!1&)
inline void AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_m77090A1636BA1001FD6018EED4EEC9FEAE973E0C (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * ___awaiter0, U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 * ___stateMachine1, const RuntimeMethod* method)
{
	((  void (*) (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *, TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *, U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 *, const RuntimeMethod*))AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_m77090A1636BA1001FD6018EED4EEC9FEAE973E0C_gshared)(__this, ___awaiter0, ___stateMachine1, method);
}
// System.Void System.Runtime.CompilerServices.TaskAwaiter::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskAwaiter_GetResult_m578EEFEC4DD1AE5E77C899B8BAA3825EB79D1330 (TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::SetException(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncVoidMethodBuilder_SetException_m16372173CEA3031B4CB9B8D15DA97C457F835155 (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, Exception_t * ___exception0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::SetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncVoidMethodBuilder_SetResult_m901385B56EBE93E472A77EA48F61E4F498F3E00E (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, const RuntimeMethod* method);
// System.Void FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateU3Ed__8_MoveNext_mDDFCC9C7E5C00438CA37A40742B4CF2EF430368A (U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncVoidMethodBuilder_SetStateMachine_m1ED99BE03B146D8A7117E299EBA5D74999EB52D7 (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, RuntimeObject* ___stateMachine0, const RuntimeMethod* method);
// System.Void FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateU3Ed__8_SetStateMachine_m7CB0536971DEF64B4EDF5E17A72BC6F20ACC8D6B (U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 * __this, RuntimeObject* ___stateMachine0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean FilerServer.Base.BaseConnect/BaseProcces::get_isEnd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BaseProcces_get_isEnd_m8F6AA9B2BB0E3D584BA0CB277ED21E65DD002067 (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method)
{
	{
		// return _is_end;
		bool L_0 = __this->get__is_end_0();
		return L_0;
	}
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces::set_isEnd(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_set_isEnd_mB7801225FC730DD85B549BA70DDA25A1CECBAF04 (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (!_is_end && value == true)
		bool L_0 = __this->get__is_end_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ___value0;
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		// End();
		BaseProcces_End_m1CAF78A6E42FCE331CA98540417F50F507CD356B(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		// _is_end = value;
		bool L_2 = ___value0;
		__this->set__is_end_0(L_2);
		// }
		return;
	}
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces::Init(System.Action,System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_Init_m6A9D3FB045080842FBB72839A5371973092FD1FF (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___start0, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___end1, const RuntimeMethod* method)
{
	{
		// this.start = start;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_0 = ___start0;
		__this->set_start_1(L_0);
		// this.end = end;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = ___end1;
		__this->set_end_2(L_1);
		// }
		return;
	}
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_Start_m19956E45012AEF19F4054499881CDB51F047CDD5 (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method)
{
	{
		// start();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_0 = __this->get_start_1();
		NullCheck(L_0);
		Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E(L_0, /*hidden argument*/NULL);
		// Update();
		BaseProcces_Update_mBC2C04C98E90C05C6C9347EDC4F03F9C64C1E5BD(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_Update_mBC2C04C98E90C05C6C9347EDC4F03F9C64C1E5BD (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncVoidMethodBuilder_Start_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_mB911DC1C3B7DF7B772D17AC5FBAB824F5FF66FF6_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708  V_0;
	memset((&V_0), 0, sizeof(V_0));
	AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		(&V_0)->set_U3CU3E4__this_2(__this);
		AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6  L_0;
		L_0 = AsyncVoidMethodBuilder_Create_m878314259623CC47A2EBAEEF2F8E8D6B61560FA5(/*hidden argument*/NULL);
		(&V_0)->set_U3CU3Et__builder_1(L_0);
		(&V_0)->set_U3CU3E1__state_0((-1));
		U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708  L_1 = V_0;
		AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6  L_2 = L_1.get_U3CU3Et__builder_1();
		V_1 = L_2;
		AsyncVoidMethodBuilder_Start_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_mB911DC1C3B7DF7B772D17AC5FBAB824F5FF66FF6((AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *)(&V_1), (U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 *)(&V_0), /*hidden argument*/AsyncVoidMethodBuilder_Start_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_mB911DC1C3B7DF7B772D17AC5FBAB824F5FF66FF6_RuntimeMethod_var);
		return;
	}
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces::CallUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_CallUpdate_m1AC15AA8E1DA171F18003C180673F27ABC6AEB72 (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces::Connect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_Connect_mCD8ED8C8738CE882039E305BE5F30B0503F5DF45 (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_0 = (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 *)il2cpp_codegen_object_new(Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09_il2cpp_TypeInfo_var);
		Socket__ctor_m5A4B335AEC1450ABE31CF1151F3F5A93D9D0280C(L_0, 2, 1, 6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var);
		((BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields*)il2cpp_codegen_static_fields_for(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var))->set_Socket_9(L_0);
		// point = new IPEndPoint(IPAddress.Parse(IP), Port);
		String_t* L_1 = ((BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields*)il2cpp_codegen_static_fields_for(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var))->get_IP_0();
		IL2CPP_RUNTIME_CLASS_INIT(IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE_il2cpp_TypeInfo_var);
		IPAddress_t2B5F1762B4B9935BA6CA8FB12C87282C72E035AE * L_2;
		L_2 = IPAddress_Parse_m49C413225AC75DA34D1663559818861CA34C3CB0(L_1, /*hidden argument*/NULL);
		IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * L_3 = (IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E *)il2cpp_codegen_object_new(IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E_il2cpp_TypeInfo_var);
		IPEndPoint__ctor_m22783A215BA0B38674F6A6CB6803804268561321(L_3, L_2, ((int32_t)4246), /*hidden argument*/NULL);
		((BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields*)il2cpp_codegen_static_fields_for(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var))->set_point_10(L_3);
		// Socket.Connect(point);
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_4 = ((BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields*)il2cpp_codegen_static_fields_for(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var))->get_Socket_9();
		EndPoint_t18D4AE8D03090A2B262136E59F95CE61418C34DA * L_5 = ((BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields*)il2cpp_codegen_static_fields_for(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var))->get_point_10();
		NullCheck(L_4);
		Socket_Connect_mA047E173F3E1082B396D018585E0B0B2D6E8E5A8(L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces::End()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_End_m1CAF78A6E42FCE331CA98540417F50F507CD356B (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// BaseConnect.RemoveProcces();
		IL2CPP_RUNTIME_CLASS_INIT(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var);
		BaseConnect_RemoveProcces_m3D2E114B21E59102C6E9B7D3E832F976BAC9FFD7(/*hidden argument*/NULL);
		// CallEnd();
		VirtActionInvoker0::Invoke(5 /* System.Void FilerServer.Base.BaseConnect/BaseProcces::CallEnd() */, __this);
		// if (BaseConnect.Socket.Connected)
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_0 = ((BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields*)il2cpp_codegen_static_fields_for(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var))->get_Socket_9();
		NullCheck(L_0);
		bool L_1;
		L_1 = Socket_get_Connected_m6E8C88AC69580EF7782514CD48754D4D180D9CE0_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		// Socket.Close();
		IL2CPP_RUNTIME_CLASS_INIT(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var);
		Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * L_2 = ((BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_StaticFields*)il2cpp_codegen_static_fields_for(BaseConnect_t94F0609308F13C685AACC1A495718DA6C89E0D03_il2cpp_TypeInfo_var))->get_Socket_9();
		NullCheck(L_2);
		Socket_Close_m24AB78F5DAC1C39BB7FFB30A9620B2B07E01DEEB(L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		// end();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = __this->get_end_2();
		NullCheck(L_3);
		Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces::CallEnd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_CallEnd_m80AD04944941EC21BE886CB85AA358860B7AD1AC (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces__ctor_m61F338957AFD830C5F4E6C51CF2F0901838ACC00 (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces::<Update>b__8_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProcces_U3CUpdateU3Eb__8_0_mAD6652D6C6523D25840710F6A2E3C0C01767203F (BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// await Task.Run(() => { try { CallUpdate(); } catch { } isEnd = true; });
		VirtActionInvoker0::Invoke(4 /* System.Void FilerServer.Base.BaseConnect/BaseProcces::CallUpdate() */, __this);
		// await Task.Run(() => { try { CallUpdate(); } catch { } isEnd = true; });
		goto IL_000b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&RuntimeObject_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0008;
		throw e;
	}

CATCH_0008:
	{ // begin catch(System.Object)
		// await Task.Run(() => { try { CallUpdate(); } catch { } isEnd = true; });
		// await Task.Run(() => { try { CallUpdate(); } catch { } isEnd = true; });
		goto IL_000b;
	} // end catch (depth: 1)

IL_000b:
	{
		// await Task.Run(() => { try { CallUpdate(); } catch { } isEnd = true; });
		BaseProcces_set_isEnd_mB7801225FC730DD85B549BA70DDA25A1CECBAF04(__this, (bool)1, /*hidden argument*/NULL);
		// await Task.Run(() => { try { CallUpdate(); } catch { } isEnd = true; });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.UI.BodyPanel/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mFB447CA74623BFB853B20192A3BB156A896F178A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C * L_0 = (U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C *)il2cpp_codegen_object_new(U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m219117CD33D8037CB10012C35FCCFAF38B21B619(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Program.UI.BodyPanel/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m219117CD33D8037CB10012C35FCCFAF38B21B619 (U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Program.UI.BodyPanel/<>c::<Awake>b__35_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CAwakeU3Eb__35_0_m58535A70F71FED669D637CEEC787F64F987C0A8F (U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C * __this, const RuntimeMethod* method)
{
	{
		// openUV.onClick.AddListener(delegate { UVEditor.Open(); });
		UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9 * L_0;
		L_0 = BodyPanel_get_UVEditor_mB67B5BA8BC6C9B3D828712FFE97D6D5E3209E8CE_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		UVPanel_Open_mA452877254FF476D6AA0734706B8E329ED6A8665(L_0, /*hidden argument*/NULL);
		// openUV.onClick.AddListener(delegate { UVEditor.Open(); });
		return;
	}
}
// System.Void Program.UI.BodyPanel/<>c::<Awake>b__35_1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CAwakeU3Eb__35_1_m6210BB263252546A9880CB7CA24F68334C7F9ED8 (U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C * __this, const RuntimeMethod* method)
{
	{
		// renderButton.onClick.AddListener(delegate { StartRender.StartRender(); });
		Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08 * L_0;
		L_0 = BodyPanel_get_StartRender_mAAB7A1D9289510C59929934F34C22C74FD0BE8AF_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		Render_StartRender_mD3B88E93D93D28AC89F251010D7B5D6ED1A77E91(L_0, /*hidden argument*/NULL);
		// renderButton.onClick.AddListener(delegate { StartRender.StartRender(); });
		return;
	}
}
// System.Void Program.UI.BodyPanel/<>c::<SetOptionBodyParts>b__36_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CSetOptionBodyPartsU3Eb__36_0_mA4EBB0EB599C1DDE16716FECE0272D2371F85425 (U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C * __this, const RuntimeMethod* method)
{
	{
		// layerDownButton.onClick.AddListener(delegate { LayerDown.Enable = !LayerDown.Enable; });
		DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * L_0;
		L_0 = BodyPanel_get_LayerDown_m29403D6F813A24BCB8A9239FDD6573D75D3C9D7D_inline(/*hidden argument*/NULL);
		DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * L_1;
		L_1 = BodyPanel_get_LayerDown_m29403D6F813A24BCB8A9239FDD6573D75D3C9D7D_inline(/*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2;
		L_2 = DownLayer_get_Enable_m49AAD772F67A74CE9EA1E2EF80369E7963C9F42A_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		DownLayer_set_Enable_m0BF1AB18F2A620C840531101B5CC7B33022E150A(L_0, (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// layerDownButton.onClick.AddListener(delegate { LayerDown.Enable = !LayerDown.Enable; });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.Audio.ButtonClick/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m2F762773421FD81FA0A5B495455234128082FC7E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F * L_0 = (U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F *)il2cpp_codegen_object_new(U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m40813A2B2E6D775355D3CDFFC065FAD5F1C8F95B(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Program.Audio.ButtonClick/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m40813A2B2E6D775355D3CDFFC065FAD5F1C8F95B (U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Program.Audio.ButtonClick/<>c::<Start>b__1_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__1_0_m213571C10571A5A8A8319FB88AF9A64F6B4635B3 (U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F * __this, const RuntimeMethod* method)
{
	{
		// button.onClick.AddListener(delegate { Audio.AudioController.Singleton.PlayOnClickButton(); });
		AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F * L_0;
		L_0 = AudioController_get_Singleton_m23473DECF654EB8C9EF43BD85282CEFBF4023594_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		AudioController_PlayOnClickButton_mB766132F4DA6AFD200BC1C832A64AC10B06DA0A4(L_0, /*hidden argument*/NULL);
		// button.onClick.AddListener(delegate { Audio.AudioController.Singleton.PlayOnClickButton(); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.Audio.ButtonClickColor/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mA4257105044085DBF05F0275D69C7AD4DCEB2241 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4 * L_0 = (U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4 *)il2cpp_codegen_object_new(U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mB4922A770A757AC0778C8BA26D0811911F196C4E(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Program.Audio.ButtonClickColor/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mB4922A770A757AC0778C8BA26D0811911F196C4E (U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Program.Audio.ButtonClickColor/<>c::<Start>b__1_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__1_0_m362E47FAACAFA08B0FB3B2C66EB4926C7A49472F (U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4 * __this, const RuntimeMethod* method)
{
	{
		// button.onClick.AddListener(delegate { Audio.AudioController.Singleton.PlayOnClickColorButton(); });
		AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F * L_0;
		L_0 = AudioController_get_Singleton_m23473DECF654EB8C9EF43BD85282CEFBF4023594_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		AudioController_PlayOnClickColorButton_m2B6A898AB5F8DFF20BF8747D0C9DCD22A8C72DCC(L_0, /*hidden argument*/NULL);
		// button.onClick.AddListener(delegate { Audio.AudioController.Singleton.PlayOnClickColorButton(); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.UI.ColorItem/<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m9D47667D7B30A51BEC5E15A03C1792D622CB7FDB (U3CU3Ec__DisplayClass2_0_t2104F99A587EAB2260F5C1947DDBF7A6D9C6C62A * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Program.UI.ColorItem/<>c__DisplayClass2_0::<Init>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CInitU3Eb__0_m8CD7ED6D1C0E69BAA6263927FD31EC9E3F2D4ABA (U3CU3Ec__DisplayClass2_0_t2104F99A587EAB2260F5C1947DDBF7A6D9C6C62A * __this, const RuntimeMethod* method)
{
	{
		// button.onClick.AddListener(delegate { colorTake.Take(this); });
		ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3 * L_0 = __this->get_colorTake_0();
		ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650 * L_1 = __this->get_U3CU3E4__this_1();
		NullCheck(L_0);
		ColorPalitra_Take_m915CDF50CB7CE7BCF4583A354CDE2B76F5A041B0(L_0, L_1, /*hidden argument*/NULL);
		// button.onClick.AddListener(delegate { colorTake.Take(this); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.UI.DefaultCamera/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mD2BC887CE7A79175439D133D7586980F56D4479A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522 * L_0 = (U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522 *)il2cpp_codegen_object_new(U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m6E994BF29BC2D19215C5E3B73A23592ABCBC1463(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Program.UI.DefaultCamera/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m6E994BF29BC2D19215C5E3B73A23592ABCBC1463 (U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Program.UI.DefaultCamera/<>c::<Start>b__4_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__4_0_mB0D4DB18A8A65187B21EDE9D57E3C5F046855AAE (U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// button.onClick.AddListener(delegate { if (ClickButton != null) ClickButton(); });
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_0 = ((DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_StaticFields*)il2cpp_codegen_static_fields_for(DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_il2cpp_TypeInfo_var))->get_ClickButton_4();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		// button.onClick.AddListener(delegate { if (ClickButton != null) ClickButton(); });
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = ((DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_StaticFields*)il2cpp_codegen_static_fields_for(DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_il2cpp_TypeInfo_var))->get_ClickButton_4();
		NullCheck(L_1);
		Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E(L_1, /*hidden argument*/NULL);
	}

IL_0011:
	{
		// button.onClick.AddListener(delegate { if (ClickButton != null) ClickButton(); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Program.Editor.GetPixels/Triangle[] Program.Editor.GetPixels/Triangle::GetConnect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* Triangle_GetConnect_mB0722685A89A197549E71DDEDA17DC7AD119E4DD (Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_AddRange_m054C1E7F4F8B20005C0A988D0EB20B6711E58227_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m2FD0A3E3967129689A18B133C0AB1C60674D1BEC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_ToArray_mDE8A2E8CE776F68F8B0D394D1602EEB7B2D06EA1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mB8DE1969DD30A9DD0A0EB797ABE6D6E3AB1AE461_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mC36BC2EDA73A732A891DE47D6E71213DF5544622_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m07689479A4628A308A145509E0858A6A782CD8BF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// List<Triangle> triangles = new List<Triangle>();
		List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * L_0 = (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 *)il2cpp_codegen_object_new(List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923_il2cpp_TypeInfo_var);
		List_1__ctor_mB8DE1969DD30A9DD0A0EB797ABE6D6E3AB1AE461(L_0, /*hidden argument*/List_1__ctor_mB8DE1969DD30A9DD0A0EB797ABE6D6E3AB1AE461_RuntimeMethod_var);
		V_0 = L_0;
		// triangles.Add(this);
		List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * L_1 = V_0;
		NullCheck(L_1);
		List_1_Add_m2FD0A3E3967129689A18B133C0AB1C60674D1BEC(L_1, __this, /*hidden argument*/List_1_Add_m2FD0A3E3967129689A18B133C0AB1C60674D1BEC_RuntimeMethod_var);
		// isCheck = true;
		__this->set_isCheck_3((bool)1);
		// for (int i = 0; i < connect.Count; i++)
		V_1 = 0;
		goto IL_0046;
	}

IL_0018:
	{
		// if (!connect[i].isCheck)
		List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * L_2 = __this->get_connect_4();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * L_4;
		L_4 = List_1_get_Item_m07689479A4628A308A145509E0858A6A782CD8BF_inline(L_2, L_3, /*hidden argument*/List_1_get_Item_m07689479A4628A308A145509E0858A6A782CD8BF_RuntimeMethod_var);
		NullCheck(L_4);
		bool L_5 = L_4->get_isCheck_3();
		if (L_5)
		{
			goto IL_0042;
		}
	}
	{
		// triangles.AddRange(connect[i].GetConnect());
		List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * L_6 = V_0;
		List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * L_7 = __this->get_connect_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * L_9;
		L_9 = List_1_get_Item_m07689479A4628A308A145509E0858A6A782CD8BF_inline(L_7, L_8, /*hidden argument*/List_1_get_Item_m07689479A4628A308A145509E0858A6A782CD8BF_RuntimeMethod_var);
		NullCheck(L_9);
		TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* L_10;
		L_10 = Triangle_GetConnect_mB0722685A89A197549E71DDEDA17DC7AD119E4DD(L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		List_1_AddRange_m054C1E7F4F8B20005C0A988D0EB20B6711E58227(L_6, (RuntimeObject*)(RuntimeObject*)L_10, /*hidden argument*/List_1_AddRange_m054C1E7F4F8B20005C0A988D0EB20B6711E58227_RuntimeMethod_var);
	}

IL_0042:
	{
		// for (int i = 0; i < connect.Count; i++)
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0046:
	{
		// for (int i = 0; i < connect.Count; i++)
		int32_t L_12 = V_1;
		List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * L_13 = __this->get_connect_4();
		NullCheck(L_13);
		int32_t L_14;
		L_14 = List_1_get_Count_mC36BC2EDA73A732A891DE47D6E71213DF5544622_inline(L_13, /*hidden argument*/List_1_get_Count_mC36BC2EDA73A732A891DE47D6E71213DF5544622_RuntimeMethod_var);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0018;
		}
	}
	{
		// return triangles.ToArray();
		List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * L_15 = V_0;
		NullCheck(L_15);
		TriangleU5BU5D_t01A2E2DDDCE3C04A603BC09357E6484615287D79* L_16;
		L_16 = List_1_ToArray_mDE8A2E8CE776F68F8B0D394D1602EEB7B2D06EA1(L_15, /*hidden argument*/List_1_ToArray_mDE8A2E8CE776F68F8B0D394D1602EEB7B2D06EA1_RuntimeMethod_var);
		return L_16;
	}
}
// System.Void Program.Editor.GetPixels/Triangle::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Triangle__ctor_m86F2DE51B2CBE08E6040FA345E0176C69342B2ED (Triangle_tB1EA9F9F46CA2072FAA61C2FB2F9472A6576CB18 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mB8DE1969DD30A9DD0A0EB797ABE6D6E3AB1AE461_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Triangle> connect = new List<Triangle>();
		List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 * L_0 = (List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923 *)il2cpp_codegen_object_new(List_1_t0C1F693C1673BA728E4A1394C62A1FEC01955923_il2cpp_TypeInfo_var);
		List_1__ctor_mB8DE1969DD30A9DD0A0EB797ABE6D6E3AB1AE461(L_0, /*hidden argument*/List_1__ctor_mB8DE1969DD30A9DD0A0EB797ABE6D6E3AB1AE461_RuntimeMethod_var);
		__this->set_connect_4(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.Menu.ListNews/<LoadNewsWait>d__8::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadNewsWaitU3Ed__8__ctor_m954E460A8D2E865D49227C50AD953ADB62BD3751 (U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Program.Menu.ListNews/<LoadNewsWait>d__8::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadNewsWaitU3Ed__8_System_IDisposable_Dispose_mD506D633D37BB4EDC8E584479ED3C7D4D05326C2 (U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Program.Menu.ListNews/<LoadNewsWait>d__8::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CLoadNewsWaitU3Ed__8_MoveNext_mB01976F71C96F54106C92D71253B8EBF10A86D5A (U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1B2D546B68B913141D48C16BD1B6C81F159853D3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2753D2982C7E0B6A494B13A85BFA999F0F3C0D17);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79275F5DC38DFCA0BC6C5F6A6F0BDFA6B072469D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF766F1D50CD96D8870845654104505296E397678);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_00ae;
		}
	}
	{
		return (bool)0;
	}

IL_001a:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (!NewsServer.IsLoad && !NewsServer.IsLoadEnd)
		bool L_4 = ((NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields*)il2cpp_codegen_static_fields_for(NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_il2cpp_TypeInfo_var))->get_IsLoad_2();
		if (L_4)
		{
			goto IL_0061;
		}
	}
	{
		bool L_5 = ((NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields*)il2cpp_codegen_static_fields_for(NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_il2cpp_TypeInfo_var))->get_IsLoadEnd_1();
		if (L_5)
		{
			goto IL_0061;
		}
	}
	{
		// print("Start load news");
		MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090(_stringLiteral2753D2982C7E0B6A494B13A85BFA999F0F3C0D17, /*hidden argument*/NULL);
		// NewsServer.Start();
		NewsServer_Start_m5A55EA8BB183B715BECD9CFA9132988A9C12C61A(/*hidden argument*/NULL);
		// if (NewsServer.NewsLoad != null && NewsServer.NewsLoad.ArrayNews != null)
		FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A * L_6 = ((NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields*)il2cpp_codegen_static_fields_for(NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_il2cpp_TypeInfo_var))->get_NewsLoad_0();
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		FileNews_tD6DD22094085D16BB1385B590CCD0706DBF49D0A * L_7 = ((NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields*)il2cpp_codegen_static_fields_for(NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_il2cpp_TypeInfo_var))->get_NewsLoad_0();
		NullCheck(L_7);
		NewsU5BU5D_t7CD4BFBC754A2A366E68BF321543B7DEF517F927* L_8 = L_7->get_ArrayNews_2();
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		// print("Start load base");
		MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090(_stringLiteral79275F5DC38DFCA0BC6C5F6A6F0BDFA6B072469D, /*hidden argument*/NULL);
		// LoadListNews();
		ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * L_9 = V_1;
		NullCheck(L_9);
		ListNews_LoadListNews_m9C0155DFB850A4099B7700B0B635580C28BE8999(L_9, /*hidden argument*/NULL);
	}

IL_0061:
	{
		// if (NewsServer.IsLoadEnd) {
		bool L_10 = ((NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields*)il2cpp_codegen_static_fields_for(NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_il2cpp_TypeInfo_var))->get_IsLoadEnd_1();
		if (!L_10)
		{
			goto IL_007f;
		}
	}
	{
		// LoadListNews();
		ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01 * L_11 = V_1;
		NullCheck(L_11);
		ListNews_LoadListNews_m9C0155DFB850A4099B7700B0B635580C28BE8999(L_11, /*hidden argument*/NULL);
		// NewsServer.SaveCurrentFile();
		NewsServer_SaveCurrentFile_mE0D4921FC0E1622C8E3143C5FF9F93D7F36A6F81(/*hidden argument*/NULL);
		// print("end download news");
		MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090(_stringLiteral1B2D546B68B913141D48C16BD1B6C81F159853D3, /*hidden argument*/NULL);
		// break;
		goto IL_00b7;
	}

IL_007f:
	{
		// if (!NewsServer.IsLoad) {
		bool L_12 = ((NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_StaticFields*)il2cpp_codegen_static_fields_for(NewsServer_tFDB676CBF962FCB81FD8D6BA3805519B3522BBC6_il2cpp_TypeInfo_var))->get_IsLoad_2();
		if (L_12)
		{
			goto IL_0095;
		}
	}
	{
		// print("reload news");
		MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090(_stringLiteralF766F1D50CD96D8870845654104505296E397678, /*hidden argument*/NULL);
		// NewsServer.Start();
		NewsServer_Start_m5A55EA8BB183B715BECD9CFA9132988A9C12C61A(/*hidden argument*/NULL);
	}

IL_0095:
	{
		// yield return new WaitForSecondsRealtime(2.0f);
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_13 = (WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225(L_13, (2.0f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_13);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00ae:
	{
		__this->set_U3CU3E1__state_0((-1));
		// while (true) {
		goto IL_0061;
	}

IL_00b7:
	{
		// yield break;
		return (bool)0;
	}
}
// System.Object Program.Menu.ListNews/<LoadNewsWait>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadNewsWaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEA5FD9FFF67794B08B86C64DE1B8AA4D629E6CD (U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Program.Menu.ListNews/<LoadNewsWait>d__8::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_Reset_m53159E79EBA8B80BAC673EDE60518A4F30D17A98 (U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_Reset_m53159E79EBA8B80BAC673EDE60518A4F30D17A98_RuntimeMethod_var)));
	}
}
// System.Object Program.Menu.ListNews/<LoadNewsWait>d__8::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_get_Current_m3172BAD8411C52CFB7BBDC7E0C0F447315DFB4C5 (U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadTextureWaitU3Ed__11__ctor_mA8EFBFFC7538B986E47910341B21AAFC99C1D578 (U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadTextureWaitU3Ed__11_System_IDisposable_Dispose_m32278EABA09B57F45C467C7AC661D922A044A128 (U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CLoadTextureWaitU3Ed__11_MoveNext_mA8B2805F31A2027DACB264C59861C31FF622618C (U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_00b9;
		}
	}
	{
		return (bool)0;
	}

IL_001a:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (!TexturesServer.IsLoad && !TexturesServer.IsLoadEnd)
		bool L_4 = ((TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields*)il2cpp_codegen_static_fields_for(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var))->get_IsLoad_3();
		if (L_4)
		{
			goto IL_0063;
		}
	}
	{
		bool L_5 = ((TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields*)il2cpp_codegen_static_fields_for(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var))->get_IsLoadEnd_2();
		if (L_5)
		{
			goto IL_0063;
		}
	}
	{
		// TexturesServer.Start();
		TexturesServer_Start_mF17ABA42248EE6E7782E1CCD55123CE67C767FCB(/*hidden argument*/NULL);
		// if (TexturesServer.TextureLoad != null && TexturesServer.TextureLoad.ArrayTexture != null)
		FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84 * L_6 = ((TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields*)il2cpp_codegen_static_fields_for(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var))->get_TextureLoad_0();
		if (!L_6)
		{
			goto IL_0063;
		}
	}
	{
		FileTextures_t355FA46DE0F4CCCB67E25F1789CCFA7CE8902D84 * L_7 = ((TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields*)il2cpp_codegen_static_fields_for(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var))->get_TextureLoad_0();
		NullCheck(L_7);
		TextureU5BU5D_tFC397DCC79C0EAD5839F838C9E694ACB8C7C331B* L_8 = L_7->get_ArrayTexture_2();
		if (!L_8)
		{
			goto IL_0063;
		}
	}
	{
		// TexturesServer.GenerateUnityTexture();
		TexturesServer_GenerateUnityTexture_mFB79B1E2B18B242F87BAD7FC8B4924B6A7576B6F(/*hidden argument*/NULL);
		// serverFiles = TexturesServer.GenerateTexture;
		LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * L_9 = V_1;
		TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* L_10 = ((TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields*)il2cpp_codegen_static_fields_for(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var))->get_GenerateTexture_1();
		NullCheck(L_9);
		L_9->set_serverFiles_14(L_10);
		// LoadList(serverFiles);
		LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * L_11 = V_1;
		LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * L_12 = V_1;
		NullCheck(L_12);
		TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* L_13 = L_12->get_serverFiles_14();
		NullCheck(L_11);
		LoadLibraryPanel_LoadList_mAB71BDBC92DD8B3E5FCE30F951A9AAABD689263B(L_11, L_13, /*hidden argument*/NULL);
	}

IL_0063:
	{
		// if (TexturesServer.IsLoadEnd)
		bool L_14 = ((TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields*)il2cpp_codegen_static_fields_for(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var))->get_IsLoadEnd_2();
		if (!L_14)
		{
			goto IL_008d;
		}
	}
	{
		// TexturesServer.GenerateUnityTexture();
		TexturesServer_GenerateUnityTexture_mFB79B1E2B18B242F87BAD7FC8B4924B6A7576B6F(/*hidden argument*/NULL);
		// serverFiles = TexturesServer.GenerateTexture;
		LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * L_15 = V_1;
		TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* L_16 = ((TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields*)il2cpp_codegen_static_fields_for(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var))->get_GenerateTexture_1();
		NullCheck(L_15);
		L_15->set_serverFiles_14(L_16);
		// LoadList(serverFiles);
		LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * L_17 = V_1;
		LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394 * L_18 = V_1;
		NullCheck(L_18);
		TextureFileU5BU5D_t883F9A32ABC6F033E4CB3DE52B267C86CA5E919B* L_19 = L_18->get_serverFiles_14();
		NullCheck(L_17);
		LoadLibraryPanel_LoadList_mAB71BDBC92DD8B3E5FCE30F951A9AAABD689263B(L_17, L_19, /*hidden argument*/NULL);
		// TexturesServer.SaveCurrentFile();
		TexturesServer_SaveCurrentFile_m6ED026056AAA606513828662C42F38E530C4BC48(/*hidden argument*/NULL);
		// break;
		goto IL_00c2;
	}

IL_008d:
	{
		// if (!TexturesServer.IsLoad && !TexturesServer.IsLoadEnd)
		bool L_20 = ((TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields*)il2cpp_codegen_static_fields_for(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var))->get_IsLoad_3();
		if (L_20)
		{
			goto IL_00a0;
		}
	}
	{
		bool L_21 = ((TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_StaticFields*)il2cpp_codegen_static_fields_for(TexturesServer_tF0E51DF8269011C492FAB6E7CD0C62AC594B0298_il2cpp_TypeInfo_var))->get_IsLoadEnd_2();
		if (L_21)
		{
			goto IL_00a0;
		}
	}
	{
		// TexturesServer.Start();
		TexturesServer_Start_mF17ABA42248EE6E7782E1CCD55123CE67C767FCB(/*hidden argument*/NULL);
	}

IL_00a0:
	{
		// yield return new WaitForSecondsRealtime(2.0f);
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_22 = (WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225(L_22, (2.0f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_22);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00b9:
	{
		__this->set_U3CU3E1__state_0((-1));
		// while (true)
		goto IL_0063;
	}

IL_00c2:
	{
		// yield break;
		return (bool)0;
	}
}
// System.Object Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadTextureWaitU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30AD20615A56147C89D07E4E825A3D1BCA280C41 (U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_Reset_m1F745D6ACC0C15F9630C12F79F928F663ADC18B7 (U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_Reset_m1F745D6ACC0C15F9630C12F79F928F663ADC18B7_RuntimeMethod_var)));
	}
}
// System.Object Program.UI.LoadLibraryPanel/<LoadTextureWait>d__11::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_get_Current_mC8A4904545EADE0AB3EEE97FED4AFE8FECF905A9 (U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.UI.LoadMenu/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m96FB4CD47DAA0FF961F65D090EA7FDDAE16F78E4 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9 * L_0 = (U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9 *)il2cpp_codegen_object_new(U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mD73860545FF53EC49CA9184FCAD0FE2FFF78DEA0(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Program.UI.LoadMenu/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mD73860545FF53EC49CA9184FCAD0FE2FFF78DEA0 (U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Program.UI.LoadMenu/<>c::<Start>b__1_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__1_0_mD72F8ED180F42D31D96869E7F8B707EB1343F546 (U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// loadMenu.onClick.AddListener(delegate { SceneManager.LoadScene(0); });
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(0, /*hidden argument*/NULL);
		// loadMenu.onClick.AddListener(delegate { SceneManager.LoadScene(0); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.UI.MenuPaltira/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m493236BB568A424AD3F78541DC4A613CFBB56A9D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE * L_0 = (U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE *)il2cpp_codegen_object_new(U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m7E18D07EDEB57DA53149F9156B637B1C2A1EDD0B(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Program.UI.MenuPaltira/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m7E18D07EDEB57DA53149F9156B637B1C2A1EDD0B (U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// Palitra Program.UI.MenuPaltira/<>c::<OpenCallBack>b__19_0(ProjectPalitra)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Palitra_t41281A13EF2874D486547B9324202C7D112880B3 * U3CU3Ec_U3COpenCallBackU3Eb__19_0_m646AC0C43508290335E088AAFCFB92BD6DBA8770 (U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE * __this, ProjectPalitra_tE92E7BFB63915D8633D6B7A34DD7D32D2EEFC847 * ___x0, const RuntimeMethod* method)
{
	{
		// p.AddRange(palitraData.Palitras.Select((x) => x.GetPalitra()));
		ProjectPalitra_tE92E7BFB63915D8633D6B7A34DD7D32D2EEFC847 * L_0 = ___x0;
		NullCheck(L_0);
		Palitra_t41281A13EF2874D486547B9324202C7D112880B3 * L_1;
		L_1 = ProjectPalitra_GetPalitra_m426101BEAACA8C9F1E4F78C72028A9716DC1C513(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Palitra Program.UI.MenuPaltira/<>c::<UpdateList>b__24_0(ProjectPalitra)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Palitra_t41281A13EF2874D486547B9324202C7D112880B3 * U3CU3Ec_U3CUpdateListU3Eb__24_0_m60AFB0C0476B0A68B3866EBC613EA173687FA62F (U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE * __this, ProjectPalitra_tE92E7BFB63915D8633D6B7A34DD7D32D2EEFC847 * ___x0, const RuntimeMethod* method)
{
	{
		// palitra.AddRange(palitraData.Palitras.Select((x) => x.GetPalitra()));
		ProjectPalitra_tE92E7BFB63915D8633D6B7A34DD7D32D2EEFC847 * L_0 = ___x0;
		NullCheck(L_0);
		Palitra_t41281A13EF2874D486547B9324202C7D112880B3 * L_1;
		L_1 = ProjectPalitra_GetPalitra_m426101BEAACA8C9F1E4F78C72028A9716DC1C513(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.Editor.Model/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m709C23D26AAB88E6BC592F1D6C56C0974B94275C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F * L_0 = (U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F *)il2cpp_codegen_object_new(U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m8AF44807C9189D0DDBEB1F38D11927CD90DC7247(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Program.Editor.Model/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m8AF44807C9189D0DDBEB1F38D11927CD90DC7247 (U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Mesh Program.Editor.Model/<>c::<GetMeshs>b__14_0(UnityEngine.MeshRenderer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * U3CU3Ec_U3CGetMeshsU3Eb__14_0_m076B9A41934A3E5FC350998E7ABF554A90322A33 (U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F * __this, MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_m4E31C5D3D0490AEE405B54BE9F61802EA425B9DC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return meshRenderers.Select((x) => x.GetComponent<MeshFilter>().mesh).ToArray();
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_0 = ___x0;
		NullCheck(L_0);
		MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A * L_1;
		L_1 = Component_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_m4E31C5D3D0490AEE405B54BE9F61802EA425B9DC(L_0, /*hidden argument*/Component_GetComponent_TisMeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_m4E31C5D3D0490AEE405B54BE9F61802EA425B9DC_RuntimeMethod_var);
		NullCheck(L_1);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_2;
		L_2 = MeshFilter_get_mesh_mFC1DF5AFBC1E4269D08628DB83C954882FD2B417(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Program.Editor.Model/History::get_CurrentStory()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, const RuntimeMethod* method)
{
	{
		// public int CurrentStory { get; private set; }
		int32_t L_0 = __this->get_U3CCurrentStoryU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Program.Editor.Model/History::set_CurrentStory(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2 (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CurrentStory { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentStoryU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void Program.Editor.Model/History::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void History__ctor_mE60B21C617580D10000829815C4762950E4A5A28 (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m94FA9771A56F4CCC6087669FB8221EBB6316096F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private List<Story> stories = new List<Story>();
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_0 = (List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD *)il2cpp_codegen_object_new(List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD_il2cpp_TypeInfo_var);
		List_1__ctor_m94FA9771A56F4CCC6087669FB8221EBB6316096F(L_0, /*hidden argument*/List_1__ctor_m94FA9771A56F4CCC6087669FB8221EBB6316096F_RuntimeMethod_var);
		__this->set_stories_0(L_0);
		// public History()
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// CurrentStory = 0;
		History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2_inline(__this, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Program.Editor.Model/History::NextStory(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void History_NextStory_mD6D584E8C7743EB904F5CF263121ED1BCE876C02 (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___texture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m34D1481BA2C3E15122493C0DE376BFC24A3732EA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5A1278AB54EFEBBD1E0E03AB2677F22D39311C27);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (CurrentStory < stories.Count - 1)
		int32_t L_0;
		L_0 = History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline(__this, /*hidden argument*/NULL);
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_1 = __this->get_stories_0();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_inline(L_1, /*hidden argument*/List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_RuntimeMethod_var);
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)))))
		{
			goto IL_0057;
		}
	}
	{
		// CurrentStory++;
		int32_t L_3;
		L_3 = History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2_inline(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)), /*hidden argument*/NULL);
		// texture.LoadImage(stories[CurrentStory].Save.EncodeToPNG());
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_5 = ___texture0;
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_6 = __this->get_stories_0();
		int32_t L_7;
		L_7 = History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 * L_8;
		L_8 = List_1_get_Item_m34D1481BA2C3E15122493C0DE376BFC24A3732EA_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m34D1481BA2C3E15122493C0DE376BFC24A3732EA_RuntimeMethod_var);
		NullCheck(L_8);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_9 = L_8->get_Save_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10;
		L_10 = ImageConversion_EncodeToPNG_mA598C2969C878ACC5AEA5FDC92C6199EB30D51ED(L_9, /*hidden argument*/NULL);
		bool L_11;
		L_11 = ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477(L_5, L_10, /*hidden argument*/NULL);
		// texture.Apply();
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_12 = ___texture0;
		NullCheck(L_12);
		Texture2D_Apply_m3BB3975288119BA62ED9BE4243F7767EC2F88CA0(L_12, /*hidden argument*/NULL);
		// Debug.Log("Next");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral5A1278AB54EFEBBD1E0E03AB2677F22D39311C27, /*hidden argument*/NULL);
	}

IL_0057:
	{
		// }
		return;
	}
}
// System.Void Program.Editor.Model/History::BreakSory(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void History_BreakSory_mEEAE1220F52A2841F4A37D0BCF265CE79854C2BB (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___texture0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m34D1481BA2C3E15122493C0DE376BFC24A3732EA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4A261EB7E7319776625F5A015EA18053797E6890);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (CurrentStory > 0 && stories.Count > 0)
		int32_t L_0;
		L_0 = History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_1 = __this->get_stories_0();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_inline(L_1, /*hidden argument*/List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_RuntimeMethod_var);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		// CurrentStory--;
		int32_t L_3;
		L_3 = History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2_inline(__this, ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1)), /*hidden argument*/NULL);
		// texture.LoadImage(stories[CurrentStory].Save.EncodeToPNG());
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_5 = ___texture0;
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_6 = __this->get_stories_0();
		int32_t L_7;
		L_7 = History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 * L_8;
		L_8 = List_1_get_Item_m34D1481BA2C3E15122493C0DE376BFC24A3732EA_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m34D1481BA2C3E15122493C0DE376BFC24A3732EA_RuntimeMethod_var);
		NullCheck(L_8);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_9 = L_8->get_Save_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10;
		L_10 = ImageConversion_EncodeToPNG_mA598C2969C878ACC5AEA5FDC92C6199EB30D51ED(L_9, /*hidden argument*/NULL);
		bool L_11;
		L_11 = ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477(L_5, L_10, /*hidden argument*/NULL);
		// texture.Apply();
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_12 = ___texture0;
		NullCheck(L_12);
		Texture2D_Apply_m3BB3975288119BA62ED9BE4243F7767EC2F88CA0(L_12, /*hidden argument*/NULL);
		// Debug.Log("Back");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral4A261EB7E7319776625F5A015EA18053797E6890, /*hidden argument*/NULL);
	}

IL_0059:
	{
		// }
		return;
	}
}
// System.Void Program.Editor.Model/History::AddSotry(Program.Editor.Model/Story)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void History_AddSotry_m7B3C4346D3E540DBB4E3D8F9CFCF4BC8D2DFDAB7 (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 * ___stroy0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mC5F56B296FBB59F233C39F696B4811C2B3F76A18_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAt_m2383E89A18C3A19BFC8BCC1763ADC4321508C883_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral107694947DB47644F8036602F63473486E1ED925);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (stroy != null && stroy.Save)
		Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 * L_0 = ___stroy0;
		if (!L_0)
		{
			goto IL_0081;
		}
	}
	{
		Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 * L_1 = ___stroy0;
		NullCheck(L_1);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_2 = L_1->get_Save_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0081;
		}
	}
	{
		// if (CurrentStory < stories.Count - 1)
		int32_t L_4;
		L_4 = History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline(__this, /*hidden argument*/NULL);
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_5 = __this->get_stories_0();
		NullCheck(L_5);
		int32_t L_6;
		L_6 = List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_inline(L_5, /*hidden argument*/List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_RuntimeMethod_var);
		if ((((int32_t)L_4) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1)))))
		{
			goto IL_0031;
		}
	}
	{
		// ClearStory(CurrentStory);
		int32_t L_7;
		L_7 = History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline(__this, /*hidden argument*/NULL);
		History_ClearStory_m74055B06CDC0551ACF9A841D7F110EAED6B2DC50(__this, L_7, /*hidden argument*/NULL);
	}

IL_0031:
	{
		// stories.Add(stroy);
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_8 = __this->get_stories_0();
		Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 * L_9 = ___stroy0;
		NullCheck(L_8);
		List_1_Add_mC5F56B296FBB59F233C39F696B4811C2B3F76A18(L_8, L_9, /*hidden argument*/List_1_Add_mC5F56B296FBB59F233C39F696B4811C2B3F76A18_RuntimeMethod_var);
		// CurrentStory++;
		int32_t L_10;
		L_10 = History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline(__this, /*hidden argument*/NULL);
		V_0 = L_10;
		int32_t L_11 = V_0;
		History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2_inline(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1)), /*hidden argument*/NULL);
		// if (stories.Count > MaxStory)
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_12 = __this->get_stories_0();
		NullCheck(L_12);
		int32_t L_13;
		L_13 = List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_inline(L_12, /*hidden argument*/List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_RuntimeMethod_var);
		if ((((int32_t)L_13) <= ((int32_t)5)))
		{
			goto IL_0077;
		}
	}
	{
		// CurrentStory--;
		int32_t L_14;
		L_14 = History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline(__this, /*hidden argument*/NULL);
		V_0 = L_14;
		int32_t L_15 = V_0;
		History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2_inline(__this, ((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)1)), /*hidden argument*/NULL);
		// stories.RemoveAt(0);
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_16 = __this->get_stories_0();
		NullCheck(L_16);
		List_1_RemoveAt_m2383E89A18C3A19BFC8BCC1763ADC4321508C883(L_16, 0, /*hidden argument*/List_1_RemoveAt_m2383E89A18C3A19BFC8BCC1763ADC4321508C883_RuntimeMethod_var);
	}

IL_0077:
	{
		// Debug.Log("Add");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral107694947DB47644F8036602F63473486E1ED925, /*hidden argument*/NULL);
	}

IL_0081:
	{
		// }
		return;
	}
}
// System.Void Program.Editor.Model/History::ClearStory()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void History_ClearStory_m31DAD42847EB04B4B3DC76CBB4CD574D652B0845 (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m95A9EDACA9EDF273FB8BBAEE846E7738A872EA00_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CurrentStory = 0;
		History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2_inline(__this, 0, /*hidden argument*/NULL);
		// stories.Clear();
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_0 = __this->get_stories_0();
		NullCheck(L_0);
		List_1_Clear_m95A9EDACA9EDF273FB8BBAEE846E7738A872EA00(L_0, /*hidden argument*/List_1_Clear_m95A9EDACA9EDF273FB8BBAEE846E7738A872EA00_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Program.Editor.Model/History::ClearStory(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void History_ClearStory_m74055B06CDC0551ACF9A841D7F110EAED6B2DC50 (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, int32_t ___num0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveRange_m0266B9170CB4F7A00AFF336AA360766D0F6FAF66_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// stories.RemoveRange(num + 1, stories.Count - num - 1);
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_0 = __this->get_stories_0();
		int32_t L_1 = ___num0;
		List_1_t4FCAF3AB498137CE543A067975D00EA99BBE2FBD * L_2 = __this->get_stories_0();
		NullCheck(L_2);
		int32_t L_3;
		L_3 = List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_inline(L_2, /*hidden argument*/List_1_get_Count_mA3D5E8A52DB423B714694152F2BF0D92376E0F9E_RuntimeMethod_var);
		int32_t L_4 = ___num0;
		NullCheck(L_0);
		List_1_RemoveRange_m0266B9170CB4F7A00AFF336AA360766D0F6FAF66(L_0, ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1)), ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)L_4)), (int32_t)1)), /*hidden argument*/List_1_RemoveRange_m0266B9170CB4F7A00AFF336AA360766D0F6FAF66_RuntimeMethod_var);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.Editor.Model/Story::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Story__ctor_mED39062554A90BF1D8888C0729EBCABF22F6BD56 (Story_t26EF0C6BD27E9DD5220287EAF718E6047005CD22 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.Option.OptionController/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mF7D5A7EBF23935B269753913300E79FE8CACC96D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E * L_0 = (U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E *)il2cpp_codegen_object_new(U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m50BC0DBD2EC29FE4E39FD44C84748386CC5260E9(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Program.Option.OptionController/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m50BC0DBD2EC29FE4E39FD44C84748386CC5260E9 (U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Program.Option.OptionController/<>c::<OnDestroy>b__3_0(Program.Option.OptionObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CU3Ec_U3COnDestroyU3Eb__3_0_mF4334CBCDBCE27553F3885D1509075FE41E78823 (U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E * __this, OptionObject_t108F4312E931F36F66EF6A91E95988338600EC8F * ___x0, const RuntimeMethod* method)
{
	{
		// fileOption.Options = options.Select((x) => x.GetOption()).ToArray();
		OptionObject_t108F4312E931F36F66EF6A91E95988338600EC8F * L_0 = ___x0;
		NullCheck(L_0);
		RuntimeObject * L_1;
		L_1 = VirtFuncInvoker0< RuntimeObject * >::Invoke(4 /* System.Object Program.Option.OptionObject::GetOption() */, L_0);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.UI.PalitraItem/<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_mA560F3F96A665FBA0BC86AC587B274466E6760FF (U3CU3Ec__DisplayClass2_0_tD3927C92A4B65FB4EC5119DE2D09429DBBB6B3E9 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Program.UI.PalitraItem/<>c__DisplayClass2_0::<Init>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CInitU3Eb__0_mE57306A6603FECA78567E73E3081B6B51FBA89AE (U3CU3Ec__DisplayClass2_0_tD3927C92A4B65FB4EC5119DE2D09429DBBB6B3E9 * __this, const RuntimeMethod* method)
{
	{
		// button.onClick.AddListener(delegate { menuPaltira.NewTakePalitra(palitra); });
		MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A * L_0 = __this->get_menuPaltira_0();
		Palitra_t41281A13EF2874D486547B9324202C7D112880B3 * L_1 = __this->get_palitra_1();
		NullCheck(L_0);
		MenuPaltira_NewTakePalitra_m8504090ACB63ECC3B5A1DFD458F6995373A8A5E1(L_0, L_1, /*hidden argument*/NULL);
		// button.onClick.AddListener(delegate { menuPaltira.NewTakePalitra(palitra); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.UI.PalitraPanel/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m86622DB45A148F39C2F7FA7FFD5621382DB60BCF (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * L_0 = (U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 *)il2cpp_codegen_object_new(U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mF7D1199BFF2C780D70E9D73848C8D33ADFDA4F72(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Program.UI.PalitraPanel/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mF7D1199BFF2C780D70E9D73848C8D33ADFDA4F72 (U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__39_0_m70562F083C46F792C0472CB5D1679B6C75A69F2E (U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * __this, const RuntimeMethod* method)
{
	{
		// backHistory.onClick.AddListener(delegate { History.BackHistory(); });
		HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB * L_0;
		L_0 = PalitraPanel_get_History_mD749E9F50722D5B8D5110C265F2CDAEAFDD881AB_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		HistoryButton_BackHistory_m87C5E919DA71B99989BBF0FB67EFFB6DA65C9C8B(L_0, /*hidden argument*/NULL);
		// backHistory.onClick.AddListener(delegate { History.BackHistory(); });
		return;
	}
}
// System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__39_1_mF52496C719D1807D07433C2CCE00C1F67145154F (U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * __this, const RuntimeMethod* method)
{
	{
		// nextHistory.onClick.AddListener(delegate { History.NextHistory(); });
		HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB * L_0;
		L_0 = PalitraPanel_get_History_mD749E9F50722D5B8D5110C265F2CDAEAFDD881AB_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		HistoryButton_NextHistory_m809836890A22F382AD96F564748FBB68C2A3934F(L_0, /*hidden argument*/NULL);
		// nextHistory.onClick.AddListener(delegate { History.NextHistory(); });
		return;
	}
}
// System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__39_2_m490C9C997BDCC3AB3927EB28E491C4BB6BB1565E (U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * __this, const RuntimeMethod* method)
{
	{
		// paint.onClick.AddListener(delegate { Instrument.TakePaint(); });
		Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * L_0;
		L_0 = PalitraPanel_get_Instrument_m7F79D1932B2C71BB7B236D976366BFDC4965020C_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		Instruments_TakePaint_mF5A5660834B59D12BEE85DBECE098C2B9A518867(L_0, /*hidden argument*/NULL);
		// paint.onClick.AddListener(delegate { Instrument.TakePaint(); });
		return;
	}
}
// System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__39_3_mF141EB1A932A2FBBD567E52EB7222A0486E95518 (U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * __this, const RuntimeMethod* method)
{
	{
		// takeColor.onClick.AddListener(delegate { Instrument.TakeColor(); });
		Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * L_0;
		L_0 = PalitraPanel_get_Instrument_m7F79D1932B2C71BB7B236D976366BFDC4965020C_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		Instruments_TakeColor_m1286AF6ABD47C645C9CA73EE5E37806B59DAF8AA(L_0, /*hidden argument*/NULL);
		// takeColor.onClick.AddListener(delegate { Instrument.TakeColor(); });
		return;
	}
}
// System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_4()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__39_4_mCB1E9C476F4ADF5714F7A25C2FFDF35B4A90005A (U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * __this, const RuntimeMethod* method)
{
	{
		// fillColor.onClick.AddListener(delegate { Instrument.TakeFillColor(); });
		Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * L_0;
		L_0 = PalitraPanel_get_Instrument_m7F79D1932B2C71BB7B236D976366BFDC4965020C_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		Instruments_TakeFillColor_mB3AE6CBE53415CD455CF250626B7B7BA72739B40(L_0, /*hidden argument*/NULL);
		// fillColor.onClick.AddListener(delegate { Instrument.TakeFillColor(); });
		return;
	}
}
// System.Void Program.UI.PalitraPanel/<>c::<Start>b__39_5()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__39_5_m46A52B92D10C4136C07278213C21B8D22D463A32 (U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0 * __this, const RuntimeMethod* method)
{
	{
		// erase.onClick.AddListener(delegate { Instrument.TakeErase(); });
		Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * L_0;
		L_0 = PalitraPanel_get_Instrument_m7F79D1932B2C71BB7B236D976366BFDC4965020C_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		Instruments_TakeErase_m556D71FCB88F4B8D858379D644A22DABFD6E47BE(L_0, /*hidden argument*/NULL);
		// erase.onClick.AddListener(delegate { Instrument.TakeErase(); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.UI.ProgramUI/CorrectCanvance::SetScale(UnityEngine.UI.CanvasScaler,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorrectCanvance_SetScale_mA56FF70FB43A05EC9853C4F876EE8D34779434DF (CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1 * ___canvas0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___scale1, const RuntimeMethod* method)
{
	{
		// canvas.referenceResolution = scale;
		CanvasScaler_t8EF50255FD2913C31BD62B14476C994F64D711F1 * L_0 = ___canvas0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1 = ___scale1;
		NullCheck(L_0);
		CanvasScaler_set_referenceResolution_mD3200E76103B16C8064B6E48595EFC9C5BDCB719(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FilerServer.Texture/Pixel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pixel__ctor_mC565487D5E165D777AC9E446E62C4A01C3844BE3 (Pixel_t626CE4860392029BBF83CD2ADA27B179C3B93203 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Program.Menu.UIMenu/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m9DDBB15F18EE8E10C6D13FE76ED8E43EC5F4732A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D * L_0 = (U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D *)il2cpp_codegen_object_new(U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mC295447BB5C8F7D4EE7E0278E80DF0EC99E1264D(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Program.Menu.UIMenu/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mC295447BB5C8F7D4EE7E0278E80DF0EC99E1264D (U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Program.Menu.UIMenu/<>c::<Start>b__3_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__3_0_m093F84DB5C5B1B508B2554397AC744290518253F (U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D * __this, const RuntimeMethod* method)
{
	{
		// loadProgram.onClick.AddListener(delegate { BaseMenu.Singleton.LoadEditor(); });
		BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 * L_0;
		L_0 = BaseMenu_get_Singleton_m069EC972BD638A8FB525054E1611FB620CA75AA5_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		BaseMenu_LoadEditor_mB6D54A1389D8C9489397FAA0E85E65BB159AA571(L_0, /*hidden argument*/NULL);
		// loadProgram.onClick.AddListener(delegate { BaseMenu.Singleton.LoadEditor(); });
		return;
	}
}
// System.Void Program.Menu.UIMenu/<>c::<Start>b__3_1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__3_1_mC0EBF3283D624C5905EE1BB460955B71E640231E (U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D * __this, const RuntimeMethod* method)
{
	{
		// exit.onClick.AddListener(delegate { BaseMenu.Singleton.ExitProgram(); });
		BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 * L_0;
		L_0 = BaseMenu_get_Singleton_m069EC972BD638A8FB525054E1611FB620CA75AA5_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		BaseMenu_ExitProgram_m35F3098E90075D44F5141D44A15A2A730C27B7B5(L_0, /*hidden argument*/NULL);
		// exit.onClick.AddListener(delegate { BaseMenu.Singleton.ExitProgram(); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateU3Ed__8_MoveNext_mDDFCC9C7E5C00438CA37A40742B4CF2EF430368A (U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_m77090A1636BA1001FD6018EED4EEC9FEAE973E0C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseProcces_U3CUpdateU3Eb__8_0_mAD6652D6C6523D25840710F6A2E3C0C01767203F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * V_1 = NULL;
	TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Exception_t * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = V_0;
			if (!L_2)
			{
				goto IL_0051;
			}
		}

IL_0011:
		{
			// await Task.Run(() => { try { CallUpdate(); } catch { } isEnd = true; });
			BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA * L_3 = V_1;
			Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_4 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
			Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_4, L_3, (intptr_t)((intptr_t)BaseProcces_U3CUpdateU3Eb__8_0_mAD6652D6C6523D25840710F6A2E3C0C01767203F_RuntimeMethod_var), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_il2cpp_TypeInfo_var);
			Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * L_5;
			L_5 = Task_Run_m77F41B8D89AFD69BE94888BC2CAD5E3183A79B8D(L_4, /*hidden argument*/NULL);
			NullCheck(L_5);
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  L_6;
			L_6 = Task_GetAwaiter_m1FF7528A8FE13F79207DFE970F642078EF6B1260(L_5, /*hidden argument*/NULL);
			V_2 = L_6;
			bool L_7;
			L_7 = TaskAwaiter_get_IsCompleted_m6F97613C55E505B5664C3C0CFC4677D296EAA8BC((TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *)(&V_2), /*hidden argument*/NULL);
			if (L_7)
			{
				goto IL_006d;
			}
		}

IL_0031:
		{
			int32_t L_8 = 0;
			V_0 = L_8;
			__this->set_U3CU3E1__state_0(L_8);
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  L_9 = V_2;
			__this->set_U3CU3Eu__1_3(L_9);
			AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * L_10 = __this->get_address_of_U3CU3Et__builder_1();
			AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_m77090A1636BA1001FD6018EED4EEC9FEAE973E0C((AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *)L_10, (TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *)(&V_2), (U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 *)__this, /*hidden argument*/AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_m77090A1636BA1001FD6018EED4EEC9FEAE973E0C_RuntimeMethod_var);
			goto IL_00a0;
		}

IL_0051:
		{
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  L_11 = __this->get_U3CU3Eu__1_3();
			V_2 = L_11;
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * L_12 = __this->get_address_of_U3CU3Eu__1_3();
			il2cpp_codegen_initobj(L_12, sizeof(TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C ));
			int32_t L_13 = (-1);
			V_0 = L_13;
			__this->set_U3CU3E1__state_0(L_13);
		}

IL_006d:
		{
			TaskAwaiter_GetResult_m578EEFEC4DD1AE5E77C899B8BAA3825EB79D1330((TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *)(&V_2), /*hidden argument*/NULL);
			goto IL_008d;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0076;
		throw e;
	}

CATCH_0076:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t *)__exception_local);
		__this->set_U3CU3E1__state_0(((int32_t)-2));
		AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * L_14 = __this->get_address_of_U3CU3Et__builder_1();
		Exception_t * L_15 = V_3;
		AsyncVoidMethodBuilder_SetException_m16372173CEA3031B4CB9B8D15DA97C457F835155((AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *)L_14, L_15, /*hidden argument*/NULL);
		goto IL_00a0;
	} // end catch (depth: 1)

IL_008d:
	{
		// }
		__this->set_U3CU3E1__state_0(((int32_t)-2));
		AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * L_16 = __this->get_address_of_U3CU3Et__builder_1();
		AsyncVoidMethodBuilder_SetResult_m901385B56EBE93E472A77EA48F61E4F498F3E00E((AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *)L_16, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		return;
	}
}
IL2CPP_EXTERN_C  void U3CUpdateU3Ed__8_MoveNext_mDDFCC9C7E5C00438CA37A40742B4CF2EF430368A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 * _thisAdjusted = reinterpret_cast<U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 *>(__this + _offset);
	U3CUpdateU3Ed__8_MoveNext_mDDFCC9C7E5C00438CA37A40742B4CF2EF430368A(_thisAdjusted, method);
}
// System.Void FilerServer.Base.BaseConnect/BaseProcces/<Update>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateU3Ed__8_SetStateMachine_m7CB0536971DEF64B4EDF5E17A72BC6F20ACC8D6B (U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 * __this, RuntimeObject* ___stateMachine0, const RuntimeMethod* method)
{
	{
		AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * L_0 = __this->get_address_of_U3CU3Et__builder_1();
		RuntimeObject* L_1 = ___stateMachine0;
		AsyncVoidMethodBuilder_SetStateMachine_m1ED99BE03B146D8A7117E299EBA5D74999EB52D7((AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *)L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void U3CUpdateU3Ed__8_SetStateMachine_m7CB0536971DEF64B4EDF5E17A72BC6F20ACC8D6B_AdjustorThunk (RuntimeObject * __this, RuntimeObject* ___stateMachine0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 * _thisAdjusted = reinterpret_cast<U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708 *>(__this + _offset);
	U3CUpdateU3Ed__8_SetStateMachine_m7CB0536971DEF64B4EDF5E17A72BC6F20ACC8D6B(_thisAdjusted, ___stateMachine0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Socket_get_Connected_m6E8C88AC69580EF7782514CD48754D4D180D9CE0_inline (Socket_tD9721140F91BE95BA05B87DD26A855B215D84D09 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_is_connected_19();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9 * BodyPanel_get_UVEditor_mB67B5BA8BC6C9B3D828712FFE97D6D5E3209E8CE_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static UVPanel UVEditor { get; private set; }
		UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9 * L_0 = ((BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_StaticFields*)il2cpp_codegen_static_fields_for(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_il2cpp_TypeInfo_var))->get_U3CUVEditorU3Ek__BackingField_22();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08 * BodyPanel_get_StartRender_mAAB7A1D9289510C59929934F34C22C74FD0BE8AF_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Render StartRender { get; private set; }
		Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08 * L_0 = ((BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_StaticFields*)il2cpp_codegen_static_fields_for(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_il2cpp_TypeInfo_var))->get_U3CStartRenderU3Ek__BackingField_21();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * BodyPanel_get_LayerDown_m29403D6F813A24BCB8A9239FDD6573D75D3C9D7D_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static DownLayer LayerDown { get; private set; }
		DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * L_0 = ((BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_StaticFields*)il2cpp_codegen_static_fields_for(BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_il2cpp_TypeInfo_var))->get_U3CLayerDownU3Ek__BackingField_19();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool DownLayer_get_Enable_m49AAD772F67A74CE9EA1E2EF80369E7963C9F42A_inline (DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C * __this, const RuntimeMethod* method)
{
	{
		// return _enable;
		bool L_0 = __this->get__enable_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F * AudioController_get_Singleton_m23473DECF654EB8C9EF43BD85282CEFBF4023594_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static AudioController Singleton { get; private set; }
		AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F * L_0 = ((AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_StaticFields*)il2cpp_codegen_static_fields_for(AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_il2cpp_TypeInfo_var))->get_U3CSingletonU3Ek__BackingField_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2_inline (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CurrentStory { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentStoryU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE_inline (History_tB9376052A6C8C07D431414CC16E050FE0EE9A156 * __this, const RuntimeMethod* method)
{
	{
		// public int CurrentStory { get; private set; }
		int32_t L_0 = __this->get_U3CCurrentStoryU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB * PalitraPanel_get_History_mD749E9F50722D5B8D5110C265F2CDAEAFDD881AB_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static HistoryButton History { get; private set; }
		HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB * L_0 = ((PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_StaticFields*)il2cpp_codegen_static_fields_for(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_il2cpp_TypeInfo_var))->get_U3CHistoryU3Ek__BackingField_28();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * PalitraPanel_get_Instrument_m7F79D1932B2C71BB7B236D976366BFDC4965020C_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Instruments Instrument { get; private set; }
		Instruments_tF75DA728107310E221C75AD3538952EAEE98442E * L_0 = ((PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_StaticFields*)il2cpp_codegen_static_fields_for(PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_il2cpp_TypeInfo_var))->get_U3CInstrumentU3Ek__BackingField_26();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 * BaseMenu_get_Singleton_m069EC972BD638A8FB525054E1611FB620CA75AA5_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static BaseMenu Singleton { get; private set; }
		BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475 * L_0 = ((BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_StaticFields*)il2cpp_codegen_static_fields_for(BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_il2cpp_TypeInfo_var))->get_U3CSingletonU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
