﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.ConditionalAttribute
struct ConditionalAttribute_t5DD558ED67ECF65952A82B94A75C6E8E121B2D8C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// Sirenix.Serialization.EmittedFormatterAttribute
struct EmittedFormatterAttribute_tE1AE94E0FB78B936B1C1F9881FCF29CF5D06B05F;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// JetBrains.Annotations.MeansImplicitUseAttribute
struct MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute
struct RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695;
// Sirenix.Serialization.RegisterFormatterAttribute
struct RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E;
// Sirenix.Serialization.RegisterFormatterLocatorAttribute
struct RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute
struct ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* AnimationCurveFormatter_t9DC9E69CAB99C0372B77A4E44ABA8F9DA3AF5A1F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ArrayFormatterLocator_tE729954088276BEB3F07AA62648858D863054A22_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ArrayListFormatter_t9FBC2F6E80D8A6F84CB1A708FA8C39DA5F22F99A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* BoundsFormatter_t1B2A06125AA025F24F0E1F6252DD8FAB8E01AE97_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Color32Formatter_t7DEE99B33D9AE2C983F10C42B80F051D4F98DA7D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ColorBlockFormatterLocator_t6B2953245798D46E38C6753D852675D05D6E7550_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ColorFormatter_t434DDCE2F36DC38B1210645C514568A3C86D0B71_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* CoroutineFormatter_tED36398512E10F0E67D346FE78A2ECA8BBC205D4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DateTimeFormatter_t6DFF5CDE2EA7969DA0CBAA7C4BD5D943F797D1EC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DateTimeOffsetFormatter_t5ACAD25084B09C83E80297265E7D10B7B702B6A4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DelegateFormatterLocator_t1F95C68737C71073D1978B79002A85861CDB4776_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DerivedDictionaryFormatter_3_t578ED9F90427BC2B7F87C9FDBFE3C5A75E8B1478_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DictionaryFormatter_2_t6522868CAF7FAD86B0AFA27BA528085A3B169C67_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DoubleLookupDictionaryFormatter_3_t913CE2150584748FEBFF7AE5EDEC8BBD88A0AF9E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* GenericCollectionFormatterLocator_t48BA7D2538EBF9F7E967BFFACB692A98D67E60A5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* GradientAlphaKeyFormatter_t9F644CAF171CFE023C10F56527EED83464F2F21C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* GradientColorKeyFormatter_t320370C3D0E68394741BC4F5CDB76EE987B9317F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* GradientFormatter_tC0424F4B8E1BA9B60B24281D221F942405ECAF24_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* HashSetFormatter_1_t658148EF16DB946F04F64D641BA39C4882A2A9BC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ISerializableFormatterLocator_tE053B0B051AF5CAE7D582DC1BD4497B98D500900_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* KeyValuePairFormatter_2_t119AB3A285F27C14CDDB5E97F79601F4440C3E6A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* KeyframeFormatter_t025E7661A71BBA63F454C17C815D23D59F33925F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* LayerMaskFormatter_tF6156F275D61174AF28F42B2C6659682E81EBC11_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ListFormatter_1_t68B3163F162A59657C34B769D0C62E9822CE42EF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MethodInfoFormatter_1_tCFCFEA4E4C56739A9A307DE0990FC5292C7F41C6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* NullableFormatter_1_t7CD1897D46FD139C0A8569AAFD48C14A97E16E75_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* QuaternionFormatter_t191A38F016652F309037E2E003738116D78169FD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* QueueFormatter_2_t4297CDB0AC6687D76CD40D35B2BF5EF5168E2828_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* RectFormatter_t598340E9CE1C921FA3D236C79060B845E1A3A497_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* SelfFormatterLocator_t558C4FCFD0AFF5C762927DE05F048F1A21D985D8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* StackFormatter_2_t0D1441A8DFEAF653801F3AD686415DCE2DEB4124_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* TimeSpanFormatter_tDFA6395F0CFBF123A2115E2B639863CEEC7CB94A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* TypeFormatterLocator_tDE34C82E31477509FE4EB87F19ABE264041BF7D3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* UnityEventFormatter_1_t9622DFCC4A76BCC826987EEF0843DA0CA8C6E910_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector2DictionaryKeyPathProvider_t158CEC5A7B7F1342BF2A88B71C2E013A629595F5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector2Formatter_t1881340AF246501FDEA5B2D98320DCC6391EA475_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector3DictionaryKeyPathProvider_t6C5E1C3D56CF6B258B1F02CEABB034B2CCF27361_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector3Formatter_t99EEACDDE65C8FFEEA8FC3E89612F2DBFCFB4B89_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector4DictionaryKeyPathProvider_t585894B6BD3E96C7579DA424BA43519171CC187A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector4Formatter_tB2CFF78C205DB67804188702754E7AA2C50A34E9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* VersionFormatter_t63B69DDE17B794A9157CA14106C21D566CD9818C_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct  AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct  AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct  AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct  AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct  AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.CLSCompliantAttribute
struct  CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.CLSCompliantAttribute::m_compliant
	bool ___m_compliant_0;

public:
	inline static int32_t get_offset_of_m_compliant_0() { return static_cast<int32_t>(offsetof(CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249, ___m_compliant_0)); }
	inline bool get_m_compliant_0() const { return ___m_compliant_0; }
	inline bool* get_address_of_m_compliant_0() { return &___m_compliant_0; }
	inline void set_m_compliant_0(bool value)
	{
		___m_compliant_0 = value;
	}
};


// System.Runtime.InteropServices.ComVisibleAttribute
struct  ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::_val
	bool ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A, ____val_0)); }
	inline bool get__val_0() const { return ____val_0; }
	inline bool* get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(bool value)
	{
		____val_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.ConditionalAttribute
struct  ConditionalAttribute_t5DD558ED67ECF65952A82B94A75C6E8E121B2D8C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Diagnostics.ConditionalAttribute::m_conditionString
	String_t* ___m_conditionString_0;

public:
	inline static int32_t get_offset_of_m_conditionString_0() { return static_cast<int32_t>(offsetof(ConditionalAttribute_t5DD558ED67ECF65952A82B94A75C6E8E121B2D8C, ___m_conditionString_0)); }
	inline String_t* get_m_conditionString_0() const { return ___m_conditionString_0; }
	inline String_t** get_address_of_m_conditionString_0() { return &___m_conditionString_0; }
	inline void set_m_conditionString_0(String_t* value)
	{
		___m_conditionString_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_conditionString_0), (void*)value);
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct  DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// Sirenix.Serialization.EmittedFormatterAttribute
struct  EmittedFormatterAttribute_tE1AE94E0FB78B936B1C1F9881FCF29CF5D06B05F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.InteropServices.GuidAttribute
struct  GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.InteropServices.GuidAttribute::_val
	String_t* ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063, ____val_0)); }
	inline String_t* get__val_0() const { return ____val_0; }
	inline String_t** get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(String_t* value)
	{
		____val_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____val_0), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct  HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ObsoleteAttribute
struct  ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.ParamArrayAttribute
struct  ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Scripting.PreserveAttribute
struct  PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute
struct  RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute::ProviderType
	Type_t * ___ProviderType_0;

public:
	inline static int32_t get_offset_of_ProviderType_0() { return static_cast<int32_t>(offsetof(RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695, ___ProviderType_0)); }
	inline Type_t * get_ProviderType_0() const { return ___ProviderType_0; }
	inline Type_t ** get_address_of_ProviderType_0() { return &___ProviderType_0; }
	inline void set_ProviderType_0(Type_t * value)
	{
		___ProviderType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ProviderType_0), (void*)value);
	}
};


// Sirenix.Serialization.RegisterFormatterAttribute
struct  RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type Sirenix.Serialization.RegisterFormatterAttribute::<FormatterType>k__BackingField
	Type_t * ___U3CFormatterTypeU3Ek__BackingField_0;
	// System.Int32 Sirenix.Serialization.RegisterFormatterAttribute::<Priority>k__BackingField
	int32_t ___U3CPriorityU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFormatterTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E, ___U3CFormatterTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CFormatterTypeU3Ek__BackingField_0() const { return ___U3CFormatterTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CFormatterTypeU3Ek__BackingField_0() { return &___U3CFormatterTypeU3Ek__BackingField_0; }
	inline void set_U3CFormatterTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CFormatterTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFormatterTypeU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E, ___U3CPriorityU3Ek__BackingField_1)); }
	inline int32_t get_U3CPriorityU3Ek__BackingField_1() const { return ___U3CPriorityU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPriorityU3Ek__BackingField_1() { return &___U3CPriorityU3Ek__BackingField_1; }
	inline void set_U3CPriorityU3Ek__BackingField_1(int32_t value)
	{
		___U3CPriorityU3Ek__BackingField_1 = value;
	}
};


// Sirenix.Serialization.RegisterFormatterLocatorAttribute
struct  RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type Sirenix.Serialization.RegisterFormatterLocatorAttribute::<FormatterLocatorType>k__BackingField
	Type_t * ___U3CFormatterLocatorTypeU3Ek__BackingField_0;
	// System.Int32 Sirenix.Serialization.RegisterFormatterLocatorAttribute::<Priority>k__BackingField
	int32_t ___U3CPriorityU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFormatterLocatorTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300, ___U3CFormatterLocatorTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CFormatterLocatorTypeU3Ek__BackingField_0() const { return ___U3CFormatterLocatorTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CFormatterLocatorTypeU3Ek__BackingField_0() { return &___U3CFormatterLocatorTypeU3Ek__BackingField_0; }
	inline void set_U3CFormatterLocatorTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CFormatterLocatorTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFormatterLocatorTypeU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300, ___U3CPriorityU3Ek__BackingField_1)); }
	inline int32_t get_U3CPriorityU3Ek__BackingField_1() const { return ___U3CPriorityU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPriorityU3Ek__BackingField_1() { return &___U3CPriorityU3Ek__BackingField_1; }
	inline void set_U3CPriorityU3Ek__BackingField_1(int32_t value)
	{
		___U3CPriorityU3Ek__BackingField_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute
struct  ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct  AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableState
struct  DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091 
{
public:
	// System.Int32 System.Diagnostics.DebuggerBrowsableState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// JetBrains.Annotations.ImplicitUseKindFlags
struct  ImplicitUseKindFlags_t9E7B1B7981A84EE60A1814447FAC4D8A90E1FDD2 
{
public:
	// System.Int32 JetBrains.Annotations.ImplicitUseKindFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImplicitUseKindFlags_t9E7B1B7981A84EE60A1814447FAC4D8A90E1FDD2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// JetBrains.Annotations.ImplicitUseTargetFlags
struct  ImplicitUseTargetFlags_t4DEA69C7F55B58E0A400DA7518460D0656125124 
{
public:
	// System.Int32 JetBrains.Annotations.ImplicitUseTargetFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImplicitUseTargetFlags_t4DEA69C7F55B58E0A400DA7518460D0656125124, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RuntimeInitializeLoadType
struct  RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct  AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableAttribute
struct  DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggerBrowsableState System.Diagnostics.DebuggerBrowsableAttribute::state
	int32_t ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}
};


// JetBrains.Annotations.MeansImplicitUseAttribute
struct  MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// JetBrains.Annotations.ImplicitUseKindFlags JetBrains.Annotations.MeansImplicitUseAttribute::<UseKindFlags>k__BackingField
	int32_t ___U3CUseKindFlagsU3Ek__BackingField_0;
	// JetBrains.Annotations.ImplicitUseTargetFlags JetBrains.Annotations.MeansImplicitUseAttribute::<TargetFlags>k__BackingField
	int32_t ___U3CTargetFlagsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUseKindFlagsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A, ___U3CUseKindFlagsU3Ek__BackingField_0)); }
	inline int32_t get_U3CUseKindFlagsU3Ek__BackingField_0() const { return ___U3CUseKindFlagsU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CUseKindFlagsU3Ek__BackingField_0() { return &___U3CUseKindFlagsU3Ek__BackingField_0; }
	inline void set_U3CUseKindFlagsU3Ek__BackingField_0(int32_t value)
	{
		___U3CUseKindFlagsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTargetFlagsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A, ___U3CTargetFlagsU3Ek__BackingField_1)); }
	inline int32_t get_U3CTargetFlagsU3Ek__BackingField_1() const { return ___U3CTargetFlagsU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTargetFlagsU3Ek__BackingField_1() { return &___U3CTargetFlagsU3Ek__BackingField_1; }
	inline void set_U3CTargetFlagsU3Ek__BackingField_1(int32_t value)
	{
		___U3CTargetFlagsU3Ek__BackingField_1 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct  RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void Sirenix.Serialization.RegisterFormatterAttribute::.ctor(System.Type,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * __this, Type_t * ___formatterType0, int32_t ___priority1, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.RegisterFormatterLocatorAttribute::.ctor(System.Type,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RegisterFormatterLocatorAttribute__ctor_mFE77FF1296D532E4B067CF3A7313355B258F49CF (RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 * __this, Type_t * ___formatterLocatorType0, int32_t ___priority1, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270 (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * __this, bool ___isCompliant0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RegisterDictionaryKeyPathProviderAttribute__ctor_m72BA975E1E2C3F05E19BDD08BA797D45385953D0 (RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695 * __this, Type_t * ___providerType0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * __this, String_t* ___guid0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172 (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * __this, bool ___visibility0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerBrowsableAttribute::.ctor(System.Diagnostics.DebuggerBrowsableState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5 (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * __this, int32_t ___state0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ShowOdinSerializedPropertiesInInspectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m6F540B2A92D616275408AEB55BC97B1500F6C9B3 (ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, bool ___error1, const RuntimeMethod* method);
// System.Void Sirenix.Serialization.EmittedFormatterAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmittedFormatterAttribute__ctor_m606F2E7D04ECA7FB692313F4893305F827FA8455 (EmittedFormatterAttribute_tE1AE94E0FB78B936B1C1F9881FCF29CF5D06B05F * __this, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute__ctor_m551D20F17D1290E1F1D5CD516242DEFBBC67B7A4 (MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.ConditionalAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConditionalAttribute__ctor_m43C71F47F8ED8FDF9A11FB20E8916C3737DD66AF (ConditionalAttribute_t5DD558ED67ECF65952A82B94A75C6E8E121B2D8C * __this, String_t* ___conditionString0, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
static void Sirenix_Serialization_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurveFormatter_t9DC9E69CAB99C0372B77A4E44ABA8F9DA3AF5A1F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArrayFormatterLocator_tE729954088276BEB3F07AA62648858D863054A22_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArrayListFormatter_t9FBC2F6E80D8A6F84CB1A708FA8C39DA5F22F99A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BoundsFormatter_t1B2A06125AA025F24F0E1F6252DD8FAB8E01AE97_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Color32Formatter_t7DEE99B33D9AE2C983F10C42B80F051D4F98DA7D_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorBlockFormatterLocator_t6B2953245798D46E38C6753D852675D05D6E7550_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorFormatter_t434DDCE2F36DC38B1210645C514568A3C86D0B71_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CoroutineFormatter_tED36398512E10F0E67D346FE78A2ECA8BBC205D4_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTimeFormatter_t6DFF5CDE2EA7969DA0CBAA7C4BD5D943F797D1EC_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTimeOffsetFormatter_t5ACAD25084B09C83E80297265E7D10B7B702B6A4_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DelegateFormatterLocator_t1F95C68737C71073D1978B79002A85861CDB4776_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DerivedDictionaryFormatter_3_t578ED9F90427BC2B7F87C9FDBFE3C5A75E8B1478_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DictionaryFormatter_2_t6522868CAF7FAD86B0AFA27BA528085A3B169C67_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DoubleLookupDictionaryFormatter_3_t913CE2150584748FEBFF7AE5EDEC8BBD88A0AF9E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GenericCollectionFormatterLocator_t48BA7D2538EBF9F7E967BFFACB692A98D67E60A5_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GradientAlphaKeyFormatter_t9F644CAF171CFE023C10F56527EED83464F2F21C_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GradientColorKeyFormatter_t320370C3D0E68394741BC4F5CDB76EE987B9317F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GradientFormatter_tC0424F4B8E1BA9B60B24281D221F942405ECAF24_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSetFormatter_1_t658148EF16DB946F04F64D641BA39C4882A2A9BC_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ISerializableFormatterLocator_tE053B0B051AF5CAE7D582DC1BD4497B98D500900_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePairFormatter_2_t119AB3A285F27C14CDDB5E97F79601F4440C3E6A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeFormatter_t025E7661A71BBA63F454C17C815D23D59F33925F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LayerMaskFormatter_tF6156F275D61174AF28F42B2C6659682E81EBC11_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListFormatter_1_t68B3163F162A59657C34B769D0C62E9822CE42EF_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MethodInfoFormatter_1_tCFCFEA4E4C56739A9A307DE0990FC5292C7F41C6_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NullableFormatter_1_t7CD1897D46FD139C0A8569AAFD48C14A97E16E75_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&QuaternionFormatter_t191A38F016652F309037E2E003738116D78169FD_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&QueueFormatter_2_t4297CDB0AC6687D76CD40D35B2BF5EF5168E2828_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectFormatter_t598340E9CE1C921FA3D236C79060B845E1A3A497_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SelfFormatterLocator_t558C4FCFD0AFF5C762927DE05F048F1A21D985D8_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StackFormatter_2_t0D1441A8DFEAF653801F3AD686415DCE2DEB4124_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TimeSpanFormatter_tDFA6395F0CFBF123A2115E2B639863CEEC7CB94A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeFormatterLocator_tDE34C82E31477509FE4EB87F19ABE264041BF7D3_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEventFormatter_1_t9622DFCC4A76BCC826987EEF0843DA0CA8C6E910_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2DictionaryKeyPathProvider_t158CEC5A7B7F1342BF2A88B71C2E013A629595F5_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2Formatter_t1881340AF246501FDEA5B2D98320DCC6391EA475_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3DictionaryKeyPathProvider_t6C5E1C3D56CF6B258B1F02CEABB034B2CCF27361_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3Formatter_t99EEACDDE65C8FFEEA8FC3E89612F2DBFCFB4B89_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4DictionaryKeyPathProvider_t585894B6BD3E96C7579DA424BA43519171CC187A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4Formatter_tB2CFF78C205DB67804188702754E7AA2C50A34E9_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VersionFormatter_t63B69DDE17B794A9157CA14106C21D566CD9818C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[0];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(UnityEventFormatter_1_t9622DFCC4A76BCC826987EEF0843DA0CA8C6E910_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[1];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(NullableFormatter_1_t7CD1897D46FD139C0A8569AAFD48C14A97E16E75_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[2];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(ListFormatter_1_t68B3163F162A59657C34B769D0C62E9822CE42EF_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[3];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(KeyValuePairFormatter_2_t119AB3A285F27C14CDDB5E97F79601F4440C3E6A_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[4];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(HashSetFormatter_1_t658148EF16DB946F04F64D641BA39C4882A2A9BC_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[5];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(DoubleLookupDictionaryFormatter_3_t913CE2150584748FEBFF7AE5EDEC8BBD88A0AF9E_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[6];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(DictionaryFormatter_2_t6522868CAF7FAD86B0AFA27BA528085A3B169C67_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[7];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(DerivedDictionaryFormatter_3_t578ED9F90427BC2B7F87C9FDBFE3C5A75E8B1478_0_0_0_var), -1LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[8];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(DateTimeOffsetFormatter_t5ACAD25084B09C83E80297265E7D10B7B702B6A4_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[9];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(DateTimeFormatter_t6DFF5CDE2EA7969DA0CBAA7C4BD5D943F797D1EC_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[10];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(ArrayListFormatter_t9FBC2F6E80D8A6F84CB1A708FA8C39DA5F22F99A_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 * tmp = (RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 *)cache->attributes[11];
		RegisterFormatterLocatorAttribute__ctor_mFE77FF1296D532E4B067CF3A7313355B258F49CF(tmp, il2cpp_codegen_type_get_object(TypeFormatterLocator_tDE34C82E31477509FE4EB87F19ABE264041BF7D3_0_0_0_var), -70LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[12];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(TimeSpanFormatter_tDFA6395F0CFBF123A2115E2B639863CEEC7CB94A_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 * tmp = (RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 *)cache->attributes[13];
		RegisterFormatterLocatorAttribute__ctor_mFE77FF1296D532E4B067CF3A7313355B258F49CF(tmp, il2cpp_codegen_type_get_object(SelfFormatterLocator_t558C4FCFD0AFF5C762927DE05F048F1A21D985D8_0_0_0_var), -60LL, NULL);
	}
	{
		RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 * tmp = (RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 *)cache->attributes[14];
		RegisterFormatterLocatorAttribute__ctor_mFE77FF1296D532E4B067CF3A7313355B258F49CF(tmp, il2cpp_codegen_type_get_object(GenericCollectionFormatterLocator_t48BA7D2538EBF9F7E967BFFACB692A98D67E60A5_0_0_0_var), -100LL, NULL);
	}
	{
		RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 * tmp = (RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 *)cache->attributes[15];
		RegisterFormatterLocatorAttribute__ctor_mFE77FF1296D532E4B067CF3A7313355B258F49CF(tmp, il2cpp_codegen_type_get_object(DelegateFormatterLocator_t1F95C68737C71073D1978B79002A85861CDB4776_0_0_0_var), -50LL, NULL);
	}
	{
		RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 * tmp = (RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 *)cache->attributes[16];
		RegisterFormatterLocatorAttribute__ctor_mFE77FF1296D532E4B067CF3A7313355B258F49CF(tmp, il2cpp_codegen_type_get_object(ArrayFormatterLocator_tE729954088276BEB3F07AA62648858D863054A22_0_0_0_var), -80LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[17];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(VersionFormatter_t63B69DDE17B794A9157CA14106C21D566CD9818C_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[18];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(StackFormatter_2_t0D1441A8DFEAF653801F3AD686415DCE2DEB4124_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[19];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(QueueFormatter_2_t4297CDB0AC6687D76CD40D35B2BF5EF5168E2828_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[20];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(MethodInfoFormatter_1_tCFCFEA4E4C56739A9A307DE0990FC5292C7F41C6_0_0_0_var), 0LL, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[21];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[22];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[23];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[24];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 * tmp = (RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 *)cache->attributes[25];
		RegisterFormatterLocatorAttribute__ctor_mFE77FF1296D532E4B067CF3A7313355B258F49CF(tmp, il2cpp_codegen_type_get_object(ISerializableFormatterLocator_tE053B0B051AF5CAE7D582DC1BD4497B98D500900_0_0_0_var), -110LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[26];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(Vector2Formatter_t1881340AF246501FDEA5B2D98320DCC6391EA475_0_0_0_var), 0LL, NULL);
	}
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[27];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[28];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[29];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(RectFormatter_t598340E9CE1C921FA3D236C79060B845E1A3A497_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[30];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(QuaternionFormatter_t191A38F016652F309037E2E003738116D78169FD_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[31];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(LayerMaskFormatter_tF6156F275D61174AF28F42B2C6659682E81EBC11_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[32];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(KeyframeFormatter_t025E7661A71BBA63F454C17C815D23D59F33925F_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[33];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(GradientFormatter_tC0424F4B8E1BA9B60B24281D221F942405ECAF24_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[34];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(GradientColorKeyFormatter_t320370C3D0E68394741BC4F5CDB76EE987B9317F_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[35];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(GradientAlphaKeyFormatter_t9F644CAF171CFE023C10F56527EED83464F2F21C_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695 * tmp = (RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695 *)cache->attributes[36];
		RegisterDictionaryKeyPathProviderAttribute__ctor_m72BA975E1E2C3F05E19BDD08BA797D45385953D0(tmp, il2cpp_codegen_type_get_object(Vector4DictionaryKeyPathProvider_t585894B6BD3E96C7579DA424BA43519171CC187A_0_0_0_var), NULL);
	}
	{
		RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695 * tmp = (RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695 *)cache->attributes[37];
		RegisterDictionaryKeyPathProviderAttribute__ctor_m72BA975E1E2C3F05E19BDD08BA797D45385953D0(tmp, il2cpp_codegen_type_get_object(Vector3DictionaryKeyPathProvider_t6C5E1C3D56CF6B258B1F02CEABB034B2CCF27361_0_0_0_var), NULL);
	}
	{
		RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695 * tmp = (RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695 *)cache->attributes[38];
		RegisterDictionaryKeyPathProviderAttribute__ctor_m72BA975E1E2C3F05E19BDD08BA797D45385953D0(tmp, il2cpp_codegen_type_get_object(Vector2DictionaryKeyPathProvider_t158CEC5A7B7F1342BF2A88B71C2E013A629595F5_0_0_0_var), NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[39];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(CoroutineFormatter_tED36398512E10F0E67D346FE78A2ECA8BBC205D4_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[40];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(Vector3Formatter_t99EEACDDE65C8FFEEA8FC3E89612F2DBFCFB4B89_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[41];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(ColorFormatter_t434DDCE2F36DC38B1210645C514568A3C86D0B71_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[42];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(Color32Formatter_t7DEE99B33D9AE2C983F10C42B80F051D4F98DA7D_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[43];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(BoundsFormatter_t1B2A06125AA025F24F0E1F6252DD8FAB8E01AE97_0_0_0_var), 0LL, NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[44];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(AnimationCurveFormatter_t9DC9E69CAB99C0372B77A4E44ABA8F9DA3AF5A1F_0_0_0_var), 0LL, NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[45];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x32\x2E\x31\x2E\x36"), NULL);
	}
	{
		GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * tmp = (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 *)cache->attributes[46];
		GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA(tmp, il2cpp_codegen_string_new_wrapper("\x64\x63\x39\x63\x39\x36\x31\x63\x2D\x62\x65\x38\x62\x2D\x34\x31\x39\x38\x2D\x38\x31\x32\x61\x2D\x31\x31\x63\x34\x65\x61\x62\x63\x62\x34\x33\x61"), NULL);
	}
	{
		ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * tmp = (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A *)cache->attributes[47];
		ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172(tmp, false, NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[48];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[49];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x32\x30\x31\x38"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[50];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x64\x69\x6E\x53\x65\x72\x69\x61\x6C\x69\x7A\x65\x72"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[51];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x72\x65\x6E\x69\x78\x20\x49\x56\x53"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[52];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 * tmp = (RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300 *)cache->attributes[53];
		RegisterFormatterLocatorAttribute__ctor_mFE77FF1296D532E4B067CF3A7313355B258F49CF(tmp, il2cpp_codegen_type_get_object(ColorBlockFormatterLocator_t6B2953245798D46E38C6753D852675D05D6E7550_0_0_0_var), 0LL, NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[54];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x64\x69\x6E\x53\x65\x72\x69\x61\x6C\x69\x7A\x65\x72"), NULL);
	}
	{
		RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E * tmp = (RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E *)cache->attributes[55];
		RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F(tmp, il2cpp_codegen_type_get_object(Vector4Formatter_tB2CFF78C205DB67804188702754E7AA2C50A34E9_0_0_0_var), 0LL, NULL);
	}
}
static void U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CAssemblyU3Ei__Field(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CAttributeU3Ei__Field(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CU3Ef__AnonymousType0_2__ctor_m9BAFFEC8A10FCCC2F583A9A8B25A25C0D206423D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CU3Ef__AnonymousType0_2_Equals_mAB0B797695F65EF3179FE5A5E70AAAE2A045C93A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CU3Ef__AnonymousType0_2_GetHashCode_m8E90D6B5FBEED2B5F9DC794234923B9D3015FD01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CU3Ef__AnonymousType0_2_ToString_m3A5AA42572BE42BAE8867E955064A2197E8D16EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SerializedBehaviour_tB93227A0A5DBE6B2DB7705DAA6C6A9FA58C3E6C3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D * tmp = (ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D *)cache->attributes[0];
		ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m6F540B2A92D616275408AEB55BC97B1500F6C9B3(tmp, NULL);
	}
}
static void SerializedBehaviour_tB93227A0A5DBE6B2DB7705DAA6C6A9FA58C3E6C3_CustomAttributesCacheGenerator_serializationData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedComponent_tCE923DA38D204372BB45E914743E05E9F16601FF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D * tmp = (ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D *)cache->attributes[0];
		ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m6F540B2A92D616275408AEB55BC97B1500F6C9B3(tmp, NULL);
	}
}
static void SerializedComponent_tCE923DA38D204372BB45E914743E05E9F16601FF_CustomAttributesCacheGenerator_serializationData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedMonoBehaviour_tEC20C3928DB2336BB3E7C2DE4CEA27C639A05228_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D * tmp = (ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D *)cache->attributes[0];
		ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m6F540B2A92D616275408AEB55BC97B1500F6C9B3(tmp, NULL);
	}
}
static void SerializedMonoBehaviour_tEC20C3928DB2336BB3E7C2DE4CEA27C639A05228_CustomAttributesCacheGenerator_serializationData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializedScriptableObject_t4C262D5E487E039D7E3ECE5D824A17FDA04248C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D * tmp = (ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D *)cache->attributes[0];
		ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m6F540B2A92D616275408AEB55BC97B1500F6C9B3(tmp, NULL);
	}
}
static void SerializedScriptableObject_t4C262D5E487E039D7E3ECE5D824A17FDA04248C1_CustomAttributesCacheGenerator_serializationData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializedStateMachineBehaviour_t02A96B002DAEDA01CB19846F50AEFE38495DC617_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D * tmp = (ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D *)cache->attributes[0];
		ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m6F540B2A92D616275408AEB55BC97B1500F6C9B3(tmp, NULL);
	}
}
static void SerializedStateMachineBehaviour_t02A96B002DAEDA01CB19846F50AEFE38495DC617_CustomAttributesCacheGenerator_serializationData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedUnityObject_t8DB502FD26241981381C21087170325BC612972E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D * tmp = (ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D *)cache->attributes[0];
		ShowOdinSerializedPropertiesInInspectorAttribute__ctor_m6F540B2A92D616275408AEB55BC97B1500F6C9B3(tmp, NULL);
	}
}
static void SerializedUnityObject_t8DB502FD26241981381C21087170325BC612972E_CustomAttributesCacheGenerator_serializationData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void U3CU3Ec_tC253EAEFF843792A004AAEFFF5FA72B57E566A62_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AllowDeserializeInvalidDataAttribute_t3F7FED80A6783D4C54DA25A8D90562C803BB7D30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 12LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
	}
}
static void RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_U3CFormatterTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_U3CPriorityU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_RegisterFormatterAttribute_get_FormatterType_m1C17ED27D02A49CF77B26EAD119E1A6BD53B3774(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_RegisterFormatterAttribute_set_FormatterType_m103C9B8E2F00DD395423F02905FC2C9526439F2A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_RegisterFormatterAttribute_get_Priority_m058B73DFB7DD5061A54BAC4668D169CE59B517DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_RegisterFormatterAttribute_set_Priority_m1BD98F9B65102165E4C73855D8C1DF0CB5BF3A52(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
	}
}
static void RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_U3CFormatterLocatorTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_U3CPriorityU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_RegisterFormatterLocatorAttribute_get_FormatterLocatorType_m1CC4E212C4AC33E6970C9D25E5BE73238A45BDB1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_RegisterFormatterLocatorAttribute_set_FormatterLocatorType_m64378818193092212C1B5A56F1A4177C685C046F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_RegisterFormatterLocatorAttribute_get_Priority_m8ADAD66CF0E823C054ABEAC48FDEC5673D33D373(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_RegisterFormatterLocatorAttribute_set_Priority_m87C8053D211AB6E47C71D3AAF8320D0436C49629(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseDataReaderWriter_tD27FCD3D1F50001A7EABB0E3FA09FDAC35B3346C_CustomAttributesCacheGenerator_BaseDataReaderWriter_tD27FCD3D1F50001A7EABB0E3FA09FDAC35B3346C____Binder_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x65\x20\x42\x69\x6E\x64\x65\x72\x20\x6D\x65\x6D\x62\x65\x72\x20\x6F\x6E\x20\x74\x68\x65\x20\x77\x72\x69\x74\x65\x72\x27\x73\x20\x53\x65\x72\x69\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x43\x6F\x6E\x74\x65\x78\x74\x2F\x44\x65\x73\x65\x72\x69\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x43\x6F\x6E\x74\x65\x78\x74\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), false, NULL);
	}
}
static void U3CU3Ec_tD27ADF79C9016EE9482F4E967EE23E349CB56A98_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tC8BC1499B8D99378F8ADA44B8EBDD15413856266_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IDataReader_tF95D16F218C77A35EE0240E5D66CD5AE04E4882F_CustomAttributesCacheGenerator_IDataReader_tF95D16F218C77A35EE0240E5D66CD5AE04E4882F____Stream_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x20\x72\x65\x61\x64\x65\x72\x73\x20\x61\x6E\x64\x20\x77\x72\x69\x74\x65\x72\x73\x20\x64\x6F\x6E\x27\x74\x20\x6E\x65\x63\x65\x73\x73\x61\x72\x69\x6C\x79\x20\x68\x61\x76\x65\x20\x73\x74\x72\x65\x61\x6D\x73\x20\x61\x6E\x79\x20\x6C\x6F\x6E\x67\x65\x72\x2C\x20\x73\x6F\x20\x74\x68\x69\x73\x20\x41\x50\x49\x20\x68\x61\x73\x20\x62\x65\x65\x6E\x20\x6D\x61\x64\x65\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x2E\x20\x55\x73\x69\x6E\x67\x20\x74\x68\x69\x73\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x6D\x61\x79\x20\x72\x65\x73\x75\x6C\x74\x20\x69\x6E\x20\x4E\x6F\x74\x53\x75\x70\x70\x6F\x72\x74\x65\x64\x45\x78\x63\x65\x70\x74\x69\x6F\x6E\x73\x20\x62\x65\x69\x6E\x67\x20\x74\x68\x72\x6F\x77\x6E\x2E"), false, NULL);
	}
}
static void IDataWriter_t7D35BDD1175A987477E7233AE7424D2255F7ED24_CustomAttributesCacheGenerator_IDataWriter_t7D35BDD1175A987477E7233AE7424D2255F7ED24____Stream_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x20\x72\x65\x61\x64\x65\x72\x73\x20\x61\x6E\x64\x20\x77\x72\x69\x74\x65\x72\x73\x20\x64\x6F\x6E\x27\x74\x20\x6E\x65\x63\x65\x73\x73\x61\x72\x69\x6C\x79\x20\x68\x61\x76\x65\x20\x73\x74\x72\x65\x61\x6D\x73\x20\x61\x6E\x79\x20\x6C\x6F\x6E\x67\x65\x72\x2C\x20\x73\x6F\x20\x74\x68\x69\x73\x20\x41\x50\x49\x20\x68\x61\x73\x20\x62\x65\x65\x6E\x20\x6D\x61\x64\x65\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x2E\x20\x55\x73\x69\x6E\x67\x20\x74\x68\x69\x73\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x6D\x61\x79\x20\x72\x65\x73\x75\x6C\x74\x20\x69\x6E\x20\x4E\x6F\x74\x53\x75\x70\x70\x6F\x72\x74\x65\x64\x45\x78\x63\x65\x70\x74\x69\x6F\x6E\x73\x20\x62\x65\x69\x6E\x67\x20\x74\x68\x72\x6F\x77\x6E\x2E"), false, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_0_m218588B3EFD4554548306964F3A8BAE0B4581959(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_1_m188443B13E34FC27EB89104A2160A8DA73F3C7D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_2_mDF9455C87D245744AC3701F7E431943B46666CFE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_3_m5139CDCD9B6BD2E4B70D701599CF9E4AA026B118(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_4_m88AB03D52301DFDC42A0F43FA349DBFB026FBAEE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_5_mC91C29DE50FDBF6833681748D115ACC7E9EBD724(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_6_m902BCE00EF10AD6AA0D9AF52D1F30DC5B3B07174(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_7_m3D0D542B17868052801875B90C72346360C7A056(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_8_m66FCAFA00AF0501214FD38C9D3477C79A2CEBF64(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_9_mCD6B498A61367D2140B8E31B2980C1BB56BB00C3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_10_m6702C8E963908EC4C515470123BF42719717A6F4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_11_m8E7D85C98DA1230A22B9685469F6EE0C3C786FAD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_12_mD4275FB977E0D09EF7C74389E0BC8FD36203E15F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_13_m0C2CE08229502CB233D3531D23A8D5155AA2532A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonTextReader_tDF3061D2FE1D0761465A40B9EB90A0CAA79811DA_CustomAttributesCacheGenerator_U3CContextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonTextReader_tDF3061D2FE1D0761465A40B9EB90A0CAA79811DA_CustomAttributesCacheGenerator_JsonTextReader_get_Context_m59E7AEA4934FB3560774487C6404A94059C33598(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonTextReader_tDF3061D2FE1D0761465A40B9EB90A0CAA79811DA_CustomAttributesCacheGenerator_JsonTextReader_set_Context_m069A333FD72B60850A4E39856CFD571EA1898C96(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_0_m00F71BA12BF0DF57209A1FA5EB4AC74CBF1D7AD0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_1_m7E0978EBA0D4C9280D6B13548E06017B2C489EEF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_2_mEFA85D5013760DB520F6AA531A4BF42EEDA15E53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_3_m152BBB040A5EAD6076048DC9489202C785226101(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_4_m5FA0551D5DEFD265D9A9B57F87A8BE34B521430B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_5_m07D35A1038F73C248085CC9475E7218080ADA93E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_6_mD3F37E0C19AC407882B4D84567A27741C932EED4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_7_m1BCD4F188A91ECC086AE072A5614776ABE1ABAC2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_8_m99831C0576687FD9A72616944CDC61CE44688C02(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_9_mA12EA1F7DAD28B8ACE68647E4968263354396D09(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_10_m1F1437E82B74BE9E17270D222A8933249E86825F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_11_m2C8BEAAB1431C1A4BE9E8D96A707D19E8258E1AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_12_mD0490FAD60A6EF8DD4165446E007688F014464DA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_13_m3F7320E1559494FD26B6AC7265D7FA0C32622ADA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseFormatter_1_tEAB4C26ECD9655135EFAB77098E8BD15164E5C4C_CustomAttributesCacheGenerator_BaseFormatter_1_InvokeOnDeserializingCallbacks_mADC31F2BE377509D9968F4F04A57958B1E4C67BD(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x65\x20\x49\x6E\x76\x6F\x6B\x65\x4F\x6E\x44\x65\x73\x65\x72\x69\x61\x6C\x69\x7A\x69\x6E\x67\x43\x61\x6C\x6C\x62\x61\x63\x6B\x73\x20\x76\x61\x72\x69\x61\x6E\x74\x20\x74\x68\x61\x74\x20\x74\x61\x6B\x65\x73\x20\x61\x20\x72\x65\x66\x20\x54\x20\x76\x61\x6C\x75\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x66\x6F\x72\x20\x73\x74\x72\x75\x63\x74\x20\x63\x6F\x6D\x70\x61\x74\x69\x62\x69\x6C\x69\x74\x79\x20\x72\x65\x61\x73\x6F\x6E\x73\x2E"), false, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_t79DDFD26D16EE4231D9DC997660B4BB927875871_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t3CC39380F952F0F33B83B8F3CC54CA04BC5CFBE5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EmittedFormatterAttribute_tE1AE94E0FB78B936B1C1F9881FCF29CF5D06B05F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
}
static void AOTEmittedFormatter_1_t1795FE0DD2AD19B42166E7AC95BF57503B31640C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		EmittedFormatterAttribute_tE1AE94E0FB78B936B1C1F9881FCF29CF5D06B05F * tmp = (EmittedFormatterAttribute_tE1AE94E0FB78B936B1C1F9881FCF29CF5D06B05F *)cache->attributes[0];
		EmittedFormatterAttribute__ctor_m606F2E7D04ECA7FB692313F4893305F827FA8455(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_tC7D81128B652D05F4AA091FAAEDBD8DCC2F9A162_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_1_tCEF217A756F0C081C29E741B9F492D40D7B82CB5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t4ADA4BC1C89951C2964B86120D2C178257FE9372_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionFormatter_1_t041552A31A0285682FCAFEC5B5E7C85D8A27F176_CustomAttributesCacheGenerator_U3COverridePolicyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionFormatter_1_t041552A31A0285682FCAFEC5B5E7C85D8A27F176_CustomAttributesCacheGenerator_ReflectionFormatter_1_get_OverridePolicy_mE987D62C97DFF9DAAD3675A05B61CBF63FB746EC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionFormatter_1_t041552A31A0285682FCAFEC5B5E7C85D8A27F176_CustomAttributesCacheGenerator_ReflectionFormatter_1_set_OverridePolicy_mF92145F0C3290D4588B3339C22710AC1E0FDDDAA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t274866BB18ECC631F48D60B7108213C7AA45ACB0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AlwaysFormatsSelfAttribute_tE639A948C4C5B7B1A6ACB55831EB94C93C4ECCEC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 12LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void CustomFormatterAttribute_tB24056BEF162F516629BF0727EF0FF77766D51E1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x61\x20\x52\x65\x67\x69\x73\x74\x65\x72\x46\x6F\x72\x6D\x61\x74\x74\x65\x72\x41\x74\x74\x72\x69\x62\x75\x74\x65\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x61\x69\x6E\x69\x6E\x67\x20\x61\x73\x73\x65\x6D\x62\x6C\x79\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), true, NULL);
	}
}
static void CustomGenericFormatterAttribute_t000F1D6D62D1C5995F77CC884BF5B2D832E36F03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x61\x20\x52\x65\x67\x69\x73\x74\x65\x72\x46\x6F\x72\x6D\x61\x74\x74\x65\x72\x41\x74\x74\x72\x69\x62\x75\x74\x65\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x61\x69\x6E\x69\x6E\x67\x20\x61\x73\x73\x65\x6D\x62\x6C\x79\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), true, NULL);
	}
}
static void BindTypeNameToTypeAttribute_tA32AE85BE538C79C4E805E469C16E6C0B49C51E5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
	}
}
static void U3CU3Ec_t25851240DD0E773B56D2929FAF9D40452970CD41_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_U3CStringReferenceResolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_U3CGuidReferenceResolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_U3CIndexReferenceResolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_get_StringReferenceResolver_m2E6A2B2DD825C6A09328D89BBA8E51DE9C39CF7A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_set_StringReferenceResolver_mDCA00F48D400266293A8047DD7691313DDD7398E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_get_GuidReferenceResolver_m07C4BC6749087DE670A55821EE76E3FDC4763842(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_set_GuidReferenceResolver_mFBE1143558704260CC3056661B695858DA0CBD49(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_get_IndexReferenceResolver_mA9574D5FABC6706AA16F89CEE489D3D69CD169C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_set_IndexReferenceResolver_m085C6EE44AC7431CF98E3AB8DA0B6512274E2E34(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExcludeDataFromInspectorAttribute_t7AAADD0AB732A30C5BFA0DC9AA63CBB2F20562F2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x5B\x48\x69\x64\x65\x49\x6E\x49\x6E\x73\x70\x65\x63\x74\x6F\x72\x5D\x20\x69\x6E\x73\x74\x65\x61\x64\x20\x2D\x20\x69\x74\x20\x6E\x6F\x77\x20\x61\x6C\x73\x6F\x20\x65\x78\x63\x6C\x75\x64\x65\x73\x20\x74\x68\x65\x20\x6D\x65\x6D\x62\x65\x72\x20\x63\x6F\x6D\x70\x6C\x65\x74\x65\x6C\x79\x20\x66\x72\x6F\x6D\x20\x62\x65\x63\x6F\x6D\x69\x6E\x67\x20\x61\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x69\x6E\x20\x74\x68\x65\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x74\x72\x65\x65\x2E"), false, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 384LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void PreviouslySerializedAsAttribute_t5EA4E81CF3C83A7C162E599D2F5A904BD6E941E3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 384LL, NULL);
	}
}
static void PreviouslySerializedAsAttribute_t5EA4E81CF3C83A7C162E599D2F5A904BD6E941E3_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PreviouslySerializedAsAttribute_t5EA4E81CF3C83A7C162E599D2F5A904BD6E941E3_CustomAttributesCacheGenerator_PreviouslySerializedAsAttribute_get_Name_mD6B7A827CFCBB02C52D4361C71607E0ACA5DCC72(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PreviouslySerializedAsAttribute_t5EA4E81CF3C83A7C162E599D2F5A904BD6E941E3_CustomAttributesCacheGenerator_PreviouslySerializedAsAttribute_set_Name_m827CB90ACB0522D52FDD1273C1A88C86FC5128AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_U3CIndexReferenceResolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_U3CStringReferenceResolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_U3CGuidReferenceResolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_get_IndexReferenceResolver_mDA9EA854A29E80106218C2A9181669388AEFF7C2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_set_IndexReferenceResolver_m30246FA74C05A8929BB601618D7D2F1C054170C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_get_StringReferenceResolver_m8951CAFED2DD652D8BEF984D1F9D23397F5E61ED(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_set_StringReferenceResolver_mD8F0F1C09F84790CA6B0E1036C8A98882112D8FE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_get_GuidReferenceResolver_mDDB9FC152AB8A6A7A8B3D67105C2385820974430(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_set_GuidReferenceResolver_mBDB3D466481A10A762C1938D0FBD23198F13D5A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OdinSerializeAttribute_t45F89F42DBB53A91B8CAF3DB2CB17279FC79FB0C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A * tmp = (MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A *)cache->attributes[0];
		MeansImplicitUseAttribute__ctor_m551D20F17D1290E1F1D5CD516242DEFBBC67B7A4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 384LL, NULL);
	}
}
static void U3CU3Ec_tF1DFB478C2CB5709EB59E67ED4FBDAE7F7763649_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_t2558FCD6D059964E5287B414CE31ABBE13B10F74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Serializer_t8404F4E338393D41488CF8BF3366A1203D3BF8B8_CustomAttributesCacheGenerator_Serializer_FireOnSerializedType_mA5E91B9D9502A09C9C5097AC97AE35CC3CEA9715(CustomAttributesCache* cache)
{
	{
		ConditionalAttribute_t5DD558ED67ECF65952A82B94A75C6E8E121B2D8C * tmp = (ConditionalAttribute_t5DD558ED67ECF65952A82B94A75C6E8E121B2D8C *)cache->attributes[0];
		ConditionalAttribute__ctor_m43C71F47F8ED8FDF9A11FB20E8916C3737DD66AF(tmp, il2cpp_codegen_string_new_wrapper("\x55\x4E\x49\x54\x59\x5F\x45\x44\x49\x54\x4F\x52"), NULL);
	}
}
static void Serializer_1_t8511303B6099B7360248A85E80B302FEA8CD6569_CustomAttributesCacheGenerator_Serializer_1_FireOnSerializedType_mD4039F0C70A381362D3033CB72E0406E059F83EC(CustomAttributesCache* cache)
{
	{
		ConditionalAttribute_t5DD558ED67ECF65952A82B94A75C6E8E121B2D8C * tmp = (ConditionalAttribute_t5DD558ED67ECF65952A82B94A75C6E8E121B2D8C *)cache->attributes[0];
		ConditionalAttribute__ctor_m43C71F47F8ED8FDF9A11FB20E8916C3737DD66AF(tmp, il2cpp_codegen_string_new_wrapper("\x55\x4E\x49\x54\x59\x5F\x45\x44\x49\x54\x4F\x52"), NULL);
	}
}
static void RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
	}
}
static void SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_SerializedFormat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_SerializedBytes(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_ReferencedUnityObjects(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_SerializedBytesString(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_Prefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_PrefabModificationsReferencedUnityObjects(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_PrefabModifications(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_SerializationNodes(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF____HasEditorData_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x43\x6F\x6E\x74\x61\x69\x6E\x73\x44\x61\x74\x61\x20\x69\x6E\x73\x74\x65\x61\x64"), NULL);
	}
}
static void UnitySerializationInitializer_tEF7D5931C3AAD0E36296D9929BFFA06CA6650070_CustomAttributesCacheGenerator_U3CCurrentPlatformU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnitySerializationInitializer_tEF7D5931C3AAD0E36296D9929BFFA06CA6650070_CustomAttributesCacheGenerator_UnitySerializationInitializer_get_CurrentPlatform_mF269C7AAC83FC078F94F1116119D796ECB5820EB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnitySerializationInitializer_tEF7D5931C3AAD0E36296D9929BFFA06CA6650070_CustomAttributesCacheGenerator_UnitySerializationInitializer_set_CurrentPlatform_m55348AA2C668D1C114F90BB660ECC5382C0CD4F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UnitySerializationInitializer_tEF7D5931C3AAD0E36296D9929BFFA06CA6650070_CustomAttributesCacheGenerator_UnitySerializationInitializer_InitializeRuntime_mEC62F8B5E6AC16C44DAB3EFF5606F96ECFD1EB6B(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void EmittedAssemblyAttribute_t9F4B69CDFAF87216A4620BE079A248C5DA8A9943_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void EmittedAssemblyAttribute_t9F4B69CDFAF87216A4620BE079A248C5DA8A9943_CustomAttributesCacheGenerator_EmittedAssemblyAttribute__ctor_mF6C4FA2B76A0A7C473E38F0F3E4CF2D508FA05E7(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x74\x74\x72\x69\x62\x75\x74\x65\x20\x63\x61\x6E\x6E\x6F\x74\x20\x62\x65\x20\x75\x73\x65\x64\x20\x69\x6E\x20\x63\x6F\x64\x65\x2C\x20\x61\x6E\x64\x20\x69\x73\x20\x6F\x6E\x6C\x79\x20\x6D\x65\x61\x6E\x74\x20\x74\x6F\x20\x62\x65\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x64\x79\x6E\x61\x6D\x69\x63\x61\x6C\x6C\x79\x20\x65\x6D\x69\x74\x74\x65\x64\x20\x61\x73\x73\x65\x6D\x62\x6C\x69\x65\x73\x2E"), true, NULL);
	}
}
static void FormatterLocator_t0C95C16EA50270BEE2CAD20C270EEBDAAA9C1FE2_CustomAttributesCacheGenerator_FormatterLocator_t0C95C16EA50270BEE2CAD20C270EEBDAAA9C1FE2____FormatterResolve_EventInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x65\x20\x6E\x65\x77\x20\x49\x46\x6F\x72\x6D\x61\x74\x74\x65\x72\x4C\x6F\x63\x61\x74\x6F\x72\x20\x69\x6E\x74\x65\x72\x66\x61\x63\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x2C\x20\x61\x6E\x64\x20\x72\x65\x67\x69\x73\x74\x65\x72\x20\x79\x6F\x75\x72\x20\x63\x75\x73\x74\x6F\x6D\x20\x6C\x6F\x63\x61\x74\x6F\x72\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x52\x65\x67\x69\x73\x74\x65\x72\x46\x6F\x72\x6D\x61\x74\x74\x65\x72\x4C\x6F\x63\x61\x74\x6F\x72\x20\x61\x73\x73\x65\x6D\x62\x6C\x79\x20\x61\x74\x74\x72\x69\x62\x75\x74\x65\x2E"), true, NULL);
	}
}
static void U3CU3Ec_t9CF2C668D807D286A868C7727A0B80E7268B37D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14__ctor_m42E920DFA0EC2ED211ABF66260FC284F6C948795(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_IDisposable_Dispose_m213294EE73C9FD0BA4AE290DCFC4F05D8B0E372C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m9DE49E3A7D769B602A0780838785918940AA3CA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_Reset_m8F12030F2DE4AA1B634162D7F8E2B88FD1FCB392(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_get_Current_m5D0857D4C63F8F25DE3DB3350C16B277AC731EBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m120AA74EBA3617562BB5538F033FE57DE2E29276(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m9900CD20243E8E7059331DC685EE00C73F9AE343(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t05824FAEA168291D7E4ED0923DE07C822C447CD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_tBA082EF06B420B43B6BABFD0ADBBE0D7DDEF58B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DictionaryKeyUtility_t75EF696995E360AD629BBA3331464FE95BC84FBD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryKeyUtility_t75EF696995E360AD629BBA3331464FE95BC84FBD_CustomAttributesCacheGenerator_DictionaryKeyUtility_FromTo_mB0304339664BD0C90DEA2A7365EF3E633A78560C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t6C9EAA263674F928E593224E593741E36E5EB861_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t7A3733057C3D4665DD5CA722ACF0B4F18C2257FF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14__ctor_m6FB68B817C2FDF39AC91F2875C7B1DE111D2A0AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_IDisposable_Dispose_mB1F04B3D3BF5E0BD68489B69C56E1B7B3ACFB997(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m29217F8D66F77F53E359602C11B363F197DC05A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_Reset_mCAEC91788846F84F85BACA14A4340A852DC6FE97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_get_Current_m94C8A47343BA3A294EB02E6F7B29AC703DC1281F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m06387F1609C0D44AAFDBF33F4B5BE1AEA8396E2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m5D3536FEF22563E69093808247E92FCC1BB339EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_tC7C6F28D2E34BCDAD04E69D524499F74FDA73FC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t1200637903F1D714815EA26BE6126704CC75FB58_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass27_0_t22C109C5C9F8FF16334A92071AC941CFEA4F720F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass28_0_t22038F7CD4912967868E40B0A741D75D31EABEB9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldInfoExtensions_t5CA2C580D159B86450882F3F196A1FE3494BFAA0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FieldInfoExtensions_t5CA2C580D159B86450882F3F196A1FE3494BFAA0_CustomAttributesCacheGenerator_FieldInfoExtensions_IsAliasField_m1D38EAEE76FEF928BB693F75D5DFDEB70FAA1089(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FieldInfoExtensions_t5CA2C580D159B86450882F3F196A1FE3494BFAA0_CustomAttributesCacheGenerator_FieldInfoExtensions_DeAliasField_m1E13F279646C60998D6805F037E9BB465A732234(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GarbageFreeIterators_t6634156C3A461E01CE722CE84EEF03463B06375F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GarbageFreeIterators_t6634156C3A461E01CE722CE84EEF03463B06375F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_mC9D4CE22738DA8F212B75F501E2AEBD6F47939D7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GarbageFreeIterators_t6634156C3A461E01CE722CE84EEF03463B06375F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_m01169C32FBD4FD4B7AB3143A73C5B9D99DFC74DB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GarbageFreeIterators_t6634156C3A461E01CE722CE84EEF03463B06375F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFValueIterator_m2C54801D14CA35281108B7B5458176DB267BCFD0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GarbageFreeIterators_t6634156C3A461E01CE722CE84EEF03463B06375F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_mF15181EFD74EF2F00E4B1F2B370E189A99218D32(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t2ADAB361A5FF1B8551AA91AA50844CB055BFAD01_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t2ADAB361A5FF1B8551AA91AA50844CB055BFAD01_CustomAttributesCacheGenerator_LinqExtensions_ForEach_m682ECE7184ABDEE663968140F44F4249197044B1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t2ADAB361A5FF1B8551AA91AA50844CB055BFAD01_CustomAttributesCacheGenerator_LinqExtensions_ForEach_mDBAFDD8C659C6B3E3C4754DD5B2ECC5904A31969(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void LinqExtensions_t2ADAB361A5FF1B8551AA91AA50844CB055BFAD01_CustomAttributesCacheGenerator_LinqExtensions_Append_m31D827BC31116AD1AC12E381456D6E78A5CFC5BB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1__ctor_m5BCE362117CD75F937057F3B000A3A6A824B0111(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_IDisposable_Dispose_mEBBCAECAAC7C6B9E8B8A556BE1B27A1B861C5D14(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4A976CB2DE3D630629F9328DC46109C2DC3FE76C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_Collections_IEnumerator_Reset_m0D5C1666419808DE9BD41B0FABBE76CCFA477E72(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_Collections_IEnumerator_get_Current_m06F50BFF096ACA8EBE9B22FAF7E3408D556112E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11936940F2E33D51086BE45198062B197A066F26(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_Collections_IEnumerable_GetEnumerator_mC3618D25545447E5B15DFB0E2D1556D1F0D39323(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_IsDefined_mBC7A546527246072348370BB9BD9490E18E401CA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_IsDefined_m824D59E87ADD23158AF22274514F93721449EBC1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttribute_m1FCCAE4D679AF90869A1FD1F049A687855701B1F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttribute_m3A9D9535F2735CD89EB5A0530F848730BB87A1A1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_mCA942852D862D5ECDA4791B7D7716DD49C16E041(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_mD24310354E86F67E15F47D5CCC4CCB21938A735C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m349694041FCA1FF2377E50DB8AE258AD3139F5D5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m5B61F53BDD320500874039F20CE82C780CB4E90A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetNiceName_mCD6D786C0268CD6B40DCB737D5A8892369EF7E6D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_IsStatic_m0D65779AE20FC116BC260AF93A9398FAA167B8FD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_IsAlias_m2008AF6C8F05D73C9A66C3261B677D850D6B6A2F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_DeAlias_m7A87BBC0F97D506668D2919E6FADE5FA3A04EB2F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_GetFullName_m7289EF1866E240A4D8166835E23D870CAE19116E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_GetParamsNames_m0BD5DB755CF0FB1222863A5A7C73BED573E1DF53(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_GetFullName_m16F11391FA8EB6B9AF3604D0D68DEE379A6E4169(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_IsExtensionMethod_mCBF921ED9BCA898FDA5BC4B01670C47D86F0CB1C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_IsAliasMethod_mC735E84942B6AA66BE3CF2337C6EE77FA3F66257(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_DeAliasMethod_mDE8989617E2D4C45BD200463E09A2ABCFD541A59(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PathUtilities_t702A32068A4F2FACF97E3E4E83BE622FA0F46AB5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PathUtilities_t702A32068A4F2FACF97E3E4E83BE622FA0F46AB5_CustomAttributesCacheGenerator_PathUtilities_HasSubDirectory_mA04B6A623ADEE2828CD0BD0C9A39E3699B0761B7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PropertyInfoExtensions_tE5E5148B32666C2E6B356F2F599FB2D061D6477E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PropertyInfoExtensions_tE5E5148B32666C2E6B356F2F599FB2D061D6477E_CustomAttributesCacheGenerator_PropertyInfoExtensions_IsAutoProperty_mF5E9A6C194E664F9556D51B8D09007F8EAACE91C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PropertyInfoExtensions_tE5E5148B32666C2E6B356F2F599FB2D061D6477E_CustomAttributesCacheGenerator_PropertyInfoExtensions_IsAliasProperty_m87041E7971788AE6886143359B4B07B999125A2F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void PropertyInfoExtensions_tE5E5148B32666C2E6B356F2F599FB2D061D6477E_CustomAttributesCacheGenerator_PropertyInfoExtensions_DeAliasProperty_m015DB168AC8F90AA5143293D1A290A60CF55A570(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_tB67B0E35C92CE19EBCAB707D4EB905B98223E263_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_tB67B0E35C92CE19EBCAB707D4EB905B98223E263_CustomAttributesCacheGenerator_StringExtensions_ToTitleCase_m75E36FAEAEB8CB6FC90F54B5B30BD5CC673DD0C6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_tB67B0E35C92CE19EBCAB707D4EB905B98223E263_CustomAttributesCacheGenerator_StringExtensions_IsNullOrWhitespace_mE7508CCAA9EA8CCC21D96229BD62C292539A1ED8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCastDefined_mFE16EE4FE9FF3852768497633F01D0E991C3BBED(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_IsCastableTo_m12EF9C5160625DEE41CB62C3D8F2D2E7D378D51F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCastMethodDelegate_m987046E7D47F282D654496261B7883C28D6D31DA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCastMethod_mD03313876F01CE5085BFDB60AE52D9A338439CA9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_m4C7325DD9F6B1A1DEDBCC2F283A28FE556681A52(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_m0BD313CB34C59F667C535D11BC7FDCE04318F0BE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_mBF3910CB7A5294795886EE74D37329336842B6C8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_m0100EFB063EDD278209AE9CE045A8785C73842BB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_m807409621DE640D0570A47C2C7C93A23726B4918(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_m02DCC99327737793E7746DA3B4CCDECFAA6554ED(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetAttribute_m2A31B5E13E657B683512F65EB806A4D04C7EC644(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOrInherits_mBB069DC34FD0A3D5A9AAE45FF5FCC86D3A0AC3B9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericType_m3FC6E38C111B4C1BF15919E393DE4258469E09E5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericInterface_m36B2221D9263FB90DF896EDE0C20F294D955572F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericClass_mEF109A8BA5BADD6C482009E891DCAB86DDC3CEB9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m6CB1DCD49EBE82C352539704EA4A74670B21C5C0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_mD5EBA354E9E6A9607CFFEFCA448E418668E35056(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m61744A5C5CC21963B53858F744CE2B557E15E30A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetOperatorMethod_mAC2F5581188D79F35D91677D1A9054EF842BBC26(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetOperatorMethods_m6D139F72C9221E06D5F6DF0C21F0656C2947C4BD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_mAF1EC9227C3CDA77A78B41600741BC22B282B330(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_mD652499D27311B8B3E231622ECBC3223C8FEACA4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_m243007EA85F625D6246997703D8FC2B22DA3E2E3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetGenericBaseType_mFEF667CA3694CE4833C076C2465EC8BEC35856EA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetGenericBaseType_mFB61C16F72798D16114C6F724FB868F9244345BC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetBaseTypes_m983F94E0CFB953F1D94BC51BB7637490EE5DEB14(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetBaseClasses_mDC132249680C2F2FC1D237587A00841A013F61FC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_TypeNameGauntlet_mE817E158A4A383649188FC15D0B79E2663C6499F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetNiceName_m5520AD5A39ABAB082F2DD30222BCADBE03CD21B6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetNiceFullName_mF291B98B75ECEB06240A18E2AA9BA2DE1F17A230(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCompilableNiceName_m4B236B397503B7C845545AF25337B7515CE1352C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCompilableNiceFullName_m2599EDF5418ECCACF2B4E3DF981E9CFFCCD0B9E0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttribute_m171FC165C46326A232661CCB254240740B3CEC23(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttribute_mFAC852EEBCAAEC6789E8A21AA075184863C0CA0F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttributes_m47BA6BB60DE751653BFEDBD7DD8BF221D8D1DBD7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttributes_mE4E800AA23C47395FCB161ED16599FF7311891ED(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_IsDefined_mEB516F440FD69AB91905130287EEBF0A3938023D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_IsDefined_m3188D2AA09EAC6C1165C335E33D7DAB3CC0C55CA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_InheritsFrom_m497CA7EEBF216C6E671859C25551F64D4D193F5C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_InheritsFrom_mB2C8CCD4BC2F89D305E9E82CED3E6D078F6B1C9D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetInheritanceDistance_m0520A54AE277190031955B26C48CF1C4793B7AF3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasParamaters_m8B781DA0243FDDE934633308D48B7006BB1393FF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetReturnType_mE350B159933DEF36F13497D2648D1023927E0105(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetMemberValue_mA49CC5ACB73A92E1559601E6BCB9B465FCABED36(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_SetMemberValue_m535B84D7681E0D3ACD19641D7D7F6184C50D1DB7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_TryInferGenericParameters_m16A99D765E67EBE6F6441327D86682E736D00457(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_TryInferGenericParameters_m16A99D765E67EBE6F6441327D86682E736D00457____knownParameters2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m8A5E53288BFDFBAD31CE121DFCEBD72215CD3799(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m8A5E53288BFDFBAD31CE121DFCEBD72215CD3799____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_mF6CC8438959153139AB1EDEE8A68FF75C184C64F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_mF6CC8438959153139AB1EDEE8A68FF75C184C64F____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GenericParameterIsFulfilledBy_m6CBD13619911088E757B92211349F616C9779E2D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GenericParameterIsFulfilledBy_m2E2EE166F01A2BC075772E3AF0C462C13DAB3D23(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetGenericConstraintsString_m235BDED19C12FF498E7BE5515FFFE62B3EFBC917(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetGenericParameterConstraintsString_m6150DD784FC1554DDCC958F42F821CA5D9631E5B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GenericArgumentsContainsTypes_m8BC8C323DE7F8BDAE770B61B0FD7CAC68EF751FB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GenericArgumentsContainsTypes_m8BC8C323DE7F8BDAE770B61B0FD7CAC68EF751FB____types1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_IsFullyConstructedGenericType_m9FA2CB4ECC617B95C129F22FBA6619B03786B236(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_IsNullableType_m9E8A5C8C3F6C473C6F086729A8966AA502A6AC5F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_SafeGetTypes_m3E480DDB012372630A76753275F4063BCBCA515D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_SafeIsDefined_mB09B7D31E0F9E2B39976557D61A5C5A7205D4F19(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_SafeGetCustomAttributes_m38CC04731F22628B3292D1A49D5CE61C2673BB25(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_t793BF203515E40F79AB5C60C5A4EB3E8D011C2C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__25_1_t40B1919EA4DCEFEA94717A0FA09A0055758909C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass35_0_t93C575C177E53F488DF07AB617512B671FB15C06_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass40_0_tB2334106EA2BB38A2D5B2AC9BBDA2A0E5595879A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass41_0_tAFFC35280E6FB41F7042EE709C5481D5BE47891F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42__ctor_mECC28ADDEDB2CFA0AD434C22F8369FA6DAC56390(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_IDisposable_Dispose_m9E319A62EFB4C3E57BED82A892250F00FC9B43B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mCE2F1CA338CF519DE8199720D6F10F60C833DC92(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_Reset_mA8107830BE90C795E27D5738C36CD0D452AFAF82(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_get_Current_m9D2C83C4E15A768D0658BBB1E95B8B963F88BD9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mF7146FB637C47E3922C611D84C0849179F9D44DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mF3BD4F1E1832E4D4DB4F6C5B6B1040A4530DECA4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43__ctor_mD6E740BB16530EB8DC13A05DC855CA6004C722C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_IDisposable_Dispose_mFFD18EABCD5841589699BECB72FE46280928EB67(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m32C3AE5D7E414AF63C4E2BFCFE1B524C69C74E28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_Reset_m0A334D28C9D62519A97501CC7530ECF74CB89295(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_get_Current_m37C697AEF1C2BE80E9464F0667B0466CB9FD9842(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m544D77F6409E37E5AAFDE93424D13976A215278D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_Collections_IEnumerable_GetEnumerator_mE50E2086242B0D28A1760315F8A023506D39B591(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1__ctor_mD6E46E913C898268AD2E61EA0D4D361B04DDB589(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_IDisposable_Dispose_m46F9947A5A2EAE2CFA9220202D4C638DA51933E7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m0FFFF7C630C49276F9BF8505F3D0C0F0BD20A064(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_Collections_IEnumerator_Reset_m6D3C39654544D0F36CE096951A40A6BC60227CD4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_Collections_IEnumerator_get_Current_m593BA2C585B144DCF7E5EBFD6B6FB74E53207571(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mEB3711712FC197B531BFD5D2BCDA1A8535E2883D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_Collections_IEnumerable_GetEnumerator_m9BE37070F6DEF223C88B6695796D02E023631EB2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48__ctor_mC5020F833A3331D50ED3E4E46E31AF973C690C63(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_IDisposable_Dispose_m42AFBCF3E84334C072FB5FDD3421EF847CB8EB46(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_mCE1DBB20C09DA287D6B34CC1D3635B6D0889257F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_Reset_mCBDCB3D8C952B2FC5F8E7C5E65919C03719387F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_get_Current_mD2EDA6986FDE391CBF36B8BA992B4C7E79FC0BD2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m3F002A1BBB23E235B788DBBBDE103CBBF592F3C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerable_GetEnumerator_m98F625F59DD4C3E6DF98E87E34A20BFEBB4C152F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UnityExtensions_t1CDF2900C99F7F790AC7DB98A0A42CAF10A41450_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UnityExtensions_t1CDF2900C99F7F790AC7DB98A0A42CAF10A41450_CustomAttributesCacheGenerator_UnityExtensions_SafeIsUnityNull_m148B87C1D426B3FED2F450A9776997A28ED580CF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DoubleLookupDictionary_3_tB3B899F9050549E9858BD8865D39AB1171F6617C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_1_tE56A0A36EA4BBA08B85F896382564F5831BF7FCA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_1_1_t1B793ABCF0B42EA7B8FA6302769CBE365F580D3E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tFEEFD66EBB01FF4624B49D0DF37D6C939D4CEEEC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_1_t45CB3070DBA381CE45945346AA705F82FDAE2311_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tE38605C9FE10ADDFEADB739F88B0411422052200_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_2_t05A05AA0EAC944FC77FC66323FD146DDD751E617_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_1_t3D6AC31790EBA8BA5BF3E41CF6598FDB111F0086_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_t5A833CB063BDBF19E3F2D0A322B686F1EA412C2A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_2_t5B50A6FE5D598E73D5302DF64A35796313172BCD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_1_t8046B2E3ECFB4CED511A2800944028631B99E5B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_t79801EA8D41A561F998F96A001E2715F453B98C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t0F3062CAF3C631037A5D64CDE10F4E8C7BB4FDB1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_tCE9496DFE7941906106C778794C0CA7FFA7FE923_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_1_t15A84DA933E50A36324E8403A809342380C7343C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_1_t7D254CE483158C0B88FE254AC192320349F1B783_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_2_t36CCF7AA9381313C62782DAE7132378A39F84914_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_2_t60E50A8D0875ECB6A5EAAFB429EBFEFC7EAB767F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_1_tC242288FC99E6D518ABCEDA25629B8507A41BC15_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_tBF083E413B1D18E4103502D318D5B5F82E21D987_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_2_tEEE6F84E2575CB049CF0E8DBD7D8B8CA812FD734_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_1_t57F3C75F54C05D083B272B7608FEF1407AEEBC2C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_2_t0224AE6C7CAED16FF67798A409F9058B94BC48CF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_1_t8A456FCC3CADEB8C87316D36820B95B92C1616C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_2_t41E72E9B2EC23A096BBD538B79E409A19D4EDC54_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IImmutableList_1_t150A0C49FF369998F250CFD3AE7B63508FC76257_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ImmutableList_t7EEB43042B202DEC6FDA1EE5D49FCA9BD739CF2D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ImmutableList_t7EEB43042B202DEC6FDA1EE5D49FCA9BD739CF2D_CustomAttributesCacheGenerator_innerList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_mB98A496FC6D73EB5897246F5885D9ED7FAC7284A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m14F4C94791AFA5DA05AE5DE20606B7D47011868F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD01C87D86A2E129FDDE79B964FE84B546B589053(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m493988934AAA09863EA33E918C0579D8826E0454(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m2343A58E3F14DD579E8F296C8063D129E1AF5E18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ImmutableList_1_t00FB68755114F41DEF60D3135730F156F88A3053_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ImmutableList_1_t00FB68755114F41DEF60D3135730F156F88A3053_CustomAttributesCacheGenerator_innerList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ImmutableList_2_t75D6CBEB58FB005F1F3C348659543BA209B9929A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void UnityVersion_t2F9DD8D6B1E46B65766456A24C7907DDABBE9005_CustomAttributesCacheGenerator_UnityVersion_EnsureLoaded_m38F0696D643531BF201CD3DC509423ECEF98FCAB(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t8B44F2DAA0D09E1DFC5EC1E6F58900650CDB3755_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Sirenix_Serialization_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Sirenix_Serialization_AttributeGenerators[351] = 
{
	U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator,
	SerializedBehaviour_tB93227A0A5DBE6B2DB7705DAA6C6A9FA58C3E6C3_CustomAttributesCacheGenerator,
	SerializedComponent_tCE923DA38D204372BB45E914743E05E9F16601FF_CustomAttributesCacheGenerator,
	SerializedMonoBehaviour_tEC20C3928DB2336BB3E7C2DE4CEA27C639A05228_CustomAttributesCacheGenerator,
	SerializedScriptableObject_t4C262D5E487E039D7E3ECE5D824A17FDA04248C1_CustomAttributesCacheGenerator,
	SerializedStateMachineBehaviour_t02A96B002DAEDA01CB19846F50AEFE38495DC617_CustomAttributesCacheGenerator,
	SerializedUnityObject_t8DB502FD26241981381C21087170325BC612972E_CustomAttributesCacheGenerator,
	U3CU3Ec_tC253EAEFF843792A004AAEFFF5FA72B57E566A62_CustomAttributesCacheGenerator,
	AllowDeserializeInvalidDataAttribute_t3F7FED80A6783D4C54DA25A8D90562C803BB7D30_CustomAttributesCacheGenerator,
	RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator,
	RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator,
	U3CU3Ec_tD27ADF79C9016EE9482F4E967EE23E349CB56A98_CustomAttributesCacheGenerator,
	U3CU3Ec_tC8BC1499B8D99378F8ADA44B8EBDD15413856266_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_t79DDFD26D16EE4231D9DC997660B4BB927875871_CustomAttributesCacheGenerator,
	U3CU3Ec_t3CC39380F952F0F33B83B8F3CC54CA04BC5CFBE5_CustomAttributesCacheGenerator,
	EmittedFormatterAttribute_tE1AE94E0FB78B936B1C1F9881FCF29CF5D06B05F_CustomAttributesCacheGenerator,
	AOTEmittedFormatter_1_t1795FE0DD2AD19B42166E7AC95BF57503B31640C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_tC7D81128B652D05F4AA091FAAEDBD8DCC2F9A162_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_1_tCEF217A756F0C081C29E741B9F492D40D7B82CB5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t4ADA4BC1C89951C2964B86120D2C178257FE9372_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t274866BB18ECC631F48D60B7108213C7AA45ACB0_CustomAttributesCacheGenerator,
	AlwaysFormatsSelfAttribute_tE639A948C4C5B7B1A6ACB55831EB94C93C4ECCEC_CustomAttributesCacheGenerator,
	CustomFormatterAttribute_tB24056BEF162F516629BF0727EF0FF77766D51E1_CustomAttributesCacheGenerator,
	CustomGenericFormatterAttribute_t000F1D6D62D1C5995F77CC884BF5B2D832E36F03_CustomAttributesCacheGenerator,
	BindTypeNameToTypeAttribute_tA32AE85BE538C79C4E805E469C16E6C0B49C51E5_CustomAttributesCacheGenerator,
	U3CU3Ec_t25851240DD0E773B56D2929FAF9D40452970CD41_CustomAttributesCacheGenerator,
	ExcludeDataFromInspectorAttribute_t7AAADD0AB732A30C5BFA0DC9AA63CBB2F20562F2_CustomAttributesCacheGenerator,
	PreviouslySerializedAsAttribute_t5EA4E81CF3C83A7C162E599D2F5A904BD6E941E3_CustomAttributesCacheGenerator,
	OdinSerializeAttribute_t45F89F42DBB53A91B8CAF3DB2CB17279FC79FB0C_CustomAttributesCacheGenerator,
	U3CU3Ec_tF1DFB478C2CB5709EB59E67ED4FBDAE7F7763649_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_t2558FCD6D059964E5287B414CE31ABBE13B10F74_CustomAttributesCacheGenerator,
	RegisterDictionaryKeyPathProviderAttribute_t7F4ABC29846697C1798B46B430E95DD11BFF1695_CustomAttributesCacheGenerator,
	EmittedAssemblyAttribute_t9F4B69CDFAF87216A4620BE079A248C5DA8A9943_CustomAttributesCacheGenerator,
	U3CU3Ec_t9CF2C668D807D286A868C7727A0B80E7268B37D5_CustomAttributesCacheGenerator,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator,
	U3CU3Ec_t05824FAEA168291D7E4ED0923DE07C822C447CD3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_tBA082EF06B420B43B6BABFD0ADBBE0D7DDEF58B2_CustomAttributesCacheGenerator,
	DictionaryKeyUtility_t75EF696995E360AD629BBA3331464FE95BC84FBD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t6C9EAA263674F928E593224E593741E36E5EB861_CustomAttributesCacheGenerator,
	U3CU3Ec_t7A3733057C3D4665DD5CA722ACF0B4F18C2257FF_CustomAttributesCacheGenerator,
	U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator,
	U3CU3Ec_tC7C6F28D2E34BCDAD04E69D524499F74FDA73FC2_CustomAttributesCacheGenerator,
	U3CU3Ec_t1200637903F1D714815EA26BE6126704CC75FB58_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass27_0_t22C109C5C9F8FF16334A92071AC941CFEA4F720F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass28_0_t22038F7CD4912967868E40B0A741D75D31EABEB9_CustomAttributesCacheGenerator,
	FieldInfoExtensions_t5CA2C580D159B86450882F3F196A1FE3494BFAA0_CustomAttributesCacheGenerator,
	GarbageFreeIterators_t6634156C3A461E01CE722CE84EEF03463B06375F_CustomAttributesCacheGenerator,
	LinqExtensions_t2ADAB361A5FF1B8551AA91AA50844CB055BFAD01_CustomAttributesCacheGenerator,
	U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator,
	MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator,
	PathUtilities_t702A32068A4F2FACF97E3E4E83BE622FA0F46AB5_CustomAttributesCacheGenerator,
	PropertyInfoExtensions_tE5E5148B32666C2E6B356F2F599FB2D061D6477E_CustomAttributesCacheGenerator,
	StringExtensions_tB67B0E35C92CE19EBCAB707D4EB905B98223E263_CustomAttributesCacheGenerator,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_t793BF203515E40F79AB5C60C5A4EB3E8D011C2C0_CustomAttributesCacheGenerator,
	U3CU3Ec__25_1_t40B1919EA4DCEFEA94717A0FA09A0055758909C4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass35_0_t93C575C177E53F488DF07AB617512B671FB15C06_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass40_0_tB2334106EA2BB38A2D5B2AC9BBDA2A0E5595879A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass41_0_tAFFC35280E6FB41F7042EE709C5481D5BE47891F_CustomAttributesCacheGenerator,
	U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator,
	U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator,
	U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator,
	U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator,
	UnityExtensions_t1CDF2900C99F7F790AC7DB98A0A42CAF10A41450_CustomAttributesCacheGenerator,
	DoubleLookupDictionary_3_tB3B899F9050549E9858BD8865D39AB1171F6617C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_1_tE56A0A36EA4BBA08B85F896382564F5831BF7FCA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_1_1_t1B793ABCF0B42EA7B8FA6302769CBE365F580D3E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tFEEFD66EBB01FF4624B49D0DF37D6C939D4CEEEC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_1_t45CB3070DBA381CE45945346AA705F82FDAE2311_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tE38605C9FE10ADDFEADB739F88B0411422052200_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_2_t05A05AA0EAC944FC77FC66323FD146DDD751E617_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_1_t3D6AC31790EBA8BA5BF3E41CF6598FDB111F0086_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_t5A833CB063BDBF19E3F2D0A322B686F1EA412C2A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_2_t5B50A6FE5D598E73D5302DF64A35796313172BCD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_1_t8046B2E3ECFB4CED511A2800944028631B99E5B2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_t79801EA8D41A561F998F96A001E2715F453B98C8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t0F3062CAF3C631037A5D64CDE10F4E8C7BB4FDB1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_tCE9496DFE7941906106C778794C0CA7FFA7FE923_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_1_t15A84DA933E50A36324E8403A809342380C7343C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_1_t7D254CE483158C0B88FE254AC192320349F1B783_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_2_t36CCF7AA9381313C62782DAE7132378A39F84914_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_2_t60E50A8D0875ECB6A5EAAFB429EBFEFC7EAB767F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_1_tC242288FC99E6D518ABCEDA25629B8507A41BC15_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_tBF083E413B1D18E4103502D318D5B5F82E21D987_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_2_tEEE6F84E2575CB049CF0E8DBD7D8B8CA812FD734_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_1_t57F3C75F54C05D083B272B7608FEF1407AEEBC2C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_2_t0224AE6C7CAED16FF67798A409F9058B94BC48CF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_1_t8A456FCC3CADEB8C87316D36820B95B92C1616C1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_2_t41E72E9B2EC23A096BBD538B79E409A19D4EDC54_CustomAttributesCacheGenerator,
	IImmutableList_1_t150A0C49FF369998F250CFD3AE7B63508FC76257_CustomAttributesCacheGenerator,
	ImmutableList_t7EEB43042B202DEC6FDA1EE5D49FCA9BD739CF2D_CustomAttributesCacheGenerator,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator,
	ImmutableList_1_t00FB68755114F41DEF60D3135730F156F88A3053_CustomAttributesCacheGenerator,
	ImmutableList_2_t75D6CBEB58FB005F1F3C348659543BA209B9929A_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t8B44F2DAA0D09E1DFC5EC1E6F58900650CDB3755_CustomAttributesCacheGenerator,
	U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CAssemblyU3Ei__Field,
	U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CAttributeU3Ei__Field,
	SerializedBehaviour_tB93227A0A5DBE6B2DB7705DAA6C6A9FA58C3E6C3_CustomAttributesCacheGenerator_serializationData,
	SerializedComponent_tCE923DA38D204372BB45E914743E05E9F16601FF_CustomAttributesCacheGenerator_serializationData,
	SerializedMonoBehaviour_tEC20C3928DB2336BB3E7C2DE4CEA27C639A05228_CustomAttributesCacheGenerator_serializationData,
	SerializedScriptableObject_t4C262D5E487E039D7E3ECE5D824A17FDA04248C1_CustomAttributesCacheGenerator_serializationData,
	SerializedStateMachineBehaviour_t02A96B002DAEDA01CB19846F50AEFE38495DC617_CustomAttributesCacheGenerator_serializationData,
	SerializedUnityObject_t8DB502FD26241981381C21087170325BC612972E_CustomAttributesCacheGenerator_serializationData,
	RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_U3CFormatterTypeU3Ek__BackingField,
	RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_U3CPriorityU3Ek__BackingField,
	RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_U3CFormatterLocatorTypeU3Ek__BackingField,
	RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_U3CPriorityU3Ek__BackingField,
	JsonTextReader_tDF3061D2FE1D0761465A40B9EB90A0CAA79811DA_CustomAttributesCacheGenerator_U3CContextU3Ek__BackingField,
	ReflectionFormatter_1_t041552A31A0285682FCAFEC5B5E7C85D8A27F176_CustomAttributesCacheGenerator_U3COverridePolicyU3Ek__BackingField,
	DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_U3CStringReferenceResolverU3Ek__BackingField,
	DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_U3CGuidReferenceResolverU3Ek__BackingField,
	DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_U3CIndexReferenceResolverU3Ek__BackingField,
	PreviouslySerializedAsAttribute_t5EA4E81CF3C83A7C162E599D2F5A904BD6E941E3_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_U3CIndexReferenceResolverU3Ek__BackingField,
	SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_U3CStringReferenceResolverU3Ek__BackingField,
	SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_U3CGuidReferenceResolverU3Ek__BackingField,
	SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_SerializedFormat,
	SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_SerializedBytes,
	SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_ReferencedUnityObjects,
	SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_SerializedBytesString,
	SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_Prefab,
	SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_PrefabModificationsReferencedUnityObjects,
	SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_PrefabModifications,
	SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_SerializationNodes,
	UnitySerializationInitializer_tEF7D5931C3AAD0E36296D9929BFFA06CA6650070_CustomAttributesCacheGenerator_U3CCurrentPlatformU3Ek__BackingField,
	ImmutableList_t7EEB43042B202DEC6FDA1EE5D49FCA9BD739CF2D_CustomAttributesCacheGenerator_innerList,
	ImmutableList_1_t00FB68755114F41DEF60D3135730F156F88A3053_CustomAttributesCacheGenerator_innerList,
	U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CU3Ef__AnonymousType0_2__ctor_m9BAFFEC8A10FCCC2F583A9A8B25A25C0D206423D,
	U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CU3Ef__AnonymousType0_2_Equals_mAB0B797695F65EF3179FE5A5E70AAAE2A045C93A,
	U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CU3Ef__AnonymousType0_2_GetHashCode_m8E90D6B5FBEED2B5F9DC794234923B9D3015FD01,
	U3CU3Ef__AnonymousType0_2_t9182C6C9BA5FBF697B99CBDADF5F625699C62CE9_CustomAttributesCacheGenerator_U3CU3Ef__AnonymousType0_2_ToString_m3A5AA42572BE42BAE8867E955064A2197E8D16EF,
	RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_RegisterFormatterAttribute_get_FormatterType_m1C17ED27D02A49CF77B26EAD119E1A6BD53B3774,
	RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_RegisterFormatterAttribute_set_FormatterType_m103C9B8E2F00DD395423F02905FC2C9526439F2A,
	RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_RegisterFormatterAttribute_get_Priority_m058B73DFB7DD5061A54BAC4668D169CE59B517DD,
	RegisterFormatterAttribute_t5A264668D672A8E4EB0A3D22A25CE8CFF1520C6E_CustomAttributesCacheGenerator_RegisterFormatterAttribute_set_Priority_m1BD98F9B65102165E4C73855D8C1DF0CB5BF3A52,
	RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_RegisterFormatterLocatorAttribute_get_FormatterLocatorType_m1CC4E212C4AC33E6970C9D25E5BE73238A45BDB1,
	RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_RegisterFormatterLocatorAttribute_set_FormatterLocatorType_m64378818193092212C1B5A56F1A4177C685C046F,
	RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_RegisterFormatterLocatorAttribute_get_Priority_m8ADAD66CF0E823C054ABEAC48FDEC5673D33D373,
	RegisterFormatterLocatorAttribute_t58FC588DBDFB551B4152DE3ABA23FFB915F44300_CustomAttributesCacheGenerator_RegisterFormatterLocatorAttribute_set_Priority_m87C8053D211AB6E47C71D3AAF8320D0436C49629,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_0_m218588B3EFD4554548306964F3A8BAE0B4581959,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_1_m188443B13E34FC27EB89104A2160A8DA73F3C7D1,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_2_mDF9455C87D245744AC3701F7E431943B46666CFE,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_3_m5139CDCD9B6BD2E4B70D701599CF9E4AA026B118,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_4_m88AB03D52301DFDC42A0F43FA349DBFB026FBAEE,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_5_mC91C29DE50FDBF6833681748D115ACC7E9EBD724,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_6_m902BCE00EF10AD6AA0D9AF52D1F30DC5B3B07174,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_7_m3D0D542B17868052801875B90C72346360C7A056,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_8_m66FCAFA00AF0501214FD38C9D3477C79A2CEBF64,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_9_mCD6B498A61367D2140B8E31B2980C1BB56BB00C3,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_10_m6702C8E963908EC4C515470123BF42719717A6F4,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_11_m8E7D85C98DA1230A22B9685469F6EE0C3C786FAD,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_12_mD4275FB977E0D09EF7C74389E0BC8FD36203E15F,
	JsonDataReader_t557593E8C6052C9B4BDF7566D5E68A9F78CBA8DB_CustomAttributesCacheGenerator_JsonDataReader_U3C_ctorU3Eb__7_13_m0C2CE08229502CB233D3531D23A8D5155AA2532A,
	JsonTextReader_tDF3061D2FE1D0761465A40B9EB90A0CAA79811DA_CustomAttributesCacheGenerator_JsonTextReader_get_Context_m59E7AEA4934FB3560774487C6404A94059C33598,
	JsonTextReader_tDF3061D2FE1D0761465A40B9EB90A0CAA79811DA_CustomAttributesCacheGenerator_JsonTextReader_set_Context_m069A333FD72B60850A4E39856CFD571EA1898C96,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_0_m00F71BA12BF0DF57209A1FA5EB4AC74CBF1D7AD0,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_1_m7E0978EBA0D4C9280D6B13548E06017B2C489EEF,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_2_mEFA85D5013760DB520F6AA531A4BF42EEDA15E53,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_3_m152BBB040A5EAD6076048DC9489202C785226101,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_4_m5FA0551D5DEFD265D9A9B57F87A8BE34B521430B,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_5_m07D35A1038F73C248085CC9475E7218080ADA93E,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_6_mD3F37E0C19AC407882B4D84567A27741C932EED4,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_7_m1BCD4F188A91ECC086AE072A5614776ABE1ABAC2,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_8_m99831C0576687FD9A72616944CDC61CE44688C02,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_9_mA12EA1F7DAD28B8ACE68647E4968263354396D09,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_10_m1F1437E82B74BE9E17270D222A8933249E86825F,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_11_m2C8BEAAB1431C1A4BE9E8D96A707D19E8258E1AA,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_12_mD0490FAD60A6EF8DD4165446E007688F014464DA,
	SerializationNodeDataReader_tF5582DC0C75CA522C5B005F477F25BE84CA5C421_CustomAttributesCacheGenerator_SerializationNodeDataReader_U3C_ctorU3Eb__6_13_m3F7320E1559494FD26B6AC7265D7FA0C32622ADA,
	BaseFormatter_1_tEAB4C26ECD9655135EFAB77098E8BD15164E5C4C_CustomAttributesCacheGenerator_BaseFormatter_1_InvokeOnDeserializingCallbacks_mADC31F2BE377509D9968F4F04A57958B1E4C67BD,
	ReflectionFormatter_1_t041552A31A0285682FCAFEC5B5E7C85D8A27F176_CustomAttributesCacheGenerator_ReflectionFormatter_1_get_OverridePolicy_mE987D62C97DFF9DAAD3675A05B61CBF63FB746EC,
	ReflectionFormatter_1_t041552A31A0285682FCAFEC5B5E7C85D8A27F176_CustomAttributesCacheGenerator_ReflectionFormatter_1_set_OverridePolicy_mF92145F0C3290D4588B3339C22710AC1E0FDDDAA,
	DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_get_StringReferenceResolver_m2E6A2B2DD825C6A09328D89BBA8E51DE9C39CF7A,
	DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_set_StringReferenceResolver_mDCA00F48D400266293A8047DD7691313DDD7398E,
	DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_get_GuidReferenceResolver_m07C4BC6749087DE670A55821EE76E3FDC4763842,
	DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_set_GuidReferenceResolver_mFBE1143558704260CC3056661B695858DA0CBD49,
	DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_get_IndexReferenceResolver_mA9574D5FABC6706AA16F89CEE489D3D69CD169C7,
	DeserializationContext_tACB532F7FCBA43A0B07147614B081FCD180CDD4E_CustomAttributesCacheGenerator_DeserializationContext_set_IndexReferenceResolver_m085C6EE44AC7431CF98E3AB8DA0B6512274E2E34,
	PreviouslySerializedAsAttribute_t5EA4E81CF3C83A7C162E599D2F5A904BD6E941E3_CustomAttributesCacheGenerator_PreviouslySerializedAsAttribute_get_Name_mD6B7A827CFCBB02C52D4361C71607E0ACA5DCC72,
	PreviouslySerializedAsAttribute_t5EA4E81CF3C83A7C162E599D2F5A904BD6E941E3_CustomAttributesCacheGenerator_PreviouslySerializedAsAttribute_set_Name_m827CB90ACB0522D52FDD1273C1A88C86FC5128AB,
	SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_get_IndexReferenceResolver_mDA9EA854A29E80106218C2A9181669388AEFF7C2,
	SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_set_IndexReferenceResolver_m30246FA74C05A8929BB601618D7D2F1C054170C1,
	SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_get_StringReferenceResolver_m8951CAFED2DD652D8BEF984D1F9D23397F5E61ED,
	SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_set_StringReferenceResolver_mD8F0F1C09F84790CA6B0E1036C8A98882112D8FE,
	SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_get_GuidReferenceResolver_mDDB9FC152AB8A6A7A8B3D67105C2385820974430,
	SerializationContext_t7F0719C20676A53B1CBAB51EA2786FA1F8F23BEC_CustomAttributesCacheGenerator_SerializationContext_set_GuidReferenceResolver_mBDB3D466481A10A762C1938D0FBD23198F13D5A6,
	Serializer_t8404F4E338393D41488CF8BF3366A1203D3BF8B8_CustomAttributesCacheGenerator_Serializer_FireOnSerializedType_mA5E91B9D9502A09C9C5097AC97AE35CC3CEA9715,
	Serializer_1_t8511303B6099B7360248A85E80B302FEA8CD6569_CustomAttributesCacheGenerator_Serializer_1_FireOnSerializedType_mD4039F0C70A381362D3033CB72E0406E059F83EC,
	UnitySerializationInitializer_tEF7D5931C3AAD0E36296D9929BFFA06CA6650070_CustomAttributesCacheGenerator_UnitySerializationInitializer_get_CurrentPlatform_mF269C7AAC83FC078F94F1116119D796ECB5820EB,
	UnitySerializationInitializer_tEF7D5931C3AAD0E36296D9929BFFA06CA6650070_CustomAttributesCacheGenerator_UnitySerializationInitializer_set_CurrentPlatform_m55348AA2C668D1C114F90BB660ECC5382C0CD4F0,
	UnitySerializationInitializer_tEF7D5931C3AAD0E36296D9929BFFA06CA6650070_CustomAttributesCacheGenerator_UnitySerializationInitializer_InitializeRuntime_mEC62F8B5E6AC16C44DAB3EFF5606F96ECFD1EB6B,
	EmittedAssemblyAttribute_t9F4B69CDFAF87216A4620BE079A248C5DA8A9943_CustomAttributesCacheGenerator_EmittedAssemblyAttribute__ctor_mF6C4FA2B76A0A7C473E38F0F3E4CF2D508FA05E7,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14__ctor_m42E920DFA0EC2ED211ABF66260FC284F6C948795,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_IDisposable_Dispose_m213294EE73C9FD0BA4AE290DCFC4F05D8B0E372C,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m9DE49E3A7D769B602A0780838785918940AA3CA9,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_Reset_m8F12030F2DE4AA1B634162D7F8E2B88FD1FCB392,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_get_Current_m5D0857D4C63F8F25DE3DB3350C16B277AC731EBB,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m120AA74EBA3617562BB5538F033FE57DE2E29276,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_t8523C5C14585D77475EFC56420280110AE9C7B7A_CustomAttributesCacheGenerator_U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m9900CD20243E8E7059331DC685EE00C73F9AE343,
	DictionaryKeyUtility_t75EF696995E360AD629BBA3331464FE95BC84FBD_CustomAttributesCacheGenerator_DictionaryKeyUtility_FromTo_mB0304339664BD0C90DEA2A7365EF3E633A78560C,
	U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14__ctor_m6FB68B817C2FDF39AC91F2875C7B1DE111D2A0AC,
	U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_IDisposable_Dispose_mB1F04B3D3BF5E0BD68489B69C56E1B7B3ACFB997,
	U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m29217F8D66F77F53E359602C11B363F197DC05A6,
	U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_Reset_mCAEC91788846F84F85BACA14A4340A852DC6FE97,
	U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_get_Current_m94C8A47343BA3A294EB02E6F7B29AC703DC1281F,
	U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m06387F1609C0D44AAFDBF33F4B5BE1AEA8396E2C,
	U3CGetPersistentPathKeyTypesU3Ed__14_tBE9C4A5F9D5D751C71EA2101CE12A1F1BDA40A88_CustomAttributesCacheGenerator_U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m5D3536FEF22563E69093808247E92FCC1BB339EB,
	FieldInfoExtensions_t5CA2C580D159B86450882F3F196A1FE3494BFAA0_CustomAttributesCacheGenerator_FieldInfoExtensions_IsAliasField_m1D38EAEE76FEF928BB693F75D5DFDEB70FAA1089,
	FieldInfoExtensions_t5CA2C580D159B86450882F3F196A1FE3494BFAA0_CustomAttributesCacheGenerator_FieldInfoExtensions_DeAliasField_m1E13F279646C60998D6805F037E9BB465A732234,
	GarbageFreeIterators_t6634156C3A461E01CE722CE84EEF03463B06375F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_mC9D4CE22738DA8F212B75F501E2AEBD6F47939D7,
	GarbageFreeIterators_t6634156C3A461E01CE722CE84EEF03463B06375F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_m01169C32FBD4FD4B7AB3143A73C5B9D99DFC74DB,
	GarbageFreeIterators_t6634156C3A461E01CE722CE84EEF03463B06375F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFValueIterator_m2C54801D14CA35281108B7B5458176DB267BCFD0,
	GarbageFreeIterators_t6634156C3A461E01CE722CE84EEF03463B06375F_CustomAttributesCacheGenerator_GarbageFreeIterators_GFIterator_mF15181EFD74EF2F00E4B1F2B370E189A99218D32,
	LinqExtensions_t2ADAB361A5FF1B8551AA91AA50844CB055BFAD01_CustomAttributesCacheGenerator_LinqExtensions_ForEach_m682ECE7184ABDEE663968140F44F4249197044B1,
	LinqExtensions_t2ADAB361A5FF1B8551AA91AA50844CB055BFAD01_CustomAttributesCacheGenerator_LinqExtensions_ForEach_mDBAFDD8C659C6B3E3C4754DD5B2ECC5904A31969,
	LinqExtensions_t2ADAB361A5FF1B8551AA91AA50844CB055BFAD01_CustomAttributesCacheGenerator_LinqExtensions_Append_m31D827BC31116AD1AC12E381456D6E78A5CFC5BB,
	U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1__ctor_m5BCE362117CD75F937057F3B000A3A6A824B0111,
	U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_IDisposable_Dispose_mEBBCAECAAC7C6B9E8B8A556BE1B27A1B861C5D14,
	U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4A976CB2DE3D630629F9328DC46109C2DC3FE76C,
	U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_Collections_IEnumerator_Reset_m0D5C1666419808DE9BD41B0FABBE76CCFA477E72,
	U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_Collections_IEnumerator_get_Current_m06F50BFF096ACA8EBE9B22FAF7E3408D556112E6,
	U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11936940F2E33D51086BE45198062B197A066F26,
	U3CAppendU3Ed__2_1_t01711A9FCBE0FFCA5B430505A07FBDEFB1A15EFA_CustomAttributesCacheGenerator_U3CAppendU3Ed__2_1_System_Collections_IEnumerable_GetEnumerator_mC3618D25545447E5B15DFB0E2D1556D1F0D39323,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_IsDefined_mBC7A546527246072348370BB9BD9490E18E401CA,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_IsDefined_m824D59E87ADD23158AF22274514F93721449EBC1,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttribute_m1FCCAE4D679AF90869A1FD1F049A687855701B1F,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttribute_m3A9D9535F2735CD89EB5A0530F848730BB87A1A1,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_mCA942852D862D5ECDA4791B7D7716DD49C16E041,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_mD24310354E86F67E15F47D5CCC4CCB21938A735C,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m349694041FCA1FF2377E50DB8AE258AD3139F5D5,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetAttributes_m5B61F53BDD320500874039F20CE82C780CB4E90A,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_GetNiceName_mCD6D786C0268CD6B40DCB737D5A8892369EF7E6D,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_IsStatic_m0D65779AE20FC116BC260AF93A9398FAA167B8FD,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_IsAlias_m2008AF6C8F05D73C9A66C3261B677D850D6B6A2F,
	MemberInfoExtensions_t25D7AAA94FF96B0D74BBEDBBEC2A3AC096DE2676_CustomAttributesCacheGenerator_MemberInfoExtensions_DeAlias_m7A87BBC0F97D506668D2919E6FADE5FA3A04EB2F,
	MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_GetFullName_m7289EF1866E240A4D8166835E23D870CAE19116E,
	MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_GetParamsNames_m0BD5DB755CF0FB1222863A5A7C73BED573E1DF53,
	MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_GetFullName_m16F11391FA8EB6B9AF3604D0D68DEE379A6E4169,
	MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_IsExtensionMethod_mCBF921ED9BCA898FDA5BC4B01670C47D86F0CB1C,
	MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_IsAliasMethod_mC735E84942B6AA66BE3CF2337C6EE77FA3F66257,
	MethodInfoExtensions_t3EBE01CEC3E1F3F3140D5631DDDFE94463413881_CustomAttributesCacheGenerator_MethodInfoExtensions_DeAliasMethod_mDE8989617E2D4C45BD200463E09A2ABCFD541A59,
	PathUtilities_t702A32068A4F2FACF97E3E4E83BE622FA0F46AB5_CustomAttributesCacheGenerator_PathUtilities_HasSubDirectory_mA04B6A623ADEE2828CD0BD0C9A39E3699B0761B7,
	PropertyInfoExtensions_tE5E5148B32666C2E6B356F2F599FB2D061D6477E_CustomAttributesCacheGenerator_PropertyInfoExtensions_IsAutoProperty_mF5E9A6C194E664F9556D51B8D09007F8EAACE91C,
	PropertyInfoExtensions_tE5E5148B32666C2E6B356F2F599FB2D061D6477E_CustomAttributesCacheGenerator_PropertyInfoExtensions_IsAliasProperty_m87041E7971788AE6886143359B4B07B999125A2F,
	PropertyInfoExtensions_tE5E5148B32666C2E6B356F2F599FB2D061D6477E_CustomAttributesCacheGenerator_PropertyInfoExtensions_DeAliasProperty_m015DB168AC8F90AA5143293D1A290A60CF55A570,
	StringExtensions_tB67B0E35C92CE19EBCAB707D4EB905B98223E263_CustomAttributesCacheGenerator_StringExtensions_ToTitleCase_m75E36FAEAEB8CB6FC90F54B5B30BD5CC673DD0C6,
	StringExtensions_tB67B0E35C92CE19EBCAB707D4EB905B98223E263_CustomAttributesCacheGenerator_StringExtensions_IsNullOrWhitespace_mE7508CCAA9EA8CCC21D96229BD62C292539A1ED8,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCastDefined_mFE16EE4FE9FF3852768497633F01D0E991C3BBED,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_IsCastableTo_m12EF9C5160625DEE41CB62C3D8F2D2E7D378D51F,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCastMethodDelegate_m987046E7D47F282D654496261B7883C28D6D31DA,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCastMethod_mD03313876F01CE5085BFDB60AE52D9A338439CA9,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_m4C7325DD9F6B1A1DEDBCC2F283A28FE556681A52,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_m0BD313CB34C59F667C535D11BC7FDCE04318F0BE,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_mBF3910CB7A5294795886EE74D37329336842B6C8,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_m0100EFB063EDD278209AE9CE045A8785C73842BB,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_m807409621DE640D0570A47C2C7C93A23726B4918,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasCustomAttribute_m02DCC99327737793E7746DA3B4CCDECFAA6554ED,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetAttribute_m2A31B5E13E657B683512F65EB806A4D04C7EC644,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOrInherits_mBB069DC34FD0A3D5A9AAE45FF5FCC86D3A0AC3B9,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericType_m3FC6E38C111B4C1BF15919E393DE4258469E09E5,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericInterface_m36B2221D9263FB90DF896EDE0C20F294D955572F,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_ImplementsOpenGenericClass_mEF109A8BA5BADD6C482009E891DCAB86DDC3CEB9,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m6CB1DCD49EBE82C352539704EA4A74670B21C5C0,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_mD5EBA354E9E6A9607CFFEFCA448E418668E35056,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m61744A5C5CC21963B53858F744CE2B557E15E30A,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetOperatorMethod_mAC2F5581188D79F35D91677D1A9054EF842BBC26,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetOperatorMethods_m6D139F72C9221E06D5F6DF0C21F0656C2947C4BD,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_mAF1EC9227C3CDA77A78B41600741BC22B282B330,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_mD652499D27311B8B3E231622ECBC3223C8FEACA4,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetAllMembers_m243007EA85F625D6246997703D8FC2B22DA3E2E3,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetGenericBaseType_mFEF667CA3694CE4833C076C2465EC8BEC35856EA,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetGenericBaseType_mFB61C16F72798D16114C6F724FB868F9244345BC,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetBaseTypes_m983F94E0CFB953F1D94BC51BB7637490EE5DEB14,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetBaseClasses_mDC132249680C2F2FC1D237587A00841A013F61FC,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_TypeNameGauntlet_mE817E158A4A383649188FC15D0B79E2663C6499F,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetNiceName_m5520AD5A39ABAB082F2DD30222BCADBE03CD21B6,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetNiceFullName_mF291B98B75ECEB06240A18E2AA9BA2DE1F17A230,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCompilableNiceName_m4B236B397503B7C845545AF25337B7515CE1352C,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCompilableNiceFullName_m2599EDF5418ECCACF2B4E3DF981E9CFFCCD0B9E0,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttribute_m171FC165C46326A232661CCB254240740B3CEC23,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttribute_mFAC852EEBCAAEC6789E8A21AA075184863C0CA0F,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttributes_m47BA6BB60DE751653BFEDBD7DD8BF221D8D1DBD7,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetCustomAttributes_mE4E800AA23C47395FCB161ED16599FF7311891ED,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_IsDefined_mEB516F440FD69AB91905130287EEBF0A3938023D,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_IsDefined_m3188D2AA09EAC6C1165C335E33D7DAB3CC0C55CA,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_InheritsFrom_m497CA7EEBF216C6E671859C25551F64D4D193F5C,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_InheritsFrom_mB2C8CCD4BC2F89D305E9E82CED3E6D078F6B1C9D,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetInheritanceDistance_m0520A54AE277190031955B26C48CF1C4793B7AF3,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_HasParamaters_m8B781DA0243FDDE934633308D48B7006BB1393FF,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetReturnType_mE350B159933DEF36F13497D2648D1023927E0105,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetMemberValue_mA49CC5ACB73A92E1559601E6BCB9B465FCABED36,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_SetMemberValue_m535B84D7681E0D3ACD19641D7D7F6184C50D1DB7,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_TryInferGenericParameters_m16A99D765E67EBE6F6441327D86682E736D00457,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m8A5E53288BFDFBAD31CE121DFCEBD72215CD3799,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_mF6CC8438959153139AB1EDEE8A68FF75C184C64F,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GenericParameterIsFulfilledBy_m6CBD13619911088E757B92211349F616C9779E2D,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GenericParameterIsFulfilledBy_m2E2EE166F01A2BC075772E3AF0C462C13DAB3D23,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetGenericConstraintsString_m235BDED19C12FF498E7BE5515FFFE62B3EFBC917,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GetGenericParameterConstraintsString_m6150DD784FC1554DDCC958F42F821CA5D9631E5B,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GenericArgumentsContainsTypes_m8BC8C323DE7F8BDAE770B61B0FD7CAC68EF751FB,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_IsFullyConstructedGenericType_m9FA2CB4ECC617B95C129F22FBA6619B03786B236,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_IsNullableType_m9E8A5C8C3F6C473C6F086729A8966AA502A6AC5F,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_SafeGetTypes_m3E480DDB012372630A76753275F4063BCBCA515D,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_SafeIsDefined_mB09B7D31E0F9E2B39976557D61A5C5A7205D4F19,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_SafeGetCustomAttributes_m38CC04731F22628B3292D1A49D5CE61C2673BB25,
	U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42__ctor_mECC28ADDEDB2CFA0AD434C22F8369FA6DAC56390,
	U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_IDisposable_Dispose_m9E319A62EFB4C3E57BED82A892250F00FC9B43B8,
	U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mCE2F1CA338CF519DE8199720D6F10F60C833DC92,
	U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_Reset_mA8107830BE90C795E27D5738C36CD0D452AFAF82,
	U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_get_Current_m9D2C83C4E15A768D0658BBB1E95B8B963F88BD9E,
	U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mF7146FB637C47E3922C611D84C0849179F9D44DD,
	U3CGetAllMembersU3Ed__42_t1CD57AC7E2E25BE84780C41618118D74FA426132_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mF3BD4F1E1832E4D4DB4F6C5B6B1040A4530DECA4,
	U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43__ctor_mD6E740BB16530EB8DC13A05DC855CA6004C722C5,
	U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_IDisposable_Dispose_mFFD18EABCD5841589699BECB72FE46280928EB67,
	U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m32C3AE5D7E414AF63C4E2BFCFE1B524C69C74E28,
	U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_Reset_m0A334D28C9D62519A97501CC7530ECF74CB89295,
	U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_get_Current_m37C697AEF1C2BE80E9464F0667B0466CB9FD9842,
	U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m544D77F6409E37E5AAFDE93424D13976A215278D,
	U3CGetAllMembersU3Ed__43_tC9331F6E590FAF59B2976CF763929547B85E4D42_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__43_System_Collections_IEnumerable_GetEnumerator_mE50E2086242B0D28A1760315F8A023506D39B591,
	U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1__ctor_mD6E46E913C898268AD2E61EA0D4D361B04DDB589,
	U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_IDisposable_Dispose_m46F9947A5A2EAE2CFA9220202D4C638DA51933E7,
	U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m0FFFF7C630C49276F9BF8505F3D0C0F0BD20A064,
	U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_Collections_IEnumerator_Reset_m6D3C39654544D0F36CE096951A40A6BC60227CD4,
	U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_Collections_IEnumerator_get_Current_m593BA2C585B144DCF7E5EBFD6B6FB74E53207571,
	U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mEB3711712FC197B531BFD5D2BCDA1A8535E2883D,
	U3CGetAllMembersU3Ed__44_1_tB04655D93B5B0C8FE4BDDD95651B194AB9D5495F_CustomAttributesCacheGenerator_U3CGetAllMembersU3Ed__44_1_System_Collections_IEnumerable_GetEnumerator_m9BE37070F6DEF223C88B6695796D02E023631EB2,
	U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48__ctor_mC5020F833A3331D50ED3E4E46E31AF973C690C63,
	U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_IDisposable_Dispose_m42AFBCF3E84334C072FB5FDD3421EF847CB8EB46,
	U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_mCE1DBB20C09DA287D6B34CC1D3635B6D0889257F,
	U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_Reset_mCBDCB3D8C952B2FC5F8E7C5E65919C03719387F6,
	U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_get_Current_mD2EDA6986FDE391CBF36B8BA992B4C7E79FC0BD2,
	U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m3F002A1BBB23E235B788DBBBDE103CBBF592F3C7,
	U3CGetBaseClassesU3Ed__48_t67474043D434E0679AD9CBC1253B2F27A7EED481_CustomAttributesCacheGenerator_U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerable_GetEnumerator_m98F625F59DD4C3E6DF98E87E34A20BFEBB4C152F,
	UnityExtensions_t1CDF2900C99F7F790AC7DB98A0A42CAF10A41450_CustomAttributesCacheGenerator_UnityExtensions_SafeIsUnityNull_m148B87C1D426B3FED2F450A9776997A28ED580CF,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_mB98A496FC6D73EB5897246F5885D9ED7FAC7284A,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m14F4C94791AFA5DA05AE5DE20606B7D47011868F,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD01C87D86A2E129FDDE79B964FE84B546B589053,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m493988934AAA09863EA33E918C0579D8826E0454,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_tC1B046000608CFE32B75D50F7D96C00A9C4A521B_CustomAttributesCacheGenerator_U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m2343A58E3F14DD579E8F296C8063D129E1AF5E18,
	UnityVersion_t2F9DD8D6B1E46B65766456A24C7907DDABBE9005_CustomAttributesCacheGenerator_UnityVersion_EnsureLoaded_m38F0696D643531BF201CD3DC509423ECEF98FCAB,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_TryInferGenericParameters_m16A99D765E67EBE6F6441327D86682E736D00457____knownParameters2,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_m8A5E53288BFDFBAD31CE121DFCEBD72215CD3799____parameters1,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_AreGenericConstraintsSatisfiedBy_mF6CC8438959153139AB1EDEE8A68FF75C184C64F____parameters1,
	TypeExtensions_t9E454D578E602F76BC904178531EFEB1789FCAE4_CustomAttributesCacheGenerator_TypeExtensions_GenericArgumentsContainsTypes_m8BC8C323DE7F8BDAE770B61B0FD7CAC68EF751FB____types1,
	FormatterLocator_t0C95C16EA50270BEE2CAD20C270EEBDAAA9C1FE2_CustomAttributesCacheGenerator_FormatterLocator_t0C95C16EA50270BEE2CAD20C270EEBDAAA9C1FE2____FormatterResolve_EventInfo,
	BaseDataReaderWriter_tD27FCD3D1F50001A7EABB0E3FA09FDAC35B3346C_CustomAttributesCacheGenerator_BaseDataReaderWriter_tD27FCD3D1F50001A7EABB0E3FA09FDAC35B3346C____Binder_PropertyInfo,
	IDataReader_tF95D16F218C77A35EE0240E5D66CD5AE04E4882F_CustomAttributesCacheGenerator_IDataReader_tF95D16F218C77A35EE0240E5D66CD5AE04E4882F____Stream_PropertyInfo,
	IDataWriter_t7D35BDD1175A987477E7233AE7424D2255F7ED24_CustomAttributesCacheGenerator_IDataWriter_t7D35BDD1175A987477E7233AE7424D2255F7ED24____Stream_PropertyInfo,
	SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF_CustomAttributesCacheGenerator_SerializationData_t020D3C87415239E0DCEDCB3C00C9D58F146542AF____HasEditorData_PropertyInfo,
	Sirenix_Serialization_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_allowMultiple_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_inherited_2(L_0);
		return;
	}
}
