﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// Sirenix.OdinInspector.DontApplyToListElementsAttribute
struct DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143;
// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// Sirenix.OdinInspector.IncludeMyAttributesAttribute
struct IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833;
// JetBrains.Annotations.MeansImplicitUseAttribute
struct MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// Sirenix.OdinInspector.ShowInInspectorAttribute
struct ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41;
// System.String
struct String_t;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct  AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct  AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct  AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct  AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct  AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.CLSCompliantAttribute
struct  CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.CLSCompliantAttribute::m_compliant
	bool ___m_compliant_0;

public:
	inline static int32_t get_offset_of_m_compliant_0() { return static_cast<int32_t>(offsetof(CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249, ___m_compliant_0)); }
	inline bool get_m_compliant_0() const { return ___m_compliant_0; }
	inline bool* get_address_of_m_compliant_0() { return &___m_compliant_0; }
	inline void set_m_compliant_0(bool value)
	{
		___m_compliant_0 = value;
	}
};


// System.Runtime.InteropServices.ComVisibleAttribute
struct  ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::_val
	bool ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A, ____val_0)); }
	inline bool get__val_0() const { return ____val_0; }
	inline bool* get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(bool value)
	{
		____val_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.DontApplyToListElementsAttribute
struct  DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.InteropServices.GuidAttribute
struct  GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.InteropServices.GuidAttribute::_val
	String_t* ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063, ____val_0)); }
	inline String_t* get__val_0() const { return ____val_0; }
	inline String_t** get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(String_t* value)
	{
		____val_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____val_0), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct  HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.IncludeMyAttributesAttribute
struct  IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ObsoleteAttribute
struct  ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Sirenix.OdinInspector.ShowInInspectorAttribute
struct  ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct  AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// JetBrains.Annotations.ImplicitUseKindFlags
struct  ImplicitUseKindFlags_t9E7B1B7981A84EE60A1814447FAC4D8A90E1FDD2 
{
public:
	// System.Int32 JetBrains.Annotations.ImplicitUseKindFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImplicitUseKindFlags_t9E7B1B7981A84EE60A1814447FAC4D8A90E1FDD2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// JetBrains.Annotations.ImplicitUseTargetFlags
struct  ImplicitUseTargetFlags_t4DEA69C7F55B58E0A400DA7518460D0656125124 
{
public:
	// System.Int32 JetBrains.Annotations.ImplicitUseTargetFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImplicitUseTargetFlags_t4DEA69C7F55B58E0A400DA7518460D0656125124, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct  AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// JetBrains.Annotations.MeansImplicitUseAttribute
struct  MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// JetBrains.Annotations.ImplicitUseKindFlags JetBrains.Annotations.MeansImplicitUseAttribute::<UseKindFlags>k__BackingField
	int32_t ___U3CUseKindFlagsU3Ek__BackingField_0;
	// JetBrains.Annotations.ImplicitUseTargetFlags JetBrains.Annotations.MeansImplicitUseAttribute::<TargetFlags>k__BackingField
	int32_t ___U3CTargetFlagsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUseKindFlagsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A, ___U3CUseKindFlagsU3Ek__BackingField_0)); }
	inline int32_t get_U3CUseKindFlagsU3Ek__BackingField_0() const { return ___U3CUseKindFlagsU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CUseKindFlagsU3Ek__BackingField_0() { return &___U3CUseKindFlagsU3Ek__BackingField_0; }
	inline void set_U3CUseKindFlagsU3Ek__BackingField_0(int32_t value)
	{
		___U3CUseKindFlagsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTargetFlagsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A, ___U3CTargetFlagsU3Ek__BackingField_1)); }
	inline int32_t get_U3CTargetFlagsU3Ek__BackingField_1() const { return ___U3CTargetFlagsU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTargetFlagsU3Ek__BackingField_1() { return &___U3CTargetFlagsU3Ek__BackingField_1; }
	inline void set_U3CTargetFlagsU3Ek__BackingField_1(int32_t value)
	{
		___U3CTargetFlagsU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172 (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * __this, bool ___visibility0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270 (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * __this, bool ___isCompliant0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * __this, String_t* ___guid0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.IncludeMyAttributesAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IncludeMyAttributesAttribute__ctor_m791A833A21BD2049C497C812D1A1DE4F8532187E (IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.ShowInInspectorAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233 (ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41 * __this, const RuntimeMethod* method);
// System.Void Sirenix.OdinInspector.DontApplyToListElementsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4 (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, bool ___error1, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.MeansImplicitUseAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeansImplicitUseAttribute__ctor_m551D20F17D1290E1F1D5CD516242DEFBBC67B7A4 (MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A * __this, const RuntimeMethod* method);
static void Sirenix_OdinInspector_Attributes_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[0];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x32\x2E\x31\x2E\x36"), NULL);
	}
	{
		ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * tmp = (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A *)cache->attributes[1];
		ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172(tmp, false, NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[2];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[3];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x32\x30\x31\x37"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[4];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x72\x65\x6E\x69\x78\x2E\x4F\x64\x69\x6E\x49\x6E\x73\x70\x65\x63\x74\x6F\x72\x2E\x41\x74\x74\x72\x69\x62\x75\x74\x65\x73"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[5];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x72\x65\x6E\x69\x78\x20\x49\x56\x53"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[6];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[7];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[8];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x72\x65\x6E\x69\x78\x2E\x4F\x64\x69\x6E\x49\x6E\x73\x70\x65\x63\x74\x6F\x72\x2E\x41\x74\x74\x72\x69\x62\x75\x74\x65\x73"), NULL);
	}
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[9];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[10];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[11];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[12];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * tmp = (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 *)cache->attributes[13];
		GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA(tmp, il2cpp_codegen_string_new_wrapper("\x39\x38\x31\x37\x39\x36\x44\x41\x2D\x36\x39\x45\x44\x2D\x34\x32\x43\x34\x2D\x41\x30\x32\x37\x2D\x30\x36\x44\x35\x37\x36\x37\x38\x36\x39\x37\x33"), NULL);
	}
}
static void AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AssetsOnlyAttribute_tCC6A1D3E6DBF4014A229FFADCF481275DC8CE8EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void ButtonGroupAttribute_t20631D3282E17CD252851E3A40703BA4F5EB4A86_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833 * tmp = (IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833 *)cache->attributes[0];
		IncludeMyAttributesAttribute__ctor_m791A833A21BD2049C497C812D1A1DE4F8532187E(tmp, NULL);
	}
	{
		ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41 * tmp = (ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41 *)cache->attributes[1];
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[2];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void CustomValueDrawerAttribute_t7F42DE8AE9C58EB688259D42B09473C0DEE98A31_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void DisableInInlineEditorsAttribute_t2EB18DF22DC1A708520C4AD34151B95A7D570744_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void DisableInNonPrefabsAttribute_tE0BD101B759B56A43490981CAC42FC73F4AB81CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void DisableInPrefabAssetsAttribute_t5A0A0810282C07F9A30A66E2B829A080B9B97BC1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void DisableInPrefabInstancesAttribute_t88BB1DCC0C1B7F899C6C07C65B1CA463D2506DDC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void DisableInPrefabsAttribute_t2612557CCF5BD1035F1E8990029DDD1A3D2A33F7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void EnableGUIAttribute_tC43B92B9971628498DF2B4F91C1667F357265CB0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void EnumPagingAttribute_tF54A51875E6BA517B1D30C52BA323866A891FB1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator_RequireValidPath(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x52\x65\x71\x75\x69\x72\x65\x45\x78\x69\x73\x74\x69\x6E\x67\x50\x61\x74\x68\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator_U3CReadOnlyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator_FilePathAttribute_get_ReadOnly_m3A6314406C2D58C15799C01B671EE88CC4399D70(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator_FilePathAttribute_set_ReadOnly_mC86951B771DBB611C650516E350D4DA4EADA3ECD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator_FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458____ReadOnly_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x20\x61\x20\x52\x65\x61\x64\x4F\x6E\x6C\x79\x20\x61\x74\x74\x72\x69\x62\x75\x74\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389_CustomAttributesCacheGenerator_RequireValidPath(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x52\x65\x71\x75\x69\x72\x65\x45\x78\x69\x73\x74\x69\x6E\x67\x50\x61\x74\x68\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void HideInInlineEditorsAttribute_t30CAEEF70FDB341465674DA00FB39A43320F868B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void HideInNonPrefabsAttribute_t0BF8A36CF5FAE4EF1217ED0B4F1285A3098A594B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void HideInPrefabAssetsAttribute_tB9D268E4438857EE5032A747B4629682092E69D6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void HideInPrefabInstancesAttribute_t439BFC26506199B446892DFB6C329D8DE37DA492_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void HideInPrefabsAttribute_tB539C9027320E92429C7F183A8D0B488213D7CF4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void HideInTablesAttribute_t110DFA0CFAC26EBCE8B6C9086B6DC345A0D8B47B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void HideNetworkBehaviourFieldsAttribute_tF2720B20E003A5A3947466638AC7081930321BD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void InlinePropertyAttribute_tC4F6EE65AF6B4B9F3D0995C379732DA61E459158_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void LabelWidthAttribute_t2E4C87DC106B9C9E3D7E47E038D3161026726A8B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_U3CDrawValueLabelHasValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_U3CValueLabelAlignmentHasValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_ProgressBarAttribute_get_DrawValueLabelHasValue_m06BCD66013F03A68A6C08FF5D7A49AEF316F71D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_ProgressBarAttribute_get_ValueLabelAlignmentHasValue_m7DC1402D717D0D4EE8E72DF81C941D244509A447(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void PropertySpaceAttribute_tDF729E8EB760A22841021D50F961B59FAB282048_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833 * tmp = (IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833 *)cache->attributes[0];
		IncludeMyAttributesAttribute__ctor_m791A833A21BD2049C497C812D1A1DE4F8532187E(tmp, NULL);
	}
	{
		ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41 * tmp = (ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41 *)cache->attributes[1];
		ShowInInspectorAttribute__ctor_m347C9F753DAEF0EE7B7B17F0E4E8DFA710303233(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[2];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ShowInInlineEditorsAttribute_t074163930680127467E651ECE68C8A44528E93D2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void SuffixLabelAttribute_t990C9A8E87740E342FB14097884866F5D3A9869C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void TableColumnWidthAttribute_t62FFBE33C21C9BCA7419C73D67C20BBF291671BF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9_CustomAttributesCacheGenerator_showPagingHasValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9_CustomAttributesCacheGenerator_showPaging(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void TypeFilterAttribute_t7989849079B6440606CBDA93E5AE67B4877A56FE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void TypeInfoBoxAttribute_t43B3E84724467282A1E3696E35325EF8C66E4BBD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1036LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
}
static void OdinRegisterAttributeAttribute_t8A82A1C5F996F02644E959C5EAD6533EBF268FFA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ColorPaletteAttribute_t89BD14DE6C9A17B37A64A8E188A49AB7621D759C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void CustomContextMenuAttribute_tE9AFF948A16686A5D0C8831B5383270F7134AB49_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void DelayedPropertyAttribute_t18B4307EE458D335803442517336AE1208186A88_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void DetailedInfoBoxAttribute_t8CEBCE93D61675CB38CA659984F4C121381C565E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void DisableContextMenuAttribute_t1EC42DF3D6D965B29C1578354756628012429BC8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void DisableIfAttribute_t4938E088E077FE7ABA2C514DBF3B3CF42BE41771_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void DisableInEditorModeAttribute_t50BED704C92D71F81B99C7F72118468D918FF96D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void DisableInPlayModeAttribute_t62870C62E5C7F02C470B68549E1323500520604C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void DisplayAsStringAttribute_tC0C89AF4B51CB834ED7AD9AB028D617487E7297F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
}
static void DrawWithUnityAttribute_t22A1203943EDA03AB38CF29B28743CB94753A153_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_U3CMemberMethodU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_U3CLabelU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_InlineButtonAttribute_get_MemberMethod_mECE123FE218739415AE473D16E36D3FCF36042EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_InlineButtonAttribute_set_MemberMethod_mF0C22FEE372BFE43D98A7B8FDD6BD29365CEDAE8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_InlineButtonAttribute_get_Label_m06C931996AADA45AD653F209A5713A4AB09135E0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_InlineButtonAttribute_set_Label_m24E6BE08859E432C9819B26B95BED6F00C3DC0D0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShowForPrefabOnlyAttribute_tF680D3BAFA9AF8C0C8BE3A0EC1DB9380BCB319D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x48\x69\x64\x65\x49\x6E\x50\x72\x65\x66\x61\x62\x49\x6E\x73\x74\x61\x6E\x63\x65\x20\x6F\x72\x20\x48\x69\x64\x65\x49\x6E\x50\x72\x65\x66\x61\x62\x41\x73\x73\x65\x74\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), false, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void EnableForPrefabOnlyAttribute_t179CCCCF9D183ACF7AAAB884F147EEAB89BBFFDB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x44\x69\x73\x61\x62\x6C\x65\x49\x6E\x50\x72\x65\x66\x61\x62\x49\x6E\x73\x74\x61\x6E\x63\x65\x20\x6F\x72\x20\x44\x69\x73\x61\x62\x6C\x65\x49\x6E\x50\x72\x65\x66\x61\x62\x41\x73\x73\x65\x74\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), false, NULL);
	}
}
static void EnableIfAttribute_t3B8036F797E23325323E2064C7CCEA71BAAF7BE9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void HideIfAttribute_t11B1419645C36889523DAB5156404E47D08167B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void HideInPlayModeAttribute_t5A71EEB1100BED2A35EF076EF6B99B628C7E32A7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void HideInEditorModeAttribute_tA80C5951E867A0D27A1C64176166E457B9C4C69F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void HideMonoScriptAttribute_tF0E9B439B2C4AF5DF40AA4408216484CBC611A18_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void HideReferenceObjectPickerAttribute_t904F2AE6C1E46F3630F27B5717DAA2A078240400_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void SuppressInvalidAttributeErrorAttribute_t8CCDC91E2DECC329BCDF59102809D220CCF7794E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ShowDrawerChainAttribute_tAF05CAAEC33491EA7281A449128A45877A66A7CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_CustomAttributesCacheGenerator_U3CHasDefinedExpandedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_CustomAttributesCacheGenerator_FoldoutGroupAttribute_get_HasDefinedExpanded_mF65E7E54B706CCDE7EA9DB0C3606F2B10DCFCB25(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_CustomAttributesCacheGenerator_FoldoutGroupAttribute_set_HasDefinedExpanded_mABAA99EDDE953EEF83E4F04F80DAC78D2E3851B3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GUIColorAttribute_t7E1FEBC8732A8232FE82C68019C98709FD244DB4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void HideLabelAttribute_t8CFE493B307FE0E07EA75618A406700A44FD2B00_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
	}
}
static void IndentAttribute_tE8DFFDF7D9778532690A56BC8145CC86AA6C74EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void InfoBoxAttribute_t8D2A3D9022D85EC4424CAB78C71C24C8BF8AEFD5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ToggleLeftAttribute_t4CC8B95C528CF91FEA46AABBC6237B2258FD77C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void LabelTextAttribute_tAC01C34A3856E3C62B39EA8A4412BFE522E35D27_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void MaxValueAttribute_tFC98EB9F62599D96C7A430775FE8543EC98309BC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void MinValueAttribute_t45E8A787D2858D90D37F7A87C0582C0BF131FA34_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void MultiLinePropertyAttribute_tD35BC318DFDFAC8469DA2D276235CCF0E59B319D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void PropertyTooltipAttribute_t7BC0BF969C21340C1CA2B7180CFD04350B79204B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ReadOnlyAttribute_t4A1BE30F9257C5E0187EFB8FC659A42F4B61472C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void OnInspectorGUIAttribute_t1CCCB842BCEBEB2AB07C1298B7FFBAB7571B0F2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void OnValueChangedAttribute_tFE13CD46B1B5466F8AA8C351562C9D3BB9FB7623_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void PropertyOrderAttribute_t2F83A60EEA2D6D2C91E10D2A83E22326F5F71936_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void RequiredAttribute_t0941212FA1DC5F51F3309F0EB27016EE914366D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void SceneObjectsOnlyAttribute_t74D00B9886B272BE983F0AE1F447B56E222402A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A * tmp = (MeansImplicitUseAttribute_tA3580E7EE13563A0C3801DE5F0E74624B97CA51A *)cache->attributes[0];
		MeansImplicitUseAttribute__ctor_m551D20F17D1290E1F1D5CD516242DEFBBC67B7A4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_CustomAttributesCacheGenerator_U3CTabsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_CustomAttributesCacheGenerator_TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_CustomAttributesCacheGenerator_TabGroupAttribute_set_Tabs_m5BB49FEBE6589933B4F2B96B581E9EE4DC5838B4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[0];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[1];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ToggleAttribute_tE3BA633AE4C4A847529A9FEB83DFF3C17CB83F2B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
static void ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator_U3CTitleStringMemberNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator_ToggleGroupAttribute__ctor_mCE42334D8F057E33C717935C60446068B17B1AE8(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x5B\x54\x6F\x67\x67\x6C\x65\x47\x72\x6F\x75\x70\x28\x22\x74\x6F\x67\x67\x6C\x65\x4D\x65\x6D\x62\x65\x72\x4E\x61\x6D\x65\x22\x2C\x20\x67\x72\x6F\x75\x70\x54\x69\x74\x6C\x65\x3A\x20\x22\x24\x74\x69\x74\x6C\x65\x53\x74\x72\x69\x6E\x67\x4D\x65\x6D\x62\x65\x72\x4E\x61\x6D\x65\x22\x29\x5D\x20\x69\x6E\x73\x74\x65\x61\x64"), NULL);
	}
}
static void ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator_ToggleGroupAttribute_get_TitleStringMemberName_m7A12EF988A3F9A85FAAAEF843B62E52C0BF10D20(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator_ToggleGroupAttribute_set_TitleStringMemberName_m3354817FD279639CAE69FF7C3B431854763B55CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator_ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00____TitleStringMemberName_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x20\x61\x20\x24\x20\x69\x6E\x66\x72\x6F\x6E\x74\x20\x6F\x66\x20\x67\x72\x6F\x75\x70\x20\x74\x69\x74\x6C\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x2C\x20\x69\x2E\x65\x3A\x20\x22\x24\x4D\x79\x53\x74\x72\x69\x6E\x67\x4D\x65\x6D\x62\x65\x72\x22\x2E"), NULL);
	}
}
static void ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE_CustomAttributesCacheGenerator_ValidateInputAttribute__ctor_m64BCF6EB7B972FD8563BA5A7E14CFEA80D122038(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6A\x65\x63\x74\x69\x6E\x67\x20\x69\x6E\x76\x61\x6C\x69\x64\x20\x69\x6E\x70\x75\x74\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x6F\x74\x68\x65\x72\x20\x63\x6F\x6E\x73\x74\x72\x75\x63\x74\x6F\x72\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), false, NULL);
	}
}
static void ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE_CustomAttributesCacheGenerator_ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE____ContiniousValidationCheck_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x65\x20\x43\x6F\x6E\x74\x69\x6E\x75\x6F\x75\x73\x56\x61\x6C\x69\x64\x61\x74\x69\x6F\x6E\x43\x68\x65\x63\x6B\x20\x6D\x65\x6D\x62\x65\x72\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void ShowIfAttribute_tDF7F785B12F1489ED80C78FC2695E7363CC5B8AE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
	{
		DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 * tmp = (DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143 *)cache->attributes[1];
		DontApplyToListElementsAttribute__ctor_m93B57B417AE48061722137D8F472C7113CBC14F4(tmp, NULL);
	}
}
static void WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32767LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, true, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Sirenix_OdinInspector_Attributes_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Sirenix_OdinInspector_Attributes_AttributeGenerators[129] = 
{
	AssetListAttribute_t2623F29E1A767B57C0B06CAADC9ACAAB132DDDE7_CustomAttributesCacheGenerator,
	U3CU3Ec_tEDE02B8221938FED3BBED89DCF955F81EFE3AEC6_CustomAttributesCacheGenerator,
	AssetsOnlyAttribute_tCC6A1D3E6DBF4014A229FFADCF481275DC8CE8EE_CustomAttributesCacheGenerator,
	BoxGroupAttribute_t970934F291F11C612D977748DCE4F06E8D4E322C_CustomAttributesCacheGenerator,
	ButtonAttribute_t2D9F5E2C41DA51F2700CBC306B4B6391EECC4782_CustomAttributesCacheGenerator,
	ButtonGroupAttribute_t20631D3282E17CD252851E3A40703BA4F5EB4A86_CustomAttributesCacheGenerator,
	CustomValueDrawerAttribute_t7F42DE8AE9C58EB688259D42B09473C0DEE98A31_CustomAttributesCacheGenerator,
	DisableInInlineEditorsAttribute_t2EB18DF22DC1A708520C4AD34151B95A7D570744_CustomAttributesCacheGenerator,
	DisableInNonPrefabsAttribute_tE0BD101B759B56A43490981CAC42FC73F4AB81CE_CustomAttributesCacheGenerator,
	DisableInPrefabAssetsAttribute_t5A0A0810282C07F9A30A66E2B829A080B9B97BC1_CustomAttributesCacheGenerator,
	DisableInPrefabInstancesAttribute_t88BB1DCC0C1B7F899C6C07C65B1CA463D2506DDC_CustomAttributesCacheGenerator,
	DisableInPrefabsAttribute_t2612557CCF5BD1035F1E8990029DDD1A3D2A33F7_CustomAttributesCacheGenerator,
	EnableGUIAttribute_tC43B92B9971628498DF2B4F91C1667F357265CB0_CustomAttributesCacheGenerator,
	EnumPagingAttribute_tF54A51875E6BA517B1D30C52BA323866A891FB1F_CustomAttributesCacheGenerator,
	FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator,
	FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389_CustomAttributesCacheGenerator,
	HideInInlineEditorsAttribute_t30CAEEF70FDB341465674DA00FB39A43320F868B_CustomAttributesCacheGenerator,
	HideInNonPrefabsAttribute_t0BF8A36CF5FAE4EF1217ED0B4F1285A3098A594B_CustomAttributesCacheGenerator,
	HideInPrefabAssetsAttribute_tB9D268E4438857EE5032A747B4629682092E69D6_CustomAttributesCacheGenerator,
	HideInPrefabInstancesAttribute_t439BFC26506199B446892DFB6C329D8DE37DA492_CustomAttributesCacheGenerator,
	HideInPrefabsAttribute_tB539C9027320E92429C7F183A8D0B488213D7CF4_CustomAttributesCacheGenerator,
	HideInTablesAttribute_t110DFA0CFAC26EBCE8B6C9086B6DC345A0D8B47B_CustomAttributesCacheGenerator,
	HideNetworkBehaviourFieldsAttribute_tF2720B20E003A5A3947466638AC7081930321BD3_CustomAttributesCacheGenerator,
	InlinePropertyAttribute_tC4F6EE65AF6B4B9F3D0995C379732DA61E459158_CustomAttributesCacheGenerator,
	LabelWidthAttribute_t2E4C87DC106B9C9E3D7E47E038D3161026726A8B_CustomAttributesCacheGenerator,
	PreviewFieldAttribute_t6C10D5A3475A434DCD300730E6815E7DBF3348D4_CustomAttributesCacheGenerator,
	ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator,
	PropertyRangeAttribute_t112689409F5AAC37E0F7292E0CFE151897B049C0_CustomAttributesCacheGenerator,
	PropertySpaceAttribute_tDF729E8EB760A22841021D50F961B59FAB282048_CustomAttributesCacheGenerator,
	ResponsiveButtonGroupAttribute_t45A915F984DABE325CD9B4126E9D1F62105FA0C9_CustomAttributesCacheGenerator,
	ShowInInlineEditorsAttribute_t074163930680127467E651ECE68C8A44528E93D2_CustomAttributesCacheGenerator,
	SuffixLabelAttribute_t990C9A8E87740E342FB14097884866F5D3A9869C_CustomAttributesCacheGenerator,
	TableColumnWidthAttribute_t62FFBE33C21C9BCA7419C73D67C20BBF291671BF_CustomAttributesCacheGenerator,
	TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9_CustomAttributesCacheGenerator,
	TableMatrixAttribute_tE9B99706020A7D7B1966AB0535F56B207598B86E_CustomAttributesCacheGenerator,
	TypeFilterAttribute_t7989849079B6440606CBDA93E5AE67B4877A56FE_CustomAttributesCacheGenerator,
	TypeInfoBoxAttribute_t43B3E84724467282A1E3696E35325EF8C66E4BBD_CustomAttributesCacheGenerator,
	VerticalGroupAttribute_t0F1E11303B2F4DDBD1BF46A9354249AD96A934CF_CustomAttributesCacheGenerator,
	IncludeMyAttributesAttribute_tE024027922318561B8A23E5EF94A09C852C74833_CustomAttributesCacheGenerator,
	OdinRegisterAttributeAttribute_t8A82A1C5F996F02644E959C5EAD6533EBF268FFA_CustomAttributesCacheGenerator,
	TitleGroupAttribute_tB64879476828FD78A5E67F740E7149460386C6ED_CustomAttributesCacheGenerator,
	ColorPaletteAttribute_t89BD14DE6C9A17B37A64A8E188A49AB7621D759C_CustomAttributesCacheGenerator,
	CustomContextMenuAttribute_tE9AFF948A16686A5D0C8831B5383270F7134AB49_CustomAttributesCacheGenerator,
	DelayedPropertyAttribute_t18B4307EE458D335803442517336AE1208186A88_CustomAttributesCacheGenerator,
	DetailedInfoBoxAttribute_t8CEBCE93D61675CB38CA659984F4C121381C565E_CustomAttributesCacheGenerator,
	DisableContextMenuAttribute_t1EC42DF3D6D965B29C1578354756628012429BC8_CustomAttributesCacheGenerator,
	DisableIfAttribute_t4938E088E077FE7ABA2C514DBF3B3CF42BE41771_CustomAttributesCacheGenerator,
	DisableInEditorModeAttribute_t50BED704C92D71F81B99C7F72118468D918FF96D_CustomAttributesCacheGenerator,
	DisableInPlayModeAttribute_t62870C62E5C7F02C470B68549E1323500520604C_CustomAttributesCacheGenerator,
	DisplayAsStringAttribute_tC0C89AF4B51CB834ED7AD9AB028D617487E7297F_CustomAttributesCacheGenerator,
	DontApplyToListElementsAttribute_tE5E24007C6EB5678E545BFCB84A6DDEDEE3C8143_CustomAttributesCacheGenerator,
	DrawWithUnityAttribute_t22A1203943EDA03AB38CF29B28743CB94753A153_CustomAttributesCacheGenerator,
	InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator,
	ShowForPrefabOnlyAttribute_tF680D3BAFA9AF8C0C8BE3A0EC1DB9380BCB319D4_CustomAttributesCacheGenerator,
	EnableForPrefabOnlyAttribute_t179CCCCF9D183ACF7AAAB884F147EEAB89BBFFDB_CustomAttributesCacheGenerator,
	EnableIfAttribute_t3B8036F797E23325323E2064C7CCEA71BAAF7BE9_CustomAttributesCacheGenerator,
	HideIfAttribute_t11B1419645C36889523DAB5156404E47D08167B0_CustomAttributesCacheGenerator,
	HideInPlayModeAttribute_t5A71EEB1100BED2A35EF076EF6B99B628C7E32A7_CustomAttributesCacheGenerator,
	HideInEditorModeAttribute_tA80C5951E867A0D27A1C64176166E457B9C4C69F_CustomAttributesCacheGenerator,
	HideMonoScriptAttribute_tF0E9B439B2C4AF5DF40AA4408216484CBC611A18_CustomAttributesCacheGenerator,
	HideReferenceObjectPickerAttribute_t904F2AE6C1E46F3630F27B5717DAA2A078240400_CustomAttributesCacheGenerator,
	HorizontalGroupAttribute_t0E26397052585D5E12A5B453536EB14AF1A5FDE3_CustomAttributesCacheGenerator,
	InlineEditorAttribute_tFFDAC87477D9E24DE3200414E52814D52DC7CB76_CustomAttributesCacheGenerator,
	SuppressInvalidAttributeErrorAttribute_t8CCDC91E2DECC329BCDF59102809D220CCF7794E_CustomAttributesCacheGenerator,
	ShowDrawerChainAttribute_tAF05CAAEC33491EA7281A449128A45877A66A7CC_CustomAttributesCacheGenerator,
	ShowOdinSerializedPropertiesInInspectorAttribute_t67F781785F8D357CDBA0608B34CCF1F401BF137D_CustomAttributesCacheGenerator,
	FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_CustomAttributesCacheGenerator,
	GUIColorAttribute_t7E1FEBC8732A8232FE82C68019C98709FD244DB4_CustomAttributesCacheGenerator,
	HideLabelAttribute_t8CFE493B307FE0E07EA75618A406700A44FD2B00_CustomAttributesCacheGenerator,
	IndentAttribute_tE8DFFDF7D9778532690A56BC8145CC86AA6C74EE_CustomAttributesCacheGenerator,
	InfoBoxAttribute_t8D2A3D9022D85EC4424CAB78C71C24C8BF8AEFD5_CustomAttributesCacheGenerator,
	MinMaxSliderAttribute_t93E18F2979793608C8D2211F0E7EFEB2D99E9F41_CustomAttributesCacheGenerator,
	ToggleLeftAttribute_t4CC8B95C528CF91FEA46AABBC6237B2258FD77C9_CustomAttributesCacheGenerator,
	LabelTextAttribute_tAC01C34A3856E3C62B39EA8A4412BFE522E35D27_CustomAttributesCacheGenerator,
	ListDrawerSettingsAttribute_tDD2315DE255DF278706F81BA4422B5E55C97E181_CustomAttributesCacheGenerator,
	MaxValueAttribute_tFC98EB9F62599D96C7A430775FE8543EC98309BC_CustomAttributesCacheGenerator,
	MinValueAttribute_t45E8A787D2858D90D37F7A87C0582C0BF131FA34_CustomAttributesCacheGenerator,
	MultiLinePropertyAttribute_tD35BC318DFDFAC8469DA2D276235CCF0E59B319D_CustomAttributesCacheGenerator,
	PropertyTooltipAttribute_t7BC0BF969C21340C1CA2B7180CFD04350B79204B_CustomAttributesCacheGenerator,
	ReadOnlyAttribute_t4A1BE30F9257C5E0187EFB8FC659A42F4B61472C_CustomAttributesCacheGenerator,
	OnInspectorGUIAttribute_t1CCCB842BCEBEB2AB07C1298B7FFBAB7571B0F2F_CustomAttributesCacheGenerator,
	OnValueChangedAttribute_tFE13CD46B1B5466F8AA8C351562C9D3BB9FB7623_CustomAttributesCacheGenerator,
	PropertyGroupAttribute_t5B494962C4745B644D6C98BD80636769644CAFF1_CustomAttributesCacheGenerator,
	PropertyOrderAttribute_t2F83A60EEA2D6D2C91E10D2A83E22326F5F71936_CustomAttributesCacheGenerator,
	RequiredAttribute_t0941212FA1DC5F51F3309F0EB27016EE914366D4_CustomAttributesCacheGenerator,
	SceneObjectsOnlyAttribute_t74D00B9886B272BE983F0AE1F447B56E222402A2_CustomAttributesCacheGenerator,
	ValueDropdownAttribute_tA50AE0AACB3CC26139BB7879F84A4BD015BB3671_CustomAttributesCacheGenerator,
	ShowInInspectorAttribute_t821F24719748C2A29B437606CF25050AF60BAE41_CustomAttributesCacheGenerator,
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_CustomAttributesCacheGenerator,
	TitleAttribute_t03042512A6D813379BA60400D9CCEC569B3A9C44_CustomAttributesCacheGenerator,
	ToggleAttribute_tE3BA633AE4C4A847529A9FEB83DFF3C17CB83F2B_CustomAttributesCacheGenerator,
	ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator,
	ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE_CustomAttributesCacheGenerator,
	ShowIfAttribute_tDF7F785B12F1489ED80C78FC2695E7363CC5B8AE_CustomAttributesCacheGenerator,
	WrapAttribute_t2E9762C7FF30CE001AF993DE75EBF1FC599B6930_CustomAttributesCacheGenerator,
	FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator_RequireValidPath,
	FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator_U3CReadOnlyU3Ek__BackingField,
	FolderPathAttribute_t6243B822FA2CEC846D23751165EA5D9A9DD9B389_CustomAttributesCacheGenerator_RequireValidPath,
	ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_U3CDrawValueLabelHasValueU3Ek__BackingField,
	ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_U3CValueLabelAlignmentHasValueU3Ek__BackingField,
	TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9_CustomAttributesCacheGenerator_showPagingHasValue,
	TableListAttribute_t7E9F34366912C8FD3CECA8E8902209CEF55B44B9_CustomAttributesCacheGenerator_showPaging,
	InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_U3CMemberMethodU3Ek__BackingField,
	InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_U3CLabelU3Ek__BackingField,
	FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_CustomAttributesCacheGenerator_U3CHasDefinedExpandedU3Ek__BackingField,
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_CustomAttributesCacheGenerator_U3CTabsU3Ek__BackingField,
	ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator_U3CTitleStringMemberNameU3Ek__BackingField,
	FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator_FilePathAttribute_get_ReadOnly_m3A6314406C2D58C15799C01B671EE88CC4399D70,
	FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator_FilePathAttribute_set_ReadOnly_mC86951B771DBB611C650516E350D4DA4EADA3ECD,
	ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_ProgressBarAttribute_get_DrawValueLabelHasValue_m06BCD66013F03A68A6C08FF5D7A49AEF316F71D9,
	ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_ProgressBarAttribute_set_DrawValueLabelHasValue_m8189A86AD9B1A472F52D1F7E7DB563DF9B42F624,
	ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_ProgressBarAttribute_get_ValueLabelAlignmentHasValue_m7DC1402D717D0D4EE8E72DF81C941D244509A447,
	ProgressBarAttribute_t8803F2158848B5DFA7435B7691F8A79901D8510F_CustomAttributesCacheGenerator_ProgressBarAttribute_set_ValueLabelAlignmentHasValue_m9848026305C1BAA63C32C508983A41B5CAE466F0,
	InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_InlineButtonAttribute_get_MemberMethod_mECE123FE218739415AE473D16E36D3FCF36042EE,
	InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_InlineButtonAttribute_set_MemberMethod_mF0C22FEE372BFE43D98A7B8FDD6BD29365CEDAE8,
	InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_InlineButtonAttribute_get_Label_m06C931996AADA45AD653F209A5713A4AB09135E0,
	InlineButtonAttribute_t284AB94682646A44ABFD8C35529E227624C87E18_CustomAttributesCacheGenerator_InlineButtonAttribute_set_Label_m24E6BE08859E432C9819B26B95BED6F00C3DC0D0,
	FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_CustomAttributesCacheGenerator_FoldoutGroupAttribute_get_HasDefinedExpanded_mF65E7E54B706CCDE7EA9DB0C3606F2B10DCFCB25,
	FoldoutGroupAttribute_tBE164FD8BEFB32EE3F0ED3EF7651DD405A1FAE5D_CustomAttributesCacheGenerator_FoldoutGroupAttribute_set_HasDefinedExpanded_mABAA99EDDE953EEF83E4F04F80DAC78D2E3851B3,
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_CustomAttributesCacheGenerator_TabGroupAttribute_get_Tabs_m84AE8015251DAA7188C73907B8FE6BF5196C6F25,
	TabGroupAttribute_tCCF1CFE8D6FC3AA20CA29419C559A98E8241626D_CustomAttributesCacheGenerator_TabGroupAttribute_set_Tabs_m5BB49FEBE6589933B4F2B96B581E9EE4DC5838B4,
	ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator_ToggleGroupAttribute__ctor_mCE42334D8F057E33C717935C60446068B17B1AE8,
	ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator_ToggleGroupAttribute_get_TitleStringMemberName_m7A12EF988A3F9A85FAAAEF843B62E52C0BF10D20,
	ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator_ToggleGroupAttribute_set_TitleStringMemberName_m3354817FD279639CAE69FF7C3B431854763B55CF,
	ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE_CustomAttributesCacheGenerator_ValidateInputAttribute__ctor_m64BCF6EB7B972FD8563BA5A7E14CFEA80D122038,
	FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458_CustomAttributesCacheGenerator_FilePathAttribute_tD553EC82A1877D9CD13FEA4A2D102600F4CBE458____ReadOnly_PropertyInfo,
	ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00_CustomAttributesCacheGenerator_ToggleGroupAttribute_t97649EF28BDB2689D99F812C05A76BBF6B4A8D00____TitleStringMemberName_PropertyInfo,
	ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE_CustomAttributesCacheGenerator_ValidateInputAttribute_tCAB6F81898958787945296538A7B9AD9034854CE____ContiniousValidationCheck_PropertyInfo,
	Sirenix_OdinInspector_Attributes_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_allowMultiple_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_inherited_2(L_0);
		return;
	}
}
