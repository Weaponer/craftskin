﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct  CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct  AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct  RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct  SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44 (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, float ___height0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_image(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_button(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_U3CTextureU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_U3CTextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_Click(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_get_Texture_m53253EBBBA6158EAB8DCCD722403673B1386CF94(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_set_Texture_mC9765B001469946A0A8EF7B8065DBBA5F043C972(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_get_Text_m6C1987DAD8DFE3E1D90C303D0E0AA5D7DD654564(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_set_Text_m31574CAB93FC184D76FB8E097C470695BC89815F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_add_Click_m8E9AE2693D5BDE2564D307359FB3FADB33685507(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_remove_Click_mC9B29E8B2CCBC5041E75B90F01168B3CFB6A8E73(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_U3CInitializedU3Eb__14_0_m7EF2031D3B9BCCEAED92F21DCDFF8641972D7AEB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x6C\x69\x74\x72\x61\x20\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x6C\x69\x74\x72\x61\x44\x61\x74\x61\x42\x61\x73\x65"), NULL);
	}
}
static void BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA_CustomAttributesCacheGenerator_BaseProcces_Update_mBC2C04C98E90C05C6C9347EDC4F03F9C64C1E5BD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_0_0_0_var), NULL);
	}
}
static void BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA_CustomAttributesCacheGenerator_BaseProcces_U3CUpdateU3Eb__8_0_mAD6652D6C6523D25840710F6A2E3C0C01767203F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_CustomAttributesCacheGenerator_U3CUpdateU3Ed__8_SetStateMachine_m7CB0536971DEF64B4EDF5E17A72BC6F20ACC8D6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t0A70B6062957E3C958B0DE3445F4C7CF345C1B1E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tD55C61F279A1D1FA19C2B2740E97385733EB3165_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_t474D82BB28B3311DD59D6311A0635ECC58708348_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_t6DE15F992103D7332B0D5B424DEBD70EABAF8E8E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_t213A5D1C1C246DB83F90CA38E63D4EBBC71AB62D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_tC57E546DDDEC2E80F5BD801124F5ACCEC86427D1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_U3CCountNewsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_U3CNewsArrayU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_U3CIsEndWorkU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_get_CountNews_m92E438786ED41318DA81C1FB3DFFE5CF7234F215(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_set_CountNews_m0A24C4C87F4FE1FAC7E3EC4006787F0E0BDB455B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_get_NewsArray_mF722DA3FD7C30C3EC761EFA3974A3B0F54AE8081(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_set_NewsArray_m166A2E8A25F0880D8C7F6FAAA5ACED2CC6BF12CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_get_IsEndWork_m2D63D92610E037657373AA35FF09F5D7BAA25FF7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_set_IsEndWork_m241DDEEC0039C4CED556498BBDD2F083D8662082(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_U3CIsEndWorkU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_U3CNumU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_LoadEndUpdateNews_get_IsEndWork_mDB2748229CAAD5B060BFA6607452B9A14EA3CA8A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_LoadEndUpdateNews_set_IsEndWork_m8D75DDF4F66504494E3CDAC7DB77313817545D39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_LoadEndUpdateNews_get_Num_mF0D49D7DBB67DE0C6718301F671BC7470E8638C8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_LoadEndUpdateNews_set_Num_m37601EDFC0456EA7A4BC68273D80ADE82DBAA8A7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_U3CIsEndWorkU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_U3CNumU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_LoadEndUpdateTexture_get_IsEndWork_mF605A538B16DE996D43EDDB2E79ED8B7AA9999F2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_LoadEndUpdateTexture_set_IsEndWork_m0C3FE4EBB8B3D2DD9FB8AD10A577FC1135F5FA5F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_LoadEndUpdateTexture_get_Num_mA2EE4E82CD39EBA34A69AE2EF5B4F9EECB2BA338(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_LoadEndUpdateTexture_set_Num_m321BB637BB2B8EE158C93879E71B15FEEB3F36B4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_U3CCountTexutreU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_U3CTextureArrayU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_U3CIsEndWorkU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_get_CountTexutre_m147756749F5732A2C443302779F3B9699AD0E7C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_set_CountTexutre_m93428BEDC013472F04360688F6DE747A0FFC0926(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_get_TextureArray_m8947F4DCF10C54A327061AEAC24400D221576596(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_set_TextureArray_m34688F069E93DDBC4E45C2F1A430E98ACD9119A7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_get_IsEndWork_m7DD92BF32C6AD32573965DB0A6E90F3BCEE3BFF7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_set_IsEndWork_mD6F1D8C1BA620349A1455C4B252C7D85EF2ABFCC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_U3CCountTexutreU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_U3CTextureArrayU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_U3CIsEndWorkU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_get_CountTexutre_m2A4B1FAC71C75827EFA5D64BD81626B19118B0B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_set_CountTexutre_m328768693BBB339C6180E37946D02693CAE85CFE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_get_TextureArray_mA6E6EDBE325878BEC96FB301C9D44405193B4DE5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_set_TextureArray_m025BC6386770316DB84C605661CF736042EE6619(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_get_IsEndWork_m3052A9D89F7B08D72207ACD47961E9D5C674CB1D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_set_IsEndWork_m97BF73EFF2F5B1D964F5951981F639263BBBABB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProgramStart_t7B04A21AF4E572E90C2DD2A6933B25D07FF615AF_CustomAttributesCacheGenerator_programUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProgramStart_t7B04A21AF4E572E90C2DD2A6933B25D07FF615AF_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProgramStart_t7B04A21AF4E572E90C2DD2A6933B25D07FF615AF_CustomAttributesCacheGenerator_ProgramStart_get_Singleton_mB8385D9BEC7397EA9A4C13F6909BC82C4D193AC5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProgramStart_t7B04A21AF4E572E90C2DD2A6933B25D07FF615AF_CustomAttributesCacheGenerator_ProgramStart_set_Singleton_mA48138D894D42ECD87875046BC445E11EABC6318(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_renderButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_layerDownButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_openUV(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_headButton(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_headImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_bodyButton(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_bodyImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_lhandButton(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_lhandImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_rhandButton(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_rhandImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_lfutButton(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_lfutImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_rfutButton(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_rfutImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_U3CLayerDownU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_U3CPartsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_U3CStartRenderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_U3CUVEditorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_get_LayerDown_m29403D6F813A24BCB8A9239FDD6573D75D3C9D7D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_set_LayerDown_m065C5DE1A0C6D76E23A669094FEC4AC4718F19A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_get_Parts_m1410D3E832A1F784DF4C35D996A6FADA5EFE4F18(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_set_Parts_m3DA8F269E710CC9622904725F13135E25DBE358E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_get_StartRender_mAAB7A1D9289510C59929934F34C22C74FD0BE8AF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_set_StartRender_m65096D7AAFD44A49671DF36EC38EDD7ED0DCC304(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_get_UVEditor_mB67B5BA8BC6C9B3D828712FFE97D6D5E3209E8CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_set_UVEditor_mD108AECBA4C41EAD2B20CE1795C6B500B7706AB8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_get_Singleton_m1E3FF48D1E92D595061ECA0A09ACA769629914BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_set_Singleton_m0AC44F12E77EE31F4B7A84DEF68A9BC1D4B5443D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_1_m33AD094949C86A3429421A06C84D0877DEC3330E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_2_mF1D6080E2B01C3DB832325EDE01AFDDAED438455(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_3_m95003663D46376F659843D0A630C3DAA8AA47551(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_4_m0537CE1C4AD26325D752F5A8497E8284FEB05898(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_5_mBEBBE5BD5BEE427611BD3658AC3BAE00A1496E63(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_6_mAF2CD75448661E15E8A71C2FB05EE01D7C12BA74(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3_CustomAttributesCacheGenerator_EditBodyParts(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3_CustomAttributesCacheGenerator_BodyParts_add_EditBodyParts_m6D2CFA64ED6B35DF789F4D17BD52D2672429E2F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3_CustomAttributesCacheGenerator_BodyParts_remove_EditBodyParts_mF3DDA185AAED7C47D3E5DDF8BC2F73EAE334A722(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C_CustomAttributesCacheGenerator_EditAction(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C_CustomAttributesCacheGenerator_DownLayer_add_EditAction_m2ABA6CB845A909612F0BEB7E32B27DF2C9FFACCD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C_CustomAttributesCacheGenerator_DownLayer_remove_EditAction_mF49977A5F1993324D174BFAF65662BF21FED50D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08_CustomAttributesCacheGenerator_StartRenderEvent(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08_CustomAttributesCacheGenerator_Render_add_StartRenderEvent_mB699EF95F1CDBD4322088BE240531555963D510A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08_CustomAttributesCacheGenerator_Render_remove_StartRenderEvent_m1AF5E76CEDE5897C4DCA7FD2A2A2A33D2BD6D62A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CenterPanel_t6E95A42E4283947A4575C58DC97DCDA346C8425B_CustomAttributesCacheGenerator_U3CIsOpenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CenterPanel_t6E95A42E4283947A4575C58DC97DCDA346C8425B_CustomAttributesCacheGenerator_CenterPanel_get_IsOpen_m940BF4B14CFCDE8712C73BA4977DC35915E4A8D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CenterPanel_t6E95A42E4283947A4575C58DC97DCDA346C8425B_CustomAttributesCacheGenerator_CenterPanel_set_IsOpen_m152CBDFFA76B9CC104CCD915BCCBEF5DFC04561B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_CustomAttributesCacheGenerator_ClickButton(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_CustomAttributesCacheGenerator_button(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_CustomAttributesCacheGenerator_DefaultCamera_add_ClickButton_m24CEFEDB1358B9016454A446AF9051A56A3B2F15(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_CustomAttributesCacheGenerator_DefaultCamera_remove_ClickButton_m84647F94186D6F54187B10DFC40058CB701D2F47(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_panel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_programUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_takeFile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_inputField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_back(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_load(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_content(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_prefabItemFile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_LoadLibraryPanel_LoadTextureWait_mA618B45C0CF015FB67E0CC2BAEC5C514C04186EA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_0_0_0_var), NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_LoadLibraryPanel_U3CStartU3Eb__10_0_m77BEB50BC1A88F7C4AF9BB91AE23E0B59E668CE4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_LoadLibraryPanel_U3CStartU3Eb__10_1_m510E620AFE2724D80700CB9844057BB36345D472(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_LoadLibraryPanel_U3CStartU3Eb__10_2_m4960AF4EC184CD83F48532911CDB32D3CD917A17(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator_U3CLoadTextureWaitU3Ed__11__ctor_mA8EFBFFC7538B986E47910341B21AAFC99C1D578(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator_U3CLoadTextureWaitU3Ed__11_System_IDisposable_Dispose_m32278EABA09B57F45C467C7AC661D922A044A128(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator_U3CLoadTextureWaitU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30AD20615A56147C89D07E4E825A3D1BCA280C41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator_U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_Reset_m1F745D6ACC0C15F9630C12F79F928F663ADC18B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator_U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_get_Current_mC8A4904545EADE0AB3EEE97FED4AFE8FECF905A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_panel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_takeFile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_programUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_back(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_load(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_inputField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_content(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_prefabItemFile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_LoadPanel_U3CStartU3Eb__11_0_m5F905EF6A739AA5B00F23B22FD2BF2EDF64E025C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_LoadPanel_U3CStartU3Eb__11_1_mDF50C8201D6487D2849659B9ADEC3C9B7F2986DC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_LoadPanel_U3CStartU3Eb__11_2_mE799FD27D48690B248A84024F97E0E2AC1B22915(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_panel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_back(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_yes(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_programUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_NewFilePanel_U3CStartU3Eb__4_0_m2D133F62A9D1BFF48F5302C4D9A6BB5C1C0C1BC9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_NewFilePanel_U3CStartU3Eb__4_1_m26F08126DE7B00399E9508433CE748BED69FAC36(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_panel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_programUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_back(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_save(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_inputField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_content(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_prefabItemFile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_SavePanel_U3CStartU3Eb__9_0_m06526363D4ED4F05D85EDFFB06F24408C0A902C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_SavePanel_U3CStartU3Eb__9_1_m2800BE4939AD9E8A9CCFD2B5E5EC375596E7124C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_button(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_EditLanguage(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_U3CisActiveU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_ControllerLaguage_add_EditLanguage_m0E43F99E1EDBEB3A9D92AD2B32D2BAEDE10C73E3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_ControllerLaguage_remove_EditLanguage_mFDD958D17D6B48C6330DD42B9ADEA5DD5338DF6E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_ControllerLaguage_get_isActive_mAAB75859C66032897913E16442299EBF5E0E91D2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_ControllerLaguage_set_isActive_mD16197CBBC968E72943AB612C47A75039709877F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_ControllerLaguage_U3CStartU3Eb__8_0_mE39D5D344153FFEFA57B2B535131900903E72576(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditText_t499C274C73496C970B8BF35E1DB77F630021A993_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EditText_t499C274C73496C970B8BF35E1DB77F630021A993_CustomAttributesCacheGenerator_russian(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EditText_t499C274C73496C970B8BF35E1DB77F630021A993_CustomAttributesCacheGenerator_english(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonSaveFile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonLoadFile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonNewFile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonLoadFileFromLibrary(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonEditPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonEditItem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonEditBlock(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_SaveFile(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_LoadFile(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_NewFile(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_LoadFileFromLibrary(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_EditPlayer(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_EditItem(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_EditBlock(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_SaveFile_m05694E995B00E0A5B7C2398D638FFA488C6D9347(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_SaveFile_m353A16A62CAB468992DE601B9BCF7C82DE5124D5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_LoadFile_m6BB857F0CAF2C51B2C0963F45B1579AEF5851978(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_LoadFile_m16C9D55F4A513E0B64BE9488511963928A78E4E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_NewFile_m41EF1125E326092467EB209A4928228AE1B03026(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_NewFile_mA3249B7015A2BCF69645E1F20635414C5A9AE801(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_LoadFileFromLibrary_m238B1208B65BF59D270AC282904BD000EDDD0254(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_LoadFileFromLibrary_m36D1A8A034C4671C4F9C42C9BDB432D9BAB8F904(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_EditPlayer_m48E07A1778D3D487B49DF3F9FC7233BBBAC42DE5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_EditPlayer_mDD89630473B86F3CA70C5A1BE9802613FEC5CC97(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_EditItem_mC2063CA506A5FCD945E4C57A096FD7D6DDD76CBE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_EditItem_mFF58CE5FB87F808F327D2E36A9ED3C750ABEBDE4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_EditBlock_mE9AD06BA55F71022E6F8D72C5E400741A318C6F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_EditBlock_m19CCBB8F2CD9A2BC04160F9345F28D89744EF5B0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_get_Singleton_mDE37F6231B0F7936FFEEB0E801085D89EC6A8428(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_set_Singleton_m635E9F118FFF8B01383828CFA59D048613B2F11F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_0_mCC6958034FDEA45ABB33ECF47C1CCAC01601BD9B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_1_m40355F530568FDC7E1DEFBC00946261C315F4AFC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_2_m5DA812FB86F0BDA0354F4D6875B1961C93966BA4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_3_m2FC0FFC86010CBA9D4A3DF4DE23901A886D82333(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_4_m8F30655D551DD9CABA8D84BADB4ABBFAE5FDA0AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_5_m82FA5D1ED9AD9E5BF0A3BFA85CC3F1E768F7AE79(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_6_m4B0C097BF677DDA5DC4441A6DAFFBBDE08D81AB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadMenu_t05DFAD528D1A3D0479DD5FB130515DA4ED9D18B9_CustomAttributesCacheGenerator_loadMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650_CustomAttributesCacheGenerator_button(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650_CustomAttributesCacheGenerator_image(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t2104F99A587EAB2260F5C1947DDBF7A6D9C6C62A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3_CustomAttributesCacheGenerator_TakeItem(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3_CustomAttributesCacheGenerator_ColorPalitra_add_TakeItem_m2AB67F861A515A06640B67A3F8B6230B8742787C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3_CustomAttributesCacheGenerator_ColorPalitra_remove_TakeItem_m0588DA2EB6E2C9BD8F33A8D394EE9BA9110D45B2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_panel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_back(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_textureUp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_texturePicker(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_slider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_joystic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_ColorPickerPanel_U3CStartU3Eb__8_0_m75A75D7EA72B5C04D91F9A0E891668EF2729D771(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_ColorPickerPanel_U3CStartU3Eb__8_1_m9F169F862462214577F1A6005F98C8EA23F315AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryNext(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryBack(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryButton_add_HistoryNext_mFA7B5A0E94A850C99B6F186447423F70F5E53D8F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryButton_remove_HistoryNext_m4952CFA1B71AB4C14D5E798971CB5F6E5A427941(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryButton_add_HistoryBack_mFE5A88B7E7BF434ED70B6D60A5FB87CE48191E82(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryButton_remove_HistoryBack_mD2863D6DE0ACBCF75E6742C8ADF504E66A41E2F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_U3CInstrumentTakeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_TakeInstrument(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_Instruments_get_InstrumentTake_mE8360FD723E9F6466EF0BF10D48301EF3B73B59F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_Instruments_set_InstrumentTake_mCAD9D564496C1F839D68AA299DE96B668752E99E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_Instruments_add_TakeInstrument_m647488514FAF8BDC89B6361ACE7D5CDA7975677A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_Instruments_remove_TakeInstrument_mB0D58527D59E36C769009665538F8831017970BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_saveFile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_deletFile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_panel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_breakBut(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_prefabItem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_content(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_palitraData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_TakePalitra(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_add_TakePalitra_mE3BFDB24358C0AE3C122F29D911C18B86CA9F778(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_remove_TakePalitra_mC79E72F7911BABB60AC3DA561527D15B9B038AE8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_get_Singleton_mA27F91D69CC9387223DBCDFF52512A59CFB69CAE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_set_Singleton_mAF9BE98C99C52395B40215E2EE515500077D654D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_U3CStartU3Eb__18_0_mB0B57DAE05470FABAE4B70A730AAA53D52EF28D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_U3CStartU3Eb__18_1_mFE44FADF95EBB75F7ECF776DA2C4C04912DAEA42(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_U3CStartU3Eb__18_2_m5B895236BB6DCB620EE58B5529018C9099E361CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraItem_tB7364AC466C6FA3A859EB47095C3B8A563A0AD4A_CustomAttributesCacheGenerator_button(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraItem_tB7364AC466C6FA3A859EB47095C3B8A563A0AD4A_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tD3927C92A4B65FB4EC5119DE2D09429DBBB6B3E9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_colorPickerPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_openPicker(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_programUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_palitraDataBase(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_openPalitras(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_menuPaltira(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_paint(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_paintOn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_paintOff(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_takeColor(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_takeColorOn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_takeColorOff(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_fillColor(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_fillColorOn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_fillColorOff(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_erase(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_eraseOn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_eraseOff(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_colorItems(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_pointTake(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_backHistory(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_nextHistory(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_U3CInstrumentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_U3CColorListU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_U3CHistoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_get_Instrument_m7F79D1932B2C71BB7B236D976366BFDC4965020C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_set_Instrument_mE1F840BF319190F398A837FEA3A67DC41F936436(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_get_ColorList_m097292F6376815B20209CCD233B1F23800CD0C02(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_set_ColorList_m0C9A3AA69E4B3C52393A5D862DCAD9165E17BF43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_get_History_mD749E9F50722D5B8D5110C265F2CDAEAFDD881AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_set_History_m5F1EDBD7B6D224B8F7CFB16668B6A03FA8588EB2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_get_Singleton_mCE3314F8DAA7B9C9C3FE5193C610299AA1549574(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_set_Singleton_m5AAC644BDE919FCCA0846FC5CABE4328BEEF7630(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_U3CStartU3Eb__39_6_m5FD8DF5AED30CF7751EB8C00D5D6A4905887E247(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_U3CStartU3Eb__39_7_m14B1E4FB7298CCD39B2BE9108F1D9282F7D93FB0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_isStart(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_isHideOpen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_panel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_open(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_close(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_PanelProgram_U3CStartU3Eb__6_0_mB24D08F5DC87D7C9AB155E06FB3F72C4F69A8706(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_PanelProgram_U3CStartU3Eb__6_1_m38395C281139DE98FAE39AC937126D857AFD6E52(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7_CustomAttributesCacheGenerator_canvasScaler(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C_CustomAttributesCacheGenerator_programUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C_CustomAttributesCacheGenerator_close(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C_CustomAttributesCacheGenerator_basePanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C_CustomAttributesCacheGenerator_UV_U3CStartU3Eb__3_0_mD1AE53B2DA7CAAD27C216DCD91C3BCB0B121B85D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_EditUV(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_U3CIsActiveU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_UVPanel_add_EditUV_m87ACD1040A8EAFCE0561D164276C7657896F145D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_UVPanel_remove_EditUV_m1F8CD365E32F47094F9F1E00E8BB22C9E2912A2E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_UVPanel_get_IsActive_mF298A653781B5B2009EB060D71EE5A41EB561C75(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_UVPanel_set_IsActive_m257C20DA61922AA7F7B5CAFD94DEC2F178478008(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BackEditor_t2B12029F349FB59E5B17DF1AD5D9A07BF10BE59E_CustomAttributesCacheGenerator_button(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BackEditor_t2B12029F349FB59E5B17DF1AD5D9A07BF10BE59E_CustomAttributesCacheGenerator_BackEditor_U3CStartU3Eb__1_0_m6C7249A519A003BADB6F90A05660C1D4DAEA74DB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_speedRotate(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 300.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_U3CDirectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_player(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_panel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_eventSystem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_joistic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_cameraPos(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_ControllerPlayer_get_Direct_m058EE79D5CDB56B7B316B20456071E6433346142(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_ControllerPlayer_set_Direct_m07006D227548EB9A006A564398F8DAFEE2F0BBBF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MovePlayer_t4C6356F44622AB80C42E9D9CA88292395A2860B8_CustomAttributesCacheGenerator_characterController(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovePlayer_t4C6356F44622AB80C42E9D9CA88292395A2860B8_CustomAttributesCacheGenerator_controllerPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovePlayer_t4C6356F44622AB80C42E9D9CA88292395A2860B8_CustomAttributesCacheGenerator_animator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovePlayer_t4C6356F44622AB80C42E9D9CA88292395A2860B8_CustomAttributesCacheGenerator_maxSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovePlayer_t4C6356F44622AB80C42E9D9CA88292395A2860B8_CustomAttributesCacheGenerator_acceleration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetTexture_t3793A669A7F00360BC0C5E6297BFDDAF6FB5F0D5_CustomAttributesCacheGenerator_renderers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OptionController_tA8E9CDD42837DC31FE5DA96694014949D240F9E5_CustomAttributesCacheGenerator_options(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_CustomAttributesCacheGenerator_BaseMenu_get_Singleton_m069EC972BD638A8FB525054E1611FB620CA75AA5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_CustomAttributesCacheGenerator_BaseMenu_set_Singleton_mB1A2F5FFCDCC484C66BD011396C1192066AFBDAD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemNews_t56EF3225D84E7162C9E47B98493683E32B6C08F1_CustomAttributesCacheGenerator_editText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemNews_t56EF3225D84E7162C9E47B98493683E32B6C08F1_CustomAttributesCacheGenerator_panel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_uIMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_prefabNews(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_ListNews_get_Singleton_mA989ACDE2D531B9D55A5BE2D5432EA4AA6356CBA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_ListNews_set_Singleton_m105CAC52256E242D85A30E5121072DF7CD25DEB8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_ListNews_LoadNewsWait_m8BA4ABE06B74001CABF6FC88B8E7C0E056C0730E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_0_0_0_var), NULL);
	}
}
static void U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator_U3CLoadNewsWaitU3Ed__8__ctor_m954E460A8D2E865D49227C50AD953ADB62BD3751(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator_U3CLoadNewsWaitU3Ed__8_System_IDisposable_Dispose_mD506D633D37BB4EDC8E584479ED3C7D4D05326C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator_U3CLoadNewsWaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEA5FD9FFF67794B08B86C64DE1B8AA4D629E6CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator_U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_Reset_m53159E79EBA8B80BAC673EDE60518A4F30D17A98(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator_U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_get_Current_m3172BAD8411C52CFB7BBDC7E0C0F447315DFB4C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UIMenu_t6A3241472F39D561390A7106B024030062D29EA6_CustomAttributesCacheGenerator_loadProgram(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIMenu_t6A3241472F39D561390A7106B024030062D29EA6_CustomAttributesCacheGenerator_exit(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIMenu_t6A3241472F39D561390A7106B024030062D29EA6_CustomAttributesCacheGenerator_contentNews(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_speedRotate(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 300.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_speedMove(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 2.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_speedScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 2.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_limitScaleMin(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 6.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_limitScaleMax(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 6.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_eventSystem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_cameraPos(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_skinModel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_itemModel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_blockModel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_U3CCurrentModelU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_EditModel_get_CurrentModel_m93448B5F4798C5462C4E37A57C117B5472B56A4D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_EditModel_set_CurrentModel_m24FB61D1B1CD0F63E872BA9F32A2D262AD65E94C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_EditModel_get_Singleton_mC5F7909A086EF7180B1518C5BF34A15278714A31(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_EditModel_set_Singleton_m976F3966ED7AC897C0321CB41B8E8B5BF7EB6346(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_baseModel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_meshRenderers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_defaultTexture(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_uv(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_U3CIsWriteHistoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_Model_get_IsWriteHistory_m5EDD0BAEB57D2027752B3D5FE6F654A1289B4595(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_Model_set_IsWriteHistory_mC31712A7FC082CABA44BB58C5E52AAE6DA8A5EB0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void History_tB9376052A6C8C07D431414CC16E050FE0EE9A156_CustomAttributesCacheGenerator_U3CCurrentStoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void History_tB9376052A6C8C07D431414CC16E050FE0EE9A156_CustomAttributesCacheGenerator_History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void History_tB9376052A6C8C07D431414CC16E050FE0EE9A156_CustomAttributesCacheGenerator_History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_head(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_headD(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_body(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_bodyD(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_lhand(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_lhandD(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_rhand(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_rhandD(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_lfut(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_lfutD(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_rfut(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_rfutD(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UVBlock_t927443768AA8C3D6A15049D01EC00B6D71BF469B_CustomAttributesCacheGenerator_baseImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UVBlock_t927443768AA8C3D6A15049D01EC00B6D71BF469B_CustomAttributesCacheGenerator_copyImages(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UVSkin_tB439E4EF0DF1DEEC232A0125CC37375C527CCFBF_CustomAttributesCacheGenerator_image(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Paint_tCE6EDA417D3BAD0D85A15E535C8B89AAC64A96EA_CustomAttributesCacheGenerator_moveCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Paint_tCE6EDA417D3BAD0D85A15E535C8B89AAC64A96EA_CustomAttributesCacheGenerator_eventSystem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Paint_tCE6EDA417D3BAD0D85A15E535C8B89AAC64A96EA_CustomAttributesCacheGenerator_camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_moveCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_eventSystem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_mask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_U3CTypeInstrumentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_Tassel_get_TypeInstrument_m918CF89558E2EB0FCB856E8C3F13580C493DEDD2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_Tassel_set_TypeInstrument_m398D1C548B8B6AFDB3D582B5497CE0E26B635AE4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_clickButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_clickColorButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_paint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_AudioController_get_Singleton_m23473DECF654EB8C9EF43BD85282CEFBF4023594(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_AudioController_set_Singleton_m68D0A477505FE586F9AA25086DEA44F96AF3A6A9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ButtonClick_t3C622671EAB380BB861EB1B6DE1B8EFE17B4A04B_CustomAttributesCacheGenerator_button(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ButtonClickColor_tC24184B07F50130F0F74C8E8D07F6D1E68398402_CustomAttributesCacheGenerator_button(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SetVolume_tA4ECFDEB766396A958FC240586677F2D5C1F42D5_CustomAttributesCacheGenerator_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetVolume_tA4ECFDEB766396A958FC240586677F2D5C1F42D5_CustomAttributesCacheGenerator_volume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeAudio_t86FC5AF2A5FAEA4E7F1786255E39005EB558577A_CustomAttributesCacheGenerator_Editor(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VolumeAudio_t86FC5AF2A5FAEA4E7F1786255E39005EB558577A_CustomAttributesCacheGenerator_slider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeAudio_t86FC5AF2A5FAEA4E7F1786255E39005EB558577A_CustomAttributesCacheGenerator_VolumeAudio_add_Editor_m129CC5D58B5CB0F17A2CB47BAFA30D576BA84364(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VolumeAudio_t86FC5AF2A5FAEA4E7F1786255E39005EB558577A_CustomAttributesCacheGenerator_VolumeAudio_remove_Editor_mC2A7A7BFE820002D2ED7E321813E21CFDA4DEECA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VolumeAudio_t86FC5AF2A5FAEA4E7F1786255E39005EB558577A_CustomAttributesCacheGenerator_VolumeAudio_U3CStartU3Eb__5_0_mF07DE3ABA2B6761464B1DBEF8B4543BBB229E6D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[431] = 
{
	PalitraDataBase_t52950E5D22D2A32C241B69943963A87E4EF487DD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t0A70B6062957E3C958B0DE3445F4C7CF345C1B1E_CustomAttributesCacheGenerator,
	U3CU3Ec_tD55C61F279A1D1FA19C2B2740E97385733EB3165_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_t474D82BB28B3311DD59D6311A0635ECC58708348_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_t6DE15F992103D7332B0D5B424DEBD70EABAF8E8E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_t213A5D1C1C246DB83F90CA38E63D4EBBC71AB62D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_tC57E546DDDEC2E80F5BD801124F5ACCEC86427D1_CustomAttributesCacheGenerator,
	U3CU3Ec_t7950F476AEDDEE6710A2DFD3B4AB65A06215F52C_CustomAttributesCacheGenerator,
	U3CU3Ec_t2FA2DC415DF836D4AB21F723185E25AEF3C4C522_CustomAttributesCacheGenerator,
	U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator,
	U3CU3Ec_t01708DF512D05E76386F39FE9F160881FCC00BD9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t2104F99A587EAB2260F5C1947DDBF7A6D9C6C62A_CustomAttributesCacheGenerator,
	U3CU3Ec_t256107C8EDC44F924E993B98099B06A533D257DE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tD3927C92A4B65FB4EC5119DE2D09429DBBB6B3E9_CustomAttributesCacheGenerator,
	U3CU3Ec_tC9BF5BA780920A837265DD44942B52A90F92B9E0_CustomAttributesCacheGenerator,
	U3CU3Ec_t884C19CDF4FAE71E8FE825569754DDC675E1BD4E_CustomAttributesCacheGenerator,
	U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator,
	U3CU3Ec_tA8212012D8D6D1690769A7828CE6B408FE49784D_CustomAttributesCacheGenerator,
	U3CU3Ec_t15CDB02EED4124DA30791EDB4FAD6959DCCF899F_CustomAttributesCacheGenerator,
	U3CU3Ec_tF77D09CB850D53FB1AD3CBCED4106353CA27B02F_CustomAttributesCacheGenerator,
	U3CU3Ec_t291A70CF668B2772FA32B9736019DD1E2001CAF4_CustomAttributesCacheGenerator,
	U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_CustomAttributesCacheGenerator,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_text,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_image,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_button,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_U3CTextureU3Ek__BackingField,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_U3CTextU3Ek__BackingField,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_Click,
	LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_U3CCountNewsU3Ek__BackingField,
	LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_U3CNewsArrayU3Ek__BackingField,
	LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_U3CIsEndWorkU3Ek__BackingField,
	LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_U3CIsEndWorkU3Ek__BackingField,
	LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_U3CNumU3Ek__BackingField,
	LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_U3CIsEndWorkU3Ek__BackingField,
	LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_U3CNumU3Ek__BackingField,
	LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_U3CCountTexutreU3Ek__BackingField,
	LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_U3CTextureArrayU3Ek__BackingField,
	LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_U3CIsEndWorkU3Ek__BackingField,
	LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_U3CCountTexutreU3Ek__BackingField,
	LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_U3CTextureArrayU3Ek__BackingField,
	LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_U3CIsEndWorkU3Ek__BackingField,
	ProgramStart_t7B04A21AF4E572E90C2DD2A6933B25D07FF615AF_CustomAttributesCacheGenerator_programUI,
	ProgramStart_t7B04A21AF4E572E90C2DD2A6933B25D07FF615AF_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_renderButton,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_layerDownButton,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_openUV,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_headButton,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_headImage,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_bodyButton,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_bodyImage,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_lhandButton,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_lhandImage,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_rhandButton,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_rhandImage,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_lfutButton,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_lfutImage,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_rfutButton,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_rfutImage,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_U3CLayerDownU3Ek__BackingField,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_U3CPartsU3Ek__BackingField,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_U3CStartRenderU3Ek__BackingField,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_U3CUVEditorU3Ek__BackingField,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField,
	BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3_CustomAttributesCacheGenerator_EditBodyParts,
	DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C_CustomAttributesCacheGenerator_EditAction,
	Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08_CustomAttributesCacheGenerator_StartRenderEvent,
	CenterPanel_t6E95A42E4283947A4575C58DC97DCDA346C8425B_CustomAttributesCacheGenerator_U3CIsOpenU3Ek__BackingField,
	DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_CustomAttributesCacheGenerator_ClickButton,
	DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_CustomAttributesCacheGenerator_button,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_panel,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_programUI,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_takeFile,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_inputField,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_back,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_load,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_content,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_prefabItemFile,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_panel,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_takeFile,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_programUI,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_back,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_load,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_inputField,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_content,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_prefabItemFile,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_text,
	NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_panel,
	NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_back,
	NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_yes,
	NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_programUI,
	SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_panel,
	SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_programUI,
	SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_back,
	SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_save,
	SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_inputField,
	SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_content,
	SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_prefabItemFile,
	SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_text,
	ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_button,
	ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_EditLanguage,
	ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_U3CisActiveU3Ek__BackingField,
	EditText_t499C274C73496C970B8BF35E1DB77F630021A993_CustomAttributesCacheGenerator_text,
	EditText_t499C274C73496C970B8BF35E1DB77F630021A993_CustomAttributesCacheGenerator_russian,
	EditText_t499C274C73496C970B8BF35E1DB77F630021A993_CustomAttributesCacheGenerator_english,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonSaveFile,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonLoadFile,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonNewFile,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonLoadFileFromLibrary,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonEditPlayer,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonEditItem,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_buttonEditBlock,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_SaveFile,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_LoadFile,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_NewFile,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_LoadFileFromLibrary,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_EditPlayer,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_EditItem,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_EditBlock,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField,
	LoadMenu_t05DFAD528D1A3D0479DD5FB130515DA4ED9D18B9_CustomAttributesCacheGenerator_loadMenu,
	ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650_CustomAttributesCacheGenerator_button,
	ColorItem_t034ABAB60E2B3E0E33AB774129A3BDDF70C45650_CustomAttributesCacheGenerator_image,
	ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3_CustomAttributesCacheGenerator_TakeItem,
	ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_panel,
	ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_back,
	ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_textureUp,
	ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_texturePicker,
	ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_slider,
	ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_joystic,
	HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryNext,
	HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryBack,
	Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_U3CInstrumentTakeU3Ek__BackingField,
	Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_TakeInstrument,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_saveFile,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_deletFile,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_panel,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_breakBut,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_prefabItem,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_content,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_palitraData,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_TakePalitra,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField,
	PalitraItem_tB7364AC466C6FA3A859EB47095C3B8A563A0AD4A_CustomAttributesCacheGenerator_button,
	PalitraItem_tB7364AC466C6FA3A859EB47095C3B8A563A0AD4A_CustomAttributesCacheGenerator_text,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_colorPickerPanel,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_openPicker,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_programUI,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_palitraDataBase,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_openPalitras,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_menuPaltira,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_paint,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_paintOn,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_paintOff,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_takeColor,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_takeColorOn,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_takeColorOff,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_fillColor,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_fillColorOn,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_fillColorOff,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_erase,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_eraseOn,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_eraseOff,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_colorItems,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_pointTake,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_backHistory,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_nextHistory,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_U3CInstrumentU3Ek__BackingField,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_U3CColorListU3Ek__BackingField,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_U3CHistoryU3Ek__BackingField,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField,
	PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_isStart,
	PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_isHideOpen,
	PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_panel,
	PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_open,
	PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_close,
	ProgramUI_t90AEC9EB7D27DE71FDB47B2675CC3909F2CEBFB7_CustomAttributesCacheGenerator_canvasScaler,
	UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C_CustomAttributesCacheGenerator_programUI,
	UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C_CustomAttributesCacheGenerator_close,
	UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C_CustomAttributesCacheGenerator_basePanel,
	UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_EditUV,
	UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_U3CIsActiveU3Ek__BackingField,
	BackEditor_t2B12029F349FB59E5B17DF1AD5D9A07BF10BE59E_CustomAttributesCacheGenerator_button,
	ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_speedRotate,
	ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_U3CDirectU3Ek__BackingField,
	ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_player,
	ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_panel,
	ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_eventSystem,
	ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_radius,
	ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_joistic,
	ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_cameraPos,
	MovePlayer_t4C6356F44622AB80C42E9D9CA88292395A2860B8_CustomAttributesCacheGenerator_characterController,
	MovePlayer_t4C6356F44622AB80C42E9D9CA88292395A2860B8_CustomAttributesCacheGenerator_controllerPlayer,
	MovePlayer_t4C6356F44622AB80C42E9D9CA88292395A2860B8_CustomAttributesCacheGenerator_animator,
	MovePlayer_t4C6356F44622AB80C42E9D9CA88292395A2860B8_CustomAttributesCacheGenerator_maxSpeed,
	MovePlayer_t4C6356F44622AB80C42E9D9CA88292395A2860B8_CustomAttributesCacheGenerator_acceleration,
	SetTexture_t3793A669A7F00360BC0C5E6297BFDDAF6FB5F0D5_CustomAttributesCacheGenerator_renderers,
	OptionController_tA8E9CDD42837DC31FE5DA96694014949D240F9E5_CustomAttributesCacheGenerator_options,
	BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField,
	ItemNews_t56EF3225D84E7162C9E47B98493683E32B6C08F1_CustomAttributesCacheGenerator_editText,
	ItemNews_t56EF3225D84E7162C9E47B98493683E32B6C08F1_CustomAttributesCacheGenerator_panel,
	ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField,
	ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_uIMenu,
	ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_prefabNews,
	UIMenu_t6A3241472F39D561390A7106B024030062D29EA6_CustomAttributesCacheGenerator_loadProgram,
	UIMenu_t6A3241472F39D561390A7106B024030062D29EA6_CustomAttributesCacheGenerator_exit,
	UIMenu_t6A3241472F39D561390A7106B024030062D29EA6_CustomAttributesCacheGenerator_contentNews,
	CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_speedRotate,
	CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_speedMove,
	CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_speedScale,
	CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_limitScaleMin,
	CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_limitScaleMax,
	CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_eventSystem,
	CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_camera,
	CameraMove_t9E8C994360E0EA627AB682754BBEAD10ACABDF5F_CustomAttributesCacheGenerator_cameraPos,
	EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_skinModel,
	EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_itemModel,
	EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_blockModel,
	EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_U3CCurrentModelU3Ek__BackingField,
	EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField,
	Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_baseModel,
	Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_meshRenderers,
	Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_defaultTexture,
	Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_uv,
	Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_U3CIsWriteHistoryU3Ek__BackingField,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_head,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_headD,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_body,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_bodyD,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_lhand,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_lhandD,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_rhand,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_rhandD,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_lfut,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_lfutD,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_rfut,
	Skin_t1DBC8D6CA10E3E2FE08C44930DDF54A8D0574293_CustomAttributesCacheGenerator_rfutD,
	UVBlock_t927443768AA8C3D6A15049D01EC00B6D71BF469B_CustomAttributesCacheGenerator_baseImage,
	UVBlock_t927443768AA8C3D6A15049D01EC00B6D71BF469B_CustomAttributesCacheGenerator_copyImages,
	UVSkin_tB439E4EF0DF1DEEC232A0125CC37375C527CCFBF_CustomAttributesCacheGenerator_image,
	Paint_tCE6EDA417D3BAD0D85A15E535C8B89AAC64A96EA_CustomAttributesCacheGenerator_moveCamera,
	Paint_tCE6EDA417D3BAD0D85A15E535C8B89AAC64A96EA_CustomAttributesCacheGenerator_eventSystem,
	Paint_tCE6EDA417D3BAD0D85A15E535C8B89AAC64A96EA_CustomAttributesCacheGenerator_camera,
	Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_moveCamera,
	Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_eventSystem,
	Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_camera,
	Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_mask,
	Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_U3CTypeInstrumentU3Ek__BackingField,
	AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_clickButton,
	AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_clickColorButton,
	AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_paint,
	AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_U3CSingletonU3Ek__BackingField,
	ButtonClick_t3C622671EAB380BB861EB1B6DE1B8EFE17B4A04B_CustomAttributesCacheGenerator_button,
	ButtonClickColor_tC24184B07F50130F0F74C8E8D07F6D1E68398402_CustomAttributesCacheGenerator_button,
	SetVolume_tA4ECFDEB766396A958FC240586677F2D5C1F42D5_CustomAttributesCacheGenerator_audioSource,
	SetVolume_tA4ECFDEB766396A958FC240586677F2D5C1F42D5_CustomAttributesCacheGenerator_volume,
	VolumeAudio_t86FC5AF2A5FAEA4E7F1786255E39005EB558577A_CustomAttributesCacheGenerator_Editor,
	VolumeAudio_t86FC5AF2A5FAEA4E7F1786255E39005EB558577A_CustomAttributesCacheGenerator_slider,
	History_tB9376052A6C8C07D431414CC16E050FE0EE9A156_CustomAttributesCacheGenerator_U3CCurrentStoryU3Ek__BackingField,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_get_Texture_m53253EBBBA6158EAB8DCCD722403673B1386CF94,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_set_Texture_mC9765B001469946A0A8EF7B8065DBBA5F043C972,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_get_Text_m6C1987DAD8DFE3E1D90C303D0E0AA5D7DD654564,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_set_Text_m31574CAB93FC184D76FB8E097C470695BC89815F,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_add_Click_m8E9AE2693D5BDE2564D307359FB3FADB33685507,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_remove_Click_mC9B29E8B2CCBC5041E75B90F01168B3CFB6A8E73,
	ItemFile_t5534CA74F2AF700F4F11D63F0F031B0CFDD6BFA8_CustomAttributesCacheGenerator_ItemFile_U3CInitializedU3Eb__14_0_m7EF2031D3B9BCCEAED92F21DCDFF8641972D7AEB,
	LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_get_CountNews_m92E438786ED41318DA81C1FB3DFFE5CF7234F215,
	LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_set_CountNews_m0A24C4C87F4FE1FAC7E3EC4006787F0E0BDB455B,
	LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_get_NewsArray_mF722DA3FD7C30C3EC761EFA3974A3B0F54AE8081,
	LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_set_NewsArray_m166A2E8A25F0880D8C7F6FAAA5ACED2CC6BF12CF,
	LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_get_IsEndWork_m2D63D92610E037657373AA35FF09F5D7BAA25FF7,
	LoadListNews_t0000CB57DBE5FA286938AFB5FF612A1A88C1EB70_CustomAttributesCacheGenerator_LoadListNews_set_IsEndWork_m241DDEEC0039C4CED556498BBDD2F083D8662082,
	LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_LoadEndUpdateNews_get_IsEndWork_mDB2748229CAAD5B060BFA6607452B9A14EA3CA8A,
	LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_LoadEndUpdateNews_set_IsEndWork_m8D75DDF4F66504494E3CDAC7DB77313817545D39,
	LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_LoadEndUpdateNews_get_Num_mF0D49D7DBB67DE0C6718301F671BC7470E8638C8,
	LoadEndUpdateNews_tC2821C8ABF8F49B0CD6240F702D08A49E5D975E8_CustomAttributesCacheGenerator_LoadEndUpdateNews_set_Num_m37601EDFC0456EA7A4BC68273D80ADE82DBAA8A7,
	LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_LoadEndUpdateTexture_get_IsEndWork_mF605A538B16DE996D43EDDB2E79ED8B7AA9999F2,
	LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_LoadEndUpdateTexture_set_IsEndWork_m0C3FE4EBB8B3D2DD9FB8AD10A577FC1135F5FA5F,
	LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_LoadEndUpdateTexture_get_Num_mA2EE4E82CD39EBA34A69AE2EF5B4F9EECB2BA338,
	LoadEndUpdateTexture_tE438F7DAFA1F2E31DB0E74DEE516C9CA032DCE14_CustomAttributesCacheGenerator_LoadEndUpdateTexture_set_Num_m321BB637BB2B8EE158C93879E71B15FEEB3F36B4,
	LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_get_CountTexutre_m147756749F5732A2C443302779F3B9699AD0E7C0,
	LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_set_CountTexutre_m93428BEDC013472F04360688F6DE747A0FFC0926,
	LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_get_TextureArray_m8947F4DCF10C54A327061AEAC24400D221576596,
	LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_set_TextureArray_m34688F069E93DDBC4E45C2F1A430E98ACD9119A7,
	LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_get_IsEndWork_m7DD92BF32C6AD32573965DB0A6E90F3BCEE3BFF7,
	LoadListTextureName_t925BFAB362CA9B8017DD2FC8BC1E2194C2502522_CustomAttributesCacheGenerator_LoadListTextureName_set_IsEndWork_mD6F1D8C1BA620349A1455C4B252C7D85EF2ABFCC,
	LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_get_CountTexutre_m2A4B1FAC71C75827EFA5D64BD81626B19118B0B5,
	LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_set_CountTexutre_m328768693BBB339C6180E37946D02693CAE85CFE,
	LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_get_TextureArray_mA6E6EDBE325878BEC96FB301C9D44405193B4DE5,
	LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_set_TextureArray_m025BC6386770316DB84C605661CF736042EE6619,
	LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_get_IsEndWork_m3052A9D89F7B08D72207ACD47961E9D5C674CB1D,
	LoadListTexture_t94381B4E91F8764B467EF5C24E1C2CFD64BF4BD2_CustomAttributesCacheGenerator_LoadListTexture_set_IsEndWork_m97BF73EFF2F5B1D964F5951981F639263BBBABB6,
	ProgramStart_t7B04A21AF4E572E90C2DD2A6933B25D07FF615AF_CustomAttributesCacheGenerator_ProgramStart_get_Singleton_mB8385D9BEC7397EA9A4C13F6909BC82C4D193AC5,
	ProgramStart_t7B04A21AF4E572E90C2DD2A6933B25D07FF615AF_CustomAttributesCacheGenerator_ProgramStart_set_Singleton_mA48138D894D42ECD87875046BC445E11EABC6318,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_get_LayerDown_m29403D6F813A24BCB8A9239FDD6573D75D3C9D7D,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_set_LayerDown_m065C5DE1A0C6D76E23A669094FEC4AC4718F19A4,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_get_Parts_m1410D3E832A1F784DF4C35D996A6FADA5EFE4F18,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_set_Parts_m3DA8F269E710CC9622904725F13135E25DBE358E,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_get_StartRender_mAAB7A1D9289510C59929934F34C22C74FD0BE8AF,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_set_StartRender_m65096D7AAFD44A49671DF36EC38EDD7ED0DCC304,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_get_UVEditor_mB67B5BA8BC6C9B3D828712FFE97D6D5E3209E8CE,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_set_UVEditor_mD108AECBA4C41EAD2B20CE1795C6B500B7706AB8,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_get_Singleton_m1E3FF48D1E92D595061ECA0A09ACA769629914BF,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_set_Singleton_m0AC44F12E77EE31F4B7A84DEF68A9BC1D4B5443D,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_1_m33AD094949C86A3429421A06C84D0877DEC3330E,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_2_mF1D6080E2B01C3DB832325EDE01AFDDAED438455,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_3_m95003663D46376F659843D0A630C3DAA8AA47551,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_4_m0537CE1C4AD26325D752F5A8497E8284FEB05898,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_5_mBEBBE5BD5BEE427611BD3658AC3BAE00A1496E63,
	BodyPanel_t3351FC05E1439F36B1BACABE9C33D27A0008CB15_CustomAttributesCacheGenerator_BodyPanel_U3CSetOptionBodyPartsU3Eb__36_6_mAF2CD75448661E15E8A71C2FB05EE01D7C12BA74,
	BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3_CustomAttributesCacheGenerator_BodyParts_add_EditBodyParts_m6D2CFA64ED6B35DF789F4D17BD52D2672429E2F0,
	BodyParts_tD016F5D744BF03D99BECA44596E06319D85A93E3_CustomAttributesCacheGenerator_BodyParts_remove_EditBodyParts_mF3DDA185AAED7C47D3E5DDF8BC2F73EAE334A722,
	DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C_CustomAttributesCacheGenerator_DownLayer_add_EditAction_m2ABA6CB845A909612F0BEB7E32B27DF2C9FFACCD,
	DownLayer_t3CDFDF902E622E95F4C5F0F28E0B26172F52939C_CustomAttributesCacheGenerator_DownLayer_remove_EditAction_mF49977A5F1993324D174BFAF65662BF21FED50D9,
	Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08_CustomAttributesCacheGenerator_Render_add_StartRenderEvent_mB699EF95F1CDBD4322088BE240531555963D510A,
	Render_t0BDDB8E862E50A2E21AA453992B3A04B34D59A08_CustomAttributesCacheGenerator_Render_remove_StartRenderEvent_m1AF5E76CEDE5897C4DCA7FD2A2A2A33D2BD6D62A,
	CenterPanel_t6E95A42E4283947A4575C58DC97DCDA346C8425B_CustomAttributesCacheGenerator_CenterPanel_get_IsOpen_m940BF4B14CFCDE8712C73BA4977DC35915E4A8D9,
	CenterPanel_t6E95A42E4283947A4575C58DC97DCDA346C8425B_CustomAttributesCacheGenerator_CenterPanel_set_IsOpen_m152CBDFFA76B9CC104CCD915BCCBEF5DFC04561B,
	DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_CustomAttributesCacheGenerator_DefaultCamera_add_ClickButton_m24CEFEDB1358B9016454A446AF9051A56A3B2F15,
	DefaultCamera_t840DC9D4E3D58642415C278E53152059FAAC2D0B_CustomAttributesCacheGenerator_DefaultCamera_remove_ClickButton_m84647F94186D6F54187B10DFC40058CB701D2F47,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_LoadLibraryPanel_LoadTextureWait_mA618B45C0CF015FB67E0CC2BAEC5C514C04186EA,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_LoadLibraryPanel_U3CStartU3Eb__10_0_m77BEB50BC1A88F7C4AF9BB91AE23E0B59E668CE4,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_LoadLibraryPanel_U3CStartU3Eb__10_1_m510E620AFE2724D80700CB9844057BB36345D472,
	LoadLibraryPanel_t3E88F208B1C35E8643367F0295731DB5C19F2394_CustomAttributesCacheGenerator_LoadLibraryPanel_U3CStartU3Eb__10_2_m4960AF4EC184CD83F48532911CDB32D3CD917A17,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_LoadPanel_U3CStartU3Eb__11_0_m5F905EF6A739AA5B00F23B22FD2BF2EDF64E025C,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_LoadPanel_U3CStartU3Eb__11_1_mDF50C8201D6487D2849659B9ADEC3C9B7F2986DC,
	LoadPanel_t15012A8AB1E8FDABEF21541EE515A68928038F9C_CustomAttributesCacheGenerator_LoadPanel_U3CStartU3Eb__11_2_mE799FD27D48690B248A84024F97E0E2AC1B22915,
	NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_NewFilePanel_U3CStartU3Eb__4_0_m2D133F62A9D1BFF48F5302C4D9A6BB5C1C0C1BC9,
	NewFilePanel_t0D595C412F6D3A0AE724980C2C16C1BACCFB134A_CustomAttributesCacheGenerator_NewFilePanel_U3CStartU3Eb__4_1_m26F08126DE7B00399E9508433CE748BED69FAC36,
	SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_SavePanel_U3CStartU3Eb__9_0_m06526363D4ED4F05D85EDFFB06F24408C0A902C1,
	SavePanel_tECC627C289AE0B979C1C9B356B32C2377303D7BE_CustomAttributesCacheGenerator_SavePanel_U3CStartU3Eb__9_1_m2800BE4939AD9E8A9CCFD2B5E5EC375596E7124C,
	ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_ControllerLaguage_add_EditLanguage_m0E43F99E1EDBEB3A9D92AD2B32D2BAEDE10C73E3,
	ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_ControllerLaguage_remove_EditLanguage_mFDD958D17D6B48C6330DD42B9ADEA5DD5338DF6E,
	ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_ControllerLaguage_get_isActive_mAAB75859C66032897913E16442299EBF5E0E91D2,
	ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_ControllerLaguage_set_isActive_mD16197CBBC968E72943AB612C47A75039709877F,
	ControllerLaguage_tF0BD79C705C247BDAABA08616015CCBFCDD90ECA_CustomAttributesCacheGenerator_ControllerLaguage_U3CStartU3Eb__8_0_mE39D5D344153FFEFA57B2B535131900903E72576,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_SaveFile_m05694E995B00E0A5B7C2398D638FFA488C6D9347,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_SaveFile_m353A16A62CAB468992DE601B9BCF7C82DE5124D5,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_LoadFile_m6BB857F0CAF2C51B2C0963F45B1579AEF5851978,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_LoadFile_m16C9D55F4A513E0B64BE9488511963928A78E4E6,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_NewFile_m41EF1125E326092467EB209A4928228AE1B03026,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_NewFile_mA3249B7015A2BCF69645E1F20635414C5A9AE801,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_LoadFileFromLibrary_m238B1208B65BF59D270AC282904BD000EDDD0254,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_LoadFileFromLibrary_m36D1A8A034C4671C4F9C42C9BDB432D9BAB8F904,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_EditPlayer_m48E07A1778D3D487B49DF3F9FC7233BBBAC42DE5,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_EditPlayer_mDD89630473B86F3CA70C5A1BE9802613FEC5CC97,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_EditItem_mC2063CA506A5FCD945E4C57A096FD7D6DDD76CBE,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_EditItem_mFF58CE5FB87F808F327D2E36A9ED3C750ABEBDE4,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_add_EditBlock_mE9AD06BA55F71022E6F8D72C5E400741A318C6F9,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_remove_EditBlock_m19CCBB8F2CD9A2BC04160F9345F28D89744EF5B0,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_get_Singleton_mDE37F6231B0F7936FFEEB0E801085D89EC6A8428,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_set_Singleton_m635E9F118FFF8B01383828CFA59D048613B2F11F,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_0_mCC6958034FDEA45ABB33ECF47C1CCAC01601BD9B,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_1_m40355F530568FDC7E1DEFBC00946261C315F4AFC,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_2_m5DA812FB86F0BDA0354F4D6875B1961C93966BA4,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_3_m2FC0FFC86010CBA9D4A3DF4DE23901A886D82333,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_4_m8F30655D551DD9CABA8D84BADB4ABBFAE5FDA0AA,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_5_m82FA5D1ED9AD9E5BF0A3BFA85CC3F1E768F7AE79,
	MenuPanel_t1524ECB737D9FDA755A20CBED1E229DF9A50861F_CustomAttributesCacheGenerator_MenuPanel_U3CStartU3Eb__33_6_m4B0C097BF677DDA5DC4441A6DAFFBBDE08D81AB6,
	ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3_CustomAttributesCacheGenerator_ColorPalitra_add_TakeItem_m2AB67F861A515A06640B67A3F8B6230B8742787C,
	ColorPalitra_tD6DF8824268317D289F12037B625A7804611D8C3_CustomAttributesCacheGenerator_ColorPalitra_remove_TakeItem_m0588DA2EB6E2C9BD8F33A8D394EE9BA9110D45B2,
	ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_ColorPickerPanel_U3CStartU3Eb__8_0_m75A75D7EA72B5C04D91F9A0E891668EF2729D771,
	ColorPickerPanel_tC6535189AE92DCC74E1C23BD2E01DC9743C93FC7_CustomAttributesCacheGenerator_ColorPickerPanel_U3CStartU3Eb__8_1_m9F169F862462214577F1A6005F98C8EA23F315AB,
	HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryButton_add_HistoryNext_mFA7B5A0E94A850C99B6F186447423F70F5E53D8F,
	HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryButton_remove_HistoryNext_m4952CFA1B71AB4C14D5E798971CB5F6E5A427941,
	HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryButton_add_HistoryBack_mFE5A88B7E7BF434ED70B6D60A5FB87CE48191E82,
	HistoryButton_t829C130DFBE5E8C5B3450D3BC18FD6EC85FE31CB_CustomAttributesCacheGenerator_HistoryButton_remove_HistoryBack_mD2863D6DE0ACBCF75E6742C8ADF504E66A41E2F9,
	Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_Instruments_get_InstrumentTake_mE8360FD723E9F6466EF0BF10D48301EF3B73B59F,
	Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_Instruments_set_InstrumentTake_mCAD9D564496C1F839D68AA299DE96B668752E99E,
	Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_Instruments_add_TakeInstrument_m647488514FAF8BDC89B6361ACE7D5CDA7975677A,
	Instruments_tF75DA728107310E221C75AD3538952EAEE98442E_CustomAttributesCacheGenerator_Instruments_remove_TakeInstrument_mB0D58527D59E36C769009665538F8831017970BF,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_add_TakePalitra_mE3BFDB24358C0AE3C122F29D911C18B86CA9F778,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_remove_TakePalitra_mC79E72F7911BABB60AC3DA561527D15B9B038AE8,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_get_Singleton_mA27F91D69CC9387223DBCDFF52512A59CFB69CAE,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_set_Singleton_mAF9BE98C99C52395B40215E2EE515500077D654D,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_U3CStartU3Eb__18_0_mB0B57DAE05470FABAE4B70A730AAA53D52EF28D4,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_U3CStartU3Eb__18_1_mFE44FADF95EBB75F7ECF776DA2C4C04912DAEA42,
	MenuPaltira_t9E867AE6D67110D139774686911DE61C5509E93A_CustomAttributesCacheGenerator_MenuPaltira_U3CStartU3Eb__18_2_m5B895236BB6DCB620EE58B5529018C9099E361CA,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_get_Instrument_m7F79D1932B2C71BB7B236D976366BFDC4965020C,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_set_Instrument_mE1F840BF319190F398A837FEA3A67DC41F936436,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_get_ColorList_m097292F6376815B20209CCD233B1F23800CD0C02,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_set_ColorList_m0C9A3AA69E4B3C52393A5D862DCAD9165E17BF43,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_get_History_mD749E9F50722D5B8D5110C265F2CDAEAFDD881AB,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_set_History_m5F1EDBD7B6D224B8F7CFB16668B6A03FA8588EB2,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_get_Singleton_mCE3314F8DAA7B9C9C3FE5193C610299AA1549574,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_set_Singleton_m5AAC644BDE919FCCA0846FC5CABE4328BEEF7630,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_U3CStartU3Eb__39_6_m5FD8DF5AED30CF7751EB8C00D5D6A4905887E247,
	PalitraPanel_t4A30316F331F65AF0A14DD0311BC351083E121FD_CustomAttributesCacheGenerator_PalitraPanel_U3CStartU3Eb__39_7_m14B1E4FB7298CCD39B2BE9108F1D9282F7D93FB0,
	PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_PanelProgram_U3CStartU3Eb__6_0_mB24D08F5DC87D7C9AB155E06FB3F72C4F69A8706,
	PanelProgram_t27479DB8FD11220FCCE67C81BBF0010F6CC5191F_CustomAttributesCacheGenerator_PanelProgram_U3CStartU3Eb__6_1_m38395C281139DE98FAE39AC937126D857AFD6E52,
	UV_tD6D433ACAC925D8B4FCF2E3D2C58DBA53D9BA72C_CustomAttributesCacheGenerator_UV_U3CStartU3Eb__3_0_mD1AE53B2DA7CAAD27C216DCD91C3BCB0B121B85D,
	UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_UVPanel_add_EditUV_m87ACD1040A8EAFCE0561D164276C7657896F145D,
	UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_UVPanel_remove_EditUV_m1F8CD365E32F47094F9F1E00E8BB22C9E2912A2E,
	UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_UVPanel_get_IsActive_mF298A653781B5B2009EB060D71EE5A41EB561C75,
	UVPanel_t5844DBF01F70219F84E09985B93BF9E6D3A19DB9_CustomAttributesCacheGenerator_UVPanel_set_IsActive_m257C20DA61922AA7F7B5CAFD94DEC2F178478008,
	BackEditor_t2B12029F349FB59E5B17DF1AD5D9A07BF10BE59E_CustomAttributesCacheGenerator_BackEditor_U3CStartU3Eb__1_0_m6C7249A519A003BADB6F90A05660C1D4DAEA74DB,
	ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_ControllerPlayer_get_Direct_m058EE79D5CDB56B7B316B20456071E6433346142,
	ControllerPlayer_t8D009BAAA2775A38957D80FA759F19316D2E8FC0_CustomAttributesCacheGenerator_ControllerPlayer_set_Direct_m07006D227548EB9A006A564398F8DAFEE2F0BBBF,
	BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_CustomAttributesCacheGenerator_BaseMenu_get_Singleton_m069EC972BD638A8FB525054E1611FB620CA75AA5,
	BaseMenu_t47EEFB72B009FCD4786619A94C374DC4B5918475_CustomAttributesCacheGenerator_BaseMenu_set_Singleton_mB1A2F5FFCDCC484C66BD011396C1192066AFBDAD,
	ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_ListNews_get_Singleton_mA989ACDE2D531B9D55A5BE2D5432EA4AA6356CBA,
	ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_ListNews_set_Singleton_m105CAC52256E242D85A30E5121072DF7CD25DEB8,
	ListNews_t3B0612B3B4C062FDA4CFE3260F768197287BEF01_CustomAttributesCacheGenerator_ListNews_LoadNewsWait_m8BA4ABE06B74001CABF6FC88B8E7C0E056C0730E,
	EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_EditModel_get_CurrentModel_m93448B5F4798C5462C4E37A57C117B5472B56A4D,
	EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_EditModel_set_CurrentModel_m24FB61D1B1CD0F63E872BA9F32A2D262AD65E94C,
	EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_EditModel_get_Singleton_mC5F7909A086EF7180B1518C5BF34A15278714A31,
	EditModel_tAC862ACEF13F0D5BC1261F3FD185ED44758C44E0_CustomAttributesCacheGenerator_EditModel_set_Singleton_m976F3966ED7AC897C0321CB41B8E8B5BF7EB6346,
	Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_Model_get_IsWriteHistory_m5EDD0BAEB57D2027752B3D5FE6F654A1289B4595,
	Model_t7DD65E77FC337D8F7C11F4F8BD2D056EFDCCF91A_CustomAttributesCacheGenerator_Model_set_IsWriteHistory_mC31712A7FC082CABA44BB58C5E52AAE6DA8A5EB0,
	Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_Tassel_get_TypeInstrument_m918CF89558E2EB0FCB856E8C3F13580C493DEDD2,
	Tassel_tACF7120C473D9DDA0A7FBF7DCAA36DE6E90BF87A_CustomAttributesCacheGenerator_Tassel_set_TypeInstrument_m398D1C548B8B6AFDB3D582B5497CE0E26B635AE4,
	AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_AudioController_get_Singleton_m23473DECF654EB8C9EF43BD85282CEFBF4023594,
	AudioController_tB84C770DEBCA2902ACF31469C023B7F71A863D3F_CustomAttributesCacheGenerator_AudioController_set_Singleton_m68D0A477505FE586F9AA25086DEA44F96AF3A6A9,
	VolumeAudio_t86FC5AF2A5FAEA4E7F1786255E39005EB558577A_CustomAttributesCacheGenerator_VolumeAudio_add_Editor_m129CC5D58B5CB0F17A2CB47BAFA30D576BA84364,
	VolumeAudio_t86FC5AF2A5FAEA4E7F1786255E39005EB558577A_CustomAttributesCacheGenerator_VolumeAudio_remove_Editor_mC2A7A7BFE820002D2ED7E321813E21CFDA4DEECA,
	VolumeAudio_t86FC5AF2A5FAEA4E7F1786255E39005EB558577A_CustomAttributesCacheGenerator_VolumeAudio_U3CStartU3Eb__5_0_mF07DE3ABA2B6761464B1DBEF8B4543BBB229E6D9,
	BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA_CustomAttributesCacheGenerator_BaseProcces_Update_mBC2C04C98E90C05C6C9347EDC4F03F9C64C1E5BD,
	BaseProcces_tA95954BB1CAEF84347F8485D3D3D09D65C3279FA_CustomAttributesCacheGenerator_BaseProcces_U3CUpdateU3Eb__8_0_mAD6652D6C6523D25840710F6A2E3C0C01767203F,
	U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator_U3CLoadTextureWaitU3Ed__11__ctor_mA8EFBFFC7538B986E47910341B21AAFC99C1D578,
	U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator_U3CLoadTextureWaitU3Ed__11_System_IDisposable_Dispose_m32278EABA09B57F45C467C7AC661D922A044A128,
	U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator_U3CLoadTextureWaitU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30AD20615A56147C89D07E4E825A3D1BCA280C41,
	U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator_U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_Reset_m1F745D6ACC0C15F9630C12F79F928F663ADC18B7,
	U3CLoadTextureWaitU3Ed__11_t74E30542504245A823200E7BDA9CF223E0F88D4E_CustomAttributesCacheGenerator_U3CLoadTextureWaitU3Ed__11_System_Collections_IEnumerator_get_Current_mC8A4904545EADE0AB3EEE97FED4AFE8FECF905A9,
	U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator_U3CLoadNewsWaitU3Ed__8__ctor_m954E460A8D2E865D49227C50AD953ADB62BD3751,
	U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator_U3CLoadNewsWaitU3Ed__8_System_IDisposable_Dispose_mD506D633D37BB4EDC8E584479ED3C7D4D05326C2,
	U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator_U3CLoadNewsWaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEA5FD9FFF67794B08B86C64DE1B8AA4D629E6CD,
	U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator_U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_Reset_m53159E79EBA8B80BAC673EDE60518A4F30D17A98,
	U3CLoadNewsWaitU3Ed__8_tB9A48311CC3B283613ED5DFE1B86A1E6B3AE88F9_CustomAttributesCacheGenerator_U3CLoadNewsWaitU3Ed__8_System_Collections_IEnumerator_get_Current_m3172BAD8411C52CFB7BBDC7E0C0F447315DFB4C5,
	History_tB9376052A6C8C07D431414CC16E050FE0EE9A156_CustomAttributesCacheGenerator_History_get_CurrentStory_m61B310DA105D915BC32F32D13180CE0EA6D339EE,
	History_tB9376052A6C8C07D431414CC16E050FE0EE9A156_CustomAttributesCacheGenerator_History_set_CurrentStory_mB9DD133CC721E11898C4C527125EDBF97BAE27D2,
	U3CUpdateU3Ed__8_t8DCB9D8B23DF5077A34193ECA92405089FFB1708_CustomAttributesCacheGenerator_U3CUpdateU3Ed__8_SetStateMachine_m7CB0536971DEF64B4EDF5E17A72BC6F20ACC8D6B,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
