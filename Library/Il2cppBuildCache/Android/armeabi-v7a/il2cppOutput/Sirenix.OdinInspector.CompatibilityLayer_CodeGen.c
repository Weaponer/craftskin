﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Sirenix.Serialization.Vector2IntFormatter::Read(UnityEngine.Vector2Int&,Sirenix.Serialization.IDataReader)
extern void Vector2IntFormatter_Read_mFB9DA3FC74659C4F2AED52FF1F8B7EA11B82BDD5 (void);
// 0x00000002 System.Void Sirenix.Serialization.Vector2IntFormatter::Write(UnityEngine.Vector2Int&,Sirenix.Serialization.IDataWriter)
extern void Vector2IntFormatter_Write_m9C80CB5BFB4C7E388B1D568E000FD724689CE39E (void);
// 0x00000003 System.Void Sirenix.Serialization.Vector2IntFormatter::.ctor()
extern void Vector2IntFormatter__ctor_m010B75B624A875C343F739CC2ED2527BD11A3FE2 (void);
// 0x00000004 System.Void Sirenix.Serialization.Vector2IntFormatter::.cctor()
extern void Vector2IntFormatter__cctor_m3CE7E86F8DC9E4EC407A09F406A58F48FF567CA1 (void);
// 0x00000005 System.Void Sirenix.Serialization.Vector3IntFormatter::Read(UnityEngine.Vector3Int&,Sirenix.Serialization.IDataReader)
extern void Vector3IntFormatter_Read_m6F87BB6DD0AAC6F8DDE75C41F409583A00D56524 (void);
// 0x00000006 System.Void Sirenix.Serialization.Vector3IntFormatter::Write(UnityEngine.Vector3Int&,Sirenix.Serialization.IDataWriter)
extern void Vector3IntFormatter_Write_m09558157A9795221A47737BA597A6364222ACCE3 (void);
// 0x00000007 System.Void Sirenix.Serialization.Vector3IntFormatter::.ctor()
extern void Vector3IntFormatter__ctor_mADE35180D9A59C48542B1C84A6E426ECDCA223AA (void);
// 0x00000008 System.Void Sirenix.Serialization.Vector3IntFormatter::.cctor()
extern void Vector3IntFormatter__cctor_m9C6D2A3E09FCA21B0EB4ACC452D45806B8DFF4C4 (void);
static Il2CppMethodPointer s_methodPointers[8] = 
{
	Vector2IntFormatter_Read_mFB9DA3FC74659C4F2AED52FF1F8B7EA11B82BDD5,
	Vector2IntFormatter_Write_m9C80CB5BFB4C7E388B1D568E000FD724689CE39E,
	Vector2IntFormatter__ctor_m010B75B624A875C343F739CC2ED2527BD11A3FE2,
	Vector2IntFormatter__cctor_m3CE7E86F8DC9E4EC407A09F406A58F48FF567CA1,
	Vector3IntFormatter_Read_m6F87BB6DD0AAC6F8DDE75C41F409583A00D56524,
	Vector3IntFormatter_Write_m09558157A9795221A47737BA597A6364222ACCE3,
	Vector3IntFormatter__ctor_mADE35180D9A59C48542B1C84A6E426ECDCA223AA,
	Vector3IntFormatter__cctor_m9C6D2A3E09FCA21B0EB4ACC452D45806B8DFF4C4,
};
static const int32_t s_InvokerIndices[8] = 
{
	663,
	663,
	1476,
	2458,
	663,
	663,
	1476,
	2458,
};
extern const CustomAttributesCacheGenerator g_Sirenix_OdinInspector_CompatibilityLayer_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Sirenix_OdinInspector_CompatibilityLayer_CodeGenModule;
const Il2CppCodeGenModule g_Sirenix_OdinInspector_CompatibilityLayer_CodeGenModule = 
{
	"Sirenix.OdinInspector.CompatibilityLayer.dll",
	8,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Sirenix_OdinInspector_CompatibilityLayer_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
