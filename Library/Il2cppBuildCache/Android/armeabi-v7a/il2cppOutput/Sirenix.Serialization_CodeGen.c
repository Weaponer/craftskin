﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 <Assembly>j__TPar <>f__AnonymousType0`2::get_Assembly()
// 0x00000002 <Attribute>j__TPar <>f__AnonymousType0`2::get_Attribute()
// 0x00000003 System.Void <>f__AnonymousType0`2::.ctor(<Assembly>j__TPar,<Attribute>j__TPar)
// 0x00000004 System.Boolean <>f__AnonymousType0`2::Equals(System.Object)
// 0x00000005 System.Int32 <>f__AnonymousType0`2::GetHashCode()
// 0x00000006 System.String <>f__AnonymousType0`2::ToString()
// 0x00000007 Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedBehaviour::Sirenix.Serialization.ISupportsPrefabSerialization.get_SerializationData()
extern void SerializedBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_mC289B8801FDF8F538F96BC30F496497CFD73E98C (void);
// 0x00000008 System.Void Sirenix.OdinInspector.SerializedBehaviour::Sirenix.Serialization.ISupportsPrefabSerialization.set_SerializationData(Sirenix.Serialization.SerializationData)
extern void SerializedBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m586726976E23BEDECD8A1B5F6966CE4C10C00899 (void);
// 0x00000009 System.Void Sirenix.OdinInspector.SerializedBehaviour::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m8B89639B50D750CCC8F4CE6B9EE04D3E6B7677D2 (void);
// 0x0000000A System.Void Sirenix.OdinInspector.SerializedBehaviour::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m7F62DB4AF6FBC223B2CE83C679109DBA62DA13C5 (void);
// 0x0000000B System.Void Sirenix.OdinInspector.SerializedBehaviour::OnAfterDeserialize()
extern void SerializedBehaviour_OnAfterDeserialize_mDFAF7ED43A889F0590AD84D8E73132EFBE2BC944 (void);
// 0x0000000C System.Void Sirenix.OdinInspector.SerializedBehaviour::OnBeforeSerialize()
extern void SerializedBehaviour_OnBeforeSerialize_mB764AA3AAEE6D7FEF69C2AAD9C5A2140506D3CE3 (void);
// 0x0000000D System.Void Sirenix.OdinInspector.SerializedBehaviour::.ctor()
extern void SerializedBehaviour__ctor_m356D243E69270532DA25BC4E9D1BA39D002F079D (void);
// 0x0000000E Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedComponent::Sirenix.Serialization.ISupportsPrefabSerialization.get_SerializationData()
extern void SerializedComponent_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_mA417BCE93AE8D9218DDADAE579E86940A69FB773 (void);
// 0x0000000F System.Void Sirenix.OdinInspector.SerializedComponent::Sirenix.Serialization.ISupportsPrefabSerialization.set_SerializationData(Sirenix.Serialization.SerializationData)
extern void SerializedComponent_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m3007D4F2B2F7FABC9CE117EA27C97902A5CA8FAE (void);
// 0x00000010 System.Void Sirenix.OdinInspector.SerializedComponent::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedComponent_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m9A6452B6E76E6DB9AC1EEA161BFD0C63644335CC (void);
// 0x00000011 System.Void Sirenix.OdinInspector.SerializedComponent::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedComponent_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mD6E78C9466D264C014C79D51E0E7506584F42EEE (void);
// 0x00000012 System.Void Sirenix.OdinInspector.SerializedComponent::OnAfterDeserialize()
extern void SerializedComponent_OnAfterDeserialize_m6C617D0E732FE23EF644C55621AA5D920F291B58 (void);
// 0x00000013 System.Void Sirenix.OdinInspector.SerializedComponent::OnBeforeSerialize()
extern void SerializedComponent_OnBeforeSerialize_mD47EFA734935428100551183EB29BB827A91A3B9 (void);
// 0x00000014 System.Void Sirenix.OdinInspector.SerializedComponent::.ctor()
extern void SerializedComponent__ctor_m5F48BCEECDBE9924054BC923745E3E126AB6825A (void);
// 0x00000015 Sirenix.Serialization.SerializationData Sirenix.OdinInspector.SerializedMonoBehaviour::Sirenix.Serialization.ISupportsPrefabSerialization.get_SerializationData()
extern void SerializedMonoBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_m81DE44BD44D84BCF0E0FBC38315BF962FD6F93B7 (void);
// 0x00000016 System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::Sirenix.Serialization.ISupportsPrefabSerialization.set_SerializationData(Sirenix.Serialization.SerializationData)
extern void SerializedMonoBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m4A7F7815A78A0E1D35B096A102EA785B75AF610E (void);
// 0x00000017 System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedMonoBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m4F21BFA951B3C6FBD3CBEF9B6BBB5B2837AF22EA (void);
// 0x00000018 System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedMonoBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m7EE94F9B77D5CB970456BC67765BA34FBB08CB45 (void);
// 0x00000019 System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::OnAfterDeserialize()
extern void SerializedMonoBehaviour_OnAfterDeserialize_mEEB6C905BC4A1F25CA377C0838CA7DA4F2A97ABD (void);
// 0x0000001A System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::OnBeforeSerialize()
extern void SerializedMonoBehaviour_OnBeforeSerialize_m5CCD7D67DDBD0C28F72B67D023435EB54FD6C900 (void);
// 0x0000001B System.Void Sirenix.OdinInspector.SerializedMonoBehaviour::.ctor()
extern void SerializedMonoBehaviour__ctor_mF2FF2DB3A67E9155B289B960A2D8E6C33AF9E354 (void);
// 0x0000001C System.Void Sirenix.OdinInspector.SerializedScriptableObject::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedScriptableObject_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m0BC63966B961B50811BE971F742E18C5377D2B99 (void);
// 0x0000001D System.Void Sirenix.OdinInspector.SerializedScriptableObject::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedScriptableObject_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m5B452F29FE36060E037D727B86CD2C5FA5EBF12D (void);
// 0x0000001E System.Void Sirenix.OdinInspector.SerializedScriptableObject::OnAfterDeserialize()
extern void SerializedScriptableObject_OnAfterDeserialize_mCCFBF5C89F560349721061B1AA2A4191BC05C3E5 (void);
// 0x0000001F System.Void Sirenix.OdinInspector.SerializedScriptableObject::OnBeforeSerialize()
extern void SerializedScriptableObject_OnBeforeSerialize_m98430D2E2FAB68C725366EE3AEFBEE8A792E7CC1 (void);
// 0x00000020 System.Void Sirenix.OdinInspector.SerializedScriptableObject::.ctor()
extern void SerializedScriptableObject__ctor_m163A094F76CE18ACBBC26ADF30FEE341A7A5CF71 (void);
// 0x00000021 System.Void Sirenix.OdinInspector.SerializedStateMachineBehaviour::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedStateMachineBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m45BED8434DC034C3C83DA1178BF4655D011EBCB6 (void);
// 0x00000022 System.Void Sirenix.OdinInspector.SerializedStateMachineBehaviour::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedStateMachineBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mE4AC59B703E69A064129030C07DE023D98155D70 (void);
// 0x00000023 System.Void Sirenix.OdinInspector.SerializedStateMachineBehaviour::OnAfterDeserialize()
extern void SerializedStateMachineBehaviour_OnAfterDeserialize_m03477ACAD9189F7DAFFA688C56395C225ADFC928 (void);
// 0x00000024 System.Void Sirenix.OdinInspector.SerializedStateMachineBehaviour::OnBeforeSerialize()
extern void SerializedStateMachineBehaviour_OnBeforeSerialize_m26BB1F10BCCAA5ACA995C9865DDEF3431AC2D334 (void);
// 0x00000025 System.Void Sirenix.OdinInspector.SerializedStateMachineBehaviour::.ctor()
extern void SerializedStateMachineBehaviour__ctor_m8E3801F75444E3DCC5871C9F1AB481D1E1546E37 (void);
// 0x00000026 System.Void Sirenix.OdinInspector.SerializedUnityObject::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void SerializedUnityObject_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m05E7F651B93DFB6D4679F57CF790BC5CFA929930 (void);
// 0x00000027 System.Void Sirenix.OdinInspector.SerializedUnityObject::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void SerializedUnityObject_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mB90A211A2583217EEE116B2CFDA518CEFCCEED70 (void);
// 0x00000028 System.Void Sirenix.OdinInspector.SerializedUnityObject::OnAfterDeserialize()
extern void SerializedUnityObject_OnAfterDeserialize_m621CCAB08FA678BBD56F2C0D84D22D079CAD9CED (void);
// 0x00000029 System.Void Sirenix.OdinInspector.SerializedUnityObject::OnBeforeSerialize()
extern void SerializedUnityObject_OnBeforeSerialize_m9C862889F19ADF4DBE5C5F5F1D091865AA286050 (void);
// 0x0000002A System.Void Sirenix.OdinInspector.SerializedUnityObject::.ctor()
extern void SerializedUnityObject__ctor_m1D8701D969719F7356869A37DC28E5E9408DE981 (void);
// 0x0000002B System.Void Sirenix.Serialization.MethodInfoFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x0000002C System.Void Sirenix.Serialization.MethodInfoFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x0000002D T Sirenix.Serialization.MethodInfoFormatter`1::GetUninitializedObject()
// 0x0000002E System.Void Sirenix.Serialization.MethodInfoFormatter`1::.ctor()
// 0x0000002F System.Void Sirenix.Serialization.MethodInfoFormatter`1::.cctor()
// 0x00000030 System.Void Sirenix.Serialization.MethodInfoFormatter`1/<>c::.cctor()
// 0x00000031 System.Void Sirenix.Serialization.MethodInfoFormatter`1/<>c::.ctor()
// 0x00000032 System.String Sirenix.Serialization.MethodInfoFormatter`1/<>c::<DeserializeImplementation>b__3_0(System.Type)
// 0x00000033 System.String Sirenix.Serialization.MethodInfoFormatter`1/<>c::<DeserializeImplementation>b__3_1(System.Type)
// 0x00000034 System.Void Sirenix.Serialization.QueueFormatter`2::.cctor()
// 0x00000035 System.Void Sirenix.Serialization.QueueFormatter`2::.ctor()
// 0x00000036 TQueue Sirenix.Serialization.QueueFormatter`2::GetUninitializedObject()
// 0x00000037 System.Void Sirenix.Serialization.QueueFormatter`2::DeserializeImplementation(TQueue&,Sirenix.Serialization.IDataReader)
// 0x00000038 System.Void Sirenix.Serialization.QueueFormatter`2::SerializeImplementation(TQueue&,Sirenix.Serialization.IDataWriter)
// 0x00000039 System.Void Sirenix.Serialization.ReflectionOrEmittedBaseFormatter`1::.ctor()
// 0x0000003A System.Void Sirenix.Serialization.StackFormatter`2::.cctor()
// 0x0000003B System.Void Sirenix.Serialization.StackFormatter`2::.ctor()
// 0x0000003C TStack Sirenix.Serialization.StackFormatter`2::GetUninitializedObject()
// 0x0000003D System.Void Sirenix.Serialization.StackFormatter`2::DeserializeImplementation(TStack&,Sirenix.Serialization.IDataReader)
// 0x0000003E System.Void Sirenix.Serialization.StackFormatter`2::SerializeImplementation(TStack&,Sirenix.Serialization.IDataWriter)
// 0x0000003F System.Version Sirenix.Serialization.VersionFormatter::GetUninitializedObject()
extern void VersionFormatter_GetUninitializedObject_m5DED5D826FA2D2DA98C0AD2BB013DA4D6C6A03A3 (void);
// 0x00000040 System.Void Sirenix.Serialization.VersionFormatter::Read(System.Version&,Sirenix.Serialization.IDataReader)
extern void VersionFormatter_Read_m0452207D96D195E7BF3722F5981B1A680995EC62 (void);
// 0x00000041 System.Void Sirenix.Serialization.VersionFormatter::Write(System.Version&,Sirenix.Serialization.IDataWriter)
extern void VersionFormatter_Write_m14081079EC7769C626631431AB69EBD93B71437E (void);
// 0x00000042 System.Void Sirenix.Serialization.VersionFormatter::.ctor()
extern void VersionFormatter__ctor_m1F0C803A8982E1B816351279D2A02F18BE8CE232 (void);
// 0x00000043 System.Void Sirenix.Serialization.AllowDeserializeInvalidDataAttribute::.ctor()
extern void AllowDeserializeInvalidDataAttribute__ctor_mFFF4786F2649B69939A6D1B47D3E5083A2D0FAAD (void);
// 0x00000044 System.Void Sirenix.Serialization.ArchitectureInfo::.cctor()
extern void ArchitectureInfo__cctor_m692CCE538DFA897EE1885AA0CAE9CFF86C56B188 (void);
// 0x00000045 System.Void Sirenix.Serialization.ArchitectureInfo::SetIsOnAndroid(System.String)
extern void ArchitectureInfo_SetIsOnAndroid_m15ADE609814605F039081310CC3B339B03D3D195 (void);
// 0x00000046 System.Void Sirenix.Serialization.ArchitectureInfo::SetIsNotOnAndroid()
extern void ArchitectureInfo_SetIsNotOnAndroid_m2B12FABB59E301E7B56CFEF6D79E81DF00AF2B62 (void);
// 0x00000047 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.IOverridesSerializationPolicy::get_SerializationPolicy()
// 0x00000048 System.Boolean Sirenix.Serialization.IOverridesSerializationPolicy::get_OdinSerializesUnityFields()
// 0x00000049 System.Boolean Sirenix.Serialization.ArrayFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void ArrayFormatterLocator_TryGetFormatter_m8C34CBFCB8FC58A022B322D5562751ED58264FA3 (void);
// 0x0000004A System.Void Sirenix.Serialization.ArrayFormatterLocator::.ctor()
extern void ArrayFormatterLocator__ctor_m41BD5C69A3B30FB2ED65AC5E8EFD651F1E34D597 (void);
// 0x0000004B System.Boolean Sirenix.Serialization.DelegateFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void DelegateFormatterLocator_TryGetFormatter_mCA7DA7565932BAA967FBE64F4F63C12EFF1BC990 (void);
// 0x0000004C System.Void Sirenix.Serialization.DelegateFormatterLocator::.ctor()
extern void DelegateFormatterLocator__ctor_mD4BEAC1E9DD546B9222B5BA3BED7B3B836FD0A14 (void);
// 0x0000004D System.Boolean Sirenix.Serialization.IAskIfCanFormatTypes::CanFormatType(System.Type)
// 0x0000004E System.Boolean Sirenix.Serialization.IFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
// 0x0000004F System.Boolean Sirenix.Serialization.GenericCollectionFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void GenericCollectionFormatterLocator_TryGetFormatter_mE145653AF6915E25A2B6BF06E41376A636448660 (void);
// 0x00000050 System.Void Sirenix.Serialization.GenericCollectionFormatterLocator::.ctor()
extern void GenericCollectionFormatterLocator__ctor_m900B5B9E7AF1AFA077807D88D19449CFAE150C82 (void);
// 0x00000051 System.Boolean Sirenix.Serialization.ISerializableFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void ISerializableFormatterLocator_TryGetFormatter_m79DFE5A7D6182AA34FD65DD869752C4821580932 (void);
// 0x00000052 System.Void Sirenix.Serialization.ISerializableFormatterLocator::.ctor()
extern void ISerializableFormatterLocator__ctor_m7234E223207BCED84989670AA09019272647D6D9 (void);
// 0x00000053 System.Type Sirenix.Serialization.RegisterFormatterAttribute::get_FormatterType()
extern void RegisterFormatterAttribute_get_FormatterType_m1C17ED27D02A49CF77B26EAD119E1A6BD53B3774 (void);
// 0x00000054 System.Void Sirenix.Serialization.RegisterFormatterAttribute::set_FormatterType(System.Type)
extern void RegisterFormatterAttribute_set_FormatterType_m103C9B8E2F00DD395423F02905FC2C9526439F2A (void);
// 0x00000055 System.Int32 Sirenix.Serialization.RegisterFormatterAttribute::get_Priority()
extern void RegisterFormatterAttribute_get_Priority_m058B73DFB7DD5061A54BAC4668D169CE59B517DD (void);
// 0x00000056 System.Void Sirenix.Serialization.RegisterFormatterAttribute::set_Priority(System.Int32)
extern void RegisterFormatterAttribute_set_Priority_m1BD98F9B65102165E4C73855D8C1DF0CB5BF3A52 (void);
// 0x00000057 System.Void Sirenix.Serialization.RegisterFormatterAttribute::.ctor(System.Type,System.Int32)
extern void RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F (void);
// 0x00000058 System.Type Sirenix.Serialization.RegisterFormatterLocatorAttribute::get_FormatterLocatorType()
extern void RegisterFormatterLocatorAttribute_get_FormatterLocatorType_m1CC4E212C4AC33E6970C9D25E5BE73238A45BDB1 (void);
// 0x00000059 System.Void Sirenix.Serialization.RegisterFormatterLocatorAttribute::set_FormatterLocatorType(System.Type)
extern void RegisterFormatterLocatorAttribute_set_FormatterLocatorType_m64378818193092212C1B5A56F1A4177C685C046F (void);
// 0x0000005A System.Int32 Sirenix.Serialization.RegisterFormatterLocatorAttribute::get_Priority()
extern void RegisterFormatterLocatorAttribute_get_Priority_m8ADAD66CF0E823C054ABEAC48FDEC5673D33D373 (void);
// 0x0000005B System.Void Sirenix.Serialization.RegisterFormatterLocatorAttribute::set_Priority(System.Int32)
extern void RegisterFormatterLocatorAttribute_set_Priority_m87C8053D211AB6E47C71D3AAF8320D0436C49629 (void);
// 0x0000005C System.Void Sirenix.Serialization.RegisterFormatterLocatorAttribute::.ctor(System.Type,System.Int32)
extern void RegisterFormatterLocatorAttribute__ctor_mFE77FF1296D532E4B067CF3A7313355B258F49CF (void);
// 0x0000005D System.Boolean Sirenix.Serialization.SelfFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void SelfFormatterLocator_TryGetFormatter_mC7700F7994BD29E2E432BE3BB398DF1C28ECFB29 (void);
// 0x0000005E System.Void Sirenix.Serialization.SelfFormatterLocator::.ctor()
extern void SelfFormatterLocator__ctor_m2DFBDEB40459F426EF1D520739761D56B59570C8 (void);
// 0x0000005F System.Boolean Sirenix.Serialization.TypeFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void TypeFormatterLocator_TryGetFormatter_mFFA8422E40A034B1C206879D28A56FD5D5AF2342 (void);
// 0x00000060 System.Void Sirenix.Serialization.TypeFormatterLocator::.ctor()
extern void TypeFormatterLocator__ctor_m19B687011F899AD0CEACB819F052C9039D885C90 (void);
// 0x00000061 System.Void Sirenix.Serialization.BaseDataReader::.ctor(System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void BaseDataReader__ctor_m256753B94B198C37A18F99AB3E00D012DB0A67B9 (void);
// 0x00000062 System.Int32 Sirenix.Serialization.BaseDataReader::get_CurrentNodeId()
extern void BaseDataReader_get_CurrentNodeId_mC6F23729B5DFA64E14E406B955ACA5B254B75013 (void);
// 0x00000063 System.Int32 Sirenix.Serialization.BaseDataReader::get_CurrentNodeDepth()
extern void BaseDataReader_get_CurrentNodeDepth_m17D19783D54C4270601ADE7A3B5E7E7FC3FD53E6 (void);
// 0x00000064 System.String Sirenix.Serialization.BaseDataReader::get_CurrentNodeName()
extern void BaseDataReader_get_CurrentNodeName_m36F3D74FDC23FB8BE63B9D0AD02DCAA33D38403C (void);
// 0x00000065 System.IO.Stream Sirenix.Serialization.BaseDataReader::get_Stream()
extern void BaseDataReader_get_Stream_m0B8E2A821FBFD53339BD694838A6D63071A906D9 (void);
// 0x00000066 System.Void Sirenix.Serialization.BaseDataReader::set_Stream(System.IO.Stream)
extern void BaseDataReader_set_Stream_mD9A087F8758320D8C3B31CCEA70F0BFB512F7D54 (void);
// 0x00000067 Sirenix.Serialization.DeserializationContext Sirenix.Serialization.BaseDataReader::get_Context()
extern void BaseDataReader_get_Context_m0D563DB48CC3EFD72D62B7B571C855E511EF77AF (void);
// 0x00000068 System.Void Sirenix.Serialization.BaseDataReader::set_Context(Sirenix.Serialization.DeserializationContext)
extern void BaseDataReader_set_Context_mDF7C1D3E3C8332B38DA9BFF73CCFCF98A4D0BC44 (void);
// 0x00000069 System.Boolean Sirenix.Serialization.BaseDataReader::EnterNode(System.Type&)
// 0x0000006A System.Boolean Sirenix.Serialization.BaseDataReader::ExitNode()
// 0x0000006B System.Boolean Sirenix.Serialization.BaseDataReader::EnterArray(System.Int64&)
// 0x0000006C System.Boolean Sirenix.Serialization.BaseDataReader::ExitArray()
// 0x0000006D System.Boolean Sirenix.Serialization.BaseDataReader::ReadPrimitiveArray(T[]&)
// 0x0000006E Sirenix.Serialization.EntryType Sirenix.Serialization.BaseDataReader::PeekEntry(System.String&)
// 0x0000006F System.Boolean Sirenix.Serialization.BaseDataReader::ReadInternalReference(System.Int32&)
// 0x00000070 System.Boolean Sirenix.Serialization.BaseDataReader::ReadExternalReference(System.Int32&)
// 0x00000071 System.Boolean Sirenix.Serialization.BaseDataReader::ReadExternalReference(System.Guid&)
// 0x00000072 System.Boolean Sirenix.Serialization.BaseDataReader::ReadExternalReference(System.String&)
// 0x00000073 System.Boolean Sirenix.Serialization.BaseDataReader::ReadChar(System.Char&)
// 0x00000074 System.Boolean Sirenix.Serialization.BaseDataReader::ReadString(System.String&)
// 0x00000075 System.Boolean Sirenix.Serialization.BaseDataReader::ReadGuid(System.Guid&)
// 0x00000076 System.Boolean Sirenix.Serialization.BaseDataReader::ReadSByte(System.SByte&)
// 0x00000077 System.Boolean Sirenix.Serialization.BaseDataReader::ReadInt16(System.Int16&)
// 0x00000078 System.Boolean Sirenix.Serialization.BaseDataReader::ReadInt32(System.Int32&)
// 0x00000079 System.Boolean Sirenix.Serialization.BaseDataReader::ReadInt64(System.Int64&)
// 0x0000007A System.Boolean Sirenix.Serialization.BaseDataReader::ReadByte(System.Byte&)
// 0x0000007B System.Boolean Sirenix.Serialization.BaseDataReader::ReadUInt16(System.UInt16&)
// 0x0000007C System.Boolean Sirenix.Serialization.BaseDataReader::ReadUInt32(System.UInt32&)
// 0x0000007D System.Boolean Sirenix.Serialization.BaseDataReader::ReadUInt64(System.UInt64&)
// 0x0000007E System.Boolean Sirenix.Serialization.BaseDataReader::ReadDecimal(System.Decimal&)
// 0x0000007F System.Boolean Sirenix.Serialization.BaseDataReader::ReadSingle(System.Single&)
// 0x00000080 System.Boolean Sirenix.Serialization.BaseDataReader::ReadDouble(System.Double&)
// 0x00000081 System.Boolean Sirenix.Serialization.BaseDataReader::ReadBoolean(System.Boolean&)
// 0x00000082 System.Boolean Sirenix.Serialization.BaseDataReader::ReadNull()
// 0x00000083 System.Void Sirenix.Serialization.BaseDataReader::SkipEntry()
extern void BaseDataReader_SkipEntry_m10955099B4E1F766C3837AD40E6F02BF682844B8 (void);
// 0x00000084 System.Void Sirenix.Serialization.BaseDataReader::Dispose()
// 0x00000085 System.Void Sirenix.Serialization.BaseDataReader::PrepareNewSerializationSession()
extern void BaseDataReader_PrepareNewSerializationSession_mF6AF3B535901BA601B2CBFF20563557FFDA3FAC5 (void);
// 0x00000086 System.String Sirenix.Serialization.BaseDataReader::GetDataDump()
// 0x00000087 Sirenix.Serialization.EntryType Sirenix.Serialization.BaseDataReader::PeekEntry()
// 0x00000088 Sirenix.Serialization.EntryType Sirenix.Serialization.BaseDataReader::ReadToNextEntry()
// 0x00000089 Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.BaseDataReaderWriter::get_Binder()
extern void BaseDataReaderWriter_get_Binder_m39DA92F0A5E7DA06EF49C1934D35E9966C61574A (void);
// 0x0000008A System.Void Sirenix.Serialization.BaseDataReaderWriter::set_Binder(Sirenix.Serialization.TwoWaySerializationBinder)
extern void BaseDataReaderWriter_set_Binder_m1C468BD577D8B4E008D4F2E59BB5036D596BDE24 (void);
// 0x0000008B System.Boolean Sirenix.Serialization.BaseDataReaderWriter::get_IsInArrayNode()
extern void BaseDataReaderWriter_get_IsInArrayNode_mCBFA18B413E9F33644D948D4BFAFE45DCE985309 (void);
// 0x0000008C System.Int32 Sirenix.Serialization.BaseDataReaderWriter::get_NodeDepth()
extern void BaseDataReaderWriter_get_NodeDepth_m6AB0F093329721AA6090479F46C820CD71EDA423 (void);
// 0x0000008D Sirenix.Serialization.NodeInfo Sirenix.Serialization.BaseDataReaderWriter::get_CurrentNode()
extern void BaseDataReaderWriter_get_CurrentNode_m2B4FAFE63F62D605EA764E5B190407D616CBBF1F (void);
// 0x0000008E System.Void Sirenix.Serialization.BaseDataReaderWriter::PushNode(Sirenix.Serialization.NodeInfo)
extern void BaseDataReaderWriter_PushNode_mCA60B6F3D84333621B2273D29EFA93E3FF0FDBFB (void);
// 0x0000008F System.Void Sirenix.Serialization.BaseDataReaderWriter::PushNode(System.String,System.Int32,System.Type)
extern void BaseDataReaderWriter_PushNode_mEBBA2F2A706EF74847F1DC4F14ED912FAB9EC8A1 (void);
// 0x00000090 System.Void Sirenix.Serialization.BaseDataReaderWriter::PushArray()
extern void BaseDataReaderWriter_PushArray_m9F29E9F0CAB3669AB9C24CFC3E78327DC439C8F2 (void);
// 0x00000091 System.Void Sirenix.Serialization.BaseDataReaderWriter::ExpandNodes()
extern void BaseDataReaderWriter_ExpandNodes_m8206869312402DF7F119526B979376A60B7D8457 (void);
// 0x00000092 System.Void Sirenix.Serialization.BaseDataReaderWriter::PopNode(System.String)
extern void BaseDataReaderWriter_PopNode_m0FA87E4A14793A8E06D1668D4ADAB9844C53C135 (void);
// 0x00000093 System.Void Sirenix.Serialization.BaseDataReaderWriter::PopArray()
extern void BaseDataReaderWriter_PopArray_m6430ED605E93A2D89C0BFE7721075D2164FC6B9D (void);
// 0x00000094 System.Void Sirenix.Serialization.BaseDataReaderWriter::ClearNodes()
extern void BaseDataReaderWriter_ClearNodes_m4D867C6DDA7F78E5881903876FB17E7CD821EB63 (void);
// 0x00000095 System.Void Sirenix.Serialization.BaseDataReaderWriter::.ctor()
extern void BaseDataReaderWriter__ctor_m5BC5AF02E5D64FD49CB2BB2D0DD5541500666590 (void);
// 0x00000096 System.Void Sirenix.Serialization.BaseDataWriter::.ctor(System.IO.Stream,Sirenix.Serialization.SerializationContext)
extern void BaseDataWriter__ctor_mAD50DC5F7248B12D05EF365F9BADA914712BE01D (void);
// 0x00000097 System.IO.Stream Sirenix.Serialization.BaseDataWriter::get_Stream()
extern void BaseDataWriter_get_Stream_m8AD1108B366507F290BE556EDF1CA06874F010BB (void);
// 0x00000098 System.Void Sirenix.Serialization.BaseDataWriter::set_Stream(System.IO.Stream)
extern void BaseDataWriter_set_Stream_mE72B376E6485FF218E81A23C720DB1DF3928E204 (void);
// 0x00000099 Sirenix.Serialization.SerializationContext Sirenix.Serialization.BaseDataWriter::get_Context()
extern void BaseDataWriter_get_Context_m2C473D483A6F6A53F8D3C1A7236E7DF027C74C57 (void);
// 0x0000009A System.Void Sirenix.Serialization.BaseDataWriter::set_Context(Sirenix.Serialization.SerializationContext)
extern void BaseDataWriter_set_Context_m3CD791A13D75824C0D87124A37234CC7A407C40A (void);
// 0x0000009B System.Void Sirenix.Serialization.BaseDataWriter::FlushToStream()
extern void BaseDataWriter_FlushToStream_m3E58B8F046C907089AC66237C16BD3BE86D5082B (void);
// 0x0000009C System.Void Sirenix.Serialization.BaseDataWriter::BeginReferenceNode(System.String,System.Type,System.Int32)
// 0x0000009D System.Void Sirenix.Serialization.BaseDataWriter::BeginStructNode(System.String,System.Type)
// 0x0000009E System.Void Sirenix.Serialization.BaseDataWriter::EndNode(System.String)
// 0x0000009F System.Void Sirenix.Serialization.BaseDataWriter::BeginArrayNode(System.Int64)
// 0x000000A0 System.Void Sirenix.Serialization.BaseDataWriter::EndArrayNode()
// 0x000000A1 System.Void Sirenix.Serialization.BaseDataWriter::WritePrimitiveArray(T[])
// 0x000000A2 System.Void Sirenix.Serialization.BaseDataWriter::WriteNull(System.String)
// 0x000000A3 System.Void Sirenix.Serialization.BaseDataWriter::WriteInternalReference(System.String,System.Int32)
// 0x000000A4 System.Void Sirenix.Serialization.BaseDataWriter::WriteExternalReference(System.String,System.Int32)
// 0x000000A5 System.Void Sirenix.Serialization.BaseDataWriter::WriteExternalReference(System.String,System.Guid)
// 0x000000A6 System.Void Sirenix.Serialization.BaseDataWriter::WriteExternalReference(System.String,System.String)
// 0x000000A7 System.Void Sirenix.Serialization.BaseDataWriter::WriteChar(System.String,System.Char)
// 0x000000A8 System.Void Sirenix.Serialization.BaseDataWriter::WriteString(System.String,System.String)
// 0x000000A9 System.Void Sirenix.Serialization.BaseDataWriter::WriteGuid(System.String,System.Guid)
// 0x000000AA System.Void Sirenix.Serialization.BaseDataWriter::WriteSByte(System.String,System.SByte)
// 0x000000AB System.Void Sirenix.Serialization.BaseDataWriter::WriteInt16(System.String,System.Int16)
// 0x000000AC System.Void Sirenix.Serialization.BaseDataWriter::WriteInt32(System.String,System.Int32)
// 0x000000AD System.Void Sirenix.Serialization.BaseDataWriter::WriteInt64(System.String,System.Int64)
// 0x000000AE System.Void Sirenix.Serialization.BaseDataWriter::WriteByte(System.String,System.Byte)
// 0x000000AF System.Void Sirenix.Serialization.BaseDataWriter::WriteUInt16(System.String,System.UInt16)
// 0x000000B0 System.Void Sirenix.Serialization.BaseDataWriter::WriteUInt32(System.String,System.UInt32)
// 0x000000B1 System.Void Sirenix.Serialization.BaseDataWriter::WriteUInt64(System.String,System.UInt64)
// 0x000000B2 System.Void Sirenix.Serialization.BaseDataWriter::WriteDecimal(System.String,System.Decimal)
// 0x000000B3 System.Void Sirenix.Serialization.BaseDataWriter::WriteSingle(System.String,System.Single)
// 0x000000B4 System.Void Sirenix.Serialization.BaseDataWriter::WriteDouble(System.String,System.Double)
// 0x000000B5 System.Void Sirenix.Serialization.BaseDataWriter::WriteBoolean(System.String,System.Boolean)
// 0x000000B6 System.Void Sirenix.Serialization.BaseDataWriter::Dispose()
// 0x000000B7 System.Void Sirenix.Serialization.BaseDataWriter::PrepareNewSerializationSession()
extern void BaseDataWriter_PrepareNewSerializationSession_mC7F69114FC5086BEA0F3E06A12B3E4BCE26AE3EC (void);
// 0x000000B8 System.String Sirenix.Serialization.BaseDataWriter::GetDataDump()
// 0x000000B9 System.Void Sirenix.Serialization.BinaryDataReader::.ctor()
extern void BinaryDataReader__ctor_m05150B03F07E1347650C2AF9A1D20B5991BDD850 (void);
// 0x000000BA System.Void Sirenix.Serialization.BinaryDataReader::.ctor(System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void BinaryDataReader__ctor_mFDD96108B2BFD1AB4CD1991525AB8904E86FAC35 (void);
// 0x000000BB System.Void Sirenix.Serialization.BinaryDataReader::Dispose()
extern void BinaryDataReader_Dispose_mC7BDE37C48E5E7EF1391B8AEC47F7326947E3810 (void);
// 0x000000BC Sirenix.Serialization.EntryType Sirenix.Serialization.BinaryDataReader::PeekEntry(System.String&)
extern void BinaryDataReader_PeekEntry_mEC799EA1CC942523C88AF4669265E78EF095EC96 (void);
// 0x000000BD System.Boolean Sirenix.Serialization.BinaryDataReader::EnterArray(System.Int64&)
extern void BinaryDataReader_EnterArray_m41BB6D5EA1C76904028F79F5BD6644BE354C3DDD (void);
// 0x000000BE System.Boolean Sirenix.Serialization.BinaryDataReader::EnterNode(System.Type&)
extern void BinaryDataReader_EnterNode_mF82CEEC287E24BD0966CB343107470587DE20813 (void);
// 0x000000BF System.Boolean Sirenix.Serialization.BinaryDataReader::ExitArray()
extern void BinaryDataReader_ExitArray_mF7CFD5166FA17CEF7F9899A82E4A0118D8319B88 (void);
// 0x000000C0 System.Boolean Sirenix.Serialization.BinaryDataReader::ExitNode()
extern void BinaryDataReader_ExitNode_m2EC7D442B3385FC322641E4D45BFDD9D4CFE305E (void);
// 0x000000C1 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadPrimitiveArray(T[]&)
// 0x000000C2 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadBoolean(System.Boolean&)
extern void BinaryDataReader_ReadBoolean_mD7F1DB823D02DF29F25AF01262775D52939D24A6 (void);
// 0x000000C3 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadSByte(System.SByte&)
extern void BinaryDataReader_ReadSByte_m48F1429088A24529BE223548D360A460CD88326A (void);
// 0x000000C4 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadByte(System.Byte&)
extern void BinaryDataReader_ReadByte_m8D47EDF23AA7D0CEC48D14FB5545BD436A6CF5B1 (void);
// 0x000000C5 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadInt16(System.Int16&)
extern void BinaryDataReader_ReadInt16_mCA2DF35896428C31996EEB22CC11BFBB6C6ABCC6 (void);
// 0x000000C6 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadUInt16(System.UInt16&)
extern void BinaryDataReader_ReadUInt16_m632C99D11618FD895D775FAC821F512CA818151F (void);
// 0x000000C7 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadInt32(System.Int32&)
extern void BinaryDataReader_ReadInt32_m09C1594A33027EA4BB5DC52B6CFCC6802F2541BE (void);
// 0x000000C8 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadUInt32(System.UInt32&)
extern void BinaryDataReader_ReadUInt32_m5FBF556B59431A9AB172186B9BC1B950B812715D (void);
// 0x000000C9 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadInt64(System.Int64&)
extern void BinaryDataReader_ReadInt64_mEB5EC49D6123C1674B1C222A5BF5DF95EB20CBA5 (void);
// 0x000000CA System.Boolean Sirenix.Serialization.BinaryDataReader::ReadUInt64(System.UInt64&)
extern void BinaryDataReader_ReadUInt64_m9CF4EA5C13AE1320D2215AE17D24C6FC4013E44A (void);
// 0x000000CB System.Boolean Sirenix.Serialization.BinaryDataReader::ReadChar(System.Char&)
extern void BinaryDataReader_ReadChar_mCD0BC343F96BEBF65A7DFF78D7542DD282025B7F (void);
// 0x000000CC System.Boolean Sirenix.Serialization.BinaryDataReader::ReadSingle(System.Single&)
extern void BinaryDataReader_ReadSingle_mD68DD096FBAB474D89A317C84601C750C790BC07 (void);
// 0x000000CD System.Boolean Sirenix.Serialization.BinaryDataReader::ReadDouble(System.Double&)
extern void BinaryDataReader_ReadDouble_mF1682719F3267CECBF0B03FD9D3E415FA5A97A1E (void);
// 0x000000CE System.Boolean Sirenix.Serialization.BinaryDataReader::ReadDecimal(System.Decimal&)
extern void BinaryDataReader_ReadDecimal_m727CD887A65D823FF0B71704FF3D70838AA445F2 (void);
// 0x000000CF System.Boolean Sirenix.Serialization.BinaryDataReader::ReadExternalReference(System.Guid&)
extern void BinaryDataReader_ReadExternalReference_m4EB2BC7CFD6BE53D13A163F91549C96F7D08BDE6 (void);
// 0x000000D0 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadGuid(System.Guid&)
extern void BinaryDataReader_ReadGuid_m69E91E179A7CDE8C7A1514A980EE46015B71E5C0 (void);
// 0x000000D1 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadExternalReference(System.Int32&)
extern void BinaryDataReader_ReadExternalReference_m3783E471805C048E2E6867C83D8DA28BF3161C06 (void);
// 0x000000D2 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadExternalReference(System.String&)
extern void BinaryDataReader_ReadExternalReference_m16E5FFE6DBD5F11F9571ABA820A5B797BC7CE495 (void);
// 0x000000D3 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadNull()
extern void BinaryDataReader_ReadNull_mF1E81D13C67DB44403256DB0D3428F7E31EC8D80 (void);
// 0x000000D4 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadInternalReference(System.Int32&)
extern void BinaryDataReader_ReadInternalReference_m0C40DF6875D1BF4724C2C37F69BD72C2168D9BB9 (void);
// 0x000000D5 System.Boolean Sirenix.Serialization.BinaryDataReader::ReadString(System.String&)
extern void BinaryDataReader_ReadString_m1D6906C020609C2FF04B3D571E247AB37226A4DB (void);
// 0x000000D6 System.Void Sirenix.Serialization.BinaryDataReader::PrepareNewSerializationSession()
extern void BinaryDataReader_PrepareNewSerializationSession_m702B8BF4D463BD73FAF5EEFF804CC80CBEEA9006 (void);
// 0x000000D7 System.String Sirenix.Serialization.BinaryDataReader::GetDataDump()
extern void BinaryDataReader_GetDataDump_m74E44CF70297CBC6A5A611973835C1C5C9C3CD08 (void);
// 0x000000D8 System.String Sirenix.Serialization.BinaryDataReader::ReadStringValue()
extern void BinaryDataReader_ReadStringValue_mA9DF0F142F9786152F12719D972773CD2451E6A8 (void);
// 0x000000D9 System.Void Sirenix.Serialization.BinaryDataReader::SkipStringValue()
extern void BinaryDataReader_SkipStringValue_mEF77DDFED47E4027B7DFF9640C110B019F7DC3AD (void);
// 0x000000DA System.Void Sirenix.Serialization.BinaryDataReader::SkipPeekedEntryContent()
extern void BinaryDataReader_SkipPeekedEntryContent_mCB193D7120CC6E432B5F36C2538F57BD3DB185C5 (void);
// 0x000000DB System.Boolean Sirenix.Serialization.BinaryDataReader::SkipBuffer(System.Int32)
extern void BinaryDataReader_SkipBuffer_m9A5FE0AABAFDA581BA66B4A908FA92F7ADF7F43F (void);
// 0x000000DC System.Type Sirenix.Serialization.BinaryDataReader::ReadTypeEntry()
extern void BinaryDataReader_ReadTypeEntry_mF9D131780191BBE416F74C863A74F1AA70238A4C (void);
// 0x000000DD System.Void Sirenix.Serialization.BinaryDataReader::MarkEntryContentConsumed()
extern void BinaryDataReader_MarkEntryContentConsumed_m376D33AB9CBFA74D2B2AA90702C179B8EF4F09BF (void);
// 0x000000DE Sirenix.Serialization.EntryType Sirenix.Serialization.BinaryDataReader::PeekEntry()
extern void BinaryDataReader_PeekEntry_m22654373898FF434F0E3081F7B9D1ACCFCDEA39D (void);
// 0x000000DF Sirenix.Serialization.EntryType Sirenix.Serialization.BinaryDataReader::ReadToNextEntry()
extern void BinaryDataReader_ReadToNextEntry_mF8440534928AB759B5BBB9CEC6D0EA50995C05E2 (void);
// 0x000000E0 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_1_Byte(System.Byte&)
extern void BinaryDataReader_UNSAFE_Read_1_Byte_mE414283A399102404054B59F6E203D7E28F4AE48 (void);
// 0x000000E1 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_1_SByte(System.SByte&)
extern void BinaryDataReader_UNSAFE_Read_1_SByte_m7E94DA7F6E44246B9B9BAA5C1A52A4DCAE8ED88C (void);
// 0x000000E2 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_2_Int16(System.Int16&)
extern void BinaryDataReader_UNSAFE_Read_2_Int16_mCDF250F2DD9D3DB5C6D45DBC0004844D26A2AB0F (void);
// 0x000000E3 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_2_UInt16(System.UInt16&)
extern void BinaryDataReader_UNSAFE_Read_2_UInt16_m36661B82546F41BBC9FD3F923F8BAFB486C711BB (void);
// 0x000000E4 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_2_Char(System.Char&)
extern void BinaryDataReader_UNSAFE_Read_2_Char_m0B54BB77A1FE3D2A726FCA81A387659C488351EF (void);
// 0x000000E5 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_4_Int32(System.Int32&)
extern void BinaryDataReader_UNSAFE_Read_4_Int32_mAB8D01448B84864A13CAA127683DA0A4FA6BD43F (void);
// 0x000000E6 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_4_UInt32(System.UInt32&)
extern void BinaryDataReader_UNSAFE_Read_4_UInt32_m3FC70B82A7E2AA55B5F1D3FF04D53AD4D1AB78CA (void);
// 0x000000E7 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_4_Float32(System.Single&)
extern void BinaryDataReader_UNSAFE_Read_4_Float32_m18E4D26C4679AD4F0405073E2AEE0BB08655E327 (void);
// 0x000000E8 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_8_Int64(System.Int64&)
extern void BinaryDataReader_UNSAFE_Read_8_Int64_m975676DF5F8DD88B654DDC6D141FCB9579613589 (void);
// 0x000000E9 System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_8_UInt64(System.UInt64&)
extern void BinaryDataReader_UNSAFE_Read_8_UInt64_m35973ECCD27EFD7A73686C09DD8169B01AAEA639 (void);
// 0x000000EA System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_8_Float64(System.Double&)
extern void BinaryDataReader_UNSAFE_Read_8_Float64_mE2B2AD4E4B2232DC70ED3D9F52A6936BE84C25BF (void);
// 0x000000EB System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_16_Decimal(System.Decimal&)
extern void BinaryDataReader_UNSAFE_Read_16_Decimal_m65C544A7D5C7D00B0ACDBB3E059C28A10255E312 (void);
// 0x000000EC System.Boolean Sirenix.Serialization.BinaryDataReader::UNSAFE_Read_16_Guid(System.Guid&)
extern void BinaryDataReader_UNSAFE_Read_16_Guid_m58B05CA30EAA5DD6BC7AEB8B003753CB02CE09D0 (void);
// 0x000000ED System.Boolean Sirenix.Serialization.BinaryDataReader::HasBufferData(System.Int32)
extern void BinaryDataReader_HasBufferData_m68DA87BB787EB364B3FD91F51F50B0D5A8126690 (void);
// 0x000000EE System.Void Sirenix.Serialization.BinaryDataReader::ReadEntireStreamToBuffer()
extern void BinaryDataReader_ReadEntireStreamToBuffer_m659E0ECFF6498FA11AE2C2BBBBBEB12B13613474 (void);
// 0x000000EF System.Void Sirenix.Serialization.BinaryDataReader::.cctor()
extern void BinaryDataReader__cctor_mD90112E0AF3AFFFDA5B561BB3018888EB1E00411 (void);
// 0x000000F0 System.Void Sirenix.Serialization.BinaryDataReader/<>c::.cctor()
extern void U3CU3Ec__cctor_mC23584B0D0FB4ABA47CC4D2EED92786EC7DC92D1 (void);
// 0x000000F1 System.Void Sirenix.Serialization.BinaryDataReader/<>c::.ctor()
extern void U3CU3Ec__ctor_m3310EEB1994E1D9AABBF3418A7BC8339C839A176 (void);
// 0x000000F2 System.Char Sirenix.Serialization.BinaryDataReader/<>c::<.cctor>b__64_0(System.Byte[],System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__64_0_m03794B435B268991B2EA5541F76BD9334A106194 (void);
// 0x000000F3 System.Byte Sirenix.Serialization.BinaryDataReader/<>c::<.cctor>b__64_1(System.Byte[],System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__64_1_mCE0C81C34DBFE39D0FC2E523F20CC3C97C1E51D3 (void);
// 0x000000F4 System.SByte Sirenix.Serialization.BinaryDataReader/<>c::<.cctor>b__64_2(System.Byte[],System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__64_2_m3035E901D228B2660A434C68E813F397D2B9D41E (void);
// 0x000000F5 System.Boolean Sirenix.Serialization.BinaryDataReader/<>c::<.cctor>b__64_3(System.Byte[],System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__64_3_m04D9427995E281CAEFE2F8B9FCF735273659E0F3 (void);
// 0x000000F6 System.Void Sirenix.Serialization.BinaryDataWriter::.ctor()
extern void BinaryDataWriter__ctor_m0D918FDED9B75527D6500D5EDEC68127C9BA3A51 (void);
// 0x000000F7 System.Void Sirenix.Serialization.BinaryDataWriter::.ctor(System.IO.Stream,Sirenix.Serialization.SerializationContext)
extern void BinaryDataWriter__ctor_m737C390018E3E5B9B6E9787056F4336C15E45D8F (void);
// 0x000000F8 System.Void Sirenix.Serialization.BinaryDataWriter::BeginArrayNode(System.Int64)
extern void BinaryDataWriter_BeginArrayNode_mAF5FF0268104DBD16E2E6C6752C63215B322F2FE (void);
// 0x000000F9 System.Void Sirenix.Serialization.BinaryDataWriter::BeginReferenceNode(System.String,System.Type,System.Int32)
extern void BinaryDataWriter_BeginReferenceNode_m3C601B5A7F8CB0A494BC66CF9B386BD621960D38 (void);
// 0x000000FA System.Void Sirenix.Serialization.BinaryDataWriter::BeginStructNode(System.String,System.Type)
extern void BinaryDataWriter_BeginStructNode_m757F5E00FF8D3D9D5BC8E9B289C74E7B58A79BAF (void);
// 0x000000FB System.Void Sirenix.Serialization.BinaryDataWriter::Dispose()
extern void BinaryDataWriter_Dispose_m9CFBEF57700C58F8848A5B0AE120C002D788991B (void);
// 0x000000FC System.Void Sirenix.Serialization.BinaryDataWriter::EndArrayNode()
extern void BinaryDataWriter_EndArrayNode_m5D6A0D542BD0ECC1853C621DB47A981970813249 (void);
// 0x000000FD System.Void Sirenix.Serialization.BinaryDataWriter::EndNode(System.String)
extern void BinaryDataWriter_EndNode_mD784EFBB0C54CC8551360B85E2488B291B46B48F (void);
// 0x000000FE System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_byte(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_byte_m19F289DBE9944027C6BBEE60D704007CCAE10401 (void);
// 0x000000FF System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_sbyte(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_sbyte_mFD03433E1DFBB176AC088FFB79D6F3ACCBB40FD3 (void);
// 0x00000100 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_bool(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_bool_m13D7BA99370F11BC66C7263CF2DA284A5DB67D69 (void);
// 0x00000101 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_char(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_char_mD7947980505195A13C60CDC137CE6C5D04E3C684 (void);
// 0x00000102 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_short(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_short_m964D571FA8D76B2033ECC0EDA7C54AE421336C56 (void);
// 0x00000103 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_int(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_int_mF99AB82AE5F911F838F50554DFB8263B53E75FFF (void);
// 0x00000104 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_long(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_long_m612F9F5DA61FD84A6B0A4F701ABBDC62C8C57B2C (void);
// 0x00000105 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_ushort(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_ushort_m968A14AAA1A562D21756517A21CA6CCEB0AF2476 (void);
// 0x00000106 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_uint(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_uint_m0280C0AFFD9DEC8BC20F34D40AC69FDFFF2C141D (void);
// 0x00000107 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_ulong(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_ulong_mD319B0781D26C8109FD3664BCDF1CDD8430D8F43 (void);
// 0x00000108 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_decimal(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_decimal_m92BA283AF2C8E29B16A9C31FE8021D57F8FA0BF1 (void);
// 0x00000109 System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_float(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_float_mF23EB6CDE73979D2F3E2CCE094DE6B38F252431A (void);
// 0x0000010A System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_double(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_double_m62FD1CA79DAA55C783B73F19B19B1D940DC26C98 (void);
// 0x0000010B System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray_Guid(Sirenix.Serialization.BinaryDataWriter,System.Object)
extern void BinaryDataWriter_WritePrimitiveArray_Guid_mCE593C8BF2896B8FCC77F140E1629B8793C0AF74 (void);
// 0x0000010C System.Void Sirenix.Serialization.BinaryDataWriter::WritePrimitiveArray(T[])
// 0x0000010D System.Void Sirenix.Serialization.BinaryDataWriter::WriteBoolean(System.String,System.Boolean)
extern void BinaryDataWriter_WriteBoolean_m72DE5862429EBDE23F7E6B97227CA74BBF6001C1 (void);
// 0x0000010E System.Void Sirenix.Serialization.BinaryDataWriter::WriteByte(System.String,System.Byte)
extern void BinaryDataWriter_WriteByte_m62934475FEA8B7088F9237E5DFBA065034C0FD4F (void);
// 0x0000010F System.Void Sirenix.Serialization.BinaryDataWriter::WriteChar(System.String,System.Char)
extern void BinaryDataWriter_WriteChar_mA7361D660282614EDBDB2272FDBD1FE5DF6DC0F9 (void);
// 0x00000110 System.Void Sirenix.Serialization.BinaryDataWriter::WriteDecimal(System.String,System.Decimal)
extern void BinaryDataWriter_WriteDecimal_m792615D5E9084425DD8EA9FA11357E878BB1842F (void);
// 0x00000111 System.Void Sirenix.Serialization.BinaryDataWriter::WriteDouble(System.String,System.Double)
extern void BinaryDataWriter_WriteDouble_mD25F3519EF7C75AD336EB91E7674D56D6F9BE56C (void);
// 0x00000112 System.Void Sirenix.Serialization.BinaryDataWriter::WriteGuid(System.String,System.Guid)
extern void BinaryDataWriter_WriteGuid_m23CBE7612D490FE1EA4EF0734C560F593FA9A4AF (void);
// 0x00000113 System.Void Sirenix.Serialization.BinaryDataWriter::WriteExternalReference(System.String,System.Guid)
extern void BinaryDataWriter_WriteExternalReference_m82ED5C5C4D26F662C57BFE840FF03B5719CC6CA5 (void);
// 0x00000114 System.Void Sirenix.Serialization.BinaryDataWriter::WriteExternalReference(System.String,System.Int32)
extern void BinaryDataWriter_WriteExternalReference_m35BAB0BFF468EC32A758E6BA20E5DEAFDF7F832B (void);
// 0x00000115 System.Void Sirenix.Serialization.BinaryDataWriter::WriteExternalReference(System.String,System.String)
extern void BinaryDataWriter_WriteExternalReference_mD081F90EA74F0DC891B218C27AD6C0272F88BEF3 (void);
// 0x00000116 System.Void Sirenix.Serialization.BinaryDataWriter::WriteInt32(System.String,System.Int32)
extern void BinaryDataWriter_WriteInt32_mA2E73CB755191705A27E6AFC622B49F3B30C505D (void);
// 0x00000117 System.Void Sirenix.Serialization.BinaryDataWriter::WriteInt64(System.String,System.Int64)
extern void BinaryDataWriter_WriteInt64_m8BF748DC039F5C1C71A7323A09DC8C2C3F8BC53A (void);
// 0x00000118 System.Void Sirenix.Serialization.BinaryDataWriter::WriteNull(System.String)
extern void BinaryDataWriter_WriteNull_mC6DC08FFA6792D1F6644855CB9EC58D75D55BC2F (void);
// 0x00000119 System.Void Sirenix.Serialization.BinaryDataWriter::WriteInternalReference(System.String,System.Int32)
extern void BinaryDataWriter_WriteInternalReference_mB0FBC9DBCF400728252E33C7F9903877F9617227 (void);
// 0x0000011A System.Void Sirenix.Serialization.BinaryDataWriter::WriteSByte(System.String,System.SByte)
extern void BinaryDataWriter_WriteSByte_m71E36C7CA56BC9D36CB426798997A1F3284083F2 (void);
// 0x0000011B System.Void Sirenix.Serialization.BinaryDataWriter::WriteInt16(System.String,System.Int16)
extern void BinaryDataWriter_WriteInt16_m63CC1F32C8F68FFC310EEF222C43CA2A9651A698 (void);
// 0x0000011C System.Void Sirenix.Serialization.BinaryDataWriter::WriteSingle(System.String,System.Single)
extern void BinaryDataWriter_WriteSingle_mE7FD19AE455C0C5DEABA9B2C461006C0268E6676 (void);
// 0x0000011D System.Void Sirenix.Serialization.BinaryDataWriter::WriteString(System.String,System.String)
extern void BinaryDataWriter_WriteString_mDDE36BC61EA507A8530A3D75AECBF214C6357AB8 (void);
// 0x0000011E System.Void Sirenix.Serialization.BinaryDataWriter::WriteUInt32(System.String,System.UInt32)
extern void BinaryDataWriter_WriteUInt32_mE89913083C8B32CE52197FB3E2825F8AD25CC258 (void);
// 0x0000011F System.Void Sirenix.Serialization.BinaryDataWriter::WriteUInt64(System.String,System.UInt64)
extern void BinaryDataWriter_WriteUInt64_m1C2258628A2D94F89C710E0F37C1AF037E9110E7 (void);
// 0x00000120 System.Void Sirenix.Serialization.BinaryDataWriter::WriteUInt16(System.String,System.UInt16)
extern void BinaryDataWriter_WriteUInt16_m471F5A5BB991243188F3BC33BC16090E7B18433D (void);
// 0x00000121 System.Void Sirenix.Serialization.BinaryDataWriter::PrepareNewSerializationSession()
extern void BinaryDataWriter_PrepareNewSerializationSession_m91A2C1DA1CC64D0A4D975C24DDA9BA327002AED9 (void);
// 0x00000122 System.String Sirenix.Serialization.BinaryDataWriter::GetDataDump()
extern void BinaryDataWriter_GetDataDump_mEDB2849B05CF37EDD48AAEF65EC21E6A81DE680D (void);
// 0x00000123 System.Void Sirenix.Serialization.BinaryDataWriter::WriteType(System.Type)
extern void BinaryDataWriter_WriteType_m0FEB9FBA84E83C88797A8B4A47791B8D16AA5A91 (void);
// 0x00000124 System.Void Sirenix.Serialization.BinaryDataWriter::WriteStringFast(System.String)
extern void BinaryDataWriter_WriteStringFast_m30753140E9C0E748398E1BD8D50E3A74192908F8 (void);
// 0x00000125 System.Void Sirenix.Serialization.BinaryDataWriter::FlushToStream()
extern void BinaryDataWriter_FlushToStream_mDBC9FD9D630949FF549A44B3B32F91536ACD5FD2 (void);
// 0x00000126 System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_2_Char(System.Char)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_2_Char_m796361C5552DD7F16C8FD4CF105BD7389F9805B6 (void);
// 0x00000127 System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_2_Int16(System.Int16)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_2_Int16_m68DB329368407F259FFBC69DF2369A70A86DAFD1 (void);
// 0x00000128 System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_2_UInt16(System.UInt16)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_2_UInt16_m4AE7098D2560C06ECC01871E22288CE3500AB8BE (void);
// 0x00000129 System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_4_Int32(System.Int32)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_4_Int32_mBD098D9F48459BCB06D4B08F58B9DCBC4F6C4364 (void);
// 0x0000012A System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_4_UInt32(System.UInt32)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_4_UInt32_mF435F48F9C66419E089D85479E55AD0425488AE4 (void);
// 0x0000012B System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_4_Float32(System.Single)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_4_Float32_mB4098723BE3E6433556523F545F1A6865C439A58 (void);
// 0x0000012C System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_8_Int64(System.Int64)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_8_Int64_mAC5313176EA0BE00EF040FFC773AEEE9B0654B1B (void);
// 0x0000012D System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_8_UInt64(System.UInt64)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_8_UInt64_m37FD9AEAA7624553DADAB198BD44A74BF2669A98 (void);
// 0x0000012E System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_8_Float64(System.Double)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_8_Float64_m426B920250E1A93C94C8E768938C7322A04A002E (void);
// 0x0000012F System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_16_Decimal(System.Decimal)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_16_Decimal_m0A18EE380070D4B634DECD535A988EE163B0DC8C (void);
// 0x00000130 System.Void Sirenix.Serialization.BinaryDataWriter::UNSAFE_WriteToBuffer_16_Guid(System.Guid)
extern void BinaryDataWriter_UNSAFE_WriteToBuffer_16_Guid_m20DF6E596922869DDF8AEF20CE91BAC8B9113BE7 (void);
// 0x00000131 System.Void Sirenix.Serialization.BinaryDataWriter::EnsureBufferSpace(System.Int32)
extern void BinaryDataWriter_EnsureBufferSpace_m6897067A9BB56F0501FAD23740CCC2340D2FE1CD (void);
// 0x00000132 System.Boolean Sirenix.Serialization.BinaryDataWriter::TryEnsureBufferSpace(System.Int32)
extern void BinaryDataWriter_TryEnsureBufferSpace_m7FBDCDC9E2544CD317B26B2094798F76B0CF022E (void);
// 0x00000133 System.Void Sirenix.Serialization.BinaryDataWriter::.cctor()
extern void BinaryDataWriter__cctor_mFB19B146BC3F3FBC8EB7B467DEC6A24B4C1478D5 (void);
// 0x00000134 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::.cctor()
extern void U3CU3Ec__cctor_m139D2B56C8CC8C7BDD564B61BA50B6D2BE08F716 (void);
// 0x00000135 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::.ctor()
extern void U3CU3Ec__ctor_m4E98F875587A4D1CFD873A6CBE0E1FBB7528B992 (void);
// 0x00000136 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::<.cctor>b__70_0(System.Byte[],System.Int32,System.Char)
extern void U3CU3Ec_U3C_cctorU3Eb__70_0_m76566D480A9798A22C138F73F72ADC0C89CE0D27 (void);
// 0x00000137 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::<.cctor>b__70_1(System.Byte[],System.Int32,System.Byte)
extern void U3CU3Ec_U3C_cctorU3Eb__70_1_m514D7B30BE6C40315AE5E12FCDAE5A428EE9C193 (void);
// 0x00000138 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::<.cctor>b__70_2(System.Byte[],System.Int32,System.SByte)
extern void U3CU3Ec_U3C_cctorU3Eb__70_2_m039A1A54A1217FD30A6E8117ADCF0806552B7BBA (void);
// 0x00000139 System.Void Sirenix.Serialization.BinaryDataWriter/<>c::<.cctor>b__70_3(System.Byte[],System.Int32,System.Boolean)
extern void U3CU3Ec_U3C_cctorU3Eb__70_3_mE736C9227AC4DF0CAF003D8F926FCD48EDF5C257 (void);
// 0x0000013A Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.IDataReader::get_Binder()
// 0x0000013B System.Void Sirenix.Serialization.IDataReader::set_Binder(Sirenix.Serialization.TwoWaySerializationBinder)
// 0x0000013C System.IO.Stream Sirenix.Serialization.IDataReader::get_Stream()
// 0x0000013D System.Void Sirenix.Serialization.IDataReader::set_Stream(System.IO.Stream)
// 0x0000013E System.Boolean Sirenix.Serialization.IDataReader::get_IsInArrayNode()
// 0x0000013F System.String Sirenix.Serialization.IDataReader::get_CurrentNodeName()
// 0x00000140 System.Int32 Sirenix.Serialization.IDataReader::get_CurrentNodeId()
// 0x00000141 System.Int32 Sirenix.Serialization.IDataReader::get_CurrentNodeDepth()
// 0x00000142 Sirenix.Serialization.DeserializationContext Sirenix.Serialization.IDataReader::get_Context()
// 0x00000143 System.Void Sirenix.Serialization.IDataReader::set_Context(Sirenix.Serialization.DeserializationContext)
// 0x00000144 System.String Sirenix.Serialization.IDataReader::GetDataDump()
// 0x00000145 System.Boolean Sirenix.Serialization.IDataReader::EnterNode(System.Type&)
// 0x00000146 System.Boolean Sirenix.Serialization.IDataReader::ExitNode()
// 0x00000147 System.Boolean Sirenix.Serialization.IDataReader::EnterArray(System.Int64&)
// 0x00000148 System.Boolean Sirenix.Serialization.IDataReader::ExitArray()
// 0x00000149 System.Boolean Sirenix.Serialization.IDataReader::ReadPrimitiveArray(T[]&)
// 0x0000014A Sirenix.Serialization.EntryType Sirenix.Serialization.IDataReader::PeekEntry(System.String&)
// 0x0000014B System.Boolean Sirenix.Serialization.IDataReader::ReadInternalReference(System.Int32&)
// 0x0000014C System.Boolean Sirenix.Serialization.IDataReader::ReadExternalReference(System.Int32&)
// 0x0000014D System.Boolean Sirenix.Serialization.IDataReader::ReadExternalReference(System.Guid&)
// 0x0000014E System.Boolean Sirenix.Serialization.IDataReader::ReadExternalReference(System.String&)
// 0x0000014F System.Boolean Sirenix.Serialization.IDataReader::ReadChar(System.Char&)
// 0x00000150 System.Boolean Sirenix.Serialization.IDataReader::ReadString(System.String&)
// 0x00000151 System.Boolean Sirenix.Serialization.IDataReader::ReadGuid(System.Guid&)
// 0x00000152 System.Boolean Sirenix.Serialization.IDataReader::ReadSByte(System.SByte&)
// 0x00000153 System.Boolean Sirenix.Serialization.IDataReader::ReadInt16(System.Int16&)
// 0x00000154 System.Boolean Sirenix.Serialization.IDataReader::ReadInt32(System.Int32&)
// 0x00000155 System.Boolean Sirenix.Serialization.IDataReader::ReadInt64(System.Int64&)
// 0x00000156 System.Boolean Sirenix.Serialization.IDataReader::ReadByte(System.Byte&)
// 0x00000157 System.Boolean Sirenix.Serialization.IDataReader::ReadUInt16(System.UInt16&)
// 0x00000158 System.Boolean Sirenix.Serialization.IDataReader::ReadUInt32(System.UInt32&)
// 0x00000159 System.Boolean Sirenix.Serialization.IDataReader::ReadUInt64(System.UInt64&)
// 0x0000015A System.Boolean Sirenix.Serialization.IDataReader::ReadDecimal(System.Decimal&)
// 0x0000015B System.Boolean Sirenix.Serialization.IDataReader::ReadSingle(System.Single&)
// 0x0000015C System.Boolean Sirenix.Serialization.IDataReader::ReadDouble(System.Double&)
// 0x0000015D System.Boolean Sirenix.Serialization.IDataReader::ReadBoolean(System.Boolean&)
// 0x0000015E System.Boolean Sirenix.Serialization.IDataReader::ReadNull()
// 0x0000015F System.Void Sirenix.Serialization.IDataReader::SkipEntry()
// 0x00000160 System.Void Sirenix.Serialization.IDataReader::PrepareNewSerializationSession()
// 0x00000161 Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.IDataWriter::get_Binder()
// 0x00000162 System.Void Sirenix.Serialization.IDataWriter::set_Binder(Sirenix.Serialization.TwoWaySerializationBinder)
// 0x00000163 System.IO.Stream Sirenix.Serialization.IDataWriter::get_Stream()
// 0x00000164 System.Void Sirenix.Serialization.IDataWriter::set_Stream(System.IO.Stream)
// 0x00000165 System.Boolean Sirenix.Serialization.IDataWriter::get_IsInArrayNode()
// 0x00000166 Sirenix.Serialization.SerializationContext Sirenix.Serialization.IDataWriter::get_Context()
// 0x00000167 System.Void Sirenix.Serialization.IDataWriter::set_Context(Sirenix.Serialization.SerializationContext)
// 0x00000168 System.String Sirenix.Serialization.IDataWriter::GetDataDump()
// 0x00000169 System.Void Sirenix.Serialization.IDataWriter::FlushToStream()
// 0x0000016A System.Void Sirenix.Serialization.IDataWriter::BeginReferenceNode(System.String,System.Type,System.Int32)
// 0x0000016B System.Void Sirenix.Serialization.IDataWriter::BeginStructNode(System.String,System.Type)
// 0x0000016C System.Void Sirenix.Serialization.IDataWriter::EndNode(System.String)
// 0x0000016D System.Void Sirenix.Serialization.IDataWriter::BeginArrayNode(System.Int64)
// 0x0000016E System.Void Sirenix.Serialization.IDataWriter::EndArrayNode()
// 0x0000016F System.Void Sirenix.Serialization.IDataWriter::WritePrimitiveArray(T[])
// 0x00000170 System.Void Sirenix.Serialization.IDataWriter::WriteNull(System.String)
// 0x00000171 System.Void Sirenix.Serialization.IDataWriter::WriteInternalReference(System.String,System.Int32)
// 0x00000172 System.Void Sirenix.Serialization.IDataWriter::WriteExternalReference(System.String,System.Int32)
// 0x00000173 System.Void Sirenix.Serialization.IDataWriter::WriteExternalReference(System.String,System.Guid)
// 0x00000174 System.Void Sirenix.Serialization.IDataWriter::WriteExternalReference(System.String,System.String)
// 0x00000175 System.Void Sirenix.Serialization.IDataWriter::WriteChar(System.String,System.Char)
// 0x00000176 System.Void Sirenix.Serialization.IDataWriter::WriteString(System.String,System.String)
// 0x00000177 System.Void Sirenix.Serialization.IDataWriter::WriteGuid(System.String,System.Guid)
// 0x00000178 System.Void Sirenix.Serialization.IDataWriter::WriteSByte(System.String,System.SByte)
// 0x00000179 System.Void Sirenix.Serialization.IDataWriter::WriteInt16(System.String,System.Int16)
// 0x0000017A System.Void Sirenix.Serialization.IDataWriter::WriteInt32(System.String,System.Int32)
// 0x0000017B System.Void Sirenix.Serialization.IDataWriter::WriteInt64(System.String,System.Int64)
// 0x0000017C System.Void Sirenix.Serialization.IDataWriter::WriteByte(System.String,System.Byte)
// 0x0000017D System.Void Sirenix.Serialization.IDataWriter::WriteUInt16(System.String,System.UInt16)
// 0x0000017E System.Void Sirenix.Serialization.IDataWriter::WriteUInt32(System.String,System.UInt32)
// 0x0000017F System.Void Sirenix.Serialization.IDataWriter::WriteUInt64(System.String,System.UInt64)
// 0x00000180 System.Void Sirenix.Serialization.IDataWriter::WriteDecimal(System.String,System.Decimal)
// 0x00000181 System.Void Sirenix.Serialization.IDataWriter::WriteSingle(System.String,System.Single)
// 0x00000182 System.Void Sirenix.Serialization.IDataWriter::WriteDouble(System.String,System.Double)
// 0x00000183 System.Void Sirenix.Serialization.IDataWriter::WriteBoolean(System.String,System.Boolean)
// 0x00000184 System.Void Sirenix.Serialization.IDataWriter::PrepareNewSerializationSession()
// 0x00000185 System.Void Sirenix.Serialization.JsonDataReader::.ctor()
extern void JsonDataReader__ctor_mDF271315EE4ABBA622C1C0E84FD1BE6FED51A9A7 (void);
// 0x00000186 System.Void Sirenix.Serialization.JsonDataReader::.ctor(System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void JsonDataReader__ctor_m91D72BBB37E0AB41E760C2AD88D5EF0ACB75550D (void);
// 0x00000187 System.IO.Stream Sirenix.Serialization.JsonDataReader::get_Stream()
extern void JsonDataReader_get_Stream_mF71DEB758403110A2E8B9AEA85B5B4D4DD4F40A3 (void);
// 0x00000188 System.Void Sirenix.Serialization.JsonDataReader::set_Stream(System.IO.Stream)
extern void JsonDataReader_set_Stream_m828F0713E040E39BC146EEC04A407920D1070DA6 (void);
// 0x00000189 System.Void Sirenix.Serialization.JsonDataReader::Dispose()
extern void JsonDataReader_Dispose_m8A6B163CFE2246A35BD4BAC63831FCF20D44638B (void);
// 0x0000018A Sirenix.Serialization.EntryType Sirenix.Serialization.JsonDataReader::PeekEntry(System.String&)
extern void JsonDataReader_PeekEntry_m262346079D8FFB9CB842E6B97DBECCEA8469A8AE (void);
// 0x0000018B System.Boolean Sirenix.Serialization.JsonDataReader::EnterNode(System.Type&)
extern void JsonDataReader_EnterNode_mE8C8C7651AFF98C83F6512E769EB9ADD1D727C76 (void);
// 0x0000018C System.Boolean Sirenix.Serialization.JsonDataReader::ExitNode()
extern void JsonDataReader_ExitNode_m7E5823D7638D9EA470B1AD0CD3A99EBD9D4F7B4D (void);
// 0x0000018D System.Boolean Sirenix.Serialization.JsonDataReader::EnterArray(System.Int64&)
extern void JsonDataReader_EnterArray_m770CF1B56B21C066D6D5DEB08733715A1FA11CBC (void);
// 0x0000018E System.Boolean Sirenix.Serialization.JsonDataReader::ExitArray()
extern void JsonDataReader_ExitArray_mC4310417340450801D726ED89E1F364846616844 (void);
// 0x0000018F System.Boolean Sirenix.Serialization.JsonDataReader::ReadPrimitiveArray(T[]&)
// 0x00000190 System.Boolean Sirenix.Serialization.JsonDataReader::ReadBoolean(System.Boolean&)
extern void JsonDataReader_ReadBoolean_m0CE136A4B8904C2C8361CB69A6C52A7090BD5934 (void);
// 0x00000191 System.Boolean Sirenix.Serialization.JsonDataReader::ReadInternalReference(System.Int32&)
extern void JsonDataReader_ReadInternalReference_m501B32CC37D54A367B232EED7EFAB5D4507C9E5C (void);
// 0x00000192 System.Boolean Sirenix.Serialization.JsonDataReader::ReadExternalReference(System.Int32&)
extern void JsonDataReader_ReadExternalReference_m01F1E7DD28A3BF7D4B269DBE781A50F1074B2B30 (void);
// 0x00000193 System.Boolean Sirenix.Serialization.JsonDataReader::ReadExternalReference(System.Guid&)
extern void JsonDataReader_ReadExternalReference_m2807AA3E7D0F82C570B247CDC7D11E67C423EC0E (void);
// 0x00000194 System.Boolean Sirenix.Serialization.JsonDataReader::ReadExternalReference(System.String&)
extern void JsonDataReader_ReadExternalReference_m7C0B95610CA097020A4AD8966D6FE29F016ABD15 (void);
// 0x00000195 System.Boolean Sirenix.Serialization.JsonDataReader::ReadChar(System.Char&)
extern void JsonDataReader_ReadChar_mEB620061C2EF28EC502CF5DCF2FD8B491A76F14C (void);
// 0x00000196 System.Boolean Sirenix.Serialization.JsonDataReader::ReadString(System.String&)
extern void JsonDataReader_ReadString_mB9181C23108596F438552081E1F7A955AAC19689 (void);
// 0x00000197 System.Boolean Sirenix.Serialization.JsonDataReader::ReadGuid(System.Guid&)
extern void JsonDataReader_ReadGuid_m11C63A3BD5A66B63E17E0359C808DF96881B5213 (void);
// 0x00000198 System.Boolean Sirenix.Serialization.JsonDataReader::ReadSByte(System.SByte&)
extern void JsonDataReader_ReadSByte_mBACB809E2CDDA960177C7AAF68ECB8A89FF60ABC (void);
// 0x00000199 System.Boolean Sirenix.Serialization.JsonDataReader::ReadInt16(System.Int16&)
extern void JsonDataReader_ReadInt16_m405A178FF91E518AFB6A3852BA6B7ED61B92524B (void);
// 0x0000019A System.Boolean Sirenix.Serialization.JsonDataReader::ReadInt32(System.Int32&)
extern void JsonDataReader_ReadInt32_m37A38FBD86446C855C6B3548B6211194E73D80E9 (void);
// 0x0000019B System.Boolean Sirenix.Serialization.JsonDataReader::ReadInt64(System.Int64&)
extern void JsonDataReader_ReadInt64_m09090F23226E8F1663315E855B76395A1621C0DF (void);
// 0x0000019C System.Boolean Sirenix.Serialization.JsonDataReader::ReadByte(System.Byte&)
extern void JsonDataReader_ReadByte_mB22F366AAF3C437A87B3C3F550647FCC7AC3D9D4 (void);
// 0x0000019D System.Boolean Sirenix.Serialization.JsonDataReader::ReadUInt16(System.UInt16&)
extern void JsonDataReader_ReadUInt16_m8C48104D77B02C32AE00477FAE432FFE625637AC (void);
// 0x0000019E System.Boolean Sirenix.Serialization.JsonDataReader::ReadUInt32(System.UInt32&)
extern void JsonDataReader_ReadUInt32_m9A95E6BCAC54D5CEDBD9A7A8E9E7B51BAA47EB60 (void);
// 0x0000019F System.Boolean Sirenix.Serialization.JsonDataReader::ReadUInt64(System.UInt64&)
extern void JsonDataReader_ReadUInt64_mABE9B0300C4042D7115E02D43CCDD269B1130CFD (void);
// 0x000001A0 System.Boolean Sirenix.Serialization.JsonDataReader::ReadDecimal(System.Decimal&)
extern void JsonDataReader_ReadDecimal_m38898323FFB2AC56969367E51A83CBC9C397CDF5 (void);
// 0x000001A1 System.Boolean Sirenix.Serialization.JsonDataReader::ReadSingle(System.Single&)
extern void JsonDataReader_ReadSingle_mE27F3457DCAEA9C63578390DB46E7C36EC09024A (void);
// 0x000001A2 System.Boolean Sirenix.Serialization.JsonDataReader::ReadDouble(System.Double&)
extern void JsonDataReader_ReadDouble_m5794C351005C9039C4BFD9BA04DEB04CF0608C60 (void);
// 0x000001A3 System.Boolean Sirenix.Serialization.JsonDataReader::ReadNull()
extern void JsonDataReader_ReadNull_m979A946AC984A6CB18C50A2575B0A861466FDCD1 (void);
// 0x000001A4 System.Void Sirenix.Serialization.JsonDataReader::PrepareNewSerializationSession()
extern void JsonDataReader_PrepareNewSerializationSession_m718FF1C6C4A8CEB92C2B4CD910100F34D23BD2A5 (void);
// 0x000001A5 System.String Sirenix.Serialization.JsonDataReader::GetDataDump()
extern void JsonDataReader_GetDataDump_m698C08C1404300BB68453D02D7D31FEB20040689 (void);
// 0x000001A6 Sirenix.Serialization.EntryType Sirenix.Serialization.JsonDataReader::PeekEntry()
extern void JsonDataReader_PeekEntry_mBF55454CA16E99107C796AB96429B3B252B12C68 (void);
// 0x000001A7 Sirenix.Serialization.EntryType Sirenix.Serialization.JsonDataReader::ReadToNextEntry()
extern void JsonDataReader_ReadToNextEntry_mCE352ECD9B16F9A00D9C1BDA0A4E8706BB0C8BE0 (void);
// 0x000001A8 System.Void Sirenix.Serialization.JsonDataReader::MarkEntryConsumed()
extern void JsonDataReader_MarkEntryConsumed_mD39A12986A07DF92F30B3742D2D192F1D23669C4 (void);
// 0x000001A9 System.Boolean Sirenix.Serialization.JsonDataReader::ReadAnyIntReference(System.Int32&)
extern void JsonDataReader_ReadAnyIntReference_m95D232FC6DF63482E14623734BBC4007D8B93789 (void);
// 0x000001AA System.Char Sirenix.Serialization.JsonDataReader::<.ctor>b__7_0()
extern void JsonDataReader_U3C_ctorU3Eb__7_0_m218588B3EFD4554548306964F3A8BAE0B4581959 (void);
// 0x000001AB System.SByte Sirenix.Serialization.JsonDataReader::<.ctor>b__7_1()
extern void JsonDataReader_U3C_ctorU3Eb__7_1_m188443B13E34FC27EB89104A2160A8DA73F3C7D1 (void);
// 0x000001AC System.Int16 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_2()
extern void JsonDataReader_U3C_ctorU3Eb__7_2_mDF9455C87D245744AC3701F7E431943B46666CFE (void);
// 0x000001AD System.Int32 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_3()
extern void JsonDataReader_U3C_ctorU3Eb__7_3_m5139CDCD9B6BD2E4B70D701599CF9E4AA026B118 (void);
// 0x000001AE System.Int64 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_4()
extern void JsonDataReader_U3C_ctorU3Eb__7_4_m88AB03D52301DFDC42A0F43FA349DBFB026FBAEE (void);
// 0x000001AF System.Byte Sirenix.Serialization.JsonDataReader::<.ctor>b__7_5()
extern void JsonDataReader_U3C_ctorU3Eb__7_5_mC91C29DE50FDBF6833681748D115ACC7E9EBD724 (void);
// 0x000001B0 System.UInt16 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_6()
extern void JsonDataReader_U3C_ctorU3Eb__7_6_m902BCE00EF10AD6AA0D9AF52D1F30DC5B3B07174 (void);
// 0x000001B1 System.UInt32 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_7()
extern void JsonDataReader_U3C_ctorU3Eb__7_7_m3D0D542B17868052801875B90C72346360C7A056 (void);
// 0x000001B2 System.UInt64 Sirenix.Serialization.JsonDataReader::<.ctor>b__7_8()
extern void JsonDataReader_U3C_ctorU3Eb__7_8_m66FCAFA00AF0501214FD38C9D3477C79A2CEBF64 (void);
// 0x000001B3 System.Decimal Sirenix.Serialization.JsonDataReader::<.ctor>b__7_9()
extern void JsonDataReader_U3C_ctorU3Eb__7_9_mCD6B498A61367D2140B8E31B2980C1BB56BB00C3 (void);
// 0x000001B4 System.Boolean Sirenix.Serialization.JsonDataReader::<.ctor>b__7_10()
extern void JsonDataReader_U3C_ctorU3Eb__7_10_m6702C8E963908EC4C515470123BF42719717A6F4 (void);
// 0x000001B5 System.Single Sirenix.Serialization.JsonDataReader::<.ctor>b__7_11()
extern void JsonDataReader_U3C_ctorU3Eb__7_11_m8E7D85C98DA1230A22B9685469F6EE0C3C786FAD (void);
// 0x000001B6 System.Double Sirenix.Serialization.JsonDataReader::<.ctor>b__7_12()
extern void JsonDataReader_U3C_ctorU3Eb__7_12_mD4275FB977E0D09EF7C74389E0BC8FD36203E15F (void);
// 0x000001B7 System.Guid Sirenix.Serialization.JsonDataReader::<.ctor>b__7_13()
extern void JsonDataReader_U3C_ctorU3Eb__7_13_m0C2CE08229502CB233D3531D23A8D5155AA2532A (void);
// 0x000001B8 System.Void Sirenix.Serialization.JsonDataWriter::.ctor()
extern void JsonDataWriter__ctor_mDA5B358207B9C0A0F436BF518363DEECBA25B4C1 (void);
// 0x000001B9 System.Void Sirenix.Serialization.JsonDataWriter::.ctor(System.IO.Stream,Sirenix.Serialization.SerializationContext,System.Boolean)
extern void JsonDataWriter__ctor_m0F74B5244CDA8B8E222F65565BAE2F5636BCBD1E (void);
// 0x000001BA System.Void Sirenix.Serialization.JsonDataWriter::MarkJustStarted()
extern void JsonDataWriter_MarkJustStarted_mBFA8FD42E3556BE58E53DDDCA358EE93ACD7ABBC (void);
// 0x000001BB System.Void Sirenix.Serialization.JsonDataWriter::FlushToStream()
extern void JsonDataWriter_FlushToStream_m8D68F10EDCB9CA1CEAA36AF56F467C74B13C20E6 (void);
// 0x000001BC System.Void Sirenix.Serialization.JsonDataWriter::BeginReferenceNode(System.String,System.Type,System.Int32)
extern void JsonDataWriter_BeginReferenceNode_m711FCFC333C82CC53EC938A8E514140576C40243 (void);
// 0x000001BD System.Void Sirenix.Serialization.JsonDataWriter::BeginStructNode(System.String,System.Type)
extern void JsonDataWriter_BeginStructNode_mFE7FC76AA8DFA9634B7A12AC6B02CE1F199BF842 (void);
// 0x000001BE System.Void Sirenix.Serialization.JsonDataWriter::EndNode(System.String)
extern void JsonDataWriter_EndNode_m630BCAA44383DFB11040B126DEB8C7AFC1785CA6 (void);
// 0x000001BF System.Void Sirenix.Serialization.JsonDataWriter::BeginArrayNode(System.Int64)
extern void JsonDataWriter_BeginArrayNode_m9B7348518E6FA8E461B52C122AA51A5B822840CD (void);
// 0x000001C0 System.Void Sirenix.Serialization.JsonDataWriter::EndArrayNode()
extern void JsonDataWriter_EndArrayNode_m85D36A3E34025FB032FA3CF86A55359C61CF628F (void);
// 0x000001C1 System.Void Sirenix.Serialization.JsonDataWriter::WritePrimitiveArray(T[])
// 0x000001C2 System.Void Sirenix.Serialization.JsonDataWriter::WriteBoolean(System.String,System.Boolean)
extern void JsonDataWriter_WriteBoolean_mB998761FF11336A0FFE49E69BC766A8CA3034358 (void);
// 0x000001C3 System.Void Sirenix.Serialization.JsonDataWriter::WriteByte(System.String,System.Byte)
extern void JsonDataWriter_WriteByte_mDE05D293893B9317249560DCB562AD2DF75276AE (void);
// 0x000001C4 System.Void Sirenix.Serialization.JsonDataWriter::WriteChar(System.String,System.Char)
extern void JsonDataWriter_WriteChar_m6B0FEE74A2D0BBA6BE12A13591CC112907286D8B (void);
// 0x000001C5 System.Void Sirenix.Serialization.JsonDataWriter::WriteDecimal(System.String,System.Decimal)
extern void JsonDataWriter_WriteDecimal_m3C149822DAF8BAC12DB6298142BD37F1B7465D10 (void);
// 0x000001C6 System.Void Sirenix.Serialization.JsonDataWriter::WriteDouble(System.String,System.Double)
extern void JsonDataWriter_WriteDouble_m662AB0AAE7BEB1A2EE3EAA85FF26825F7C94CC46 (void);
// 0x000001C7 System.Void Sirenix.Serialization.JsonDataWriter::WriteInt32(System.String,System.Int32)
extern void JsonDataWriter_WriteInt32_m00AB4C24BD36896839BDA9AB2EA504354BC59995 (void);
// 0x000001C8 System.Void Sirenix.Serialization.JsonDataWriter::WriteInt64(System.String,System.Int64)
extern void JsonDataWriter_WriteInt64_m2C207D18185B7E5813A7EDE7675F25A6E394EB5C (void);
// 0x000001C9 System.Void Sirenix.Serialization.JsonDataWriter::WriteNull(System.String)
extern void JsonDataWriter_WriteNull_mD840D5C9F20BC206669E11651CA519A24CD06D46 (void);
// 0x000001CA System.Void Sirenix.Serialization.JsonDataWriter::WriteInternalReference(System.String,System.Int32)
extern void JsonDataWriter_WriteInternalReference_mFBDCC3E38AFDD7E31127BBFADDD3383049D07297 (void);
// 0x000001CB System.Void Sirenix.Serialization.JsonDataWriter::WriteSByte(System.String,System.SByte)
extern void JsonDataWriter_WriteSByte_m57754B12D58666DA2653F07D177D71B1B71D151E (void);
// 0x000001CC System.Void Sirenix.Serialization.JsonDataWriter::WriteInt16(System.String,System.Int16)
extern void JsonDataWriter_WriteInt16_m9181E7EAFEA72B3276ED1797241874C0025A64DF (void);
// 0x000001CD System.Void Sirenix.Serialization.JsonDataWriter::WriteSingle(System.String,System.Single)
extern void JsonDataWriter_WriteSingle_mF79F3DF8315622B38F894D30B69DD7A44CDC9D5C (void);
// 0x000001CE System.Void Sirenix.Serialization.JsonDataWriter::WriteString(System.String,System.String)
extern void JsonDataWriter_WriteString_m20451A9F707A47C3D175994E3AA713CBB15D6B71 (void);
// 0x000001CF System.Void Sirenix.Serialization.JsonDataWriter::WriteGuid(System.String,System.Guid)
extern void JsonDataWriter_WriteGuid_mFA22883317B946CFDABE2909E7A6342278E43BD3 (void);
// 0x000001D0 System.Void Sirenix.Serialization.JsonDataWriter::WriteUInt32(System.String,System.UInt32)
extern void JsonDataWriter_WriteUInt32_m64EC2E078D316B1417A16CFD81117F5C7ADE594E (void);
// 0x000001D1 System.Void Sirenix.Serialization.JsonDataWriter::WriteUInt64(System.String,System.UInt64)
extern void JsonDataWriter_WriteUInt64_m4AB6D67D84707AC2C0F5E8FAFBD7807CC9B06067 (void);
// 0x000001D2 System.Void Sirenix.Serialization.JsonDataWriter::WriteExternalReference(System.String,System.Int32)
extern void JsonDataWriter_WriteExternalReference_mABF868BCB5FF6FD3E8B5FD332DD5AA1CC5C3A115 (void);
// 0x000001D3 System.Void Sirenix.Serialization.JsonDataWriter::WriteExternalReference(System.String,System.Guid)
extern void JsonDataWriter_WriteExternalReference_mDB4D37028750ABEBCEA5C3E1138DB62FD128B2F9 (void);
// 0x000001D4 System.Void Sirenix.Serialization.JsonDataWriter::WriteExternalReference(System.String,System.String)
extern void JsonDataWriter_WriteExternalReference_mEA20E95C82C1229691D01598A3DC7296578F91E3 (void);
// 0x000001D5 System.Void Sirenix.Serialization.JsonDataWriter::WriteUInt16(System.String,System.UInt16)
extern void JsonDataWriter_WriteUInt16_m7D528CFFBC029870858712CC70A505C6FAF797EE (void);
// 0x000001D6 System.Void Sirenix.Serialization.JsonDataWriter::Dispose()
extern void JsonDataWriter_Dispose_mEEE7493717757A1CB911F932673E03AAD17F9D2B (void);
// 0x000001D7 System.Void Sirenix.Serialization.JsonDataWriter::PrepareNewSerializationSession()
extern void JsonDataWriter_PrepareNewSerializationSession_m7A01209268539F2CA98E12A5D266256C9BDED269 (void);
// 0x000001D8 System.String Sirenix.Serialization.JsonDataWriter::GetDataDump()
extern void JsonDataWriter_GetDataDump_mD095242222FC337DC1514E40231A31EEDBFDE71D (void);
// 0x000001D9 System.Void Sirenix.Serialization.JsonDataWriter::WriteEntry(System.String,System.String)
extern void JsonDataWriter_WriteEntry_mD943A7A1D0B5691C1B5AA4D155CAE36DFE26BE4B (void);
// 0x000001DA System.Void Sirenix.Serialization.JsonDataWriter::WriteEntry(System.String,System.String,System.Char)
extern void JsonDataWriter_WriteEntry_m1B692598FB39C2CF103A4CF921448329E42C51C6 (void);
// 0x000001DB System.Void Sirenix.Serialization.JsonDataWriter::WriteTypeEntry(System.Type)
extern void JsonDataWriter_WriteTypeEntry_m8BC24D8F42E50CF75DC8082931E638301B6F871D (void);
// 0x000001DC System.Void Sirenix.Serialization.JsonDataWriter::StartNewLine(System.Boolean)
extern void JsonDataWriter_StartNewLine_mCFA4E25E129AF561F3D5A969D2A5BF1822D18409 (void);
// 0x000001DD System.Void Sirenix.Serialization.JsonDataWriter::EnsureBufferSpace(System.Int32)
extern void JsonDataWriter_EnsureBufferSpace_mE0C10023A845B161319E47781D77E52A971A0980 (void);
// 0x000001DE System.Void Sirenix.Serialization.JsonDataWriter::Buffer_WriteString_WithEscape(System.String)
extern void JsonDataWriter_Buffer_WriteString_WithEscape_m2F66F5C9A9B3E2C62ACFF9ECFEBB966B25B31610 (void);
// 0x000001DF System.UInt32[] Sirenix.Serialization.JsonDataWriter::CreateByteToHexLookup()
extern void JsonDataWriter_CreateByteToHexLookup_m51DCC5506A1A3176C0826BE930CCB2A2E7884910 (void);
// 0x000001E0 System.Void Sirenix.Serialization.JsonDataWriter::.cctor()
extern void JsonDataWriter__cctor_m8BDA0DF07378AB25AE6DA611EB26DAFF90FFE19B (void);
// 0x000001E1 Sirenix.Serialization.DeserializationContext Sirenix.Serialization.JsonTextReader::get_Context()
extern void JsonTextReader_get_Context_m59E7AEA4934FB3560774487C6404A94059C33598 (void);
// 0x000001E2 System.Void Sirenix.Serialization.JsonTextReader::set_Context(Sirenix.Serialization.DeserializationContext)
extern void JsonTextReader_set_Context_m069A333FD72B60850A4E39856CFD571EA1898C96 (void);
// 0x000001E3 System.Void Sirenix.Serialization.JsonTextReader::.ctor(System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void JsonTextReader__ctor_m1843426EC471BABDC14A87D2A297B1032A1B0201 (void);
// 0x000001E4 System.Void Sirenix.Serialization.JsonTextReader::Reset()
extern void JsonTextReader_Reset_m1CECC9FB31DCB8172C06F4594691160890403C07 (void);
// 0x000001E5 System.Void Sirenix.Serialization.JsonTextReader::Dispose()
extern void JsonTextReader_Dispose_mFD4484D16E52E161015F4557834E8AD69BAC53C1 (void);
// 0x000001E6 System.Void Sirenix.Serialization.JsonTextReader::ReadToNextEntry(System.String&,System.String&,Sirenix.Serialization.EntryType&)
extern void JsonTextReader_ReadToNextEntry_mC83678273262F242F967B34A5BEAB85A5A8DF121 (void);
// 0x000001E7 System.Void Sirenix.Serialization.JsonTextReader::ParseEntryFromBuffer(System.String&,System.String&,Sirenix.Serialization.EntryType&,System.Int32,System.Nullable`1<Sirenix.Serialization.EntryType>)
extern void JsonTextReader_ParseEntryFromBuffer_m12C306C5FA28AEAB1DFE07676BE7E430BAE109A0 (void);
// 0x000001E8 System.Boolean Sirenix.Serialization.JsonTextReader::IsHex(System.Char)
extern void JsonTextReader_IsHex_m5A8F12E098C6EAC094633F35D2402A6B8D72C3E2 (void);
// 0x000001E9 System.UInt32 Sirenix.Serialization.JsonTextReader::ParseSingleChar(System.Char,System.UInt32)
extern void JsonTextReader_ParseSingleChar_m793A1115438B6912B1A99D1ACA388C24F190F1F0 (void);
// 0x000001EA System.Char Sirenix.Serialization.JsonTextReader::ParseHexChar(System.Char,System.Char,System.Char,System.Char)
extern void JsonTextReader_ParseHexChar_m696BE0355BE84FD8ECC8B604FD2B383D1DF8A844 (void);
// 0x000001EB System.Char Sirenix.Serialization.JsonTextReader::ReadCharIntoBuffer()
extern void JsonTextReader_ReadCharIntoBuffer_m38763E8F01FA37D618B1521A1A6C9A45FB3FCB9E (void);
// 0x000001EC System.Nullable`1<Sirenix.Serialization.EntryType> Sirenix.Serialization.JsonTextReader::GuessPrimitiveType(System.String)
extern void JsonTextReader_GuessPrimitiveType_m0CA35E5D184DCC285E7F336DECD937E7E2987BEA (void);
// 0x000001ED System.Char Sirenix.Serialization.JsonTextReader::PeekChar()
extern void JsonTextReader_PeekChar_m58BE9FE4DAC82492F3690E7E13836E86AA5319EB (void);
// 0x000001EE System.Void Sirenix.Serialization.JsonTextReader::SkipChar()
extern void JsonTextReader_SkipChar_m0FD8FD46D2EB3580492EF817477ADA61FA75495A (void);
// 0x000001EF System.Char Sirenix.Serialization.JsonTextReader::ConsumeChar()
extern void JsonTextReader_ConsumeChar_m4DFD6C511B30FD201758722E1BDE07F9BE05B11F (void);
// 0x000001F0 System.Void Sirenix.Serialization.JsonTextReader::.cctor()
extern void JsonTextReader__cctor_mDFB109FB347AAD4A6462B3E890ECD773CED3E9FF (void);
// 0x000001F1 System.Void Sirenix.Serialization.SerializationNodeDataReader::.ctor(Sirenix.Serialization.DeserializationContext)
extern void SerializationNodeDataReader__ctor_m6648CA4537E206ED1081F9668B67A0696877B1F3 (void);
// 0x000001F2 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::get_IndexIsValid()
extern void SerializationNodeDataReader_get_IndexIsValid_m8FE7011B21B79506991120A31D20C1DCB60FBFB8 (void);
// 0x000001F3 System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode> Sirenix.Serialization.SerializationNodeDataReader::get_Nodes()
extern void SerializationNodeDataReader_get_Nodes_mC85780EB00D20782B4627FE2153CF7851205840B (void);
// 0x000001F4 System.Void Sirenix.Serialization.SerializationNodeDataReader::set_Nodes(System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode>)
extern void SerializationNodeDataReader_set_Nodes_m7CB44C695A15A439199727BD49E9358DE7AB84EC (void);
// 0x000001F5 System.IO.Stream Sirenix.Serialization.SerializationNodeDataReader::get_Stream()
extern void SerializationNodeDataReader_get_Stream_m1E92D28139914D2A83C7F938E7862711A8907723 (void);
// 0x000001F6 System.Void Sirenix.Serialization.SerializationNodeDataReader::set_Stream(System.IO.Stream)
extern void SerializationNodeDataReader_set_Stream_m1096375A0BB74727F1C557A96FFD3C594DBFF915 (void);
// 0x000001F7 System.Void Sirenix.Serialization.SerializationNodeDataReader::Dispose()
extern void SerializationNodeDataReader_Dispose_m5A11DF980455E89E2CD85C0F124659E75D0EE007 (void);
// 0x000001F8 System.Void Sirenix.Serialization.SerializationNodeDataReader::PrepareNewSerializationSession()
extern void SerializationNodeDataReader_PrepareNewSerializationSession_mF7B0F08B2DF5D08FF771A03DE5E36A28DE94E36F (void);
// 0x000001F9 Sirenix.Serialization.EntryType Sirenix.Serialization.SerializationNodeDataReader::PeekEntry(System.String&)
extern void SerializationNodeDataReader_PeekEntry_m5A9ECC288336BFB24A306EBDC2166CC0F4C2F7DE (void);
// 0x000001FA System.Boolean Sirenix.Serialization.SerializationNodeDataReader::EnterArray(System.Int64&)
extern void SerializationNodeDataReader_EnterArray_m62DD77255A46BD88BF0C31B1EF520485BF38E5EE (void);
// 0x000001FB System.Boolean Sirenix.Serialization.SerializationNodeDataReader::EnterNode(System.Type&)
extern void SerializationNodeDataReader_EnterNode_m7ADEE1810EE729DD8E0C4E20A8E9F260CFD7BA74 (void);
// 0x000001FC System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ExitArray()
extern void SerializationNodeDataReader_ExitArray_m44BD69E047EC2484A6A9814076F9F999927CB0BC (void);
// 0x000001FD System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ExitNode()
extern void SerializationNodeDataReader_ExitNode_m6C17226D73F8FA0A68F9C1E8B63DD21DB6D8F5ED (void);
// 0x000001FE System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadBoolean(System.Boolean&)
extern void SerializationNodeDataReader_ReadBoolean_m12BF38B6B2A60421D8FF5ABC73284AD9FE16CCCB (void);
// 0x000001FF System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadByte(System.Byte&)
extern void SerializationNodeDataReader_ReadByte_m7615C7F6A69EC8F2B8E9426C13FD7460890D2C59 (void);
// 0x00000200 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadChar(System.Char&)
extern void SerializationNodeDataReader_ReadChar_mB7AE9F49190F4FBDCE47F1859E86CC527D2F73C6 (void);
// 0x00000201 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadDecimal(System.Decimal&)
extern void SerializationNodeDataReader_ReadDecimal_mA83E1B2C95A400F6EA8699F9682421522525BA25 (void);
// 0x00000202 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadDouble(System.Double&)
extern void SerializationNodeDataReader_ReadDouble_m04C1670981E0E60985F8F3135972690C334B0982 (void);
// 0x00000203 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadExternalReference(System.Guid&)
extern void SerializationNodeDataReader_ReadExternalReference_m265339449A37FFA6C94D339B9A7B95A92DB7FEC1 (void);
// 0x00000204 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadExternalReference(System.String&)
extern void SerializationNodeDataReader_ReadExternalReference_mDE013938578706F911D2697158CFF346AE4862EA (void);
// 0x00000205 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadExternalReference(System.Int32&)
extern void SerializationNodeDataReader_ReadExternalReference_mDD0BEA6B33E860ED147E77AFE888A5612BC7F340 (void);
// 0x00000206 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadGuid(System.Guid&)
extern void SerializationNodeDataReader_ReadGuid_mA50A94F28279877CD5E8E0435D0CDADA88F3CFED (void);
// 0x00000207 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadInt16(System.Int16&)
extern void SerializationNodeDataReader_ReadInt16_m704974B5C7101FB8ACF3099D6F3A314E958B959D (void);
// 0x00000208 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadInt32(System.Int32&)
extern void SerializationNodeDataReader_ReadInt32_mAB615CBF0F9C03E3EE74B9A5C6862E3A4C88FC4D (void);
// 0x00000209 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadInt64(System.Int64&)
extern void SerializationNodeDataReader_ReadInt64_m48590BA88FC1D15D64AC9FFF1AC2FA4C4B4CDCF5 (void);
// 0x0000020A System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadInternalReference(System.Int32&)
extern void SerializationNodeDataReader_ReadInternalReference_m8CF90FAA481FF9D96ABDBD22071D41E0D4DAE7AA (void);
// 0x0000020B System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadNull()
extern void SerializationNodeDataReader_ReadNull_mC772A3C988401FB15CD678D3A2F956F5513AC184 (void);
// 0x0000020C System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadPrimitiveArray(T[]&)
// 0x0000020D System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadSByte(System.SByte&)
extern void SerializationNodeDataReader_ReadSByte_m133CE0480AF9AC04C2CE42F070474CFB32FD3D22 (void);
// 0x0000020E System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadSingle(System.Single&)
extern void SerializationNodeDataReader_ReadSingle_m7036400EB199C491BED2E5861652A03C55A5C398 (void);
// 0x0000020F System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadString(System.String&)
extern void SerializationNodeDataReader_ReadString_m950EB91C738A0FC547DEDCA7353E977DE50F15C5 (void);
// 0x00000210 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadUInt16(System.UInt16&)
extern void SerializationNodeDataReader_ReadUInt16_mE6768104E2E72E9FB3AC1B60C7ECA34C78987B20 (void);
// 0x00000211 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadUInt32(System.UInt32&)
extern void SerializationNodeDataReader_ReadUInt32_mE71430CCED6068A9529CAA82AE3C3452EDCBFB09 (void);
// 0x00000212 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::ReadUInt64(System.UInt64&)
extern void SerializationNodeDataReader_ReadUInt64_m3173800E9B438642C3D6D00458DA6DF1BEDCA5C0 (void);
// 0x00000213 System.String Sirenix.Serialization.SerializationNodeDataReader::GetDataDump()
extern void SerializationNodeDataReader_GetDataDump_m495B2125BAED41EBEEAD33AD4D8749FEDC4BC8F2 (void);
// 0x00000214 System.Void Sirenix.Serialization.SerializationNodeDataReader::ConsumeCurrentEntry()
extern void SerializationNodeDataReader_ConsumeCurrentEntry_m3E8AD976F7F840229AFDF4FEB2FFBAA71C867C76 (void);
// 0x00000215 Sirenix.Serialization.EntryType Sirenix.Serialization.SerializationNodeDataReader::PeekEntry()
extern void SerializationNodeDataReader_PeekEntry_mC2ECBF6A7579A16E27B44928B40B1AAD6DC6B951 (void);
// 0x00000216 Sirenix.Serialization.EntryType Sirenix.Serialization.SerializationNodeDataReader::ReadToNextEntry()
extern void SerializationNodeDataReader_ReadToNextEntry_m9AC6C12A36B2028BD2148D3B36D4D77B44DA02C6 (void);
// 0x00000217 System.Char Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_0()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_0_m00F71BA12BF0DF57209A1FA5EB4AC74CBF1D7AD0 (void);
// 0x00000218 System.SByte Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_1()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_1_m7E0978EBA0D4C9280D6B13548E06017B2C489EEF (void);
// 0x00000219 System.Int16 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_2()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_2_mEFA85D5013760DB520F6AA531A4BF42EEDA15E53 (void);
// 0x0000021A System.Int32 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_3()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_3_m152BBB040A5EAD6076048DC9489202C785226101 (void);
// 0x0000021B System.Int64 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_4()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_4_m5FA0551D5DEFD265D9A9B57F87A8BE34B521430B (void);
// 0x0000021C System.Byte Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_5()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_5_m07D35A1038F73C248085CC9475E7218080ADA93E (void);
// 0x0000021D System.UInt16 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_6()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_6_mD3F37E0C19AC407882B4D84567A27741C932EED4 (void);
// 0x0000021E System.UInt32 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_7()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_7_m1BCD4F188A91ECC086AE072A5614776ABE1ABAC2 (void);
// 0x0000021F System.UInt64 Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_8()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_8_m99831C0576687FD9A72616944CDC61CE44688C02 (void);
// 0x00000220 System.Decimal Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_9()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_9_mA12EA1F7DAD28B8ACE68647E4968263354396D09 (void);
// 0x00000221 System.Boolean Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_10()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_10_m1F1437E82B74BE9E17270D222A8933249E86825F (void);
// 0x00000222 System.Single Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_11()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_11_m2C8BEAAB1431C1A4BE9E8D96A707D19E8258E1AA (void);
// 0x00000223 System.Double Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_12()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_12_mD0490FAD60A6EF8DD4165446E007688F014464DA (void);
// 0x00000224 System.Guid Sirenix.Serialization.SerializationNodeDataReader::<.ctor>b__6_13()
extern void SerializationNodeDataReader_U3C_ctorU3Eb__6_13_m3F7320E1559494FD26B6AC7265D7FA0C32622ADA (void);
// 0x00000225 System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode> Sirenix.Serialization.SerializationNodeDataWriter::get_Nodes()
extern void SerializationNodeDataWriter_get_Nodes_m7E58FCF62900D73580499C22934B6AB67EA3D788 (void);
// 0x00000226 System.Void Sirenix.Serialization.SerializationNodeDataWriter::set_Nodes(System.Collections.Generic.List`1<Sirenix.Serialization.SerializationNode>)
extern void SerializationNodeDataWriter_set_Nodes_m52B04D21A78DB5B6C0D8977FB876B879422FE395 (void);
// 0x00000227 System.Void Sirenix.Serialization.SerializationNodeDataWriter::.ctor(Sirenix.Serialization.SerializationContext)
extern void SerializationNodeDataWriter__ctor_m944885959C772E5A5EF752FE5B74282B844B53C5 (void);
// 0x00000228 System.IO.Stream Sirenix.Serialization.SerializationNodeDataWriter::get_Stream()
extern void SerializationNodeDataWriter_get_Stream_mBF3169697F18E64086704369BF251DABE2394B0A (void);
// 0x00000229 System.Void Sirenix.Serialization.SerializationNodeDataWriter::set_Stream(System.IO.Stream)
extern void SerializationNodeDataWriter_set_Stream_m1417234035C2FBBF5B7183C82CEA3F5D725D37B8 (void);
// 0x0000022A System.Void Sirenix.Serialization.SerializationNodeDataWriter::BeginArrayNode(System.Int64)
extern void SerializationNodeDataWriter_BeginArrayNode_mED91AC7F284211C36E0C53B30FD00C1646F65238 (void);
// 0x0000022B System.Void Sirenix.Serialization.SerializationNodeDataWriter::BeginReferenceNode(System.String,System.Type,System.Int32)
extern void SerializationNodeDataWriter_BeginReferenceNode_mCE129C4021A7118D1934DDF9511067D4614159CC (void);
// 0x0000022C System.Void Sirenix.Serialization.SerializationNodeDataWriter::BeginStructNode(System.String,System.Type)
extern void SerializationNodeDataWriter_BeginStructNode_mAFE4F9152167A47F7AFEA2D6B569E9081FC48A1F (void);
// 0x0000022D System.Void Sirenix.Serialization.SerializationNodeDataWriter::Dispose()
extern void SerializationNodeDataWriter_Dispose_m1176C864F6D225175542AB7DA2BF663E513059D6 (void);
// 0x0000022E System.Void Sirenix.Serialization.SerializationNodeDataWriter::EndArrayNode()
extern void SerializationNodeDataWriter_EndArrayNode_mEC2688B6C27ECFB542F676F60D2A415FB889A841 (void);
// 0x0000022F System.Void Sirenix.Serialization.SerializationNodeDataWriter::EndNode(System.String)
extern void SerializationNodeDataWriter_EndNode_m951F92C367060183BA4FD26BC752A537033AF8DC (void);
// 0x00000230 System.Void Sirenix.Serialization.SerializationNodeDataWriter::PrepareNewSerializationSession()
extern void SerializationNodeDataWriter_PrepareNewSerializationSession_m846F3AA9AFDD5566C2896C0C6E1D960EF1CA5117 (void);
// 0x00000231 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteBoolean(System.String,System.Boolean)
extern void SerializationNodeDataWriter_WriteBoolean_m6FCC384602536F8789B009D0C373C01F7308B90F (void);
// 0x00000232 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteByte(System.String,System.Byte)
extern void SerializationNodeDataWriter_WriteByte_m134531E41933302B98D06CF7BFB53D209DF53BBF (void);
// 0x00000233 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteChar(System.String,System.Char)
extern void SerializationNodeDataWriter_WriteChar_m9537A34163133D7383221FD86A673C0A281FD999 (void);
// 0x00000234 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteDecimal(System.String,System.Decimal)
extern void SerializationNodeDataWriter_WriteDecimal_m6D6958267CD13C29D7B4CC2D7462363CD195DCBE (void);
// 0x00000235 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteSingle(System.String,System.Single)
extern void SerializationNodeDataWriter_WriteSingle_m16C5D22F9DC4B542B93FEE1CA0C05628762EEA59 (void);
// 0x00000236 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteDouble(System.String,System.Double)
extern void SerializationNodeDataWriter_WriteDouble_mD4B24EB8A284CBBB1767DEDD6968607B18BCE9E5 (void);
// 0x00000237 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteExternalReference(System.String,System.Guid)
extern void SerializationNodeDataWriter_WriteExternalReference_mA74B0405492C80CC187110D0C45CA78E78DF77C6 (void);
// 0x00000238 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteExternalReference(System.String,System.String)
extern void SerializationNodeDataWriter_WriteExternalReference_mD8E954662AD09013154C2798EB7BDB1BB16C8BB5 (void);
// 0x00000239 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteExternalReference(System.String,System.Int32)
extern void SerializationNodeDataWriter_WriteExternalReference_mD9E07F1232C8B53775F7F250F2A8FA2F1E13E4CC (void);
// 0x0000023A System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteGuid(System.String,System.Guid)
extern void SerializationNodeDataWriter_WriteGuid_m215BCE2E7BE7C3661696509F0574297507ADFA83 (void);
// 0x0000023B System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteInt16(System.String,System.Int16)
extern void SerializationNodeDataWriter_WriteInt16_m274C251E02065116D1E2452FEE6E9B3B5D2F55BA (void);
// 0x0000023C System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteInt32(System.String,System.Int32)
extern void SerializationNodeDataWriter_WriteInt32_m2E148DD18DF94B299079C1A3F385776179A7E46D (void);
// 0x0000023D System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteInt64(System.String,System.Int64)
extern void SerializationNodeDataWriter_WriteInt64_mD21E1EB74E6DC549AE1049119EEA5444F9DA56B9 (void);
// 0x0000023E System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteInternalReference(System.String,System.Int32)
extern void SerializationNodeDataWriter_WriteInternalReference_m66204B29A08B0CA26FE097A846A5864A671AD558 (void);
// 0x0000023F System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteNull(System.String)
extern void SerializationNodeDataWriter_WriteNull_m3C95257CE595AADD2416808B20B04AF85B0B9EEF (void);
// 0x00000240 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WritePrimitiveArray(T[])
// 0x00000241 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteSByte(System.String,System.SByte)
extern void SerializationNodeDataWriter_WriteSByte_m79351986B7AD5A5E7C6D148E8EC7B5A78D830139 (void);
// 0x00000242 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteString(System.String,System.String)
extern void SerializationNodeDataWriter_WriteString_m29531F959480E4D63F6353E497E7136DEA23DB0B (void);
// 0x00000243 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteUInt16(System.String,System.UInt16)
extern void SerializationNodeDataWriter_WriteUInt16_m6BAD2C2C5866672AEB444B2AD0AC527255A7BE37 (void);
// 0x00000244 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteUInt32(System.String,System.UInt32)
extern void SerializationNodeDataWriter_WriteUInt32_m2B4E084E8CE1FCF3744500765B85049F2857317E (void);
// 0x00000245 System.Void Sirenix.Serialization.SerializationNodeDataWriter::WriteUInt64(System.String,System.UInt64)
extern void SerializationNodeDataWriter_WriteUInt64_m26D88CAB1FBC70E5F73D6082292AFD5BB6866101 (void);
// 0x00000246 System.Void Sirenix.Serialization.SerializationNodeDataWriter::FlushToStream()
extern void SerializationNodeDataWriter_FlushToStream_m982DF8A7792730C3216B5BCDE97C01B820B3D416 (void);
// 0x00000247 System.String Sirenix.Serialization.SerializationNodeDataWriter::GetDataDump()
extern void SerializationNodeDataWriter_GetDataDump_mC330F6B13F9299F68F00DA055713A960BC4F8EF1 (void);
// 0x00000248 T[] Sirenix.Serialization.ArrayFormatter`1::GetUninitializedObject()
// 0x00000249 System.Void Sirenix.Serialization.ArrayFormatter`1::DeserializeImplementation(T[]&,Sirenix.Serialization.IDataReader)
// 0x0000024A System.Void Sirenix.Serialization.ArrayFormatter`1::SerializeImplementation(T[]&,Sirenix.Serialization.IDataWriter)
// 0x0000024B System.Void Sirenix.Serialization.ArrayFormatter`1::.ctor()
// 0x0000024C System.Void Sirenix.Serialization.ArrayFormatter`1::.cctor()
// 0x0000024D System.Collections.ArrayList Sirenix.Serialization.ArrayListFormatter::GetUninitializedObject()
extern void ArrayListFormatter_GetUninitializedObject_m04190330ADB4CC71AA46AE5D5E7F0F2BDB1EC867 (void);
// 0x0000024E System.Void Sirenix.Serialization.ArrayListFormatter::DeserializeImplementation(System.Collections.ArrayList&,Sirenix.Serialization.IDataReader)
extern void ArrayListFormatter_DeserializeImplementation_m18C20E2589960BB5B006B56802BA6E0DC8483403 (void);
// 0x0000024F System.Void Sirenix.Serialization.ArrayListFormatter::SerializeImplementation(System.Collections.ArrayList&,Sirenix.Serialization.IDataWriter)
extern void ArrayListFormatter_SerializeImplementation_mDF1EDEA81AFE13AA0AE279883EFD18456DBDD405 (void);
// 0x00000250 System.Void Sirenix.Serialization.ArrayListFormatter::.ctor()
extern void ArrayListFormatter__ctor_m1D2C45A4B87D64E95512F617B0F008E228B3CAA7 (void);
// 0x00000251 System.Void Sirenix.Serialization.ArrayListFormatter::.cctor()
extern void ArrayListFormatter__cctor_mEEEA10B0CE4D7DFA42B626B93724423AA095A051 (void);
// 0x00000252 System.Void Sirenix.Serialization.BaseFormatter`1::.cctor()
// 0x00000253 Sirenix.Serialization.BaseFormatter`1/SerializationCallback<T>[] Sirenix.Serialization.BaseFormatter`1::GetCallbacks(System.Reflection.MethodInfo[],System.Type,System.Collections.Generic.List`1<Sirenix.Serialization.BaseFormatter`1/SerializationCallback<T>>&)
// 0x00000254 Sirenix.Serialization.BaseFormatter`1/SerializationCallback<T> Sirenix.Serialization.BaseFormatter`1::CreateCallback(System.Reflection.MethodInfo)
// 0x00000255 System.Type Sirenix.Serialization.BaseFormatter`1::get_SerializedType()
// 0x00000256 System.Void Sirenix.Serialization.BaseFormatter`1::Sirenix.Serialization.IFormatter.Serialize(System.Object,Sirenix.Serialization.IDataWriter)
// 0x00000257 System.Object Sirenix.Serialization.BaseFormatter`1::Sirenix.Serialization.IFormatter.Deserialize(Sirenix.Serialization.IDataReader)
// 0x00000258 T Sirenix.Serialization.BaseFormatter`1::Deserialize(Sirenix.Serialization.IDataReader)
// 0x00000259 System.Void Sirenix.Serialization.BaseFormatter`1::Serialize(T,Sirenix.Serialization.IDataWriter)
// 0x0000025A T Sirenix.Serialization.BaseFormatter`1::GetUninitializedObject()
// 0x0000025B System.Void Sirenix.Serialization.BaseFormatter`1::RegisterReferenceID(T,Sirenix.Serialization.IDataReader)
// 0x0000025C System.Void Sirenix.Serialization.BaseFormatter`1::InvokeOnDeserializingCallbacks(T,Sirenix.Serialization.DeserializationContext)
// 0x0000025D System.Void Sirenix.Serialization.BaseFormatter`1::InvokeOnDeserializingCallbacks(T&,Sirenix.Serialization.DeserializationContext)
// 0x0000025E System.Void Sirenix.Serialization.BaseFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x0000025F System.Void Sirenix.Serialization.BaseFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x00000260 System.Void Sirenix.Serialization.BaseFormatter`1::.ctor()
// 0x00000261 System.Void Sirenix.Serialization.BaseFormatter`1/SerializationCallback::.ctor(System.Object,System.IntPtr)
// 0x00000262 System.Void Sirenix.Serialization.BaseFormatter`1/SerializationCallback::Invoke(T&,System.Runtime.Serialization.StreamingContext)
// 0x00000263 System.IAsyncResult Sirenix.Serialization.BaseFormatter`1/SerializationCallback::BeginInvoke(T&,System.Runtime.Serialization.StreamingContext,System.AsyncCallback,System.Object)
// 0x00000264 System.Void Sirenix.Serialization.BaseFormatter`1/SerializationCallback::EndInvoke(T&,System.IAsyncResult)
// 0x00000265 System.Void Sirenix.Serialization.BaseFormatter`1/<>c__DisplayClass11_0::.ctor()
// 0x00000266 System.Void Sirenix.Serialization.BaseFormatter`1/<>c__DisplayClass11_0::<CreateCallback>b__0(T&,System.Runtime.Serialization.StreamingContext)
// 0x00000267 System.Void Sirenix.Serialization.BaseFormatter`1/<>c__DisplayClass11_0::<CreateCallback>b__1(T&,System.Runtime.Serialization.StreamingContext)
// 0x00000268 System.Void Sirenix.Serialization.DateTimeFormatter::Read(System.DateTime&,Sirenix.Serialization.IDataReader)
extern void DateTimeFormatter_Read_m86CD9D11C1BC478D2FEC9EE09A0FEE3DD01881E9 (void);
// 0x00000269 System.Void Sirenix.Serialization.DateTimeFormatter::Write(System.DateTime&,Sirenix.Serialization.IDataWriter)
extern void DateTimeFormatter_Write_mEAA80920E179D421F5B2ABE19793652D033830BF (void);
// 0x0000026A System.Void Sirenix.Serialization.DateTimeFormatter::.ctor()
extern void DateTimeFormatter__ctor_m7556B4C54F3EA4845A7DC77AE78C061A6A42B866 (void);
// 0x0000026B System.Void Sirenix.Serialization.DateTimeOffsetFormatter::Read(System.DateTimeOffset&,Sirenix.Serialization.IDataReader)
extern void DateTimeOffsetFormatter_Read_mE881633D54D16447247F581FA551922E7D828DAB (void);
// 0x0000026C System.Void Sirenix.Serialization.DateTimeOffsetFormatter::Write(System.DateTimeOffset&,Sirenix.Serialization.IDataWriter)
extern void DateTimeOffsetFormatter_Write_m10CD694BDB50B0A5FDA7B6842FAE5D8B59F85C98 (void);
// 0x0000026D System.Void Sirenix.Serialization.DateTimeOffsetFormatter::.ctor()
extern void DateTimeOffsetFormatter__ctor_mBF5EE9E84B884D8747A5013104994FC258C76FA9 (void);
// 0x0000026E System.Void Sirenix.Serialization.DelegateFormatter`1::.cctor()
// 0x0000026F System.Void Sirenix.Serialization.DelegateFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x00000270 System.Void Sirenix.Serialization.DelegateFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x00000271 T Sirenix.Serialization.DelegateFormatter`1::GetUninitializedObject()
// 0x00000272 System.Void Sirenix.Serialization.DelegateFormatter`1::.ctor()
// 0x00000273 System.Void Sirenix.Serialization.DelegateFormatter`1/<>c::.cctor()
// 0x00000274 System.Void Sirenix.Serialization.DelegateFormatter`1/<>c::.ctor()
// 0x00000275 System.String Sirenix.Serialization.DelegateFormatter`1/<>c::<DeserializeImplementation>b__6_0(System.Type)
// 0x00000276 System.String Sirenix.Serialization.DelegateFormatter`1/<>c::<DeserializeImplementation>b__6_1(System.Type)
// 0x00000277 System.Void Sirenix.Serialization.DerivedDictionaryFormatter`3::.cctor()
// 0x00000278 System.Void Sirenix.Serialization.DerivedDictionaryFormatter`3::.ctor()
// 0x00000279 TDictionary Sirenix.Serialization.DerivedDictionaryFormatter`3::GetUninitializedObject()
// 0x0000027A System.Void Sirenix.Serialization.DerivedDictionaryFormatter`3::DeserializeImplementation(TDictionary&,Sirenix.Serialization.IDataReader)
// 0x0000027B System.Void Sirenix.Serialization.DerivedDictionaryFormatter`3::SerializeImplementation(TDictionary&,Sirenix.Serialization.IDataWriter)
// 0x0000027C System.Void Sirenix.Serialization.DictionaryFormatter`2::.cctor()
// 0x0000027D System.Void Sirenix.Serialization.DictionaryFormatter`2::.ctor()
// 0x0000027E System.Collections.Generic.Dictionary`2<TKey,TValue> Sirenix.Serialization.DictionaryFormatter`2::GetUninitializedObject()
// 0x0000027F System.Void Sirenix.Serialization.DictionaryFormatter`2::DeserializeImplementation(System.Collections.Generic.Dictionary`2<TKey,TValue>&,Sirenix.Serialization.IDataReader)
// 0x00000280 System.Void Sirenix.Serialization.DictionaryFormatter`2::SerializeImplementation(System.Collections.Generic.Dictionary`2<TKey,TValue>&,Sirenix.Serialization.IDataWriter)
// 0x00000281 System.Void Sirenix.Serialization.DoubleLookupDictionaryFormatter`3::.cctor()
// 0x00000282 System.Void Sirenix.Serialization.DoubleLookupDictionaryFormatter`3::.ctor()
// 0x00000283 Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<TPrimary,TSecondary,TValue> Sirenix.Serialization.DoubleLookupDictionaryFormatter`3::GetUninitializedObject()
// 0x00000284 System.Void Sirenix.Serialization.DoubleLookupDictionaryFormatter`3::SerializeImplementation(Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<TPrimary,TSecondary,TValue>&,Sirenix.Serialization.IDataWriter)
// 0x00000285 System.Void Sirenix.Serialization.DoubleLookupDictionaryFormatter`3::DeserializeImplementation(Sirenix.Serialization.Utilities.DoubleLookupDictionary`3<TPrimary,TSecondary,TValue>&,Sirenix.Serialization.IDataReader)
// 0x00000286 System.Void Sirenix.Serialization.EasyBaseFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x00000287 System.Void Sirenix.Serialization.EasyBaseFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x00000288 System.Void Sirenix.Serialization.EasyBaseFormatter`1::ReadDataEntry(T&,System.String,Sirenix.Serialization.EntryType,Sirenix.Serialization.IDataReader)
// 0x00000289 System.Void Sirenix.Serialization.EasyBaseFormatter`1::WriteDataEntries(T&,Sirenix.Serialization.IDataWriter)
// 0x0000028A System.Void Sirenix.Serialization.EasyBaseFormatter`1::.ctor()
// 0x0000028B System.Void Sirenix.Serialization.EmittedFormatterAttribute::.ctor()
extern void EmittedFormatterAttribute__ctor_m606F2E7D04ECA7FB692313F4893305F827FA8455 (void);
// 0x0000028C System.Void Sirenix.Serialization.EmptyTypeFormatter`1::ReadDataEntry(T&,System.String,Sirenix.Serialization.EntryType,Sirenix.Serialization.IDataReader)
// 0x0000028D System.Void Sirenix.Serialization.EmptyTypeFormatter`1::WriteDataEntries(T&,Sirenix.Serialization.IDataWriter)
// 0x0000028E System.Void Sirenix.Serialization.EmptyTypeFormatter`1::.ctor()
// 0x0000028F Sirenix.Serialization.IFormatter Sirenix.Serialization.FormatterEmitter::GetEmittedFormatter(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterEmitter_GetEmittedFormatter_mA3B806BBFFEC9BDC692C593587B6F71987802EE2 (void);
// 0x00000290 System.Void Sirenix.Serialization.FormatterEmitter/AOTEmittedFormatter`1::.ctor()
// 0x00000291 System.Void Sirenix.Serialization.FormatterEmitter/EmptyAOTEmittedFormatter`1::ReadDataEntry(T&,System.String,Sirenix.Serialization.EntryType,Sirenix.Serialization.IDataReader)
// 0x00000292 System.Void Sirenix.Serialization.FormatterEmitter/EmptyAOTEmittedFormatter`1::WriteDataEntries(T&,Sirenix.Serialization.IDataWriter)
// 0x00000293 System.Void Sirenix.Serialization.FormatterEmitter/EmptyAOTEmittedFormatter`1::.ctor()
// 0x00000294 System.Boolean Sirenix.Serialization.GenericCollectionFormatter::CanFormat(System.Type,System.Type&)
extern void GenericCollectionFormatter_CanFormat_mD040D5096BD3A8B9236986D53534C2A30C832930 (void);
// 0x00000295 System.Void Sirenix.Serialization.GenericCollectionFormatter`2::.cctor()
// 0x00000296 System.Void Sirenix.Serialization.GenericCollectionFormatter`2::.ctor()
// 0x00000297 TCollection Sirenix.Serialization.GenericCollectionFormatter`2::GetUninitializedObject()
// 0x00000298 System.Void Sirenix.Serialization.GenericCollectionFormatter`2::DeserializeImplementation(TCollection&,Sirenix.Serialization.IDataReader)
// 0x00000299 System.Void Sirenix.Serialization.GenericCollectionFormatter`2::SerializeImplementation(TCollection&,Sirenix.Serialization.IDataWriter)
// 0x0000029A System.Void Sirenix.Serialization.HashSetFormatter`1::.cctor()
// 0x0000029B System.Void Sirenix.Serialization.HashSetFormatter`1::.ctor()
// 0x0000029C System.Collections.Generic.HashSet`1<T> Sirenix.Serialization.HashSetFormatter`1::GetUninitializedObject()
// 0x0000029D System.Void Sirenix.Serialization.HashSetFormatter`1::DeserializeImplementation(System.Collections.Generic.HashSet`1<T>&,Sirenix.Serialization.IDataReader)
// 0x0000029E System.Void Sirenix.Serialization.HashSetFormatter`1::SerializeImplementation(System.Collections.Generic.HashSet`1<T>&,Sirenix.Serialization.IDataWriter)
// 0x0000029F System.Type Sirenix.Serialization.IFormatter::get_SerializedType()
// 0x000002A0 System.Void Sirenix.Serialization.IFormatter::Serialize(System.Object,Sirenix.Serialization.IDataWriter)
// 0x000002A1 System.Object Sirenix.Serialization.IFormatter::Deserialize(Sirenix.Serialization.IDataReader)
// 0x000002A2 System.Void Sirenix.Serialization.IFormatter`1::Serialize(T,Sirenix.Serialization.IDataWriter)
// 0x000002A3 T Sirenix.Serialization.IFormatter`1::Deserialize(Sirenix.Serialization.IDataReader)
// 0x000002A4 System.Void Sirenix.Serialization.KeyValuePairFormatter`2::SerializeImplementation(System.Collections.Generic.KeyValuePair`2<TKey,TValue>&,Sirenix.Serialization.IDataWriter)
// 0x000002A5 System.Void Sirenix.Serialization.KeyValuePairFormatter`2::DeserializeImplementation(System.Collections.Generic.KeyValuePair`2<TKey,TValue>&,Sirenix.Serialization.IDataReader)
// 0x000002A6 System.Void Sirenix.Serialization.KeyValuePairFormatter`2::.ctor()
// 0x000002A7 System.Void Sirenix.Serialization.KeyValuePairFormatter`2::.cctor()
// 0x000002A8 System.Void Sirenix.Serialization.ListFormatter`1::.cctor()
// 0x000002A9 System.Void Sirenix.Serialization.ListFormatter`1::.ctor()
// 0x000002AA System.Collections.Generic.List`1<T> Sirenix.Serialization.ListFormatter`1::GetUninitializedObject()
// 0x000002AB System.Void Sirenix.Serialization.ListFormatter`1::DeserializeImplementation(System.Collections.Generic.List`1<T>&,Sirenix.Serialization.IDataReader)
// 0x000002AC System.Void Sirenix.Serialization.ListFormatter`1::SerializeImplementation(System.Collections.Generic.List`1<T>&,Sirenix.Serialization.IDataWriter)
// 0x000002AD System.Type Sirenix.Serialization.MinimalBaseFormatter`1::get_SerializedType()
// 0x000002AE T Sirenix.Serialization.MinimalBaseFormatter`1::Deserialize(Sirenix.Serialization.IDataReader)
// 0x000002AF System.Void Sirenix.Serialization.MinimalBaseFormatter`1::Serialize(T,Sirenix.Serialization.IDataWriter)
// 0x000002B0 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::Sirenix.Serialization.IFormatter.Serialize(System.Object,Sirenix.Serialization.IDataWriter)
// 0x000002B1 System.Object Sirenix.Serialization.MinimalBaseFormatter`1::Sirenix.Serialization.IFormatter.Deserialize(Sirenix.Serialization.IDataReader)
// 0x000002B2 T Sirenix.Serialization.MinimalBaseFormatter`1::GetUninitializedObject()
// 0x000002B3 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::Read(T&,Sirenix.Serialization.IDataReader)
// 0x000002B4 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::Write(T&,Sirenix.Serialization.IDataWriter)
// 0x000002B5 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::RegisterReferenceID(T,Sirenix.Serialization.IDataReader)
// 0x000002B6 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::.ctor()
// 0x000002B7 System.Void Sirenix.Serialization.MinimalBaseFormatter`1::.cctor()
// 0x000002B8 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::.cctor()
// 0x000002B9 TArray Sirenix.Serialization.MultiDimensionalArrayFormatter`2::GetUninitializedObject()
// 0x000002BA System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::DeserializeImplementation(TArray&,Sirenix.Serialization.IDataReader)
// 0x000002BB System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::SerializeImplementation(TArray&,Sirenix.Serialization.IDataWriter)
// 0x000002BC System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::IterateArrayWrite(System.Array,System.Func`1<TElement>)
// 0x000002BD System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::IterateArrayWrite(System.Array,System.Int32,System.Int32[],System.Func`1<TElement>)
// 0x000002BE System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::IterateArrayRead(System.Array,System.Action`1<TElement>)
// 0x000002BF System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::IterateArrayRead(System.Array,System.Int32,System.Int32[],System.Action`1<TElement>)
// 0x000002C0 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2::.ctor()
// 0x000002C1 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2/<>c__DisplayClass6_0::.ctor()
// 0x000002C2 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2/<>c__DisplayClass6_1::.ctor()
// 0x000002C3 TElement Sirenix.Serialization.MultiDimensionalArrayFormatter`2/<>c__DisplayClass6_1::<DeserializeImplementation>b__0()
// 0x000002C4 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2/<>c__DisplayClass7_0::.ctor()
// 0x000002C5 System.Void Sirenix.Serialization.MultiDimensionalArrayFormatter`2/<>c__DisplayClass7_0::<SerializeImplementation>b__0(TElement)
// 0x000002C6 System.Void Sirenix.Serialization.NullableFormatter`1::.cctor()
// 0x000002C7 System.Void Sirenix.Serialization.NullableFormatter`1::.ctor()
// 0x000002C8 System.Void Sirenix.Serialization.NullableFormatter`1::DeserializeImplementation(System.Nullable`1<T>&,Sirenix.Serialization.IDataReader)
// 0x000002C9 System.Void Sirenix.Serialization.NullableFormatter`1::SerializeImplementation(System.Nullable`1<T>&,Sirenix.Serialization.IDataWriter)
// 0x000002CA T[] Sirenix.Serialization.PrimitiveArrayFormatter`1::GetUninitializedObject()
// 0x000002CB System.Void Sirenix.Serialization.PrimitiveArrayFormatter`1::Read(T[]&,Sirenix.Serialization.IDataReader)
// 0x000002CC System.Void Sirenix.Serialization.PrimitiveArrayFormatter`1::Write(T[]&,Sirenix.Serialization.IDataWriter)
// 0x000002CD System.Void Sirenix.Serialization.PrimitiveArrayFormatter`1::.ctor()
// 0x000002CE System.Void Sirenix.Serialization.ReflectionFormatter`1::.ctor()
// 0x000002CF System.Void Sirenix.Serialization.ReflectionFormatter`1::.ctor(Sirenix.Serialization.ISerializationPolicy)
// 0x000002D0 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.ReflectionFormatter`1::get_OverridePolicy()
// 0x000002D1 System.Void Sirenix.Serialization.ReflectionFormatter`1::set_OverridePolicy(Sirenix.Serialization.ISerializationPolicy)
// 0x000002D2 System.Void Sirenix.Serialization.ReflectionFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x000002D3 System.Void Sirenix.Serialization.ReflectionFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x000002D4 System.Void Sirenix.Serialization.SelfFormatterFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x000002D5 System.Void Sirenix.Serialization.SelfFormatterFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x000002D6 System.Void Sirenix.Serialization.SelfFormatterFormatter`1::.ctor()
// 0x000002D7 System.Void Sirenix.Serialization.SerializableFormatter`1::.cctor()
// 0x000002D8 T Sirenix.Serialization.SerializableFormatter`1::GetUninitializedObject()
// 0x000002D9 System.Void Sirenix.Serialization.SerializableFormatter`1::DeserializeImplementation(T&,Sirenix.Serialization.IDataReader)
// 0x000002DA System.Void Sirenix.Serialization.SerializableFormatter`1::SerializeImplementation(T&,Sirenix.Serialization.IDataWriter)
// 0x000002DB System.Runtime.Serialization.SerializationInfo Sirenix.Serialization.SerializableFormatter`1::ReadSerializationInfo(Sirenix.Serialization.IDataReader)
// 0x000002DC System.Void Sirenix.Serialization.SerializableFormatter`1::WriteSerializationInfo(System.Runtime.Serialization.SerializationInfo,Sirenix.Serialization.IDataWriter)
// 0x000002DD System.Void Sirenix.Serialization.SerializableFormatter`1::.ctor()
// 0x000002DE System.Void Sirenix.Serialization.SerializableFormatter`1/<>c__DisplayClass2_0::.ctor()
// 0x000002DF T Sirenix.Serialization.SerializableFormatter`1/<>c__DisplayClass2_0::<.cctor>b__0(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000002E0 System.Void Sirenix.Serialization.TimeSpanFormatter::Read(System.TimeSpan&,Sirenix.Serialization.IDataReader)
extern void TimeSpanFormatter_Read_mD8967D32F06467976012CBFD0853EA46B6A2541A (void);
// 0x000002E1 System.Void Sirenix.Serialization.TimeSpanFormatter::Write(System.TimeSpan&,Sirenix.Serialization.IDataWriter)
extern void TimeSpanFormatter_Write_m0237441AF1DBFD6E6716E32770DF2EE8710C6BC6 (void);
// 0x000002E2 System.Void Sirenix.Serialization.TimeSpanFormatter::.ctor()
extern void TimeSpanFormatter__ctor_mBB3F18137C3F2C0AAD311C58CC1D7AF45ACCC932 (void);
// 0x000002E3 System.Void Sirenix.Serialization.TypeFormatter::Read(System.Type&,Sirenix.Serialization.IDataReader)
extern void TypeFormatter_Read_mD80849457C98338DB6100DD60646D09308CD869B (void);
// 0x000002E4 System.Void Sirenix.Serialization.TypeFormatter::Write(System.Type&,Sirenix.Serialization.IDataWriter)
extern void TypeFormatter_Write_mCEEA83B29473614DBC541FFA09C4C83171A3000B (void);
// 0x000002E5 System.Type Sirenix.Serialization.TypeFormatter::GetUninitializedObject()
extern void TypeFormatter_GetUninitializedObject_mBA650171F06BAFF967D343F633D324675731C744 (void);
// 0x000002E6 System.Void Sirenix.Serialization.TypeFormatter::.ctor()
extern void TypeFormatter__ctor_m13E5F58172F12B68FD6A978C7687AC19A8110DD7 (void);
// 0x000002E7 System.Void Sirenix.Serialization.CustomSerializationPolicy::.ctor(System.String,System.Boolean,System.Func`2<System.Reflection.MemberInfo,System.Boolean>)
extern void CustomSerializationPolicy__ctor_mF5F1DA0F2373F8B66C8F94682444CE26225BF6ED (void);
// 0x000002E8 System.String Sirenix.Serialization.CustomSerializationPolicy::get_ID()
extern void CustomSerializationPolicy_get_ID_mD7CEC7D24FADBF7C0D9001A76F26B3D8F0358BDE (void);
// 0x000002E9 System.Boolean Sirenix.Serialization.CustomSerializationPolicy::get_AllowNonSerializableTypes()
extern void CustomSerializationPolicy_get_AllowNonSerializableTypes_m9915ED22749DA2B7A04FC22932B1410A7CA9F721 (void);
// 0x000002EA System.Boolean Sirenix.Serialization.CustomSerializationPolicy::ShouldSerializeMember(System.Reflection.MemberInfo)
extern void CustomSerializationPolicy_ShouldSerializeMember_m181837020AE6BF0EBF3D18D2003542CB9BE74A58 (void);
// 0x000002EB System.Void Sirenix.Serialization.AlwaysFormatsSelfAttribute::.ctor()
extern void AlwaysFormatsSelfAttribute__ctor_m1541ADA655A347F12F8F26C40B7D952C50C8CF50 (void);
// 0x000002EC System.Void Sirenix.Serialization.CustomFormatterAttribute::.ctor()
extern void CustomFormatterAttribute__ctor_m36953AC85BD97885921B073BA3EF3F6F62DD61AA (void);
// 0x000002ED System.Void Sirenix.Serialization.CustomFormatterAttribute::.ctor(System.Int32)
extern void CustomFormatterAttribute__ctor_m526DBB0A8BD4136097789728E4202D0174D1648C (void);
// 0x000002EE System.Void Sirenix.Serialization.CustomGenericFormatterAttribute::.ctor(System.Type,System.Int32)
extern void CustomGenericFormatterAttribute__ctor_mC1B9E154BA604D2A434AA5B3223FA2E14D0C25A2 (void);
// 0x000002EF System.Void Sirenix.Serialization.BindTypeNameToTypeAttribute::.ctor(System.String,System.Type)
extern void BindTypeNameToTypeAttribute__ctor_m4D779E05709E4A4E5C3A951D2CDFFEC85D691B9C (void);
// 0x000002F0 System.Void Sirenix.Serialization.DefaultSerializationBinder::.cctor()
extern void DefaultSerializationBinder__cctor_m4E56F7F408C0126441918D598FC455D0FD0599FF (void);
// 0x000002F1 System.Void Sirenix.Serialization.DefaultSerializationBinder::RegisterAssembly(System.Reflection.Assembly)
extern void DefaultSerializationBinder_RegisterAssembly_m876B33EC9E77AFB6D612E8F7E12477B31C113103 (void);
// 0x000002F2 System.String Sirenix.Serialization.DefaultSerializationBinder::BindToName(System.Type,Sirenix.Serialization.DebugContext)
extern void DefaultSerializationBinder_BindToName_m6F5A7AE51776942F8A8EB18E93F5C2380DB6454B (void);
// 0x000002F3 System.Boolean Sirenix.Serialization.DefaultSerializationBinder::ContainsType(System.String)
extern void DefaultSerializationBinder_ContainsType_mDA5AFEB66B4D2191A97376A57DDC5551C0495025 (void);
// 0x000002F4 System.Type Sirenix.Serialization.DefaultSerializationBinder::BindToType(System.String,Sirenix.Serialization.DebugContext)
extern void DefaultSerializationBinder_BindToType_mAFFB58163889FFB2E39A7DAA13DE9B82DF572DB2 (void);
// 0x000002F5 System.Type Sirenix.Serialization.DefaultSerializationBinder::ParseTypeName(System.String,Sirenix.Serialization.DebugContext)
extern void DefaultSerializationBinder_ParseTypeName_m6F74A5D1FD035060DFB110F9D4F52A1E29AD716E (void);
// 0x000002F6 System.Void Sirenix.Serialization.DefaultSerializationBinder::ParseName(System.String,System.String&,System.String&)
extern void DefaultSerializationBinder_ParseName_mED6C3E3BB66FECB451B4405059E59BEB29B72045 (void);
// 0x000002F7 System.Type Sirenix.Serialization.DefaultSerializationBinder::ParseGenericAndOrArrayType(System.String,Sirenix.Serialization.DebugContext)
extern void DefaultSerializationBinder_ParseGenericAndOrArrayType_mF5AC0FE8DFECA33ACD77CC89882F3C137650F720 (void);
// 0x000002F8 System.Boolean Sirenix.Serialization.DefaultSerializationBinder::TryParseGenericAndOrArrayTypeName(System.String,System.String&,System.Boolean&,System.Collections.Generic.List`1<System.String>&,System.Boolean&,System.Int32&)
extern void DefaultSerializationBinder_TryParseGenericAndOrArrayTypeName_m8EF5780F2785277DE6CA0312CCEB7B3D8F03EAE5 (void);
// 0x000002F9 System.Char Sirenix.Serialization.DefaultSerializationBinder::Peek(System.String,System.Int32,System.Int32)
extern void DefaultSerializationBinder_Peek_m297D89DF109E0DD23D792F95206E48B6E73B81C2 (void);
// 0x000002FA System.Boolean Sirenix.Serialization.DefaultSerializationBinder::ReadGenericArg(System.String,System.Int32&,System.String&)
extern void DefaultSerializationBinder_ReadGenericArg_mEF3DA1F2B717193B75158DC9BE1B373204D1D5B1 (void);
// 0x000002FB System.Void Sirenix.Serialization.DefaultSerializationBinder::.ctor()
extern void DefaultSerializationBinder__ctor_m22B29482A0F586CBE75CCD2E9B35583C56DB1F3E (void);
// 0x000002FC System.Void Sirenix.Serialization.DefaultSerializationBinder/<>c::.cctor()
extern void U3CU3Ec__cctor_m7D4E84798DE34140B22462E62480D747B7F4FE16 (void);
// 0x000002FD System.Void Sirenix.Serialization.DefaultSerializationBinder/<>c::.ctor()
extern void U3CU3Ec__ctor_m5B9239DC89210A7E40190FD4928AEEB2529F4F6D (void);
// 0x000002FE System.Void Sirenix.Serialization.DefaultSerializationBinder/<>c::<.cctor>b__9_0(System.Object,System.AssemblyLoadEventArgs)
extern void U3CU3Ec_U3C_cctorU3Eb__9_0_m300707410D7EFD45DE00D9B86D390F0E75F677D4 (void);
// 0x000002FF System.Void Sirenix.Serialization.DeserializationContext::.ctor()
extern void DeserializationContext__ctor_m9779A96F262A51AFF232FAE4FAFDF6231FDFCB59 (void);
// 0x00000300 System.Void Sirenix.Serialization.DeserializationContext::.ctor(System.Runtime.Serialization.StreamingContext)
extern void DeserializationContext__ctor_mD34C3165AEDEF271A67A4EF381C916F7B793CED4 (void);
// 0x00000301 System.Void Sirenix.Serialization.DeserializationContext::.ctor(System.Runtime.Serialization.FormatterConverter)
extern void DeserializationContext__ctor_m274B1E7A2F09E5A7EE58663F9ECD7541A251DD5E (void);
// 0x00000302 System.Void Sirenix.Serialization.DeserializationContext::.ctor(System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.FormatterConverter)
extern void DeserializationContext__ctor_m498FFB2989BACE4DDA56EA6A16C58A4BD49C64BF (void);
// 0x00000303 Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.DeserializationContext::get_Binder()
extern void DeserializationContext_get_Binder_m4B0930934A1EEEBBF7AEDA2C5293FD76FAD0B6D8 (void);
// 0x00000304 System.Void Sirenix.Serialization.DeserializationContext::set_Binder(Sirenix.Serialization.TwoWaySerializationBinder)
extern void DeserializationContext_set_Binder_m5A534C7B227E60A4A20C221355DEBF34974D9F1E (void);
// 0x00000305 Sirenix.Serialization.IExternalStringReferenceResolver Sirenix.Serialization.DeserializationContext::get_StringReferenceResolver()
extern void DeserializationContext_get_StringReferenceResolver_m2E6A2B2DD825C6A09328D89BBA8E51DE9C39CF7A (void);
// 0x00000306 System.Void Sirenix.Serialization.DeserializationContext::set_StringReferenceResolver(Sirenix.Serialization.IExternalStringReferenceResolver)
extern void DeserializationContext_set_StringReferenceResolver_mDCA00F48D400266293A8047DD7691313DDD7398E (void);
// 0x00000307 Sirenix.Serialization.IExternalGuidReferenceResolver Sirenix.Serialization.DeserializationContext::get_GuidReferenceResolver()
extern void DeserializationContext_get_GuidReferenceResolver_m07C4BC6749087DE670A55821EE76E3FDC4763842 (void);
// 0x00000308 System.Void Sirenix.Serialization.DeserializationContext::set_GuidReferenceResolver(Sirenix.Serialization.IExternalGuidReferenceResolver)
extern void DeserializationContext_set_GuidReferenceResolver_mFBE1143558704260CC3056661B695858DA0CBD49 (void);
// 0x00000309 Sirenix.Serialization.IExternalIndexReferenceResolver Sirenix.Serialization.DeserializationContext::get_IndexReferenceResolver()
extern void DeserializationContext_get_IndexReferenceResolver_mA9574D5FABC6706AA16F89CEE489D3D69CD169C7 (void);
// 0x0000030A System.Void Sirenix.Serialization.DeserializationContext::set_IndexReferenceResolver(Sirenix.Serialization.IExternalIndexReferenceResolver)
extern void DeserializationContext_set_IndexReferenceResolver_m085C6EE44AC7431CF98E3AB8DA0B6512274E2E34 (void);
// 0x0000030B System.Runtime.Serialization.StreamingContext Sirenix.Serialization.DeserializationContext::get_StreamingContext()
extern void DeserializationContext_get_StreamingContext_m1C401093278556DD4292408E248CFC1406673B65 (void);
// 0x0000030C System.Runtime.Serialization.IFormatterConverter Sirenix.Serialization.DeserializationContext::get_FormatterConverter()
extern void DeserializationContext_get_FormatterConverter_mC292079B7C156A8661073287AE55558F0A38FAE8 (void);
// 0x0000030D Sirenix.Serialization.SerializationConfig Sirenix.Serialization.DeserializationContext::get_Config()
extern void DeserializationContext_get_Config_mBCE9B24095D46BB256F798F6DC382F7206BFFABD (void);
// 0x0000030E System.Void Sirenix.Serialization.DeserializationContext::set_Config(Sirenix.Serialization.SerializationConfig)
extern void DeserializationContext_set_Config_m37F328893E1C5D2ACDDB6856D44BCC96BD834947 (void);
// 0x0000030F System.Void Sirenix.Serialization.DeserializationContext::RegisterInternalReference(System.Int32,System.Object)
extern void DeserializationContext_RegisterInternalReference_mE899B15215AAFA95E7B30FE7A3031FCDD0A21CA2 (void);
// 0x00000310 System.Object Sirenix.Serialization.DeserializationContext::GetInternalReference(System.Int32)
extern void DeserializationContext_GetInternalReference_m50E00070EF10DE4A896B2577FCD98319E06B5564 (void);
// 0x00000311 System.Object Sirenix.Serialization.DeserializationContext::GetExternalObject(System.Int32)
extern void DeserializationContext_GetExternalObject_mC40F1651D8D8E43127BFF2E994B4CFCC6615431C (void);
// 0x00000312 System.Object Sirenix.Serialization.DeserializationContext::GetExternalObject(System.Guid)
extern void DeserializationContext_GetExternalObject_mFF370114AE3B1BAF1ADB06BA52B67D75743F8005 (void);
// 0x00000313 System.Object Sirenix.Serialization.DeserializationContext::GetExternalObject(System.String)
extern void DeserializationContext_GetExternalObject_m0811101A4CC004CBF0ABF88ACF4A6FA30E442F6D (void);
// 0x00000314 System.Void Sirenix.Serialization.DeserializationContext::Reset()
extern void DeserializationContext_Reset_m16192CB9AD4BEDBFB45A532499A9CDE07C659A04 (void);
// 0x00000315 System.Void Sirenix.Serialization.DeserializationContext::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnFreed()
extern void DeserializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_mF832E26ADB74079AA4DD5EEADDEAB37033F113FF (void);
// 0x00000316 System.Void Sirenix.Serialization.DeserializationContext::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnClaimed()
extern void DeserializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_m7BB538880888B8F0F0F2514FF72C2A251DAC068C (void);
// 0x00000317 System.Void Sirenix.Serialization.ExcludeDataFromInspectorAttribute::.ctor()
extern void ExcludeDataFromInspectorAttribute__ctor_m5F2E8B6F6636312F9EA4E7C1C98E1FA64C6717EF (void);
// 0x00000318 Sirenix.Serialization.IExternalGuidReferenceResolver Sirenix.Serialization.IExternalGuidReferenceResolver::get_NextResolver()
// 0x00000319 System.Void Sirenix.Serialization.IExternalGuidReferenceResolver::set_NextResolver(Sirenix.Serialization.IExternalGuidReferenceResolver)
// 0x0000031A System.Boolean Sirenix.Serialization.IExternalGuidReferenceResolver::TryResolveReference(System.Guid,System.Object&)
// 0x0000031B System.Boolean Sirenix.Serialization.IExternalGuidReferenceResolver::CanReference(System.Object,System.Guid&)
// 0x0000031C System.Boolean Sirenix.Serialization.IExternalIndexReferenceResolver::TryResolveReference(System.Int32,System.Object&)
// 0x0000031D System.Boolean Sirenix.Serialization.IExternalIndexReferenceResolver::CanReference(System.Object,System.Int32&)
// 0x0000031E Sirenix.Serialization.IExternalStringReferenceResolver Sirenix.Serialization.IExternalStringReferenceResolver::get_NextResolver()
// 0x0000031F System.Void Sirenix.Serialization.IExternalStringReferenceResolver::set_NextResolver(Sirenix.Serialization.IExternalStringReferenceResolver)
// 0x00000320 System.Boolean Sirenix.Serialization.IExternalStringReferenceResolver::TryResolveReference(System.String,System.Object&)
// 0x00000321 System.Boolean Sirenix.Serialization.IExternalStringReferenceResolver::CanReference(System.Object,System.String&)
// 0x00000322 System.Void Sirenix.Serialization.ISelfFormatter::Serialize(Sirenix.Serialization.IDataWriter)
// 0x00000323 System.Void Sirenix.Serialization.ISelfFormatter::Deserialize(Sirenix.Serialization.IDataReader)
// 0x00000324 System.String Sirenix.Serialization.ISerializationPolicy::get_ID()
// 0x00000325 System.Boolean Sirenix.Serialization.ISerializationPolicy::get_AllowNonSerializableTypes()
// 0x00000326 System.Boolean Sirenix.Serialization.ISerializationPolicy::ShouldSerializeMember(System.Reflection.MemberInfo)
// 0x00000327 System.Void Sirenix.Serialization.NodeInfo::.ctor(System.String,System.Int32,System.Type,System.Boolean)
extern void NodeInfo__ctor_m80DB71C7F04F575293A2C3ED71FEFEFDA81F0A7A_AdjustorThunk (void);
// 0x00000328 System.Void Sirenix.Serialization.NodeInfo::.ctor(System.Boolean)
extern void NodeInfo__ctor_mC7784D929F099B20EF3F485D21546F89A15F7229_AdjustorThunk (void);
// 0x00000329 System.Boolean Sirenix.Serialization.NodeInfo::op_Equality(Sirenix.Serialization.NodeInfo,Sirenix.Serialization.NodeInfo)
extern void NodeInfo_op_Equality_mC00ACFE400D8D926E93F13824307C7C03C074EA5 (void);
// 0x0000032A System.Boolean Sirenix.Serialization.NodeInfo::op_Inequality(Sirenix.Serialization.NodeInfo,Sirenix.Serialization.NodeInfo)
extern void NodeInfo_op_Inequality_mF6D738C53EF7114186029E27AAC8F4253797F6D6 (void);
// 0x0000032B System.Boolean Sirenix.Serialization.NodeInfo::Equals(System.Object)
extern void NodeInfo_Equals_m8557079FD46245447C0DB54C05885DB558520554_AdjustorThunk (void);
// 0x0000032C System.Int32 Sirenix.Serialization.NodeInfo::GetHashCode()
extern void NodeInfo_GetHashCode_m65C7EB50665D7EC8C3914EEC4635BD7D196EF449_AdjustorThunk (void);
// 0x0000032D System.Void Sirenix.Serialization.NodeInfo::.cctor()
extern void NodeInfo__cctor_m70BB7A18A1FE03F3BF2DD83E6BC3E2C64406554F (void);
// 0x0000032E System.String Sirenix.Serialization.PreviouslySerializedAsAttribute::get_Name()
extern void PreviouslySerializedAsAttribute_get_Name_mD6B7A827CFCBB02C52D4361C71607E0ACA5DCC72 (void);
// 0x0000032F System.Void Sirenix.Serialization.PreviouslySerializedAsAttribute::set_Name(System.String)
extern void PreviouslySerializedAsAttribute_set_Name_m827CB90ACB0522D52FDD1273C1A88C86FC5128AB (void);
// 0x00000330 System.Void Sirenix.Serialization.PreviouslySerializedAsAttribute::.ctor(System.String)
extern void PreviouslySerializedAsAttribute__ctor_m8F72302DD4AB5A73047CBE7E20AEA70393C76E91 (void);
// 0x00000331 System.Void Sirenix.Serialization.SerializationAbortException::.ctor(System.String)
extern void SerializationAbortException__ctor_m9EB2F36F2A04850D5BA49ED9B98292083C96EDF2 (void);
// 0x00000332 System.Void Sirenix.Serialization.SerializationAbortException::.ctor(System.String,System.Exception)
extern void SerializationAbortException__ctor_m68CC62D5CDCFE4A9ADADFF44D1CABEC624BA0CAE (void);
// 0x00000333 System.Void Sirenix.Serialization.SerializationConfig::.ctor()
extern void SerializationConfig__ctor_m390208C2DC88A87D7F75A2593D65B08C06A2C72C (void);
// 0x00000334 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.SerializationConfig::get_SerializationPolicy()
extern void SerializationConfig_get_SerializationPolicy_m4E1D882B9C86CC9451CFDE4CB12DAB4C7F5C380A (void);
// 0x00000335 System.Void Sirenix.Serialization.SerializationConfig::set_SerializationPolicy(Sirenix.Serialization.ISerializationPolicy)
extern void SerializationConfig_set_SerializationPolicy_m614F0C6FB730AC46A63E0FA104C170E7A181BED7 (void);
// 0x00000336 Sirenix.Serialization.DebugContext Sirenix.Serialization.SerializationConfig::get_DebugContext()
extern void SerializationConfig_get_DebugContext_m9D0569609A9BAE8B52E04A6741271D290C3FC0A3 (void);
// 0x00000337 System.Void Sirenix.Serialization.SerializationConfig::set_DebugContext(Sirenix.Serialization.DebugContext)
extern void SerializationConfig_set_DebugContext_m6B57CACD98CA8D5CECFD2DE6CBB24D0638B51264 (void);
// 0x00000338 System.Void Sirenix.Serialization.SerializationConfig::ResetToDefault()
extern void SerializationConfig_ResetToDefault_m2D8799CF030BA890B48C5B334FD40D747094CBE3 (void);
// 0x00000339 Sirenix.Serialization.ILogger Sirenix.Serialization.DebugContext::get_Logger()
extern void DebugContext_get_Logger_m31C1C7D14197167011C3822D74E898C98FC8E834 (void);
// 0x0000033A System.Void Sirenix.Serialization.DebugContext::set_Logger(Sirenix.Serialization.ILogger)
extern void DebugContext_set_Logger_mB94A74527AD594F75E31606BD8132F3FAB7EF73C (void);
// 0x0000033B Sirenix.Serialization.LoggingPolicy Sirenix.Serialization.DebugContext::get_LoggingPolicy()
extern void DebugContext_get_LoggingPolicy_m57B11101D37D423BDFF9B8CBDE7A2D2F97C95961 (void);
// 0x0000033C System.Void Sirenix.Serialization.DebugContext::set_LoggingPolicy(Sirenix.Serialization.LoggingPolicy)
extern void DebugContext_set_LoggingPolicy_mC6351AA3797186252B61FA1D05F18C6A54844BB3 (void);
// 0x0000033D Sirenix.Serialization.ErrorHandlingPolicy Sirenix.Serialization.DebugContext::get_ErrorHandlingPolicy()
extern void DebugContext_get_ErrorHandlingPolicy_mA116727478BDFF90325007011E90CD2A4629A61C (void);
// 0x0000033E System.Void Sirenix.Serialization.DebugContext::set_ErrorHandlingPolicy(Sirenix.Serialization.ErrorHandlingPolicy)
extern void DebugContext_set_ErrorHandlingPolicy_m090EE6E61EAA63E1EEA02DA34B0E587D66925C44 (void);
// 0x0000033F System.Void Sirenix.Serialization.DebugContext::LogWarning(System.String)
extern void DebugContext_LogWarning_m9C2033B394AB9D718BD1497C470560FEE62EC7CC (void);
// 0x00000340 System.Void Sirenix.Serialization.DebugContext::LogError(System.String)
extern void DebugContext_LogError_mEC2F10FFB48C301041DCFF384CA26DE58D2952BC (void);
// 0x00000341 System.Void Sirenix.Serialization.DebugContext::LogException(System.Exception)
extern void DebugContext_LogException_mB56991ADAFF7DCE1E99FC292A45C718609036936 (void);
// 0x00000342 System.Void Sirenix.Serialization.DebugContext::ResetToDefault()
extern void DebugContext_ResetToDefault_mBED55368A6B2C7A30AF2392A514E67BE187ED8C2 (void);
// 0x00000343 System.Void Sirenix.Serialization.DebugContext::.ctor()
extern void DebugContext__ctor_m7D538BA803F40E2E7DA9EC551D1FC99CF0DB363E (void);
// 0x00000344 System.Void Sirenix.Serialization.SerializationContext::.ctor()
extern void SerializationContext__ctor_mFA3AA4339B95B7C4FBB1749E971EDFB031F2C815 (void);
// 0x00000345 System.Void Sirenix.Serialization.SerializationContext::.ctor(System.Runtime.Serialization.StreamingContext)
extern void SerializationContext__ctor_m13288803C6ADD0F4F6EC61351D894BBEE6C76173 (void);
// 0x00000346 System.Void Sirenix.Serialization.SerializationContext::.ctor(System.Runtime.Serialization.FormatterConverter)
extern void SerializationContext__ctor_m84B12C44312834E6EC50CDD2702795487F231A04 (void);
// 0x00000347 System.Void Sirenix.Serialization.SerializationContext::.ctor(System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.FormatterConverter)
extern void SerializationContext__ctor_m5723D441D289C54817EA2B428A4430D3F00E939C (void);
// 0x00000348 Sirenix.Serialization.TwoWaySerializationBinder Sirenix.Serialization.SerializationContext::get_Binder()
extern void SerializationContext_get_Binder_m91596C211FB7F857FFAFEDD72CE479BA73C3800B (void);
// 0x00000349 System.Void Sirenix.Serialization.SerializationContext::set_Binder(Sirenix.Serialization.TwoWaySerializationBinder)
extern void SerializationContext_set_Binder_m581116A2F0172FD501F0AB6951DA515B89103CD5 (void);
// 0x0000034A System.Runtime.Serialization.StreamingContext Sirenix.Serialization.SerializationContext::get_StreamingContext()
extern void SerializationContext_get_StreamingContext_m41531D50D93ADD4DD80E20833F164F261F1EA0E3 (void);
// 0x0000034B System.Runtime.Serialization.IFormatterConverter Sirenix.Serialization.SerializationContext::get_FormatterConverter()
extern void SerializationContext_get_FormatterConverter_mAC33ACEF47C70B571174523347488EE28E94AB68 (void);
// 0x0000034C Sirenix.Serialization.IExternalIndexReferenceResolver Sirenix.Serialization.SerializationContext::get_IndexReferenceResolver()
extern void SerializationContext_get_IndexReferenceResolver_mDA9EA854A29E80106218C2A9181669388AEFF7C2 (void);
// 0x0000034D System.Void Sirenix.Serialization.SerializationContext::set_IndexReferenceResolver(Sirenix.Serialization.IExternalIndexReferenceResolver)
extern void SerializationContext_set_IndexReferenceResolver_m30246FA74C05A8929BB601618D7D2F1C054170C1 (void);
// 0x0000034E Sirenix.Serialization.IExternalStringReferenceResolver Sirenix.Serialization.SerializationContext::get_StringReferenceResolver()
extern void SerializationContext_get_StringReferenceResolver_m8951CAFED2DD652D8BEF984D1F9D23397F5E61ED (void);
// 0x0000034F System.Void Sirenix.Serialization.SerializationContext::set_StringReferenceResolver(Sirenix.Serialization.IExternalStringReferenceResolver)
extern void SerializationContext_set_StringReferenceResolver_mD8F0F1C09F84790CA6B0E1036C8A98882112D8FE (void);
// 0x00000350 Sirenix.Serialization.IExternalGuidReferenceResolver Sirenix.Serialization.SerializationContext::get_GuidReferenceResolver()
extern void SerializationContext_get_GuidReferenceResolver_mDDB9FC152AB8A6A7A8B3D67105C2385820974430 (void);
// 0x00000351 System.Void Sirenix.Serialization.SerializationContext::set_GuidReferenceResolver(Sirenix.Serialization.IExternalGuidReferenceResolver)
extern void SerializationContext_set_GuidReferenceResolver_mBDB3D466481A10A762C1938D0FBD23198F13D5A6 (void);
// 0x00000352 Sirenix.Serialization.SerializationConfig Sirenix.Serialization.SerializationContext::get_Config()
extern void SerializationContext_get_Config_mD1D20E1B17FA1CEBEA1941043CFF2AF925B142F7 (void);
// 0x00000353 System.Void Sirenix.Serialization.SerializationContext::set_Config(Sirenix.Serialization.SerializationConfig)
extern void SerializationContext_set_Config_mA8905FA765B7BF8FAA36CE2EB3BB1B9BCD6CDC84 (void);
// 0x00000354 System.Boolean Sirenix.Serialization.SerializationContext::TryGetInternalReferenceId(System.Object,System.Int32&)
extern void SerializationContext_TryGetInternalReferenceId_m5A02931772D6C50C75F4B5CB88C142A9B9E7A0FD (void);
// 0x00000355 System.Boolean Sirenix.Serialization.SerializationContext::TryRegisterInternalReference(System.Object,System.Int32&)
extern void SerializationContext_TryRegisterInternalReference_m9ACBEF1882BE5697B5EF9CF68B1C02EEE017CFB3 (void);
// 0x00000356 System.Boolean Sirenix.Serialization.SerializationContext::TryRegisterExternalReference(System.Object,System.Int32&)
extern void SerializationContext_TryRegisterExternalReference_mBD785756C6A59336C63A7017A61CD467E601C636 (void);
// 0x00000357 System.Boolean Sirenix.Serialization.SerializationContext::TryRegisterExternalReference(System.Object,System.Guid&)
extern void SerializationContext_TryRegisterExternalReference_mA394B4E837B29F45B7E1FAACEF6B7B85BFDB590B (void);
// 0x00000358 System.Boolean Sirenix.Serialization.SerializationContext::TryRegisterExternalReference(System.Object,System.String&)
extern void SerializationContext_TryRegisterExternalReference_mB84920A8F3B1A100E0B9830F92FE62E04A23E303 (void);
// 0x00000359 System.Void Sirenix.Serialization.SerializationContext::ResetInternalReferences()
extern void SerializationContext_ResetInternalReferences_m6DA41CA826CC28EDBA429F6B5DC1795CDBD65EFF (void);
// 0x0000035A System.Void Sirenix.Serialization.SerializationContext::ResetToDefault()
extern void SerializationContext_ResetToDefault_m5EAD037F525E5C6E1A7F876C1311275EE697E98B (void);
// 0x0000035B System.Void Sirenix.Serialization.SerializationContext::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnFreed()
extern void SerializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_m97ECEDC3A5F1818633A3559D552584B8CA7FFD87 (void);
// 0x0000035C System.Void Sirenix.Serialization.SerializationContext::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnClaimed()
extern void SerializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_m15870EE712946EA768023908755BCA139EBBB3C4 (void);
// 0x0000035D System.Void Sirenix.Serialization.OdinSerializeAttribute::.ctor()
extern void OdinSerializeAttribute__ctor_m25FD490CA40157F3EE1EA0CCE0B9B4B395E3A25D (void);
// 0x0000035E System.String Sirenix.Serialization.TwoWaySerializationBinder::BindToName(System.Type,Sirenix.Serialization.DebugContext)
// 0x0000035F System.Type Sirenix.Serialization.TwoWaySerializationBinder::BindToType(System.String,Sirenix.Serialization.DebugContext)
// 0x00000360 System.Boolean Sirenix.Serialization.TwoWaySerializationBinder::ContainsType(System.String)
// 0x00000361 System.Void Sirenix.Serialization.TwoWaySerializationBinder::.ctor()
extern void TwoWaySerializationBinder__ctor_mBB6209AA85C6718D51CC1B5D44EFD43B6F125850 (void);
// 0x00000362 System.Void Sirenix.Serialization.TwoWaySerializationBinder::.cctor()
extern void TwoWaySerializationBinder__cctor_mEBF18F68D0BB08E5A1F426E1A476C3F25B333D9A (void);
// 0x00000363 System.Boolean Sirenix.Serialization.SerializationPolicies::TryGetByID(System.String,Sirenix.Serialization.ISerializationPolicy&)
extern void SerializationPolicies_TryGetByID_m8075991C4D3C34FB40599F7A6AD6A4AE2D912FFA (void);
// 0x00000364 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.SerializationPolicies::get_Everything()
extern void SerializationPolicies_get_Everything_m63ABB7C174CF5077DCEA03F7DA5E4B8F6C7B66B7 (void);
// 0x00000365 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.SerializationPolicies::get_Unity()
extern void SerializationPolicies_get_Unity_m66BDA95A3D225273AC1AEDF3A2B5E04D09F9F2B9 (void);
// 0x00000366 Sirenix.Serialization.ISerializationPolicy Sirenix.Serialization.SerializationPolicies::get_Strict()
extern void SerializationPolicies_get_Strict_m47F05C2C25C4F6B850709B3FAB6C54C6C5BB4245 (void);
// 0x00000367 System.Void Sirenix.Serialization.SerializationPolicies::.cctor()
extern void SerializationPolicies__cctor_m2DB7B0F1BB03544D104B2E07C3F2B3A4ADB6E9BA (void);
// 0x00000368 System.Void Sirenix.Serialization.SerializationPolicies/<>c::.cctor()
extern void U3CU3Ec__cctor_m189B45E4F1272D02E8E2754A2E1CAD106FF566A4 (void);
// 0x00000369 System.Void Sirenix.Serialization.SerializationPolicies/<>c::.ctor()
extern void U3CU3Ec__ctor_m542CAE9E28C21BB6FAE4D2116847C13EA4C63D4D (void);
// 0x0000036A System.Boolean Sirenix.Serialization.SerializationPolicies/<>c::<get_Everything>b__6_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3Cget_EverythingU3Eb__6_0_m9BC65B36F659100819D8C4B088A932637F741BD8 (void);
// 0x0000036B System.Boolean Sirenix.Serialization.SerializationPolicies/<>c::<get_Strict>b__10_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3Cget_StrictU3Eb__10_0_m467EBC473301EA945B6F057B77B13DFB1ECD2F74 (void);
// 0x0000036C System.Void Sirenix.Serialization.SerializationPolicies/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m34304279BFF55350DBC69C33406677A59801B229 (void);
// 0x0000036D System.Boolean Sirenix.Serialization.SerializationPolicies/<>c__DisplayClass8_0::<get_Unity>b__0(System.Reflection.MemberInfo)
extern void U3CU3Ec__DisplayClass8_0_U3Cget_UnityU3Eb__0_m0C7D21A7B7062810B1985EF6B4D045A616C201A6 (void);
// 0x0000036E System.Boolean Sirenix.Serialization.BooleanSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void BooleanSerializer_ReadValue_mB24D6DE1A71324A7DC53D2F4A762D1C5CD47B64E (void);
// 0x0000036F System.Void Sirenix.Serialization.BooleanSerializer::WriteValue(System.String,System.Boolean,Sirenix.Serialization.IDataWriter)
extern void BooleanSerializer_WriteValue_m5DE4CA51560EFE7AE85F4C2796A427369250370F (void);
// 0x00000370 System.Void Sirenix.Serialization.BooleanSerializer::.ctor()
extern void BooleanSerializer__ctor_m84122A09616E473CF066EF3D576D781956A6EB02 (void);
// 0x00000371 System.Byte Sirenix.Serialization.ByteSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void ByteSerializer_ReadValue_mEDF73946AC3BA28542C9306C0C4F24E2C249006A (void);
// 0x00000372 System.Void Sirenix.Serialization.ByteSerializer::WriteValue(System.String,System.Byte,Sirenix.Serialization.IDataWriter)
extern void ByteSerializer_WriteValue_mAF9CAAFC0D114580D70843999287633C605C0BCE (void);
// 0x00000373 System.Void Sirenix.Serialization.ByteSerializer::.ctor()
extern void ByteSerializer__ctor_mC120B986867479BC047B18219E2A7CA9B9255B60 (void);
// 0x00000374 System.Char Sirenix.Serialization.CharSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void CharSerializer_ReadValue_m1E48416978FC00573D8343E00B34846D2C8E04D5 (void);
// 0x00000375 System.Void Sirenix.Serialization.CharSerializer::WriteValue(System.String,System.Char,Sirenix.Serialization.IDataWriter)
extern void CharSerializer_WriteValue_m50F73209C54DB855A4A97B768836A08047D0F7C5 (void);
// 0x00000376 System.Void Sirenix.Serialization.CharSerializer::.ctor()
extern void CharSerializer__ctor_m32C8CAC5AB6082233E71CE6C215551029A1EEF62 (void);
// 0x00000377 T Sirenix.Serialization.ComplexTypeSerializer`1::ReadValue(Sirenix.Serialization.IDataReader)
// 0x00000378 Sirenix.Serialization.IFormatter`1<T> Sirenix.Serialization.ComplexTypeSerializer`1::GetBaseFormatter(Sirenix.Serialization.ISerializationPolicy)
// 0x00000379 System.Void Sirenix.Serialization.ComplexTypeSerializer`1::WriteValue(System.String,T,Sirenix.Serialization.IDataWriter)
// 0x0000037A System.Void Sirenix.Serialization.ComplexTypeSerializer`1::.ctor()
// 0x0000037B System.Void Sirenix.Serialization.ComplexTypeSerializer`1::.cctor()
// 0x0000037C System.Decimal Sirenix.Serialization.DecimalSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void DecimalSerializer_ReadValue_m90EA6028DD57788F6AD2F96E3A73405BC72FE1B4 (void);
// 0x0000037D System.Void Sirenix.Serialization.DecimalSerializer::WriteValue(System.String,System.Decimal,Sirenix.Serialization.IDataWriter)
extern void DecimalSerializer_WriteValue_mE139172EFB64163F7050D82DA641E6BB616C1C97 (void);
// 0x0000037E System.Void Sirenix.Serialization.DecimalSerializer::.ctor()
extern void DecimalSerializer__ctor_mB9952FECC1DC9917D2F3844D1C8AD2BFDA61C1E7 (void);
// 0x0000037F System.Double Sirenix.Serialization.DoubleSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void DoubleSerializer_ReadValue_m7AF1A3B6FFC44925712A60845BA3312ADF36FE97 (void);
// 0x00000380 System.Void Sirenix.Serialization.DoubleSerializer::WriteValue(System.String,System.Double,Sirenix.Serialization.IDataWriter)
extern void DoubleSerializer_WriteValue_m81ECB16AD9225B8F4715CACE84F83146CE822F0A (void);
// 0x00000381 System.Void Sirenix.Serialization.DoubleSerializer::.ctor()
extern void DoubleSerializer__ctor_m1744CB8ECBB309625C008379D5EE309B649E57AA (void);
// 0x00000382 System.Void Sirenix.Serialization.EnumSerializer`1::.cctor()
// 0x00000383 T Sirenix.Serialization.EnumSerializer`1::ReadValue(Sirenix.Serialization.IDataReader)
// 0x00000384 System.Void Sirenix.Serialization.EnumSerializer`1::WriteValue(System.String,T,Sirenix.Serialization.IDataWriter)
// 0x00000385 System.Void Sirenix.Serialization.EnumSerializer`1::.ctor()
// 0x00000386 System.Guid Sirenix.Serialization.GuidSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void GuidSerializer_ReadValue_mF11C73047E8E9D49435716A37A8D0D651DA3FB75 (void);
// 0x00000387 System.Void Sirenix.Serialization.GuidSerializer::WriteValue(System.String,System.Guid,Sirenix.Serialization.IDataWriter)
extern void GuidSerializer_WriteValue_mF3F24FA9B7704CA6B7A886A776D24F8435B9D01A (void);
// 0x00000388 System.Void Sirenix.Serialization.GuidSerializer::.ctor()
extern void GuidSerializer__ctor_m12CAD3617A9ADBE6E49880F370BD802EB8C8E697 (void);
// 0x00000389 System.Int16 Sirenix.Serialization.Int16Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void Int16Serializer_ReadValue_m4FFDDECE06DBC7B764F8545DBF62F86EEBF8BF72 (void);
// 0x0000038A System.Void Sirenix.Serialization.Int16Serializer::WriteValue(System.String,System.Int16,Sirenix.Serialization.IDataWriter)
extern void Int16Serializer_WriteValue_m1C002B77B8F5B1717CB72211CA8BF6A083762C40 (void);
// 0x0000038B System.Void Sirenix.Serialization.Int16Serializer::.ctor()
extern void Int16Serializer__ctor_mE145CB8E3BB6E01B7DD3149E24DFA25DFC8A86A8 (void);
// 0x0000038C System.Int32 Sirenix.Serialization.Int32Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void Int32Serializer_ReadValue_m7CC01A3FE3FDD4F710C4BD8AE67D662D513BCC95 (void);
// 0x0000038D System.Void Sirenix.Serialization.Int32Serializer::WriteValue(System.String,System.Int32,Sirenix.Serialization.IDataWriter)
extern void Int32Serializer_WriteValue_m3326B39D1CDBD6AB23D79F574B70906AB2A9CDD3 (void);
// 0x0000038E System.Void Sirenix.Serialization.Int32Serializer::.ctor()
extern void Int32Serializer__ctor_m958BF4399EBBB8FA17072FA4CE03B74279F795AE (void);
// 0x0000038F System.Int64 Sirenix.Serialization.Int64Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void Int64Serializer_ReadValue_mC1059ABE53F7376CB09F6C1B38B713D0D4635FB8 (void);
// 0x00000390 System.Void Sirenix.Serialization.Int64Serializer::WriteValue(System.String,System.Int64,Sirenix.Serialization.IDataWriter)
extern void Int64Serializer_WriteValue_m73BF747656E83BCD7BE4A7F8D69D6DD3DD25477E (void);
// 0x00000391 System.Void Sirenix.Serialization.Int64Serializer::.ctor()
extern void Int64Serializer__ctor_mEEF926FC59D286AC775A44D8297C43328F6B48D2 (void);
// 0x00000392 System.IntPtr Sirenix.Serialization.IntPtrSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void IntPtrSerializer_ReadValue_m7BB65364B88B29ADBB642D378D8F6E2C92E1EDD9 (void);
// 0x00000393 System.Void Sirenix.Serialization.IntPtrSerializer::WriteValue(System.String,System.IntPtr,Sirenix.Serialization.IDataWriter)
extern void IntPtrSerializer_WriteValue_mD3A14E3B74E32AC4E6B4C541A8F266935CC587CA (void);
// 0x00000394 System.Void Sirenix.Serialization.IntPtrSerializer::.ctor()
extern void IntPtrSerializer__ctor_m63BC93CA7AA51FC2E5ADCA3D0ED69B0A66F3B369 (void);
// 0x00000395 System.SByte Sirenix.Serialization.SByteSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void SByteSerializer_ReadValue_m194715E789B6CAC23FC94FF65FA2A6B37B027DB8 (void);
// 0x00000396 System.Void Sirenix.Serialization.SByteSerializer::WriteValue(System.String,System.SByte,Sirenix.Serialization.IDataWriter)
extern void SByteSerializer_WriteValue_m62E1C8DFA9D07DB7DA05345D02EFDF2B186C8961 (void);
// 0x00000397 System.Void Sirenix.Serialization.SByteSerializer::.ctor()
extern void SByteSerializer__ctor_mE4A0C8F60A9F572A73DD00BED3AA9B87CEE949A4 (void);
// 0x00000398 System.Void Sirenix.Serialization.Serializer::FireOnSerializedType(System.Type)
extern void Serializer_FireOnSerializedType_mA5E91B9D9502A09C9C5097AC97AE35CC3CEA9715 (void);
// 0x00000399 Sirenix.Serialization.Serializer Sirenix.Serialization.Serializer::GetForValue(System.Object)
extern void Serializer_GetForValue_m59BA4E7CF86FA8C5F413B9E8A1210A5BBAD8C597 (void);
// 0x0000039A Sirenix.Serialization.Serializer`1<T> Sirenix.Serialization.Serializer::Get()
// 0x0000039B Sirenix.Serialization.Serializer Sirenix.Serialization.Serializer::Get(System.Type)
extern void Serializer_Get_mE6F318A9F7C1127B3AAADF2A8778ABBDC6D776EE (void);
// 0x0000039C System.Object Sirenix.Serialization.Serializer::ReadValueWeak(Sirenix.Serialization.IDataReader)
// 0x0000039D System.Void Sirenix.Serialization.Serializer::WriteValueWeak(System.Object,Sirenix.Serialization.IDataWriter)
extern void Serializer_WriteValueWeak_m3FFF974CEA0DE4A544CE1E19BC70B04F3A543E05 (void);
// 0x0000039E System.Void Sirenix.Serialization.Serializer::WriteValueWeak(System.String,System.Object,Sirenix.Serialization.IDataWriter)
// 0x0000039F Sirenix.Serialization.Serializer Sirenix.Serialization.Serializer::Create(System.Type)
extern void Serializer_Create_m3FAF74364F2B543A5E401514D9271B94617CEFC9 (void);
// 0x000003A0 System.Void Sirenix.Serialization.Serializer::LogAOTError(System.Type,System.ExecutionEngineException)
extern void Serializer_LogAOTError_mC27E104AE9454A88A17AE94937DB8738D9DE6E80 (void);
// 0x000003A1 System.Void Sirenix.Serialization.Serializer::.ctor()
extern void Serializer__ctor_m45C015B6F9C601D1C5285C5AA20441FDF3533408 (void);
// 0x000003A2 System.Void Sirenix.Serialization.Serializer::.cctor()
extern void Serializer__cctor_mC319621B2CA4581640DB4B0FF4936953AF0EDBE5 (void);
// 0x000003A3 System.Object Sirenix.Serialization.Serializer`1::ReadValueWeak(Sirenix.Serialization.IDataReader)
// 0x000003A4 System.Void Sirenix.Serialization.Serializer`1::WriteValueWeak(System.String,System.Object,Sirenix.Serialization.IDataWriter)
// 0x000003A5 T Sirenix.Serialization.Serializer`1::ReadValue(Sirenix.Serialization.IDataReader)
// 0x000003A6 System.Void Sirenix.Serialization.Serializer`1::WriteValue(T,Sirenix.Serialization.IDataWriter)
// 0x000003A7 System.Void Sirenix.Serialization.Serializer`1::WriteValue(System.String,T,Sirenix.Serialization.IDataWriter)
// 0x000003A8 System.Void Sirenix.Serialization.Serializer`1::FireOnSerializedType()
// 0x000003A9 System.Void Sirenix.Serialization.Serializer`1::.ctor()
// 0x000003AA System.Single Sirenix.Serialization.SingleSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void SingleSerializer_ReadValue_m08ADBCF53C810AE82821F80AD767D0D050E549E7 (void);
// 0x000003AB System.Void Sirenix.Serialization.SingleSerializer::WriteValue(System.String,System.Single,Sirenix.Serialization.IDataWriter)
extern void SingleSerializer_WriteValue_m299B78B4D615A807F6CE1446E133A743363561D6 (void);
// 0x000003AC System.Void Sirenix.Serialization.SingleSerializer::.ctor()
extern void SingleSerializer__ctor_mCE39A6FEDF0B74D2F2F26DBF24A60390A423F66E (void);
// 0x000003AD System.String Sirenix.Serialization.StringSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void StringSerializer_ReadValue_mFF413B8A55A9868455C87A695317255676A40D8F (void);
// 0x000003AE System.Void Sirenix.Serialization.StringSerializer::WriteValue(System.String,System.String,Sirenix.Serialization.IDataWriter)
extern void StringSerializer_WriteValue_m11FECEF5966FFB38D2BF3E5B540F978CF4A07112 (void);
// 0x000003AF System.Void Sirenix.Serialization.StringSerializer::.ctor()
extern void StringSerializer__ctor_m1CA97544812FC0ADC58C09DF21E18F5D9698FA20 (void);
// 0x000003B0 System.UInt16 Sirenix.Serialization.UInt16Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void UInt16Serializer_ReadValue_m68441D9FA8D5860DD88215F4271EBD36A361C540 (void);
// 0x000003B1 System.Void Sirenix.Serialization.UInt16Serializer::WriteValue(System.String,System.UInt16,Sirenix.Serialization.IDataWriter)
extern void UInt16Serializer_WriteValue_mBEA9A10DD526B33C16ACB513B3F614530E8454C0 (void);
// 0x000003B2 System.Void Sirenix.Serialization.UInt16Serializer::.ctor()
extern void UInt16Serializer__ctor_mAB64A7DAE55003A3C3E668EAA2BA639EAA4A0559 (void);
// 0x000003B3 System.UInt32 Sirenix.Serialization.UInt32Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void UInt32Serializer_ReadValue_mD076D5DD17F1C0418F5E7FB850259699A5867755 (void);
// 0x000003B4 System.Void Sirenix.Serialization.UInt32Serializer::WriteValue(System.String,System.UInt32,Sirenix.Serialization.IDataWriter)
extern void UInt32Serializer_WriteValue_m0ADA275570A30CD6D0AD989AFD26875879336C06 (void);
// 0x000003B5 System.Void Sirenix.Serialization.UInt32Serializer::.ctor()
extern void UInt32Serializer__ctor_mB5B5C392CCB252F938EB8CAE7339031CADE67B73 (void);
// 0x000003B6 System.UInt64 Sirenix.Serialization.UInt64Serializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void UInt64Serializer_ReadValue_mFDE0D9088DFE95A936786BCD6853E91FB461BC0D (void);
// 0x000003B7 System.Void Sirenix.Serialization.UInt64Serializer::WriteValue(System.String,System.UInt64,Sirenix.Serialization.IDataWriter)
extern void UInt64Serializer_WriteValue_mBDEAEEC03C2B8E8883C75FB393A2022FA8D8B784 (void);
// 0x000003B8 System.Void Sirenix.Serialization.UInt64Serializer::.ctor()
extern void UInt64Serializer__ctor_m246252F1E014014F2548425C409591D9FD056E98 (void);
// 0x000003B9 System.UIntPtr Sirenix.Serialization.UIntPtrSerializer::ReadValue(Sirenix.Serialization.IDataReader)
extern void UIntPtrSerializer_ReadValue_mF09418A4BEA2D23D9924E45F9C09A384AC6E491F (void);
// 0x000003BA System.Void Sirenix.Serialization.UIntPtrSerializer::WriteValue(System.String,System.UIntPtr,Sirenix.Serialization.IDataWriter)
extern void UIntPtrSerializer_WriteValue_m7FD80EEFD11F66ED597F9D332F3E1DA817CD8DC3 (void);
// 0x000003BB System.Void Sirenix.Serialization.UIntPtrSerializer::.ctor()
extern void UIntPtrSerializer__ctor_m189B69F0273A9B1198A4560AE7FD135446E5B0F9 (void);
// 0x000003BC UnityEngine.AnimationCurve Sirenix.Serialization.AnimationCurveFormatter::GetUninitializedObject()
extern void AnimationCurveFormatter_GetUninitializedObject_m39D7C2032EA2B73D311032E21891DC85C1CFDE0F (void);
// 0x000003BD System.Void Sirenix.Serialization.AnimationCurveFormatter::Read(UnityEngine.AnimationCurve&,Sirenix.Serialization.IDataReader)
extern void AnimationCurveFormatter_Read_mF186997C7F474A58A503F97AB7F31D197385D919 (void);
// 0x000003BE System.Void Sirenix.Serialization.AnimationCurveFormatter::Write(UnityEngine.AnimationCurve&,Sirenix.Serialization.IDataWriter)
extern void AnimationCurveFormatter_Write_m1CC4E7935C0E6735116758E2D2F180B8753DB868 (void);
// 0x000003BF System.Void Sirenix.Serialization.AnimationCurveFormatter::.ctor()
extern void AnimationCurveFormatter__ctor_mC587C72DE21DA5C667B7C6A7D38DFF09B78BE992 (void);
// 0x000003C0 System.Void Sirenix.Serialization.AnimationCurveFormatter::.cctor()
extern void AnimationCurveFormatter__cctor_m5C899B6EADD5B0212BE2D98F29E26FCA37DE929D (void);
// 0x000003C1 System.Void Sirenix.Serialization.BoundsFormatter::Read(UnityEngine.Bounds&,Sirenix.Serialization.IDataReader)
extern void BoundsFormatter_Read_m86A104BD2DB957A42B6B5DF16A8A982B2BCDB769 (void);
// 0x000003C2 System.Void Sirenix.Serialization.BoundsFormatter::Write(UnityEngine.Bounds&,Sirenix.Serialization.IDataWriter)
extern void BoundsFormatter_Write_m4D5C3DEAAB2EE8FFEDAE2FCDD7AC4A754D0D01FD (void);
// 0x000003C3 System.Void Sirenix.Serialization.BoundsFormatter::.ctor()
extern void BoundsFormatter__ctor_m995CAEAE8724BEDE5D3028B40C49D8E3AFC282DA (void);
// 0x000003C4 System.Void Sirenix.Serialization.BoundsFormatter::.cctor()
extern void BoundsFormatter__cctor_m8A6EAFBE881328B2997F3C595855AA78280FA7BE (void);
// 0x000003C5 System.Void Sirenix.Serialization.Color32Formatter::Read(UnityEngine.Color32&,Sirenix.Serialization.IDataReader)
extern void Color32Formatter_Read_m916B4634EDA522BEEE2CBA8ECCDA223B9147DD21 (void);
// 0x000003C6 System.Void Sirenix.Serialization.Color32Formatter::Write(UnityEngine.Color32&,Sirenix.Serialization.IDataWriter)
extern void Color32Formatter_Write_mB424D3DCD796962900C1DEA08F9D1DFD1C92B93B (void);
// 0x000003C7 System.Void Sirenix.Serialization.Color32Formatter::.ctor()
extern void Color32Formatter__ctor_mBE595C0FC0FB29EC8C75260900CC2BCA7DCE2B0C (void);
// 0x000003C8 System.Void Sirenix.Serialization.Color32Formatter::.cctor()
extern void Color32Formatter__cctor_mAD42BF4DD4D6639E53B59E53BF30EFE4E1BDD643 (void);
// 0x000003C9 System.Boolean Sirenix.Serialization.ColorBlockFormatterLocator::TryGetFormatter(System.Type,Sirenix.Serialization.FormatterLocationStep,Sirenix.Serialization.ISerializationPolicy,Sirenix.Serialization.IFormatter&)
extern void ColorBlockFormatterLocator_TryGetFormatter_m6BE2548A9E500F695489B2F4F895A883D948DA0A (void);
// 0x000003CA System.Void Sirenix.Serialization.ColorBlockFormatterLocator::.ctor()
extern void ColorBlockFormatterLocator__ctor_mD012D228086F82901A2E97F2A4B4B15DF2E0ACD4 (void);
// 0x000003CB System.Void Sirenix.Serialization.ColorBlockFormatter`1::Read(T&,Sirenix.Serialization.IDataReader)
// 0x000003CC System.Void Sirenix.Serialization.ColorBlockFormatter`1::Write(T&,Sirenix.Serialization.IDataWriter)
// 0x000003CD System.Void Sirenix.Serialization.ColorBlockFormatter`1::.ctor()
// 0x000003CE System.Void Sirenix.Serialization.ColorBlockFormatter`1::.cctor()
// 0x000003CF System.Void Sirenix.Serialization.ColorFormatter::Read(UnityEngine.Color&,Sirenix.Serialization.IDataReader)
extern void ColorFormatter_Read_m991AC0864D249315602CF692FB014F68C5ECF5AA (void);
// 0x000003D0 System.Void Sirenix.Serialization.ColorFormatter::Write(UnityEngine.Color&,Sirenix.Serialization.IDataWriter)
extern void ColorFormatter_Write_m1C37E6C2C0205A58D8FE4304D32490572BBAB77C (void);
// 0x000003D1 System.Void Sirenix.Serialization.ColorFormatter::.ctor()
extern void ColorFormatter__ctor_m56CD5060CA089709ECA768AED0942CCB9036D5DF (void);
// 0x000003D2 System.Void Sirenix.Serialization.ColorFormatter::.cctor()
extern void ColorFormatter__cctor_m1E542D2B1200265BB3933A67CC94EEB2C7635E7C (void);
// 0x000003D3 System.Type Sirenix.Serialization.CoroutineFormatter::get_SerializedType()
extern void CoroutineFormatter_get_SerializedType_m252F4D0305E6ACEA15C6716544FAB8ABA9102B90 (void);
// 0x000003D4 System.Object Sirenix.Serialization.CoroutineFormatter::Sirenix.Serialization.IFormatter.Deserialize(Sirenix.Serialization.IDataReader)
extern void CoroutineFormatter_Sirenix_Serialization_IFormatter_Deserialize_m3485B13A5725704863EDCDB93544882F74608BD7 (void);
// 0x000003D5 UnityEngine.Coroutine Sirenix.Serialization.CoroutineFormatter::Deserialize(Sirenix.Serialization.IDataReader)
extern void CoroutineFormatter_Deserialize_mE3E71B20252BE4D1A9847A775889002D9D3E9FD5 (void);
// 0x000003D6 System.Void Sirenix.Serialization.CoroutineFormatter::Serialize(System.Object,Sirenix.Serialization.IDataWriter)
extern void CoroutineFormatter_Serialize_m05308770DF054E7D038948D71B88F19F52A2F9B4 (void);
// 0x000003D7 System.Void Sirenix.Serialization.CoroutineFormatter::Serialize(UnityEngine.Coroutine,Sirenix.Serialization.IDataWriter)
extern void CoroutineFormatter_Serialize_m423E50A59E60160263A3AC10C9A7B667E2259813 (void);
// 0x000003D8 System.Void Sirenix.Serialization.CoroutineFormatter::.ctor()
extern void CoroutineFormatter__ctor_m0D130A619A49FF286E016711EE6472BB960502F2 (void);
// 0x000003D9 System.String Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::get_ProviderID()
// 0x000003DA T Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::GetKeyFromPathString(System.String)
// 0x000003DB System.String Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::GetPathStringFromKey(T)
// 0x000003DC System.Int32 Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::Compare(T,T)
// 0x000003DD System.Int32 Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::Sirenix.Serialization.IDictionaryKeyPathProvider.Compare(System.Object,System.Object)
// 0x000003DE System.Object Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::Sirenix.Serialization.IDictionaryKeyPathProvider.GetKeyFromPathString(System.String)
// 0x000003DF System.String Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::Sirenix.Serialization.IDictionaryKeyPathProvider.GetPathStringFromKey(System.Object)
// 0x000003E0 System.Void Sirenix.Serialization.BaseDictionaryKeyPathProvider`1::.ctor()
// 0x000003E1 System.String Sirenix.Serialization.IDictionaryKeyPathProvider::get_ProviderID()
// 0x000003E2 System.String Sirenix.Serialization.IDictionaryKeyPathProvider::GetPathStringFromKey(System.Object)
// 0x000003E3 System.Object Sirenix.Serialization.IDictionaryKeyPathProvider::GetKeyFromPathString(System.String)
// 0x000003E4 System.Int32 Sirenix.Serialization.IDictionaryKeyPathProvider::Compare(System.Object,System.Object)
// 0x000003E5 System.String Sirenix.Serialization.IDictionaryKeyPathProvider`1::GetPathStringFromKey(T)
// 0x000003E6 T Sirenix.Serialization.IDictionaryKeyPathProvider`1::GetKeyFromPathString(System.String)
// 0x000003E7 System.Int32 Sirenix.Serialization.IDictionaryKeyPathProvider`1::Compare(T,T)
// 0x000003E8 System.Void Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute::.ctor(System.Type)
extern void RegisterDictionaryKeyPathProviderAttribute__ctor_m72BA975E1E2C3F05E19BDD08BA797D45385953D0 (void);
// 0x000003E9 System.String Sirenix.Serialization.Vector2DictionaryKeyPathProvider::get_ProviderID()
extern void Vector2DictionaryKeyPathProvider_get_ProviderID_m2D7053A716D09183D69021301A61443E29A04033 (void);
// 0x000003EA System.Int32 Sirenix.Serialization.Vector2DictionaryKeyPathProvider::Compare(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2DictionaryKeyPathProvider_Compare_m58D394FA139F2C08CEF59F831E64FB9D42EC8B29 (void);
// 0x000003EB UnityEngine.Vector2 Sirenix.Serialization.Vector2DictionaryKeyPathProvider::GetKeyFromPathString(System.String)
extern void Vector2DictionaryKeyPathProvider_GetKeyFromPathString_mA4A9A571EB0005B4BCBB871F18C22AF6661823A1 (void);
// 0x000003EC System.String Sirenix.Serialization.Vector2DictionaryKeyPathProvider::GetPathStringFromKey(UnityEngine.Vector2)
extern void Vector2DictionaryKeyPathProvider_GetPathStringFromKey_m54D8ABB043ACD0679F2D2F19415862345203046B (void);
// 0x000003ED System.Void Sirenix.Serialization.Vector2DictionaryKeyPathProvider::.ctor()
extern void Vector2DictionaryKeyPathProvider__ctor_m061EE5991CA2444B17EAFB3CED11A253BA2F914C (void);
// 0x000003EE System.String Sirenix.Serialization.Vector3DictionaryKeyPathProvider::get_ProviderID()
extern void Vector3DictionaryKeyPathProvider_get_ProviderID_mBFC57D55097DEAF73CE14A8BC6A155E5CEFC9C4D (void);
// 0x000003EF System.Int32 Sirenix.Serialization.Vector3DictionaryKeyPathProvider::Compare(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3DictionaryKeyPathProvider_Compare_m37837A32DFEADE4FFC5C7F655D183138FF79D21A (void);
// 0x000003F0 UnityEngine.Vector3 Sirenix.Serialization.Vector3DictionaryKeyPathProvider::GetKeyFromPathString(System.String)
extern void Vector3DictionaryKeyPathProvider_GetKeyFromPathString_m2E709C1620555D6F1251B21152FA0875D9D8123F (void);
// 0x000003F1 System.String Sirenix.Serialization.Vector3DictionaryKeyPathProvider::GetPathStringFromKey(UnityEngine.Vector3)
extern void Vector3DictionaryKeyPathProvider_GetPathStringFromKey_m4A9EBE2A0C7AFDA5B66A00635D558FEFDF2DA670 (void);
// 0x000003F2 System.Void Sirenix.Serialization.Vector3DictionaryKeyPathProvider::.ctor()
extern void Vector3DictionaryKeyPathProvider__ctor_m14616B5DAE3BDB8CFD5916AC53DD90B07397F92F (void);
// 0x000003F3 System.String Sirenix.Serialization.Vector4DictionaryKeyPathProvider::get_ProviderID()
extern void Vector4DictionaryKeyPathProvider_get_ProviderID_m74BF0B3042719851DB7EA4EFE24A276AE16F68C3 (void);
// 0x000003F4 System.Int32 Sirenix.Serialization.Vector4DictionaryKeyPathProvider::Compare(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Vector4DictionaryKeyPathProvider_Compare_mC0AEE68D5D51068275686663804DAC700063EDB1 (void);
// 0x000003F5 UnityEngine.Vector4 Sirenix.Serialization.Vector4DictionaryKeyPathProvider::GetKeyFromPathString(System.String)
extern void Vector4DictionaryKeyPathProvider_GetKeyFromPathString_m495910FADAEAC571410E409389C10419F0328532 (void);
// 0x000003F6 System.String Sirenix.Serialization.Vector4DictionaryKeyPathProvider::GetPathStringFromKey(UnityEngine.Vector4)
extern void Vector4DictionaryKeyPathProvider_GetPathStringFromKey_mB658F24BE548B4434C27A082E53E7C8D54672AA6 (void);
// 0x000003F7 System.Void Sirenix.Serialization.Vector4DictionaryKeyPathProvider::.ctor()
extern void Vector4DictionaryKeyPathProvider__ctor_m8391BB09BA9097575E0B916513A63482D59879AB (void);
// 0x000003F8 System.Void Sirenix.Serialization.GradientAlphaKeyFormatter::Read(UnityEngine.GradientAlphaKey&,Sirenix.Serialization.IDataReader)
extern void GradientAlphaKeyFormatter_Read_mF48E175C75C4678EE568941D7E5C602860FE949B (void);
// 0x000003F9 System.Void Sirenix.Serialization.GradientAlphaKeyFormatter::Write(UnityEngine.GradientAlphaKey&,Sirenix.Serialization.IDataWriter)
extern void GradientAlphaKeyFormatter_Write_m408F06EA2C718FE38617AEBB516991597D1078E6 (void);
// 0x000003FA System.Void Sirenix.Serialization.GradientAlphaKeyFormatter::.ctor()
extern void GradientAlphaKeyFormatter__ctor_mC75B0E279AC3589B51D27039E745550703043480 (void);
// 0x000003FB System.Void Sirenix.Serialization.GradientAlphaKeyFormatter::.cctor()
extern void GradientAlphaKeyFormatter__cctor_m49D611FD80B31502D4E8834F1E3E884A28BA2E8A (void);
// 0x000003FC System.Void Sirenix.Serialization.GradientColorKeyFormatter::Read(UnityEngine.GradientColorKey&,Sirenix.Serialization.IDataReader)
extern void GradientColorKeyFormatter_Read_mEF8ED09DE70500A651B108CA8EC4774B099FF79B (void);
// 0x000003FD System.Void Sirenix.Serialization.GradientColorKeyFormatter::Write(UnityEngine.GradientColorKey&,Sirenix.Serialization.IDataWriter)
extern void GradientColorKeyFormatter_Write_m10357B06628BBBE1C86A2B507A2EFFC71DD8F140 (void);
// 0x000003FE System.Void Sirenix.Serialization.GradientColorKeyFormatter::.ctor()
extern void GradientColorKeyFormatter__ctor_m0CADB69E108A0EEC3115D531D6E19E4D3659B693 (void);
// 0x000003FF System.Void Sirenix.Serialization.GradientColorKeyFormatter::.cctor()
extern void GradientColorKeyFormatter__cctor_m7755EF1FEDAD7589CFF022770A4FE7A21FFFD287 (void);
// 0x00000400 UnityEngine.Gradient Sirenix.Serialization.GradientFormatter::GetUninitializedObject()
extern void GradientFormatter_GetUninitializedObject_m505BBB6A15D5E2ECCE9298386C05D5AD5C1A174B (void);
// 0x00000401 System.Void Sirenix.Serialization.GradientFormatter::Read(UnityEngine.Gradient&,Sirenix.Serialization.IDataReader)
extern void GradientFormatter_Read_mCAF47D3960C2F901311B1550B10E5238DEB8E8B1 (void);
// 0x00000402 System.Void Sirenix.Serialization.GradientFormatter::Write(UnityEngine.Gradient&,Sirenix.Serialization.IDataWriter)
extern void GradientFormatter_Write_mD7DCF6C940F73905331CCAB256F17E74C2DD6F2F (void);
// 0x00000403 System.Void Sirenix.Serialization.GradientFormatter::.ctor()
extern void GradientFormatter__ctor_m80B772E9FF808CE84F6532F5224D9FDCABB0E41F (void);
// 0x00000404 System.Void Sirenix.Serialization.GradientFormatter::.cctor()
extern void GradientFormatter__cctor_mDD3B6DFC9B6E2967DF555436F544DCB5322131B6 (void);
// 0x00000405 Sirenix.Serialization.DataFormat Sirenix.Serialization.IOverridesSerializationFormat::GetFormatToSerializeAs(System.Boolean)
// 0x00000406 Sirenix.Serialization.SerializationData Sirenix.Serialization.ISupportsPrefabSerialization::get_SerializationData()
// 0x00000407 System.Void Sirenix.Serialization.ISupportsPrefabSerialization::set_SerializationData(Sirenix.Serialization.SerializationData)
// 0x00000408 System.Void Sirenix.Serialization.KeyframeFormatter::.cctor()
extern void KeyframeFormatter__cctor_m2CE8A00AA320842FE51168A3B2EFB78FEB103C2D (void);
// 0x00000409 System.Void Sirenix.Serialization.KeyframeFormatter::Read(UnityEngine.Keyframe&,Sirenix.Serialization.IDataReader)
extern void KeyframeFormatter_Read_mA05CDE0698AB00770915AB1160DC109E36EA21C9 (void);
// 0x0000040A System.Void Sirenix.Serialization.KeyframeFormatter::Write(UnityEngine.Keyframe&,Sirenix.Serialization.IDataWriter)
extern void KeyframeFormatter_Write_m77DD03FFF6893257A03EA2D50D97123B1BAE080C (void);
// 0x0000040B System.Void Sirenix.Serialization.KeyframeFormatter::.ctor()
extern void KeyframeFormatter__ctor_mA435A99D963FBC45A61889887176D30590DCECF7 (void);
// 0x0000040C System.Void Sirenix.Serialization.LayerMaskFormatter::Read(UnityEngine.LayerMask&,Sirenix.Serialization.IDataReader)
extern void LayerMaskFormatter_Read_m50982A46C8504F92FCAD146A94B211A765428FC9 (void);
// 0x0000040D System.Void Sirenix.Serialization.LayerMaskFormatter::Write(UnityEngine.LayerMask&,Sirenix.Serialization.IDataWriter)
extern void LayerMaskFormatter_Write_mCC5CA9EA068B0139316DC4BBBB39C9994828957A (void);
// 0x0000040E System.Void Sirenix.Serialization.LayerMaskFormatter::.ctor()
extern void LayerMaskFormatter__ctor_m02DF1C3BDF0D5ACBF304C0F8FE3BEF062314A3A3 (void);
// 0x0000040F System.Void Sirenix.Serialization.LayerMaskFormatter::.cctor()
extern void LayerMaskFormatter__cctor_m91C1A58C62C111E404F874776F3B786930672312 (void);
// 0x00000410 System.Void Sirenix.Serialization.QuaternionFormatter::Read(UnityEngine.Quaternion&,Sirenix.Serialization.IDataReader)
extern void QuaternionFormatter_Read_m8AF9FBFEDFFE5671683D04F614EB0FDFDC03754A (void);
// 0x00000411 System.Void Sirenix.Serialization.QuaternionFormatter::Write(UnityEngine.Quaternion&,Sirenix.Serialization.IDataWriter)
extern void QuaternionFormatter_Write_m5724E371EEEAED26B37D6D0BFB492164A51BF0CC (void);
// 0x00000412 System.Void Sirenix.Serialization.QuaternionFormatter::.ctor()
extern void QuaternionFormatter__ctor_mD02B4C8202E83C78C28E8D0C3675F2E0C0A7CE80 (void);
// 0x00000413 System.Void Sirenix.Serialization.QuaternionFormatter::.cctor()
extern void QuaternionFormatter__cctor_m7ED0437F19AA4D7A7F510CC105B8346B02C4D16C (void);
// 0x00000414 System.Void Sirenix.Serialization.RectFormatter::Read(UnityEngine.Rect&,Sirenix.Serialization.IDataReader)
extern void RectFormatter_Read_m459C3D86CD61B465F5888F64896D6E5B1812D09A (void);
// 0x00000415 System.Void Sirenix.Serialization.RectFormatter::Write(UnityEngine.Rect&,Sirenix.Serialization.IDataWriter)
extern void RectFormatter_Write_m57C1916CE23F6CF5050061CAB69C479224C8254B (void);
// 0x00000416 System.Void Sirenix.Serialization.RectFormatter::.ctor()
extern void RectFormatter__ctor_m619F4F0094FB16ED8D0A8BEE8F267E72B0FB5A69 (void);
// 0x00000417 System.Void Sirenix.Serialization.RectFormatter::.cctor()
extern void RectFormatter__cctor_m6F8E5B5931EA36EE393F6ABDE86819306D962DC4 (void);
// 0x00000418 System.Boolean Sirenix.Serialization.SerializationData::get_HasEditorData()
extern void SerializationData_get_HasEditorData_m8A28A607C9FA81619AB350C707110080E2BF28DE_AdjustorThunk (void);
// 0x00000419 System.Boolean Sirenix.Serialization.SerializationData::get_ContainsData()
extern void SerializationData_get_ContainsData_mD5C03579573CD9160DBA710C64061F7112336678_AdjustorThunk (void);
// 0x0000041A System.Void Sirenix.Serialization.SerializationData::Reset()
extern void SerializationData_Reset_mB8D787047F63FF9C15B6ACBAAB2518854F9D1A4D_AdjustorThunk (void);
// 0x0000041B T Sirenix.Serialization.UnityEventFormatter`1::GetUninitializedObject()
// 0x0000041C System.Void Sirenix.Serialization.UnityEventFormatter`1::.ctor()
// 0x0000041D System.Void Sirenix.Serialization.UnityReferenceResolver::.ctor()
extern void UnityReferenceResolver__ctor_mCEDD3179CC9BB4CF843A1872248EF590FC5030DD (void);
// 0x0000041E System.Void Sirenix.Serialization.UnityReferenceResolver::.ctor(System.Collections.Generic.List`1<UnityEngine.Object>)
extern void UnityReferenceResolver__ctor_m06ECB413A2FB7A3D7E034C7402E981238A24D01E (void);
// 0x0000041F System.Collections.Generic.List`1<UnityEngine.Object> Sirenix.Serialization.UnityReferenceResolver::GetReferencedUnityObjects()
extern void UnityReferenceResolver_GetReferencedUnityObjects_m9D7B548635FAC988774730EAC4214C981EB91E82 (void);
// 0x00000420 System.Void Sirenix.Serialization.UnityReferenceResolver::SetReferencedUnityObjects(System.Collections.Generic.List`1<UnityEngine.Object>)
extern void UnityReferenceResolver_SetReferencedUnityObjects_m91FF6B96A3B3734248DF7024BA780BA506D0A959 (void);
// 0x00000421 System.Boolean Sirenix.Serialization.UnityReferenceResolver::CanReference(System.Object,System.Int32&)
extern void UnityReferenceResolver_CanReference_mF307957AF758932397286AA2E81A971616211BF0 (void);
// 0x00000422 System.Boolean Sirenix.Serialization.UnityReferenceResolver::TryResolveReference(System.Int32,System.Object&)
extern void UnityReferenceResolver_TryResolveReference_m2C596E934C4453023616C14A4F8967FE11723168 (void);
// 0x00000423 System.Void Sirenix.Serialization.UnityReferenceResolver::Reset()
extern void UnityReferenceResolver_Reset_m180EF0D7A2EBEC85416D528B2CADE219D16B90E5 (void);
// 0x00000424 System.Void Sirenix.Serialization.UnityReferenceResolver::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnFreed()
extern void UnityReferenceResolver_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_m06D104F7EDFD529B95447225BF585201FA933CA1 (void);
// 0x00000425 System.Void Sirenix.Serialization.UnityReferenceResolver::Sirenix.Serialization.Utilities.ICacheNotificationReceiver.OnClaimed()
extern void UnityReferenceResolver_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_m0676D80FD6E65477FFE45F06EEE0213397FEC5F4 (void);
// 0x00000426 System.Boolean Sirenix.Serialization.UnitySerializationInitializer::get_Initialized()
extern void UnitySerializationInitializer_get_Initialized_m4418A5120E97720C7A1644E68B35E49F5D8E6A9D (void);
// 0x00000427 UnityEngine.RuntimePlatform Sirenix.Serialization.UnitySerializationInitializer::get_CurrentPlatform()
extern void UnitySerializationInitializer_get_CurrentPlatform_mF269C7AAC83FC078F94F1116119D796ECB5820EB (void);
// 0x00000428 System.Void Sirenix.Serialization.UnitySerializationInitializer::set_CurrentPlatform(UnityEngine.RuntimePlatform)
extern void UnitySerializationInitializer_set_CurrentPlatform_m55348AA2C668D1C114F90BB660ECC5382C0CD4F0 (void);
// 0x00000429 System.Void Sirenix.Serialization.UnitySerializationInitializer::Initialize()
extern void UnitySerializationInitializer_Initialize_m6A83084C82113DE884AAAF9F9470B438A61F69EE (void);
// 0x0000042A System.Void Sirenix.Serialization.UnitySerializationInitializer::InitializeRuntime()
extern void UnitySerializationInitializer_InitializeRuntime_mEC62F8B5E6AC16C44DAB3EFF5606F96ECFD1EB6B (void);
// 0x0000042B System.Void Sirenix.Serialization.UnitySerializationInitializer::InitializeEditor()
extern void UnitySerializationInitializer_InitializeEditor_mB74EA6BE4B9672EB2F6F81E4C503FFAED6F46C1F (void);
// 0x0000042C System.Void Sirenix.Serialization.UnitySerializationInitializer::.cctor()
extern void UnitySerializationInitializer__cctor_mE5935107BF6660DC62F6B73BC47F9BB6C92ABFE0 (void);
// 0x0000042D System.Void Sirenix.Serialization.Vector2Formatter::Read(UnityEngine.Vector2&,Sirenix.Serialization.IDataReader)
extern void Vector2Formatter_Read_m27AA5128534AB4AAA2FD75E135A31334A53CE863 (void);
// 0x0000042E System.Void Sirenix.Serialization.Vector2Formatter::Write(UnityEngine.Vector2&,Sirenix.Serialization.IDataWriter)
extern void Vector2Formatter_Write_mA19EF356CC9C452E0E621F6FBC48AE0F895D7894 (void);
// 0x0000042F System.Void Sirenix.Serialization.Vector2Formatter::.ctor()
extern void Vector2Formatter__ctor_mAF1B5664C6C5AD7FFFD936C977AF80BA6989D25B (void);
// 0x00000430 System.Void Sirenix.Serialization.Vector2Formatter::.cctor()
extern void Vector2Formatter__cctor_m2327CF76C0ABE77592F403F504A8EC7AC5699C2F (void);
// 0x00000431 System.Void Sirenix.Serialization.Vector3Formatter::Read(UnityEngine.Vector3&,Sirenix.Serialization.IDataReader)
extern void Vector3Formatter_Read_mC2D02E6D830BA62945AF7BB3A24B1C663363FDB7 (void);
// 0x00000432 System.Void Sirenix.Serialization.Vector3Formatter::Write(UnityEngine.Vector3&,Sirenix.Serialization.IDataWriter)
extern void Vector3Formatter_Write_m180053302E6B7B25BF3676354A93CF6E4D0C8B0C (void);
// 0x00000433 System.Void Sirenix.Serialization.Vector3Formatter::.ctor()
extern void Vector3Formatter__ctor_m48FA57AC400D6480911C623F348E553A0179A870 (void);
// 0x00000434 System.Void Sirenix.Serialization.Vector3Formatter::.cctor()
extern void Vector3Formatter__cctor_m6FA4C8ACA49CB97F4C7EE6D25537C40F1C21B9F0 (void);
// 0x00000435 System.Void Sirenix.Serialization.Vector4Formatter::Read(UnityEngine.Vector4&,Sirenix.Serialization.IDataReader)
extern void Vector4Formatter_Read_m78319BA03C8A34A7A3BF595C0D62C9EFE14FEF51 (void);
// 0x00000436 System.Void Sirenix.Serialization.Vector4Formatter::Write(UnityEngine.Vector4&,Sirenix.Serialization.IDataWriter)
extern void Vector4Formatter_Write_mB04AD604312209138439BB3BE5FC9C1000EE9B42 (void);
// 0x00000437 System.Void Sirenix.Serialization.Vector4Formatter::.ctor()
extern void Vector4Formatter__ctor_m17E07D3D65AA9200C2B61A0FF82253E4AE5C613F (void);
// 0x00000438 System.Void Sirenix.Serialization.Vector4Formatter::.cctor()
extern void Vector4Formatter__cctor_mFC6691CC4BC11DFB4D41E0534A00B071FB6F68BE (void);
// 0x00000439 System.Void Sirenix.Serialization.Buffer`1::.ctor(System.Int32)
// 0x0000043A System.Int32 Sirenix.Serialization.Buffer`1::get_Count()
// 0x0000043B T[] Sirenix.Serialization.Buffer`1::get_Array()
// 0x0000043C System.Boolean Sirenix.Serialization.Buffer`1::get_IsFree()
// 0x0000043D Sirenix.Serialization.Buffer`1<T> Sirenix.Serialization.Buffer`1::Claim(System.Int32)
// 0x0000043E System.Void Sirenix.Serialization.Buffer`1::Free(Sirenix.Serialization.Buffer`1<T>)
// 0x0000043F System.Void Sirenix.Serialization.Buffer`1::Free()
// 0x00000440 System.Void Sirenix.Serialization.Buffer`1::Dispose()
// 0x00000441 System.Int32 Sirenix.Serialization.Buffer`1::NextPowerOfTwo(System.Int32)
// 0x00000442 System.Void Sirenix.Serialization.Buffer`1::.cctor()
// 0x00000443 System.IO.MemoryStream Sirenix.Serialization.CachedMemoryStream::get_MemoryStream()
extern void CachedMemoryStream_get_MemoryStream_mC05F447C9F5077FE2A534BD50406CD86ACF53959 (void);
// 0x00000444 System.Void Sirenix.Serialization.CachedMemoryStream::.ctor()
extern void CachedMemoryStream__ctor_m0F8DB78A5C89C102E1B3D4ADB2EF3AF001D428CF (void);
// 0x00000445 System.Void Sirenix.Serialization.CachedMemoryStream::OnFreed()
extern void CachedMemoryStream_OnFreed_m49275EEF1AA830893D91EF570B08A25A906AFC80 (void);
// 0x00000446 System.Void Sirenix.Serialization.CachedMemoryStream::OnClaimed()
extern void CachedMemoryStream_OnClaimed_m4B0519D698DD4F79E92E7159DA8046BE209D6DB5 (void);
// 0x00000447 Sirenix.Serialization.Utilities.Cache`1<Sirenix.Serialization.CachedMemoryStream> Sirenix.Serialization.CachedMemoryStream::Claim(System.Int32)
extern void CachedMemoryStream_Claim_m158B6D5D546DFD260D7A7BDB0E7DC8A29B97945C (void);
// 0x00000448 Sirenix.Serialization.Utilities.Cache`1<Sirenix.Serialization.CachedMemoryStream> Sirenix.Serialization.CachedMemoryStream::Claim(System.Byte[])
extern void CachedMemoryStream_Claim_mD79A0F4F969E8A616EA76BD7EE76AFC43C3BDA2E (void);
// 0x00000449 System.Void Sirenix.Serialization.CachedMemoryStream::.cctor()
extern void CachedMemoryStream__cctor_m7A602030B2BE71C8DCF4ACE446DA737371E02A24 (void);
// 0x0000044A System.Void Sirenix.Serialization.EmittedAssemblyAttribute::.ctor()
extern void EmittedAssemblyAttribute__ctor_mF6C4FA2B76A0A7C473E38F0F3E4CF2D508FA05E7 (void);
// 0x0000044B System.Void Sirenix.Serialization.FormatterLocator::.cctor()
extern void FormatterLocator__cctor_mD132535014F04E03EF5B842D64A9197E387018F6 (void);
// 0x0000044C System.Void Sirenix.Serialization.FormatterLocator::add_FormatterResolve(System.Func`2<System.Type,Sirenix.Serialization.IFormatter>)
extern void FormatterLocator_add_FormatterResolve_mC3FC83CD4A004074E56AD2EFBFE07680D24A9D2C (void);
// 0x0000044D System.Void Sirenix.Serialization.FormatterLocator::remove_FormatterResolve(System.Func`2<System.Type,Sirenix.Serialization.IFormatter>)
extern void FormatterLocator_remove_FormatterResolve_m573DCACA76DC8BB72437A36C7DA8D57584179569 (void);
// 0x0000044E Sirenix.Serialization.IFormatter`1<T> Sirenix.Serialization.FormatterLocator::GetFormatter(Sirenix.Serialization.ISerializationPolicy)
// 0x0000044F Sirenix.Serialization.IFormatter Sirenix.Serialization.FormatterLocator::GetFormatter(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterLocator_GetFormatter_m609E20B144E94BA26F598167FC5205596A17E88F (void);
// 0x00000450 System.Void Sirenix.Serialization.FormatterLocator::LogAOTError(System.Type,System.Exception)
extern void FormatterLocator_LogAOTError_mC60C7F8A68389FA719BB057DA4F046FE0795ED09 (void);
// 0x00000451 System.Collections.Generic.IEnumerable`1<System.String> Sirenix.Serialization.FormatterLocator::GetAllPossibleMissingAOTTypes(System.Type)
extern void FormatterLocator_GetAllPossibleMissingAOTTypes_mFCB6F4A338DE6811EA8913468B1E8364E7D81608 (void);
// 0x00000452 System.Collections.Generic.List`1<Sirenix.Serialization.IFormatter> Sirenix.Serialization.FormatterLocator::GetAllCompatiblePredefinedFormatters(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterLocator_GetAllCompatiblePredefinedFormatters_m5DD9623720D6A31E69EDCD2504BFC708BF552D52 (void);
// 0x00000453 Sirenix.Serialization.IFormatter Sirenix.Serialization.FormatterLocator::CreateFormatter(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterLocator_CreateFormatter_m6D5588BC7C987C584B8C6EA03A229CE86394075F (void);
// 0x00000454 Sirenix.Serialization.IFormatter Sirenix.Serialization.FormatterLocator::GetFormatterInstance(System.Type)
extern void FormatterLocator_GetFormatterInstance_m2E950BB5E88B610A53747AFB348905F30DF20BDB (void);
// 0x00000455 System.Void Sirenix.Serialization.FormatterLocator/<>c::.cctor()
extern void U3CU3Ec__cctor_m33B43354C60ADD4CC37D2404604D02237EA3A225 (void);
// 0x00000456 System.Void Sirenix.Serialization.FormatterLocator/<>c::.ctor()
extern void U3CU3Ec__ctor_m9CDB9589FBD1AE85E33477B437CF954747F790B5 (void);
// 0x00000457 System.Int32 Sirenix.Serialization.FormatterLocator/<>c::<.cctor>b__7_0(Sirenix.Serialization.FormatterLocator/FormatterInfo,Sirenix.Serialization.FormatterLocator/FormatterInfo)
extern void U3CU3Ec_U3C_cctorU3Eb__7_0_mBF4C5A8955BE92DC9BD982576DD95C269B5E03E5 (void);
// 0x00000458 System.Int32 Sirenix.Serialization.FormatterLocator/<>c::<.cctor>b__7_1(Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo,Sirenix.Serialization.FormatterLocator/FormatterLocatorInfo)
extern void U3CU3Ec_U3C_cctorU3Eb__7_1_m746B3A9405CC4A3BD40DD58F0B2A4D26601F3CB5 (void);
// 0x00000459 System.Void Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::.ctor(System.Int32)
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14__ctor_m42E920DFA0EC2ED211ABF66260FC284F6C948795 (void);
// 0x0000045A System.Void Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.IDisposable.Dispose()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_IDisposable_Dispose_m213294EE73C9FD0BA4AE290DCFC4F05D8B0E372C (void);
// 0x0000045B System.Boolean Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::MoveNext()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_MoveNext_m0CEA908B3EB2E3E326B19F1ED09F4668D21A1A94 (void);
// 0x0000045C System.Void Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::<>m__Finally1()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_U3CU3Em__Finally1_m64E4B5F2C7CD7BFB1554DFC8F54AAD567E8F2DA2 (void);
// 0x0000045D System.String Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m9DE49E3A7D769B602A0780838785918940AA3CA9 (void);
// 0x0000045E System.Void Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.Collections.IEnumerator.Reset()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_Reset_m8F12030F2DE4AA1B634162D7F8E2B88FD1FCB392 (void);
// 0x0000045F System.Object Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_get_Current_m5D0857D4C63F8F25DE3DB3350C16B277AC731EBB (void);
// 0x00000460 System.Collections.Generic.IEnumerator`1<System.String> Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m120AA74EBA3617562BB5538F033FE57DE2E29276 (void);
// 0x00000461 System.Collections.IEnumerator Sirenix.Serialization.FormatterLocator/<GetAllPossibleMissingAOTTypes>d__14::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m9900CD20243E8E7059331DC685EE00C73F9AE343 (void);
// 0x00000462 System.Void Sirenix.Serialization.FormatterUtilities::.cctor()
extern void FormatterUtilities__cctor_m5A2CCD8F74CEDE7ECF626FDD3911EEE60978AD5F (void);
// 0x00000463 System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> Sirenix.Serialization.FormatterUtilities::GetSerializableMembersMap(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterUtilities_GetSerializableMembersMap_m2BCCD09DE91716C65861B3C987A01FECA09E5FD7 (void);
// 0x00000464 System.Reflection.MemberInfo[] Sirenix.Serialization.FormatterUtilities::GetSerializableMembers(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterUtilities_GetSerializableMembers_mCD7491A73EC6C46B532DDD2BDA55A8B2FAA6E756 (void);
// 0x00000465 UnityEngine.Object Sirenix.Serialization.FormatterUtilities::CreateUnityNull(System.Type,System.Type)
extern void FormatterUtilities_CreateUnityNull_m1C86CB4B3D79E840473A2E8D62C09CF61DDEF17D (void);
// 0x00000466 System.Boolean Sirenix.Serialization.FormatterUtilities::IsPrimitiveType(System.Type)
extern void FormatterUtilities_IsPrimitiveType_mC55C698DF301272897C5BACFBCFA1F5B6C8652DC (void);
// 0x00000467 System.Boolean Sirenix.Serialization.FormatterUtilities::IsPrimitiveArrayType(System.Type)
extern void FormatterUtilities_IsPrimitiveArrayType_m5EE44616BEE8895DCEC460C52633837DC1191649 (void);
// 0x00000468 System.Type Sirenix.Serialization.FormatterUtilities::GetContainedType(System.Reflection.MemberInfo)
extern void FormatterUtilities_GetContainedType_m3BF0E4E26DAFC2801F118FDF173FB4915AFD6D30 (void);
// 0x00000469 System.Object Sirenix.Serialization.FormatterUtilities::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern void FormatterUtilities_GetMemberValue_m446E6E6F36BA8FF104F0ED0C4B3DE517169FB025 (void);
// 0x0000046A System.Void Sirenix.Serialization.FormatterUtilities::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern void FormatterUtilities_SetMemberValue_mF72532625DF3C6C48A14F346DA590C04D68AC785 (void);
// 0x0000046B System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> Sirenix.Serialization.FormatterUtilities::FindSerializableMembersMap(System.Type,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterUtilities_FindSerializableMembersMap_m58D7BD4AE01CCE554F4B29DCAE4D6975631BFB81 (void);
// 0x0000046C System.Void Sirenix.Serialization.FormatterUtilities::FindSerializableMembers(System.Type,System.Collections.Generic.List`1<System.Reflection.MemberInfo>,Sirenix.Serialization.ISerializationPolicy)
extern void FormatterUtilities_FindSerializableMembers_m48AA97E98D1AEAFD5A7077E27C1883675C33C995 (void);
// 0x0000046D System.Reflection.MemberInfo Sirenix.Serialization.FormatterUtilities::GetPrivateMemberAlias(System.Reflection.MemberInfo,System.String,System.String)
extern void FormatterUtilities_GetPrivateMemberAlias_m7532AD47F334E60FFD55DE4DC66A0FE34B97F4B0 (void);
// 0x0000046E System.Boolean Sirenix.Serialization.FormatterUtilities::MemberIsPrivate(System.Reflection.MemberInfo)
extern void FormatterUtilities_MemberIsPrivate_m8E4B6B8B9DB4046DF9CCB322994B6E35D94F69F2 (void);
// 0x0000046F System.Void Sirenix.Serialization.FormatterUtilities/<>c::.cctor()
extern void U3CU3Ec__cctor_m16A2C04C7D05D2858A3F46E4CF367A9C349D5A3E (void);
// 0x00000470 System.Void Sirenix.Serialization.FormatterUtilities/<>c::.ctor()
extern void U3CU3Ec__ctor_mB0988998C15D2CF5DE01E73F0CD54A696A90B0A1 (void);
// 0x00000471 System.String Sirenix.Serialization.FormatterUtilities/<>c::<FindSerializableMembersMap>b__15_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CFindSerializableMembersMapU3Eb__15_0_m496274F2394DE778E9C03E4859FCBDE892F26DF7 (void);
// 0x00000472 System.Reflection.MemberInfo Sirenix.Serialization.FormatterUtilities/<>c::<FindSerializableMembersMap>b__15_1(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CFindSerializableMembersMapU3Eb__15_1_mBFD069C99D28E02AF9CB2331DE6C458175CA2649 (void);
// 0x00000473 System.Boolean Sirenix.Serialization.FormatterUtilities/<>c::<FindSerializableMembers>b__16_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CFindSerializableMembersU3Eb__16_0_m7346F8814E8A548032D203786563DB10522ADE26 (void);
// 0x00000474 System.Void Sirenix.Serialization.FormatterUtilities/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mDB4EEE19341B400C8BDED106E4A8EEF8D68689B7 (void);
// 0x00000475 System.Boolean Sirenix.Serialization.FormatterUtilities/<>c__DisplayClass16_0::<FindSerializableMembers>b__1(System.Reflection.MemberInfo)
extern void U3CU3Ec__DisplayClass16_0_U3CFindSerializableMembersU3Eb__1_m9CF5733277C55392CA8F8150BB3FE81DB1CBBE2E (void);
// 0x00000476 System.Void Sirenix.Serialization.DictionaryKeyUtility::.cctor()
extern void DictionaryKeyUtility__cctor_mF8E8576B0A2527ABA665AD497B591FDDED2B24FC (void);
// 0x00000477 System.Void Sirenix.Serialization.DictionaryKeyUtility::LogInvalidKeyPathProvider(System.Type,System.Reflection.Assembly,System.String)
extern void DictionaryKeyUtility_LogInvalidKeyPathProvider_mFC5107212E8DFCA4C01E4ADCFAA6E22DA360376B (void);
// 0x00000478 System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Serialization.DictionaryKeyUtility::GetPersistentPathKeyTypes()
extern void DictionaryKeyUtility_GetPersistentPathKeyTypes_m560066363F0240207A5EA9D08C2BD12C714893CD (void);
// 0x00000479 System.Boolean Sirenix.Serialization.DictionaryKeyUtility::KeyTypeSupportsPersistentPaths(System.Type)
extern void DictionaryKeyUtility_KeyTypeSupportsPersistentPaths_m80B8F6E8AD30B255BF135F56E2EA48F804DA0AFB (void);
// 0x0000047A System.Boolean Sirenix.Serialization.DictionaryKeyUtility::PrivateIsSupportedDictionaryKeyType(System.Type)
extern void DictionaryKeyUtility_PrivateIsSupportedDictionaryKeyType_m732CD72FE58A3EA7E6F60A6272521A26849F039D (void);
// 0x0000047B System.String Sirenix.Serialization.DictionaryKeyUtility::GetDictionaryKeyString(System.Object)
extern void DictionaryKeyUtility_GetDictionaryKeyString_m0DF429B9CE1CD1F5B35AE9161C5BE271F3A0B088 (void);
// 0x0000047C System.Object Sirenix.Serialization.DictionaryKeyUtility::GetDictionaryKeyValue(System.String,System.Type)
extern void DictionaryKeyUtility_GetDictionaryKeyValue_mA8E94C8276DF4CB6A92E78DE62F8FE5A222E097F (void);
// 0x0000047D System.String Sirenix.Serialization.DictionaryKeyUtility::FromTo(System.String,System.Int32,System.Int32)
extern void DictionaryKeyUtility_FromTo_mB0304339664BD0C90DEA2A7365EF3E633A78560C (void);
// 0x0000047E System.Int32 Sirenix.Serialization.DictionaryKeyUtility/UnityObjectKeyComparer`1::Compare(T,T)
// 0x0000047F System.Void Sirenix.Serialization.DictionaryKeyUtility/UnityObjectKeyComparer`1::.ctor()
// 0x00000480 System.Int32 Sirenix.Serialization.DictionaryKeyUtility/FallbackKeyComparer`1::Compare(T,T)
// 0x00000481 System.Void Sirenix.Serialization.DictionaryKeyUtility/FallbackKeyComparer`1::.ctor()
// 0x00000482 System.Void Sirenix.Serialization.DictionaryKeyUtility/KeyComparer`1::.ctor()
// 0x00000483 System.Int32 Sirenix.Serialization.DictionaryKeyUtility/KeyComparer`1::Compare(T,T)
// 0x00000484 System.Void Sirenix.Serialization.DictionaryKeyUtility/KeyComparer`1::.cctor()
// 0x00000485 System.Void Sirenix.Serialization.DictionaryKeyUtility/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m71E0F48CC9CF144F05702159EF4780DD61ED7C49 (void);
// 0x00000486 <>f__AnonymousType0`2<System.Reflection.Assembly,Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute> Sirenix.Serialization.DictionaryKeyUtility/<>c__DisplayClass12_0::<.cctor>b__1(System.Object)
extern void U3CU3Ec__DisplayClass12_0_U3C_cctorU3Eb__1_m8ED1C41C4EBC067991B75E57BBF9CA731348BCFB (void);
// 0x00000487 System.Void Sirenix.Serialization.DictionaryKeyUtility/<>c::.cctor()
extern void U3CU3Ec__cctor_mD1665EEB974332A5A23EE9819F7E6CDAA42537E0 (void);
// 0x00000488 System.Void Sirenix.Serialization.DictionaryKeyUtility/<>c::.ctor()
extern void U3CU3Ec__ctor_m81A9C227125D528E6ACD22AF39F1BCDD8F3D0A82 (void);
// 0x00000489 System.Collections.Generic.IEnumerable`1<<>f__AnonymousType0`2<System.Reflection.Assembly,Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute>> Sirenix.Serialization.DictionaryKeyUtility/<>c::<.cctor>b__12_0(System.Reflection.Assembly)
extern void U3CU3Ec_U3C_cctorU3Eb__12_0_m147EE29DA5957C137C52D24228B4998CD303DBFF (void);
// 0x0000048A System.Boolean Sirenix.Serialization.DictionaryKeyUtility/<>c::<.cctor>b__12_2(<>f__AnonymousType0`2<System.Reflection.Assembly,Sirenix.Serialization.RegisterDictionaryKeyPathProviderAttribute>)
extern void U3CU3Ec_U3C_cctorU3Eb__12_2_mE3B74A1EB0FE353A68D0537F31AA344A24DC8885 (void);
// 0x0000048B System.Void Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::.ctor(System.Int32)
extern void U3CGetPersistentPathKeyTypesU3Ed__14__ctor_m6FB68B817C2FDF39AC91F2875C7B1DE111D2A0AC (void);
// 0x0000048C System.Void Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.IDisposable.Dispose()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_IDisposable_Dispose_mB1F04B3D3BF5E0BD68489B69C56E1B7B3ACFB997 (void);
// 0x0000048D System.Boolean Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::MoveNext()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_MoveNext_m2A73997701AFAAC177C2A36A5BD0C0DC384D9067 (void);
// 0x0000048E System.Void Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::<>m__Finally1()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_U3CU3Em__Finally1_m4DE1C1D93EFF1438C396A77B1A5A322803987FC1 (void);
// 0x0000048F System.Void Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::<>m__Finally2()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_U3CU3Em__Finally2_m28CEA52B02EFB21808E5D57C32B540A7E382D13A (void);
// 0x00000490 System.Type Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m29217F8D66F77F53E359602C11B363F197DC05A6 (void);
// 0x00000491 System.Void Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.Collections.IEnumerator.Reset()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_Reset_mCAEC91788846F84F85BACA14A4340A852DC6FE97 (void);
// 0x00000492 System.Object Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_get_Current_m94C8A47343BA3A294EB02E6F7B29AC703DC1281F (void);
// 0x00000493 System.Collections.Generic.IEnumerator`1<System.Type> Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m06387F1609C0D44AAFDBF33F4B5BE1AEA8396E2C (void);
// 0x00000494 System.Collections.IEnumerator Sirenix.Serialization.DictionaryKeyUtility/<GetPersistentPathKeyTypes>d__14::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m5D3536FEF22563E69093808247E92FCC1BB339EB (void);
// 0x00000495 System.Void Sirenix.Serialization.PrefabModification::Apply(UnityEngine.Object)
extern void PrefabModification_Apply_m50F2AB4537A11A558F191DF8AC6BE55DD17CD78B (void);
// 0x00000496 System.Void Sirenix.Serialization.PrefabModification::ApplyValue(UnityEngine.Object)
extern void PrefabModification_ApplyValue_m997A47C824DA2CB9411E1E2514606085890329EE (void);
// 0x00000497 System.Void Sirenix.Serialization.PrefabModification::ApplyListLength(UnityEngine.Object)
extern void PrefabModification_ApplyListLength_m8A979152B4E7ACEA90F46016B64456B491F92E81 (void);
// 0x00000498 System.Void Sirenix.Serialization.PrefabModification::ApplyDictionaryModifications(UnityEngine.Object)
extern void PrefabModification_ApplyDictionaryModifications_mF127B6C88016C30B6F82838408DF247AC768F8F8 (void);
// 0x00000499 System.Void Sirenix.Serialization.PrefabModification::ReplaceAllReferencesInGraph(System.Object,System.Object,System.Object,System.Collections.Generic.HashSet`1<System.Object>)
extern void PrefabModification_ReplaceAllReferencesInGraph_m5C8F67CB9830E7F7CAAD8FD9401F6138740CDEB2 (void);
// 0x0000049A System.Object Sirenix.Serialization.PrefabModification::GetInstanceFromPath(System.String,System.Object)
extern void PrefabModification_GetInstanceFromPath_m34AB23216460B882191ADB80ED3BF3061720CC1E (void);
// 0x0000049B System.Object Sirenix.Serialization.PrefabModification::GetInstanceOfStep(System.String,System.Object)
extern void PrefabModification_GetInstanceOfStep_mA74FAAAA53AA5F6C23338B65052D7064559F315B (void);
// 0x0000049C System.Void Sirenix.Serialization.PrefabModification::SetInstanceToPath(System.String,System.Object,System.Object)
extern void PrefabModification_SetInstanceToPath_m317AAE5E04F9EE04DAC1CA6EDF9761B881BA790A (void);
// 0x0000049D System.Void Sirenix.Serialization.PrefabModification::SetInstanceToPath(System.String,System.String[],System.Int32,System.Object,System.Object,System.Boolean&)
extern void PrefabModification_SetInstanceToPath_m9B3428498928EBD20B51ACF2EAAD7912E15FC3B6 (void);
// 0x0000049E System.Boolean Sirenix.Serialization.PrefabModification::TrySetInstanceOfStep(System.String,System.Object,System.Object,System.Boolean&)
extern void PrefabModification_TrySetInstanceOfStep_mAF7357CFEBEDD84DECAF2AB16CDAA2988810D43B (void);
// 0x0000049F System.Void Sirenix.Serialization.PrefabModification::.ctor()
extern void PrefabModification__ctor_mA19ED9A0A426DA0353487216863B0201C001B61D (void);
// 0x000004A0 System.Void Sirenix.Serialization.PrefabModification/<>c::.cctor()
extern void U3CU3Ec__cctor_mC30CA7A75810343A709D31E717D8C615677EBD4C (void);
// 0x000004A1 System.Void Sirenix.Serialization.PrefabModification/<>c::.ctor()
extern void U3CU3Ec__ctor_m0BBA48B9BEA5AF74E3FE44ADBA4EF8C5757BE620 (void);
// 0x000004A2 System.Boolean Sirenix.Serialization.PrefabModification/<>c::<GetInstanceOfStep>b__13_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetInstanceOfStepU3Eb__13_0_m660409AEB311EADD7B4760E95A8F3D9309C39EE4 (void);
// 0x000004A3 System.Boolean Sirenix.Serialization.PrefabModification/<>c::<TrySetInstanceOfStep>b__16_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CTrySetInstanceOfStepU3Eb__16_0_m93A17FE11531E75B68553C8ACF804E82B14822C8 (void);
// 0x000004A4 System.UInt32[] Sirenix.Serialization.ProperBitConverter::CreateByteToHexLookup(System.Boolean)
extern void ProperBitConverter_CreateByteToHexLookup_m73BAB7275A34CAF1AE466D0DCDD43CAA4587C19F (void);
// 0x000004A5 System.String Sirenix.Serialization.ProperBitConverter::BytesToHexString(System.Byte[],System.Boolean)
extern void ProperBitConverter_BytesToHexString_mCA44F0644DA5A3625A15B93908FA864400367C63 (void);
// 0x000004A6 System.Byte[] Sirenix.Serialization.ProperBitConverter::HexStringToBytes(System.String)
extern void ProperBitConverter_HexStringToBytes_m491292DF0F1BAFD272608EAF26F54F1966C20B12 (void);
// 0x000004A7 System.Int16 Sirenix.Serialization.ProperBitConverter::ToInt16(System.Byte[],System.Int32)
extern void ProperBitConverter_ToInt16_mB4A349F3564E34D1105D4F7BFB798B2293BACF4C (void);
// 0x000004A8 System.UInt16 Sirenix.Serialization.ProperBitConverter::ToUInt16(System.Byte[],System.Int32)
extern void ProperBitConverter_ToUInt16_mAFD7797E67E36ACE512EF4B4429A72D1BA886C6E (void);
// 0x000004A9 System.Int32 Sirenix.Serialization.ProperBitConverter::ToInt32(System.Byte[],System.Int32)
extern void ProperBitConverter_ToInt32_m8BE08C53CB3E9E65686DECD4C0F5A9579E74CDBC (void);
// 0x000004AA System.UInt32 Sirenix.Serialization.ProperBitConverter::ToUInt32(System.Byte[],System.Int32)
extern void ProperBitConverter_ToUInt32_m83A12DBF4D4358D6FF8072C754915445ABA6EAEF (void);
// 0x000004AB System.Int64 Sirenix.Serialization.ProperBitConverter::ToInt64(System.Byte[],System.Int32)
extern void ProperBitConverter_ToInt64_m7784692E792DAB80CE47D8577C5A0F31A7BD9451 (void);
// 0x000004AC System.UInt64 Sirenix.Serialization.ProperBitConverter::ToUInt64(System.Byte[],System.Int32)
extern void ProperBitConverter_ToUInt64_m133360007C04BFED25A653F7C820D466B530CCAA (void);
// 0x000004AD System.Single Sirenix.Serialization.ProperBitConverter::ToSingle(System.Byte[],System.Int32)
extern void ProperBitConverter_ToSingle_m4F12B8A92122673D50B5DD6DC3846C9CB950DE06 (void);
// 0x000004AE System.Double Sirenix.Serialization.ProperBitConverter::ToDouble(System.Byte[],System.Int32)
extern void ProperBitConverter_ToDouble_m26A37A3C763D9BE7FE68D2B0EDCF59FD2E18151E (void);
// 0x000004AF System.Decimal Sirenix.Serialization.ProperBitConverter::ToDecimal(System.Byte[],System.Int32)
extern void ProperBitConverter_ToDecimal_m7D62AED65A3B88AE363DD6661040B232AF846827 (void);
// 0x000004B0 System.Guid Sirenix.Serialization.ProperBitConverter::ToGuid(System.Byte[],System.Int32)
extern void ProperBitConverter_ToGuid_m6959EB7BC7985732ABC48833CEB9ABEDB51C7266 (void);
// 0x000004B1 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Int16)
extern void ProperBitConverter_GetBytes_mE978BA7F62BCA44C1EBBF9C968417210867C3CC7 (void);
// 0x000004B2 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.UInt16)
extern void ProperBitConverter_GetBytes_mAD9689C32EE5A59909B76F70AD8FDF66C19F4C01 (void);
// 0x000004B3 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Int32)
extern void ProperBitConverter_GetBytes_mDEE37B79F68B223FCFEAEAD833072188D7E03354 (void);
// 0x000004B4 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.UInt32)
extern void ProperBitConverter_GetBytes_m1E9E7ECFF7C7F84DE0DCAAF507C22D707B2504FD (void);
// 0x000004B5 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Int64)
extern void ProperBitConverter_GetBytes_m14F078D09B264CD93C1F20E6383D6CA0A372826F (void);
// 0x000004B6 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.UInt64)
extern void ProperBitConverter_GetBytes_mABE0323AA667F14644577991420B6570C749B2A4 (void);
// 0x000004B7 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Single)
extern void ProperBitConverter_GetBytes_m532BCCAB14E0E176B6ECCC7BD2BF59B2F77B59D9 (void);
// 0x000004B8 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Double)
extern void ProperBitConverter_GetBytes_m3AAC5768F1CD7B25425293474DB2BB60D82D2B22 (void);
// 0x000004B9 System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Decimal)
extern void ProperBitConverter_GetBytes_m1AC3431F2B260C60D34C8CB79DEF31DCA6E42235 (void);
// 0x000004BA System.Void Sirenix.Serialization.ProperBitConverter::GetBytes(System.Byte[],System.Int32,System.Guid)
extern void ProperBitConverter_GetBytes_mF5EA86094F8CCCEC87B96D51ADD6E3F2B3E7D1DE (void);
// 0x000004BB System.Void Sirenix.Serialization.ProperBitConverter::.cctor()
extern void ProperBitConverter__cctor_m7E7AFF56468F153499C1F7F4E7D4175350501469 (void);
// 0x000004BC Sirenix.Serialization.IDataWriter Sirenix.Serialization.SerializationUtility::CreateWriter(System.IO.Stream,Sirenix.Serialization.SerializationContext,Sirenix.Serialization.DataFormat)
extern void SerializationUtility_CreateWriter_m0B0E8B5B8026730D7ADBEB22AC1E607F9739F9B7 (void);
// 0x000004BD Sirenix.Serialization.IDataReader Sirenix.Serialization.SerializationUtility::CreateReader(System.IO.Stream,Sirenix.Serialization.DeserializationContext,Sirenix.Serialization.DataFormat)
extern void SerializationUtility_CreateReader_mBECC352FC81147283B604C97755E35F918692C2B (void);
// 0x000004BE Sirenix.Serialization.IDataWriter Sirenix.Serialization.SerializationUtility::GetCachedWriter(System.IDisposable&,Sirenix.Serialization.DataFormat,System.IO.Stream,Sirenix.Serialization.SerializationContext)
extern void SerializationUtility_GetCachedWriter_mE97BBE490AE4B59E60821DA667004FD89A722088 (void);
// 0x000004BF Sirenix.Serialization.IDataReader Sirenix.Serialization.SerializationUtility::GetCachedReader(System.IDisposable&,Sirenix.Serialization.DataFormat,System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void SerializationUtility_GetCachedReader_m7F45574BE654EF4F27D1B5871ADE45E4DF1455D8 (void);
// 0x000004C0 System.Void Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,Sirenix.Serialization.IDataWriter)
extern void SerializationUtility_SerializeValueWeak_m7D035F5301ABAC60A42B68CD58510920226F9315 (void);
// 0x000004C1 System.Void Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,Sirenix.Serialization.IDataWriter,System.Collections.Generic.List`1<UnityEngine.Object>&)
extern void SerializationUtility_SerializeValueWeak_m28F328CE947B3828F637B46CE2B86B5D69EB04C4 (void);
// 0x000004C2 System.Void Sirenix.Serialization.SerializationUtility::SerializeValue(T,Sirenix.Serialization.IDataWriter)
// 0x000004C3 System.Void Sirenix.Serialization.SerializationUtility::SerializeValue(T,Sirenix.Serialization.IDataWriter,System.Collections.Generic.List`1<UnityEngine.Object>&)
// 0x000004C4 System.Void Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,System.IO.Stream,Sirenix.Serialization.DataFormat,Sirenix.Serialization.SerializationContext)
extern void SerializationUtility_SerializeValueWeak_m7E35D99AD9C14D0F71E1B3F6D3C1DDD14FC61CD0 (void);
// 0x000004C5 System.Void Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,System.IO.Stream,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.SerializationContext)
extern void SerializationUtility_SerializeValueWeak_mEE176F81E4D1ADED8BCAA3CDAA330CDA72EFBDC2 (void);
// 0x000004C6 System.Void Sirenix.Serialization.SerializationUtility::SerializeValue(T,System.IO.Stream,Sirenix.Serialization.DataFormat,Sirenix.Serialization.SerializationContext)
// 0x000004C7 System.Void Sirenix.Serialization.SerializationUtility::SerializeValue(T,System.IO.Stream,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.SerializationContext)
// 0x000004C8 System.Byte[] Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,Sirenix.Serialization.DataFormat,Sirenix.Serialization.SerializationContext)
extern void SerializationUtility_SerializeValueWeak_mA0AF8480E49272D593BEAC919CB6D7AB5476B440 (void);
// 0x000004C9 System.Byte[] Sirenix.Serialization.SerializationUtility::SerializeValueWeak(System.Object,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>&)
extern void SerializationUtility_SerializeValueWeak_m1370CD3DFAC7DE3F2188F780682183F6D27E3023 (void);
// 0x000004CA System.Byte[] Sirenix.Serialization.SerializationUtility::SerializeValue(T,Sirenix.Serialization.DataFormat,Sirenix.Serialization.SerializationContext)
// 0x000004CB System.Byte[] Sirenix.Serialization.SerializationUtility::SerializeValue(T,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.SerializationContext)
// 0x000004CC System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(Sirenix.Serialization.IDataReader)
extern void SerializationUtility_DeserializeValueWeak_m13B497C4E199A82EAA33CE5D71FE666525BFB280 (void);
// 0x000004CD System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(Sirenix.Serialization.IDataReader,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void SerializationUtility_DeserializeValueWeak_mE8D9AC2BF9E8237C4ADD6CD29F218DA925DC3BA6 (void);
// 0x000004CE T Sirenix.Serialization.SerializationUtility::DeserializeValue(Sirenix.Serialization.IDataReader)
// 0x000004CF T Sirenix.Serialization.SerializationUtility::DeserializeValue(Sirenix.Serialization.IDataReader,System.Collections.Generic.List`1<UnityEngine.Object>)
// 0x000004D0 System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(System.IO.Stream,Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
extern void SerializationUtility_DeserializeValueWeak_m38A9FE35D945F0B959EAE82BF973BAC141072070 (void);
// 0x000004D1 System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(System.IO.Stream,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>,Sirenix.Serialization.DeserializationContext)
extern void SerializationUtility_DeserializeValueWeak_mBB71201C1A189308509BE9B8C8F4FA6E20C9D205 (void);
// 0x000004D2 T Sirenix.Serialization.SerializationUtility::DeserializeValue(System.IO.Stream,Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
// 0x000004D3 T Sirenix.Serialization.SerializationUtility::DeserializeValue(System.IO.Stream,Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>,Sirenix.Serialization.DeserializationContext)
// 0x000004D4 System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(System.Byte[],Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
extern void SerializationUtility_DeserializeValueWeak_m2F958811917E12829411D0C1117A85E6A8B9F706 (void);
// 0x000004D5 System.Object Sirenix.Serialization.SerializationUtility::DeserializeValueWeak(System.Byte[],Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void SerializationUtility_DeserializeValueWeak_m9703E9A794B28C64FBF59B9E75D83C66CD495B27 (void);
// 0x000004D6 T Sirenix.Serialization.SerializationUtility::DeserializeValue(System.Byte[],Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
// 0x000004D7 T Sirenix.Serialization.SerializationUtility::DeserializeValue(System.Byte[],Sirenix.Serialization.DataFormat,System.Collections.Generic.List`1<UnityEngine.Object>,Sirenix.Serialization.DeserializationContext)
// 0x000004D8 System.Object Sirenix.Serialization.SerializationUtility::CreateCopy(System.Object)
extern void SerializationUtility_CreateCopy_m212DA679FFFAC362DE044003A6010A7142B6DF8C (void);
// 0x000004D9 System.Boolean Sirenix.Serialization.UnitySerializationUtility::OdinWillSerialize(System.Reflection.MemberInfo,System.Boolean,Sirenix.Serialization.ISerializationPolicy)
extern void UnitySerializationUtility_OdinWillSerialize_mE90172AB6B9907F0BE025536B5AC60B716264FFC (void);
// 0x000004DA System.Boolean Sirenix.Serialization.UnitySerializationUtility::GuessIfUnityWillSerialize(System.Reflection.MemberInfo)
extern void UnitySerializationUtility_GuessIfUnityWillSerialize_m6687311F880826A5C23BDA1494CE723841691337 (void);
// 0x000004DB System.Boolean Sirenix.Serialization.UnitySerializationUtility::GuessIfUnityWillSerializePrivate(System.Reflection.MemberInfo)
extern void UnitySerializationUtility_GuessIfUnityWillSerializePrivate_m72FB37BEB41FFEF57F049AB763F761B5256CA84E (void);
// 0x000004DC System.Boolean Sirenix.Serialization.UnitySerializationUtility::GuessIfUnityWillSerialize(System.Type)
extern void UnitySerializationUtility_GuessIfUnityWillSerialize_m9387F8E930238A0C1514BA899467917BB6D37D5D (void);
// 0x000004DD System.Boolean Sirenix.Serialization.UnitySerializationUtility::GuessIfUnityWillSerializePrivate(System.Type)
extern void UnitySerializationUtility_GuessIfUnityWillSerializePrivate_m9BCFAED74EB8508A5AE2F462A8768AFD577F0580 (void);
// 0x000004DE System.Void Sirenix.Serialization.UnitySerializationUtility::SerializeUnityObject(UnityEngine.Object,Sirenix.Serialization.SerializationData&,System.Boolean,Sirenix.Serialization.SerializationContext)
extern void UnitySerializationUtility_SerializeUnityObject_mB794020F3D57DF255B0085BF10B44C9684B1F538 (void);
// 0x000004DF System.Void Sirenix.Serialization.UnitySerializationUtility::SerializeUnityObject(UnityEngine.Object,System.String&,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.DataFormat,System.Boolean,Sirenix.Serialization.SerializationContext)
extern void UnitySerializationUtility_SerializeUnityObject_mF73448F11561F4B1D7B949013B7C08713CCF04B7 (void);
// 0x000004E0 System.Void Sirenix.Serialization.UnitySerializationUtility::SerializeUnityObject(UnityEngine.Object,System.Byte[]&,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.DataFormat,System.Boolean,Sirenix.Serialization.SerializationContext)
extern void UnitySerializationUtility_SerializeUnityObject_m6EA3208B12D567A0C6CD636FEAC973B555831741 (void);
// 0x000004E1 System.Void Sirenix.Serialization.UnitySerializationUtility::SerializeUnityObject(UnityEngine.Object,Sirenix.Serialization.IDataWriter,System.Boolean)
extern void UnitySerializationUtility_SerializeUnityObject_m362DF467DE558E8A64018EBAA86BCA0A98EF9526 (void);
// 0x000004E2 System.Void Sirenix.Serialization.UnitySerializationUtility::DeserializeUnityObject(UnityEngine.Object,Sirenix.Serialization.SerializationData&,Sirenix.Serialization.DeserializationContext)
extern void UnitySerializationUtility_DeserializeUnityObject_m36F8BD16920F3F3F20FF9FB4A4941AAB10FFA027 (void);
// 0x000004E3 System.Void Sirenix.Serialization.UnitySerializationUtility::DeserializeUnityObject(UnityEngine.Object,Sirenix.Serialization.SerializationData&,Sirenix.Serialization.DeserializationContext,System.Boolean,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void UnitySerializationUtility_DeserializeUnityObject_m44191A48C594565DFC2E10CCBD9ABEF87BFDEDAB (void);
// 0x000004E4 System.Void Sirenix.Serialization.UnitySerializationUtility::DeserializeUnityObject(UnityEngine.Object,System.String&,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
extern void UnitySerializationUtility_DeserializeUnityObject_m09D1ED8ACFB43987C4E04959A7C6772C12A27D22 (void);
// 0x000004E5 System.Void Sirenix.Serialization.UnitySerializationUtility::DeserializeUnityObject(UnityEngine.Object,System.Byte[]&,System.Collections.Generic.List`1<UnityEngine.Object>&,Sirenix.Serialization.DataFormat,Sirenix.Serialization.DeserializationContext)
extern void UnitySerializationUtility_DeserializeUnityObject_m1D6B0F4C63C3F5D173BE03CDD74B491F140921D9 (void);
// 0x000004E6 System.Void Sirenix.Serialization.UnitySerializationUtility::DeserializeUnityObject(UnityEngine.Object,Sirenix.Serialization.IDataReader)
extern void UnitySerializationUtility_DeserializeUnityObject_mCC0E99733A203933B95204B3EBAA97A05478DEA0 (void);
// 0x000004E7 System.Collections.Generic.List`1<System.String> Sirenix.Serialization.UnitySerializationUtility::SerializePrefabModifications(System.Collections.Generic.List`1<Sirenix.Serialization.PrefabModification>,System.Collections.Generic.List`1<UnityEngine.Object>&)
extern void UnitySerializationUtility_SerializePrefabModifications_m1263B5E48150E7E9C70087AD05509C7A92C73743 (void);
// 0x000004E8 System.String Sirenix.Serialization.UnitySerializationUtility::GetStringFromStreamAndReset(System.IO.Stream)
extern void UnitySerializationUtility_GetStringFromStreamAndReset_m0D34284A06058E881DF3032EE46582CA70208478 (void);
// 0x000004E9 System.Collections.Generic.List`1<Sirenix.Serialization.PrefabModification> Sirenix.Serialization.UnitySerializationUtility::DeserializePrefabModifications(System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void UnitySerializationUtility_DeserializePrefabModifications_m5B39A81AEC7EE189907F07FE071676577BFAB3A0 (void);
// 0x000004EA System.Object Sirenix.Serialization.UnitySerializationUtility::CreateDefaultUnityInitializedObject(System.Type)
extern void UnitySerializationUtility_CreateDefaultUnityInitializedObject_m55F8A571B3754DD32678FD49C784B8C3545BA5EA (void);
// 0x000004EB System.Object Sirenix.Serialization.UnitySerializationUtility::CreateDefaultUnityInitializedObject(System.Type,System.Int32)
extern void UnitySerializationUtility_CreateDefaultUnityInitializedObject_mDD21877EC5B40D501F5D6145884B303D4AC6A606 (void);
// 0x000004EC System.Void Sirenix.Serialization.UnitySerializationUtility::ApplyPrefabModifications(UnityEngine.Object,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void UnitySerializationUtility_ApplyPrefabModifications_m73843E56AB2802B63B3944CADAA37EE955CAB300 (void);
// 0x000004ED Sirenix.Serialization.Utilities.WeakValueGetter Sirenix.Serialization.UnitySerializationUtility::GetCachedUnityMemberGetter(System.Reflection.MemberInfo)
extern void UnitySerializationUtility_GetCachedUnityMemberGetter_m4AB3E52A12739A7B7BE2C168B54419FB4BB87EE4 (void);
// 0x000004EE Sirenix.Serialization.Utilities.WeakValueSetter Sirenix.Serialization.UnitySerializationUtility::GetCachedUnityMemberSetter(System.Reflection.MemberInfo)
extern void UnitySerializationUtility_GetCachedUnityMemberSetter_m61BF3D74031792D57B15F74F53425A62755B763A (void);
// 0x000004EF Sirenix.Serialization.IDataWriter Sirenix.Serialization.UnitySerializationUtility::GetCachedUnityWriter(Sirenix.Serialization.DataFormat,System.IO.Stream,Sirenix.Serialization.SerializationContext)
extern void UnitySerializationUtility_GetCachedUnityWriter_m1B196B40E451BA415140BF9A8C84F6DC38FD6C60 (void);
// 0x000004F0 Sirenix.Serialization.IDataReader Sirenix.Serialization.UnitySerializationUtility::GetCachedUnityReader(Sirenix.Serialization.DataFormat,System.IO.Stream,Sirenix.Serialization.DeserializationContext)
extern void UnitySerializationUtility_GetCachedUnityReader_mA26B230A1666802055916E1C2A7B82D1A24D23A0 (void);
// 0x000004F1 System.Void Sirenix.Serialization.UnitySerializationUtility::.cctor()
extern void UnitySerializationUtility__cctor_m2D14638BF3A5E75A2686C76936DDDE7B5CE8BFB1 (void);
// 0x000004F2 System.Void Sirenix.Serialization.UnitySerializationUtility/<>c::.cctor()
extern void U3CU3Ec__cctor_m575B5F84DE36F2439B8F3C602242410519BCDEAA (void);
// 0x000004F3 System.Void Sirenix.Serialization.UnitySerializationUtility/<>c::.ctor()
extern void U3CU3Ec__ctor_mBC550CEA47CB3379CB0F0989DA646DE538BD2206 (void);
// 0x000004F4 System.Int32 Sirenix.Serialization.UnitySerializationUtility/<>c::<SerializePrefabModifications>b__21_0(Sirenix.Serialization.PrefabModification,Sirenix.Serialization.PrefabModification)
extern void U3CU3Ec_U3CSerializePrefabModificationsU3Eb__21_0_mAD2D290899CBDC82733C24A5158E89A3E620B3D5 (void);
// 0x000004F5 System.Void Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m2395222375438043B6CF1FE712B31C151E507FF6 (void);
// 0x000004F6 System.Object Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass27_0::<GetCachedUnityMemberGetter>b__0(System.Object&)
extern void U3CU3Ec__DisplayClass27_0_U3CGetCachedUnityMemberGetterU3Eb__0_m005C84F2033684D22611E537144815B50ADCC842 (void);
// 0x000004F7 System.Void Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m72BE488A6C960069828129684FC60185F4587BD7 (void);
// 0x000004F8 System.Void Sirenix.Serialization.UnitySerializationUtility/<>c__DisplayClass28_0::<GetCachedUnityMemberSetter>b__0(System.Object&,System.Object)
extern void U3CU3Ec__DisplayClass28_0_U3CGetCachedUnityMemberSetterU3Eb__0_mEA1266310193A37416E30E570DE6D9AD41AC8DF4 (void);
// 0x000004F9 System.Boolean Sirenix.Serialization.Utilities.FieldInfoExtensions::IsAliasField(System.Reflection.FieldInfo)
extern void FieldInfoExtensions_IsAliasField_m1D38EAEE76FEF928BB693F75D5DFDEB70FAA1089 (void);
// 0x000004FA System.Reflection.FieldInfo Sirenix.Serialization.Utilities.FieldInfoExtensions::DeAliasField(System.Reflection.FieldInfo,System.Boolean)
extern void FieldInfoExtensions_DeAliasField_m1E13F279646C60998D6805F037E9BB465A732234 (void);
// 0x000004FB Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1<T> Sirenix.Serialization.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.List`1<T>)
// 0x000004FC Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2<T1,T2> Sirenix.Serialization.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x000004FD Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2<T1,T2> Sirenix.Serialization.Utilities.GarbageFreeIterators::GFValueIterator(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x000004FE Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1<T> Sirenix.Serialization.Utilities.GarbageFreeIterators::GFIterator(System.Collections.Generic.HashSet`1<T>)
// 0x000004FF System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1::.ctor(System.Collections.Generic.List`1<T>)
// 0x00000500 Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1<T> Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1::GetEnumerator()
// 0x00000501 T Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1::get_Current()
// 0x00000502 System.Boolean Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1::MoveNext()
// 0x00000503 System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/ListIterator`1::Dispose()
// 0x00000504 System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000505 Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1<T> Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1::GetEnumerator()
// 0x00000506 T Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1::get_Current()
// 0x00000507 System.Boolean Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1::MoveNext()
// 0x00000508 System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/HashsetIterator`1::Dispose()
// 0x00000509 System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2::.ctor(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x0000050A Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2<T1,T2> Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2::GetEnumerator()
// 0x0000050B System.Collections.Generic.KeyValuePair`2<T1,T2> Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2::get_Current()
// 0x0000050C System.Boolean Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2::MoveNext()
// 0x0000050D System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryIterator`2::Dispose()
// 0x0000050E System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::.ctor(System.Collections.Generic.Dictionary`2<T1,T2>)
// 0x0000050F Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2<T1,T2> Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::GetEnumerator()
// 0x00000510 T2 Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::get_Current()
// 0x00000511 System.Boolean Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::MoveNext()
// 0x00000512 System.Void Sirenix.Serialization.Utilities.GarbageFreeIterators/DictionaryValueIterator`2::Dispose()
// 0x00000513 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.LinqExtensions::ForEach(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x00000514 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.LinqExtensions::ForEach(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
// 0x00000515 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.LinqExtensions::Append(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000516 System.Void Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::.ctor(System.Int32)
// 0x00000517 System.Void Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.IDisposable.Dispose()
// 0x00000518 System.Boolean Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::MoveNext()
// 0x00000519 System.Void Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::<>m__Finally1()
// 0x0000051A System.Void Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::<>m__Finally2()
// 0x0000051B T Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000051C System.Void Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.Collections.IEnumerator.Reset()
// 0x0000051D System.Object Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x0000051E System.Collections.Generic.IEnumerator`1<T> Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000051F System.Collections.IEnumerator Sirenix.Serialization.Utilities.LinqExtensions/<Append>d__2`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000520 System.Boolean Sirenix.Serialization.Utilities.MemberInfoExtensions::IsDefined(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x00000521 System.Boolean Sirenix.Serialization.Utilities.MemberInfoExtensions::IsDefined(System.Reflection.ICustomAttributeProvider)
// 0x00000522 T Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttribute(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x00000523 T Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttribute(System.Reflection.ICustomAttributeProvider)
// 0x00000524 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider)
// 0x00000525 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
// 0x00000526 System.Attribute[] Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider)
extern void MemberInfoExtensions_GetAttributes_m349694041FCA1FF2377E50DB8AE258AD3139F5D5 (void);
// 0x00000527 System.Attribute[] Sirenix.Serialization.Utilities.MemberInfoExtensions::GetAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
extern void MemberInfoExtensions_GetAttributes_m5B61F53BDD320500874039F20CE82C780CB4E90A (void);
// 0x00000528 System.String Sirenix.Serialization.Utilities.MemberInfoExtensions::GetNiceName(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_GetNiceName_mCD6D786C0268CD6B40DCB737D5A8892369EF7E6D (void);
// 0x00000529 System.Boolean Sirenix.Serialization.Utilities.MemberInfoExtensions::IsStatic(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_IsStatic_m0D65779AE20FC116BC260AF93A9398FAA167B8FD (void);
// 0x0000052A System.Boolean Sirenix.Serialization.Utilities.MemberInfoExtensions::IsAlias(System.Reflection.MemberInfo)
extern void MemberInfoExtensions_IsAlias_m2008AF6C8F05D73C9A66C3261B677D850D6B6A2F (void);
// 0x0000052B System.Reflection.MemberInfo Sirenix.Serialization.Utilities.MemberInfoExtensions::DeAlias(System.Reflection.MemberInfo,System.Boolean)
extern void MemberInfoExtensions_DeAlias_m7A87BBC0F97D506668D2919E6FADE5FA3A04EB2F (void);
// 0x0000052C System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase,System.String)
extern void MethodInfoExtensions_GetFullName_m7289EF1866E240A4D8166835E23D870CAE19116E (void);
// 0x0000052D System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetParamsNames(System.Reflection.MethodBase)
extern void MethodInfoExtensions_GetParamsNames_m0BD5DB755CF0FB1222863A5A7C73BED573E1DF53 (void);
// 0x0000052E System.String Sirenix.Serialization.Utilities.MethodInfoExtensions::GetFullName(System.Reflection.MethodBase)
extern void MethodInfoExtensions_GetFullName_m16F11391FA8EB6B9AF3604D0D68DEE379A6E4169 (void);
// 0x0000052F System.Boolean Sirenix.Serialization.Utilities.MethodInfoExtensions::IsExtensionMethod(System.Reflection.MethodBase)
extern void MethodInfoExtensions_IsExtensionMethod_mCBF921ED9BCA898FDA5BC4B01670C47D86F0CB1C (void);
// 0x00000530 System.Boolean Sirenix.Serialization.Utilities.MethodInfoExtensions::IsAliasMethod(System.Reflection.MethodInfo)
extern void MethodInfoExtensions_IsAliasMethod_mC735E84942B6AA66BE3CF2337C6EE77FA3F66257 (void);
// 0x00000531 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MethodInfoExtensions::DeAliasMethod(System.Reflection.MethodInfo,System.Boolean)
extern void MethodInfoExtensions_DeAliasMethod_mDE8989617E2D4C45BD200463E09A2ABCFD541A59 (void);
// 0x00000532 System.Boolean Sirenix.Serialization.Utilities.PathUtilities::HasSubDirectory(System.IO.DirectoryInfo,System.IO.DirectoryInfo)
extern void PathUtilities_HasSubDirectory_mA04B6A623ADEE2828CD0BD0C9A39E3699B0761B7 (void);
// 0x00000533 System.Boolean Sirenix.Serialization.Utilities.PropertyInfoExtensions::IsAutoProperty(System.Reflection.PropertyInfo,System.Boolean)
extern void PropertyInfoExtensions_IsAutoProperty_mF5E9A6C194E664F9556D51B8D09007F8EAACE91C (void);
// 0x00000534 System.Boolean Sirenix.Serialization.Utilities.PropertyInfoExtensions::IsAliasProperty(System.Reflection.PropertyInfo)
extern void PropertyInfoExtensions_IsAliasProperty_m87041E7971788AE6886143359B4B07B999125A2F (void);
// 0x00000535 System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.PropertyInfoExtensions::DeAliasProperty(System.Reflection.PropertyInfo,System.Boolean)
extern void PropertyInfoExtensions_DeAliasProperty_m015DB168AC8F90AA5143293D1A290A60CF55A570 (void);
// 0x00000536 System.String Sirenix.Serialization.Utilities.StringExtensions::ToTitleCase(System.String)
extern void StringExtensions_ToTitleCase_m75E36FAEAEB8CB6FC90F54B5B30BD5CC673DD0C6 (void);
// 0x00000537 System.Boolean Sirenix.Serialization.Utilities.StringExtensions::IsNullOrWhitespace(System.String)
extern void StringExtensions_IsNullOrWhitespace_mE7508CCAA9EA8CCC21D96229BD62C292539A1ED8 (void);
// 0x00000538 System.String Sirenix.Serialization.Utilities.TypeExtensions::GetCachedNiceName(System.Type)
extern void TypeExtensions_GetCachedNiceName_m1532573DEA6D23C49EC015558CC33328FF85F2AA (void);
// 0x00000539 System.String Sirenix.Serialization.Utilities.TypeExtensions::CreateNiceName(System.Type)
extern void TypeExtensions_CreateNiceName_mDBCF3CAC485C4CAC57A3F37B6B5ED4317FE7909D (void);
// 0x0000053A System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCastDefined(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_HasCastDefined_mFE16EE4FE9FF3852768497633F01D0E991C3BBED (void);
// 0x0000053B System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifier(System.String)
extern void TypeExtensions_IsValidIdentifier_m685A2493CA6F7C2AEF3C13EA3EEEAD612856C89C (void);
// 0x0000053C System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifierStartCharacter(System.Char)
extern void TypeExtensions_IsValidIdentifierStartCharacter_m0CB6EE49AB1C6A7037A00D84CE9EB6E08E81D83B (void);
// 0x0000053D System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsValidIdentifierPartCharacter(System.Char)
extern void TypeExtensions_IsValidIdentifierPartCharacter_m7AC59F411B66B097C9B05509D21AE43EBA3FFCC1 (void);
// 0x0000053E System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsCastableTo(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_IsCastableTo_m12EF9C5160625DEE41CB62C3D8F2D2E7D378D51F (void);
// 0x0000053F System.Func`2<System.Object,System.Object> Sirenix.Serialization.Utilities.TypeExtensions::GetCastMethodDelegate(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_GetCastMethodDelegate_m987046E7D47F282D654496261B7883C28D6D31DA (void);
// 0x00000540 System.Func`2<TFrom,TTo> Sirenix.Serialization.Utilities.TypeExtensions::GetCastMethodDelegate(System.Boolean)
// 0x00000541 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.TypeExtensions::GetCastMethod(System.Type,System.Type,System.Boolean)
extern void TypeExtensions_GetCastMethod_mD03313876F01CE5085BFDB60AE52D9A338439CA9 (void);
// 0x00000542 System.Func`3<T,T,System.Boolean> Sirenix.Serialization.Utilities.TypeExtensions::GetEqualityComparerDelegate()
// 0x00000543 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCustomAttribute(System.Reflection.MemberInfo)
// 0x00000544 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCustomAttribute(System.Reflection.MemberInfo,System.Boolean)
// 0x00000545 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCustomAttribute(System.Reflection.MemberInfo,T&)
// 0x00000546 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCustomAttribute(System.Reflection.MemberInfo,System.Boolean,T&)
// 0x00000547 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCustomAttribute(System.Reflection.MemberInfo,System.Type,System.Boolean)
extern void TypeExtensions_HasCustomAttribute_m807409621DE640D0570A47C2C7C93A23726B4918 (void);
// 0x00000548 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasCustomAttribute(System.Reflection.MemberInfo,System.Type,System.Boolean,System.Attribute&)
extern void TypeExtensions_HasCustomAttribute_m02DCC99327737793E7746DA3B4CCDECFAA6554ED (void);
// 0x00000549 T Sirenix.Serialization.Utilities.TypeExtensions::GetAttribute(System.Type,System.Boolean)
// 0x0000054A System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOrInherits(System.Type,System.Type)
extern void TypeExtensions_ImplementsOrInherits_mBB069DC34FD0A3D5A9AAE45FF5FCC86D3A0AC3B9 (void);
// 0x0000054B System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericType(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericType_m3FC6E38C111B4C1BF15919E393DE4258469E09E5 (void);
// 0x0000054C System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericInterface(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericInterface_m36B2221D9263FB90DF896EDE0C20F294D955572F (void);
// 0x0000054D System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::ImplementsOpenGenericClass(System.Type,System.Type)
extern void TypeExtensions_ImplementsOpenGenericClass_mEF109A8BA5BADD6C482009E891DCAB86DDC3CEB9 (void);
// 0x0000054E System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericType(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m6CB1DCD49EBE82C352539704EA4A74670B21C5C0 (void);
// 0x0000054F System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericClass(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_mD5EBA354E9E6A9607CFFEFCA448E418668E35056 (void);
// 0x00000550 System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::GetArgumentsOfInheritedOpenGenericInterface(System.Type,System.Type)
extern void TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m61744A5C5CC21963B53858F744CE2B557E15E30A (void);
// 0x00000551 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.TypeExtensions::GetOperatorMethod(System.Type,Sirenix.Serialization.Utilities.Operator)
extern void TypeExtensions_GetOperatorMethod_mAC2F5581188D79F35D91677D1A9054EF842BBC26 (void);
// 0x00000552 System.Reflection.MethodInfo[] Sirenix.Serialization.Utilities.TypeExtensions::GetOperatorMethods(System.Type,Sirenix.Serialization.Utilities.Operator)
extern void TypeExtensions_GetOperatorMethods_m6D139F72C9221E06D5F6DF0C21F0656C2947C4BD (void);
// 0x00000553 System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions::GetAllMembers(System.Type,System.Reflection.BindingFlags)
extern void TypeExtensions_GetAllMembers_mAF1EC9227C3CDA77A78B41600741BC22B282B330 (void);
// 0x00000554 System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions::GetAllMembers(System.Type,System.String,System.Reflection.BindingFlags)
extern void TypeExtensions_GetAllMembers_mD652499D27311B8B3E231622ECBC3223C8FEACA4 (void);
// 0x00000555 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.TypeExtensions::GetAllMembers(System.Type,System.Reflection.BindingFlags)
// 0x00000556 System.Type Sirenix.Serialization.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type)
extern void TypeExtensions_GetGenericBaseType_mFEF667CA3694CE4833C076C2465EC8BEC35856EA (void);
// 0x00000557 System.Type Sirenix.Serialization.Utilities.TypeExtensions::GetGenericBaseType(System.Type,System.Type,System.Int32&)
extern void TypeExtensions_GetGenericBaseType_mFB61C16F72798D16114C6F724FB868F9244345BC (void);
// 0x00000558 System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GetBaseTypes(System.Type,System.Boolean)
extern void TypeExtensions_GetBaseTypes_m983F94E0CFB953F1D94BC51BB7637490EE5DEB14 (void);
// 0x00000559 System.Collections.Generic.IEnumerable`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions::GetBaseClasses(System.Type,System.Boolean)
extern void TypeExtensions_GetBaseClasses_mDC132249680C2F2FC1D237587A00841A013F61FC (void);
// 0x0000055A System.String Sirenix.Serialization.Utilities.TypeExtensions::TypeNameGauntlet(System.Type)
extern void TypeExtensions_TypeNameGauntlet_mE817E158A4A383649188FC15D0B79E2663C6499F (void);
// 0x0000055B System.String Sirenix.Serialization.Utilities.TypeExtensions::GetNiceName(System.Type)
extern void TypeExtensions_GetNiceName_m5520AD5A39ABAB082F2DD30222BCADBE03CD21B6 (void);
// 0x0000055C System.String Sirenix.Serialization.Utilities.TypeExtensions::GetNiceFullName(System.Type)
extern void TypeExtensions_GetNiceFullName_mF291B98B75ECEB06240A18E2AA9BA2DE1F17A230 (void);
// 0x0000055D System.String Sirenix.Serialization.Utilities.TypeExtensions::GetCompilableNiceName(System.Type)
extern void TypeExtensions_GetCompilableNiceName_m4B236B397503B7C845545AF25337B7515CE1352C (void);
// 0x0000055E System.String Sirenix.Serialization.Utilities.TypeExtensions::GetCompilableNiceFullName(System.Type)
extern void TypeExtensions_GetCompilableNiceFullName_m2599EDF5418ECCACF2B4E3DF981E9CFFCCD0B9E0 (void);
// 0x0000055F T Sirenix.Serialization.Utilities.TypeExtensions::GetCustomAttribute(System.Type,System.Boolean)
// 0x00000560 T Sirenix.Serialization.Utilities.TypeExtensions::GetCustomAttribute(System.Type)
// 0x00000561 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.TypeExtensions::GetCustomAttributes(System.Type)
// 0x00000562 System.Collections.Generic.IEnumerable`1<T> Sirenix.Serialization.Utilities.TypeExtensions::GetCustomAttributes(System.Type,System.Boolean)
// 0x00000563 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsDefined(System.Type)
// 0x00000564 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsDefined(System.Type,System.Boolean)
// 0x00000565 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::InheritsFrom(System.Type)
// 0x00000566 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::InheritsFrom(System.Type,System.Type)
extern void TypeExtensions_InheritsFrom_mB2C8CCD4BC2F89D305E9E82CED3E6D078F6B1C9D (void);
// 0x00000567 System.Int32 Sirenix.Serialization.Utilities.TypeExtensions::GetInheritanceDistance(System.Type,System.Type)
extern void TypeExtensions_GetInheritanceDistance_m0520A54AE277190031955B26C48CF1C4793B7AF3 (void);
// 0x00000568 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::HasParamaters(System.Reflection.MethodInfo,System.Collections.Generic.IList`1<System.Type>,System.Boolean)
extern void TypeExtensions_HasParamaters_m8B781DA0243FDDE934633308D48B7006BB1393FF (void);
// 0x00000569 System.Type Sirenix.Serialization.Utilities.TypeExtensions::GetReturnType(System.Reflection.MemberInfo)
extern void TypeExtensions_GetReturnType_mE350B159933DEF36F13497D2648D1023927E0105 (void);
// 0x0000056A System.Object Sirenix.Serialization.Utilities.TypeExtensions::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern void TypeExtensions_GetMemberValue_mA49CC5ACB73A92E1559601E6BCB9B465FCABED36 (void);
// 0x0000056B System.Void Sirenix.Serialization.Utilities.TypeExtensions::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern void TypeExtensions_SetMemberValue_m535B84D7681E0D3ACD19641D7D7F6184C50D1DB7 (void);
// 0x0000056C System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::TryInferGenericParameters(System.Type,System.Type[]&,System.Type[])
extern void TypeExtensions_TryInferGenericParameters_m16A99D765E67EBE6F6441327D86682E736D00457 (void);
// 0x0000056D System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type,System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_m8A5E53288BFDFBAD31CE121DFCEBD72215CD3799 (void);
// 0x0000056E System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Reflection.MethodBase,System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_mF6CC8438959153139AB1EDEE8A68FF75C184C64F (void);
// 0x0000056F System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::AreGenericConstraintsSatisfiedBy(System.Type[],System.Type[])
extern void TypeExtensions_AreGenericConstraintsSatisfiedBy_mE8B388E4541BE5A94533E37107FCCF33F0B5F900 (void);
// 0x00000570 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type)
extern void TypeExtensions_GenericParameterIsFulfilledBy_m6CBD13619911088E757B92211349F616C9779E2D (void);
// 0x00000571 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::GenericParameterIsFulfilledBy(System.Type,System.Type,System.Collections.Generic.Dictionary`2<System.Type,System.Type>,System.Collections.Generic.HashSet`1<System.Type>)
extern void TypeExtensions_GenericParameterIsFulfilledBy_m2E2EE166F01A2BC075772E3AF0C462C13DAB3D23 (void);
// 0x00000572 System.String Sirenix.Serialization.Utilities.TypeExtensions::GetGenericConstraintsString(System.Type,System.Boolean)
extern void TypeExtensions_GetGenericConstraintsString_m235BDED19C12FF498E7BE5515FFFE62B3EFBC917 (void);
// 0x00000573 System.String Sirenix.Serialization.Utilities.TypeExtensions::GetGenericParameterConstraintsString(System.Type,System.Boolean)
extern void TypeExtensions_GetGenericParameterConstraintsString_m6150DD784FC1554DDCC958F42F821CA5D9631E5B (void);
// 0x00000574 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::GenericArgumentsContainsTypes(System.Type,System.Type[])
extern void TypeExtensions_GenericArgumentsContainsTypes_m8BC8C323DE7F8BDAE770B61B0FD7CAC68EF751FB (void);
// 0x00000575 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsFullyConstructedGenericType(System.Type)
extern void TypeExtensions_IsFullyConstructedGenericType_m9FA2CB4ECC617B95C129F22FBA6619B03786B236 (void);
// 0x00000576 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::IsNullableType(System.Type)
extern void TypeExtensions_IsNullableType_m9E8A5C8C3F6C473C6F086729A8966AA502A6AC5F (void);
// 0x00000577 System.UInt64 Sirenix.Serialization.Utilities.TypeExtensions::GetEnumBitmask(System.Object,System.Type)
extern void TypeExtensions_GetEnumBitmask_mF4BC3721F47A34A4E4A40D067F5A4A2A2483045D (void);
// 0x00000578 System.Type[] Sirenix.Serialization.Utilities.TypeExtensions::SafeGetTypes(System.Reflection.Assembly)
extern void TypeExtensions_SafeGetTypes_m3E480DDB012372630A76753275F4063BCBCA515D (void);
// 0x00000579 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions::SafeIsDefined(System.Reflection.Assembly,System.Type,System.Boolean)
extern void TypeExtensions_SafeIsDefined_mB09B7D31E0F9E2B39976557D61A5C5A7205D4F19 (void);
// 0x0000057A System.Object[] Sirenix.Serialization.Utilities.TypeExtensions::SafeGetCustomAttributes(System.Reflection.Assembly,System.Type,System.Boolean)
extern void TypeExtensions_SafeGetCustomAttributes_m38CC04731F22628B3292D1A49D5CE61C2673BB25 (void);
// 0x0000057B System.Void Sirenix.Serialization.Utilities.TypeExtensions::.cctor()
extern void TypeExtensions__cctor_mD461614546003B9003AA8F62C3C0146C1EEC0941 (void);
// 0x0000057C System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_mDDFEE96485F9BE0186CBDC36FE1C9C6511C759AE (void);
// 0x0000057D System.Object Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass22_0::<GetCastMethodDelegate>b__0(System.Object)
extern void U3CU3Ec__DisplayClass22_0_U3CGetCastMethodDelegateU3Eb__0_m64C3E77A88DACFAF3F12C7CA7FDAB97EDA92B5EF (void);
// 0x0000057E System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__25`1::.cctor()
// 0x0000057F System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__25`1::.ctor()
// 0x00000580 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__25`1::<GetEqualityComparerDelegate>b__25_0(System.Reflection.MethodInfo)
// 0x00000581 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__25`1::<GetEqualityComparerDelegate>b__25_1(UnityEngine.Quaternion,UnityEngine.Quaternion)
// 0x00000582 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__25`1::<GetEqualityComparerDelegate>b__25_2(T,T)
// 0x00000583 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__25`1::<GetEqualityComparerDelegate>b__25_3(T,T)
// 0x00000584 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_mD4D067CEB5C9592D987A9CCDB2289B028E858348 (void);
// 0x00000585 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass35_0::<ImplementsOpenGenericInterface>b__0(System.Type)
extern void U3CU3Ec__DisplayClass35_0_U3CImplementsOpenGenericInterfaceU3Eb__0_m0024B9BED2445A7118CCFA1F605CB3334B8C8E5E (void);
// 0x00000586 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_mAE9A9CEA217A2EFAF5057B26929D00E8E2B95858 (void);
// 0x00000587 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass40_0::<GetOperatorMethod>b__0(System.Reflection.MethodInfo)
extern void U3CU3Ec__DisplayClass40_0_U3CGetOperatorMethodU3Eb__0_m61D083358392C079B66AC313E2142095411E53A0 (void);
// 0x00000588 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_m3DE835180C4FFBDE2FDD604097FE5A9BFC8F87E6 (void);
// 0x00000589 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<>c__DisplayClass41_0::<GetOperatorMethods>b__0(System.Reflection.MethodInfo)
extern void U3CU3Ec__DisplayClass41_0_U3CGetOperatorMethodsU3Eb__0_mFD614AD39E7C16A45E46379BD58D6340B96CE980 (void);
// 0x0000058A System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::.ctor(System.Int32)
extern void U3CGetAllMembersU3Ed__42__ctor_mECC28ADDEDB2CFA0AD434C22F8369FA6DAC56390 (void);
// 0x0000058B System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.IDisposable.Dispose()
extern void U3CGetAllMembersU3Ed__42_System_IDisposable_Dispose_m9E319A62EFB4C3E57BED82A892250F00FC9B43B8 (void);
// 0x0000058C System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::MoveNext()
extern void U3CGetAllMembersU3Ed__42_MoveNext_m1CF3C44D786CEE618E67877AC112CC9EE665ED26 (void);
// 0x0000058D System.Reflection.MemberInfo Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.Generic.IEnumerator<System.Reflection.MemberInfo>.get_Current()
extern void U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mCE2F1CA338CF519DE8199720D6F10F60C833DC92 (void);
// 0x0000058E System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.IEnumerator.Reset()
extern void U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_Reset_mA8107830BE90C795E27D5738C36CD0D452AFAF82 (void);
// 0x0000058F System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_get_Current_m9D2C83C4E15A768D0658BBB1E95B8B963F88BD9E (void);
// 0x00000590 System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern void U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mF7146FB637C47E3922C611D84C0849179F9D44DD (void);
// 0x00000591 System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__42::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllMembersU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mF3BD4F1E1832E4D4DB4F6C5B6B1040A4530DECA4 (void);
// 0x00000592 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::.ctor(System.Int32)
extern void U3CGetAllMembersU3Ed__43__ctor_mD6E740BB16530EB8DC13A05DC855CA6004C722C5 (void);
// 0x00000593 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.IDisposable.Dispose()
extern void U3CGetAllMembersU3Ed__43_System_IDisposable_Dispose_mFFD18EABCD5841589699BECB72FE46280928EB67 (void);
// 0x00000594 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::MoveNext()
extern void U3CGetAllMembersU3Ed__43_MoveNext_m532C28E07A56D917CE6784BF962C09AA06120E77 (void);
// 0x00000595 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::<>m__Finally1()
extern void U3CGetAllMembersU3Ed__43_U3CU3Em__Finally1_m354EA9E7247B309D3AF0A23B9C36A847FB594029 (void);
// 0x00000596 System.Reflection.MemberInfo Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.Generic.IEnumerator<System.Reflection.MemberInfo>.get_Current()
extern void U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m32C3AE5D7E414AF63C4E2BFCFE1B524C69C74E28 (void);
// 0x00000597 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.IEnumerator.Reset()
extern void U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_Reset_m0A334D28C9D62519A97501CC7530ECF74CB89295 (void);
// 0x00000598 System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_get_Current_m37C697AEF1C2BE80E9464F0667B0466CB9FD9842 (void);
// 0x00000599 System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.Generic.IEnumerable<System.Reflection.MemberInfo>.GetEnumerator()
extern void U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m544D77F6409E37E5AAFDE93424D13976A215278D (void);
// 0x0000059A System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__43::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllMembersU3Ed__43_System_Collections_IEnumerable_GetEnumerator_mE50E2086242B0D28A1760315F8A023506D39B591 (void);
// 0x0000059B System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__44`1::.ctor(System.Int32)
// 0x0000059C System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__44`1::System.IDisposable.Dispose()
// 0x0000059D System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__44`1::MoveNext()
// 0x0000059E T Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__44`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000059F System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__44`1::System.Collections.IEnumerator.Reset()
// 0x000005A0 System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__44`1::System.Collections.IEnumerator.get_Current()
// 0x000005A1 System.Collections.Generic.IEnumerator`1<T> Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__44`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000005A2 System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetAllMembers>d__44`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000005A3 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::.ctor(System.Int32)
extern void U3CGetBaseClassesU3Ed__48__ctor_mC5020F833A3331D50ED3E4E46E31AF973C690C63 (void);
// 0x000005A4 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.IDisposable.Dispose()
extern void U3CGetBaseClassesU3Ed__48_System_IDisposable_Dispose_m42AFBCF3E84334C072FB5FDD3421EF847CB8EB46 (void);
// 0x000005A5 System.Boolean Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::MoveNext()
extern void U3CGetBaseClassesU3Ed__48_MoveNext_mEA5BFC904E5DEDA1EB4AA7366AFA47BD19EAAA4E (void);
// 0x000005A6 System.Type Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern void U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_mCE1DBB20C09DA287D6B34CC1D3635B6D0889257F (void);
// 0x000005A7 System.Void Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.IEnumerator.Reset()
extern void U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_Reset_mCBDCB3D8C952B2FC5F8E7C5E65919C03719387F6 (void);
// 0x000005A8 System.Object Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_get_Current_mD2EDA6986FDE391CBF36B8BA992B4C7E79FC0BD2 (void);
// 0x000005A9 System.Collections.Generic.IEnumerator`1<System.Type> Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern void U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m3F002A1BBB23E235B788DBBBDE103CBBF592F3C7 (void);
// 0x000005AA System.Collections.IEnumerator Sirenix.Serialization.Utilities.TypeExtensions/<GetBaseClasses>d__48::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerable_GetEnumerator_m98F625F59DD4C3E6DF98E87E34A20BFEBB4C152F (void);
// 0x000005AB System.Void Sirenix.Serialization.Utilities.UnityExtensions::.cctor()
extern void UnityExtensions__cctor_mB9941F239C1115B445B60D3B19D911E848B06813 (void);
// 0x000005AC System.Boolean Sirenix.Serialization.Utilities.UnityExtensions::SafeIsUnityNull(UnityEngine.Object)
extern void UnityExtensions_SafeIsUnityNull_m148B87C1D426B3FED2F450A9776997A28ED580CF (void);
// 0x000005AD System.Int32 Sirenix.Serialization.Utilities.Cache`1::get_MaxCacheSize()
// 0x000005AE System.Void Sirenix.Serialization.Utilities.Cache`1::set_MaxCacheSize(System.Int32)
// 0x000005AF System.Void Sirenix.Serialization.Utilities.Cache`1::.ctor()
// 0x000005B0 System.Boolean Sirenix.Serialization.Utilities.Cache`1::get_IsFree()
// 0x000005B1 Sirenix.Serialization.Utilities.Cache`1<T> Sirenix.Serialization.Utilities.Cache`1::Claim()
// 0x000005B2 System.Void Sirenix.Serialization.Utilities.Cache`1::Release(Sirenix.Serialization.Utilities.Cache`1<T>)
// 0x000005B3 T Sirenix.Serialization.Utilities.Cache`1::op_Implicit(Sirenix.Serialization.Utilities.Cache`1<T>)
// 0x000005B4 System.Void Sirenix.Serialization.Utilities.Cache`1::Release()
// 0x000005B5 System.Void Sirenix.Serialization.Utilities.Cache`1::System.IDisposable.Dispose()
// 0x000005B6 System.Void Sirenix.Serialization.Utilities.Cache`1::.cctor()
// 0x000005B7 System.Void Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::.ctor()
// 0x000005B8 System.Void Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirstKey>,System.Collections.Generic.IEqualityComparer`1<TSecondKey>)
// 0x000005B9 System.Collections.Generic.Dictionary`2<TSecondKey,TValue> Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::get_Item(TFirstKey)
// 0x000005BA System.Int32 Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::InnerCount(TFirstKey)
// 0x000005BB System.Int32 Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::TotalInnerCount()
// 0x000005BC System.Boolean Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::ContainsKeys(TFirstKey,TSecondKey)
// 0x000005BD System.Boolean Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::TryGetInnerValue(TFirstKey,TSecondKey,TValue&)
// 0x000005BE TValue Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::AddInner(TFirstKey,TSecondKey,TValue)
// 0x000005BF System.Boolean Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::RemoveInner(TFirstKey,TSecondKey)
// 0x000005C0 System.Void Sirenix.Serialization.Utilities.DoubleLookupDictionary`3::RemoveWhere(System.Func`2<TValue,System.Boolean>)
// 0x000005C1 System.Void Sirenix.Serialization.Utilities.WeakValueGetter::.ctor(System.Object,System.IntPtr)
extern void WeakValueGetter__ctor_mC25912C1222D3D105A3B571C74F46E91ABF2E7E4 (void);
// 0x000005C2 System.Object Sirenix.Serialization.Utilities.WeakValueGetter::Invoke(System.Object&)
extern void WeakValueGetter_Invoke_mA5F428CA3C08F91E2E0E533D292F4358A2CB50AA (void);
// 0x000005C3 System.IAsyncResult Sirenix.Serialization.Utilities.WeakValueGetter::BeginInvoke(System.Object&,System.AsyncCallback,System.Object)
extern void WeakValueGetter_BeginInvoke_mCF4151097E01C35E0832BA6D2D7CD5A68B7EB823 (void);
// 0x000005C4 System.Object Sirenix.Serialization.Utilities.WeakValueGetter::EndInvoke(System.Object&,System.IAsyncResult)
extern void WeakValueGetter_EndInvoke_m08CE8064927417650195E223AE2095BBFF13FE88 (void);
// 0x000005C5 System.Void Sirenix.Serialization.Utilities.WeakValueSetter::.ctor(System.Object,System.IntPtr)
extern void WeakValueSetter__ctor_mF9A8C8647D42D24A8269A1454EB1BB079E6DD367 (void);
// 0x000005C6 System.Void Sirenix.Serialization.Utilities.WeakValueSetter::Invoke(System.Object&,System.Object)
extern void WeakValueSetter_Invoke_mB499FB8A653D20DACDBB7E06448CA49F5AE4ECD7 (void);
// 0x000005C7 System.IAsyncResult Sirenix.Serialization.Utilities.WeakValueSetter::BeginInvoke(System.Object&,System.Object,System.AsyncCallback,System.Object)
extern void WeakValueSetter_BeginInvoke_m8415DE549ABF756980451E5954C3F95C251E9F6E (void);
// 0x000005C8 System.Void Sirenix.Serialization.Utilities.WeakValueSetter::EndInvoke(System.Object&,System.IAsyncResult)
extern void WeakValueSetter_EndInvoke_mFB31A15078B1B15A5438D5011AE5D736BF21A112 (void);
// 0x000005C9 System.Void Sirenix.Serialization.Utilities.WeakValueGetter`1::.ctor(System.Object,System.IntPtr)
// 0x000005CA FieldType Sirenix.Serialization.Utilities.WeakValueGetter`1::Invoke(System.Object&)
// 0x000005CB System.IAsyncResult Sirenix.Serialization.Utilities.WeakValueGetter`1::BeginInvoke(System.Object&,System.AsyncCallback,System.Object)
// 0x000005CC FieldType Sirenix.Serialization.Utilities.WeakValueGetter`1::EndInvoke(System.Object&,System.IAsyncResult)
// 0x000005CD System.Void Sirenix.Serialization.Utilities.WeakValueSetter`1::.ctor(System.Object,System.IntPtr)
// 0x000005CE System.Void Sirenix.Serialization.Utilities.WeakValueSetter`1::Invoke(System.Object&,FieldType)
// 0x000005CF System.IAsyncResult Sirenix.Serialization.Utilities.WeakValueSetter`1::BeginInvoke(System.Object&,FieldType,System.AsyncCallback,System.Object)
// 0x000005D0 System.Void Sirenix.Serialization.Utilities.WeakValueSetter`1::EndInvoke(System.Object&,System.IAsyncResult)
// 0x000005D1 System.Void Sirenix.Serialization.Utilities.ValueGetter`2::.ctor(System.Object,System.IntPtr)
// 0x000005D2 FieldType Sirenix.Serialization.Utilities.ValueGetter`2::Invoke(InstanceType&)
// 0x000005D3 System.IAsyncResult Sirenix.Serialization.Utilities.ValueGetter`2::BeginInvoke(InstanceType&,System.AsyncCallback,System.Object)
// 0x000005D4 FieldType Sirenix.Serialization.Utilities.ValueGetter`2::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x000005D5 System.Void Sirenix.Serialization.Utilities.ValueSetter`2::.ctor(System.Object,System.IntPtr)
// 0x000005D6 System.Void Sirenix.Serialization.Utilities.ValueSetter`2::Invoke(InstanceType&,FieldType)
// 0x000005D7 System.IAsyncResult Sirenix.Serialization.Utilities.ValueSetter`2::BeginInvoke(InstanceType&,FieldType,System.AsyncCallback,System.Object)
// 0x000005D8 System.Void Sirenix.Serialization.Utilities.ValueSetter`2::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x000005D9 System.Boolean Sirenix.Serialization.Utilities.EmitUtilities::get_CanEmit()
extern void EmitUtilities_get_CanEmit_m6A2EFD81967C966AFA35F44F0AEC230831098C32 (void);
// 0x000005DA System.Func`1<FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateStaticFieldGetter(System.Reflection.FieldInfo)
// 0x000005DB System.Func`1<System.Object> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakStaticFieldGetter(System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakStaticFieldGetter_m501994BA75C597AE8E7C17DA32076990924A2E3A (void);
// 0x000005DC System.Action`1<FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateStaticFieldSetter(System.Reflection.FieldInfo)
// 0x000005DD System.Action`1<System.Object> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakStaticFieldSetter(System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakStaticFieldSetter_m1306D5871C868B751B070F8090B58D80C494A517 (void);
// 0x000005DE Sirenix.Serialization.Utilities.ValueGetter`2<InstanceType,FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceFieldGetter(System.Reflection.FieldInfo)
// 0x000005DF Sirenix.Serialization.Utilities.WeakValueGetter`1<FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceFieldGetter(System.Type,System.Reflection.FieldInfo)
// 0x000005E0 Sirenix.Serialization.Utilities.WeakValueGetter Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceFieldGetter(System.Type,System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakInstanceFieldGetter_m80FD606B8AA5EA5DFC518906D6DC79C4CD370EB4 (void);
// 0x000005E1 Sirenix.Serialization.Utilities.ValueSetter`2<InstanceType,FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceFieldSetter(System.Reflection.FieldInfo)
// 0x000005E2 Sirenix.Serialization.Utilities.WeakValueSetter`1<FieldType> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceFieldSetter(System.Type,System.Reflection.FieldInfo)
// 0x000005E3 Sirenix.Serialization.Utilities.WeakValueSetter Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceFieldSetter(System.Type,System.Reflection.FieldInfo)
extern void EmitUtilities_CreateWeakInstanceFieldSetter_m7AB5E226D7BB63093B6872FB6C2EE35E8D2817A8 (void);
// 0x000005E4 Sirenix.Serialization.Utilities.WeakValueGetter Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstancePropertyGetter(System.Type,System.Reflection.PropertyInfo)
extern void EmitUtilities_CreateWeakInstancePropertyGetter_m446BA8C86CA5FE5FC197BA041700AFE13E5B3FFE (void);
// 0x000005E5 Sirenix.Serialization.Utilities.WeakValueSetter Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstancePropertySetter(System.Type,System.Reflection.PropertyInfo)
extern void EmitUtilities_CreateWeakInstancePropertySetter_mD8EA9D775FFAA83132EACF11E3EAAB2270029112 (void);
// 0x000005E6 System.Action`1<PropType> Sirenix.Serialization.Utilities.EmitUtilities::CreateStaticPropertySetter(System.Reflection.PropertyInfo)
// 0x000005E7 System.Func`1<PropType> Sirenix.Serialization.Utilities.EmitUtilities::CreateStaticPropertyGetter(System.Reflection.PropertyInfo)
// 0x000005E8 Sirenix.Serialization.Utilities.ValueSetter`2<InstanceType,PropType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstancePropertySetter(System.Reflection.PropertyInfo)
// 0x000005E9 Sirenix.Serialization.Utilities.ValueGetter`2<InstanceType,PropType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstancePropertyGetter(System.Reflection.PropertyInfo)
// 0x000005EA System.Func`2<InstanceType,ReturnType> Sirenix.Serialization.Utilities.EmitUtilities::CreateMethodReturner(System.Reflection.MethodInfo)
// 0x000005EB System.Action Sirenix.Serialization.Utilities.EmitUtilities::CreateStaticMethodCaller(System.Reflection.MethodInfo)
extern void EmitUtilities_CreateStaticMethodCaller_mAAB81F7426E87E32721FF38A68C6828C744836BD (void);
// 0x000005EC System.Action`2<System.Object,TArg1> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x000005ED System.Action`1<System.Object> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
extern void EmitUtilities_CreateWeakInstanceMethodCaller_mDDC78D426B93498948BE485AE7254B4BDD6AF966 (void);
// 0x000005EE System.Func`3<System.Object,TArg1,TResult> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x000005EF System.Func`2<System.Object,TResult> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceMethodCallerFunc(System.Reflection.MethodInfo)
// 0x000005F0 System.Func`3<System.Object,TArg,TResult> Sirenix.Serialization.Utilities.EmitUtilities::CreateWeakInstanceMethodCallerFunc(System.Reflection.MethodInfo)
// 0x000005F1 System.Action`1<InstanceType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x000005F2 System.Action`2<InstanceType,Arg1> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceMethodCaller(System.Reflection.MethodInfo)
// 0x000005F3 Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`1<InstanceType> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceRefMethodCaller(System.Reflection.MethodInfo)
// 0x000005F4 Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`2<InstanceType,Arg1> Sirenix.Serialization.Utilities.EmitUtilities::CreateInstanceRefMethodCaller(System.Reflection.MethodInfo)
// 0x000005F5 System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`1::.ctor(System.Object,System.IntPtr)
// 0x000005F6 System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`1::Invoke(InstanceType&)
// 0x000005F7 System.IAsyncResult Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`1::BeginInvoke(InstanceType&,System.AsyncCallback,System.Object)
// 0x000005F8 System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`1::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x000005F9 System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`2::.ctor(System.Object,System.IntPtr)
// 0x000005FA System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`2::Invoke(InstanceType&,TArg1)
// 0x000005FB System.IAsyncResult Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`2::BeginInvoke(InstanceType&,TArg1,System.AsyncCallback,System.Object)
// 0x000005FC System.Void Sirenix.Serialization.Utilities.EmitUtilities/InstanceRefMethodCaller`2::EndInvoke(InstanceType&,System.IAsyncResult)
// 0x000005FD System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass2_0`1::.ctor()
// 0x000005FE FieldType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass2_0`1::<CreateStaticFieldGetter>b__0()
// 0x000005FF System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass2_1`1::.ctor()
// 0x00000600 FieldType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass2_1`1::<CreateStaticFieldGetter>b__1()
// 0x00000601 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m34B8F7BAD9EAD75A9BBCD508B9612EEF99EBE0AD (void);
// 0x00000602 System.Object Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass3_0::<CreateWeakStaticFieldGetter>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CCreateWeakStaticFieldGetterU3Eb__0_m86026FCD57D37B8534AAB862C375C43926204451 (void);
// 0x00000603 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass4_0`1::.ctor()
// 0x00000604 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass4_0`1::<CreateStaticFieldSetter>b__0(FieldType)
// 0x00000605 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mEA67D5D4ED3A8E7368E48D8D27CF0A1158F5C7E3 (void);
// 0x00000606 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass5_0::<CreateWeakStaticFieldSetter>b__0(System.Object)
extern void U3CU3Ec__DisplayClass5_0_U3CCreateWeakStaticFieldSetterU3Eb__0_m8AD0502169274D85BCFE86E102DF09C4CDEEA4CE (void);
// 0x00000607 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass6_0`2::.ctor()
// 0x00000608 FieldType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass6_0`2::<CreateInstanceFieldGetter>b__0(InstanceType&)
// 0x00000609 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass7_0`1::.ctor()
// 0x0000060A FieldType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass7_0`1::<CreateWeakInstanceFieldGetter>b__0(System.Object&)
// 0x0000060B System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m3774AC56DC6965D7BA91A7DCB053316B1D75CB14 (void);
// 0x0000060C System.Object Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass8_0::<CreateWeakInstanceFieldGetter>b__0(System.Object&)
extern void U3CU3Ec__DisplayClass8_0_U3CCreateWeakInstanceFieldGetterU3Eb__0_m798776B45AE9644C8642A01F4ECD19D2683CB32D (void);
// 0x0000060D System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass9_0`2::.ctor()
// 0x0000060E System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass9_0`2::<CreateInstanceFieldSetter>b__0(InstanceType&,FieldType)
// 0x0000060F System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass10_0`1::.ctor()
// 0x00000610 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass10_0`1::<CreateWeakInstanceFieldSetter>b__0(System.Object&,FieldType)
// 0x00000611 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m204CF01344CE0096F747F1530CDFD6C32793C30D (void);
// 0x00000612 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass11_0::<CreateWeakInstanceFieldSetter>b__0(System.Object&,System.Object)
extern void U3CU3Ec__DisplayClass11_0_U3CCreateWeakInstanceFieldSetterU3Eb__0_mD514088E786DD146B6BF3C88F44AC0D0A9FBCAC5 (void);
// 0x00000613 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mD80107369A63C94184567AEE3A4B9F2FD518EB6E (void);
// 0x00000614 System.Object Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass12_0::<CreateWeakInstancePropertyGetter>b__0(System.Object&)
extern void U3CU3Ec__DisplayClass12_0_U3CCreateWeakInstancePropertyGetterU3Eb__0_m53AE48240AC9FAAE3F328E21240A8C5FB2494B46 (void);
// 0x00000615 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m14841C32661C631E9193777037BEEB3CDB8C737A (void);
// 0x00000616 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass13_0::<CreateWeakInstancePropertySetter>b__0(System.Object&,System.Object)
extern void U3CU3Ec__DisplayClass13_0_U3CCreateWeakInstancePropertySetterU3Eb__0_m1A765B4D7FFA23F0F674AE075113D73F065C8BF0 (void);
// 0x00000617 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass14_0`1::.ctor()
// 0x00000618 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass14_0`1::<CreateStaticPropertySetter>b__0(PropType)
// 0x00000619 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass15_0`1::.ctor()
// 0x0000061A PropType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass15_0`1::<CreateStaticPropertyGetter>b__0()
// 0x0000061B System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass16_0`2::.ctor()
// 0x0000061C System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass16_0`2::<CreateInstancePropertySetter>b__0(InstanceType&,PropType)
// 0x0000061D System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass17_0`2::.ctor()
// 0x0000061E PropType Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass17_0`2::<CreateInstancePropertyGetter>b__0(InstanceType&)
// 0x0000061F System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass20_0`1::.ctor()
// 0x00000620 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass20_0`1::<CreateWeakInstanceMethodCaller>b__0(System.Object,TArg1)
// 0x00000621 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mF7D5D0502C6E32DEB589B1731EC6E41D8DB0BCB6 (void);
// 0x00000622 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass21_0::<CreateWeakInstanceMethodCaller>b__0(System.Object)
extern void U3CU3Ec__DisplayClass21_0_U3CCreateWeakInstanceMethodCallerU3Eb__0_m7878B49FB1EA302C4D20562567D84E31E7707164 (void);
// 0x00000623 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass22_0`2::.ctor()
// 0x00000624 TResult Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass22_0`2::<CreateWeakInstanceMethodCaller>b__0(System.Object,TArg1)
// 0x00000625 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass23_0`1::.ctor()
// 0x00000626 TResult Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass23_0`1::<CreateWeakInstanceMethodCallerFunc>b__0(System.Object)
// 0x00000627 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass24_0`2::.ctor()
// 0x00000628 TResult Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass24_0`2::<CreateWeakInstanceMethodCallerFunc>b__0(System.Object,TArg)
// 0x00000629 System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass29_0`1::.ctor()
// 0x0000062A System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass29_0`1::<CreateInstanceRefMethodCaller>b__0(InstanceType&)
// 0x0000062B System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass30_0`2::.ctor()
// 0x0000062C System.Void Sirenix.Serialization.Utilities.EmitUtilities/<>c__DisplayClass30_0`2::<CreateInstanceRefMethodCaller>b__0(InstanceType&,Arg1)
// 0x0000062D System.Boolean Sirenix.Serialization.Utilities.FastTypeComparer::Equals(System.Type,System.Type)
extern void FastTypeComparer_Equals_mBDB33646E25667DB2509E7AA121ECA11C2D313C0 (void);
// 0x0000062E System.Int32 Sirenix.Serialization.Utilities.FastTypeComparer::GetHashCode(System.Type)
extern void FastTypeComparer_GetHashCode_mA7A7AE3A534E8B94D78730B0719917A099C277C3 (void);
// 0x0000062F System.Void Sirenix.Serialization.Utilities.FastTypeComparer::.ctor()
extern void FastTypeComparer__ctor_mF53C14AA3EBA18D924BF8DAB322312D245EC3DAC (void);
// 0x00000630 System.Void Sirenix.Serialization.Utilities.FastTypeComparer::.cctor()
extern void FastTypeComparer__cctor_mF44B00C0BA807130B6301A79E4E6E66C4EB3A0C6 (void);
// 0x00000631 System.Void Sirenix.Serialization.Utilities.ICacheNotificationReceiver::OnFreed()
// 0x00000632 System.Void Sirenix.Serialization.Utilities.ICacheNotificationReceiver::OnClaimed()
// 0x00000633 T Sirenix.Serialization.Utilities.IImmutableList`1::get_Item(System.Int32)
// 0x00000634 System.Void Sirenix.Serialization.Utilities.ImmutableList::.ctor(System.Collections.IList)
extern void ImmutableList__ctor_mD3AD0AE6E6D3082F26A458B8E7A143473A3139E9 (void);
// 0x00000635 System.Int32 Sirenix.Serialization.Utilities.ImmutableList::get_Count()
extern void ImmutableList_get_Count_m1C404BF3D36800B1CC58823948768ACCE8E0A43C (void);
// 0x00000636 System.Boolean Sirenix.Serialization.Utilities.ImmutableList::get_IsFixedSize()
extern void ImmutableList_get_IsFixedSize_m04DF44C325A4A0B7B19478B949E13723557D5EC8 (void);
// 0x00000637 System.Boolean Sirenix.Serialization.Utilities.ImmutableList::get_IsReadOnly()
extern void ImmutableList_get_IsReadOnly_mAC133AF521699C29D5E0EEB01623BE9CAA26AA51 (void);
// 0x00000638 System.Boolean Sirenix.Serialization.Utilities.ImmutableList::get_IsSynchronized()
extern void ImmutableList_get_IsSynchronized_m43E4AA692B59DF5E8ADAD5C2551A3606ECE8D418 (void);
// 0x00000639 System.Object Sirenix.Serialization.Utilities.ImmutableList::get_SyncRoot()
extern void ImmutableList_get_SyncRoot_mA9AF60F215B700336A78A6A9ECD6B002858AD785 (void);
// 0x0000063A System.Object Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.get_Item(System.Int32)
extern void ImmutableList_System_Collections_IList_get_Item_m02E44E3A3DA91369B85B90ED1427F8CE985F3596 (void);
// 0x0000063B System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void ImmutableList_System_Collections_IList_set_Item_m3BB5159F43EB47FB21F4532E300F5D165184B527 (void);
// 0x0000063C System.Object Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.get_Item(System.Int32)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_get_Item_m75BBBB8651EC734C2707449C08532E6F432FEC1E (void);
// 0x0000063D System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.set_Item(System.Int32,System.Object)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_set_Item_m1BC154A26CE149693DD12E014192B551391CFCEA (void);
// 0x0000063E System.Object Sirenix.Serialization.Utilities.ImmutableList::get_Item(System.Int32)
extern void ImmutableList_get_Item_mECF538C1C9EEAFE0E3B07D7C04EED873F790453F (void);
// 0x0000063F System.Boolean Sirenix.Serialization.Utilities.ImmutableList::Contains(System.Object)
extern void ImmutableList_Contains_m188AD0A52AB9811B009DA0372C735C7766B0877F (void);
// 0x00000640 System.Void Sirenix.Serialization.Utilities.ImmutableList::CopyTo(System.Object[],System.Int32)
extern void ImmutableList_CopyTo_m0192579DBC7B00BB7805FA897E17E664FA1DB495 (void);
// 0x00000641 System.Void Sirenix.Serialization.Utilities.ImmutableList::CopyTo(System.Array,System.Int32)
extern void ImmutableList_CopyTo_mB190CC0DD9E8986001374BE6B16DCE54BCCDBDA9 (void);
// 0x00000642 System.Collections.IEnumerator Sirenix.Serialization.Utilities.ImmutableList::GetEnumerator()
extern void ImmutableList_GetEnumerator_m727A7B69B07F066B80A7AB84DDFCAD7FC191BE14 (void);
// 0x00000643 System.Collections.IEnumerator Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IEnumerable.GetEnumerator()
extern void ImmutableList_System_Collections_IEnumerable_GetEnumerator_m7410D7F5DD22BFA66DC2519EB731CA29B9F53B34 (void);
// 0x00000644 System.Collections.Generic.IEnumerator`1<System.Object> Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
extern void ImmutableList_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m92F132F345C71BFEAD934D6EFC4C22DBB696606F (void);
// 0x00000645 System.Int32 Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.Add(System.Object)
extern void ImmutableList_System_Collections_IList_Add_m651A59979271AA1C84A5F318DA8D797BDD795CDA (void);
// 0x00000646 System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.Clear()
extern void ImmutableList_System_Collections_IList_Clear_mC731204B3AF7555F80B1916418A6E1DFA02B5CD5 (void);
// 0x00000647 System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.Insert(System.Int32,System.Object)
extern void ImmutableList_System_Collections_IList_Insert_m99C94F6E0A896CDCEE0912A704A0C8D51F0B11E7 (void);
// 0x00000648 System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.Remove(System.Object)
extern void ImmutableList_System_Collections_IList_Remove_m8F69CB7E6496AFAC09D74C5BDD12E9D1D10459EB (void);
// 0x00000649 System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.IList.RemoveAt(System.Int32)
extern void ImmutableList_System_Collections_IList_RemoveAt_mAE15FD52A3BBCF116233AB657246B5CA007F6A2F (void);
// 0x0000064A System.Int32 Sirenix.Serialization.Utilities.ImmutableList::IndexOf(System.Object)
extern void ImmutableList_IndexOf_m8F53B2B91F12845107CA21E6574E5652F4CC5E21 (void);
// 0x0000064B System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.RemoveAt(System.Int32)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_RemoveAt_m77A06A620F48A9656A7C8AC941FA180E6F2BA0D5 (void);
// 0x0000064C System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.IList<System.Object>.Insert(System.Int32,System.Object)
extern void ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_Insert_m3EB9C824D35401BB1491D4DBE36AF1A6BE4587FA (void);
// 0x0000064D System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Add(System.Object)
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Add_m1D1C0EC8CC8AA410A268196E00AD0ABDA7DF7DA6 (void);
// 0x0000064E System.Void Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Clear()
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Clear_mD7DAB8DD13D5FEB0F4F8A1419B2CA9D0060A0F95 (void);
// 0x0000064F System.Boolean Sirenix.Serialization.Utilities.ImmutableList::System.Collections.Generic.ICollection<System.Object>.Remove(System.Object)
extern void ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Remove_mA01260F225FA8A3FB795DB1A01AA09233FAD1075 (void);
// 0x00000650 System.Void Sirenix.Serialization.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::.ctor(System.Int32)
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_mB98A496FC6D73EB5897246F5885D9ED7FAC7284A (void);
// 0x00000651 System.Void Sirenix.Serialization.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::System.IDisposable.Dispose()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m14F4C94791AFA5DA05AE5DE20606B7D47011868F (void);
// 0x00000652 System.Boolean Sirenix.Serialization.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::MoveNext()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_mB8B7B9FB1825CE9610F26025EC18205A500AF186 (void);
// 0x00000653 System.Void Sirenix.Serialization.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::<>m__Finally1()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_m18871DF14FF7A1788814024B4D840F5DCD716AD7 (void);
// 0x00000654 System.Object Sirenix.Serialization.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD01C87D86A2E129FDDE79B964FE84B546B589053 (void);
// 0x00000655 System.Void Sirenix.Serialization.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::System.Collections.IEnumerator.Reset()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m493988934AAA09863EA33E918C0579D8826E0454 (void);
// 0x00000656 System.Object Sirenix.Serialization.Utilities.ImmutableList/<SystemU2DCollectionsU2DGenericU2DIEnumerable<SystemU2DObject>U2DGetEnumerator>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m2343A58E3F14DD579E8F296C8063D129E1AF5E18 (void);
// 0x00000657 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::.ctor(System.Collections.Generic.IList`1<T>)
// 0x00000658 System.Int32 Sirenix.Serialization.Utilities.ImmutableList`1::get_Count()
// 0x00000659 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.ICollection.get_IsSynchronized()
// 0x0000065A System.Object Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.ICollection.get_SyncRoot()
// 0x0000065B System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.get_IsFixedSize()
// 0x0000065C System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.get_IsReadOnly()
// 0x0000065D System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::get_IsReadOnly()
// 0x0000065E System.Object Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.get_Item(System.Int32)
// 0x0000065F System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x00000660 T Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.get_Item(System.Int32)
// 0x00000661 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
// 0x00000662 T Sirenix.Serialization.Utilities.ImmutableList`1::get_Item(System.Int32)
// 0x00000663 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::Contains(T)
// 0x00000664 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::CopyTo(T[],System.Int32)
// 0x00000665 System.Collections.Generic.IEnumerator`1<T> Sirenix.Serialization.Utilities.ImmutableList`1::GetEnumerator()
// 0x00000666 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000667 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000668 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Clear()
// 0x00000669 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.ICollection<T>.Remove(T)
// 0x0000066A System.Collections.IEnumerator Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000066B System.Int32 Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.Add(System.Object)
// 0x0000066C System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.Clear()
// 0x0000066D System.Boolean Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.Contains(System.Object)
// 0x0000066E System.Int32 Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.IndexOf(System.Object)
// 0x0000066F System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x00000670 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.Remove(System.Object)
// 0x00000671 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
// 0x00000672 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.IList.RemoveAt(System.Int32)
// 0x00000673 System.Int32 Sirenix.Serialization.Utilities.ImmutableList`1::IndexOf(T)
// 0x00000674 System.Void Sirenix.Serialization.Utilities.ImmutableList`1::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
// 0x00000675 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::.ctor(TList)
// 0x00000676 System.Int32 Sirenix.Serialization.Utilities.ImmutableList`2::get_Count()
// 0x00000677 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.ICollection.get_IsSynchronized()
// 0x00000678 System.Object Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.ICollection.get_SyncRoot()
// 0x00000679 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.get_IsFixedSize()
// 0x0000067A System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.get_IsReadOnly()
// 0x0000067B System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::get_IsReadOnly()
// 0x0000067C System.Object Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.get_Item(System.Int32)
// 0x0000067D System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x0000067E TElement Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x0000067F System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x00000680 TElement Sirenix.Serialization.Utilities.ImmutableList`2::get_Item(System.Int32)
// 0x00000681 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::Contains(TElement)
// 0x00000682 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::CopyTo(TElement[],System.Int32)
// 0x00000683 System.Collections.Generic.IEnumerator`1<TElement> Sirenix.Serialization.Utilities.ImmutableList`2::GetEnumerator()
// 0x00000684 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000685 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x00000686 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x00000687 System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x00000688 System.Collections.IEnumerator Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000689 System.Int32 Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.Add(System.Object)
// 0x0000068A System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.Clear()
// 0x0000068B System.Boolean Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.Contains(System.Object)
// 0x0000068C System.Int32 Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.IndexOf(System.Object)
// 0x0000068D System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x0000068E System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.Remove(System.Object)
// 0x0000068F System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x00000690 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.IList.RemoveAt(System.Int32)
// 0x00000691 System.Int32 Sirenix.Serialization.Utilities.ImmutableList`2::IndexOf(TElement)
// 0x00000692 System.Void Sirenix.Serialization.Utilities.ImmutableList`2::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x00000693 System.Void Sirenix.Serialization.Utilities.MemberAliasFieldInfo::.ctor(System.Reflection.FieldInfo,System.String)
extern void MemberAliasFieldInfo__ctor_m4C7EFE1B86FB7272AAE2C9C21C985D55DA66775F (void);
// 0x00000694 System.Void Sirenix.Serialization.Utilities.MemberAliasFieldInfo::.ctor(System.Reflection.FieldInfo,System.String,System.String)
extern void MemberAliasFieldInfo__ctor_m97665DF2870846A3059EBEA7DA1A65F5D21181F6 (void);
// 0x00000695 System.Reflection.FieldInfo Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_AliasedField()
extern void MemberAliasFieldInfo_get_AliasedField_mC29D1AA05B88ED32A55544D133A80ECC08C1431F (void);
// 0x00000696 System.Reflection.Module Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_Module()
extern void MemberAliasFieldInfo_get_Module_mE2B2DF70C87FEBD7768AE569E16DBF3E01A290C5 (void);
// 0x00000697 System.Int32 Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_MetadataToken()
extern void MemberAliasFieldInfo_get_MetadataToken_mF8151E7B6B152FE7F5636E63B40679889B505747 (void);
// 0x00000698 System.String Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_Name()
extern void MemberAliasFieldInfo_get_Name_mD85D2E84045F5ABE921DC41901BF758C32449F42 (void);
// 0x00000699 System.Type Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_DeclaringType()
extern void MemberAliasFieldInfo_get_DeclaringType_mFC779BA7B2E63918EB7D8CEFBA11FC8D63598F42 (void);
// 0x0000069A System.Type Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_ReflectedType()
extern void MemberAliasFieldInfo_get_ReflectedType_m259F9696743632AE55534C6F6197E4A6F029BC4C (void);
// 0x0000069B System.Type Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_FieldType()
extern void MemberAliasFieldInfo_get_FieldType_mE0933ABE9E051A77BC1DCCC231687A182B0AD0AE (void);
// 0x0000069C System.RuntimeFieldHandle Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_FieldHandle()
extern void MemberAliasFieldInfo_get_FieldHandle_mBEA27E0ECFBDAFBD99BB48CD47C15D15C275FBD5 (void);
// 0x0000069D System.Reflection.FieldAttributes Sirenix.Serialization.Utilities.MemberAliasFieldInfo::get_Attributes()
extern void MemberAliasFieldInfo_get_Attributes_mFE2217C52681C19C8A99C7001AFC21BEED92166B (void);
// 0x0000069E System.Object[] Sirenix.Serialization.Utilities.MemberAliasFieldInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasFieldInfo_GetCustomAttributes_mB3B3DC80669D90E19E413281E442F3D12C9CDD46 (void);
// 0x0000069F System.Object[] Sirenix.Serialization.Utilities.MemberAliasFieldInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasFieldInfo_GetCustomAttributes_m94BF1FC2E8A40C017F29963AE74D262C841C8FF3 (void);
// 0x000006A0 System.Boolean Sirenix.Serialization.Utilities.MemberAliasFieldInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasFieldInfo_IsDefined_mF449506D9BFF0883F1DE5959615950C379421519 (void);
// 0x000006A1 System.Object Sirenix.Serialization.Utilities.MemberAliasFieldInfo::GetValue(System.Object)
extern void MemberAliasFieldInfo_GetValue_m3A68DD18FE9E438D960193E4EF55F5E81778BD1F (void);
// 0x000006A2 System.Void Sirenix.Serialization.Utilities.MemberAliasFieldInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern void MemberAliasFieldInfo_SetValue_m419B08D5F4F9505C3103D54E7B6ABD58D0BF266C (void);
// 0x000006A3 System.Void Sirenix.Serialization.Utilities.MemberAliasMethodInfo::.ctor(System.Reflection.MethodInfo,System.String)
extern void MemberAliasMethodInfo__ctor_mE3B16B07732B86C8952351853C15EC501543F879 (void);
// 0x000006A4 System.Void Sirenix.Serialization.Utilities.MemberAliasMethodInfo::.ctor(System.Reflection.MethodInfo,System.String,System.String)
extern void MemberAliasMethodInfo__ctor_m1981CD912E83CF99D0850B881396DC97D440B8EC (void);
// 0x000006A5 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_AliasedMethod()
extern void MemberAliasMethodInfo_get_AliasedMethod_m2D0C60CA7D97A0BA54DAC4EDF8AFB4405D83A1D1 (void);
// 0x000006A6 System.Reflection.ICustomAttributeProvider Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_ReturnTypeCustomAttributes()
extern void MemberAliasMethodInfo_get_ReturnTypeCustomAttributes_mAD4A554DE6DC6885B6D52B4FA2F1E75E01E6DE29 (void);
// 0x000006A7 System.RuntimeMethodHandle Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_MethodHandle()
extern void MemberAliasMethodInfo_get_MethodHandle_mA901A429350D8493DBC78B676E06A2266A1073B1 (void);
// 0x000006A8 System.Reflection.MethodAttributes Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_Attributes()
extern void MemberAliasMethodInfo_get_Attributes_m270BA994621DF661A7FDEB63B5DE443698699681 (void);
// 0x000006A9 System.Type Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_ReturnType()
extern void MemberAliasMethodInfo_get_ReturnType_mE5E76CB44F44545E5DF1E0CE597BD76019D100EE (void);
// 0x000006AA System.Type Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_DeclaringType()
extern void MemberAliasMethodInfo_get_DeclaringType_m2FD3E5A6103883EC85523A9436F5AAAE789ED32A (void);
// 0x000006AB System.String Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_Name()
extern void MemberAliasMethodInfo_get_Name_m0ACBE0BCF7D8BF8EB433428A8C0B78FC066BBAD4 (void);
// 0x000006AC System.Type Sirenix.Serialization.Utilities.MemberAliasMethodInfo::get_ReflectedType()
extern void MemberAliasMethodInfo_get_ReflectedType_mC385E812CE5E2F7A4B6E5A36C8FA0F3BDD036FED (void);
// 0x000006AD System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetBaseDefinition()
extern void MemberAliasMethodInfo_GetBaseDefinition_m18010FDDFC15BF72B98B462CAB7B6EC3A8FF0F16 (void);
// 0x000006AE System.Object[] Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasMethodInfo_GetCustomAttributes_m61937014A1B94160598354BAF881FE54F3854EC6 (void);
// 0x000006AF System.Object[] Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasMethodInfo_GetCustomAttributes_mC5D6D08C832D6494C4156FA932FDCF5BEDB6542A (void);
// 0x000006B0 System.Reflection.MethodImplAttributes Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetMethodImplementationFlags()
extern void MemberAliasMethodInfo_GetMethodImplementationFlags_mB33EB9AB49A3570DF28CD47C20EFAEB7A76D8236 (void);
// 0x000006B1 System.Reflection.ParameterInfo[] Sirenix.Serialization.Utilities.MemberAliasMethodInfo::GetParameters()
extern void MemberAliasMethodInfo_GetParameters_m9ED19AF9B9C759827A871F9DEBA7DCF643F3A381 (void);
// 0x000006B2 System.Object Sirenix.Serialization.Utilities.MemberAliasMethodInfo::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasMethodInfo_Invoke_mBD9A3D588627BD1268068F591767567990A42ACA (void);
// 0x000006B3 System.Boolean Sirenix.Serialization.Utilities.MemberAliasMethodInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasMethodInfo_IsDefined_mCCB5831AFF2D69829B0C2CA25447F657B8728936 (void);
// 0x000006B4 System.Void Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::.ctor(System.Reflection.PropertyInfo,System.String)
extern void MemberAliasPropertyInfo__ctor_m6E80A656E48FE3D0B0D28D511DC6371DD69626F4 (void);
// 0x000006B5 System.Void Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::.ctor(System.Reflection.PropertyInfo,System.String,System.String)
extern void MemberAliasPropertyInfo__ctor_m910BE931BB1FED2AF45A3115AF9F621CCFC33757 (void);
// 0x000006B6 System.Reflection.PropertyInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_AliasedProperty()
extern void MemberAliasPropertyInfo_get_AliasedProperty_m4A9FD4981F8E2EB2AE94916AC2C3927222830BD0 (void);
// 0x000006B7 System.Reflection.Module Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_Module()
extern void MemberAliasPropertyInfo_get_Module_m09DB8BEA369ED7AA0488A8F7FD380DC5FBB7F538 (void);
// 0x000006B8 System.Int32 Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_MetadataToken()
extern void MemberAliasPropertyInfo_get_MetadataToken_m2B13A5FACC2B68CE017146D7765F68AFFCF536FE (void);
// 0x000006B9 System.String Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_Name()
extern void MemberAliasPropertyInfo_get_Name_m1B09EF82176BB25BB0190E16233D4C0EE637A9FC (void);
// 0x000006BA System.Type Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_DeclaringType()
extern void MemberAliasPropertyInfo_get_DeclaringType_mEBEB80B5D0AC4B71B0D3D099923AFDA1871549F3 (void);
// 0x000006BB System.Type Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_ReflectedType()
extern void MemberAliasPropertyInfo_get_ReflectedType_m04BC94F2AC8A09DD295C81EE0E937FB4C18230F7 (void);
// 0x000006BC System.Type Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_PropertyType()
extern void MemberAliasPropertyInfo_get_PropertyType_m9753E8FC71707FD1886F2E983473900210989C3C (void);
// 0x000006BD System.Reflection.PropertyAttributes Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_Attributes()
extern void MemberAliasPropertyInfo_get_Attributes_m33801C43E68C13DD2DD3EB038A74BC1B7B580CCD (void);
// 0x000006BE System.Boolean Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_CanRead()
extern void MemberAliasPropertyInfo_get_CanRead_m91187BCD842EF6D3D735896D034B0DA3CEFE6DD7 (void);
// 0x000006BF System.Boolean Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::get_CanWrite()
extern void MemberAliasPropertyInfo_get_CanWrite_m45A4127C64DC385ED90DEA567867FDF130C00A29 (void);
// 0x000006C0 System.Object[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetCustomAttributes(System.Boolean)
extern void MemberAliasPropertyInfo_GetCustomAttributes_mEC95B81CFFCD71D911EEE5261178A7B6C8D7ED33 (void);
// 0x000006C1 System.Object[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetCustomAttributes(System.Type,System.Boolean)
extern void MemberAliasPropertyInfo_GetCustomAttributes_m681684AF9EB34EDAE940E86B6E1FFE90FD697677 (void);
// 0x000006C2 System.Boolean Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::IsDefined(System.Type,System.Boolean)
extern void MemberAliasPropertyInfo_IsDefined_mAA506EF00AEFE9E6247035409396F9FA93017F68 (void);
// 0x000006C3 System.Reflection.MethodInfo[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetAccessors(System.Boolean)
extern void MemberAliasPropertyInfo_GetAccessors_mA99FB37BC5D56BF7A1782F70C85A13270364D2DA (void);
// 0x000006C4 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetGetMethod(System.Boolean)
extern void MemberAliasPropertyInfo_GetGetMethod_m711D930F7F61FE2BD71B33E4DA2B9DBB2EEF6CD4 (void);
// 0x000006C5 System.Reflection.ParameterInfo[] Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetIndexParameters()
extern void MemberAliasPropertyInfo_GetIndexParameters_mC609DFF130C7919A91A67D9BEBAB43A98093AEDC (void);
// 0x000006C6 System.Reflection.MethodInfo Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetSetMethod(System.Boolean)
extern void MemberAliasPropertyInfo_GetSetMethod_mD4AA6242FEFB26F3378CD8AD21B1ADC377DF2D98 (void);
// 0x000006C7 System.Object Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasPropertyInfo_GetValue_m6DAF26ACCA5C234675D6C9C58BE77A46E567C10A (void);
// 0x000006C8 System.Void Sirenix.Serialization.Utilities.MemberAliasPropertyInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern void MemberAliasPropertyInfo_SetValue_mDC965AFD3ACE84A8BA7D8834A3CA395B19CEA0D2 (void);
// 0x000006C9 System.Boolean Sirenix.Serialization.Utilities.ReferenceEqualityComparer`1::Equals(T,T)
// 0x000006CA System.Int32 Sirenix.Serialization.Utilities.ReferenceEqualityComparer`1::GetHashCode(T)
// 0x000006CB System.Void Sirenix.Serialization.Utilities.ReferenceEqualityComparer`1::.ctor()
// 0x000006CC System.Void Sirenix.Serialization.Utilities.ReferenceEqualityComparer`1::.cctor()
// 0x000006CD System.Void Sirenix.Serialization.Utilities.UnityVersion::.cctor()
extern void UnityVersion__cctor_m109F33D254286A97D4AAC1CFFF08D822C27B2864 (void);
// 0x000006CE System.Void Sirenix.Serialization.Utilities.UnityVersion::EnsureLoaded()
extern void UnityVersion_EnsureLoaded_m38F0696D643531BF201CD3DC509423ECEF98FCAB (void);
// 0x000006CF System.Boolean Sirenix.Serialization.Utilities.UnityVersion::IsVersionOrGreater(System.Int32,System.Int32)
extern void UnityVersion_IsVersionOrGreater_mB393B211415A7E3318A2D0E077524C6DA99E1470 (void);
// 0x000006D0 T[] Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StructArrayFromBytes(System.Byte[],System.Int32)
// 0x000006D1 T[] Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StructArrayFromBytes(System.Byte[],System.Int32,System.Int32)
// 0x000006D2 System.Byte[] Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StructArrayToBytes(T[])
// 0x000006D3 System.Byte[] Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StructArrayToBytes(T[],System.Byte[]&,System.Int32)
// 0x000006D4 System.String Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StringFromBytes(System.Byte[],System.Int32,System.Boolean)
extern void UnsafeUtilities_StringFromBytes_mA622CC1B81E541F113BE52B88191FE9F4D0F8563 (void);
// 0x000006D5 System.Int32 Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::StringToBytes(System.Byte[],System.String,System.Boolean)
extern void UnsafeUtilities_StringToBytes_mF86FA8885DC3CB27B719D91EA4DAEBA36776C6BF (void);
// 0x000006D6 System.Void Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::MemoryCopy(System.Void*,System.Void*,System.Int32)
extern void UnsafeUtilities_MemoryCopy_mD475484BEAC5A72AAAA5D6C8A74C8EB8789FBD34 (void);
// 0x000006D7 System.Void Sirenix.Serialization.Utilities.Unsafe.UnsafeUtilities::MemoryCopy(System.Object,System.Object,System.Int32,System.Int32,System.Int32)
extern void UnsafeUtilities_MemoryCopy_m6975422A2EF04A36117833CBF7EBB194E4F98E1C (void);
// 0x000006D8 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m415FE5942EE0C0FB2CD006F84FCCC802362FCF5A (void);
static Il2CppMethodPointer s_methodPointers[1752] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializedBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_mC289B8801FDF8F538F96BC30F496497CFD73E98C,
	SerializedBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m586726976E23BEDECD8A1B5F6966CE4C10C00899,
	SerializedBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m8B89639B50D750CCC8F4CE6B9EE04D3E6B7677D2,
	SerializedBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m7F62DB4AF6FBC223B2CE83C679109DBA62DA13C5,
	SerializedBehaviour_OnAfterDeserialize_mDFAF7ED43A889F0590AD84D8E73132EFBE2BC944,
	SerializedBehaviour_OnBeforeSerialize_mB764AA3AAEE6D7FEF69C2AAD9C5A2140506D3CE3,
	SerializedBehaviour__ctor_m356D243E69270532DA25BC4E9D1BA39D002F079D,
	SerializedComponent_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_mA417BCE93AE8D9218DDADAE579E86940A69FB773,
	SerializedComponent_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m3007D4F2B2F7FABC9CE117EA27C97902A5CA8FAE,
	SerializedComponent_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m9A6452B6E76E6DB9AC1EEA161BFD0C63644335CC,
	SerializedComponent_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mD6E78C9466D264C014C79D51E0E7506584F42EEE,
	SerializedComponent_OnAfterDeserialize_m6C617D0E732FE23EF644C55621AA5D920F291B58,
	SerializedComponent_OnBeforeSerialize_mD47EFA734935428100551183EB29BB827A91A3B9,
	SerializedComponent__ctor_m5F48BCEECDBE9924054BC923745E3E126AB6825A,
	SerializedMonoBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_get_SerializationData_m81DE44BD44D84BCF0E0FBC38315BF962FD6F93B7,
	SerializedMonoBehaviour_Sirenix_Serialization_ISupportsPrefabSerialization_set_SerializationData_m4A7F7815A78A0E1D35B096A102EA785B75AF610E,
	SerializedMonoBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m4F21BFA951B3C6FBD3CBEF9B6BBB5B2837AF22EA,
	SerializedMonoBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m7EE94F9B77D5CB970456BC67765BA34FBB08CB45,
	SerializedMonoBehaviour_OnAfterDeserialize_mEEB6C905BC4A1F25CA377C0838CA7DA4F2A97ABD,
	SerializedMonoBehaviour_OnBeforeSerialize_m5CCD7D67DDBD0C28F72B67D023435EB54FD6C900,
	SerializedMonoBehaviour__ctor_mF2FF2DB3A67E9155B289B960A2D8E6C33AF9E354,
	SerializedScriptableObject_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m0BC63966B961B50811BE971F742E18C5377D2B99,
	SerializedScriptableObject_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m5B452F29FE36060E037D727B86CD2C5FA5EBF12D,
	SerializedScriptableObject_OnAfterDeserialize_mCCFBF5C89F560349721061B1AA2A4191BC05C3E5,
	SerializedScriptableObject_OnBeforeSerialize_m98430D2E2FAB68C725366EE3AEFBEE8A792E7CC1,
	SerializedScriptableObject__ctor_m163A094F76CE18ACBBC26ADF30FEE341A7A5CF71,
	SerializedStateMachineBehaviour_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m45BED8434DC034C3C83DA1178BF4655D011EBCB6,
	SerializedStateMachineBehaviour_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mE4AC59B703E69A064129030C07DE023D98155D70,
	SerializedStateMachineBehaviour_OnAfterDeserialize_m03477ACAD9189F7DAFFA688C56395C225ADFC928,
	SerializedStateMachineBehaviour_OnBeforeSerialize_m26BB1F10BCCAA5ACA995C9865DDEF3431AC2D334,
	SerializedStateMachineBehaviour__ctor_m8E3801F75444E3DCC5871C9F1AB481D1E1546E37,
	SerializedUnityObject_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m05E7F651B93DFB6D4679F57CF790BC5CFA929930,
	SerializedUnityObject_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mB90A211A2583217EEE116B2CFDA518CEFCCEED70,
	SerializedUnityObject_OnAfterDeserialize_m621CCAB08FA678BBD56F2C0D84D22D079CAD9CED,
	SerializedUnityObject_OnBeforeSerialize_m9C862889F19ADF4DBE5C5F5F1D091865AA286050,
	SerializedUnityObject__ctor_m1D8701D969719F7356869A37DC28E5E9408DE981,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	VersionFormatter_GetUninitializedObject_m5DED5D826FA2D2DA98C0AD2BB013DA4D6C6A03A3,
	VersionFormatter_Read_m0452207D96D195E7BF3722F5981B1A680995EC62,
	VersionFormatter_Write_m14081079EC7769C626631431AB69EBD93B71437E,
	VersionFormatter__ctor_m1F0C803A8982E1B816351279D2A02F18BE8CE232,
	AllowDeserializeInvalidDataAttribute__ctor_mFFF4786F2649B69939A6D1B47D3E5083A2D0FAAD,
	ArchitectureInfo__cctor_m692CCE538DFA897EE1885AA0CAE9CFF86C56B188,
	ArchitectureInfo_SetIsOnAndroid_m15ADE609814605F039081310CC3B339B03D3D195,
	ArchitectureInfo_SetIsNotOnAndroid_m2B12FABB59E301E7B56CFEF6D79E81DF00AF2B62,
	NULL,
	NULL,
	ArrayFormatterLocator_TryGetFormatter_m8C34CBFCB8FC58A022B322D5562751ED58264FA3,
	ArrayFormatterLocator__ctor_m41BD5C69A3B30FB2ED65AC5E8EFD651F1E34D597,
	DelegateFormatterLocator_TryGetFormatter_mCA7DA7565932BAA967FBE64F4F63C12EFF1BC990,
	DelegateFormatterLocator__ctor_mD4BEAC1E9DD546B9222B5BA3BED7B3B836FD0A14,
	NULL,
	NULL,
	GenericCollectionFormatterLocator_TryGetFormatter_mE145653AF6915E25A2B6BF06E41376A636448660,
	GenericCollectionFormatterLocator__ctor_m900B5B9E7AF1AFA077807D88D19449CFAE150C82,
	ISerializableFormatterLocator_TryGetFormatter_m79DFE5A7D6182AA34FD65DD869752C4821580932,
	ISerializableFormatterLocator__ctor_m7234E223207BCED84989670AA09019272647D6D9,
	RegisterFormatterAttribute_get_FormatterType_m1C17ED27D02A49CF77B26EAD119E1A6BD53B3774,
	RegisterFormatterAttribute_set_FormatterType_m103C9B8E2F00DD395423F02905FC2C9526439F2A,
	RegisterFormatterAttribute_get_Priority_m058B73DFB7DD5061A54BAC4668D169CE59B517DD,
	RegisterFormatterAttribute_set_Priority_m1BD98F9B65102165E4C73855D8C1DF0CB5BF3A52,
	RegisterFormatterAttribute__ctor_m332F9BD3DD7DCC8F40137D3121F75AE2E12DC23F,
	RegisterFormatterLocatorAttribute_get_FormatterLocatorType_m1CC4E212C4AC33E6970C9D25E5BE73238A45BDB1,
	RegisterFormatterLocatorAttribute_set_FormatterLocatorType_m64378818193092212C1B5A56F1A4177C685C046F,
	RegisterFormatterLocatorAttribute_get_Priority_m8ADAD66CF0E823C054ABEAC48FDEC5673D33D373,
	RegisterFormatterLocatorAttribute_set_Priority_m87C8053D211AB6E47C71D3AAF8320D0436C49629,
	RegisterFormatterLocatorAttribute__ctor_mFE77FF1296D532E4B067CF3A7313355B258F49CF,
	SelfFormatterLocator_TryGetFormatter_mC7700F7994BD29E2E432BE3BB398DF1C28ECFB29,
	SelfFormatterLocator__ctor_m2DFBDEB40459F426EF1D520739761D56B59570C8,
	TypeFormatterLocator_TryGetFormatter_mFFA8422E40A034B1C206879D28A56FD5D5AF2342,
	TypeFormatterLocator__ctor_m19B687011F899AD0CEACB819F052C9039D885C90,
	BaseDataReader__ctor_m256753B94B198C37A18F99AB3E00D012DB0A67B9,
	BaseDataReader_get_CurrentNodeId_mC6F23729B5DFA64E14E406B955ACA5B254B75013,
	BaseDataReader_get_CurrentNodeDepth_m17D19783D54C4270601ADE7A3B5E7E7FC3FD53E6,
	BaseDataReader_get_CurrentNodeName_m36F3D74FDC23FB8BE63B9D0AD02DCAA33D38403C,
	BaseDataReader_get_Stream_m0B8E2A821FBFD53339BD694838A6D63071A906D9,
	BaseDataReader_set_Stream_mD9A087F8758320D8C3B31CCEA70F0BFB512F7D54,
	BaseDataReader_get_Context_m0D563DB48CC3EFD72D62B7B571C855E511EF77AF,
	BaseDataReader_set_Context_mDF7C1D3E3C8332B38DA9BFF73CCFCF98A4D0BC44,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseDataReader_SkipEntry_m10955099B4E1F766C3837AD40E6F02BF682844B8,
	NULL,
	BaseDataReader_PrepareNewSerializationSession_mF6AF3B535901BA601B2CBFF20563557FFDA3FAC5,
	NULL,
	NULL,
	NULL,
	BaseDataReaderWriter_get_Binder_m39DA92F0A5E7DA06EF49C1934D35E9966C61574A,
	BaseDataReaderWriter_set_Binder_m1C468BD577D8B4E008D4F2E59BB5036D596BDE24,
	BaseDataReaderWriter_get_IsInArrayNode_mCBFA18B413E9F33644D948D4BFAFE45DCE985309,
	BaseDataReaderWriter_get_NodeDepth_m6AB0F093329721AA6090479F46C820CD71EDA423,
	BaseDataReaderWriter_get_CurrentNode_m2B4FAFE63F62D605EA764E5B190407D616CBBF1F,
	BaseDataReaderWriter_PushNode_mCA60B6F3D84333621B2273D29EFA93E3FF0FDBFB,
	BaseDataReaderWriter_PushNode_mEBBA2F2A706EF74847F1DC4F14ED912FAB9EC8A1,
	BaseDataReaderWriter_PushArray_m9F29E9F0CAB3669AB9C24CFC3E78327DC439C8F2,
	BaseDataReaderWriter_ExpandNodes_m8206869312402DF7F119526B979376A60B7D8457,
	BaseDataReaderWriter_PopNode_m0FA87E4A14793A8E06D1668D4ADAB9844C53C135,
	BaseDataReaderWriter_PopArray_m6430ED605E93A2D89C0BFE7721075D2164FC6B9D,
	BaseDataReaderWriter_ClearNodes_m4D867C6DDA7F78E5881903876FB17E7CD821EB63,
	BaseDataReaderWriter__ctor_m5BC5AF02E5D64FD49CB2BB2D0DD5541500666590,
	BaseDataWriter__ctor_mAD50DC5F7248B12D05EF365F9BADA914712BE01D,
	BaseDataWriter_get_Stream_m8AD1108B366507F290BE556EDF1CA06874F010BB,
	BaseDataWriter_set_Stream_mE72B376E6485FF218E81A23C720DB1DF3928E204,
	BaseDataWriter_get_Context_m2C473D483A6F6A53F8D3C1A7236E7DF027C74C57,
	BaseDataWriter_set_Context_m3CD791A13D75824C0D87124A37234CC7A407C40A,
	BaseDataWriter_FlushToStream_m3E58B8F046C907089AC66237C16BD3BE86D5082B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseDataWriter_PrepareNewSerializationSession_mC7F69114FC5086BEA0F3E06A12B3E4BCE26AE3EC,
	NULL,
	BinaryDataReader__ctor_m05150B03F07E1347650C2AF9A1D20B5991BDD850,
	BinaryDataReader__ctor_mFDD96108B2BFD1AB4CD1991525AB8904E86FAC35,
	BinaryDataReader_Dispose_mC7BDE37C48E5E7EF1391B8AEC47F7326947E3810,
	BinaryDataReader_PeekEntry_mEC799EA1CC942523C88AF4669265E78EF095EC96,
	BinaryDataReader_EnterArray_m41BB6D5EA1C76904028F79F5BD6644BE354C3DDD,
	BinaryDataReader_EnterNode_mF82CEEC287E24BD0966CB343107470587DE20813,
	BinaryDataReader_ExitArray_mF7CFD5166FA17CEF7F9899A82E4A0118D8319B88,
	BinaryDataReader_ExitNode_m2EC7D442B3385FC322641E4D45BFDD9D4CFE305E,
	NULL,
	BinaryDataReader_ReadBoolean_mD7F1DB823D02DF29F25AF01262775D52939D24A6,
	BinaryDataReader_ReadSByte_m48F1429088A24529BE223548D360A460CD88326A,
	BinaryDataReader_ReadByte_m8D47EDF23AA7D0CEC48D14FB5545BD436A6CF5B1,
	BinaryDataReader_ReadInt16_mCA2DF35896428C31996EEB22CC11BFBB6C6ABCC6,
	BinaryDataReader_ReadUInt16_m632C99D11618FD895D775FAC821F512CA818151F,
	BinaryDataReader_ReadInt32_m09C1594A33027EA4BB5DC52B6CFCC6802F2541BE,
	BinaryDataReader_ReadUInt32_m5FBF556B59431A9AB172186B9BC1B950B812715D,
	BinaryDataReader_ReadInt64_mEB5EC49D6123C1674B1C222A5BF5DF95EB20CBA5,
	BinaryDataReader_ReadUInt64_m9CF4EA5C13AE1320D2215AE17D24C6FC4013E44A,
	BinaryDataReader_ReadChar_mCD0BC343F96BEBF65A7DFF78D7542DD282025B7F,
	BinaryDataReader_ReadSingle_mD68DD096FBAB474D89A317C84601C750C790BC07,
	BinaryDataReader_ReadDouble_mF1682719F3267CECBF0B03FD9D3E415FA5A97A1E,
	BinaryDataReader_ReadDecimal_m727CD887A65D823FF0B71704FF3D70838AA445F2,
	BinaryDataReader_ReadExternalReference_m4EB2BC7CFD6BE53D13A163F91549C96F7D08BDE6,
	BinaryDataReader_ReadGuid_m69E91E179A7CDE8C7A1514A980EE46015B71E5C0,
	BinaryDataReader_ReadExternalReference_m3783E471805C048E2E6867C83D8DA28BF3161C06,
	BinaryDataReader_ReadExternalReference_m16E5FFE6DBD5F11F9571ABA820A5B797BC7CE495,
	BinaryDataReader_ReadNull_mF1E81D13C67DB44403256DB0D3428F7E31EC8D80,
	BinaryDataReader_ReadInternalReference_m0C40DF6875D1BF4724C2C37F69BD72C2168D9BB9,
	BinaryDataReader_ReadString_m1D6906C020609C2FF04B3D571E247AB37226A4DB,
	BinaryDataReader_PrepareNewSerializationSession_m702B8BF4D463BD73FAF5EEFF804CC80CBEEA9006,
	BinaryDataReader_GetDataDump_m74E44CF70297CBC6A5A611973835C1C5C9C3CD08,
	BinaryDataReader_ReadStringValue_mA9DF0F142F9786152F12719D972773CD2451E6A8,
	BinaryDataReader_SkipStringValue_mEF77DDFED47E4027B7DFF9640C110B019F7DC3AD,
	BinaryDataReader_SkipPeekedEntryContent_mCB193D7120CC6E432B5F36C2538F57BD3DB185C5,
	BinaryDataReader_SkipBuffer_m9A5FE0AABAFDA581BA66B4A908FA92F7ADF7F43F,
	BinaryDataReader_ReadTypeEntry_mF9D131780191BBE416F74C863A74F1AA70238A4C,
	BinaryDataReader_MarkEntryContentConsumed_m376D33AB9CBFA74D2B2AA90702C179B8EF4F09BF,
	BinaryDataReader_PeekEntry_m22654373898FF434F0E3081F7B9D1ACCFCDEA39D,
	BinaryDataReader_ReadToNextEntry_mF8440534928AB759B5BBB9CEC6D0EA50995C05E2,
	BinaryDataReader_UNSAFE_Read_1_Byte_mE414283A399102404054B59F6E203D7E28F4AE48,
	BinaryDataReader_UNSAFE_Read_1_SByte_m7E94DA7F6E44246B9B9BAA5C1A52A4DCAE8ED88C,
	BinaryDataReader_UNSAFE_Read_2_Int16_mCDF250F2DD9D3DB5C6D45DBC0004844D26A2AB0F,
	BinaryDataReader_UNSAFE_Read_2_UInt16_m36661B82546F41BBC9FD3F923F8BAFB486C711BB,
	BinaryDataReader_UNSAFE_Read_2_Char_m0B54BB77A1FE3D2A726FCA81A387659C488351EF,
	BinaryDataReader_UNSAFE_Read_4_Int32_mAB8D01448B84864A13CAA127683DA0A4FA6BD43F,
	BinaryDataReader_UNSAFE_Read_4_UInt32_m3FC70B82A7E2AA55B5F1D3FF04D53AD4D1AB78CA,
	BinaryDataReader_UNSAFE_Read_4_Float32_m18E4D26C4679AD4F0405073E2AEE0BB08655E327,
	BinaryDataReader_UNSAFE_Read_8_Int64_m975676DF5F8DD88B654DDC6D141FCB9579613589,
	BinaryDataReader_UNSAFE_Read_8_UInt64_m35973ECCD27EFD7A73686C09DD8169B01AAEA639,
	BinaryDataReader_UNSAFE_Read_8_Float64_mE2B2AD4E4B2232DC70ED3D9F52A6936BE84C25BF,
	BinaryDataReader_UNSAFE_Read_16_Decimal_m65C544A7D5C7D00B0ACDBB3E059C28A10255E312,
	BinaryDataReader_UNSAFE_Read_16_Guid_m58B05CA30EAA5DD6BC7AEB8B003753CB02CE09D0,
	BinaryDataReader_HasBufferData_m68DA87BB787EB364B3FD91F51F50B0D5A8126690,
	BinaryDataReader_ReadEntireStreamToBuffer_m659E0ECFF6498FA11AE2C2BBBBBEB12B13613474,
	BinaryDataReader__cctor_mD90112E0AF3AFFFDA5B561BB3018888EB1E00411,
	U3CU3Ec__cctor_mC23584B0D0FB4ABA47CC4D2EED92786EC7DC92D1,
	U3CU3Ec__ctor_m3310EEB1994E1D9AABBF3418A7BC8339C839A176,
	U3CU3Ec_U3C_cctorU3Eb__64_0_m03794B435B268991B2EA5541F76BD9334A106194,
	U3CU3Ec_U3C_cctorU3Eb__64_1_mCE0C81C34DBFE39D0FC2E523F20CC3C97C1E51D3,
	U3CU3Ec_U3C_cctorU3Eb__64_2_m3035E901D228B2660A434C68E813F397D2B9D41E,
	U3CU3Ec_U3C_cctorU3Eb__64_3_m04D9427995E281CAEFE2F8B9FCF735273659E0F3,
	BinaryDataWriter__ctor_m0D918FDED9B75527D6500D5EDEC68127C9BA3A51,
	BinaryDataWriter__ctor_m737C390018E3E5B9B6E9787056F4336C15E45D8F,
	BinaryDataWriter_BeginArrayNode_mAF5FF0268104DBD16E2E6C6752C63215B322F2FE,
	BinaryDataWriter_BeginReferenceNode_m3C601B5A7F8CB0A494BC66CF9B386BD621960D38,
	BinaryDataWriter_BeginStructNode_m757F5E00FF8D3D9D5BC8E9B289C74E7B58A79BAF,
	BinaryDataWriter_Dispose_m9CFBEF57700C58F8848A5B0AE120C002D788991B,
	BinaryDataWriter_EndArrayNode_m5D6A0D542BD0ECC1853C621DB47A981970813249,
	BinaryDataWriter_EndNode_mD784EFBB0C54CC8551360B85E2488B291B46B48F,
	BinaryDataWriter_WritePrimitiveArray_byte_m19F289DBE9944027C6BBEE60D704007CCAE10401,
	BinaryDataWriter_WritePrimitiveArray_sbyte_mFD03433E1DFBB176AC088FFB79D6F3ACCBB40FD3,
	BinaryDataWriter_WritePrimitiveArray_bool_m13D7BA99370F11BC66C7263CF2DA284A5DB67D69,
	BinaryDataWriter_WritePrimitiveArray_char_mD7947980505195A13C60CDC137CE6C5D04E3C684,
	BinaryDataWriter_WritePrimitiveArray_short_m964D571FA8D76B2033ECC0EDA7C54AE421336C56,
	BinaryDataWriter_WritePrimitiveArray_int_mF99AB82AE5F911F838F50554DFB8263B53E75FFF,
	BinaryDataWriter_WritePrimitiveArray_long_m612F9F5DA61FD84A6B0A4F701ABBDC62C8C57B2C,
	BinaryDataWriter_WritePrimitiveArray_ushort_m968A14AAA1A562D21756517A21CA6CCEB0AF2476,
	BinaryDataWriter_WritePrimitiveArray_uint_m0280C0AFFD9DEC8BC20F34D40AC69FDFFF2C141D,
	BinaryDataWriter_WritePrimitiveArray_ulong_mD319B0781D26C8109FD3664BCDF1CDD8430D8F43,
	BinaryDataWriter_WritePrimitiveArray_decimal_m92BA283AF2C8E29B16A9C31FE8021D57F8FA0BF1,
	BinaryDataWriter_WritePrimitiveArray_float_mF23EB6CDE73979D2F3E2CCE094DE6B38F252431A,
	BinaryDataWriter_WritePrimitiveArray_double_m62FD1CA79DAA55C783B73F19B19B1D940DC26C98,
	BinaryDataWriter_WritePrimitiveArray_Guid_mCE593C8BF2896B8FCC77F140E1629B8793C0AF74,
	NULL,
	BinaryDataWriter_WriteBoolean_m72DE5862429EBDE23F7E6B97227CA74BBF6001C1,
	BinaryDataWriter_WriteByte_m62934475FEA8B7088F9237E5DFBA065034C0FD4F,
	BinaryDataWriter_WriteChar_mA7361D660282614EDBDB2272FDBD1FE5DF6DC0F9,
	BinaryDataWriter_WriteDecimal_m792615D5E9084425DD8EA9FA11357E878BB1842F,
	BinaryDataWriter_WriteDouble_mD25F3519EF7C75AD336EB91E7674D56D6F9BE56C,
	BinaryDataWriter_WriteGuid_m23CBE7612D490FE1EA4EF0734C560F593FA9A4AF,
	BinaryDataWriter_WriteExternalReference_m82ED5C5C4D26F662C57BFE840FF03B5719CC6CA5,
	BinaryDataWriter_WriteExternalReference_m35BAB0BFF468EC32A758E6BA20E5DEAFDF7F832B,
	BinaryDataWriter_WriteExternalReference_mD081F90EA74F0DC891B218C27AD6C0272F88BEF3,
	BinaryDataWriter_WriteInt32_mA2E73CB755191705A27E6AFC622B49F3B30C505D,
	BinaryDataWriter_WriteInt64_m8BF748DC039F5C1C71A7323A09DC8C2C3F8BC53A,
	BinaryDataWriter_WriteNull_mC6DC08FFA6792D1F6644855CB9EC58D75D55BC2F,
	BinaryDataWriter_WriteInternalReference_mB0FBC9DBCF400728252E33C7F9903877F9617227,
	BinaryDataWriter_WriteSByte_m71E36C7CA56BC9D36CB426798997A1F3284083F2,
	BinaryDataWriter_WriteInt16_m63CC1F32C8F68FFC310EEF222C43CA2A9651A698,
	BinaryDataWriter_WriteSingle_mE7FD19AE455C0C5DEABA9B2C461006C0268E6676,
	BinaryDataWriter_WriteString_mDDE36BC61EA507A8530A3D75AECBF214C6357AB8,
	BinaryDataWriter_WriteUInt32_mE89913083C8B32CE52197FB3E2825F8AD25CC258,
	BinaryDataWriter_WriteUInt64_m1C2258628A2D94F89C710E0F37C1AF037E9110E7,
	BinaryDataWriter_WriteUInt16_m471F5A5BB991243188F3BC33BC16090E7B18433D,
	BinaryDataWriter_PrepareNewSerializationSession_m91A2C1DA1CC64D0A4D975C24DDA9BA327002AED9,
	BinaryDataWriter_GetDataDump_mEDB2849B05CF37EDD48AAEF65EC21E6A81DE680D,
	BinaryDataWriter_WriteType_m0FEB9FBA84E83C88797A8B4A47791B8D16AA5A91,
	BinaryDataWriter_WriteStringFast_m30753140E9C0E748398E1BD8D50E3A74192908F8,
	BinaryDataWriter_FlushToStream_mDBC9FD9D630949FF549A44B3B32F91536ACD5FD2,
	BinaryDataWriter_UNSAFE_WriteToBuffer_2_Char_m796361C5552DD7F16C8FD4CF105BD7389F9805B6,
	BinaryDataWriter_UNSAFE_WriteToBuffer_2_Int16_m68DB329368407F259FFBC69DF2369A70A86DAFD1,
	BinaryDataWriter_UNSAFE_WriteToBuffer_2_UInt16_m4AE7098D2560C06ECC01871E22288CE3500AB8BE,
	BinaryDataWriter_UNSAFE_WriteToBuffer_4_Int32_mBD098D9F48459BCB06D4B08F58B9DCBC4F6C4364,
	BinaryDataWriter_UNSAFE_WriteToBuffer_4_UInt32_mF435F48F9C66419E089D85479E55AD0425488AE4,
	BinaryDataWriter_UNSAFE_WriteToBuffer_4_Float32_mB4098723BE3E6433556523F545F1A6865C439A58,
	BinaryDataWriter_UNSAFE_WriteToBuffer_8_Int64_mAC5313176EA0BE00EF040FFC773AEEE9B0654B1B,
	BinaryDataWriter_UNSAFE_WriteToBuffer_8_UInt64_m37FD9AEAA7624553DADAB198BD44A74BF2669A98,
	BinaryDataWriter_UNSAFE_WriteToBuffer_8_Float64_m426B920250E1A93C94C8E768938C7322A04A002E,
	BinaryDataWriter_UNSAFE_WriteToBuffer_16_Decimal_m0A18EE380070D4B634DECD535A988EE163B0DC8C,
	BinaryDataWriter_UNSAFE_WriteToBuffer_16_Guid_m20DF6E596922869DDF8AEF20CE91BAC8B9113BE7,
	BinaryDataWriter_EnsureBufferSpace_m6897067A9BB56F0501FAD23740CCC2340D2FE1CD,
	BinaryDataWriter_TryEnsureBufferSpace_m7FBDCDC9E2544CD317B26B2094798F76B0CF022E,
	BinaryDataWriter__cctor_mFB19B146BC3F3FBC8EB7B467DEC6A24B4C1478D5,
	U3CU3Ec__cctor_m139D2B56C8CC8C7BDD564B61BA50B6D2BE08F716,
	U3CU3Ec__ctor_m4E98F875587A4D1CFD873A6CBE0E1FBB7528B992,
	U3CU3Ec_U3C_cctorU3Eb__70_0_m76566D480A9798A22C138F73F72ADC0C89CE0D27,
	U3CU3Ec_U3C_cctorU3Eb__70_1_m514D7B30BE6C40315AE5E12FCDAE5A428EE9C193,
	U3CU3Ec_U3C_cctorU3Eb__70_2_m039A1A54A1217FD30A6E8117ADCF0806552B7BBA,
	U3CU3Ec_U3C_cctorU3Eb__70_3_mE736C9227AC4DF0CAF003D8F926FCD48EDF5C257,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonDataReader__ctor_mDF271315EE4ABBA622C1C0E84FD1BE6FED51A9A7,
	JsonDataReader__ctor_m91D72BBB37E0AB41E760C2AD88D5EF0ACB75550D,
	JsonDataReader_get_Stream_mF71DEB758403110A2E8B9AEA85B5B4D4DD4F40A3,
	JsonDataReader_set_Stream_m828F0713E040E39BC146EEC04A407920D1070DA6,
	JsonDataReader_Dispose_m8A6B163CFE2246A35BD4BAC63831FCF20D44638B,
	JsonDataReader_PeekEntry_m262346079D8FFB9CB842E6B97DBECCEA8469A8AE,
	JsonDataReader_EnterNode_mE8C8C7651AFF98C83F6512E769EB9ADD1D727C76,
	JsonDataReader_ExitNode_m7E5823D7638D9EA470B1AD0CD3A99EBD9D4F7B4D,
	JsonDataReader_EnterArray_m770CF1B56B21C066D6D5DEB08733715A1FA11CBC,
	JsonDataReader_ExitArray_mC4310417340450801D726ED89E1F364846616844,
	NULL,
	JsonDataReader_ReadBoolean_m0CE136A4B8904C2C8361CB69A6C52A7090BD5934,
	JsonDataReader_ReadInternalReference_m501B32CC37D54A367B232EED7EFAB5D4507C9E5C,
	JsonDataReader_ReadExternalReference_m01F1E7DD28A3BF7D4B269DBE781A50F1074B2B30,
	JsonDataReader_ReadExternalReference_m2807AA3E7D0F82C570B247CDC7D11E67C423EC0E,
	JsonDataReader_ReadExternalReference_m7C0B95610CA097020A4AD8966D6FE29F016ABD15,
	JsonDataReader_ReadChar_mEB620061C2EF28EC502CF5DCF2FD8B491A76F14C,
	JsonDataReader_ReadString_mB9181C23108596F438552081E1F7A955AAC19689,
	JsonDataReader_ReadGuid_m11C63A3BD5A66B63E17E0359C808DF96881B5213,
	JsonDataReader_ReadSByte_mBACB809E2CDDA960177C7AAF68ECB8A89FF60ABC,
	JsonDataReader_ReadInt16_m405A178FF91E518AFB6A3852BA6B7ED61B92524B,
	JsonDataReader_ReadInt32_m37A38FBD86446C855C6B3548B6211194E73D80E9,
	JsonDataReader_ReadInt64_m09090F23226E8F1663315E855B76395A1621C0DF,
	JsonDataReader_ReadByte_mB22F366AAF3C437A87B3C3F550647FCC7AC3D9D4,
	JsonDataReader_ReadUInt16_m8C48104D77B02C32AE00477FAE432FFE625637AC,
	JsonDataReader_ReadUInt32_m9A95E6BCAC54D5CEDBD9A7A8E9E7B51BAA47EB60,
	JsonDataReader_ReadUInt64_mABE9B0300C4042D7115E02D43CCDD269B1130CFD,
	JsonDataReader_ReadDecimal_m38898323FFB2AC56969367E51A83CBC9C397CDF5,
	JsonDataReader_ReadSingle_mE27F3457DCAEA9C63578390DB46E7C36EC09024A,
	JsonDataReader_ReadDouble_m5794C351005C9039C4BFD9BA04DEB04CF0608C60,
	JsonDataReader_ReadNull_m979A946AC984A6CB18C50A2575B0A861466FDCD1,
	JsonDataReader_PrepareNewSerializationSession_m718FF1C6C4A8CEB92C2B4CD910100F34D23BD2A5,
	JsonDataReader_GetDataDump_m698C08C1404300BB68453D02D7D31FEB20040689,
	JsonDataReader_PeekEntry_mBF55454CA16E99107C796AB96429B3B252B12C68,
	JsonDataReader_ReadToNextEntry_mCE352ECD9B16F9A00D9C1BDA0A4E8706BB0C8BE0,
	JsonDataReader_MarkEntryConsumed_mD39A12986A07DF92F30B3742D2D192F1D23669C4,
	JsonDataReader_ReadAnyIntReference_m95D232FC6DF63482E14623734BBC4007D8B93789,
	JsonDataReader_U3C_ctorU3Eb__7_0_m218588B3EFD4554548306964F3A8BAE0B4581959,
	JsonDataReader_U3C_ctorU3Eb__7_1_m188443B13E34FC27EB89104A2160A8DA73F3C7D1,
	JsonDataReader_U3C_ctorU3Eb__7_2_mDF9455C87D245744AC3701F7E431943B46666CFE,
	JsonDataReader_U3C_ctorU3Eb__7_3_m5139CDCD9B6BD2E4B70D701599CF9E4AA026B118,
	JsonDataReader_U3C_ctorU3Eb__7_4_m88AB03D52301DFDC42A0F43FA349DBFB026FBAEE,
	JsonDataReader_U3C_ctorU3Eb__7_5_mC91C29DE50FDBF6833681748D115ACC7E9EBD724,
	JsonDataReader_U3C_ctorU3Eb__7_6_m902BCE00EF10AD6AA0D9AF52D1F30DC5B3B07174,
	JsonDataReader_U3C_ctorU3Eb__7_7_m3D0D542B17868052801875B90C72346360C7A056,
	JsonDataReader_U3C_ctorU3Eb__7_8_m66FCAFA00AF0501214FD38C9D3477C79A2CEBF64,
	JsonDataReader_U3C_ctorU3Eb__7_9_mCD6B498A61367D2140B8E31B2980C1BB56BB00C3,
	JsonDataReader_U3C_ctorU3Eb__7_10_m6702C8E963908EC4C515470123BF42719717A6F4,
	JsonDataReader_U3C_ctorU3Eb__7_11_m8E7D85C98DA1230A22B9685469F6EE0C3C786FAD,
	JsonDataReader_U3C_ctorU3Eb__7_12_mD4275FB977E0D09EF7C74389E0BC8FD36203E15F,
	JsonDataReader_U3C_ctorU3Eb__7_13_m0C2CE08229502CB233D3531D23A8D5155AA2532A,
	JsonDataWriter__ctor_mDA5B358207B9C0A0F436BF518363DEECBA25B4C1,
	JsonDataWriter__ctor_m0F74B5244CDA8B8E222F65565BAE2F5636BCBD1E,
	JsonDataWriter_MarkJustStarted_mBFA8FD42E3556BE58E53DDDCA358EE93ACD7ABBC,
	JsonDataWriter_FlushToStream_m8D68F10EDCB9CA1CEAA36AF56F467C74B13C20E6,
	JsonDataWriter_BeginReferenceNode_m711FCFC333C82CC53EC938A8E514140576C40243,
	JsonDataWriter_BeginStructNode_mFE7FC76AA8DFA9634B7A12AC6B02CE1F199BF842,
	JsonDataWriter_EndNode_m630BCAA44383DFB11040B126DEB8C7AFC1785CA6,
	JsonDataWriter_BeginArrayNode_m9B7348518E6FA8E461B52C122AA51A5B822840CD,
	JsonDataWriter_EndArrayNode_m85D36A3E34025FB032FA3CF86A55359C61CF628F,
	NULL,
	JsonDataWriter_WriteBoolean_mB998761FF11336A0FFE49E69BC766A8CA3034358,
	JsonDataWriter_WriteByte_mDE05D293893B9317249560DCB562AD2DF75276AE,
	JsonDataWriter_WriteChar_m6B0FEE74A2D0BBA6BE12A13591CC112907286D8B,
	JsonDataWriter_WriteDecimal_m3C149822DAF8BAC12DB6298142BD37F1B7465D10,
	JsonDataWriter_WriteDouble_m662AB0AAE7BEB1A2EE3EAA85FF26825F7C94CC46,
	JsonDataWriter_WriteInt32_m00AB4C24BD36896839BDA9AB2EA504354BC59995,
	JsonDataWriter_WriteInt64_m2C207D18185B7E5813A7EDE7675F25A6E394EB5C,
	JsonDataWriter_WriteNull_mD840D5C9F20BC206669E11651CA519A24CD06D46,
	JsonDataWriter_WriteInternalReference_mFBDCC3E38AFDD7E31127BBFADDD3383049D07297,
	JsonDataWriter_WriteSByte_m57754B12D58666DA2653F07D177D71B1B71D151E,
	JsonDataWriter_WriteInt16_m9181E7EAFEA72B3276ED1797241874C0025A64DF,
	JsonDataWriter_WriteSingle_mF79F3DF8315622B38F894D30B69DD7A44CDC9D5C,
	JsonDataWriter_WriteString_m20451A9F707A47C3D175994E3AA713CBB15D6B71,
	JsonDataWriter_WriteGuid_mFA22883317B946CFDABE2909E7A6342278E43BD3,
	JsonDataWriter_WriteUInt32_m64EC2E078D316B1417A16CFD81117F5C7ADE594E,
	JsonDataWriter_WriteUInt64_m4AB6D67D84707AC2C0F5E8FAFBD7807CC9B06067,
	JsonDataWriter_WriteExternalReference_mABF868BCB5FF6FD3E8B5FD332DD5AA1CC5C3A115,
	JsonDataWriter_WriteExternalReference_mDB4D37028750ABEBCEA5C3E1138DB62FD128B2F9,
	JsonDataWriter_WriteExternalReference_mEA20E95C82C1229691D01598A3DC7296578F91E3,
	JsonDataWriter_WriteUInt16_m7D528CFFBC029870858712CC70A505C6FAF797EE,
	JsonDataWriter_Dispose_mEEE7493717757A1CB911F932673E03AAD17F9D2B,
	JsonDataWriter_PrepareNewSerializationSession_m7A01209268539F2CA98E12A5D266256C9BDED269,
	JsonDataWriter_GetDataDump_mD095242222FC337DC1514E40231A31EEDBFDE71D,
	JsonDataWriter_WriteEntry_mD943A7A1D0B5691C1B5AA4D155CAE36DFE26BE4B,
	JsonDataWriter_WriteEntry_m1B692598FB39C2CF103A4CF921448329E42C51C6,
	JsonDataWriter_WriteTypeEntry_m8BC24D8F42E50CF75DC8082931E638301B6F871D,
	JsonDataWriter_StartNewLine_mCFA4E25E129AF561F3D5A969D2A5BF1822D18409,
	JsonDataWriter_EnsureBufferSpace_mE0C10023A845B161319E47781D77E52A971A0980,
	JsonDataWriter_Buffer_WriteString_WithEscape_m2F66F5C9A9B3E2C62ACFF9ECFEBB966B25B31610,
	JsonDataWriter_CreateByteToHexLookup_m51DCC5506A1A3176C0826BE930CCB2A2E7884910,
	JsonDataWriter__cctor_m8BDA0DF07378AB25AE6DA611EB26DAFF90FFE19B,
	JsonTextReader_get_Context_m59E7AEA4934FB3560774487C6404A94059C33598,
	JsonTextReader_set_Context_m069A333FD72B60850A4E39856CFD571EA1898C96,
	JsonTextReader__ctor_m1843426EC471BABDC14A87D2A297B1032A1B0201,
	JsonTextReader_Reset_m1CECC9FB31DCB8172C06F4594691160890403C07,
	JsonTextReader_Dispose_mFD4484D16E52E161015F4557834E8AD69BAC53C1,
	JsonTextReader_ReadToNextEntry_mC83678273262F242F967B34A5BEAB85A5A8DF121,
	JsonTextReader_ParseEntryFromBuffer_m12C306C5FA28AEAB1DFE07676BE7E430BAE109A0,
	JsonTextReader_IsHex_m5A8F12E098C6EAC094633F35D2402A6B8D72C3E2,
	JsonTextReader_ParseSingleChar_m793A1115438B6912B1A99D1ACA388C24F190F1F0,
	JsonTextReader_ParseHexChar_m696BE0355BE84FD8ECC8B604FD2B383D1DF8A844,
	JsonTextReader_ReadCharIntoBuffer_m38763E8F01FA37D618B1521A1A6C9A45FB3FCB9E,
	JsonTextReader_GuessPrimitiveType_m0CA35E5D184DCC285E7F336DECD937E7E2987BEA,
	JsonTextReader_PeekChar_m58BE9FE4DAC82492F3690E7E13836E86AA5319EB,
	JsonTextReader_SkipChar_m0FD8FD46D2EB3580492EF817477ADA61FA75495A,
	JsonTextReader_ConsumeChar_m4DFD6C511B30FD201758722E1BDE07F9BE05B11F,
	JsonTextReader__cctor_mDFB109FB347AAD4A6462B3E890ECD773CED3E9FF,
	SerializationNodeDataReader__ctor_m6648CA4537E206ED1081F9668B67A0696877B1F3,
	SerializationNodeDataReader_get_IndexIsValid_m8FE7011B21B79506991120A31D20C1DCB60FBFB8,
	SerializationNodeDataReader_get_Nodes_mC85780EB00D20782B4627FE2153CF7851205840B,
	SerializationNodeDataReader_set_Nodes_m7CB44C695A15A439199727BD49E9358DE7AB84EC,
	SerializationNodeDataReader_get_Stream_m1E92D28139914D2A83C7F938E7862711A8907723,
	SerializationNodeDataReader_set_Stream_m1096375A0BB74727F1C557A96FFD3C594DBFF915,
	SerializationNodeDataReader_Dispose_m5A11DF980455E89E2CD85C0F124659E75D0EE007,
	SerializationNodeDataReader_PrepareNewSerializationSession_mF7B0F08B2DF5D08FF771A03DE5E36A28DE94E36F,
	SerializationNodeDataReader_PeekEntry_m5A9ECC288336BFB24A306EBDC2166CC0F4C2F7DE,
	SerializationNodeDataReader_EnterArray_m62DD77255A46BD88BF0C31B1EF520485BF38E5EE,
	SerializationNodeDataReader_EnterNode_m7ADEE1810EE729DD8E0C4E20A8E9F260CFD7BA74,
	SerializationNodeDataReader_ExitArray_m44BD69E047EC2484A6A9814076F9F999927CB0BC,
	SerializationNodeDataReader_ExitNode_m6C17226D73F8FA0A68F9C1E8B63DD21DB6D8F5ED,
	SerializationNodeDataReader_ReadBoolean_m12BF38B6B2A60421D8FF5ABC73284AD9FE16CCCB,
	SerializationNodeDataReader_ReadByte_m7615C7F6A69EC8F2B8E9426C13FD7460890D2C59,
	SerializationNodeDataReader_ReadChar_mB7AE9F49190F4FBDCE47F1859E86CC527D2F73C6,
	SerializationNodeDataReader_ReadDecimal_mA83E1B2C95A400F6EA8699F9682421522525BA25,
	SerializationNodeDataReader_ReadDouble_m04C1670981E0E60985F8F3135972690C334B0982,
	SerializationNodeDataReader_ReadExternalReference_m265339449A37FFA6C94D339B9A7B95A92DB7FEC1,
	SerializationNodeDataReader_ReadExternalReference_mDE013938578706F911D2697158CFF346AE4862EA,
	SerializationNodeDataReader_ReadExternalReference_mDD0BEA6B33E860ED147E77AFE888A5612BC7F340,
	SerializationNodeDataReader_ReadGuid_mA50A94F28279877CD5E8E0435D0CDADA88F3CFED,
	SerializationNodeDataReader_ReadInt16_m704974B5C7101FB8ACF3099D6F3A314E958B959D,
	SerializationNodeDataReader_ReadInt32_mAB615CBF0F9C03E3EE74B9A5C6862E3A4C88FC4D,
	SerializationNodeDataReader_ReadInt64_m48590BA88FC1D15D64AC9FFF1AC2FA4C4B4CDCF5,
	SerializationNodeDataReader_ReadInternalReference_m8CF90FAA481FF9D96ABDBD22071D41E0D4DAE7AA,
	SerializationNodeDataReader_ReadNull_mC772A3C988401FB15CD678D3A2F956F5513AC184,
	NULL,
	SerializationNodeDataReader_ReadSByte_m133CE0480AF9AC04C2CE42F070474CFB32FD3D22,
	SerializationNodeDataReader_ReadSingle_m7036400EB199C491BED2E5861652A03C55A5C398,
	SerializationNodeDataReader_ReadString_m950EB91C738A0FC547DEDCA7353E977DE50F15C5,
	SerializationNodeDataReader_ReadUInt16_mE6768104E2E72E9FB3AC1B60C7ECA34C78987B20,
	SerializationNodeDataReader_ReadUInt32_mE71430CCED6068A9529CAA82AE3C3452EDCBFB09,
	SerializationNodeDataReader_ReadUInt64_m3173800E9B438642C3D6D00458DA6DF1BEDCA5C0,
	SerializationNodeDataReader_GetDataDump_m495B2125BAED41EBEEAD33AD4D8749FEDC4BC8F2,
	SerializationNodeDataReader_ConsumeCurrentEntry_m3E8AD976F7F840229AFDF4FEB2FFBAA71C867C76,
	SerializationNodeDataReader_PeekEntry_mC2ECBF6A7579A16E27B44928B40B1AAD6DC6B951,
	SerializationNodeDataReader_ReadToNextEntry_m9AC6C12A36B2028BD2148D3B36D4D77B44DA02C6,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_0_m00F71BA12BF0DF57209A1FA5EB4AC74CBF1D7AD0,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_1_m7E0978EBA0D4C9280D6B13548E06017B2C489EEF,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_2_mEFA85D5013760DB520F6AA531A4BF42EEDA15E53,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_3_m152BBB040A5EAD6076048DC9489202C785226101,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_4_m5FA0551D5DEFD265D9A9B57F87A8BE34B521430B,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_5_m07D35A1038F73C248085CC9475E7218080ADA93E,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_6_mD3F37E0C19AC407882B4D84567A27741C932EED4,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_7_m1BCD4F188A91ECC086AE072A5614776ABE1ABAC2,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_8_m99831C0576687FD9A72616944CDC61CE44688C02,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_9_mA12EA1F7DAD28B8ACE68647E4968263354396D09,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_10_m1F1437E82B74BE9E17270D222A8933249E86825F,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_11_m2C8BEAAB1431C1A4BE9E8D96A707D19E8258E1AA,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_12_mD0490FAD60A6EF8DD4165446E007688F014464DA,
	SerializationNodeDataReader_U3C_ctorU3Eb__6_13_m3F7320E1559494FD26B6AC7265D7FA0C32622ADA,
	SerializationNodeDataWriter_get_Nodes_m7E58FCF62900D73580499C22934B6AB67EA3D788,
	SerializationNodeDataWriter_set_Nodes_m52B04D21A78DB5B6C0D8977FB876B879422FE395,
	SerializationNodeDataWriter__ctor_m944885959C772E5A5EF752FE5B74282B844B53C5,
	SerializationNodeDataWriter_get_Stream_mBF3169697F18E64086704369BF251DABE2394B0A,
	SerializationNodeDataWriter_set_Stream_m1417234035C2FBBF5B7183C82CEA3F5D725D37B8,
	SerializationNodeDataWriter_BeginArrayNode_mED91AC7F284211C36E0C53B30FD00C1646F65238,
	SerializationNodeDataWriter_BeginReferenceNode_mCE129C4021A7118D1934DDF9511067D4614159CC,
	SerializationNodeDataWriter_BeginStructNode_mAFE4F9152167A47F7AFEA2D6B569E9081FC48A1F,
	SerializationNodeDataWriter_Dispose_m1176C864F6D225175542AB7DA2BF663E513059D6,
	SerializationNodeDataWriter_EndArrayNode_mEC2688B6C27ECFB542F676F60D2A415FB889A841,
	SerializationNodeDataWriter_EndNode_m951F92C367060183BA4FD26BC752A537033AF8DC,
	SerializationNodeDataWriter_PrepareNewSerializationSession_m846F3AA9AFDD5566C2896C0C6E1D960EF1CA5117,
	SerializationNodeDataWriter_WriteBoolean_m6FCC384602536F8789B009D0C373C01F7308B90F,
	SerializationNodeDataWriter_WriteByte_m134531E41933302B98D06CF7BFB53D209DF53BBF,
	SerializationNodeDataWriter_WriteChar_m9537A34163133D7383221FD86A673C0A281FD999,
	SerializationNodeDataWriter_WriteDecimal_m6D6958267CD13C29D7B4CC2D7462363CD195DCBE,
	SerializationNodeDataWriter_WriteSingle_m16C5D22F9DC4B542B93FEE1CA0C05628762EEA59,
	SerializationNodeDataWriter_WriteDouble_mD4B24EB8A284CBBB1767DEDD6968607B18BCE9E5,
	SerializationNodeDataWriter_WriteExternalReference_mA74B0405492C80CC187110D0C45CA78E78DF77C6,
	SerializationNodeDataWriter_WriteExternalReference_mD8E954662AD09013154C2798EB7BDB1BB16C8BB5,
	SerializationNodeDataWriter_WriteExternalReference_mD9E07F1232C8B53775F7F250F2A8FA2F1E13E4CC,
	SerializationNodeDataWriter_WriteGuid_m215BCE2E7BE7C3661696509F0574297507ADFA83,
	SerializationNodeDataWriter_WriteInt16_m274C251E02065116D1E2452FEE6E9B3B5D2F55BA,
	SerializationNodeDataWriter_WriteInt32_m2E148DD18DF94B299079C1A3F385776179A7E46D,
	SerializationNodeDataWriter_WriteInt64_mD21E1EB74E6DC549AE1049119EEA5444F9DA56B9,
	SerializationNodeDataWriter_WriteInternalReference_m66204B29A08B0CA26FE097A846A5864A671AD558,
	SerializationNodeDataWriter_WriteNull_m3C95257CE595AADD2416808B20B04AF85B0B9EEF,
	NULL,
	SerializationNodeDataWriter_WriteSByte_m79351986B7AD5A5E7C6D148E8EC7B5A78D830139,
	SerializationNodeDataWriter_WriteString_m29531F959480E4D63F6353E497E7136DEA23DB0B,
	SerializationNodeDataWriter_WriteUInt16_m6BAD2C2C5866672AEB444B2AD0AC527255A7BE37,
	SerializationNodeDataWriter_WriteUInt32_m2B4E084E8CE1FCF3744500765B85049F2857317E,
	SerializationNodeDataWriter_WriteUInt64_m26D88CAB1FBC70E5F73D6082292AFD5BB6866101,
	SerializationNodeDataWriter_FlushToStream_m982DF8A7792730C3216B5BCDE97C01B820B3D416,
	SerializationNodeDataWriter_GetDataDump_mC330F6B13F9299F68F00DA055713A960BC4F8EF1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ArrayListFormatter_GetUninitializedObject_m04190330ADB4CC71AA46AE5D5E7F0F2BDB1EC867,
	ArrayListFormatter_DeserializeImplementation_m18C20E2589960BB5B006B56802BA6E0DC8483403,
	ArrayListFormatter_SerializeImplementation_mDF1EDEA81AFE13AA0AE279883EFD18456DBDD405,
	ArrayListFormatter__ctor_m1D2C45A4B87D64E95512F617B0F008E228B3CAA7,
	ArrayListFormatter__cctor_mEEEA10B0CE4D7DFA42B626B93724423AA095A051,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DateTimeFormatter_Read_m86CD9D11C1BC478D2FEC9EE09A0FEE3DD01881E9,
	DateTimeFormatter_Write_mEAA80920E179D421F5B2ABE19793652D033830BF,
	DateTimeFormatter__ctor_m7556B4C54F3EA4845A7DC77AE78C061A6A42B866,
	DateTimeOffsetFormatter_Read_mE881633D54D16447247F581FA551922E7D828DAB,
	DateTimeOffsetFormatter_Write_m10CD694BDB50B0A5FDA7B6842FAE5D8B59F85C98,
	DateTimeOffsetFormatter__ctor_mBF5EE9E84B884D8747A5013104994FC258C76FA9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmittedFormatterAttribute__ctor_m606F2E7D04ECA7FB692313F4893305F827FA8455,
	NULL,
	NULL,
	NULL,
	FormatterEmitter_GetEmittedFormatter_mA3B806BBFFEC9BDC692C593587B6F71987802EE2,
	NULL,
	NULL,
	NULL,
	NULL,
	GenericCollectionFormatter_CanFormat_mD040D5096BD3A8B9236986D53534C2A30C832930,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TimeSpanFormatter_Read_mD8967D32F06467976012CBFD0853EA46B6A2541A,
	TimeSpanFormatter_Write_m0237441AF1DBFD6E6716E32770DF2EE8710C6BC6,
	TimeSpanFormatter__ctor_mBB3F18137C3F2C0AAD311C58CC1D7AF45ACCC932,
	TypeFormatter_Read_mD80849457C98338DB6100DD60646D09308CD869B,
	TypeFormatter_Write_mCEEA83B29473614DBC541FFA09C4C83171A3000B,
	TypeFormatter_GetUninitializedObject_mBA650171F06BAFF967D343F633D324675731C744,
	TypeFormatter__ctor_m13E5F58172F12B68FD6A978C7687AC19A8110DD7,
	CustomSerializationPolicy__ctor_mF5F1DA0F2373F8B66C8F94682444CE26225BF6ED,
	CustomSerializationPolicy_get_ID_mD7CEC7D24FADBF7C0D9001A76F26B3D8F0358BDE,
	CustomSerializationPolicy_get_AllowNonSerializableTypes_m9915ED22749DA2B7A04FC22932B1410A7CA9F721,
	CustomSerializationPolicy_ShouldSerializeMember_m181837020AE6BF0EBF3D18D2003542CB9BE74A58,
	AlwaysFormatsSelfAttribute__ctor_m1541ADA655A347F12F8F26C40B7D952C50C8CF50,
	CustomFormatterAttribute__ctor_m36953AC85BD97885921B073BA3EF3F6F62DD61AA,
	CustomFormatterAttribute__ctor_m526DBB0A8BD4136097789728E4202D0174D1648C,
	CustomGenericFormatterAttribute__ctor_mC1B9E154BA604D2A434AA5B3223FA2E14D0C25A2,
	BindTypeNameToTypeAttribute__ctor_m4D779E05709E4A4E5C3A951D2CDFFEC85D691B9C,
	DefaultSerializationBinder__cctor_m4E56F7F408C0126441918D598FC455D0FD0599FF,
	DefaultSerializationBinder_RegisterAssembly_m876B33EC9E77AFB6D612E8F7E12477B31C113103,
	DefaultSerializationBinder_BindToName_m6F5A7AE51776942F8A8EB18E93F5C2380DB6454B,
	DefaultSerializationBinder_ContainsType_mDA5AFEB66B4D2191A97376A57DDC5551C0495025,
	DefaultSerializationBinder_BindToType_mAFFB58163889FFB2E39A7DAA13DE9B82DF572DB2,
	DefaultSerializationBinder_ParseTypeName_m6F74A5D1FD035060DFB110F9D4F52A1E29AD716E,
	DefaultSerializationBinder_ParseName_mED6C3E3BB66FECB451B4405059E59BEB29B72045,
	DefaultSerializationBinder_ParseGenericAndOrArrayType_mF5AC0FE8DFECA33ACD77CC89882F3C137650F720,
	DefaultSerializationBinder_TryParseGenericAndOrArrayTypeName_m8EF5780F2785277DE6CA0312CCEB7B3D8F03EAE5,
	DefaultSerializationBinder_Peek_m297D89DF109E0DD23D792F95206E48B6E73B81C2,
	DefaultSerializationBinder_ReadGenericArg_mEF3DA1F2B717193B75158DC9BE1B373204D1D5B1,
	DefaultSerializationBinder__ctor_m22B29482A0F586CBE75CCD2E9B35583C56DB1F3E,
	U3CU3Ec__cctor_m7D4E84798DE34140B22462E62480D747B7F4FE16,
	U3CU3Ec__ctor_m5B9239DC89210A7E40190FD4928AEEB2529F4F6D,
	U3CU3Ec_U3C_cctorU3Eb__9_0_m300707410D7EFD45DE00D9B86D390F0E75F677D4,
	DeserializationContext__ctor_m9779A96F262A51AFF232FAE4FAFDF6231FDFCB59,
	DeserializationContext__ctor_mD34C3165AEDEF271A67A4EF381C916F7B793CED4,
	DeserializationContext__ctor_m274B1E7A2F09E5A7EE58663F9ECD7541A251DD5E,
	DeserializationContext__ctor_m498FFB2989BACE4DDA56EA6A16C58A4BD49C64BF,
	DeserializationContext_get_Binder_m4B0930934A1EEEBBF7AEDA2C5293FD76FAD0B6D8,
	DeserializationContext_set_Binder_m5A534C7B227E60A4A20C221355DEBF34974D9F1E,
	DeserializationContext_get_StringReferenceResolver_m2E6A2B2DD825C6A09328D89BBA8E51DE9C39CF7A,
	DeserializationContext_set_StringReferenceResolver_mDCA00F48D400266293A8047DD7691313DDD7398E,
	DeserializationContext_get_GuidReferenceResolver_m07C4BC6749087DE670A55821EE76E3FDC4763842,
	DeserializationContext_set_GuidReferenceResolver_mFBE1143558704260CC3056661B695858DA0CBD49,
	DeserializationContext_get_IndexReferenceResolver_mA9574D5FABC6706AA16F89CEE489D3D69CD169C7,
	DeserializationContext_set_IndexReferenceResolver_m085C6EE44AC7431CF98E3AB8DA0B6512274E2E34,
	DeserializationContext_get_StreamingContext_m1C401093278556DD4292408E248CFC1406673B65,
	DeserializationContext_get_FormatterConverter_mC292079B7C156A8661073287AE55558F0A38FAE8,
	DeserializationContext_get_Config_mBCE9B24095D46BB256F798F6DC382F7206BFFABD,
	DeserializationContext_set_Config_m37F328893E1C5D2ACDDB6856D44BCC96BD834947,
	DeserializationContext_RegisterInternalReference_mE899B15215AAFA95E7B30FE7A3031FCDD0A21CA2,
	DeserializationContext_GetInternalReference_m50E00070EF10DE4A896B2577FCD98319E06B5564,
	DeserializationContext_GetExternalObject_mC40F1651D8D8E43127BFF2E994B4CFCC6615431C,
	DeserializationContext_GetExternalObject_mFF370114AE3B1BAF1ADB06BA52B67D75743F8005,
	DeserializationContext_GetExternalObject_m0811101A4CC004CBF0ABF88ACF4A6FA30E442F6D,
	DeserializationContext_Reset_m16192CB9AD4BEDBFB45A532499A9CDE07C659A04,
	DeserializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_mF832E26ADB74079AA4DD5EEADDEAB37033F113FF,
	DeserializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_m7BB538880888B8F0F0F2514FF72C2A251DAC068C,
	ExcludeDataFromInspectorAttribute__ctor_m5F2E8B6F6636312F9EA4E7C1C98E1FA64C6717EF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NodeInfo__ctor_m80DB71C7F04F575293A2C3ED71FEFEFDA81F0A7A_AdjustorThunk,
	NodeInfo__ctor_mC7784D929F099B20EF3F485D21546F89A15F7229_AdjustorThunk,
	NodeInfo_op_Equality_mC00ACFE400D8D926E93F13824307C7C03C074EA5,
	NodeInfo_op_Inequality_mF6D738C53EF7114186029E27AAC8F4253797F6D6,
	NodeInfo_Equals_m8557079FD46245447C0DB54C05885DB558520554_AdjustorThunk,
	NodeInfo_GetHashCode_m65C7EB50665D7EC8C3914EEC4635BD7D196EF449_AdjustorThunk,
	NodeInfo__cctor_m70BB7A18A1FE03F3BF2DD83E6BC3E2C64406554F,
	PreviouslySerializedAsAttribute_get_Name_mD6B7A827CFCBB02C52D4361C71607E0ACA5DCC72,
	PreviouslySerializedAsAttribute_set_Name_m827CB90ACB0522D52FDD1273C1A88C86FC5128AB,
	PreviouslySerializedAsAttribute__ctor_m8F72302DD4AB5A73047CBE7E20AEA70393C76E91,
	SerializationAbortException__ctor_m9EB2F36F2A04850D5BA49ED9B98292083C96EDF2,
	SerializationAbortException__ctor_m68CC62D5CDCFE4A9ADADFF44D1CABEC624BA0CAE,
	SerializationConfig__ctor_m390208C2DC88A87D7F75A2593D65B08C06A2C72C,
	SerializationConfig_get_SerializationPolicy_m4E1D882B9C86CC9451CFDE4CB12DAB4C7F5C380A,
	SerializationConfig_set_SerializationPolicy_m614F0C6FB730AC46A63E0FA104C170E7A181BED7,
	SerializationConfig_get_DebugContext_m9D0569609A9BAE8B52E04A6741271D290C3FC0A3,
	SerializationConfig_set_DebugContext_m6B57CACD98CA8D5CECFD2DE6CBB24D0638B51264,
	SerializationConfig_ResetToDefault_m2D8799CF030BA890B48C5B334FD40D747094CBE3,
	DebugContext_get_Logger_m31C1C7D14197167011C3822D74E898C98FC8E834,
	DebugContext_set_Logger_mB94A74527AD594F75E31606BD8132F3FAB7EF73C,
	DebugContext_get_LoggingPolicy_m57B11101D37D423BDFF9B8CBDE7A2D2F97C95961,
	DebugContext_set_LoggingPolicy_mC6351AA3797186252B61FA1D05F18C6A54844BB3,
	DebugContext_get_ErrorHandlingPolicy_mA116727478BDFF90325007011E90CD2A4629A61C,
	DebugContext_set_ErrorHandlingPolicy_m090EE6E61EAA63E1EEA02DA34B0E587D66925C44,
	DebugContext_LogWarning_m9C2033B394AB9D718BD1497C470560FEE62EC7CC,
	DebugContext_LogError_mEC2F10FFB48C301041DCFF384CA26DE58D2952BC,
	DebugContext_LogException_mB56991ADAFF7DCE1E99FC292A45C718609036936,
	DebugContext_ResetToDefault_mBED55368A6B2C7A30AF2392A514E67BE187ED8C2,
	DebugContext__ctor_m7D538BA803F40E2E7DA9EC551D1FC99CF0DB363E,
	SerializationContext__ctor_mFA3AA4339B95B7C4FBB1749E971EDFB031F2C815,
	SerializationContext__ctor_m13288803C6ADD0F4F6EC61351D894BBEE6C76173,
	SerializationContext__ctor_m84B12C44312834E6EC50CDD2702795487F231A04,
	SerializationContext__ctor_m5723D441D289C54817EA2B428A4430D3F00E939C,
	SerializationContext_get_Binder_m91596C211FB7F857FFAFEDD72CE479BA73C3800B,
	SerializationContext_set_Binder_m581116A2F0172FD501F0AB6951DA515B89103CD5,
	SerializationContext_get_StreamingContext_m41531D50D93ADD4DD80E20833F164F261F1EA0E3,
	SerializationContext_get_FormatterConverter_mAC33ACEF47C70B571174523347488EE28E94AB68,
	SerializationContext_get_IndexReferenceResolver_mDA9EA854A29E80106218C2A9181669388AEFF7C2,
	SerializationContext_set_IndexReferenceResolver_m30246FA74C05A8929BB601618D7D2F1C054170C1,
	SerializationContext_get_StringReferenceResolver_m8951CAFED2DD652D8BEF984D1F9D23397F5E61ED,
	SerializationContext_set_StringReferenceResolver_mD8F0F1C09F84790CA6B0E1036C8A98882112D8FE,
	SerializationContext_get_GuidReferenceResolver_mDDB9FC152AB8A6A7A8B3D67105C2385820974430,
	SerializationContext_set_GuidReferenceResolver_mBDB3D466481A10A762C1938D0FBD23198F13D5A6,
	SerializationContext_get_Config_mD1D20E1B17FA1CEBEA1941043CFF2AF925B142F7,
	SerializationContext_set_Config_mA8905FA765B7BF8FAA36CE2EB3BB1B9BCD6CDC84,
	SerializationContext_TryGetInternalReferenceId_m5A02931772D6C50C75F4B5CB88C142A9B9E7A0FD,
	SerializationContext_TryRegisterInternalReference_m9ACBEF1882BE5697B5EF9CF68B1C02EEE017CFB3,
	SerializationContext_TryRegisterExternalReference_mBD785756C6A59336C63A7017A61CD467E601C636,
	SerializationContext_TryRegisterExternalReference_mA394B4E837B29F45B7E1FAACEF6B7B85BFDB590B,
	SerializationContext_TryRegisterExternalReference_mB84920A8F3B1A100E0B9830F92FE62E04A23E303,
	SerializationContext_ResetInternalReferences_m6DA41CA826CC28EDBA429F6B5DC1795CDBD65EFF,
	SerializationContext_ResetToDefault_m5EAD037F525E5C6E1A7F876C1311275EE697E98B,
	SerializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_m97ECEDC3A5F1818633A3559D552584B8CA7FFD87,
	SerializationContext_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_m15870EE712946EA768023908755BCA139EBBB3C4,
	OdinSerializeAttribute__ctor_m25FD490CA40157F3EE1EA0CCE0B9B4B395E3A25D,
	NULL,
	NULL,
	NULL,
	TwoWaySerializationBinder__ctor_mBB6209AA85C6718D51CC1B5D44EFD43B6F125850,
	TwoWaySerializationBinder__cctor_mEBF18F68D0BB08E5A1F426E1A476C3F25B333D9A,
	SerializationPolicies_TryGetByID_m8075991C4D3C34FB40599F7A6AD6A4AE2D912FFA,
	SerializationPolicies_get_Everything_m63ABB7C174CF5077DCEA03F7DA5E4B8F6C7B66B7,
	SerializationPolicies_get_Unity_m66BDA95A3D225273AC1AEDF3A2B5E04D09F9F2B9,
	SerializationPolicies_get_Strict_m47F05C2C25C4F6B850709B3FAB6C54C6C5BB4245,
	SerializationPolicies__cctor_m2DB7B0F1BB03544D104B2E07C3F2B3A4ADB6E9BA,
	U3CU3Ec__cctor_m189B45E4F1272D02E8E2754A2E1CAD106FF566A4,
	U3CU3Ec__ctor_m542CAE9E28C21BB6FAE4D2116847C13EA4C63D4D,
	U3CU3Ec_U3Cget_EverythingU3Eb__6_0_m9BC65B36F659100819D8C4B088A932637F741BD8,
	U3CU3Ec_U3Cget_StrictU3Eb__10_0_m467EBC473301EA945B6F057B77B13DFB1ECD2F74,
	U3CU3Ec__DisplayClass8_0__ctor_m34304279BFF55350DBC69C33406677A59801B229,
	U3CU3Ec__DisplayClass8_0_U3Cget_UnityU3Eb__0_m0C7D21A7B7062810B1985EF6B4D045A616C201A6,
	BooleanSerializer_ReadValue_mB24D6DE1A71324A7DC53D2F4A762D1C5CD47B64E,
	BooleanSerializer_WriteValue_m5DE4CA51560EFE7AE85F4C2796A427369250370F,
	BooleanSerializer__ctor_m84122A09616E473CF066EF3D576D781956A6EB02,
	ByteSerializer_ReadValue_mEDF73946AC3BA28542C9306C0C4F24E2C249006A,
	ByteSerializer_WriteValue_mAF9CAAFC0D114580D70843999287633C605C0BCE,
	ByteSerializer__ctor_mC120B986867479BC047B18219E2A7CA9B9255B60,
	CharSerializer_ReadValue_m1E48416978FC00573D8343E00B34846D2C8E04D5,
	CharSerializer_WriteValue_m50F73209C54DB855A4A97B768836A08047D0F7C5,
	CharSerializer__ctor_m32C8CAC5AB6082233E71CE6C215551029A1EEF62,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DecimalSerializer_ReadValue_m90EA6028DD57788F6AD2F96E3A73405BC72FE1B4,
	DecimalSerializer_WriteValue_mE139172EFB64163F7050D82DA641E6BB616C1C97,
	DecimalSerializer__ctor_mB9952FECC1DC9917D2F3844D1C8AD2BFDA61C1E7,
	DoubleSerializer_ReadValue_m7AF1A3B6FFC44925712A60845BA3312ADF36FE97,
	DoubleSerializer_WriteValue_m81ECB16AD9225B8F4715CACE84F83146CE822F0A,
	DoubleSerializer__ctor_m1744CB8ECBB309625C008379D5EE309B649E57AA,
	NULL,
	NULL,
	NULL,
	NULL,
	GuidSerializer_ReadValue_mF11C73047E8E9D49435716A37A8D0D651DA3FB75,
	GuidSerializer_WriteValue_mF3F24FA9B7704CA6B7A886A776D24F8435B9D01A,
	GuidSerializer__ctor_m12CAD3617A9ADBE6E49880F370BD802EB8C8E697,
	Int16Serializer_ReadValue_m4FFDDECE06DBC7B764F8545DBF62F86EEBF8BF72,
	Int16Serializer_WriteValue_m1C002B77B8F5B1717CB72211CA8BF6A083762C40,
	Int16Serializer__ctor_mE145CB8E3BB6E01B7DD3149E24DFA25DFC8A86A8,
	Int32Serializer_ReadValue_m7CC01A3FE3FDD4F710C4BD8AE67D662D513BCC95,
	Int32Serializer_WriteValue_m3326B39D1CDBD6AB23D79F574B70906AB2A9CDD3,
	Int32Serializer__ctor_m958BF4399EBBB8FA17072FA4CE03B74279F795AE,
	Int64Serializer_ReadValue_mC1059ABE53F7376CB09F6C1B38B713D0D4635FB8,
	Int64Serializer_WriteValue_m73BF747656E83BCD7BE4A7F8D69D6DD3DD25477E,
	Int64Serializer__ctor_mEEF926FC59D286AC775A44D8297C43328F6B48D2,
	IntPtrSerializer_ReadValue_m7BB65364B88B29ADBB642D378D8F6E2C92E1EDD9,
	IntPtrSerializer_WriteValue_mD3A14E3B74E32AC4E6B4C541A8F266935CC587CA,
	IntPtrSerializer__ctor_m63BC93CA7AA51FC2E5ADCA3D0ED69B0A66F3B369,
	SByteSerializer_ReadValue_m194715E789B6CAC23FC94FF65FA2A6B37B027DB8,
	SByteSerializer_WriteValue_m62E1C8DFA9D07DB7DA05345D02EFDF2B186C8961,
	SByteSerializer__ctor_mE4A0C8F60A9F572A73DD00BED3AA9B87CEE949A4,
	Serializer_FireOnSerializedType_mA5E91B9D9502A09C9C5097AC97AE35CC3CEA9715,
	Serializer_GetForValue_m59BA4E7CF86FA8C5F413B9E8A1210A5BBAD8C597,
	NULL,
	Serializer_Get_mE6F318A9F7C1127B3AAADF2A8778ABBDC6D776EE,
	NULL,
	Serializer_WriteValueWeak_m3FFF974CEA0DE4A544CE1E19BC70B04F3A543E05,
	NULL,
	Serializer_Create_m3FAF74364F2B543A5E401514D9271B94617CEFC9,
	Serializer_LogAOTError_mC27E104AE9454A88A17AE94937DB8738D9DE6E80,
	Serializer__ctor_m45C015B6F9C601D1C5285C5AA20441FDF3533408,
	Serializer__cctor_mC319621B2CA4581640DB4B0FF4936953AF0EDBE5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SingleSerializer_ReadValue_m08ADBCF53C810AE82821F80AD767D0D050E549E7,
	SingleSerializer_WriteValue_m299B78B4D615A807F6CE1446E133A743363561D6,
	SingleSerializer__ctor_mCE39A6FEDF0B74D2F2F26DBF24A60390A423F66E,
	StringSerializer_ReadValue_mFF413B8A55A9868455C87A695317255676A40D8F,
	StringSerializer_WriteValue_m11FECEF5966FFB38D2BF3E5B540F978CF4A07112,
	StringSerializer__ctor_m1CA97544812FC0ADC58C09DF21E18F5D9698FA20,
	UInt16Serializer_ReadValue_m68441D9FA8D5860DD88215F4271EBD36A361C540,
	UInt16Serializer_WriteValue_mBEA9A10DD526B33C16ACB513B3F614530E8454C0,
	UInt16Serializer__ctor_mAB64A7DAE55003A3C3E668EAA2BA639EAA4A0559,
	UInt32Serializer_ReadValue_mD076D5DD17F1C0418F5E7FB850259699A5867755,
	UInt32Serializer_WriteValue_m0ADA275570A30CD6D0AD989AFD26875879336C06,
	UInt32Serializer__ctor_mB5B5C392CCB252F938EB8CAE7339031CADE67B73,
	UInt64Serializer_ReadValue_mFDE0D9088DFE95A936786BCD6853E91FB461BC0D,
	UInt64Serializer_WriteValue_mBDEAEEC03C2B8E8883C75FB393A2022FA8D8B784,
	UInt64Serializer__ctor_m246252F1E014014F2548425C409591D9FD056E98,
	UIntPtrSerializer_ReadValue_mF09418A4BEA2D23D9924E45F9C09A384AC6E491F,
	UIntPtrSerializer_WriteValue_m7FD80EEFD11F66ED597F9D332F3E1DA817CD8DC3,
	UIntPtrSerializer__ctor_m189B69F0273A9B1198A4560AE7FD135446E5B0F9,
	AnimationCurveFormatter_GetUninitializedObject_m39D7C2032EA2B73D311032E21891DC85C1CFDE0F,
	AnimationCurveFormatter_Read_mF186997C7F474A58A503F97AB7F31D197385D919,
	AnimationCurveFormatter_Write_m1CC4E7935C0E6735116758E2D2F180B8753DB868,
	AnimationCurveFormatter__ctor_mC587C72DE21DA5C667B7C6A7D38DFF09B78BE992,
	AnimationCurveFormatter__cctor_m5C899B6EADD5B0212BE2D98F29E26FCA37DE929D,
	BoundsFormatter_Read_m86A104BD2DB957A42B6B5DF16A8A982B2BCDB769,
	BoundsFormatter_Write_m4D5C3DEAAB2EE8FFEDAE2FCDD7AC4A754D0D01FD,
	BoundsFormatter__ctor_m995CAEAE8724BEDE5D3028B40C49D8E3AFC282DA,
	BoundsFormatter__cctor_m8A6EAFBE881328B2997F3C595855AA78280FA7BE,
	Color32Formatter_Read_m916B4634EDA522BEEE2CBA8ECCDA223B9147DD21,
	Color32Formatter_Write_mB424D3DCD796962900C1DEA08F9D1DFD1C92B93B,
	Color32Formatter__ctor_mBE595C0FC0FB29EC8C75260900CC2BCA7DCE2B0C,
	Color32Formatter__cctor_mAD42BF4DD4D6639E53B59E53BF30EFE4E1BDD643,
	ColorBlockFormatterLocator_TryGetFormatter_m6BE2548A9E500F695489B2F4F895A883D948DA0A,
	ColorBlockFormatterLocator__ctor_mD012D228086F82901A2E97F2A4B4B15DF2E0ACD4,
	NULL,
	NULL,
	NULL,
	NULL,
	ColorFormatter_Read_m991AC0864D249315602CF692FB014F68C5ECF5AA,
	ColorFormatter_Write_m1C37E6C2C0205A58D8FE4304D32490572BBAB77C,
	ColorFormatter__ctor_m56CD5060CA089709ECA768AED0942CCB9036D5DF,
	ColorFormatter__cctor_m1E542D2B1200265BB3933A67CC94EEB2C7635E7C,
	CoroutineFormatter_get_SerializedType_m252F4D0305E6ACEA15C6716544FAB8ABA9102B90,
	CoroutineFormatter_Sirenix_Serialization_IFormatter_Deserialize_m3485B13A5725704863EDCDB93544882F74608BD7,
	CoroutineFormatter_Deserialize_mE3E71B20252BE4D1A9847A775889002D9D3E9FD5,
	CoroutineFormatter_Serialize_m05308770DF054E7D038948D71B88F19F52A2F9B4,
	CoroutineFormatter_Serialize_m423E50A59E60160263A3AC10C9A7B667E2259813,
	CoroutineFormatter__ctor_m0D130A619A49FF286E016711EE6472BB960502F2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RegisterDictionaryKeyPathProviderAttribute__ctor_m72BA975E1E2C3F05E19BDD08BA797D45385953D0,
	Vector2DictionaryKeyPathProvider_get_ProviderID_m2D7053A716D09183D69021301A61443E29A04033,
	Vector2DictionaryKeyPathProvider_Compare_m58D394FA139F2C08CEF59F831E64FB9D42EC8B29,
	Vector2DictionaryKeyPathProvider_GetKeyFromPathString_mA4A9A571EB0005B4BCBB871F18C22AF6661823A1,
	Vector2DictionaryKeyPathProvider_GetPathStringFromKey_m54D8ABB043ACD0679F2D2F19415862345203046B,
	Vector2DictionaryKeyPathProvider__ctor_m061EE5991CA2444B17EAFB3CED11A253BA2F914C,
	Vector3DictionaryKeyPathProvider_get_ProviderID_mBFC57D55097DEAF73CE14A8BC6A155E5CEFC9C4D,
	Vector3DictionaryKeyPathProvider_Compare_m37837A32DFEADE4FFC5C7F655D183138FF79D21A,
	Vector3DictionaryKeyPathProvider_GetKeyFromPathString_m2E709C1620555D6F1251B21152FA0875D9D8123F,
	Vector3DictionaryKeyPathProvider_GetPathStringFromKey_m4A9EBE2A0C7AFDA5B66A00635D558FEFDF2DA670,
	Vector3DictionaryKeyPathProvider__ctor_m14616B5DAE3BDB8CFD5916AC53DD90B07397F92F,
	Vector4DictionaryKeyPathProvider_get_ProviderID_m74BF0B3042719851DB7EA4EFE24A276AE16F68C3,
	Vector4DictionaryKeyPathProvider_Compare_mC0AEE68D5D51068275686663804DAC700063EDB1,
	Vector4DictionaryKeyPathProvider_GetKeyFromPathString_m495910FADAEAC571410E409389C10419F0328532,
	Vector4DictionaryKeyPathProvider_GetPathStringFromKey_mB658F24BE548B4434C27A082E53E7C8D54672AA6,
	Vector4DictionaryKeyPathProvider__ctor_m8391BB09BA9097575E0B916513A63482D59879AB,
	GradientAlphaKeyFormatter_Read_mF48E175C75C4678EE568941D7E5C602860FE949B,
	GradientAlphaKeyFormatter_Write_m408F06EA2C718FE38617AEBB516991597D1078E6,
	GradientAlphaKeyFormatter__ctor_mC75B0E279AC3589B51D27039E745550703043480,
	GradientAlphaKeyFormatter__cctor_m49D611FD80B31502D4E8834F1E3E884A28BA2E8A,
	GradientColorKeyFormatter_Read_mEF8ED09DE70500A651B108CA8EC4774B099FF79B,
	GradientColorKeyFormatter_Write_m10357B06628BBBE1C86A2B507A2EFFC71DD8F140,
	GradientColorKeyFormatter__ctor_m0CADB69E108A0EEC3115D531D6E19E4D3659B693,
	GradientColorKeyFormatter__cctor_m7755EF1FEDAD7589CFF022770A4FE7A21FFFD287,
	GradientFormatter_GetUninitializedObject_m505BBB6A15D5E2ECCE9298386C05D5AD5C1A174B,
	GradientFormatter_Read_mCAF47D3960C2F901311B1550B10E5238DEB8E8B1,
	GradientFormatter_Write_mD7DCF6C940F73905331CCAB256F17E74C2DD6F2F,
	GradientFormatter__ctor_m80B772E9FF808CE84F6532F5224D9FDCABB0E41F,
	GradientFormatter__cctor_mDD3B6DFC9B6E2967DF555436F544DCB5322131B6,
	NULL,
	NULL,
	NULL,
	KeyframeFormatter__cctor_m2CE8A00AA320842FE51168A3B2EFB78FEB103C2D,
	KeyframeFormatter_Read_mA05CDE0698AB00770915AB1160DC109E36EA21C9,
	KeyframeFormatter_Write_m77DD03FFF6893257A03EA2D50D97123B1BAE080C,
	KeyframeFormatter__ctor_mA435A99D963FBC45A61889887176D30590DCECF7,
	LayerMaskFormatter_Read_m50982A46C8504F92FCAD146A94B211A765428FC9,
	LayerMaskFormatter_Write_mCC5CA9EA068B0139316DC4BBBB39C9994828957A,
	LayerMaskFormatter__ctor_m02DF1C3BDF0D5ACBF304C0F8FE3BEF062314A3A3,
	LayerMaskFormatter__cctor_m91C1A58C62C111E404F874776F3B786930672312,
	QuaternionFormatter_Read_m8AF9FBFEDFFE5671683D04F614EB0FDFDC03754A,
	QuaternionFormatter_Write_m5724E371EEEAED26B37D6D0BFB492164A51BF0CC,
	QuaternionFormatter__ctor_mD02B4C8202E83C78C28E8D0C3675F2E0C0A7CE80,
	QuaternionFormatter__cctor_m7ED0437F19AA4D7A7F510CC105B8346B02C4D16C,
	RectFormatter_Read_m459C3D86CD61B465F5888F64896D6E5B1812D09A,
	RectFormatter_Write_m57C1916CE23F6CF5050061CAB69C479224C8254B,
	RectFormatter__ctor_m619F4F0094FB16ED8D0A8BEE8F267E72B0FB5A69,
	RectFormatter__cctor_m6F8E5B5931EA36EE393F6ABDE86819306D962DC4,
	SerializationData_get_HasEditorData_m8A28A607C9FA81619AB350C707110080E2BF28DE_AdjustorThunk,
	SerializationData_get_ContainsData_mD5C03579573CD9160DBA710C64061F7112336678_AdjustorThunk,
	SerializationData_Reset_mB8D787047F63FF9C15B6ACBAAB2518854F9D1A4D_AdjustorThunk,
	NULL,
	NULL,
	UnityReferenceResolver__ctor_mCEDD3179CC9BB4CF843A1872248EF590FC5030DD,
	UnityReferenceResolver__ctor_m06ECB413A2FB7A3D7E034C7402E981238A24D01E,
	UnityReferenceResolver_GetReferencedUnityObjects_m9D7B548635FAC988774730EAC4214C981EB91E82,
	UnityReferenceResolver_SetReferencedUnityObjects_m91FF6B96A3B3734248DF7024BA780BA506D0A959,
	UnityReferenceResolver_CanReference_mF307957AF758932397286AA2E81A971616211BF0,
	UnityReferenceResolver_TryResolveReference_m2C596E934C4453023616C14A4F8967FE11723168,
	UnityReferenceResolver_Reset_m180EF0D7A2EBEC85416D528B2CADE219D16B90E5,
	UnityReferenceResolver_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnFreed_m06D104F7EDFD529B95447225BF585201FA933CA1,
	UnityReferenceResolver_Sirenix_Serialization_Utilities_ICacheNotificationReceiver_OnClaimed_m0676D80FD6E65477FFE45F06EEE0213397FEC5F4,
	UnitySerializationInitializer_get_Initialized_m4418A5120E97720C7A1644E68B35E49F5D8E6A9D,
	UnitySerializationInitializer_get_CurrentPlatform_mF269C7AAC83FC078F94F1116119D796ECB5820EB,
	UnitySerializationInitializer_set_CurrentPlatform_m55348AA2C668D1C114F90BB660ECC5382C0CD4F0,
	UnitySerializationInitializer_Initialize_m6A83084C82113DE884AAAF9F9470B438A61F69EE,
	UnitySerializationInitializer_InitializeRuntime_mEC62F8B5E6AC16C44DAB3EFF5606F96ECFD1EB6B,
	UnitySerializationInitializer_InitializeEditor_mB74EA6BE4B9672EB2F6F81E4C503FFAED6F46C1F,
	UnitySerializationInitializer__cctor_mE5935107BF6660DC62F6B73BC47F9BB6C92ABFE0,
	Vector2Formatter_Read_m27AA5128534AB4AAA2FD75E135A31334A53CE863,
	Vector2Formatter_Write_mA19EF356CC9C452E0E621F6FBC48AE0F895D7894,
	Vector2Formatter__ctor_mAF1B5664C6C5AD7FFFD936C977AF80BA6989D25B,
	Vector2Formatter__cctor_m2327CF76C0ABE77592F403F504A8EC7AC5699C2F,
	Vector3Formatter_Read_mC2D02E6D830BA62945AF7BB3A24B1C663363FDB7,
	Vector3Formatter_Write_m180053302E6B7B25BF3676354A93CF6E4D0C8B0C,
	Vector3Formatter__ctor_m48FA57AC400D6480911C623F348E553A0179A870,
	Vector3Formatter__cctor_m6FA4C8ACA49CB97F4C7EE6D25537C40F1C21B9F0,
	Vector4Formatter_Read_m78319BA03C8A34A7A3BF595C0D62C9EFE14FEF51,
	Vector4Formatter_Write_mB04AD604312209138439BB3BE5FC9C1000EE9B42,
	Vector4Formatter__ctor_m17E07D3D65AA9200C2B61A0FF82253E4AE5C613F,
	Vector4Formatter__cctor_mFC6691CC4BC11DFB4D41E0534A00B071FB6F68BE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CachedMemoryStream_get_MemoryStream_mC05F447C9F5077FE2A534BD50406CD86ACF53959,
	CachedMemoryStream__ctor_m0F8DB78A5C89C102E1B3D4ADB2EF3AF001D428CF,
	CachedMemoryStream_OnFreed_m49275EEF1AA830893D91EF570B08A25A906AFC80,
	CachedMemoryStream_OnClaimed_m4B0519D698DD4F79E92E7159DA8046BE209D6DB5,
	CachedMemoryStream_Claim_m158B6D5D546DFD260D7A7BDB0E7DC8A29B97945C,
	CachedMemoryStream_Claim_mD79A0F4F969E8A616EA76BD7EE76AFC43C3BDA2E,
	CachedMemoryStream__cctor_m7A602030B2BE71C8DCF4ACE446DA737371E02A24,
	EmittedAssemblyAttribute__ctor_mF6C4FA2B76A0A7C473E38F0F3E4CF2D508FA05E7,
	FormatterLocator__cctor_mD132535014F04E03EF5B842D64A9197E387018F6,
	FormatterLocator_add_FormatterResolve_mC3FC83CD4A004074E56AD2EFBFE07680D24A9D2C,
	FormatterLocator_remove_FormatterResolve_m573DCACA76DC8BB72437A36C7DA8D57584179569,
	NULL,
	FormatterLocator_GetFormatter_m609E20B144E94BA26F598167FC5205596A17E88F,
	FormatterLocator_LogAOTError_mC60C7F8A68389FA719BB057DA4F046FE0795ED09,
	FormatterLocator_GetAllPossibleMissingAOTTypes_mFCB6F4A338DE6811EA8913468B1E8364E7D81608,
	FormatterLocator_GetAllCompatiblePredefinedFormatters_m5DD9623720D6A31E69EDCD2504BFC708BF552D52,
	FormatterLocator_CreateFormatter_m6D5588BC7C987C584B8C6EA03A229CE86394075F,
	FormatterLocator_GetFormatterInstance_m2E950BB5E88B610A53747AFB348905F30DF20BDB,
	U3CU3Ec__cctor_m33B43354C60ADD4CC37D2404604D02237EA3A225,
	U3CU3Ec__ctor_m9CDB9589FBD1AE85E33477B437CF954747F790B5,
	U3CU3Ec_U3C_cctorU3Eb__7_0_mBF4C5A8955BE92DC9BD982576DD95C269B5E03E5,
	U3CU3Ec_U3C_cctorU3Eb__7_1_m746B3A9405CC4A3BD40DD58F0B2A4D26601F3CB5,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14__ctor_m42E920DFA0EC2ED211ABF66260FC284F6C948795,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_IDisposable_Dispose_m213294EE73C9FD0BA4AE290DCFC4F05D8B0E372C,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_MoveNext_m0CEA908B3EB2E3E326B19F1ED09F4668D21A1A94,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_U3CU3Em__Finally1_m64E4B5F2C7CD7BFB1554DFC8F54AAD567E8F2DA2,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m9DE49E3A7D769B602A0780838785918940AA3CA9,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_Reset_m8F12030F2DE4AA1B634162D7F8E2B88FD1FCB392,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerator_get_Current_m5D0857D4C63F8F25DE3DB3350C16B277AC731EBB,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m120AA74EBA3617562BB5538F033FE57DE2E29276,
	U3CGetAllPossibleMissingAOTTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m9900CD20243E8E7059331DC685EE00C73F9AE343,
	FormatterUtilities__cctor_m5A2CCD8F74CEDE7ECF626FDD3911EEE60978AD5F,
	FormatterUtilities_GetSerializableMembersMap_m2BCCD09DE91716C65861B3C987A01FECA09E5FD7,
	FormatterUtilities_GetSerializableMembers_mCD7491A73EC6C46B532DDD2BDA55A8B2FAA6E756,
	FormatterUtilities_CreateUnityNull_m1C86CB4B3D79E840473A2E8D62C09CF61DDEF17D,
	FormatterUtilities_IsPrimitiveType_mC55C698DF301272897C5BACFBCFA1F5B6C8652DC,
	FormatterUtilities_IsPrimitiveArrayType_m5EE44616BEE8895DCEC460C52633837DC1191649,
	FormatterUtilities_GetContainedType_m3BF0E4E26DAFC2801F118FDF173FB4915AFD6D30,
	FormatterUtilities_GetMemberValue_m446E6E6F36BA8FF104F0ED0C4B3DE517169FB025,
	FormatterUtilities_SetMemberValue_mF72532625DF3C6C48A14F346DA590C04D68AC785,
	FormatterUtilities_FindSerializableMembersMap_m58D7BD4AE01CCE554F4B29DCAE4D6975631BFB81,
	FormatterUtilities_FindSerializableMembers_m48AA97E98D1AEAFD5A7077E27C1883675C33C995,
	FormatterUtilities_GetPrivateMemberAlias_m7532AD47F334E60FFD55DE4DC66A0FE34B97F4B0,
	FormatterUtilities_MemberIsPrivate_m8E4B6B8B9DB4046DF9CCB322994B6E35D94F69F2,
	U3CU3Ec__cctor_m16A2C04C7D05D2858A3F46E4CF367A9C349D5A3E,
	U3CU3Ec__ctor_mB0988998C15D2CF5DE01E73F0CD54A696A90B0A1,
	U3CU3Ec_U3CFindSerializableMembersMapU3Eb__15_0_m496274F2394DE778E9C03E4859FCBDE892F26DF7,
	U3CU3Ec_U3CFindSerializableMembersMapU3Eb__15_1_mBFD069C99D28E02AF9CB2331DE6C458175CA2649,
	U3CU3Ec_U3CFindSerializableMembersU3Eb__16_0_m7346F8814E8A548032D203786563DB10522ADE26,
	U3CU3Ec__DisplayClass16_0__ctor_mDB4EEE19341B400C8BDED106E4A8EEF8D68689B7,
	U3CU3Ec__DisplayClass16_0_U3CFindSerializableMembersU3Eb__1_m9CF5733277C55392CA8F8150BB3FE81DB1CBBE2E,
	DictionaryKeyUtility__cctor_mF8E8576B0A2527ABA665AD497B591FDDED2B24FC,
	DictionaryKeyUtility_LogInvalidKeyPathProvider_mFC5107212E8DFCA4C01E4ADCFAA6E22DA360376B,
	DictionaryKeyUtility_GetPersistentPathKeyTypes_m560066363F0240207A5EA9D08C2BD12C714893CD,
	DictionaryKeyUtility_KeyTypeSupportsPersistentPaths_m80B8F6E8AD30B255BF135F56E2EA48F804DA0AFB,
	DictionaryKeyUtility_PrivateIsSupportedDictionaryKeyType_m732CD72FE58A3EA7E6F60A6272521A26849F039D,
	DictionaryKeyUtility_GetDictionaryKeyString_m0DF429B9CE1CD1F5B35AE9161C5BE271F3A0B088,
	DictionaryKeyUtility_GetDictionaryKeyValue_mA8E94C8276DF4CB6A92E78DE62F8FE5A222E097F,
	DictionaryKeyUtility_FromTo_mB0304339664BD0C90DEA2A7365EF3E633A78560C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass12_0__ctor_m71E0F48CC9CF144F05702159EF4780DD61ED7C49,
	U3CU3Ec__DisplayClass12_0_U3C_cctorU3Eb__1_m8ED1C41C4EBC067991B75E57BBF9CA731348BCFB,
	U3CU3Ec__cctor_mD1665EEB974332A5A23EE9819F7E6CDAA42537E0,
	U3CU3Ec__ctor_m81A9C227125D528E6ACD22AF39F1BCDD8F3D0A82,
	U3CU3Ec_U3C_cctorU3Eb__12_0_m147EE29DA5957C137C52D24228B4998CD303DBFF,
	U3CU3Ec_U3C_cctorU3Eb__12_2_mE3B74A1EB0FE353A68D0537F31AA344A24DC8885,
	U3CGetPersistentPathKeyTypesU3Ed__14__ctor_m6FB68B817C2FDF39AC91F2875C7B1DE111D2A0AC,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_IDisposable_Dispose_mB1F04B3D3BF5E0BD68489B69C56E1B7B3ACFB997,
	U3CGetPersistentPathKeyTypesU3Ed__14_MoveNext_m2A73997701AFAAC177C2A36A5BD0C0DC384D9067,
	U3CGetPersistentPathKeyTypesU3Ed__14_U3CU3Em__Finally1_m4DE1C1D93EFF1438C396A77B1A5A322803987FC1,
	U3CGetPersistentPathKeyTypesU3Ed__14_U3CU3Em__Finally2_m28CEA52B02EFB21808E5D57C32B540A7E382D13A,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m29217F8D66F77F53E359602C11B363F197DC05A6,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_Reset_mCAEC91788846F84F85BACA14A4340A852DC6FE97,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerator_get_Current_m94C8A47343BA3A294EB02E6F7B29AC703DC1281F,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m06387F1609C0D44AAFDBF33F4B5BE1AEA8396E2C,
	U3CGetPersistentPathKeyTypesU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m5D3536FEF22563E69093808247E92FCC1BB339EB,
	PrefabModification_Apply_m50F2AB4537A11A558F191DF8AC6BE55DD17CD78B,
	PrefabModification_ApplyValue_m997A47C824DA2CB9411E1E2514606085890329EE,
	PrefabModification_ApplyListLength_m8A979152B4E7ACEA90F46016B64456B491F92E81,
	PrefabModification_ApplyDictionaryModifications_mF127B6C88016C30B6F82838408DF247AC768F8F8,
	PrefabModification_ReplaceAllReferencesInGraph_m5C8F67CB9830E7F7CAAD8FD9401F6138740CDEB2,
	PrefabModification_GetInstanceFromPath_m34AB23216460B882191ADB80ED3BF3061720CC1E,
	PrefabModification_GetInstanceOfStep_mA74FAAAA53AA5F6C23338B65052D7064559F315B,
	PrefabModification_SetInstanceToPath_m317AAE5E04F9EE04DAC1CA6EDF9761B881BA790A,
	PrefabModification_SetInstanceToPath_m9B3428498928EBD20B51ACF2EAAD7912E15FC3B6,
	PrefabModification_TrySetInstanceOfStep_mAF7357CFEBEDD84DECAF2AB16CDAA2988810D43B,
	PrefabModification__ctor_mA19ED9A0A426DA0353487216863B0201C001B61D,
	U3CU3Ec__cctor_mC30CA7A75810343A709D31E717D8C615677EBD4C,
	U3CU3Ec__ctor_m0BBA48B9BEA5AF74E3FE44ADBA4EF8C5757BE620,
	U3CU3Ec_U3CGetInstanceOfStepU3Eb__13_0_m660409AEB311EADD7B4760E95A8F3D9309C39EE4,
	U3CU3Ec_U3CTrySetInstanceOfStepU3Eb__16_0_m93A17FE11531E75B68553C8ACF804E82B14822C8,
	ProperBitConverter_CreateByteToHexLookup_m73BAB7275A34CAF1AE466D0DCDD43CAA4587C19F,
	ProperBitConverter_BytesToHexString_mCA44F0644DA5A3625A15B93908FA864400367C63,
	ProperBitConverter_HexStringToBytes_m491292DF0F1BAFD272608EAF26F54F1966C20B12,
	ProperBitConverter_ToInt16_mB4A349F3564E34D1105D4F7BFB798B2293BACF4C,
	ProperBitConverter_ToUInt16_mAFD7797E67E36ACE512EF4B4429A72D1BA886C6E,
	ProperBitConverter_ToInt32_m8BE08C53CB3E9E65686DECD4C0F5A9579E74CDBC,
	ProperBitConverter_ToUInt32_m83A12DBF4D4358D6FF8072C754915445ABA6EAEF,
	ProperBitConverter_ToInt64_m7784692E792DAB80CE47D8577C5A0F31A7BD9451,
	ProperBitConverter_ToUInt64_m133360007C04BFED25A653F7C820D466B530CCAA,
	ProperBitConverter_ToSingle_m4F12B8A92122673D50B5DD6DC3846C9CB950DE06,
	ProperBitConverter_ToDouble_m26A37A3C763D9BE7FE68D2B0EDCF59FD2E18151E,
	ProperBitConverter_ToDecimal_m7D62AED65A3B88AE363DD6661040B232AF846827,
	ProperBitConverter_ToGuid_m6959EB7BC7985732ABC48833CEB9ABEDB51C7266,
	ProperBitConverter_GetBytes_mE978BA7F62BCA44C1EBBF9C968417210867C3CC7,
	ProperBitConverter_GetBytes_mAD9689C32EE5A59909B76F70AD8FDF66C19F4C01,
	ProperBitConverter_GetBytes_mDEE37B79F68B223FCFEAEAD833072188D7E03354,
	ProperBitConverter_GetBytes_m1E9E7ECFF7C7F84DE0DCAAF507C22D707B2504FD,
	ProperBitConverter_GetBytes_m14F078D09B264CD93C1F20E6383D6CA0A372826F,
	ProperBitConverter_GetBytes_mABE0323AA667F14644577991420B6570C749B2A4,
	ProperBitConverter_GetBytes_m532BCCAB14E0E176B6ECCC7BD2BF59B2F77B59D9,
	ProperBitConverter_GetBytes_m3AAC5768F1CD7B25425293474DB2BB60D82D2B22,
	ProperBitConverter_GetBytes_m1AC3431F2B260C60D34C8CB79DEF31DCA6E42235,
	ProperBitConverter_GetBytes_mF5EA86094F8CCCEC87B96D51ADD6E3F2B3E7D1DE,
	ProperBitConverter__cctor_m7E7AFF56468F153499C1F7F4E7D4175350501469,
	SerializationUtility_CreateWriter_m0B0E8B5B8026730D7ADBEB22AC1E607F9739F9B7,
	SerializationUtility_CreateReader_mBECC352FC81147283B604C97755E35F918692C2B,
	SerializationUtility_GetCachedWriter_mE97BBE490AE4B59E60821DA667004FD89A722088,
	SerializationUtility_GetCachedReader_m7F45574BE654EF4F27D1B5871ADE45E4DF1455D8,
	SerializationUtility_SerializeValueWeak_m7D035F5301ABAC60A42B68CD58510920226F9315,
	SerializationUtility_SerializeValueWeak_m28F328CE947B3828F637B46CE2B86B5D69EB04C4,
	NULL,
	NULL,
	SerializationUtility_SerializeValueWeak_m7E35D99AD9C14D0F71E1B3F6D3C1DDD14FC61CD0,
	SerializationUtility_SerializeValueWeak_mEE176F81E4D1ADED8BCAA3CDAA330CDA72EFBDC2,
	NULL,
	NULL,
	SerializationUtility_SerializeValueWeak_mA0AF8480E49272D593BEAC919CB6D7AB5476B440,
	SerializationUtility_SerializeValueWeak_m1370CD3DFAC7DE3F2188F780682183F6D27E3023,
	NULL,
	NULL,
	SerializationUtility_DeserializeValueWeak_m13B497C4E199A82EAA33CE5D71FE666525BFB280,
	SerializationUtility_DeserializeValueWeak_mE8D9AC2BF9E8237C4ADD6CD29F218DA925DC3BA6,
	NULL,
	NULL,
	SerializationUtility_DeserializeValueWeak_m38A9FE35D945F0B959EAE82BF973BAC141072070,
	SerializationUtility_DeserializeValueWeak_mBB71201C1A189308509BE9B8C8F4FA6E20C9D205,
	NULL,
	NULL,
	SerializationUtility_DeserializeValueWeak_m2F958811917E12829411D0C1117A85E6A8B9F706,
	SerializationUtility_DeserializeValueWeak_m9703E9A794B28C64FBF59B9E75D83C66CD495B27,
	NULL,
	NULL,
	SerializationUtility_CreateCopy_m212DA679FFFAC362DE044003A6010A7142B6DF8C,
	UnitySerializationUtility_OdinWillSerialize_mE90172AB6B9907F0BE025536B5AC60B716264FFC,
	UnitySerializationUtility_GuessIfUnityWillSerialize_m6687311F880826A5C23BDA1494CE723841691337,
	UnitySerializationUtility_GuessIfUnityWillSerializePrivate_m72FB37BEB41FFEF57F049AB763F761B5256CA84E,
	UnitySerializationUtility_GuessIfUnityWillSerialize_m9387F8E930238A0C1514BA899467917BB6D37D5D,
	UnitySerializationUtility_GuessIfUnityWillSerializePrivate_m9BCFAED74EB8508A5AE2F462A8768AFD577F0580,
	UnitySerializationUtility_SerializeUnityObject_mB794020F3D57DF255B0085BF10B44C9684B1F538,
	UnitySerializationUtility_SerializeUnityObject_mF73448F11561F4B1D7B949013B7C08713CCF04B7,
	UnitySerializationUtility_SerializeUnityObject_m6EA3208B12D567A0C6CD636FEAC973B555831741,
	UnitySerializationUtility_SerializeUnityObject_m362DF467DE558E8A64018EBAA86BCA0A98EF9526,
	UnitySerializationUtility_DeserializeUnityObject_m36F8BD16920F3F3F20FF9FB4A4941AAB10FFA027,
	UnitySerializationUtility_DeserializeUnityObject_m44191A48C594565DFC2E10CCBD9ABEF87BFDEDAB,
	UnitySerializationUtility_DeserializeUnityObject_m09D1ED8ACFB43987C4E04959A7C6772C12A27D22,
	UnitySerializationUtility_DeserializeUnityObject_m1D6B0F4C63C3F5D173BE03CDD74B491F140921D9,
	UnitySerializationUtility_DeserializeUnityObject_mCC0E99733A203933B95204B3EBAA97A05478DEA0,
	UnitySerializationUtility_SerializePrefabModifications_m1263B5E48150E7E9C70087AD05509C7A92C73743,
	UnitySerializationUtility_GetStringFromStreamAndReset_m0D34284A06058E881DF3032EE46582CA70208478,
	UnitySerializationUtility_DeserializePrefabModifications_m5B39A81AEC7EE189907F07FE071676577BFAB3A0,
	UnitySerializationUtility_CreateDefaultUnityInitializedObject_m55F8A571B3754DD32678FD49C784B8C3545BA5EA,
	UnitySerializationUtility_CreateDefaultUnityInitializedObject_mDD21877EC5B40D501F5D6145884B303D4AC6A606,
	UnitySerializationUtility_ApplyPrefabModifications_m73843E56AB2802B63B3944CADAA37EE955CAB300,
	UnitySerializationUtility_GetCachedUnityMemberGetter_m4AB3E52A12739A7B7BE2C168B54419FB4BB87EE4,
	UnitySerializationUtility_GetCachedUnityMemberSetter_m61BF3D74031792D57B15F74F53425A62755B763A,
	UnitySerializationUtility_GetCachedUnityWriter_m1B196B40E451BA415140BF9A8C84F6DC38FD6C60,
	UnitySerializationUtility_GetCachedUnityReader_mA26B230A1666802055916E1C2A7B82D1A24D23A0,
	UnitySerializationUtility__cctor_m2D14638BF3A5E75A2686C76936DDDE7B5CE8BFB1,
	U3CU3Ec__cctor_m575B5F84DE36F2439B8F3C602242410519BCDEAA,
	U3CU3Ec__ctor_mBC550CEA47CB3379CB0F0989DA646DE538BD2206,
	U3CU3Ec_U3CSerializePrefabModificationsU3Eb__21_0_mAD2D290899CBDC82733C24A5158E89A3E620B3D5,
	U3CU3Ec__DisplayClass27_0__ctor_m2395222375438043B6CF1FE712B31C151E507FF6,
	U3CU3Ec__DisplayClass27_0_U3CGetCachedUnityMemberGetterU3Eb__0_m005C84F2033684D22611E537144815B50ADCC842,
	U3CU3Ec__DisplayClass28_0__ctor_m72BE488A6C960069828129684FC60185F4587BD7,
	U3CU3Ec__DisplayClass28_0_U3CGetCachedUnityMemberSetterU3Eb__0_mEA1266310193A37416E30E570DE6D9AD41AC8DF4,
	FieldInfoExtensions_IsAliasField_m1D38EAEE76FEF928BB693F75D5DFDEB70FAA1089,
	FieldInfoExtensions_DeAliasField_m1E13F279646C60998D6805F037E9BB465A732234,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MemberInfoExtensions_GetAttributes_m349694041FCA1FF2377E50DB8AE258AD3139F5D5,
	MemberInfoExtensions_GetAttributes_m5B61F53BDD320500874039F20CE82C780CB4E90A,
	MemberInfoExtensions_GetNiceName_mCD6D786C0268CD6B40DCB737D5A8892369EF7E6D,
	MemberInfoExtensions_IsStatic_m0D65779AE20FC116BC260AF93A9398FAA167B8FD,
	MemberInfoExtensions_IsAlias_m2008AF6C8F05D73C9A66C3261B677D850D6B6A2F,
	MemberInfoExtensions_DeAlias_m7A87BBC0F97D506668D2919E6FADE5FA3A04EB2F,
	MethodInfoExtensions_GetFullName_m7289EF1866E240A4D8166835E23D870CAE19116E,
	MethodInfoExtensions_GetParamsNames_m0BD5DB755CF0FB1222863A5A7C73BED573E1DF53,
	MethodInfoExtensions_GetFullName_m16F11391FA8EB6B9AF3604D0D68DEE379A6E4169,
	MethodInfoExtensions_IsExtensionMethod_mCBF921ED9BCA898FDA5BC4B01670C47D86F0CB1C,
	MethodInfoExtensions_IsAliasMethod_mC735E84942B6AA66BE3CF2337C6EE77FA3F66257,
	MethodInfoExtensions_DeAliasMethod_mDE8989617E2D4C45BD200463E09A2ABCFD541A59,
	PathUtilities_HasSubDirectory_mA04B6A623ADEE2828CD0BD0C9A39E3699B0761B7,
	PropertyInfoExtensions_IsAutoProperty_mF5E9A6C194E664F9556D51B8D09007F8EAACE91C,
	PropertyInfoExtensions_IsAliasProperty_m87041E7971788AE6886143359B4B07B999125A2F,
	PropertyInfoExtensions_DeAliasProperty_m015DB168AC8F90AA5143293D1A290A60CF55A570,
	StringExtensions_ToTitleCase_m75E36FAEAEB8CB6FC90F54B5B30BD5CC673DD0C6,
	StringExtensions_IsNullOrWhitespace_mE7508CCAA9EA8CCC21D96229BD62C292539A1ED8,
	TypeExtensions_GetCachedNiceName_m1532573DEA6D23C49EC015558CC33328FF85F2AA,
	TypeExtensions_CreateNiceName_mDBCF3CAC485C4CAC57A3F37B6B5ED4317FE7909D,
	TypeExtensions_HasCastDefined_mFE16EE4FE9FF3852768497633F01D0E991C3BBED,
	TypeExtensions_IsValidIdentifier_m685A2493CA6F7C2AEF3C13EA3EEEAD612856C89C,
	TypeExtensions_IsValidIdentifierStartCharacter_m0CB6EE49AB1C6A7037A00D84CE9EB6E08E81D83B,
	TypeExtensions_IsValidIdentifierPartCharacter_m7AC59F411B66B097C9B05509D21AE43EBA3FFCC1,
	TypeExtensions_IsCastableTo_m12EF9C5160625DEE41CB62C3D8F2D2E7D378D51F,
	TypeExtensions_GetCastMethodDelegate_m987046E7D47F282D654496261B7883C28D6D31DA,
	NULL,
	TypeExtensions_GetCastMethod_mD03313876F01CE5085BFDB60AE52D9A338439CA9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeExtensions_HasCustomAttribute_m807409621DE640D0570A47C2C7C93A23726B4918,
	TypeExtensions_HasCustomAttribute_m02DCC99327737793E7746DA3B4CCDECFAA6554ED,
	NULL,
	TypeExtensions_ImplementsOrInherits_mBB069DC34FD0A3D5A9AAE45FF5FCC86D3A0AC3B9,
	TypeExtensions_ImplementsOpenGenericType_m3FC6E38C111B4C1BF15919E393DE4258469E09E5,
	TypeExtensions_ImplementsOpenGenericInterface_m36B2221D9263FB90DF896EDE0C20F294D955572F,
	TypeExtensions_ImplementsOpenGenericClass_mEF109A8BA5BADD6C482009E891DCAB86DDC3CEB9,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericType_m6CB1DCD49EBE82C352539704EA4A74670B21C5C0,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericClass_mD5EBA354E9E6A9607CFFEFCA448E418668E35056,
	TypeExtensions_GetArgumentsOfInheritedOpenGenericInterface_m61744A5C5CC21963B53858F744CE2B557E15E30A,
	TypeExtensions_GetOperatorMethod_mAC2F5581188D79F35D91677D1A9054EF842BBC26,
	TypeExtensions_GetOperatorMethods_m6D139F72C9221E06D5F6DF0C21F0656C2947C4BD,
	TypeExtensions_GetAllMembers_mAF1EC9227C3CDA77A78B41600741BC22B282B330,
	TypeExtensions_GetAllMembers_mD652499D27311B8B3E231622ECBC3223C8FEACA4,
	NULL,
	TypeExtensions_GetGenericBaseType_mFEF667CA3694CE4833C076C2465EC8BEC35856EA,
	TypeExtensions_GetGenericBaseType_mFB61C16F72798D16114C6F724FB868F9244345BC,
	TypeExtensions_GetBaseTypes_m983F94E0CFB953F1D94BC51BB7637490EE5DEB14,
	TypeExtensions_GetBaseClasses_mDC132249680C2F2FC1D237587A00841A013F61FC,
	TypeExtensions_TypeNameGauntlet_mE817E158A4A383649188FC15D0B79E2663C6499F,
	TypeExtensions_GetNiceName_m5520AD5A39ABAB082F2DD30222BCADBE03CD21B6,
	TypeExtensions_GetNiceFullName_mF291B98B75ECEB06240A18E2AA9BA2DE1F17A230,
	TypeExtensions_GetCompilableNiceName_m4B236B397503B7C845545AF25337B7515CE1352C,
	TypeExtensions_GetCompilableNiceFullName_m2599EDF5418ECCACF2B4E3DF981E9CFFCCD0B9E0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeExtensions_InheritsFrom_mB2C8CCD4BC2F89D305E9E82CED3E6D078F6B1C9D,
	TypeExtensions_GetInheritanceDistance_m0520A54AE277190031955B26C48CF1C4793B7AF3,
	TypeExtensions_HasParamaters_m8B781DA0243FDDE934633308D48B7006BB1393FF,
	TypeExtensions_GetReturnType_mE350B159933DEF36F13497D2648D1023927E0105,
	TypeExtensions_GetMemberValue_mA49CC5ACB73A92E1559601E6BCB9B465FCABED36,
	TypeExtensions_SetMemberValue_m535B84D7681E0D3ACD19641D7D7F6184C50D1DB7,
	TypeExtensions_TryInferGenericParameters_m16A99D765E67EBE6F6441327D86682E736D00457,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_m8A5E53288BFDFBAD31CE121DFCEBD72215CD3799,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_mF6CC8438959153139AB1EDEE8A68FF75C184C64F,
	TypeExtensions_AreGenericConstraintsSatisfiedBy_mE8B388E4541BE5A94533E37107FCCF33F0B5F900,
	TypeExtensions_GenericParameterIsFulfilledBy_m6CBD13619911088E757B92211349F616C9779E2D,
	TypeExtensions_GenericParameterIsFulfilledBy_m2E2EE166F01A2BC075772E3AF0C462C13DAB3D23,
	TypeExtensions_GetGenericConstraintsString_m235BDED19C12FF498E7BE5515FFFE62B3EFBC917,
	TypeExtensions_GetGenericParameterConstraintsString_m6150DD784FC1554DDCC958F42F821CA5D9631E5B,
	TypeExtensions_GenericArgumentsContainsTypes_m8BC8C323DE7F8BDAE770B61B0FD7CAC68EF751FB,
	TypeExtensions_IsFullyConstructedGenericType_m9FA2CB4ECC617B95C129F22FBA6619B03786B236,
	TypeExtensions_IsNullableType_m9E8A5C8C3F6C473C6F086729A8966AA502A6AC5F,
	TypeExtensions_GetEnumBitmask_mF4BC3721F47A34A4E4A40D067F5A4A2A2483045D,
	TypeExtensions_SafeGetTypes_m3E480DDB012372630A76753275F4063BCBCA515D,
	TypeExtensions_SafeIsDefined_mB09B7D31E0F9E2B39976557D61A5C5A7205D4F19,
	TypeExtensions_SafeGetCustomAttributes_m38CC04731F22628B3292D1A49D5CE61C2673BB25,
	TypeExtensions__cctor_mD461614546003B9003AA8F62C3C0146C1EEC0941,
	U3CU3Ec__DisplayClass22_0__ctor_mDDFEE96485F9BE0186CBDC36FE1C9C6511C759AE,
	U3CU3Ec__DisplayClass22_0_U3CGetCastMethodDelegateU3Eb__0_m64C3E77A88DACFAF3F12C7CA7FDAB97EDA92B5EF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass35_0__ctor_mD4D067CEB5C9592D987A9CCDB2289B028E858348,
	U3CU3Ec__DisplayClass35_0_U3CImplementsOpenGenericInterfaceU3Eb__0_m0024B9BED2445A7118CCFA1F605CB3334B8C8E5E,
	U3CU3Ec__DisplayClass40_0__ctor_mAE9A9CEA217A2EFAF5057B26929D00E8E2B95858,
	U3CU3Ec__DisplayClass40_0_U3CGetOperatorMethodU3Eb__0_m61D083358392C079B66AC313E2142095411E53A0,
	U3CU3Ec__DisplayClass41_0__ctor_m3DE835180C4FFBDE2FDD604097FE5A9BFC8F87E6,
	U3CU3Ec__DisplayClass41_0_U3CGetOperatorMethodsU3Eb__0_mFD614AD39E7C16A45E46379BD58D6340B96CE980,
	U3CGetAllMembersU3Ed__42__ctor_mECC28ADDEDB2CFA0AD434C22F8369FA6DAC56390,
	U3CGetAllMembersU3Ed__42_System_IDisposable_Dispose_m9E319A62EFB4C3E57BED82A892250F00FC9B43B8,
	U3CGetAllMembersU3Ed__42_MoveNext_m1CF3C44D786CEE618E67877AC112CC9EE665ED26,
	U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_mCE2F1CA338CF519DE8199720D6F10F60C833DC92,
	U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_Reset_mA8107830BE90C795E27D5738C36CD0D452AFAF82,
	U3CGetAllMembersU3Ed__42_System_Collections_IEnumerator_get_Current_m9D2C83C4E15A768D0658BBB1E95B8B963F88BD9E,
	U3CGetAllMembersU3Ed__42_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_mF7146FB637C47E3922C611D84C0849179F9D44DD,
	U3CGetAllMembersU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mF3BD4F1E1832E4D4DB4F6C5B6B1040A4530DECA4,
	U3CGetAllMembersU3Ed__43__ctor_mD6E740BB16530EB8DC13A05DC855CA6004C722C5,
	U3CGetAllMembersU3Ed__43_System_IDisposable_Dispose_mFFD18EABCD5841589699BECB72FE46280928EB67,
	U3CGetAllMembersU3Ed__43_MoveNext_m532C28E07A56D917CE6784BF962C09AA06120E77,
	U3CGetAllMembersU3Ed__43_U3CU3Em__Finally1_m354EA9E7247B309D3AF0A23B9C36A847FB594029,
	U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_MemberInfoU3E_get_Current_m32C3AE5D7E414AF63C4E2BFCFE1B524C69C74E28,
	U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_Reset_m0A334D28C9D62519A97501CC7530ECF74CB89295,
	U3CGetAllMembersU3Ed__43_System_Collections_IEnumerator_get_Current_m37C697AEF1C2BE80E9464F0667B0466CB9FD9842,
	U3CGetAllMembersU3Ed__43_System_Collections_Generic_IEnumerableU3CSystem_Reflection_MemberInfoU3E_GetEnumerator_m544D77F6409E37E5AAFDE93424D13976A215278D,
	U3CGetAllMembersU3Ed__43_System_Collections_IEnumerable_GetEnumerator_mE50E2086242B0D28A1760315F8A023506D39B591,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CGetBaseClassesU3Ed__48__ctor_mC5020F833A3331D50ED3E4E46E31AF973C690C63,
	U3CGetBaseClassesU3Ed__48_System_IDisposable_Dispose_m42AFBCF3E84334C072FB5FDD3421EF847CB8EB46,
	U3CGetBaseClassesU3Ed__48_MoveNext_mEA5BFC904E5DEDA1EB4AA7366AFA47BD19EAAA4E,
	U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_mCE1DBB20C09DA287D6B34CC1D3635B6D0889257F,
	U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_Reset_mCBDCB3D8C952B2FC5F8E7C5E65919C03719387F6,
	U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerator_get_Current_mD2EDA6986FDE391CBF36B8BA992B4C7E79FC0BD2,
	U3CGetBaseClassesU3Ed__48_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m3F002A1BBB23E235B788DBBBDE103CBBF592F3C7,
	U3CGetBaseClassesU3Ed__48_System_Collections_IEnumerable_GetEnumerator_m98F625F59DD4C3E6DF98E87E34A20BFEBB4C152F,
	UnityExtensions__cctor_mB9941F239C1115B445B60D3B19D911E848B06813,
	UnityExtensions_SafeIsUnityNull_m148B87C1D426B3FED2F450A9776997A28ED580CF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WeakValueGetter__ctor_mC25912C1222D3D105A3B571C74F46E91ABF2E7E4,
	WeakValueGetter_Invoke_mA5F428CA3C08F91E2E0E533D292F4358A2CB50AA,
	WeakValueGetter_BeginInvoke_mCF4151097E01C35E0832BA6D2D7CD5A68B7EB823,
	WeakValueGetter_EndInvoke_m08CE8064927417650195E223AE2095BBFF13FE88,
	WeakValueSetter__ctor_mF9A8C8647D42D24A8269A1454EB1BB079E6DD367,
	WeakValueSetter_Invoke_mB499FB8A653D20DACDBB7E06448CA49F5AE4ECD7,
	WeakValueSetter_BeginInvoke_m8415DE549ABF756980451E5954C3F95C251E9F6E,
	WeakValueSetter_EndInvoke_mFB31A15078B1B15A5438D5011AE5D736BF21A112,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmitUtilities_get_CanEmit_m6A2EFD81967C966AFA35F44F0AEC230831098C32,
	NULL,
	EmitUtilities_CreateWeakStaticFieldGetter_m501994BA75C597AE8E7C17DA32076990924A2E3A,
	NULL,
	EmitUtilities_CreateWeakStaticFieldSetter_m1306D5871C868B751B070F8090B58D80C494A517,
	NULL,
	NULL,
	EmitUtilities_CreateWeakInstanceFieldGetter_m80FD606B8AA5EA5DFC518906D6DC79C4CD370EB4,
	NULL,
	NULL,
	EmitUtilities_CreateWeakInstanceFieldSetter_m7AB5E226D7BB63093B6872FB6C2EE35E8D2817A8,
	EmitUtilities_CreateWeakInstancePropertyGetter_m446BA8C86CA5FE5FC197BA041700AFE13E5B3FFE,
	EmitUtilities_CreateWeakInstancePropertySetter_mD8EA9D775FFAA83132EACF11E3EAAB2270029112,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmitUtilities_CreateStaticMethodCaller_mAAB81F7426E87E32721FF38A68C6828C744836BD,
	NULL,
	EmitUtilities_CreateWeakInstanceMethodCaller_mDDC78D426B93498948BE485AE7254B4BDD6AF966,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass3_0__ctor_m34B8F7BAD9EAD75A9BBCD508B9612EEF99EBE0AD,
	U3CU3Ec__DisplayClass3_0_U3CCreateWeakStaticFieldGetterU3Eb__0_m86026FCD57D37B8534AAB862C375C43926204451,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass5_0__ctor_mEA67D5D4ED3A8E7368E48D8D27CF0A1158F5C7E3,
	U3CU3Ec__DisplayClass5_0_U3CCreateWeakStaticFieldSetterU3Eb__0_m8AD0502169274D85BCFE86E102DF09C4CDEEA4CE,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass8_0__ctor_m3774AC56DC6965D7BA91A7DCB053316B1D75CB14,
	U3CU3Ec__DisplayClass8_0_U3CCreateWeakInstanceFieldGetterU3Eb__0_m798776B45AE9644C8642A01F4ECD19D2683CB32D,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass11_0__ctor_m204CF01344CE0096F747F1530CDFD6C32793C30D,
	U3CU3Ec__DisplayClass11_0_U3CCreateWeakInstanceFieldSetterU3Eb__0_mD514088E786DD146B6BF3C88F44AC0D0A9FBCAC5,
	U3CU3Ec__DisplayClass12_0__ctor_mD80107369A63C94184567AEE3A4B9F2FD518EB6E,
	U3CU3Ec__DisplayClass12_0_U3CCreateWeakInstancePropertyGetterU3Eb__0_m53AE48240AC9FAAE3F328E21240A8C5FB2494B46,
	U3CU3Ec__DisplayClass13_0__ctor_m14841C32661C631E9193777037BEEB3CDB8C737A,
	U3CU3Ec__DisplayClass13_0_U3CCreateWeakInstancePropertySetterU3Eb__0_m1A765B4D7FFA23F0F674AE075113D73F065C8BF0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass21_0__ctor_mF7D5D0502C6E32DEB589B1731EC6E41D8DB0BCB6,
	U3CU3Ec__DisplayClass21_0_U3CCreateWeakInstanceMethodCallerU3Eb__0_m7878B49FB1EA302C4D20562567D84E31E7707164,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FastTypeComparer_Equals_mBDB33646E25667DB2509E7AA121ECA11C2D313C0,
	FastTypeComparer_GetHashCode_mA7A7AE3A534E8B94D78730B0719917A099C277C3,
	FastTypeComparer__ctor_mF53C14AA3EBA18D924BF8DAB322312D245EC3DAC,
	FastTypeComparer__cctor_mF44B00C0BA807130B6301A79E4E6E66C4EB3A0C6,
	NULL,
	NULL,
	NULL,
	ImmutableList__ctor_mD3AD0AE6E6D3082F26A458B8E7A143473A3139E9,
	ImmutableList_get_Count_m1C404BF3D36800B1CC58823948768ACCE8E0A43C,
	ImmutableList_get_IsFixedSize_m04DF44C325A4A0B7B19478B949E13723557D5EC8,
	ImmutableList_get_IsReadOnly_mAC133AF521699C29D5E0EEB01623BE9CAA26AA51,
	ImmutableList_get_IsSynchronized_m43E4AA692B59DF5E8ADAD5C2551A3606ECE8D418,
	ImmutableList_get_SyncRoot_mA9AF60F215B700336A78A6A9ECD6B002858AD785,
	ImmutableList_System_Collections_IList_get_Item_m02E44E3A3DA91369B85B90ED1427F8CE985F3596,
	ImmutableList_System_Collections_IList_set_Item_m3BB5159F43EB47FB21F4532E300F5D165184B527,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_get_Item_m75BBBB8651EC734C2707449C08532E6F432FEC1E,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_set_Item_m1BC154A26CE149693DD12E014192B551391CFCEA,
	ImmutableList_get_Item_mECF538C1C9EEAFE0E3B07D7C04EED873F790453F,
	ImmutableList_Contains_m188AD0A52AB9811B009DA0372C735C7766B0877F,
	ImmutableList_CopyTo_m0192579DBC7B00BB7805FA897E17E664FA1DB495,
	ImmutableList_CopyTo_mB190CC0DD9E8986001374BE6B16DCE54BCCDBDA9,
	ImmutableList_GetEnumerator_m727A7B69B07F066B80A7AB84DDFCAD7FC191BE14,
	ImmutableList_System_Collections_IEnumerable_GetEnumerator_m7410D7F5DD22BFA66DC2519EB731CA29B9F53B34,
	ImmutableList_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m92F132F345C71BFEAD934D6EFC4C22DBB696606F,
	ImmutableList_System_Collections_IList_Add_m651A59979271AA1C84A5F318DA8D797BDD795CDA,
	ImmutableList_System_Collections_IList_Clear_mC731204B3AF7555F80B1916418A6E1DFA02B5CD5,
	ImmutableList_System_Collections_IList_Insert_m99C94F6E0A896CDCEE0912A704A0C8D51F0B11E7,
	ImmutableList_System_Collections_IList_Remove_m8F69CB7E6496AFAC09D74C5BDD12E9D1D10459EB,
	ImmutableList_System_Collections_IList_RemoveAt_mAE15FD52A3BBCF116233AB657246B5CA007F6A2F,
	ImmutableList_IndexOf_m8F53B2B91F12845107CA21E6574E5652F4CC5E21,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_RemoveAt_m77A06A620F48A9656A7C8AC941FA180E6F2BA0D5,
	ImmutableList_System_Collections_Generic_IListU3CSystem_ObjectU3E_Insert_m3EB9C824D35401BB1491D4DBE36AF1A6BE4587FA,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Add_m1D1C0EC8CC8AA410A268196E00AD0ABDA7DF7DA6,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Clear_mD7DAB8DD13D5FEB0F4F8A1419B2CA9D0060A0F95,
	ImmutableList_System_Collections_Generic_ICollectionU3CSystem_ObjectU3E_Remove_mA01260F225FA8A3FB795DB1A01AA09233FAD1075,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25__ctor_mB98A496FC6D73EB5897246F5885D9ED7FAC7284A,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m14F4C94791AFA5DA05AE5DE20606B7D47011868F,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_MoveNext_mB8B7B9FB1825CE9610F26025EC18205A500AF186,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_U3CU3Em__Finally1_m18871DF14FF7A1788814024B4D840F5DCD716AD7,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD01C87D86A2E129FDDE79B964FE84B546B589053,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m493988934AAA09863EA33E918C0579D8826E0454,
	U3CSystemU2DCollectionsU2DGenericU2DIEnumerableU3CSystemU2DObjectU3EU2DGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m2343A58E3F14DD579E8F296C8063D129E1AF5E18,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MemberAliasFieldInfo__ctor_m4C7EFE1B86FB7272AAE2C9C21C985D55DA66775F,
	MemberAliasFieldInfo__ctor_m97665DF2870846A3059EBEA7DA1A65F5D21181F6,
	MemberAliasFieldInfo_get_AliasedField_mC29D1AA05B88ED32A55544D133A80ECC08C1431F,
	MemberAliasFieldInfo_get_Module_mE2B2DF70C87FEBD7768AE569E16DBF3E01A290C5,
	MemberAliasFieldInfo_get_MetadataToken_mF8151E7B6B152FE7F5636E63B40679889B505747,
	MemberAliasFieldInfo_get_Name_mD85D2E84045F5ABE921DC41901BF758C32449F42,
	MemberAliasFieldInfo_get_DeclaringType_mFC779BA7B2E63918EB7D8CEFBA11FC8D63598F42,
	MemberAliasFieldInfo_get_ReflectedType_m259F9696743632AE55534C6F6197E4A6F029BC4C,
	MemberAliasFieldInfo_get_FieldType_mE0933ABE9E051A77BC1DCCC231687A182B0AD0AE,
	MemberAliasFieldInfo_get_FieldHandle_mBEA27E0ECFBDAFBD99BB48CD47C15D15C275FBD5,
	MemberAliasFieldInfo_get_Attributes_mFE2217C52681C19C8A99C7001AFC21BEED92166B,
	MemberAliasFieldInfo_GetCustomAttributes_mB3B3DC80669D90E19E413281E442F3D12C9CDD46,
	MemberAliasFieldInfo_GetCustomAttributes_m94BF1FC2E8A40C017F29963AE74D262C841C8FF3,
	MemberAliasFieldInfo_IsDefined_mF449506D9BFF0883F1DE5959615950C379421519,
	MemberAliasFieldInfo_GetValue_m3A68DD18FE9E438D960193E4EF55F5E81778BD1F,
	MemberAliasFieldInfo_SetValue_m419B08D5F4F9505C3103D54E7B6ABD58D0BF266C,
	MemberAliasMethodInfo__ctor_mE3B16B07732B86C8952351853C15EC501543F879,
	MemberAliasMethodInfo__ctor_m1981CD912E83CF99D0850B881396DC97D440B8EC,
	MemberAliasMethodInfo_get_AliasedMethod_m2D0C60CA7D97A0BA54DAC4EDF8AFB4405D83A1D1,
	MemberAliasMethodInfo_get_ReturnTypeCustomAttributes_mAD4A554DE6DC6885B6D52B4FA2F1E75E01E6DE29,
	MemberAliasMethodInfo_get_MethodHandle_mA901A429350D8493DBC78B676E06A2266A1073B1,
	MemberAliasMethodInfo_get_Attributes_m270BA994621DF661A7FDEB63B5DE443698699681,
	MemberAliasMethodInfo_get_ReturnType_mE5E76CB44F44545E5DF1E0CE597BD76019D100EE,
	MemberAliasMethodInfo_get_DeclaringType_m2FD3E5A6103883EC85523A9436F5AAAE789ED32A,
	MemberAliasMethodInfo_get_Name_m0ACBE0BCF7D8BF8EB433428A8C0B78FC066BBAD4,
	MemberAliasMethodInfo_get_ReflectedType_mC385E812CE5E2F7A4B6E5A36C8FA0F3BDD036FED,
	MemberAliasMethodInfo_GetBaseDefinition_m18010FDDFC15BF72B98B462CAB7B6EC3A8FF0F16,
	MemberAliasMethodInfo_GetCustomAttributes_m61937014A1B94160598354BAF881FE54F3854EC6,
	MemberAliasMethodInfo_GetCustomAttributes_mC5D6D08C832D6494C4156FA932FDCF5BEDB6542A,
	MemberAliasMethodInfo_GetMethodImplementationFlags_mB33EB9AB49A3570DF28CD47C20EFAEB7A76D8236,
	MemberAliasMethodInfo_GetParameters_m9ED19AF9B9C759827A871F9DEBA7DCF643F3A381,
	MemberAliasMethodInfo_Invoke_mBD9A3D588627BD1268068F591767567990A42ACA,
	MemberAliasMethodInfo_IsDefined_mCCB5831AFF2D69829B0C2CA25447F657B8728936,
	MemberAliasPropertyInfo__ctor_m6E80A656E48FE3D0B0D28D511DC6371DD69626F4,
	MemberAliasPropertyInfo__ctor_m910BE931BB1FED2AF45A3115AF9F621CCFC33757,
	MemberAliasPropertyInfo_get_AliasedProperty_m4A9FD4981F8E2EB2AE94916AC2C3927222830BD0,
	MemberAliasPropertyInfo_get_Module_m09DB8BEA369ED7AA0488A8F7FD380DC5FBB7F538,
	MemberAliasPropertyInfo_get_MetadataToken_m2B13A5FACC2B68CE017146D7765F68AFFCF536FE,
	MemberAliasPropertyInfo_get_Name_m1B09EF82176BB25BB0190E16233D4C0EE637A9FC,
	MemberAliasPropertyInfo_get_DeclaringType_mEBEB80B5D0AC4B71B0D3D099923AFDA1871549F3,
	MemberAliasPropertyInfo_get_ReflectedType_m04BC94F2AC8A09DD295C81EE0E937FB4C18230F7,
	MemberAliasPropertyInfo_get_PropertyType_m9753E8FC71707FD1886F2E983473900210989C3C,
	MemberAliasPropertyInfo_get_Attributes_m33801C43E68C13DD2DD3EB038A74BC1B7B580CCD,
	MemberAliasPropertyInfo_get_CanRead_m91187BCD842EF6D3D735896D034B0DA3CEFE6DD7,
	MemberAliasPropertyInfo_get_CanWrite_m45A4127C64DC385ED90DEA567867FDF130C00A29,
	MemberAliasPropertyInfo_GetCustomAttributes_mEC95B81CFFCD71D911EEE5261178A7B6C8D7ED33,
	MemberAliasPropertyInfo_GetCustomAttributes_m681684AF9EB34EDAE940E86B6E1FFE90FD697677,
	MemberAliasPropertyInfo_IsDefined_mAA506EF00AEFE9E6247035409396F9FA93017F68,
	MemberAliasPropertyInfo_GetAccessors_mA99FB37BC5D56BF7A1782F70C85A13270364D2DA,
	MemberAliasPropertyInfo_GetGetMethod_m711D930F7F61FE2BD71B33E4DA2B9DBB2EEF6CD4,
	MemberAliasPropertyInfo_GetIndexParameters_mC609DFF130C7919A91A67D9BEBAB43A98093AEDC,
	MemberAliasPropertyInfo_GetSetMethod_mD4AA6242FEFB26F3378CD8AD21B1ADC377DF2D98,
	MemberAliasPropertyInfo_GetValue_m6DAF26ACCA5C234675D6C9C58BE77A46E567C10A,
	MemberAliasPropertyInfo_SetValue_mDC965AFD3ACE84A8BA7D8834A3CA395B19CEA0D2,
	NULL,
	NULL,
	NULL,
	NULL,
	UnityVersion__cctor_m109F33D254286A97D4AAC1CFFF08D822C27B2864,
	UnityVersion_EnsureLoaded_m38F0696D643531BF201CD3DC509423ECEF98FCAB,
	UnityVersion_IsVersionOrGreater_mB393B211415A7E3318A2D0E077524C6DA99E1470,
	NULL,
	NULL,
	NULL,
	NULL,
	UnsafeUtilities_StringFromBytes_mA622CC1B81E541F113BE52B88191FE9F4D0F8563,
	UnsafeUtilities_StringToBytes_mF86FA8885DC3CB27B719D91EA4DAEBA36776C6BF,
	UnsafeUtilities_MemoryCopy_mD475484BEAC5A72AAAA5D6C8A74C8EB8789FBD34,
	UnsafeUtilities_MemoryCopy_m6975422A2EF04A36117833CBF7EBB194E4F98E1C,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m415FE5942EE0C0FB2CD006F84FCCC802362FCF5A,
};
static const int32_t s_InvokerIndices[1752] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1460,
	1259,
	1476,
	1476,
	1476,
	1476,
	1476,
	1460,
	1259,
	1476,
	1476,
	1476,
	1476,
	1476,
	1460,
	1259,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	1476,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1440,
	663,
	663,
	1476,
	1476,
	2458,
	2421,
	2458,
	1440,
	1459,
	270,
	1476,
	270,
	1476,
	1101,
	270,
	270,
	1476,
	270,
	1476,
	1440,
	1241,
	1428,
	1231,
	786,
	1440,
	1241,
	1428,
	1231,
	786,
	270,
	1476,
	270,
	1476,
	789,
	1428,
	1428,
	1440,
	1440,
	1241,
	1440,
	1241,
	1054,
	1459,
	1054,
	1459,
	-1,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1459,
	1476,
	1476,
	1476,
	1440,
	1459,
	1459,
	1440,
	1241,
	1459,
	1428,
	1439,
	1240,
	479,
	1476,
	1476,
	1241,
	1476,
	1476,
	1476,
	789,
	1440,
	1241,
	1440,
	1241,
	1476,
	487,
	789,
	1241,
	1232,
	1476,
	-1,
	1241,
	786,
	786,
	784,
	789,
	785,
	789,
	784,
	791,
	785,
	786,
	787,
	791,
	785,
	786,
	787,
	782,
	792,
	783,
	791,
	1476,
	1476,
	1440,
	1476,
	789,
	1476,
	1054,
	1054,
	1054,
	1459,
	1459,
	-1,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1459,
	1054,
	1054,
	1476,
	1440,
	1440,
	1476,
	1476,
	1089,
	1440,
	1476,
	1459,
	1459,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1089,
	1476,
	2458,
	2458,
	1476,
	520,
	617,
	617,
	617,
	1476,
	789,
	1232,
	487,
	789,
	1476,
	1476,
	1241,
	2248,
	2248,
	2248,
	2248,
	2248,
	2248,
	2248,
	2248,
	2248,
	2248,
	2248,
	2248,
	2248,
	2248,
	-1,
	791,
	791,
	785,
	782,
	783,
	784,
	784,
	786,
	789,
	786,
	787,
	1241,
	786,
	791,
	785,
	792,
	789,
	786,
	787,
	785,
	1476,
	1440,
	1241,
	1241,
	1476,
	1230,
	1230,
	1230,
	1231,
	1231,
	1261,
	1232,
	1232,
	1223,
	1221,
	1229,
	1231,
	1089,
	2458,
	2458,
	1476,
	476,
	480,
	480,
	480,
	1440,
	1241,
	1440,
	1241,
	1459,
	1440,
	1428,
	1428,
	1440,
	1241,
	1440,
	1054,
	1459,
	1054,
	1459,
	-1,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1459,
	1476,
	1476,
	1440,
	1241,
	1440,
	1241,
	1459,
	1440,
	1241,
	1440,
	1476,
	487,
	789,
	1241,
	1232,
	1476,
	-1,
	1241,
	786,
	786,
	784,
	789,
	785,
	789,
	784,
	791,
	785,
	786,
	787,
	791,
	785,
	786,
	787,
	782,
	792,
	783,
	791,
	1476,
	1476,
	789,
	1440,
	1241,
	1476,
	1054,
	1054,
	1459,
	1054,
	1459,
	-1,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1459,
	1476,
	1440,
	1459,
	1459,
	1476,
	1054,
	1427,
	1459,
	1427,
	1428,
	1429,
	1459,
	1427,
	1428,
	1429,
	1419,
	1459,
	1463,
	1421,
	1426,
	1476,
	489,
	1476,
	1476,
	487,
	789,
	1241,
	1232,
	1476,
	-1,
	791,
	791,
	785,
	782,
	783,
	786,
	787,
	1241,
	786,
	791,
	785,
	792,
	789,
	784,
	786,
	787,
	786,
	784,
	789,
	785,
	1476,
	1476,
	1440,
	789,
	486,
	1241,
	1257,
	1231,
	1241,
	2445,
	2458,
	1440,
	1241,
	789,
	1476,
	1476,
	438,
	130,
	1088,
	528,
	166,
	1427,
	848,
	1427,
	1476,
	1427,
	2458,
	1241,
	1459,
	1440,
	1241,
	1440,
	1241,
	1476,
	1476,
	1054,
	1054,
	1054,
	1459,
	1459,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1459,
	-1,
	1054,
	1054,
	1054,
	1054,
	1054,
	1054,
	1440,
	1476,
	1459,
	1459,
	1427,
	1459,
	1427,
	1428,
	1429,
	1459,
	1427,
	1428,
	1429,
	1419,
	1459,
	1463,
	1421,
	1426,
	1440,
	1241,
	1241,
	1440,
	1241,
	1232,
	487,
	789,
	1476,
	1476,
	1241,
	1476,
	791,
	791,
	785,
	782,
	792,
	783,
	784,
	789,
	786,
	784,
	785,
	786,
	787,
	786,
	1241,
	-1,
	791,
	789,
	785,
	786,
	787,
	1476,
	1440,
	-1,
	-1,
	-1,
	-1,
	-1,
	1440,
	663,
	663,
	1476,
	2458,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	663,
	663,
	1476,
	663,
	663,
	1476,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1476,
	-1,
	-1,
	-1,
	2126,
	-1,
	-1,
	-1,
	-1,
	2173,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1440,
	789,
	1000,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	663,
	663,
	1476,
	663,
	663,
	1440,
	1476,
	495,
	1440,
	1459,
	1101,
	1476,
	1476,
	1231,
	786,
	789,
	2458,
	2421,
	580,
	1101,
	580,
	580,
	2013,
	580,
	1565,
	1858,
	1946,
	1476,
	2458,
	1476,
	789,
	1476,
	1263,
	1241,
	809,
	1440,
	1241,
	1440,
	1241,
	1440,
	1241,
	1440,
	1241,
	1465,
	1440,
	1440,
	1241,
	736,
	997,
	997,
	995,
	1000,
	1476,
	1476,
	1476,
	1476,
	1440,
	1241,
	601,
	615,
	608,
	615,
	1440,
	1241,
	615,
	615,
	1241,
	1241,
	1440,
	1459,
	1101,
	302,
	1257,
	2172,
	2172,
	1101,
	1428,
	2458,
	1440,
	1241,
	1241,
	1241,
	789,
	1476,
	1440,
	1241,
	1440,
	1241,
	1476,
	1440,
	1241,
	1428,
	1231,
	1428,
	1231,
	1241,
	1241,
	1241,
	1476,
	1476,
	1476,
	1263,
	1241,
	809,
	1440,
	1241,
	1465,
	1440,
	1440,
	1241,
	1440,
	1241,
	1440,
	1241,
	1440,
	1241,
	615,
	615,
	615,
	615,
	615,
	1476,
	1476,
	1476,
	1476,
	1476,
	580,
	580,
	1101,
	1476,
	2458,
	2173,
	2445,
	2445,
	2445,
	2458,
	2458,
	1476,
	1101,
	1101,
	1476,
	1101,
	1101,
	495,
	1476,
	1101,
	495,
	1476,
	892,
	471,
	1476,
	-1,
	-1,
	-1,
	-1,
	-1,
	878,
	466,
	1476,
	881,
	468,
	1476,
	-1,
	-1,
	-1,
	-1,
	889,
	469,
	1476,
	892,
	471,
	1476,
	945,
	479,
	1476,
	980,
	483,
	1476,
	983,
	484,
	1476,
	1101,
	495,
	1476,
	2421,
	2363,
	-1,
	2363,
	1000,
	789,
	488,
	2363,
	2248,
	1476,
	2458,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1151,
	497,
	1476,
	1000,
	488,
	1476,
	892,
	471,
	1476,
	945,
	479,
	1476,
	980,
	483,
	1476,
	983,
	484,
	1476,
	1440,
	663,
	663,
	1476,
	2458,
	663,
	663,
	1476,
	2458,
	663,
	663,
	1476,
	2458,
	270,
	1476,
	-1,
	-1,
	-1,
	-1,
	663,
	663,
	1476,
	2458,
	1440,
	1000,
	1000,
	789,
	789,
	1476,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1440,
	1000,
	1000,
	536,
	-1,
	-1,
	-1,
	1241,
	1440,
	546,
	1168,
	1004,
	1476,
	1440,
	548,
	1173,
	1005,
	1476,
	1440,
	549,
	1178,
	1006,
	1476,
	663,
	663,
	1476,
	2458,
	663,
	663,
	1476,
	2458,
	1440,
	663,
	663,
	1476,
	2458,
	954,
	1460,
	1259,
	2458,
	663,
	663,
	1476,
	663,
	663,
	1476,
	2458,
	663,
	663,
	1476,
	2458,
	663,
	663,
	1476,
	2458,
	1459,
	1459,
	1476,
	-1,
	-1,
	1476,
	1241,
	1440,
	1241,
	615,
	608,
	1476,
	1476,
	1476,
	2452,
	2440,
	2419,
	2458,
	2458,
	2458,
	2458,
	663,
	663,
	1476,
	2458,
	663,
	663,
	1476,
	2458,
	663,
	663,
	1476,
	2458,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1440,
	1476,
	1476,
	1476,
	2360,
	2363,
	2458,
	1476,
	2458,
	2421,
	2421,
	-1,
	2126,
	2248,
	2363,
	2126,
	2126,
	2363,
	2458,
	1476,
	553,
	554,
	1231,
	1476,
	1459,
	1476,
	1440,
	1476,
	1440,
	1440,
	1440,
	2458,
	2126,
	2126,
	2126,
	2385,
	2385,
	2363,
	2126,
	2032,
	2126,
	2032,
	1920,
	2385,
	2458,
	1476,
	1000,
	1000,
	1101,
	1476,
	1101,
	2458,
	2032,
	2445,
	2385,
	2385,
	2363,
	2126,
	1914,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1476,
	1000,
	2458,
	1476,
	1000,
	1101,
	1231,
	1476,
	1459,
	1476,
	1476,
	1440,
	1476,
	1440,
	1440,
	1440,
	1241,
	1241,
	1241,
	1241,
	1833,
	2126,
	2126,
	2032,
	1585,
	1793,
	1476,
	2458,
	1476,
	1101,
	1101,
	2368,
	2127,
	2363,
	2075,
	2075,
	2090,
	2090,
	2101,
	2101,
	2197,
	2066,
	2059,
	2070,
	2020,
	2020,
	2021,
	2021,
	2022,
	2022,
	2024,
	2018,
	2017,
	2019,
	2458,
	1919,
	1919,
	1735,
	1735,
	2248,
	2029,
	-1,
	-1,
	1831,
	1674,
	-1,
	-1,
	1915,
	1913,
	-1,
	-1,
	2363,
	2126,
	-1,
	-1,
	1915,
	1753,
	-1,
	-1,
	1915,
	1915,
	-1,
	-1,
	2363,
	1958,
	2385,
	2385,
	2385,
	2385,
	1822,
	1581,
	1581,
	2033,
	2015,
	1664,
	1663,
	1663,
	2248,
	2121,
	2363,
	2126,
	2363,
	2123,
	2032,
	2363,
	2363,
	1905,
	1905,
	2458,
	2458,
	1476,
	536,
	1476,
	993,
	1476,
	663,
	2385,
	2127,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2363,
	2127,
	2363,
	2385,
	2385,
	2127,
	2126,
	2363,
	2363,
	2385,
	2385,
	2127,
	2176,
	2177,
	2385,
	2127,
	2363,
	2385,
	2363,
	2363,
	1956,
	2385,
	2381,
	2381,
	1956,
	1922,
	-1,
	1922,
	-1,
	-1,
	-1,
	-1,
	-1,
	1956,
	1795,
	-1,
	2176,
	2176,
	2176,
	2176,
	2126,
	2126,
	2126,
	2123,
	2123,
	2123,
	1919,
	-1,
	2126,
	1918,
	2127,
	2127,
	2363,
	2363,
	2363,
	2363,
	2363,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2176,
	2092,
	1956,
	2363,
	2126,
	2032,
	1947,
	2176,
	2176,
	2176,
	2176,
	1794,
	2127,
	2127,
	2176,
	2385,
	2385,
	2102,
	2363,
	1956,
	1922,
	2458,
	1476,
	1000,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1476,
	1101,
	1476,
	1101,
	1476,
	1101,
	1231,
	1476,
	1459,
	1440,
	1476,
	1440,
	1440,
	1440,
	1231,
	1476,
	1459,
	1476,
	1440,
	1476,
	1440,
	1440,
	1440,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1231,
	1476,
	1459,
	1440,
	1476,
	1440,
	1440,
	1440,
	2458,
	2385,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	788,
	993,
	362,
	565,
	788,
	663,
	208,
	663,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2452,
	-1,
	2363,
	-1,
	2363,
	-1,
	-1,
	2126,
	-1,
	-1,
	2126,
	2126,
	2126,
	-1,
	-1,
	-1,
	-1,
	-1,
	2363,
	-1,
	2363,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1476,
	1440,
	-1,
	-1,
	1476,
	1241,
	-1,
	-1,
	-1,
	-1,
	1476,
	993,
	-1,
	-1,
	-1,
	-1,
	1476,
	663,
	1476,
	993,
	1476,
	663,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1476,
	1241,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	619,
	945,
	1476,
	2458,
	1476,
	1476,
	-1,
	1241,
	1428,
	1459,
	1459,
	1459,
	1440,
	997,
	736,
	997,
	736,
	997,
	1101,
	786,
	786,
	1440,
	1440,
	1440,
	945,
	1476,
	736,
	1241,
	1231,
	945,
	1231,
	736,
	1241,
	1476,
	1101,
	1231,
	1476,
	1459,
	1476,
	1440,
	1476,
	1440,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	789,
	488,
	1440,
	1440,
	1428,
	1440,
	1440,
	1440,
	1440,
	1456,
	1428,
	1001,
	581,
	620,
	1000,
	151,
	789,
	488,
	1440,
	1440,
	1457,
	1428,
	1440,
	1440,
	1440,
	1440,
	1440,
	1001,
	581,
	1428,
	1440,
	119,
	620,
	789,
	488,
	1440,
	1440,
	1428,
	1440,
	1440,
	1440,
	1440,
	1428,
	1459,
	1459,
	1001,
	581,
	620,
	1001,
	1001,
	1440,
	1001,
	119,
	91,
	-1,
	-1,
	-1,
	-1,
	2458,
	2458,
	2166,
	-1,
	-1,
	-1,
	-1,
	1916,
	1876,
	1980,
	1675,
	2325,
};
static const Il2CppTokenRangePair s_rgctxIndices[141] = 
{
	{ 0x02000002, { 0, 11 } },
	{ 0x02000009, { 11, 10 } },
	{ 0x0200000A, { 21, 3 } },
	{ 0x0200000B, { 24, 19 } },
	{ 0x0200000C, { 43, 3 } },
	{ 0x0200000D, { 46, 25 } },
	{ 0x02000031, { 92, 8 } },
	{ 0x02000033, { 100, 24 } },
	{ 0x02000035, { 124, 1 } },
	{ 0x02000038, { 125, 10 } },
	{ 0x02000039, { 135, 3 } },
	{ 0x0200003A, { 138, 28 } },
	{ 0x0200003B, { 166, 27 } },
	{ 0x0200003C, { 193, 20 } },
	{ 0x0200003D, { 213, 4 } },
	{ 0x0200003F, { 217, 3 } },
	{ 0x02000041, { 220, 3 } },
	{ 0x02000042, { 223, 4 } },
	{ 0x02000044, { 227, 16 } },
	{ 0x02000045, { 243, 15 } },
	{ 0x02000048, { 258, 13 } },
	{ 0x02000049, { 271, 12 } },
	{ 0x0200004A, { 283, 9 } },
	{ 0x0200004B, { 292, 27 } },
	{ 0x0200004D, { 319, 3 } },
	{ 0x0200004E, { 322, 3 } },
	{ 0x0200004F, { 325, 10 } },
	{ 0x02000050, { 335, 5 } },
	{ 0x02000051, { 340, 6 } },
	{ 0x02000052, { 346, 3 } },
	{ 0x02000053, { 349, 19 } },
	{ 0x02000054, { 368, 2 } },
	{ 0x02000074, { 370, 12 } },
	{ 0x02000077, { 382, 4 } },
	{ 0x0200007F, { 388, 3 } },
	{ 0x0200008A, { 391, 5 } },
	{ 0x0200008D, { 396, 4 } },
	{ 0x0200009E, { 400, 4 } },
	{ 0x020000A4, { 404, 12 } },
	{ 0x020000B0, { 418, 1 } },
	{ 0x020000B1, { 419, 1 } },
	{ 0x020000B2, { 420, 12 } },
	{ 0x020000C5, { 456, 4 } },
	{ 0x020000C6, { 460, 4 } },
	{ 0x020000C7, { 464, 4 } },
	{ 0x020000C8, { 468, 5 } },
	{ 0x020000CA, { 481, 9 } },
	{ 0x020000D3, { 534, 6 } },
	{ 0x020000D9, { 540, 4 } },
	{ 0x020000DC, { 544, 8 } },
	{ 0x020000DD, { 552, 48 } },
	{ 0x020000E8, { 698, 1 } },
	{ 0x020000EA, { 699, 1 } },
	{ 0x020000EC, { 700, 2 } },
	{ 0x020000ED, { 702, 1 } },
	{ 0x020000EF, { 703, 3 } },
	{ 0x020000F0, { 706, 1 } },
	{ 0x020000F4, { 707, 1 } },
	{ 0x020000F5, { 708, 1 } },
	{ 0x020000F6, { 709, 3 } },
	{ 0x020000F7, { 712, 2 } },
	{ 0x020000F8, { 714, 1 } },
	{ 0x020000FA, { 715, 2 } },
	{ 0x020000FB, { 717, 1 } },
	{ 0x020000FC, { 718, 2 } },
	{ 0x020000FD, { 720, 1 } },
	{ 0x020000FE, { 721, 2 } },
	{ 0x02000106, { 723, 7 } },
	{ 0x02000107, { 730, 14 } },
	{ 0x0200010B, { 744, 4 } },
	{ 0x060000C1, { 71, 5 } },
	{ 0x0600010C, { 76, 1 } },
	{ 0x0600018F, { 77, 4 } },
	{ 0x060001C1, { 81, 3 } },
	{ 0x0600020C, { 84, 5 } },
	{ 0x06000240, { 89, 3 } },
	{ 0x0600039A, { 386, 2 } },
	{ 0x0600044E, { 416, 2 } },
	{ 0x060004C2, { 432, 2 } },
	{ 0x060004C3, { 434, 2 } },
	{ 0x060004C6, { 436, 1 } },
	{ 0x060004C7, { 437, 1 } },
	{ 0x060004CA, { 438, 1 } },
	{ 0x060004CB, { 439, 1 } },
	{ 0x060004CE, { 440, 2 } },
	{ 0x060004CF, { 442, 2 } },
	{ 0x060004D2, { 444, 1 } },
	{ 0x060004D3, { 445, 1 } },
	{ 0x060004D6, { 446, 1 } },
	{ 0x060004D7, { 447, 1 } },
	{ 0x060004FB, { 448, 2 } },
	{ 0x060004FC, { 450, 2 } },
	{ 0x060004FD, { 452, 2 } },
	{ 0x060004FE, { 454, 2 } },
	{ 0x06000513, { 473, 3 } },
	{ 0x06000514, { 476, 3 } },
	{ 0x06000515, { 479, 2 } },
	{ 0x06000520, { 490, 1 } },
	{ 0x06000521, { 491, 1 } },
	{ 0x06000522, { 492, 2 } },
	{ 0x06000523, { 494, 1 } },
	{ 0x06000524, { 495, 1 } },
	{ 0x06000525, { 496, 3 } },
	{ 0x06000540, { 499, 4 } },
	{ 0x06000542, { 503, 13 } },
	{ 0x06000543, { 516, 1 } },
	{ 0x06000544, { 517, 1 } },
	{ 0x06000545, { 518, 1 } },
	{ 0x06000546, { 519, 2 } },
	{ 0x06000549, { 521, 2 } },
	{ 0x06000555, { 523, 2 } },
	{ 0x0600055F, { 525, 2 } },
	{ 0x06000560, { 527, 1 } },
	{ 0x06000561, { 528, 1 } },
	{ 0x06000562, { 529, 2 } },
	{ 0x06000563, { 531, 1 } },
	{ 0x06000564, { 532, 1 } },
	{ 0x06000565, { 533, 1 } },
	{ 0x060005DA, { 600, 9 } },
	{ 0x060005DC, { 609, 5 } },
	{ 0x060005DE, { 614, 5 } },
	{ 0x060005DF, { 619, 5 } },
	{ 0x060005E1, { 624, 5 } },
	{ 0x060005E2, { 629, 5 } },
	{ 0x060005E6, { 634, 5 } },
	{ 0x060005E7, { 639, 5 } },
	{ 0x060005E8, { 644, 5 } },
	{ 0x060005E9, { 649, 5 } },
	{ 0x060005EA, { 654, 2 } },
	{ 0x060005EC, { 656, 6 } },
	{ 0x060005EE, { 662, 7 } },
	{ 0x060005EF, { 669, 6 } },
	{ 0x060005F0, { 675, 7 } },
	{ 0x060005F1, { 682, 3 } },
	{ 0x060005F2, { 685, 3 } },
	{ 0x060005F3, { 688, 5 } },
	{ 0x060005F4, { 693, 5 } },
	{ 0x060006D0, { 748, 1 } },
	{ 0x060006D1, { 749, 2 } },
	{ 0x060006D2, { 751, 1 } },
	{ 0x060006D3, { 752, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[753] = 
{
	{ (Il2CppRGCTXDataType)2, 952 },
	{ (Il2CppRGCTXDataType)3, 4137 },
	{ (Il2CppRGCTXDataType)2, 1475 },
	{ (Il2CppRGCTXDataType)3, 4135 },
	{ (Il2CppRGCTXDataType)3, 4172 },
	{ (Il2CppRGCTXDataType)2, 1488 },
	{ (Il2CppRGCTXDataType)3, 4170 },
	{ (Il2CppRGCTXDataType)3, 4136 },
	{ (Il2CppRGCTXDataType)3, 4171 },
	{ (Il2CppRGCTXDataType)2, 374 },
	{ (Il2CppRGCTXDataType)2, 765 },
	{ (Il2CppRGCTXDataType)1, 467 },
	{ (Il2CppRGCTXDataType)2, 2370 },
	{ (Il2CppRGCTXDataType)2, 2779 },
	{ (Il2CppRGCTXDataType)2, 1132 },
	{ (Il2CppRGCTXDataType)2, 887 },
	{ (Il2CppRGCTXDataType)3, 2 },
	{ (Il2CppRGCTXDataType)3, 3 },
	{ (Il2CppRGCTXDataType)2, 467 },
	{ (Il2CppRGCTXDataType)3, 1437 },
	{ (Il2CppRGCTXDataType)3, 1435 },
	{ (Il2CppRGCTXDataType)2, 891 },
	{ (Il2CppRGCTXDataType)3, 6 },
	{ (Il2CppRGCTXDataType)2, 891 },
	{ (Il2CppRGCTXDataType)3, 11378 },
	{ (Il2CppRGCTXDataType)2, 2878 },
	{ (Il2CppRGCTXDataType)2, 1134 },
	{ (Il2CppRGCTXDataType)1, 481 },
	{ (Il2CppRGCTXDataType)1, 2882 },
	{ (Il2CppRGCTXDataType)3, 1449 },
	{ (Il2CppRGCTXDataType)2, 2882 },
	{ (Il2CppRGCTXDataType)3, 8589 },
	{ (Il2CppRGCTXDataType)2, 481 },
	{ (Il2CppRGCTXDataType)3, 9639 },
	{ (Il2CppRGCTXDataType)3, 1451 },
	{ (Il2CppRGCTXDataType)3, 8850 },
	{ (Il2CppRGCTXDataType)3, 8590 },
	{ (Il2CppRGCTXDataType)3, 8592 },
	{ (Il2CppRGCTXDataType)3, 8591 },
	{ (Il2CppRGCTXDataType)3, 3625 },
	{ (Il2CppRGCTXDataType)3, 8851 },
	{ (Il2CppRGCTXDataType)3, 3624 },
	{ (Il2CppRGCTXDataType)2, 1451 },
	{ (Il2CppRGCTXDataType)3, 8760 },
	{ (Il2CppRGCTXDataType)2, 2903 },
	{ (Il2CppRGCTXDataType)2, 1136 },
	{ (Il2CppRGCTXDataType)3, 11379 },
	{ (Il2CppRGCTXDataType)2, 2985 },
	{ (Il2CppRGCTXDataType)2, 1139 },
	{ (Il2CppRGCTXDataType)2, 2617 },
	{ (Il2CppRGCTXDataType)3, 6401 },
	{ (Il2CppRGCTXDataType)1, 499 },
	{ (Il2CppRGCTXDataType)1, 2991 },
	{ (Il2CppRGCTXDataType)3, 1484 },
	{ (Il2CppRGCTXDataType)2, 2991 },
	{ (Il2CppRGCTXDataType)3, 9026 },
	{ (Il2CppRGCTXDataType)2, 499 },
	{ (Il2CppRGCTXDataType)3, 9640 },
	{ (Il2CppRGCTXDataType)3, 1486 },
	{ (Il2CppRGCTXDataType)3, 8852 },
	{ (Il2CppRGCTXDataType)3, 9028 },
	{ (Il2CppRGCTXDataType)3, 9029 },
	{ (Il2CppRGCTXDataType)3, 6403 },
	{ (Il2CppRGCTXDataType)3, 9027 },
	{ (Il2CppRGCTXDataType)3, 3627 },
	{ (Il2CppRGCTXDataType)3, 6402 },
	{ (Il2CppRGCTXDataType)3, 3626 },
	{ (Il2CppRGCTXDataType)2, 1452 },
	{ (Il2CppRGCTXDataType)3, 6404 },
	{ (Il2CppRGCTXDataType)3, 6405 },
	{ (Il2CppRGCTXDataType)3, 8853 },
	{ (Il2CppRGCTXDataType)1, 79 },
	{ (Il2CppRGCTXDataType)2, 3461 },
	{ (Il2CppRGCTXDataType)2, 3461 },
	{ (Il2CppRGCTXDataType)2, 1729 },
	{ (Il2CppRGCTXDataType)3, 4636 },
	{ (Il2CppRGCTXDataType)1, 80 },
	{ (Il2CppRGCTXDataType)1, 166 },
	{ (Il2CppRGCTXDataType)2, 1546 },
	{ (Il2CppRGCTXDataType)2, 3474 },
	{ (Il2CppRGCTXDataType)3, 4461 },
	{ (Il2CppRGCTXDataType)1, 167 },
	{ (Il2CppRGCTXDataType)2, 1071 },
	{ (Il2CppRGCTXDataType)3, 851 },
	{ (Il2CppRGCTXDataType)1, 259 },
	{ (Il2CppRGCTXDataType)2, 3483 },
	{ (Il2CppRGCTXDataType)2, 3483 },
	{ (Il2CppRGCTXDataType)2, 1558 },
	{ (Il2CppRGCTXDataType)3, 4465 },
	{ (Il2CppRGCTXDataType)1, 260 },
	{ (Il2CppRGCTXDataType)2, 1072 },
	{ (Il2CppRGCTXDataType)3, 852 },
	{ (Il2CppRGCTXDataType)2, 3499 },
	{ (Il2CppRGCTXDataType)3, 1578 },
	{ (Il2CppRGCTXDataType)2, 1106 },
	{ (Il2CppRGCTXDataType)2, 1151 },
	{ (Il2CppRGCTXDataType)3, 8820 },
	{ (Il2CppRGCTXDataType)3, 8821 },
	{ (Il2CppRGCTXDataType)3, 1576 },
	{ (Il2CppRGCTXDataType)3, 11366 },
	{ (Il2CppRGCTXDataType)1, 385 },
	{ (Il2CppRGCTXDataType)2, 1126 },
	{ (Il2CppRGCTXDataType)2, 2623 },
	{ (Il2CppRGCTXDataType)3, 6463 },
	{ (Il2CppRGCTXDataType)3, 1395 },
	{ (Il2CppRGCTXDataType)3, 1392 },
	{ (Il2CppRGCTXDataType)3, 6464 },
	{ (Il2CppRGCTXDataType)3, 6466 },
	{ (Il2CppRGCTXDataType)3, 6465 },
	{ (Il2CppRGCTXDataType)2, 904 },
	{ (Il2CppRGCTXDataType)3, 62 },
	{ (Il2CppRGCTXDataType)3, 63 },
	{ (Il2CppRGCTXDataType)2, 2912 },
	{ (Il2CppRGCTXDataType)3, 8798 },
	{ (Il2CppRGCTXDataType)3, 64 },
	{ (Il2CppRGCTXDataType)2, 385 },
	{ (Il2CppRGCTXDataType)3, 1399 },
	{ (Il2CppRGCTXDataType)3, 1393 },
	{ (Il2CppRGCTXDataType)3, 1396 },
	{ (Il2CppRGCTXDataType)3, 1397 },
	{ (Il2CppRGCTXDataType)3, 1398 },
	{ (Il2CppRGCTXDataType)3, 1394 },
	{ (Il2CppRGCTXDataType)3, 8799 },
	{ (Il2CppRGCTXDataType)3, 1400 },
	{ (Il2CppRGCTXDataType)2, 535 },
	{ (Il2CppRGCTXDataType)2, 1225 },
	{ (Il2CppRGCTXDataType)2, 1127 },
	{ (Il2CppRGCTXDataType)1, 403 },
	{ (Il2CppRGCTXDataType)2, 403 },
	{ (Il2CppRGCTXDataType)2, 886 },
	{ (Il2CppRGCTXDataType)3, 0 },
	{ (Il2CppRGCTXDataType)3, 1 },
	{ (Il2CppRGCTXDataType)3, 1404 },
	{ (Il2CppRGCTXDataType)3, 1403 },
	{ (Il2CppRGCTXDataType)3, 1401 },
	{ (Il2CppRGCTXDataType)2, 889 },
	{ (Il2CppRGCTXDataType)3, 5 },
	{ (Il2CppRGCTXDataType)2, 889 },
	{ (Il2CppRGCTXDataType)1, 773 },
	{ (Il2CppRGCTXDataType)2, 1226 },
	{ (Il2CppRGCTXDataType)2, 1128 },
	{ (Il2CppRGCTXDataType)3, 11383 },
	{ (Il2CppRGCTXDataType)3, 11373 },
	{ (Il2CppRGCTXDataType)3, 11380 },
	{ (Il2CppRGCTXDataType)1, 404 },
	{ (Il2CppRGCTXDataType)1, 2281 },
	{ (Il2CppRGCTXDataType)3, 1409 },
	{ (Il2CppRGCTXDataType)3, 8862 },
	{ (Il2CppRGCTXDataType)2, 404 },
	{ (Il2CppRGCTXDataType)3, 9636 },
	{ (Il2CppRGCTXDataType)3, 1411 },
	{ (Il2CppRGCTXDataType)3, 8842 },
	{ (Il2CppRGCTXDataType)3, 8856 },
	{ (Il2CppRGCTXDataType)2, 773 },
	{ (Il2CppRGCTXDataType)3, 2267 },
	{ (Il2CppRGCTXDataType)3, 2265 },
	{ (Il2CppRGCTXDataType)3, 8863 },
	{ (Il2CppRGCTXDataType)3, 2266 },
	{ (Il2CppRGCTXDataType)3, 2264 },
	{ (Il2CppRGCTXDataType)3, 3943 },
	{ (Il2CppRGCTXDataType)3, 6177 },
	{ (Il2CppRGCTXDataType)3, 8843 },
	{ (Il2CppRGCTXDataType)3, 6178 },
	{ (Il2CppRGCTXDataType)3, 8857 },
	{ (Il2CppRGCTXDataType)3, 3942 },
	{ (Il2CppRGCTXDataType)2, 1466 },
	{ (Il2CppRGCTXDataType)1, 405 },
	{ (Il2CppRGCTXDataType)2, 1229 },
	{ (Il2CppRGCTXDataType)2, 1145 },
	{ (Il2CppRGCTXDataType)3, 11382 },
	{ (Il2CppRGCTXDataType)3, 11367 },
	{ (Il2CppRGCTXDataType)3, 11374 },
	{ (Il2CppRGCTXDataType)3, 1508 },
	{ (Il2CppRGCTXDataType)3, 8860 },
	{ (Il2CppRGCTXDataType)2, 1248 },
	{ (Il2CppRGCTXDataType)3, 2143 },
	{ (Il2CppRGCTXDataType)3, 2142 },
	{ (Il2CppRGCTXDataType)3, 1510 },
	{ (Il2CppRGCTXDataType)3, 8825 },
	{ (Il2CppRGCTXDataType)3, 8844 },
	{ (Il2CppRGCTXDataType)2, 405 },
	{ (Il2CppRGCTXDataType)3, 2147 },
	{ (Il2CppRGCTXDataType)3, 2145 },
	{ (Il2CppRGCTXDataType)3, 8861 },
	{ (Il2CppRGCTXDataType)3, 2146 },
	{ (Il2CppRGCTXDataType)3, 2144 },
	{ (Il2CppRGCTXDataType)3, 3920 },
	{ (Il2CppRGCTXDataType)3, 6150 },
	{ (Il2CppRGCTXDataType)3, 8826 },
	{ (Il2CppRGCTXDataType)3, 6151 },
	{ (Il2CppRGCTXDataType)3, 8845 },
	{ (Il2CppRGCTXDataType)3, 3919 },
	{ (Il2CppRGCTXDataType)2, 1454 },
	{ (Il2CppRGCTXDataType)3, 11368 },
	{ (Il2CppRGCTXDataType)2, 1392 },
	{ (Il2CppRGCTXDataType)2, 1146 },
	{ (Il2CppRGCTXDataType)3, 11381 },
	{ (Il2CppRGCTXDataType)3, 1515 },
	{ (Il2CppRGCTXDataType)3, 2165 },
	{ (Il2CppRGCTXDataType)3, 2164 },
	{ (Il2CppRGCTXDataType)3, 3923 },
	{ (Il2CppRGCTXDataType)3, 6155 },
	{ (Il2CppRGCTXDataType)3, 8828 },
	{ (Il2CppRGCTXDataType)3, 6156 },
	{ (Il2CppRGCTXDataType)3, 8859 },
	{ (Il2CppRGCTXDataType)3, 3922 },
	{ (Il2CppRGCTXDataType)2, 1456 },
	{ (Il2CppRGCTXDataType)2, 1394 },
	{ (Il2CppRGCTXDataType)3, 2854 },
	{ (Il2CppRGCTXDataType)3, 1517 },
	{ (Il2CppRGCTXDataType)3, 8827 },
	{ (Il2CppRGCTXDataType)3, 8858 },
	{ (Il2CppRGCTXDataType)3, 2163 },
	{ (Il2CppRGCTXDataType)3, 2912 },
	{ (Il2CppRGCTXDataType)3, 2913 },
	{ (Il2CppRGCTXDataType)3, 1416 },
	{ (Il2CppRGCTXDataType)2, 1129 },
	{ (Il2CppRGCTXDataType)3, 2914 },
	{ (Il2CppRGCTXDataType)2, 1409 },
	{ (Il2CppRGCTXDataType)2, 1130 },
	{ (Il2CppRGCTXDataType)3, 2917 },
	{ (Il2CppRGCTXDataType)2, 1410 },
	{ (Il2CppRGCTXDataType)2, 1141 },
	{ (Il2CppRGCTXDataType)3, 744 },
	{ (Il2CppRGCTXDataType)2, 1018 },
	{ (Il2CppRGCTXDataType)2, 1411 },
	{ (Il2CppRGCTXDataType)2, 1142 },
	{ (Il2CppRGCTXDataType)3, 11375 },
	{ (Il2CppRGCTXDataType)2, 1754 },
	{ (Il2CppRGCTXDataType)2, 1131 },
	{ (Il2CppRGCTXDataType)1, 422 },
	{ (Il2CppRGCTXDataType)1, 782 },
	{ (Il2CppRGCTXDataType)3, 1429 },
	{ (Il2CppRGCTXDataType)3, 9637 },
	{ (Il2CppRGCTXDataType)3, 8846 },
	{ (Il2CppRGCTXDataType)2, 422 },
	{ (Il2CppRGCTXDataType)2, 1823 },
	{ (Il2CppRGCTXDataType)3, 5052 },
	{ (Il2CppRGCTXDataType)3, 5053 },
	{ (Il2CppRGCTXDataType)2, 2095 },
	{ (Il2CppRGCTXDataType)3, 5079 },
	{ (Il2CppRGCTXDataType)2, 2229 },
	{ (Il2CppRGCTXDataType)3, 8847 },
	{ (Il2CppRGCTXDataType)3, 11369 },
	{ (Il2CppRGCTXDataType)2, 1761 },
	{ (Il2CppRGCTXDataType)2, 1147 },
	{ (Il2CppRGCTXDataType)3, 1522 },
	{ (Il2CppRGCTXDataType)2, 1768 },
	{ (Il2CppRGCTXDataType)3, 4897 },
	{ (Il2CppRGCTXDataType)3, 1524 },
	{ (Il2CppRGCTXDataType)3, 8832 },
	{ (Il2CppRGCTXDataType)3, 4898 },
	{ (Il2CppRGCTXDataType)3, 4900 },
	{ (Il2CppRGCTXDataType)3, 4899 },
	{ (Il2CppRGCTXDataType)3, 3594 },
	{ (Il2CppRGCTXDataType)3, 8833 },
	{ (Il2CppRGCTXDataType)3, 3593 },
	{ (Il2CppRGCTXDataType)2, 1438 },
	{ (Il2CppRGCTXDataType)2, 2520 },
	{ (Il2CppRGCTXDataType)2, 1148 },
	{ (Il2CppRGCTXDataType)3, 6165 },
	{ (Il2CppRGCTXDataType)3, 8835 },
	{ (Il2CppRGCTXDataType)3, 6166 },
	{ (Il2CppRGCTXDataType)3, 8849 },
	{ (Il2CppRGCTXDataType)3, 8834 },
	{ (Il2CppRGCTXDataType)3, 8848 },
	{ (Il2CppRGCTXDataType)2, 2528 },
	{ (Il2CppRGCTXDataType)3, 6164 },
	{ (Il2CppRGCTXDataType)3, 1529 },
	{ (Il2CppRGCTXDataType)3, 11370 },
	{ (Il2CppRGCTXDataType)3, 11376 },
	{ (Il2CppRGCTXDataType)3, 11371 },
	{ (Il2CppRGCTXDataType)2, 2554 },
	{ (Il2CppRGCTXDataType)2, 1149 },
	{ (Il2CppRGCTXDataType)3, 1549 },
	{ (Il2CppRGCTXDataType)2, 2601 },
	{ (Il2CppRGCTXDataType)3, 6368 },
	{ (Il2CppRGCTXDataType)3, 1551 },
	{ (Il2CppRGCTXDataType)3, 8836 },
	{ (Il2CppRGCTXDataType)3, 6369 },
	{ (Il2CppRGCTXDataType)3, 6370 },
	{ (Il2CppRGCTXDataType)3, 6371 },
	{ (Il2CppRGCTXDataType)3, 8837 },
	{ (Il2CppRGCTXDataType)1, 468 },
	{ (Il2CppRGCTXDataType)3, 7821 },
	{ (Il2CppRGCTXDataType)2, 2781 },
	{ (Il2CppRGCTXDataType)2, 468 },
	{ (Il2CppRGCTXDataType)3, 7823 },
	{ (Il2CppRGCTXDataType)3, 7822 },
	{ (Il2CppRGCTXDataType)3, 7825 },
	{ (Il2CppRGCTXDataType)3, 7824 },
	{ (Il2CppRGCTXDataType)3, 7820 },
	{ (Il2CppRGCTXDataType)3, 11377 },
	{ (Il2CppRGCTXDataType)2, 2804 },
	{ (Il2CppRGCTXDataType)2, 1133 },
	{ (Il2CppRGCTXDataType)1, 469 },
	{ (Il2CppRGCTXDataType)1, 794 },
	{ (Il2CppRGCTXDataType)2, 940 },
	{ (Il2CppRGCTXDataType)3, 217 },
	{ (Il2CppRGCTXDataType)2, 945 },
	{ (Il2CppRGCTXDataType)3, 237 },
	{ (Il2CppRGCTXDataType)2, 469 },
	{ (Il2CppRGCTXDataType)3, 1444 },
	{ (Il2CppRGCTXDataType)3, 238 },
	{ (Il2CppRGCTXDataType)2, 1576 },
	{ (Il2CppRGCTXDataType)3, 4476 },
	{ (Il2CppRGCTXDataType)3, 8024 },
	{ (Il2CppRGCTXDataType)2, 946 },
	{ (Il2CppRGCTXDataType)3, 241 },
	{ (Il2CppRGCTXDataType)3, 242 },
	{ (Il2CppRGCTXDataType)2, 1033 },
	{ (Il2CppRGCTXDataType)3, 755 },
	{ (Il2CppRGCTXDataType)3, 8022 },
	{ (Il2CppRGCTXDataType)3, 8025 },
	{ (Il2CppRGCTXDataType)3, 4477 },
	{ (Il2CppRGCTXDataType)2, 794 },
	{ (Il2CppRGCTXDataType)3, 8023 },
	{ (Il2CppRGCTXDataType)3, 756 },
	{ (Il2CppRGCTXDataType)3, 1442 },
	{ (Il2CppRGCTXDataType)2, 2805 },
	{ (Il2CppRGCTXDataType)2, 1143 },
	{ (Il2CppRGCTXDataType)3, 8854 },
	{ (Il2CppRGCTXDataType)2, 2806 },
	{ (Il2CppRGCTXDataType)2, 1144 },
	{ (Il2CppRGCTXDataType)3, 8855 },
	{ (Il2CppRGCTXDataType)3, 11372 },
	{ (Il2CppRGCTXDataType)2, 2828 },
	{ (Il2CppRGCTXDataType)2, 1150 },
	{ (Il2CppRGCTXDataType)3, 1556 },
	{ (Il2CppRGCTXDataType)3, 8838 },
	{ (Il2CppRGCTXDataType)2, 2832 },
	{ (Il2CppRGCTXDataType)3, 8097 },
	{ (Il2CppRGCTXDataType)3, 8098 },
	{ (Il2CppRGCTXDataType)3, 8099 },
	{ (Il2CppRGCTXDataType)3, 8839 },
	{ (Il2CppRGCTXDataType)3, 11141 },
	{ (Il2CppRGCTXDataType)3, 7828 },
	{ (Il2CppRGCTXDataType)3, 11142 },
	{ (Il2CppRGCTXDataType)3, 7826 },
	{ (Il2CppRGCTXDataType)2, 2782 },
	{ (Il2CppRGCTXDataType)3, 1456 },
	{ (Il2CppRGCTXDataType)2, 1135 },
	{ (Il2CppRGCTXDataType)3, 8759 },
	{ (Il2CppRGCTXDataType)2, 486 },
	{ (Il2CppRGCTXDataType)1, 486 },
	{ (Il2CppRGCTXDataType)3, 8758 },
	{ (Il2CppRGCTXDataType)2, 490 },
	{ (Il2CppRGCTXDataType)3, 1469 },
	{ (Il2CppRGCTXDataType)2, 1137 },
	{ (Il2CppRGCTXDataType)2, 932 },
	{ (Il2CppRGCTXDataType)3, 185 },
	{ (Il2CppRGCTXDataType)1, 491 },
	{ (Il2CppRGCTXDataType)3, 186 },
	{ (Il2CppRGCTXDataType)2, 1751 },
	{ (Il2CppRGCTXDataType)3, 4731 },
	{ (Il2CppRGCTXDataType)2, 2911 },
	{ (Il2CppRGCTXDataType)2, 1138 },
	{ (Il2CppRGCTXDataType)2, 2904 },
	{ (Il2CppRGCTXDataType)3, 8763 },
	{ (Il2CppRGCTXDataType)3, 8789 },
	{ (Il2CppRGCTXDataType)3, 4732 },
	{ (Il2CppRGCTXDataType)3, 1478 },
	{ (Il2CppRGCTXDataType)3, 1479 },
	{ (Il2CppRGCTXDataType)3, 1477 },
	{ (Il2CppRGCTXDataType)2, 491 },
	{ (Il2CppRGCTXDataType)3, 8790 },
	{ (Il2CppRGCTXDataType)3, 1480 },
	{ (Il2CppRGCTXDataType)3, 1476 },
	{ (Il2CppRGCTXDataType)1, 700 },
	{ (Il2CppRGCTXDataType)2, 700 },
	{ (Il2CppRGCTXDataType)2, 1198 },
	{ (Il2CppRGCTXDataType)2, 2915 },
	{ (Il2CppRGCTXDataType)2, 398 },
	{ (Il2CppRGCTXDataType)3, 1912 },
	{ (Il2CppRGCTXDataType)2, 2363 },
	{ (Il2CppRGCTXDataType)3, 11083 },
	{ (Il2CppRGCTXDataType)3, 2401 },
	{ (Il2CppRGCTXDataType)3, 2400 },
	{ (Il2CppRGCTXDataType)3, 8822 },
	{ (Il2CppRGCTXDataType)1, 398 },
	{ (Il2CppRGCTXDataType)2, 1299 },
	{ (Il2CppRGCTXDataType)3, 2399 },
	{ (Il2CppRGCTXDataType)1, 414 },
	{ (Il2CppRGCTXDataType)2, 414 },
	{ (Il2CppRGCTXDataType)3, 8829 },
	{ (Il2CppRGCTXDataType)2, 2918 },
	{ (Il2CppRGCTXDataType)1, 273 },
	{ (Il2CppRGCTXDataType)2, 2913 },
	{ (Il2CppRGCTXDataType)3, 8840 },
	{ (Il2CppRGCTXDataType)2, 492 },
	{ (Il2CppRGCTXDataType)3, 8841 },
	{ (Il2CppRGCTXDataType)2, 395 },
	{ (Il2CppRGCTXDataType)2, 1170 },
	{ (Il2CppRGCTXDataType)2, 2780 },
	{ (Il2CppRGCTXDataType)3, 7813 },
	{ (Il2CppRGCTXDataType)1, 395 },
	{ (Il2CppRGCTXDataType)2, 384 },
	{ (Il2CppRGCTXDataType)3, 1369 },
	{ (Il2CppRGCTXDataType)3, 1370 },
	{ (Il2CppRGCTXDataType)3, 1371 },
	{ (Il2CppRGCTXDataType)3, 9641 },
	{ (Il2CppRGCTXDataType)3, 8764 },
	{ (Il2CppRGCTXDataType)2, 2906 },
	{ (Il2CppRGCTXDataType)2, 1140 },
	{ (Il2CppRGCTXDataType)2, 3504 },
	{ (Il2CppRGCTXDataType)2, 1153 },
	{ (Il2CppRGCTXDataType)3, 6409 },
	{ (Il2CppRGCTXDataType)3, 6410 },
	{ (Il2CppRGCTXDataType)3, 6408 },
	{ (Il2CppRGCTXDataType)3, 1620 },
	{ (Il2CppRGCTXDataType)2, 1153 },
	{ (Il2CppRGCTXDataType)3, 1618 },
	{ (Il2CppRGCTXDataType)3, 6407 },
	{ (Il2CppRGCTXDataType)3, 1619 },
	{ (Il2CppRGCTXDataType)2, 2619 },
	{ (Il2CppRGCTXDataType)3, 6406 },
	{ (Il2CppRGCTXDataType)1, 145 },
	{ (Il2CppRGCTXDataType)2, 2360 },
	{ (Il2CppRGCTXDataType)2, 559 },
	{ (Il2CppRGCTXDataType)2, 557 },
	{ (Il2CppRGCTXDataType)1, 558 },
	{ (Il2CppRGCTXDataType)2, 1887 },
	{ (Il2CppRGCTXDataType)1, 1852 },
	{ (Il2CppRGCTXDataType)3, 1709 },
	{ (Il2CppRGCTXDataType)2, 1177 },
	{ (Il2CppRGCTXDataType)2, 3067 },
	{ (Il2CppRGCTXDataType)3, 9376 },
	{ (Il2CppRGCTXDataType)2, 1536 },
	{ (Il2CppRGCTXDataType)3, 4429 },
	{ (Il2CppRGCTXDataType)2, 2518 },
	{ (Il2CppRGCTXDataType)3, 6139 },
	{ (Il2CppRGCTXDataType)2, 2518 },
	{ (Il2CppRGCTXDataType)3, 11362 },
	{ (Il2CppRGCTXDataType)3, 8816 },
	{ (Il2CppRGCTXDataType)3, 11363 },
	{ (Il2CppRGCTXDataType)3, 8817 },
	{ (Il2CppRGCTXDataType)3, 11352 },
	{ (Il2CppRGCTXDataType)3, 11354 },
	{ (Il2CppRGCTXDataType)3, 11357 },
	{ (Il2CppRGCTXDataType)3, 11360 },
	{ (Il2CppRGCTXDataType)3, 11364 },
	{ (Il2CppRGCTXDataType)3, 8818 },
	{ (Il2CppRGCTXDataType)3, 11365 },
	{ (Il2CppRGCTXDataType)3, 8819 },
	{ (Il2CppRGCTXDataType)3, 11342 },
	{ (Il2CppRGCTXDataType)3, 11344 },
	{ (Il2CppRGCTXDataType)3, 11346 },
	{ (Il2CppRGCTXDataType)3, 11349 },
	{ (Il2CppRGCTXDataType)2, 2556 },
	{ (Il2CppRGCTXDataType)3, 6282 },
	{ (Il2CppRGCTXDataType)2, 1231 },
	{ (Il2CppRGCTXDataType)3, 2100 },
	{ (Il2CppRGCTXDataType)2, 1239 },
	{ (Il2CppRGCTXDataType)3, 2129 },
	{ (Il2CppRGCTXDataType)2, 1793 },
	{ (Il2CppRGCTXDataType)3, 5040 },
	{ (Il2CppRGCTXDataType)3, 6391 },
	{ (Il2CppRGCTXDataType)3, 3609 },
	{ (Il2CppRGCTXDataType)3, 3608 },
	{ (Il2CppRGCTXDataType)3, 3607 },
	{ (Il2CppRGCTXDataType)3, 4918 },
	{ (Il2CppRGCTXDataType)3, 3606 },
	{ (Il2CppRGCTXDataType)3, 3605 },
	{ (Il2CppRGCTXDataType)3, 3604 },
	{ (Il2CppRGCTXDataType)3, 2258 },
	{ (Il2CppRGCTXDataType)3, 3932 },
	{ (Il2CppRGCTXDataType)3, 3931 },
	{ (Il2CppRGCTXDataType)3, 3930 },
	{ (Il2CppRGCTXDataType)3, 2259 },
	{ (Il2CppRGCTXDataType)3, 3935 },
	{ (Il2CppRGCTXDataType)3, 6175 },
	{ (Il2CppRGCTXDataType)3, 3934 },
	{ (Il2CppRGCTXDataType)3, 3933 },
	{ (Il2CppRGCTXDataType)2, 1926 },
	{ (Il2CppRGCTXDataType)2, 2133 },
	{ (Il2CppRGCTXDataType)3, 751 },
	{ (Il2CppRGCTXDataType)2, 1927 },
	{ (Il2CppRGCTXDataType)2, 2134 },
	{ (Il2CppRGCTXDataType)3, 778 },
	{ (Il2CppRGCTXDataType)2, 954 },
	{ (Il2CppRGCTXDataType)3, 276 },
	{ (Il2CppRGCTXDataType)3, 278 },
	{ (Il2CppRGCTXDataType)3, 279 },
	{ (Il2CppRGCTXDataType)2, 2019 },
	{ (Il2CppRGCTXDataType)2, 2174 },
	{ (Il2CppRGCTXDataType)3, 281 },
	{ (Il2CppRGCTXDataType)2, 631 },
	{ (Il2CppRGCTXDataType)2, 955 },
	{ (Il2CppRGCTXDataType)3, 277 },
	{ (Il2CppRGCTXDataType)3, 280 },
	{ (Il2CppRGCTXDataType)1, 217 },
	{ (Il2CppRGCTXDataType)3, 11237 },
	{ (Il2CppRGCTXDataType)3, 11229 },
	{ (Il2CppRGCTXDataType)3, 11019 },
	{ (Il2CppRGCTXDataType)3, 11223 },
	{ (Il2CppRGCTXDataType)3, 11228 },
	{ (Il2CppRGCTXDataType)1, 219 },
	{ (Il2CppRGCTXDataType)3, 10966 },
	{ (Il2CppRGCTXDataType)2, 3479 },
	{ (Il2CppRGCTXDataType)1, 369 },
	{ (Il2CppRGCTXDataType)1, 760 },
	{ (Il2CppRGCTXDataType)1, 1617 },
	{ (Il2CppRGCTXDataType)2, 1617 },
	{ (Il2CppRGCTXDataType)1, 288 },
	{ (Il2CppRGCTXDataType)2, 895 },
	{ (Il2CppRGCTXDataType)3, 28 },
	{ (Il2CppRGCTXDataType)3, 29 },
	{ (Il2CppRGCTXDataType)2, 1725 },
	{ (Il2CppRGCTXDataType)1, 1725 },
	{ (Il2CppRGCTXDataType)1, 2290 },
	{ (Il2CppRGCTXDataType)3, 30 },
	{ (Il2CppRGCTXDataType)3, 4634 },
	{ (Il2CppRGCTXDataType)3, 31 },
	{ (Il2CppRGCTXDataType)3, 4132 },
	{ (Il2CppRGCTXDataType)2, 1473 },
	{ (Il2CppRGCTXDataType)3, 4131 },
	{ (Il2CppRGCTXDataType)1, 278 },
	{ (Il2CppRGCTXDataType)1, 279 },
	{ (Il2CppRGCTXDataType)3, 11472 },
	{ (Il2CppRGCTXDataType)1, 280 },
	{ (Il2CppRGCTXDataType)2, 280 },
	{ (Il2CppRGCTXDataType)1, 289 },
	{ (Il2CppRGCTXDataType)2, 289 },
	{ (Il2CppRGCTXDataType)2, 986 },
	{ (Il2CppRGCTXDataType)3, 507 },
	{ (Il2CppRGCTXDataType)3, 11465 },
	{ (Il2CppRGCTXDataType)3, 11021 },
	{ (Il2CppRGCTXDataType)3, 11461 },
	{ (Il2CppRGCTXDataType)3, 11464 },
	{ (Il2CppRGCTXDataType)1, 287 },
	{ (Il2CppRGCTXDataType)3, 10968 },
	{ (Il2CppRGCTXDataType)1, 283 },
	{ (Il2CppRGCTXDataType)1, 284 },
	{ (Il2CppRGCTXDataType)1, 282 },
	{ (Il2CppRGCTXDataType)2, 897 },
	{ (Il2CppRGCTXDataType)3, 36 },
	{ (Il2CppRGCTXDataType)2, 897 },
	{ (Il2CppRGCTXDataType)1, 707 },
	{ (Il2CppRGCTXDataType)2, 707 },
	{ (Il2CppRGCTXDataType)2, 2295 },
	{ (Il2CppRGCTXDataType)2, 708 },
	{ (Il2CppRGCTXDataType)2, 987 },
	{ (Il2CppRGCTXDataType)3, 508 },
	{ (Il2CppRGCTXDataType)3, 509 },
	{ (Il2CppRGCTXDataType)2, 1156 },
	{ (Il2CppRGCTXDataType)3, 9634 },
	{ (Il2CppRGCTXDataType)2, 1156 },
	{ (Il2CppRGCTXDataType)3, 1647 },
	{ (Il2CppRGCTXDataType)2, 388 },
	{ (Il2CppRGCTXDataType)3, 1649 },
	{ (Il2CppRGCTXDataType)3, 1648 },
	{ (Il2CppRGCTXDataType)1, 388 },
	{ (Il2CppRGCTXDataType)3, 2166 },
	{ (Il2CppRGCTXDataType)2, 1250 },
	{ (Il2CppRGCTXDataType)3, 4177 },
	{ (Il2CppRGCTXDataType)2, 1491 },
	{ (Il2CppRGCTXDataType)3, 2167 },
	{ (Il2CppRGCTXDataType)3, 2194 },
	{ (Il2CppRGCTXDataType)2, 1279 },
	{ (Il2CppRGCTXDataType)3, 2268 },
	{ (Il2CppRGCTXDataType)3, 2168 },
	{ (Il2CppRGCTXDataType)3, 2272 },
	{ (Il2CppRGCTXDataType)3, 2195 },
	{ (Il2CppRGCTXDataType)3, 2197 },
	{ (Il2CppRGCTXDataType)3, 9380 },
	{ (Il2CppRGCTXDataType)3, 3925 },
	{ (Il2CppRGCTXDataType)3, 3924 },
	{ (Il2CppRGCTXDataType)2, 1457 },
	{ (Il2CppRGCTXDataType)3, 2269 },
	{ (Il2CppRGCTXDataType)3, 2271 },
	{ (Il2CppRGCTXDataType)3, 2855 },
	{ (Il2CppRGCTXDataType)3, 2857 },
	{ (Il2CppRGCTXDataType)3, 2273 },
	{ (Il2CppRGCTXDataType)3, 2270 },
	{ (Il2CppRGCTXDataType)3, 2173 },
	{ (Il2CppRGCTXDataType)2, 2595 },
	{ (Il2CppRGCTXDataType)3, 6344 },
	{ (Il2CppRGCTXDataType)2, 2615 },
	{ (Il2CppRGCTXDataType)3, 6395 },
	{ (Il2CppRGCTXDataType)3, 11130 },
	{ (Il2CppRGCTXDataType)3, 2102 },
	{ (Il2CppRGCTXDataType)3, 2104 },
	{ (Il2CppRGCTXDataType)3, 6158 },
	{ (Il2CppRGCTXDataType)3, 11131 },
	{ (Il2CppRGCTXDataType)3, 2108 },
	{ (Il2CppRGCTXDataType)3, 2110 },
	{ (Il2CppRGCTXDataType)3, 6180 },
	{ (Il2CppRGCTXDataType)3, 4571 },
	{ (Il2CppRGCTXDataType)3, 6157 },
	{ (Il2CppRGCTXDataType)3, 6345 },
	{ (Il2CppRGCTXDataType)3, 6179 },
	{ (Il2CppRGCTXDataType)3, 6396 },
	{ (Il2CppRGCTXDataType)3, 2109 },
	{ (Il2CppRGCTXDataType)2, 1237 },
	{ (Il2CppRGCTXDataType)3, 2103 },
	{ (Il2CppRGCTXDataType)2, 1233 },
	{ (Il2CppRGCTXDataType)3, 6347 },
	{ (Il2CppRGCTXDataType)3, 6397 },
	{ (Il2CppRGCTXDataType)3, 2856 },
	{ (Il2CppRGCTXDataType)3, 6346 },
	{ (Il2CppRGCTXDataType)2, 935 },
	{ (Il2CppRGCTXDataType)3, 197 },
	{ (Il2CppRGCTXDataType)2, 933 },
	{ (Il2CppRGCTXDataType)3, 189 },
	{ (Il2CppRGCTXDataType)2, 107 },
	{ (Il2CppRGCTXDataType)3, 190 },
	{ (Il2CppRGCTXDataType)2, 1542 },
	{ (Il2CppRGCTXDataType)3, 4457 },
	{ (Il2CppRGCTXDataType)3, 198 },
	{ (Il2CppRGCTXDataType)2, 938 },
	{ (Il2CppRGCTXDataType)3, 209 },
	{ (Il2CppRGCTXDataType)3, 210 },
	{ (Il2CppRGCTXDataType)2, 1021 },
	{ (Il2CppRGCTXDataType)3, 747 },
	{ (Il2CppRGCTXDataType)2, 943 },
	{ (Il2CppRGCTXDataType)3, 225 },
	{ (Il2CppRGCTXDataType)3, 226 },
	{ (Il2CppRGCTXDataType)2, 3072 },
	{ (Il2CppRGCTXDataType)3, 9505 },
	{ (Il2CppRGCTXDataType)2, 947 },
	{ (Il2CppRGCTXDataType)3, 245 },
	{ (Il2CppRGCTXDataType)3, 246 },
	{ (Il2CppRGCTXDataType)2, 3082 },
	{ (Il2CppRGCTXDataType)3, 9539 },
	{ (Il2CppRGCTXDataType)2, 950 },
	{ (Il2CppRGCTXDataType)3, 259 },
	{ (Il2CppRGCTXDataType)3, 260 },
	{ (Il2CppRGCTXDataType)2, 3078 },
	{ (Il2CppRGCTXDataType)3, 9527 },
	{ (Il2CppRGCTXDataType)2, 902 },
	{ (Il2CppRGCTXDataType)3, 54 },
	{ (Il2CppRGCTXDataType)3, 55 },
	{ (Il2CppRGCTXDataType)2, 3084 },
	{ (Il2CppRGCTXDataType)3, 9549 },
	{ (Il2CppRGCTXDataType)2, 908 },
	{ (Il2CppRGCTXDataType)3, 89 },
	{ (Il2CppRGCTXDataType)3, 90 },
	{ (Il2CppRGCTXDataType)2, 1023 },
	{ (Il2CppRGCTXDataType)3, 748 },
	{ (Il2CppRGCTXDataType)2, 911 },
	{ (Il2CppRGCTXDataType)3, 101 },
	{ (Il2CppRGCTXDataType)3, 102 },
	{ (Il2CppRGCTXDataType)2, 1543 },
	{ (Il2CppRGCTXDataType)3, 4458 },
	{ (Il2CppRGCTXDataType)2, 915 },
	{ (Il2CppRGCTXDataType)3, 117 },
	{ (Il2CppRGCTXDataType)3, 118 },
	{ (Il2CppRGCTXDataType)2, 3079 },
	{ (Il2CppRGCTXDataType)3, 9528 },
	{ (Il2CppRGCTXDataType)2, 918 },
	{ (Il2CppRGCTXDataType)3, 127 },
	{ (Il2CppRGCTXDataType)3, 128 },
	{ (Il2CppRGCTXDataType)2, 3073 },
	{ (Il2CppRGCTXDataType)3, 9506 },
	{ (Il2CppRGCTXDataType)1, 1612 },
	{ (Il2CppRGCTXDataType)2, 1612 },
	{ (Il2CppRGCTXDataType)2, 923 },
	{ (Il2CppRGCTXDataType)3, 149 },
	{ (Il2CppRGCTXDataType)1, 106 },
	{ (Il2CppRGCTXDataType)3, 150 },
	{ (Il2CppRGCTXDataType)2, 1066 },
	{ (Il2CppRGCTXDataType)3, 783 },
	{ (Il2CppRGCTXDataType)2, 925 },
	{ (Il2CppRGCTXDataType)3, 157 },
	{ (Il2CppRGCTXDataType)1, 351 },
	{ (Il2CppRGCTXDataType)1, 742 },
	{ (Il2CppRGCTXDataType)3, 158 },
	{ (Il2CppRGCTXDataType)2, 1747 },
	{ (Il2CppRGCTXDataType)3, 4657 },
	{ (Il2CppRGCTXDataType)2, 927 },
	{ (Il2CppRGCTXDataType)3, 165 },
	{ (Il2CppRGCTXDataType)1, 109 },
	{ (Il2CppRGCTXDataType)3, 166 },
	{ (Il2CppRGCTXDataType)2, 1697 },
	{ (Il2CppRGCTXDataType)3, 4600 },
	{ (Il2CppRGCTXDataType)2, 929 },
	{ (Il2CppRGCTXDataType)3, 173 },
	{ (Il2CppRGCTXDataType)1, 741 },
	{ (Il2CppRGCTXDataType)1, 350 },
	{ (Il2CppRGCTXDataType)3, 174 },
	{ (Il2CppRGCTXDataType)2, 1745 },
	{ (Il2CppRGCTXDataType)3, 4655 },
	{ (Il2CppRGCTXDataType)1, 104 },
	{ (Il2CppRGCTXDataType)1, 1022 },
	{ (Il2CppRGCTXDataType)2, 1022 },
	{ (Il2CppRGCTXDataType)1, 348 },
	{ (Il2CppRGCTXDataType)1, 1061 },
	{ (Il2CppRGCTXDataType)2, 1061 },
	{ (Il2CppRGCTXDataType)2, 931 },
	{ (Il2CppRGCTXDataType)3, 181 },
	{ (Il2CppRGCTXDataType)3, 182 },
	{ (Il2CppRGCTXDataType)2, 2497 },
	{ (Il2CppRGCTXDataType)3, 5324 },
	{ (Il2CppRGCTXDataType)2, 937 },
	{ (Il2CppRGCTXDataType)3, 205 },
	{ (Il2CppRGCTXDataType)3, 206 },
	{ (Il2CppRGCTXDataType)2, 2498 },
	{ (Il2CppRGCTXDataType)3, 5329 },
	{ (Il2CppRGCTXDataType)2, 576 },
	{ (Il2CppRGCTXDataType)2, 578 },
	{ (Il2CppRGCTXDataType)2, 579 },
	{ (Il2CppRGCTXDataType)2, 834 },
	{ (Il2CppRGCTXDataType)2, 580 },
	{ (Il2CppRGCTXDataType)1, 581 },
	{ (Il2CppRGCTXDataType)2, 581 },
	{ (Il2CppRGCTXDataType)2, 835 },
	{ (Il2CppRGCTXDataType)2, 564 },
	{ (Il2CppRGCTXDataType)2, 565 },
	{ (Il2CppRGCTXDataType)2, 566 },
	{ (Il2CppRGCTXDataType)1, 567 },
	{ (Il2CppRGCTXDataType)2, 567 },
	{ (Il2CppRGCTXDataType)2, 829 },
	{ (Il2CppRGCTXDataType)2, 568 },
	{ (Il2CppRGCTXDataType)2, 830 },
	{ (Il2CppRGCTXDataType)2, 569 },
	{ (Il2CppRGCTXDataType)2, 831 },
	{ (Il2CppRGCTXDataType)2, 570 },
	{ (Il2CppRGCTXDataType)2, 571 },
	{ (Il2CppRGCTXDataType)2, 572 },
	{ (Il2CppRGCTXDataType)2, 832 },
	{ (Il2CppRGCTXDataType)2, 573 },
	{ (Il2CppRGCTXDataType)2, 577 },
	{ (Il2CppRGCTXDataType)2, 833 },
	{ (Il2CppRGCTXDataType)2, 1816 },
	{ (Il2CppRGCTXDataType)3, 5110 },
	{ (Il2CppRGCTXDataType)2, 446 },
	{ (Il2CppRGCTXDataType)2, 2434 },
	{ (Il2CppRGCTXDataType)2, 1992 },
	{ (Il2CppRGCTXDataType)2, 3514 },
	{ (Il2CppRGCTXDataType)3, 5109 },
	{ (Il2CppRGCTXDataType)2, 448 },
	{ (Il2CppRGCTXDataType)2, 1824 },
	{ (Il2CppRGCTXDataType)3, 5056 },
	{ (Il2CppRGCTXDataType)3, 5175 },
	{ (Il2CppRGCTXDataType)2, 785 },
	{ (Il2CppRGCTXDataType)2, 2442 },
	{ (Il2CppRGCTXDataType)3, 5094 },
	{ (Il2CppRGCTXDataType)3, 5054 },
	{ (Il2CppRGCTXDataType)3, 5055 },
	{ (Il2CppRGCTXDataType)2, 2096 },
	{ (Il2CppRGCTXDataType)3, 5080 },
	{ (Il2CppRGCTXDataType)2, 3547 },
	{ (Il2CppRGCTXDataType)3, 5174 },
	{ (Il2CppRGCTXDataType)3, 5093 },
	{ (Il2CppRGCTXDataType)2, 484 },
	{ (Il2CppRGCTXDataType)2, 2896 },
	{ (Il2CppRGCTXDataType)3, 8748 },
	{ (Il2CppRGCTXDataType)2, 2896 },
	{ (Il2CppRGCTXDataType)3, 11496 },
	{ (Il2CppRGCTXDataType)1, 305 },
	{ (Il2CppRGCTXDataType)2, 3488 },
	{ (Il2CppRGCTXDataType)3, 11497 },
	{ (Il2CppRGCTXDataType)1, 303 },
};
extern const CustomAttributesCacheGenerator g_Sirenix_Serialization_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Sirenix_Serialization_CodeGenModule;
const Il2CppCodeGenModule g_Sirenix_Serialization_CodeGenModule = 
{
	"Sirenix.Serialization.dll",
	1752,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	141,
	s_rgctxIndices,
	753,
	s_rgctxValues,
	NULL,
	g_Sirenix_Serialization_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
