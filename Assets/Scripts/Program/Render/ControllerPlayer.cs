﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace Program.Render
{
    public class ControllerPlayer : MonoBehaviour
    {

        [Range(0, 300)]
        [SerializeField] private float speedRotate;

        public Vector3 Direct { get; private set; }

        public Transform DirectCamera
        {
            get
            {
                return cameraPos;
            }
        }



        [SerializeField] private Transform player;

        [SerializeField] private Transform panel;

        [SerializeField] private EventSystem eventSystem;

        [SerializeField] private float radius;

        [SerializeField] private Transform joistic;

        [SerializeField] private Transform cameraPos;

        private Vector3 rotate;

        private Vector3 startRotate;

        bool isRotate;

        bool isMove;

        private void Update()
        {

            bool yes = false;
            if (Input.touchCount > 0 && Input.touchCount < 3 && eventSystem.currentSelectedGameObject == null)
            {
                for (int i = 0; i < Input.touchCount; i++)
                {
                    Vector3 pos = panel.InverseTransformPoint(Input.GetTouch(i).position);
                    if (pos.magnitude < radius)
                    {
                        isMove = true;
                        yes = true;
                        Direct = pos;
                        joistic.transform.localPosition = Direct;
                        if (Input.touchCount == 1)
                        {
                            startRotate = Vector3.zero;
                        }
                    }
                    else if (!(isMove && Input.touchCount == 1))
                    {
                        if (startRotate == Vector3.zero)
                        {
                            startRotate = Input.GetTouch(i).position;
                        }

                        Vector2 direct = new Vector2(Input.GetTouch(i).deltaPosition.x, Input.GetTouch(i).deltaPosition.y);

                        startRotate = Input.GetTouch(i).position;

                        rotate.x = Mathf.Clamp(rotate.x - direct.y * speedRotate * Time.fixedDeltaTime, -90, 90);
                        rotate.y += direct.x * speedRotate * Time.fixedDeltaTime;
                    }
                    else
                    {
                        startRotate = Vector3.zero;
                    }
                }
            }
            else if (Input.touchCount == 0)
            {
                isMove = false;
            }
            if (isMove && !yes)
            {
                Vector3 pos = panel.InverseTransformPoint(Input.GetTouch(0).position);
                Direct = Vector3.ClampMagnitude(pos, radius);
                joistic.transform.localPosition = Direct;
            }
            if (!yes && !isMove)
            {
                Direct = Vector3.zero;
                joistic.transform.localPosition = Direct;
            }
        }

        private void LateUpdate()
        {
            cameraPos.localEulerAngles = rotate;
            cameraPos.position = player.position + new Vector3(0, 1.4f, 0);
        }
    }
}