﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace Program.Render
{
    public class BackEditor : MonoBehaviour
    {
        [SerializeField] private Button button;

        private void Start()
        {
            button.onClick.AddListener(delegate { Back(); });
        }

        private void Back()
        {
            SceneManager.LoadScene(1);
        }
    }
}