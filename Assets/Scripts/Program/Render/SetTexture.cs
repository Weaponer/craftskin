﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Program.Render
{
    public class SetTexture : MonoBehaviour
    {
        [SerializeField] private Renderer[] renderers;

        private void Start()
        {
            Texture2D texture2D = SaveTextureScene.GetTexture();
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material.mainTexture = texture2D;
                renderers[i].material.SetTexture("_EmissionMap", texture2D);
            }
        }
    }
}