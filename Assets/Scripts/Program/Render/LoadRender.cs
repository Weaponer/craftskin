﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Program.UI;
using Program.Editor;

namespace Program.Render
{
    public class LoadRender : MonoBehaviour
    {

        private void Start()
        {
            BodyPanel.StartRender.StartRenderEvent += Load;
        }

        private void Load()
        {
            SaveTextureScene.SaveTexutre(EditModel.Singleton.SkinModel.GetTexture());
            SceneManager.LoadScene(2);
        }
    }
}