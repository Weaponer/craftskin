﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Program.Render
{
    public class MovePlayer : MonoBehaviour
    {
        [SerializeField] private CharacterController characterController;

        [SerializeField] private ControllerPlayer controllerPlayer;

        [SerializeField] private Animator animator;

        [SerializeField] private float maxSpeed;

        [SerializeField] private float acceleration;

        private Vector3 speed;

        private void Update()
        {
            Vector3 direct = new Vector3(controllerPlayer.DirectCamera.forward.x, 0, controllerPlayer.DirectCamera.forward.z).normalized * controllerPlayer.Direct.y + new Vector3(controllerPlayer.DirectCamera.right.x, 0, controllerPlayer.DirectCamera.right.z).normalized * controllerPlayer.Direct.x;


            speed = direct.normalized * maxSpeed;

            if (speed.magnitude > 0.2f)
            {
                animator.transform.rotation = Quaternion.LookRotation(direct.normalized, Vector3.up);
                animator.SetBool("Move", true);
            }
            else
            {
                animator.SetBool("Move", false);
            }

            characterController.Move(speed * Time.deltaTime);
            animator.SetFloat("Speed", speed.magnitude / maxSpeed);

        }
    }
}