﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.Menu
{
    public class UIMenu : MonoBehaviour
    {
        [SerializeField] private Button loadProgram;
        [SerializeField] private Button exit;

        [SerializeField] private Transform contentNews;

        private void Start()
        {
            loadProgram.onClick.AddListener(delegate { BaseMenu.Singleton.LoadEditor(); });
            exit.onClick.AddListener(delegate { BaseMenu.Singleton.ExitProgram(); });
        }

        public Transform GetContentNews()
        {
            return contentNews;
        }
    }
}