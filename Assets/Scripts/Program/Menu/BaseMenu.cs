﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Program.Menu
{
    public class BaseMenu : MonoBehaviour
    {
        public static BaseMenu Singleton { get; private set; }

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                Singleton = this;
            }
        }

        public void LoadEditor()
        {
            SceneManager.LoadScene(1);
        }

        public void ExitProgram()
        {
            Application.Quit();
        }

        private void OnDestroy()
        {
            if (Singleton == this)
                Singleton = null;
        }
    }
}