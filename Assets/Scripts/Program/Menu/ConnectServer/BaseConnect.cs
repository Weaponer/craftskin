﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Threading;
using System.Security.Permissions;


namespace FilerServer.Base
{
    public class BaseConnect
    {
        public readonly static string IP = "194.58.119.53";
        public const int Port = 4246;

        public const int GetCountNews = 5;
        public const int LoadNews = 1;

        public const int LoadTexture = 6;
        public const int GetCountTexture = 10;
        public const int LoadTextureName = 13;

        public const int GetEndUpdateNews = 11;
        public const int GetEndUpdateTexture = 12;


        public static Socket Socket;

        private static EndPoint point;

        private static BaseProcces procces;

        public static void LoadListNews(EndLoadNews endNormal, Action end)
        {
            LoadListNews loadListNews = new LoadListNews();
            loadListNews.Init(delegate
            {

            }, delegate
            {
                if (loadListNews.IsEndWork)
                {
                    endNormal(loadListNews.NewsArray.ToArray());
                }
                else
                {
                    end();
                }
            });
            CreateProcces(loadListNews);
        }

        public static void LoadListNameTexture(EndLoadTexture endNormal, Action end)
        {
            LoadListTextureName loadList = new LoadListTextureName();
            loadList.Init(delegate
            {

            }, delegate
            {
                if (loadList.IsEndWork)
                {
                    endNormal(loadList.TextureArray.ToArray());
                }
                else
                {
                    end();
                }
            });
            CreateProcces(loadList);
        }

        public static void LoadListTexture(EndLoadTexture endNormal, Action end)
        {
            LoadListTexture loadList = new LoadListTexture();
            loadList.Init(delegate
            {

            }, delegate
            {
                if (loadList.IsEndWork)
                {
                    
                    endNormal(loadList.TextureArray.ToArray());
                }
                else
                {
                    end();
                }
            });
            CreateProcces(loadList);
        }

        public static void LoadNumUpdateNews(LoadNum endNormal, Action end)
        {
            LoadEndUpdateNews loadEndUpdate = new LoadEndUpdateNews();
            loadEndUpdate.Init(delegate
            {

            }, delegate
            {
                if (loadEndUpdate.IsEndWork)
                {
                    endNormal(loadEndUpdate.Num);
                }
                else
                {
                    end();
                }
            });
            CreateProcces(loadEndUpdate);
        }

        public static void LoadNumUpdateTexture(LoadNum endNormal, Action end)
        {
            LoadEndUpdateTexture loadEndUpdate = new LoadEndUpdateTexture();
            loadEndUpdate.Init(delegate
            {

            }, delegate
            {
                if (loadEndUpdate.IsEndWork)
                {
                    endNormal(loadEndUpdate.Num);
                }
                else
                {
                    end();
                }
            });
            CreateProcces(loadEndUpdate);
        }

        protected static void CreateProcces(BaseProcces proc)
        {
            if (procces == null)
            {
                procces = proc;
                proc.Start();
            }
        }

        private static void RemoveProcces()
        {
            procces = null;
        }


        public abstract class BaseProcces
        {

            bool isEnd
            {
                get
                {
                    return _is_end;
                }
                set
                {

                    if (!_is_end && value == true)
                    {
                        End();
                    }
                    _is_end = value;
                }
            }

            bool _is_end;

            private Action start;
            private Action end;

            public void Init(Action start, Action end)
            {
                this.start = start;
                this.end = end;
            }

            public void Start()
            {
                start();
                Update();
            }

            private async void Update()
            {
                await Task.Run(() => { try { CallUpdate(); } catch { } isEnd = true; });
            }

            protected virtual void CallUpdate()
            {

            }

            protected void Connect()
            {
                Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                point = new IPEndPoint(IPAddress.Parse(IP), Port);
                Socket.Connect(point);
            }

            private void End()
            {
                BaseConnect.RemoveProcces();
                CallEnd();
                if (BaseConnect.Socket.Connected)
                {
                    Socket.Close();
                }
                end();
            }

            protected virtual void CallEnd()
            {

            }
        }
    }

    public delegate void EndLoadNews(News[] news);

    public delegate void EndLoadTexture(Texture[] textures);

    public delegate void LoadNum(uint num);

    public class LoadListNews : BaseConnect.BaseProcces
    {
        public int CountNews { get; private set; }

        public List<News> NewsArray { get; private set; }

        public bool IsEndWork { get; private set; }

        protected override void CallUpdate()
        {
            NewsArray = new List<News>();

            Connect();
            int countRead = 0;
            int countWrite = 0;

            byte[] bytes = new byte[6] { 1, BaseConnect.GetCountNews, 0, 0, 0, 0 };
            countWrite = BaseConnect.Socket.Send(bytes);
            if (countWrite <= 0) return;

            byte[] countByte = new byte[4];
            countRead = BaseConnect.Socket.Receive(countByte, 4, SocketFlags.None);
            if (countRead <= 0) return;

            int countNews = System.BitConverter.ToInt32(countByte, 0);
            for (int i = 0; i < countNews; i++)
            {
                int numText = 0;

                byte[] send = new byte[5];
                send[0] = BaseConnect.LoadNews;
                byte[] num = BitConverter.GetBytes(i);
                send[1] = num[0];
                send[2] = num[1];
                send[3] = num[2];
                send[4] = num[3];
                BaseConnect.Socket.Send(send);

                List<byte>[] bytes_str = new List<byte>[4];
                for (int i2 = 0; i2 < 4; i2++)
                {
                    bytes_str[i2] = new List<byte>();
                }

                //check news is exist
                byte[] bufNoNews = new byte[2];
                int countBufNoNews = BaseConnect.Socket.Receive(bufNoNews, 2, SocketFlags.Peek);


                if (bufNoNews[0] == 0 && bufNoNews[1] == 0)
                {
                    BaseConnect.Socket.Receive(bufNoNews, 2, SocketFlags.None);
                    continue;
                }
                if (countBufNoNews <= 0)
                {
                    return;
                }
                //

                News news = new News();
                NewsArray.Add(news);
                news.NumIndex = i;

                byte[] buf = new byte[1];
                while (true)
                {
                    countRead = BaseConnect.Socket.Receive(buf, 1, SocketFlags.None);
                    if (countRead <= 0)
                    {
                        return;
                    }
                    else
                    {
                        if (numText <= 3 && buf[0] == 0)
                        {
                            numText++;
                        }
                        else if (numText > 3 && buf[0] == 0)
                        {
                            break;
                        }
                        else
                        {
                            bytes_str[numText].Add(buf[0]);
                        }
                    }
                }
                /*for (int i2 = 0; i2 < bytes_str.Length; i2++)
                {
                    while ((bytes_str[i2].Remove(1)) == true)
                    {
                    }
                }*/
                news.EnTitle = Encoding.UTF8.GetString(bytes_str[0].ToArray());
                news.EnContent = Encoding.UTF8.GetString(bytes_str[1].ToArray());
                news.RuTitle = Encoding.UTF8.GetString(bytes_str[2].ToArray());
                news.RuContent = Encoding.UTF8.GetString(bytes_str[3].ToArray());
            }
            CountNews = NewsArray.Count;
            IsEndWork = true;
        }

        protected override void CallEnd()
        {

        }
    }

    public class LoadEndUpdateNews : BaseConnect.BaseProcces
    {
        public bool IsEndWork { get; private set; }

        public uint Num { get; private set; }

        protected override void CallUpdate()
        {
            Connect();
            byte[] bytes = new byte[6] { 1, BaseConnect.GetEndUpdateNews, 0, 0, 0, 0 };
            int countWrite = BaseConnect.Socket.Send(bytes);
            if (countWrite <= 0) return;

            byte[] buf = new byte[sizeof(uint)];
            int countRead = BaseConnect.Socket.Receive(buf, 4, SocketFlags.None);
            if (countRead == sizeof(uint))
            {
                Num = BitConverter.ToUInt32(buf, 0);
                IsEndWork = true;
                return;
            }
            else
            {
                return;
            }
        }

        protected override void CallEnd()
        {
            base.CallEnd();
        }
    }

    public class LoadEndUpdateTexture : BaseConnect.BaseProcces
    {
        public bool IsEndWork { get; private set; }

        public uint Num { get; private set; }

        protected override void CallUpdate()
        {
            Connect();
            byte[] bytes = new byte[6] { 1, BaseConnect.GetEndUpdateTexture, 0, 0, 0, 0 };
            int countWrite = BaseConnect.Socket.Send(bytes);
            if (countWrite <= 0) return;

            byte[] buf = new byte[sizeof(uint)];
            int countRead = BaseConnect.Socket.Receive(buf, 4, SocketFlags.None);
            if (countRead == sizeof(uint))
            {
                Num = BitConverter.ToUInt32(buf, 0);
                IsEndWork = true;
                return;
            }
            else
            {
                return;
            }
        }

        protected override void CallEnd()
        {
            base.CallEnd();
        }
    }

    public class LoadListTextureName : BaseConnect.BaseProcces
    {
        public int CountTexutre { get; private set; }

        public List<Texture> TextureArray { get; private set; }

        public bool IsEndWork { get; private set; }

        protected override void CallUpdate()
        {
            TextureArray = new List<Texture>();
            Connect();
            int countRead = 0;
            int countWrite = 0;
            byte[] bytes = new byte[6] { 1, BaseConnect.GetCountTexture, 0, 0, 0, 0 };
            countWrite = BaseConnect.Socket.Send(bytes);
            if (countWrite <= 0) return;

            byte[] countByte = new byte[4];
            countRead = BaseConnect.Socket.Receive(countByte, 4, SocketFlags.None);
            if (countRead <= 0) return;

            int countTexture = System.BitConverter.ToInt32(countByte, 0);



            for (int i = 0; i < countTexture; i++)
            {
                byte[] numByte = BitConverter.GetBytes(i);
                bytes = new byte[5] { BaseConnect.LoadTextureName, numByte[0], numByte[1], numByte[2], numByte[3] };
                countWrite = BaseConnect.Socket.Send(bytes);
                if (countWrite <= 0) return;

                //check texture exist
                byte[] bufNoNews = new byte[2];
                int countBufNoNews = BaseConnect.Socket.Receive(bufNoNews, 2, SocketFlags.Peek);


                if (bufNoNews[0] == 0 && bufNoNews[1] == 0)
                {
                    BaseConnect.Socket.Receive(bufNoNews, 2, SocketFlags.None);
                    continue;
                }
                if (countBufNoNews <= 0)
                {
                    return;
                }
                //

                TextureArray.Add(new Texture());
                TextureArray[TextureArray.Count - 1].NumIndex = i;

                byte[] buf = new byte[1];
                List<byte> byteName = new List<byte>();
                while (true)
                {
                    countRead = BaseConnect.Socket.Receive(buf, 1, SocketFlags.None);
                    if (countRead <= 0)
                    {
                        return;
                    }
                    else
                    {
                        if (buf[0] == 0)
                        {
                            break;
                        }
                        else
                        {
                            byteName.Add(buf[0]);
                        }
                    }
                }

                TextureArray[i].Name = Encoding.ASCII.GetString(byteName.ToArray());
            }
            CountTexutre = TextureArray.Count();
            IsEndWork = true;
        }

        protected override void CallEnd()
        {
            base.CallEnd();
        }
    }

    public class LoadListTexture : BaseConnect.BaseProcces
    {
        public int CountTexutre { get; private set; }

        public List<Texture> TextureArray { get; private set; }

        public bool IsEndWork { get; private set; }

        protected override void CallUpdate()
        {
            TextureArray = new List<Texture>();
            Connect();
            int countRead = 0;
            int countWrite = 0;
            byte[] bytes = new byte[6] { 1, BaseConnect.GetCountTexture, 0, 0, 0, 0 };
            countWrite = BaseConnect.Socket.Send(bytes);
            if (countWrite <= 0) return;

            byte[] countByte = new byte[4];
            countRead = BaseConnect.Socket.Receive(countByte, 4, SocketFlags.None);
            if (countRead <= 0) return;

            int countTexture = System.BitConverter.ToInt32(countByte, 0);
            for (int i = 0; i < countTexture; i++)
            {
                byte[] numByte = BitConverter.GetBytes(i);
                byte[] bytes2 = new byte[5] { BaseConnect.LoadTexture, numByte[0], numByte[1], numByte[2], numByte[3] };
                countWrite = BaseConnect.Socket.Send(bytes2);
                if (countWrite <= 0) return;


                //check texture exist
                byte[] bufNoTexture = new byte[2];
                int countBufNoNews = BaseConnect.Socket.Receive(bufNoTexture, 2, SocketFlags.Peek);


                if (bufNoTexture[0] == 0 && bufNoTexture[1] == 0)
                {
                    BaseConnect.Socket.Receive(bufNoTexture, 2, SocketFlags.None);
                    continue;
                }
                if (countBufNoNews <= 0)
                {
                    return;
                }
                //

                TextureArray.Add(new Texture());
                TextureArray[TextureArray.Count - 1].NumIndex = i;

                byte[] buf = new byte[1];


                List<byte> byteName = new List<byte>();
                while (true)
                {
                    countRead = BaseConnect.Socket.Receive(buf, 1, SocketFlags.None);
                    if (countRead <= 0)
                    {
                        return;
                    }
                    else
                    {
                        if (buf[0] == 0)
                        {
                            break;
                        }
                        else
                        {
                            byteName.Add(buf[0]);
                        }
                    }
                }
                TextureArray[TextureArray.Count - 1].Name = Encoding.ASCII.GetString(byteName.ToArray());

                byte[] loadTexture = new byte[FilerServer.Texture.SizeTexture * FilerServer.Texture.SizeTexture * 4];
                countRead = 0;
                while (countRead < loadTexture.Length)
                {
                    int recv = BaseConnect.Socket.Receive(loadTexture, countRead, loadTexture.Length - countRead, SocketFlags.None);
                    if (recv <= 0)
                    {
                        return;
                    }
                    else
                    {
                        countRead += recv;
                    }
                }

                for (int i2 = 0; i2 < TextureArray[TextureArray.Count - 1].Pixels.Length; i2++)
                {
                    TextureArray[i].Pixels[i2].r = loadTexture[i2 * 4 + 0];
                    TextureArray[i].Pixels[i2].g = loadTexture[i2 * 4 + 1];
                    TextureArray[i].Pixels[i2].b = loadTexture[i2 * 4 + 2];
                    TextureArray[i].Pixels[i2].a = loadTexture[i2 * 4 + 3];
                }
            }
            CountTexutre = TextureArray.Count();
            IsEndWork = true;
        }

        protected override void CallEnd()
        {
            base.CallEnd();
        }
    }
}
