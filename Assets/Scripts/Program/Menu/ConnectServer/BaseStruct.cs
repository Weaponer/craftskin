﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FilerServer
{
    [Serializable]
    public abstract class LoadObject {
        public int NumIndex;
    }

    [Serializable]
    public class News : LoadObject
    {
        public string EnTitle;
        public string EnContent;
        public string RuTitle;
        public string RuContent;
    }

    [Serializable]
    public class Texture : LoadObject
    {

        public const int SizeTexture = 64;

        public string Name;

        public Pixel[] Pixels;

        public Texture() {
            Pixels = new Pixel[SizeTexture * SizeTexture];
            for (int i = 0; i < Pixels.Length; i++) {
                Pixels[i] = new Pixel();
            }
        }

        [Serializable]
        public class Pixel
        {
            public byte r;
            public byte g;
            public byte b;
            public byte a;
        }
    }
}
