﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Program.Option
{
    public abstract class OptionObject : MonoBehaviour
    {
        public abstract object GetOption();

        public abstract void SetOption(object option);
    }
}