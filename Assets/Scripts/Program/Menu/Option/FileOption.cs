﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Program.Option
{
    [System.Serializable]
    public class FileOption
    {
        public object[] Options;
    }
}