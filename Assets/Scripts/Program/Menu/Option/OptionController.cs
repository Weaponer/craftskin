﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace Program.Option
{
    public class OptionController : MonoBehaviour
    {
        [SerializeField] private List<OptionObject> options = new List<OptionObject>();

        private FileOption fileOption;

        private void Start()
        {
            FileOption file = FilersOption.LoadFile();
            if (file == null || file.Options.Length != options.Count)
            {
                fileOption = new FileOption();
            }
            else
            {
                fileOption = file;
                for (int i = 0; i < options.Count; i++)
                {
                    options[i].SetOption(fileOption.Options[i]);
                }
            }
        }



        private void OnDestroy()
        {
            fileOption = new FileOption();
            fileOption.Options = options.Select((x) => x.GetOption()).ToArray();
            FilersOption.SaveFile(fileOption);
        }
    }
}