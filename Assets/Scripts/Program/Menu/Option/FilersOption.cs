﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Program.Option
{
    public static class FilersOption
    {
        public const string NameFile = "options";
        public const string Type = "opt";

        public static void SaveFile(FileOption fileOption)
        {
            using (FileStream file = File.Create(Application.dataPath + "/" + NameFile + "." + Type))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();

                binaryFormatter.Serialize(file, fileOption);
                file.Close();
            }
        }

        public static FileOption LoadFile()
        {
            if (!File.Exists(Application.dataPath + "/" + NameFile + "." + Type))
            {
                Debug.Log(1);
                return null;
            }

            using (FileStream file = File.OpenRead(Application.dataPath + "/" + NameFile + "." + Type))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();

                FileOption fileOption = (FileOption)binaryFormatter.Deserialize(file);
                file.Close();
                return fileOption;
            }
        }
    }
}