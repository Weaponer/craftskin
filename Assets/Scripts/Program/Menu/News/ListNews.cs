﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Program.Menu
{
    public class ListNews : MonoBehaviour
    {
        public static ListNews Singleton { get; private set; }

        [SerializeField] private UIMenu uIMenu;

        [SerializeField] private ItemNews prefabNews;

        private List<ItemNews> items = new List<ItemNews>();

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                Singleton = this;
            }

            StartCoroutine(LoadNewsWait());
        }

        IEnumerator LoadNewsWait() {
            if (!NewsServer.IsLoad && !NewsServer.IsLoadEnd)
            {
                print("Start load news");
                NewsServer.Start();
                if (NewsServer.NewsLoad != null && NewsServer.NewsLoad.ArrayNews != null)
                {
                    print("Start load base");
                    LoadListNews();
                }
            }
            while (true) {
                if (NewsServer.IsLoadEnd) {
                    LoadListNews();
                    NewsServer.SaveCurrentFile();
                    print("end download news");
                    break;
                }
                if (!NewsServer.IsLoad) {
                    print("reload news");
                    NewsServer.Start();
                }
                yield return new WaitForSecondsRealtime(2.0f);
            }
            yield break;
        }

        private void LoadListNews()
        {
            Clear();
            for (int i = 0; i < NewsServer.NewsLoad.ArrayNews.Length; i++) {
                CreateNews news = new CreateNews(NewsServer.NewsLoad.ArrayNews[i]);
                AddNews(news);
            }
        }

        public void AddNews(CreateNews createNews)
        {
            ItemNews item = Instantiate(prefabNews, uIMenu.GetContentNews());           
            item.Init(createNews.RussianNews, createNews.EnglishNews);
            items.Add(item);
        }

        public void Clear() {
            while (items.Count > 0) {
                ItemNews it = items[0];
                items.Remove(it);
                Destroy(it.gameObject);
            }
        }

        private void OnDestroy()
        {
            if (Singleton == this)
                Singleton = null;
        }
    }
}