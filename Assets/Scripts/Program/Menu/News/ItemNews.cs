﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.Menu
{
    public class ItemNews : MonoBehaviour
    {
        [SerializeField] private UI.EditText editText;

        [SerializeField] private RectTransform panel;



        public void Init(string russian, string english)
        {
            int countLine = 0;
            int max = 0;
            for (int i = 0; i < russian.Length; i++)
            {
                if (russian[i] == '\r' || russian[i] == '\n')
                {
                    countLine++;
                }
            }
            max = countLine;
            countLine = 0;
            for (int i = 0; i < english.Length; i++)
            {
                if (english[i] == '\r' || english[i] == '\n')
                {
                    countLine++;
                }
            }
            if (max > countLine)
                countLine = max;
            countLine++;

            editText.SetParams(russian, english);
            panel.sizeDelta = new Vector2(1219, 90f * countLine);

        }
    }
}