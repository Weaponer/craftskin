﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FilerServer;

namespace Program.Menu
{
    public class CreateNews
    {
        public string RussianNews;

        public string EnglishNews;

        public CreateNews(News news)
        {
            EnglishNews = news.EnTitle + '\n' + news.EnContent;
            RussianNews = news.RuTitle + '\n' + news.RuContent;
        }
    }
}