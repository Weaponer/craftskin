﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Program.UI
{
    public class PanelProgram : MonoBehaviour
    {
        [SerializeField] private bool isStart;

        [SerializeField] private bool isHideOpen;

        [SerializeField] private GameObject panel;

        [SerializeField] private Button open;

        [SerializeField] private Button close;

        bool isOpen;

        private void Start()
        {
            isOpen = isStart;
            SetParams();
            open.onClick.AddListener(delegate { OpenButton(); });
            close.onClick.AddListener(delegate { CloseButton(); });
        }

        private void OpenButton()
        {
            if (!isOpen)
            {
                isOpen = true;
                SetParams();
            }
        }

        private void CloseButton()
        {
            if (isOpen)
            {
                isOpen = false;
                SetParams();
            }
        }

        private void SetParams()
        {
            if (isOpen)
            {
                panel.SetActive(true);
                if (isHideOpen)
                {
                    open.gameObject.SetActive(false);
                }

            }
            else
            {
                panel.SetActive(false);
                if (isHideOpen)
                {
                    open.gameObject.SetActive(true);
                }
            }



        }
    }
}