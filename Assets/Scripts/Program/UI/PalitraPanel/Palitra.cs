﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Palitra
{
    public float[] R;
    public float[] G;
    public float[] B;
    public float[] A;


    public static bool operator ==(Palitra left, Palitra right)
    {
        if (left.A.Length != right.A.Length)
            return false;
        for (int i = 0; i < left.A.Length; i++)
        {
            if (left.A[i] != right.A[i] || left.R[i] != right.R[i] || left.G[i] != right.G[i] || left.B[i] != right.B[i])
            {
                return false;
            }
        }
        return true;
    }

    public static bool operator !=(Palitra left, Palitra right)
    {
        if (left.A.Length != right.A.Length)
            return true;
        for (int i = 0; i < left.A.Length; i++)
        {
            if (left.A[i] != right.A[i] || left.R[i] != right.R[i] || left.G[i] != right.G[i] || left.B[i] != right.B[i])
            {
                return true;
            }
        }
        return false;
    }
}
