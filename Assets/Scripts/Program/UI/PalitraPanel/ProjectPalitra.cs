﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProjectPalitra
{
    public List<Color> Colors = new List<Color>();

    public Palitra GetPalitra()
    {
        Palitra palitra = new Palitra();
        palitra.A = new float[Colors.Count];
        palitra.R = new float[Colors.Count];
        palitra.G = new float[Colors.Count];
        palitra.B = new float[Colors.Count];
        for (int i = 0; i < Colors.Count; i++)
        {
            palitra.R[i] = Colors[i].r;
            palitra.B[i] = Colors[i].b;
            palitra.G[i] = Colors[i].g;
            palitra.A[i] = Colors[i].a;
        }

        return palitra;
    }

    public void SetPalitra(Palitra palitra)
    {
        Colors.Clear();
        for (int i = 0; i < palitra.G.Length; i++)
        {
            Colors.Add(new Color(palitra.R[i], palitra.G[i], palitra.B[i], palitra.A[i]));
        }
    }
}
