﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.UI
{
    public class PalitraItem : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Text text;


        public void Init(Palitra palitra, MenuPaltira menuPaltira, int num)
        {
            text.text = num.ToString();
            button.onClick.AddListener(delegate { menuPaltira.NewTakePalitra(palitra); });
        }
    }
}