﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.UI
{
    public class ColorPickerPanel : CenterPanel
    {
        [SerializeField] private GameObject panel;

        [SerializeField] private Button back;

        [SerializeField] private Image textureUp;

        [SerializeField] private Image texturePicker;

        [SerializeField] private Slider slider;

        [SerializeField] private RectTransform joystic;

        private Texture2D gradientTexture;

        private bool isMove;

        private void Start()
        {
            back.onClick.AddListener(delegate { ClosePanel(); });
            slider.onValueChanged.AddListener(delegate { SliderRGB(); });

            InitializedParams();
        }

        protected override void OpenCallBack()
        {
            panel.SetActive(true);
            SetColorFromPalitra();
        }

        protected override void CloseCallBack()
        {
            panel.SetActive(false);
        }

        private void InitializedParams()
        {
            Texture2D tex = new Texture2D(1, (int)textureUp.rectTransform.rect.height);
            for (int i = 0; i < tex.height; i++)
            {
                tex.SetPixel(0, i, Color.HSVToRGB(1 - (float)i / (tex.height - 1), 1f, 1f));
            }

            tex.Apply();
            textureUp.sprite = Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100f);

            gradientTexture = new Texture2D((int)texturePicker.rectTransform.rect.width, (int)texturePicker.rectTransform.rect.height);
            texturePicker.sprite = Sprite.Create(gradientTexture, new Rect(0f, 0f, gradientTexture.width, gradientTexture.height), new Vector2(0.5f, 0.5f), 100f);
        }

        private void Update()
        {
            if (IsOpen)
            {
                if (Input.GetMouseButtonDown(0) && CheckMouseInPicker())
                {
                    isMove = true;
                }

                if (Input.GetMouseButtonUp(0))
                {
                    isMove = false;
                }

                if (isMove)
                {

                    Vector3 pos = texturePicker.transform.InverseTransformPoint(Input.mousePosition);

                    pos.x = Mathf.Clamp(pos.x, 0, gradientTexture.width);
                    pos.y = Mathf.Clamp(pos.y, 0, gradientTexture.height);
                    joystic.transform.localPosition = pos;
                    SetColorPalitra(GetColorJoistic());
                }
            }
        }

        private bool CheckMouseInPicker()
        {
            Vector3 pos = texturePicker.transform.InverseTransformPoint(Input.mousePosition);

            if (pos.x >= 0 && pos.x <= gradientTexture.width && pos.y >= 0 && pos.y <= gradientTexture.height)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SliderRGB()
        {
            SetGradient();

            SetColorPalitra(GetColorJoistic());
        }

        private void SetGradient()
        {
            List<Color> pixels = new List<Color>();
            float value = slider.value;

            Vector2 size;
            size.x = gradientTexture.width;
            size.y = gradientTexture.height;

            for (int i = 0; i < gradientTexture.height; i++)
            {
                for (int i2 = 0; i2 < gradientTexture.width; i2++)
                {
                    pixels.Add(Color.HSVToRGB(value, (float)i2 / size.x, (float)i / size.y));
                }
            }
            gradientTexture.SetPixels(pixels.ToArray());
            gradientTexture.Apply();
        }

        private Color GetColorJoistic()
        {
            Vector3 PosT = joystic.transform.localPosition;


            PosT.x = Mathf.Max(1, PosT.x);
            PosT.y = Mathf.Max(1, PosT.y);
            PosT.x = Mathf.Min(PosT.x, gradientTexture.width - 1);
            PosT.y = Mathf.Min(PosT.y, gradientTexture.height - 1);

            PosT.x /= gradientTexture.width;
            PosT.y /= gradientTexture.height;

            return gradientTexture.GetPixelBilinear(PosT.x, PosT.y);
        }

        private void SetColorPalitra(Color color)
        {
            PalitraPanel.ColorList.EditColor(color);
        }

        private void SetColorFromPalitra()
        {
            Color color = PalitraPanel.ColorList.GetCurrentItem().GetColor();
            float h, s, v;
            Color.RGBToHSV(color, out h, out s, out v);
            slider.SetValueWithoutNotify(h);
            SetGradient();

            Vector3 pos = new Vector3(s * texturePicker.rectTransform.rect.width, v * texturePicker.rectTransform.rect.height, 0);

            joystic.transform.localPosition = pos;
        }
    }
}