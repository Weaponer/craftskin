﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Program.UI
{
    public class ColorItem : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Image image;


        public void Init(ColorPalitra colorTake)
        {
            button.onClick.AddListener(delegate { colorTake.Take(this); });
        }

        public Color GetColor()
        {
            return image.color;
        }

        public void SetColor(Color color)
        {
            image.color = color;
        }
    }
}