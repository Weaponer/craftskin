﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.UI
{

    public class PalitraPanel : MonoBehaviour
    {
        [SerializeField] private ColorPickerPanel colorPickerPanel;

        [SerializeField] private Button openPicker;

        [SerializeField] private ProgramUI programUI;

        [SerializeField] private PalitraDataBase palitraDataBase;

        [SerializeField] private Button openPalitras;

        [SerializeField] private MenuPaltira menuPaltira;

        [Space(4)]
        [SerializeField] private Button paint;

        [SerializeField] private Image paintOn;

        [SerializeField] private Image paintOff;

        [Space(4)]
        [SerializeField] private Button takeColor;

        [SerializeField] private Image takeColorOn;

        [SerializeField] private Image takeColorOff;

        [Space(4)]
        [SerializeField] private Button fillColor;

        [SerializeField] private Image fillColorOn;

        [SerializeField] private Image fillColorOff;

        [Space(4)]
        [SerializeField] private Button erase;

        [SerializeField] private Image eraseOn;

        [SerializeField] private Image eraseOff;

        [SerializeField] private ColorItem[] colorItems;

        [SerializeField] private Transform pointTake;

        [SerializeField] private Button backHistory;

        [SerializeField] private Button nextHistory;

        public static Instruments Instrument { get; private set; }

        public static ColorPalitra ColorList { get; private set; }

        public static HistoryButton History { get; private set; }

        public static PalitraPanel Singleton { get; private set; }

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
            }
            else
            {
                Singleton = this;
            }

            History = new HistoryButton();

            ColorList = new ColorPalitra();
        }

        private void Start()
        {

            backHistory.onClick.AddListener(delegate { History.BackHistory(); });
            nextHistory.onClick.AddListener(delegate { History.NextHistory(); });


            ColorList.TakeItem += EditTakeColor;

            for (int i = 0; i < colorItems.Length; i++)
            {
                colorItems[i].Init(ColorList);
            }
            ColorList.SetItems(colorItems);
            ColorList.SetPalitra(palitraDataBase.Palitras[0]);

            Instrument = new Instruments();

            paint.onClick.AddListener(delegate { Instrument.TakePaint(); });
            takeColor.onClick.AddListener(delegate { Instrument.TakeColor(); });
            fillColor.onClick.AddListener(delegate { Instrument.TakeFillColor(); });
            erase.onClick.AddListener(delegate { Instrument.TakeErase(); });

            Instrument.TakeInstrument += EditInstrument;

            Instrument.TakePaint();

            openPalitras.onClick.AddListener(delegate { programUI.OpenCenterPanel(menuPaltira); });

            openPicker.onClick.AddListener(delegate { programUI.OpenCenterPanel(colorPickerPanel); });

            menuPaltira.TakePalitra += SetNewPalitra;
        }

        private void EditInstrument()
        {
            if (Instrument.InstrumentTake == UI.Instrument.Paint)
            {
                paintOn.gameObject.SetActive(true);
                paintOff.gameObject.SetActive(false);

                takeColorOn.gameObject.SetActive(false);
                takeColorOff.gameObject.SetActive(true);

                fillColorOn.gameObject.SetActive(false);
                fillColorOff.gameObject.SetActive(true);

                eraseOn.gameObject.SetActive(false);
                eraseOff.gameObject.SetActive(true);
            }
            else if (Instrument.InstrumentTake == UI.Instrument.FillColor)
            {
                paintOn.gameObject.SetActive(false);
                paintOff.gameObject.SetActive(true);

                takeColorOn.gameObject.SetActive(false);
                takeColorOff.gameObject.SetActive(true);

                fillColorOn.gameObject.SetActive(true);
                fillColorOff.gameObject.SetActive(false);

                eraseOn.gameObject.SetActive(false);
                eraseOff.gameObject.SetActive(true);
            }
            else if (Instrument.InstrumentTake == UI.Instrument.Erase)
            {
                paintOn.gameObject.SetActive(false);
                paintOff.gameObject.SetActive(true);

                takeColorOn.gameObject.SetActive(false);
                takeColorOff.gameObject.SetActive(true);

                fillColorOn.gameObject.SetActive(false);
                fillColorOff.gameObject.SetActive(true);

                eraseOn.gameObject.SetActive(true);
                eraseOff.gameObject.SetActive(false);
            }
            else if (Instrument.InstrumentTake == UI.Instrument.TakeColor)
            {
                paintOn.gameObject.SetActive(false);
                paintOff.gameObject.SetActive(true);

                takeColorOn.gameObject.SetActive(true);
                takeColorOff.gameObject.SetActive(false);

                fillColorOn.gameObject.SetActive(false);
                fillColorOff.gameObject.SetActive(true);

                eraseOn.gameObject.SetActive(false);
                eraseOff.gameObject.SetActive(true);
            }
        }

        private void EditTakeColor()
        {

            Transform transform = ColorList.GetCurrentItem().transform;
            pointTake.position = transform.position;
            pointTake.transform.parent = transform;
        }

        private void SetNewPalitra(Palitra palitra)
        {
            ProjectPalitra projectPalitra = new ProjectPalitra();
            projectPalitra.SetPalitra(palitra);
            ColorList.SetPalitra(projectPalitra);
        }

        private void OnDestroy()
        {

            Instrument = null;
            ColorList = null;
            History = null;
            Singleton = null;
        }
    }
}