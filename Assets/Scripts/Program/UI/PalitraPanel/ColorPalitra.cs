﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Program.UI
{
    public class ColorPalitra
    {
        public event Action TakeItem;

        private ColorItem[] items;

        private ColorItem thisItem;


        public void SetPalitra(ProjectPalitra palitra)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (palitra.Colors.Count <= i)
                {
                    return;
                }
                items[i].SetColor(palitra.Colors[i]);
            }
        }

        public ProjectPalitra GetPalitra()
        {
            ProjectPalitra palitra = new ProjectPalitra();
            for (int i = 0; i < items.Length; i++)
            {
                palitra.Colors.Add(items[i].GetColor());
            }
            return palitra;
        }

        public void EditColor(Color color)
        {
            color.a = 1;
            thisItem.SetColor(color);
        }

        public void SetItems(ColorItem[] items)
        {
            if (items.Length > 0)
            {
                this.items = items;
                Take(items[0]);
            }
        }

        public ColorItem GetCurrentItem()
        {
            return thisItem;
        }

        public void Take(ColorItem item)
        {
            thisItem = item;
            if (TakeItem != null)
                TakeItem();
        }
    }
}