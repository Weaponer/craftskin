﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Palitra Data", menuName = "PalitraDataBase")]
public class PalitraDataBase : ScriptableObject
{
    public List<ProjectPalitra> Palitras = new List<ProjectPalitra>();
}
