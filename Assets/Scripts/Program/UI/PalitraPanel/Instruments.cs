﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Program.UI
{
    public enum Instrument
    {
        None,
        Paint,
        TakeColor,
        FillColor,
        Erase,
    }

    public class Instruments
    {
        public Instrument InstrumentTake { get; private set; }

        public event Action TakeInstrument;

        public void TakePaint()
        {
            if (Instrument.Paint != InstrumentTake)
            {
                InstrumentTake = Instrument.Paint;
                if (TakeInstrument != null)
                    TakeInstrument();
            }
        }

        public void TakeColor()
        {
            if (Instrument.TakeColor != InstrumentTake)
            {
                InstrumentTake = Instrument.TakeColor;
                if (TakeInstrument != null)
                    TakeInstrument();
            }
        }

        public void TakeFillColor()
        {
            if (Instrument.FillColor != InstrumentTake)
            {
                InstrumentTake = Instrument.FillColor;
                if (TakeInstrument != null)
                    TakeInstrument();
            }
        }

        public void TakeErase()
        {
            if (Instrument.Erase != InstrumentTake)
            {
                InstrumentTake = Instrument.Erase;
                if (TakeInstrument != null)
                    TakeInstrument();
            }
        }
    }
}
