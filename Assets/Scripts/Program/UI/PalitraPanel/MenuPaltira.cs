﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;


namespace Program.UI
{

    public delegate void SetPalitra(Palitra palitra);

    public class MenuPaltira : CenterPanel
    {
        [SerializeField] private Button saveFile;

        [SerializeField] private Button deletFile;

        [SerializeField] private GameObject panel;

        [SerializeField] private Button breakBut;

        [SerializeField] private PalitraItem prefabItem;

        [SerializeField] private Transform content;

        [SerializeField] private PalitraDataBase palitraData;

        private List<PalitraItem> objs = new List<PalitraItem>();

        private const string nameDirectory = "Palitres";

        private const string typeFile = ".pl";

        public event SetPalitra TakePalitra;

        public static MenuPaltira Singleton { get; private set; }

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
            }
            else
            {
                Singleton = this;
            }
        }

        private void Start()
        {
            breakBut.onClick.AddListener(delegate { ClosePanel(); });

            deletFile.onClick.AddListener(delegate { DeletPalitra(PalitraPanel.ColorList.GetPalitra().GetPalitra()); UpdateList(); });

            saveFile.onClick.AddListener(delegate { SavePalitra(PalitraPanel.ColorList.GetPalitra().GetPalitra()); UpdateList(); });
        }

        protected override void OpenCallBack()
        {
            panel.SetActive(true);
            CheckDirectory();

            List<Palitra> p = new List<Palitra>();
            p.AddRange(palitraData.Palitras.Select((x) => x.GetPalitra()));
            p.AddRange(GetPaliters());
            UpdateList(p.ToArray());
        }

        protected override void CloseCallBack()
        {
            panel.SetActive(false);
        }

        public void NewTakePalitra(Palitra palitra)
        {
            if (TakePalitra != null)
                TakePalitra(palitra);
        }

        private void UpdateList(Palitra[] palitra)
        {
            for (int i = 0; i < objs.Count; i++)
            {
                Destroy(objs[i].gameObject);
            }
            objs.Clear();

            for (int i = 0; i < palitra.Length; i++)
            {
                PalitraItem palitraItem = Instantiate(prefabItem, content);

                palitraItem.Init(palitra[i], this, i + 1);
                objs.Add(palitraItem);
            }
        }

        private void SavePalitra(Palitra palitra)
        {
            FileStream fileStream = File.Create(Application.persistentDataPath + "/" + nameDirectory + "/" + (Directory.GetFiles(Application.persistentDataPath + "/" + nameDirectory + "/", "*" + typeFile).Length + 3) + typeFile);

            new BinaryFormatter().Serialize(fileStream, palitra);
            fileStream.Close();
        }

        private void UpdateList()
        {
            for (int i = 0; i < objs.Count; i++)
            {
                Destroy(objs[i].gameObject);
            }
            objs.Clear();

            List<Palitra> palitra = new List<Palitra>();
            palitra.AddRange(palitraData.Palitras.Select((x) => x.GetPalitra()));
            palitra.AddRange(GetPaliters());

            for (int i = 0; i < palitra.Count; i++)
            {
                PalitraItem palitraItem = Instantiate(prefabItem, content);

                palitraItem.Init(palitra[i], this, i + 1);
                objs.Add(palitraItem);
            }
        }

        private void DeletPalitra(Palitra palitra)
        {
            ProjectPalitra projectPalitra = PalitraPanel.ColorList.GetPalitra();
            Palitra pl = projectPalitra.GetPalitra();

            string[] paths = Directory.GetFiles(Application.persistentDataPath + "/" + nameDirectory + "/", "*" + typeFile);
            for (int i = 0; i < paths.Length; i++)
            {
                FileStream fileStream = File.OpenRead(paths[i]);
                Palitra p = new BinaryFormatter().Deserialize(fileStream) as Palitra;

                if (palitra == p)
                {
                    fileStream.Close();
                    File.Delete(paths[i]);
                    return;
                }
                fileStream.Close();
            }
        }

        private Palitra[] GetPaliters()
        {
            string[] paths = Directory.GetFiles(Application.persistentDataPath + "/" + nameDirectory + "/", "*" + typeFile);

            List<Palitra> palitras = new List<Palitra>();

            for (int i = 0; i < paths.Length; i++)
            {
                FileStream fileStream = File.OpenRead(paths[i]);
                palitras.Add(new BinaryFormatter().Deserialize(fileStream) as Palitra);
            }

            return palitras.ToArray();
        }

        private void CheckDirectory()
        {
            if (!Directory.Exists(Application.persistentDataPath + "/" + nameDirectory))
            {
                Directory.CreateDirectory(Application.persistentDataPath + "/" + nameDirectory);
            }
        }

        private void OnDestroy()
        {
            Singleton = null;
        }
    }
}