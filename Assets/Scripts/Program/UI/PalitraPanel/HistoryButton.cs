﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Program.UI
{
    public class HistoryButton
    {
        public event Action HistoryNext;

        public event Action HistoryBack;

        public void BackHistory()
        {
            if (HistoryBack != null)
                HistoryBack();
        }

        public void NextHistory()
        {
            if (HistoryNext != null)
                HistoryNext();
        }
    }
}