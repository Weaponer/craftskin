﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Program.UI
{
    public class MenuPanel : MonoBehaviour
    {

        [SerializeField] private Button buttonSaveFile;

        [SerializeField] private Button buttonLoadFile;

        [SerializeField] private Button buttonNewFile;

        [SerializeField] private Button buttonLoadFileFromLibrary;

        [SerializeField] private Button buttonEditPlayer;

        [SerializeField] private Button buttonEditItem;

        [SerializeField] private Button buttonEditBlock;

        public event Action SaveFile;

        public event Action LoadFile;

        public event Action NewFile;

        public event Action LoadFileFromLibrary;

        public event Action EditPlayer;

        public event Action EditItem;

        public event Action EditBlock;

        public static MenuPanel Singleton { get; private set; }

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
            }
            else
            {
                Singleton = this;
            }
        }

        private void Start()
        {
            buttonSaveFile.onClick.AddListener(delegate { if (SaveFile != null) SaveFile(); });
            buttonLoadFile.onClick.AddListener(delegate { if (LoadFile != null) LoadFile(); });
            buttonNewFile.onClick.AddListener(delegate { if (NewFile != null) NewFile(); });

            buttonEditPlayer.onClick.AddListener(delegate { if (EditPlayer != null) EditPlayer(); });
            buttonEditItem.onClick.AddListener(delegate { if (EditItem != null) EditItem(); });
            buttonEditBlock.onClick.AddListener(delegate { if (EditBlock != null) EditBlock(); });
            buttonLoadFileFromLibrary.onClick.AddListener(delegate { if (LoadFileFromLibrary != null) LoadFileFromLibrary(); });
        }


        private void OnDestroy()
        {
            Singleton = null;
        }
    }
}
