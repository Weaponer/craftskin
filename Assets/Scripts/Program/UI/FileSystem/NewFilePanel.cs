﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Program.UI
{
    public class NewFilePanel : CenterPanel
    {
        [SerializeField] private GameObject panel;

        [SerializeField] private Button back;

        [SerializeField] private Button yes;

        [SerializeField] private ProgramUI programUI;

        private void Start()
        {
            yes.onClick.AddListener(delegate { CreateNew(); ClosePanel(); });
            back.onClick.AddListener(delegate { ClosePanel(); });
            MenuPanel.Singleton.NewFile += SetButton;
        }

        private void SetButton()
        {
            programUI.OpenCenterPanel(this);
        }

        protected override void OpenCallBack()
        {
            panel.SetActive(true);
        }

        private void CreateNew()
        {
            Program.Editor.EditModel.Singleton.CurrentModel.CreateNewTexture();
        }

        protected override void CloseCallBack()
        {
            panel.SetActive(false);
        }
    }
}