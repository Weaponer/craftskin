﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Program.Editor;

namespace Program.UI
{
    public class SavePanel : CenterPanel
    {
        [SerializeField] private GameObject panel;

        [SerializeField] private ProgramUI programUI;

        [SerializeField] private Button back;

        [SerializeField] private Button save;

        [SerializeField] private InputField inputField;

        [SerializeField] private Transform content;

        [SerializeField] private ItemFile prefabItemFile;

        [SerializeField] private Text text;

        private List<ItemFile> itemFiles = new List<ItemFile>();

        private void Start()
        {
            MenuPanel.Singleton.SaveFile += SaveFile;

            back.onClick.AddListener(delegate { ClosePanel(); });
            save.onClick.AddListener(delegate { Save(); });

            text.text = Application.persistentDataPath + "/" + FileSystem.DirectoryName;
        }

        private void SaveFile()
        {
            programUI.OpenCenterPanel(this);
        }

        private void Save()
        {
            FileSystem.SaveFile(EditModel.Singleton.CurrentModel.GetTexture(), inputField.text);

            LoadList(FileSystem.LoadFiles());
        }

        private void LoadList(TextureFile[] files)
        {
            for (int i = 0; i < itemFiles.Count; i++)
            {
                Destroy(itemFiles[i].gameObject);
            }
            itemFiles.Clear();
            for (int i = 0; i < files.Length; i++)
            {

                ItemFile item = Instantiate(prefabItemFile, content);
                item.Initialized(files[i].Texture, files[i].Name);
                itemFiles.Add(item);
            }
        }

        protected override void OpenCallBack()
        {
            panel.SetActive(true);
            LoadList(FileSystem.LoadFiles());
        }
        protected override void CloseCallBack()
        {
            panel.SetActive(false);
        }
    }
}