﻿using Program.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.UI
{
    public class LoadPanel : CenterPanel
    {

        [SerializeField] private GameObject panel;

        [SerializeField] private ItemFile takeFile;

        [SerializeField] private ProgramUI programUI;

        [SerializeField] private Button back;

        [SerializeField] private Button load;

        [SerializeField] private InputField inputField;

        [SerializeField] private Transform content;

        [SerializeField] private ItemFile prefabItemFile;

        [SerializeField] private Text text;


        private List<ItemFile> itemFiles = new List<ItemFile>();

        private TextureFile[] lists;

        private void Start()
        {
            MenuPanel.Singleton.LoadFile += Load;

            back.onClick.AddListener(delegate { ClosePanel(); });
            load.onClick.AddListener(delegate { LoadFile(); });
            inputField.onEndEdit.AddListener(delegate { LoadList(lists); });

            text.text = Application.persistentDataPath + "/" + FileSystem.DirectoryName;

        }

        private void Load()
        {
            programUI.OpenCenterPanel(this);
        }

        private void SetLoadFile(ItemFile item)
        {
            Vector2 size = new Vector2(item.Texture.width, item.Texture.height);
            takeFile.Initialized(item.Texture, item.Text);
        }

        private void LoadFile()
        {
            if (takeFile.Texture != null)
            {
                EditModel.Singleton.CurrentModel.LoadTexture(takeFile.Texture);
                ClosePanel();
            }
        }

        private void LoadList(TextureFile[] files)
        {
            for (int i = 0; i < itemFiles.Count; i++)
            {
                Destroy(itemFiles[i].gameObject);
            }
            itemFiles.Clear();
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Name.IndexOf(inputField.text) > -1)
                {
                    ItemFile item = Instantiate(prefabItemFile, content);
                    item.Initialized(files[i].Texture, files[i].Name);
                    itemFiles.Add(item);
                    item.Click += SetLoadFile;
                }
            }
            if (itemFiles.Count > 0)
            {
                takeFile.gameObject.SetActive(true);
                SetLoadFile(itemFiles[0]);
            }
            else
            {
                takeFile.gameObject.SetActive(false);
            }
        }

        protected override void OpenCallBack()
        {
            panel.SetActive(true);

            lists = FileSystem.LoadFiles();

            LoadList(lists);
        }


        protected override void CloseCallBack()
        {
            panel.SetActive(false);
        }
    }
}