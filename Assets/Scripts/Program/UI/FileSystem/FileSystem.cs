﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public static class FileSystem
{

    public const string DirectoryName = "Textures";

    public const string Type = ".png";

    public static void SaveFile(Texture2D texture, string name)
    {
        CheckDirectory();

        byte[] image = texture.EncodeToPNG();

        FileStream fileStream = File.Open(Application.persistentDataPath + "/" + DirectoryName + "/" + name + "_" + texture.width + "x" + texture.height + Type, FileMode.OpenOrCreate);

        fileStream.Seek(0, SeekOrigin.Begin);
        fileStream.Write(image, 0, image.Length);

        fileStream.Close();
    }

    public static TextureFile[] LoadFiles()
    {
        CheckDirectory();

        List<TextureFile> textureFiles = new List<TextureFile>();

        string[] paths = Directory.GetFiles(Application.persistentDataPath + "/" + DirectoryName + "/", "*" + Type);

        for (int i = 0; i < paths.Length; i++)
        {
            byte[] image = File.ReadAllBytes(paths[i]);
            TextureFile texture = new TextureFile();
            texture.Texture = new Texture2D(2, 2);
            texture.Texture.LoadImage(image);
            texture.Name = Path.GetFileName(paths[i]);
            textureFiles.Add(texture);
        }

        return textureFiles.ToArray();
    }

    private static void CheckDirectory()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/" + DirectoryName))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/" + DirectoryName);
        }
    }
}


public class TextureFile
{
    public string Name;

    public Texture2D Texture;
}