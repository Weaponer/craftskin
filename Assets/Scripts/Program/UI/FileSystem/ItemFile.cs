﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public delegate void ClickItem(ItemFile item);

public class ItemFile : MonoBehaviour
{

    [SerializeField] private Text text;
    [SerializeField] private Image image;
    [SerializeField] private Button button;

    public Texture2D Texture { get; private set; }
    public string Text { get; private set; }

    public event ClickItem Click;

    public void Initialized(Texture2D texture, string text)
    {
        image.sprite = Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f);
        this.text.text = text;

        if (button)
        {
            button.onClick.AddListener(delegate { if (Click != null) Click(this); });
        }

        Texture = texture;
        Text = text;
    }
}
