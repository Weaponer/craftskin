using Program.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.UI
{
    public class LoadLibraryPanel : CenterPanel
    {

        [SerializeField] private GameObject panel;

        [SerializeField] private ProgramUI programUI;

        [SerializeField] private ItemFile takeFile;

        [SerializeField] private InputField inputField;

        [SerializeField] private Button back;

        [SerializeField] private Button load;

        [SerializeField] private Transform content;

        [SerializeField] private ItemFile prefabItemFile;

        private List<ItemFile> itemFiles = new List<ItemFile>();

        private TextureFile[] serverFiles = new TextureFile[0];

        private void Start()
        {
            MenuPanel.Singleton.LoadFileFromLibrary += OpenPanle;

            back.onClick.AddListener(delegate { ClosePanel(); });
            load.onClick.AddListener(delegate { LoadFile(); });

            inputField.onEndEdit.AddListener(delegate { LoadList(serverFiles); });

            StartCoroutine(LoadTextureWait());
        }

        IEnumerator LoadTextureWait()
        {
            if (!TexturesServer.IsLoad && !TexturesServer.IsLoadEnd)
            {
                TexturesServer.Start();
                if (TexturesServer.TextureLoad != null && TexturesServer.TextureLoad.ArrayTexture != null)
                {
                    TexturesServer.GenerateUnityTexture();
                    serverFiles = TexturesServer.GenerateTexture;
                    LoadList(serverFiles);
                }
            }
            while (true)
            {
                if (TexturesServer.IsLoadEnd)
                {
                    TexturesServer.GenerateUnityTexture();
                    serverFiles = TexturesServer.GenerateTexture;
                    LoadList(serverFiles);
                    TexturesServer.SaveCurrentFile();
                    break;
                }
                if (!TexturesServer.IsLoad && !TexturesServer.IsLoadEnd)
                {
                    TexturesServer.Start();
                }
                yield return new WaitForSecondsRealtime(2.0f);
            }
            yield break;
        }

        private void OpenPanle()
        {
            programUI.OpenCenterPanel(this);
        }

        private void LoadFile() {
            if (takeFile.Texture != null)
            {
                EditModel.Singleton.CurrentModel.LoadTexture(takeFile.Texture);
                ClosePanel();
            }
        }

        private void LoadList(TextureFile[] files)
        {
            for (int i = 0; i < itemFiles.Count; i++)
            {
                Destroy(itemFiles[i].gameObject);
            }
            itemFiles.Clear();
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Name.IndexOf(inputField.text) > -1)
                {
                    ItemFile item = Instantiate(prefabItemFile, content);
                    item.Initialized(files[i].Texture, files[i].Name);
                    itemFiles.Add(item);
                    item.Click += SetLoadFile;
                }
            }
            if (itemFiles.Count > 0) {
                takeFile.gameObject.SetActive(true);
                SetLoadFile(itemFiles[0]);
            }
            else{
                takeFile.gameObject.SetActive(false);
            }
        }

        protected override void OpenCallBack()
        {
            panel.SetActive(true);
            LoadList(serverFiles);
        }

        private void SetLoadFile(ItemFile item)
        {
            Vector2 size = new Vector2(item.Texture.width, item.Texture.height);
            takeFile.Initialized(item.Texture, item.Text);
        }

        protected override void CloseCallBack()
        {
            panel.SetActive(false);
        }
    }
}
