﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Program.UI
{
    public class DownLayer
    {
        public event Action EditAction;

        public bool Enable
        {
            get
            {
                return _enable;
            }
            set
            {
                _enable = value;
                if (EditAction != null)
                    EditAction();
            }
        }

        bool _enable = false;
    }
}