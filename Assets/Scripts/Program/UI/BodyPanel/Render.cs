﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Program.UI
{

    public class Render
    {
        public event Action StartRenderEvent;

        public void StartRender()
        {
            if (StartRenderEvent != null)
            {
                StartRenderEvent();
            }
        }
    }
}