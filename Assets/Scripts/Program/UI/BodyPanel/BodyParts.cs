﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Program.UI
{
    public class BodyParts
    {
        public event Action EditBodyParts;

        public bool IsHead
        {
            get
            {
                return _isHead;
            }
            set
            {
                _isHead = value;
                EditBody();
            }
        }

        bool _isHead = true;

        public bool IsBody
        {
            get
            {
                return _isBody;
            }
            set
            {
                _isBody = value;
                EditBody();
            }
        }

        bool _isBody = true;

        public bool IsLHand
        {
            get
            {
                return _isLHand;
            }
            set
            {
                _isLHand = value;
                EditBody();
            }
        }

        bool _isLHand = true;

        public bool IsRHand
        {
            get
            {
                return _isRHand;
            }
            set
            {
                _isRHand = value;
                EditBody();
            }
        }

        bool _isRHand = true;

        public bool IsLFut
        {
            get
            {
                return _isLFut;
            }
            set
            {
                _isLFut = value;
                EditBody();
            }
        }

        bool _isLFut = true;

        public bool IsRFut
        {
            get
            {
                return _isRFut;
            }
            set
            {
                _isRFut = value;
                EditBody();
            }
        }

        bool _isRFut = true;



        private void EditBody()
        {
            if (EditBodyParts != null)
                EditBodyParts();
        }

    }
}