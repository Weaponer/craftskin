﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Program.UI
{
    public class BodyPanel : MonoBehaviour
    {
        [SerializeField] private Button renderButton;

        [SerializeField] private Button layerDownButton;

        [SerializeField] private Button openUV;

        [Space(4)]
        [SerializeField] private Button headButton;

        [SerializeField] private Image headImage;

        [Space(4)]
        [SerializeField] private Button bodyButton;

        [SerializeField] private Image bodyImage;

        [Space(4)]
        [SerializeField] private Button lhandButton;

        [SerializeField] private Image lhandImage;

        [Space(4)]
        [SerializeField] private Button rhandButton;

        [SerializeField] private Image rhandImage;

        [Space(4)]
        [SerializeField] private Button lfutButton;

        [SerializeField] private Image lfutImage;

        [Space(4)]
        [SerializeField] private Button rfutButton;

        [SerializeField] private Image rfutImage;


        public static DownLayer LayerDown { get; private set; }

        public static BodyParts Parts { get; private set; }

        public static Render StartRender { get; private set; }

        public static UVPanel UVEditor { get; private set; }

        public static BodyPanel Singleton { get; private set; }

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
            }
            else
            {
                Singleton = this;
            }

            LayerDown = new DownLayer();

            Parts = new BodyParts();

            StartRender = new Render();

            UVEditor = new UVPanel();

            openUV.onClick.AddListener(delegate { UVEditor.Open(); });

            renderButton.onClick.AddListener(delegate { StartRender.StartRender(); });

            SetOptionBodyParts();
        }

        #region PratsBody
        private void SetOptionBodyParts()
        {
            layerDownButton.onClick.AddListener(delegate { LayerDown.Enable = !LayerDown.Enable; });

            headButton.onClick.AddListener(delegate { SetHead(); headImage.gameObject.SetActive(!Parts.IsHead); });
            headImage.gameObject.SetActive(!Parts.IsHead);

            bodyButton.onClick.AddListener(delegate { SetBody(); bodyImage.gameObject.SetActive(!Parts.IsBody); });
            bodyImage.gameObject.SetActive(!Parts.IsBody);

            lhandButton.onClick.AddListener(delegate { SetLHand(); lhandImage.gameObject.SetActive(!Parts.IsLHand); });
            lhandImage.gameObject.SetActive(!Parts.IsLHand);

            rhandButton.onClick.AddListener(delegate { SetRHand(); rhandImage.gameObject.SetActive(!Parts.IsRHand); });
            rhandImage.gameObject.SetActive(!Parts.IsRHand);

            lfutButton.onClick.AddListener(delegate { SetLFut(); lfutImage.gameObject.SetActive(!Parts.IsLFut); });
            lfutImage.gameObject.SetActive(!Parts.IsLFut);

            rfutButton.onClick.AddListener(delegate { SetRFut(); rfutImage.gameObject.SetActive(!Parts.IsRFut); });
            rfutImage.gameObject.SetActive(!Parts.IsRFut);
        }

        private void SetHead()
        {
            Parts.IsHead = !Parts.IsHead;
        }

        private void SetBody()
        {
            Parts.IsBody = !Parts.IsBody;
        }

        private void SetLHand()
        {
            Parts.IsLHand = !Parts.IsLHand;
        }

        private void SetRHand()
        {
            Parts.IsRHand = !Parts.IsRHand;
        }

        private void SetLFut()
        {
            Parts.IsLFut = !Parts.IsLFut;
        }

        private void SetRFut()
        {
            Parts.IsRFut = !Parts.IsRFut;
        }

        #endregion

        private void OnDestroy()
        {
            LayerDown = null;
            Parts = null;
            StartRender = null;

            Singleton = null;
        }
    }
}