﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


namespace Program.UI
{
    public class LoadMenu : MonoBehaviour
    {
        [SerializeField] private Button loadMenu;

        private void Start()
        {
            loadMenu.onClick.AddListener(delegate { SceneManager.LoadScene(0); });
        }
    }
}