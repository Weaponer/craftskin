﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.UI
{
    public class EditText : MonoBehaviour
    {

        [SerializeField] private Text text;

        [SerializeField] private string russian;

        [SerializeField] private string english;
        private void OnEnable()
        {
            ControllerLaguage.EditLanguage += Edit;
            Edit();
        }

        private void Edit()
        {
            if (ControllerLaguage.isActive)
            {
                text.text = english;
            }
            else
            {
                text.text = russian;
            }
        }

        public void SetParams(string russian, string english)
        {
            this.russian = russian;
            this.english = english;
            Edit();
        }

        private void OnDisable()
        {
            ControllerLaguage.EditLanguage -= Edit;
        }
    }

}