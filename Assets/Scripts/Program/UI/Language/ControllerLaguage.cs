﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Program.Option;


namespace Program.UI
{
    public class ControllerLaguage : OptionObject
    {
        [SerializeField] private Button button;

        public static event Action EditLanguage;

        public static bool isActive { get; private set; }

        private void Start()
        {
            button.onClick.AddListener(delegate { Edit(); });
            if (EditLanguage != null)
                EditLanguage();
        }

        private void Edit()
        {
            isActive = !isActive;
            if (EditLanguage != null)
                EditLanguage();
        }


        public override object GetOption()
        {
            return isActive;
        }

        public override void SetOption(object option)
        {
            isActive = (bool)option;
            if (EditLanguage != null)
                EditLanguage();
        }
    }
}
