﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.UI
{
    public class DefaultCamera : MonoBehaviour
    {
        public static event Action ClickButton;

        [SerializeField] private Button button;

        private void Start()
        {
            button.onClick.AddListener(delegate { if (ClickButton != null) ClickButton(); });
        }
    }
}
