﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Program.UI
{
    public class CenterPanel : MonoBehaviour
    {

        public bool IsOpen { get; private set; }
        public void OpenPanel()
        {
            IsOpen = true;
            OpenCallBack();
        }

        protected virtual void OpenCallBack()
        {

        }

        public void ClosePanel()
        {
            IsOpen = false;
            CloseCallBack();
        }

        protected virtual void CloseCallBack()
        {

        }
    }
}