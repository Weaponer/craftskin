﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.UI
{

    public class ProgramUI : MonoBehaviour
    {
        [SerializeField] private CanvasScaler canvasScaler;

        private CenterPanel centerPanel;

        public void Set()
        {
            // CorrectCanvance.SetScale(canvasScaler, new Vector2(Screen.currentResolution.width, Screen.currentResolution.height));
           // canvasScaler.matchWidthOrHeight = 0;
            
        }

        public void OpenCenterPanel(CenterPanel centerPanel)
        {
            if (this.centerPanel == null || !this.centerPanel.IsOpen)
            {
                this.centerPanel = centerPanel;
                centerPanel.OpenPanel();
            }
        }


        private static class CorrectCanvance
        {
            public static void SetScale(CanvasScaler canvas, Vector2 scale)
            {
                canvas.referenceResolution = scale;
            }
        }
    }

}