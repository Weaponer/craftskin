﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Program.Editor;
using System;

namespace Program.UI
{
    public class UVPanel
    {

        public event Action EditUV;

        public bool IsActive { get; private set; }

        private UV currentUV;


        public void Open()
        {
            if (currentUV != null)
            {
                currentUV.Close();
                if (!currentUV.IsOpen)
                {
                    IsActive = false;
                }
            }
            if (EditModel.Singleton.CurrentModel.GetUv() != null)
            {
                currentUV = EditModel.Singleton.CurrentModel.GetUv();
                currentUV.Open();
                currentUV.SetTexture(EditModel.Singleton.CurrentModel.GetTexture());
                if (currentUV.IsOpen)
                {
                    IsActive = true;
                }
            }
            if (EditUV != null)
            {
                EditUV();
            }
        }

        public void Close()
        {
            if (currentUV != null)
            {
                currentUV.Close();
                if (!currentUV.IsOpen)
                {
                    IsActive = false;
                }
            }
            if (EditUV != null)
            {
                EditUV();
            }
        }

        public Vector2 GetMousePostion()
        {
            if (currentUV == null)
            {
                return new Vector2(-1, -1);
            }
            else
            {
                return currentUV.GetPositionMouse();
            }
        }
    }
}