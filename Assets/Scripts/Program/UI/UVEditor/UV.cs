﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Program.UI
{
    public abstract class UV : CenterPanel
    {
        [SerializeField] private ProgramUI programUI;

        [SerializeField] private Button close;

        [SerializeField] protected GameObject basePanel;

        private void Start()
        {
            close.onClick.AddListener(delegate { ClickClose(); });
        }

        public void Open()
        {
            programUI.OpenCenterPanel(this);
        }

        private void ClickClose()
        {
            BodyPanel.UVEditor.Close();
        }

        public void Close()
        {
            ClosePanel();
        }

        protected override void OpenCallBack()
        {
            basePanel.SetActive(true);

        }

        protected override void CloseCallBack()
        {
            basePanel.SetActive(false);
        }

        public abstract Vector2 GetPositionMouse();

        public abstract void SetTexture(Texture2D texture);
    }
}