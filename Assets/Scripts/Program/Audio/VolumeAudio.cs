﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Program.Option;

namespace Program.Audio
{
    public class VolumeAudio : OptionObject
    {
        public static float Volume = 0.5f;

        public static event System.Action Editor;

        [SerializeField] private Slider slider;

        private void Start()
        {
            slider.onValueChanged.AddListener(delegate { SetParams(); });
            SetOption(Volume);
        }

        private void SetParams()
        {
            Volume = slider.value;
            if (Editor != null)
            {
                Editor();
            }
        }

        public override object GetOption()
        {
            return slider.value;
        }

        public override void SetOption(object option)
        {
            slider.SetValueWithoutNotify((float)option);
            SetParams();
        }
    }
}