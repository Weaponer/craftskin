﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Program.Audio
{
    public class SetVolume : MonoBehaviour
    {
        [SerializeField] private AudioSource audioSource;

        [SerializeField] private float volume;
        void Start()
        {
            audioSource.volume = volume * VolumeAudio.Volume;
            VolumeAudio.Editor += Edit;
        }

        private void Edit()
        {
            audioSource.volume = volume * VolumeAudio.Volume;
        }

        private void OnDestroy()
        {
            VolumeAudio.Editor -= Edit;
        }

    }
}