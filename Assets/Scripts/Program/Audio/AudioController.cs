﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Program.Audio
{
    public class AudioController : MonoBehaviour
    {
        [SerializeField] private AudioSource clickButton;

        [SerializeField] private AudioSource clickColorButton;

        [SerializeField] private AudioSource paint;

        public static AudioController Singleton { get; private set; }

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                Singleton = this;
            }
        }


        public void PlayOnClickButton()
        {
            clickButton.PlayOneShot(clickButton.clip);
        }

        public void PlayOnClickColorButton()
        {
            clickColorButton.PlayOneShot(clickColorButton.clip);
        }

        public void PlayPaint()
        {
            if (!paint.isPlaying)
                paint.Play();
        }

        public void OffPaint()
        {
            if (paint.isPlaying)
                paint.Stop();
        }

        private void OnDestroy()
        {
            if (Singleton == this)
            {
                Singleton = null;
            }
        }
    }
}