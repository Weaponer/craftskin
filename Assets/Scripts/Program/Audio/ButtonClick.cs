﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.Audio
{
    public class ButtonClick : MonoBehaviour
    {
        [SerializeField] private Button button;
        private void Start()
        {
            button.onClick.AddListener(delegate { Audio.AudioController.Singleton.PlayOnClickButton(); });
        }
    }
}