﻿using Program.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Program.Editor;

namespace Program
{
    public class ProgramStart : MonoBehaviour
    {
        [SerializeField] private ProgramUI programUI;

        public static ProgramStart Singleton { get; private set; }

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
            }
            else
            {
                Singleton = this;
            }
        }

        private void Start()
        {
            programUI.Set();
            Texture2D texture2D = SaveTextureScene.GetTexture();
            if (texture2D)
            {
                Debug.Log(EditModel.Singleton.SkinModel);
                EditModel.Singleton.SkinModel.LoadTexture(texture2D);
            }
        }


        private void OnDestroy()
        {
            Singleton = null;
        }
    }
}