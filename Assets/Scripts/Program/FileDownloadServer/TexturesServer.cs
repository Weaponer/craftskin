using System;
using System.Collections;
using System.Collections.Generic;
using FilerServer;
using FilerServer.Base;

namespace Program.Editor
{
    public static class TexturesServer
    {
        public static FileTextures TextureLoad;

        public static TextureFile[] GenerateTexture;

        public static bool IsLoadEnd;

        public static bool IsLoad;


        private static bool isSave;

        public static void Start()
        {
            IsLoad = true;
            TextureLoad = FileTextures.LoadFile();
            if (TextureLoad == null)
            {
                BaseConnect.LoadListTexture(LoadTextureAsync, EndLoadNoServer);
            }
            else
            {
                BaseConnect.LoadNumUpdateTexture(IsCurrentUpdateAsync, EndLoadNoServer);
            }
        }

        //If file exists
        private static void IsCurrentUpdateAsync(uint num)//Check version texture
        {
            if (num == TextureLoad.NumUpdate)
            {
                EndLoad();
            }
            else
            {
                TextureLoad.NumUpdate = num;
                BaseConnect.LoadListTexture(LoadNewVersionTextureAsync, EndLoadNoServer);
            }
        }

        private static void LoadNewVersionTextureAsync(Texture[] textures)//Load list texture
        {
            TextureLoad.ArrayTexture = textures;
            Array.Reverse(TextureLoad.ArrayTexture);
            isSave = true;
            EndLoad();
        }

        //If no NewsFile
        private static void LoadTextureAsync(Texture[] textures)//Load list texture
        {
            TextureLoad = new FileTextures();
            TextureLoad.ArrayTexture = textures;
            Array.Reverse(TextureLoad.ArrayTexture);
            BaseConnect.LoadNumUpdateTexture(IsLoadAsync, EndLoadNoServer);
        }

        private static void IsLoadAsync(uint num)//Load num update
        {
            TextureLoad.NumUpdate = num;
            isSave = true;
            EndLoad();
        }


        //End
        private static void EndLoad()
        {
            IsLoadEnd = true;
            IsLoad = false;
        }

        private static void EndLoadNoServer()
        {
            if (TextureLoad != null)
            {
                IsLoadEnd = true;
                IsLoad = false;
            }
            else
            {
                IsLoadEnd = false;
                IsLoad = false;
            }
        }

        public static void SaveCurrentFile()
        {
            if (isSave)
            {
                FileTextures.SaveFile(TextureLoad);
            }
        }

        public static void GenerateUnityTexture()
        {
            List<TextureFile> textures = new List<TextureFile>();
            for (int i = 0; i < TextureLoad.ArrayTexture.Length; i++)
            {
                TextureFile texture = new TextureFile();
                texture.Name = TextureLoad.ArrayTexture[i].Name;
                UnityEngine.Texture2D tx2D = new UnityEngine.Texture2D(Texture.SizeTexture, Texture.SizeTexture, UnityEngine.TextureFormat.RGBA32, false);
                tx2D.filterMode = UnityEngine.FilterMode.Point;
                for (int i2 = 0; i2 < Texture.SizeTexture; i2++)
                {
                    for (int i3 = 0; i3 < Texture.SizeTexture; i3++)
                    {
                        int numPix = i2 * Texture.SizeTexture + i3;
                        Texture t = TextureLoad.ArrayTexture[i];
                        tx2D.SetPixel(i3, Texture.SizeTexture - i2 - 1, new UnityEngine.Color((float)t.Pixels[numPix].r / byte.MaxValue, (float)t.Pixels[numPix].g / byte.MaxValue,
                            (float)t.Pixels[numPix].b / byte.MaxValue, (float)t.Pixels[numPix].a / byte.MaxValue));
                    }
                }
                tx2D.Apply();
                texture.Texture = tx2D;
                textures.Add(texture);
            }
            GenerateTexture = textures.ToArray();
        }

        public static void ClearVariable()
        {
            GenerateTexture = null;
            TextureLoad = null;
            IsLoadEnd = false;
            IsLoad = false;
            isSave = false;
        }
    }
}