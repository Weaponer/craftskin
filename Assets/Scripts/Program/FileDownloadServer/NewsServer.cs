using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FilerServer;
using FilerServer.Base;
using System;

namespace Program.Menu
{
    public static class NewsServer
    {
        public static FileNews NewsLoad;

        public static bool IsLoadEnd;

        public static bool IsLoad;


        private static bool isSave;

        public static void Start()
        {
            IsLoad = true;
            NewsLoad = FileNews.LoadFile();
            if (NewsLoad == null)
            {
                BaseConnect.LoadListNews(LoadNewsAsync, EndLoadNoServer);
            }
            else
            {
                BaseConnect.LoadNumUpdateNews(IsCurrentUpdateAsync, EndLoadNoServer);
            }
        }
        //If file exists
        private static void IsCurrentUpdateAsync(uint num)//Check version news
        {
            if (num == NewsLoad.NumUpdate)
            {
                EndLoad();
            }
            else
            {
                NewsLoad.NumUpdate = num;
                BaseConnect.LoadListNews(LoadNewVersionNewsAsync, EndLoadNoServer);
            }
        }

        private static void LoadNewVersionNewsAsync(News[] news)//Load list news
        {
            NewsLoad.ArrayNews = news;
            Array.Reverse(NewsLoad.ArrayNews);
            isSave = true;
            EndLoad();
        }

        //If no NewsFile
        private static void LoadNewsAsync(News[] news)//Load list news
        {
            NewsLoad = new FileNews();
            NewsLoad.ArrayNews = news;
            Array.Reverse(NewsLoad.ArrayNews);
            BaseConnect.LoadNumUpdateNews(IsLoadAsync, EndLoadNoServer);
        }

        private static void IsLoadAsync(uint num)//Load num update
        {
            NewsLoad.NumUpdate = num;
            isSave = true;
            EndLoad();
        }


        //End
        private static void EndLoad()
        {
            IsLoadEnd = true;
            IsLoad = false;
        }

        private static void EndLoadNoServer()
        {
            if (NewsLoad != null)
            {
                IsLoadEnd = true;
                IsLoad = false;
            }
            else
            {
                IsLoadEnd = false;
                IsLoad = false;
            }
        }

        public static void SaveCurrentFile()
        {
            if (isSave)
            {   
                FileNews.SaveFile(NewsLoad);
            }
        }

        public static void ClearVariable()
        {
            NewsLoad = null;
            IsLoadEnd = false;
            IsLoad = false;
            isSave = false;
        }
    }
}