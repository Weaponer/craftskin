using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FilerServer;


namespace Program.Menu
{
    [Serializable]
    public class FileNews
    {

        public const string Path = "/loadserver.nws";

        public uint NumUpdate;

        public News[] ArrayNews;


#if UNITY_EDITOR
        private static string path = Application.dataPath + Path;
#else
        private static string path = Application.persistentDataPath + Path;
#endif
        public static FileNews LoadFile()
        {
            if (File.Exists(path))
            {

                FileStream fileStream = File.Open(path, FileMode.Open);
                BinaryFormatter f = new BinaryFormatter();

                FileNews fileNews = (FileNews)f.Deserialize(fileStream);
                fileStream.Close();
                return fileNews;
            }
            else
            {
                return null;
            }
        }

        public static void SaveFile(FileNews fileNews)
        {
            using (FileStream fileStream = File.Open(path, FileMode.OpenOrCreate))
            {
                BinaryFormatter f = new BinaryFormatter();

                f.Serialize(fileStream, fileNews);
                fileStream.Close();
            }
        }
    }
}