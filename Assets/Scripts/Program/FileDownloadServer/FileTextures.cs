using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Collections.Generic;
using FilerServer;

namespace Program.Editor
{
    [Serializable]
    public class FileTextures
    {
        public const string Path = "/loadserver.txr";

        public uint NumUpdate;

        public Texture[] ArrayTexture;

#if UNITY_EDITOR
        private static string path = UnityEngine.Application.dataPath + Path;
#else
        private static string path = UnityEngine.Application.persistentDataPath + Path;
#endif
        public static FileTextures LoadFile()
        {
            if (File.Exists(path))
            {
                FileStream fileStream = File.Open(path, FileMode.Open);
                BinaryFormatter f = new BinaryFormatter();

                FileTextures fileNews = (FileTextures)f.Deserialize(fileStream);
                fileStream.Close();
                return fileNews;
            }
            else
            {
                return null;
            }
        }

        public static void SaveFile(FileTextures fileNews)
        {
            using (FileStream fileStream = File.Open(path, FileMode.OpenOrCreate))
            {
                BinaryFormatter f = new BinaryFormatter();

                f.Serialize(fileStream, fileNews);
                fileStream.Close();
            }
        }
    }
}