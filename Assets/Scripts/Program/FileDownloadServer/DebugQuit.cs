using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Program.Menu;
using Program.Editor;

namespace Program
{
    public class DebugQuit : MonoBehaviour
    {
        private void OnApplicationQuit()
        {
#if UNITY_EDITOR
            NewsServer.ClearVariable();
            TexturesServer.ClearVariable();
#endif
        }
    }
}