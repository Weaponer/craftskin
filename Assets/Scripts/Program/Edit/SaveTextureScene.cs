﻿using UnityEngine;

namespace Program
{
    public static class SaveTextureScene
    {
        private static byte[] textureByte;
        static Vector2 size;
        public static void SaveTexutre(Texture2D texture)
        {
            size.x = texture.width;
            size.y = texture.height;
            textureByte = texture.EncodeToPNG();
        }

        public static Texture2D GetTexture()
        {
            if (textureByte != null && textureByte.Length > 0)
            {
                Texture2D texture = new Texture2D((int)size.x, (int)size.y, TextureFormat.RGBA32, false);
                texture.LoadImage(textureByte);
                texture.filterMode = FilterMode.Point;
                texture.Apply();
                return texture;
            }
            else
            {
                return null;
            }
        }
    }
}