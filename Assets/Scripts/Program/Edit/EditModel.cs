﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Program.UI;

namespace Program.Editor
{
    public class EditModel : MonoBehaviour
    {
        public Model SkinModel
        {
            get
            {
                return skinModel;
            }
        }

        [SerializeField] private Skin skinModel;

        public Model ItemModel
        {
            get
            {
                return itemModel;
            }
        }

        [SerializeField] private Item itemModel;

        public Model BlockModel
        {
            get
            {
                return blockModel;
            }
        }

        [SerializeField] private Block blockModel;

        public Model CurrentModel { get; private set; }


        public static EditModel Singleton { get; private set; }


        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
            }
            else
            {
                Singleton = this;
            }


        }

        private void Start()
        {
            TakeSkin();

            MenuPanel.Singleton.EditBlock += TakeBlock;
            MenuPanel.Singleton.EditItem += TakeItem;
            MenuPanel.Singleton.EditPlayer += TakeSkin;

        }

        private void TakeSkin()
        {
            CurrentModel = SkinModel;
            
            CurrentModel.ActiveEditor();

            BlockModel.DisableEditor();
            ItemModel.DisableEditor();
        }

        private void TakeItem()
        {
            CurrentModel = ItemModel;

            CurrentModel.ActiveEditor();

            BlockModel.DisableEditor();
            SkinModel.DisableEditor();
        }

        private void TakeBlock()
        {
            CurrentModel = BlockModel;

            CurrentModel.ActiveEditor();

            SkinModel.DisableEditor();
            ItemModel.DisableEditor();
        }

        private void OnDestroy()
        {
            Singleton = null;
        }
    }
}