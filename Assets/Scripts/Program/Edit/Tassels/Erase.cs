﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Program.UI;

namespace Program.Editor
{
    public class Erase : Tassel
    {
        protected override void SetParams()
        {
            SetTypeInstrument(Instrument.Erase);
            SetIsSaveHistory(true);
        }

        protected override void Work(RaycastHit hit)
        {
            Model current = EditModel.Singleton.CurrentModel;
            Vector2Int pos = GetPixels.GetPixel(hit, current.GetTexture().width, current.GetTexture().height);
            current.UpdateTexture(new Vector2Int[1] { pos }, new Color[1] { new Color(1, 1, 1, 0) });
        }

        protected override void WorkUV(Vector2 pos)
        {
            Model current = EditModel.Singleton.CurrentModel;
            current.UpdateTexture(new Vector2Int[1] { new Vector2Int((int)pos.x, (int)pos.y) }, new Color[1] { new Color(1, 1, 1, 0) });
        }
    }
}