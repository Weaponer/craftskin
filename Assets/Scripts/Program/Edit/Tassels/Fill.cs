﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Program.UI;

namespace Program.Editor
{
    public class Fill : Tassel
    {
        protected override void SetParams()
        {
            SetTypeInstrument(Instrument.FillColor);
            SetIsSaveHistory(true);
        }

        protected override void Work(RaycastHit hit)
        {
            Model current = EditModel.Singleton.CurrentModel;
            Vector2Int[] pos = GetPixels.GetPixelsFill(hit, current.GetTexture().width, current.GetTexture().height);
            Color[] col = new Color[pos.Length];
            for (int i = 0; i < col.Length; i++)
            {
                col[i] = PalitraPanel.ColorList.GetCurrentItem().GetColor();
            }
            current.UpdateTexture(pos, col);
        }

        protected override void WorkUV(Vector2 pos)
        {
            Model current = EditModel.Singleton.CurrentModel;
            Vector2Int[] poss = GetPixels.GetPixelsFill(pos, current.GetTexture().width, current.GetTexture().height, current.GetMeshs());
            if (poss != null && poss.Length > 0)
            {
                Color[] col = new Color[poss.Length];
                for (int i = 0; i < col.Length; i++)
                {
                    col[i] = PalitraPanel.ColorList.GetCurrentItem().GetColor();
                }
                current.UpdateTexture(poss, col);
            }
        }
    }
}