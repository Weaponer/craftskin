﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Program.UI;
using UnityEngine.EventSystems;

namespace Program.Editor
{
    public abstract class Tassel : MonoBehaviour
    {
        [SerializeField] protected CameraMove moveCamera;
        [SerializeField] protected EventSystem eventSystem;
        [SerializeField] protected Camera camera;

        [SerializeField] protected LayerMask mask;

        protected bool isWriteHistory;

        protected bool isSaveHistory;

        public Instrument TypeInstrument { get; private set; }

        private void Start()
        {
            SetParams();
        }

        protected void SetTypeInstrument(Instrument instrument)
        {
            TypeInstrument = instrument;
        }

        protected void SetIsSaveHistory(bool isSave)
        {
            isSaveHistory = isSave;
        }

        protected abstract void SetParams();

        private void Update()
        {
            if (PalitraPanel.Instrument.InstrumentTake == TypeInstrument && !moveCamera.IsBlock && (eventSystem.currentSelectedGameObject == null || BodyPanel.UVEditor.IsActive))
            {
                bool isActive = false;
                if (Input.touchCount == 1)
                {
                    Ray ray = camera.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (!BodyPanel.UVEditor.IsActive && Physics.Raycast(ray, out hit, float.MaxValue, mask))
                    {
                        if (Input.GetMouseButtonDown(0) && isSaveHistory)
                        {
                            isWriteHistory = true;
                            EditModel.Singleton.CurrentModel.StartWriteHistory();
                        }
                        isActive = true;
                        Work(hit);
                    }
                    else if (BodyPanel.UVEditor.IsActive && BodyPanel.UVEditor.GetMousePostion() != new Vector2(-1, -1))
                    {
                        if (Input.GetMouseButtonDown(0) && isSaveHistory)
                        {
                            isWriteHistory = true;
                            EditModel.Singleton.CurrentModel.StartWriteHistory();
                        }
                        isActive = true;
                        WorkUV(BodyPanel.UVEditor.GetMousePostion());
                    }
                }

                if (isActive)
                {
                    Audio.AudioController.Singleton.PlayPaint();
                }
                else
                {
                    Audio.AudioController.Singleton.OffPaint();
                }

                if ((Input.GetMouseButtonUp(0) || Input.touchCount != 1) && isSaveHistory)
                {
                    isWriteHistory = false;
                    EditModel.Singleton.CurrentModel.StopWriteHistory();
                }
            }
            else if (isWriteHistory && isSaveHistory)
            {
                isWriteHistory = false;
                EditModel.Singleton.CurrentModel.StopWriteHistory();
            }
        }

        protected abstract void Work(RaycastHit hit);

        protected abstract void WorkUV(Vector2 pos);
    }
}