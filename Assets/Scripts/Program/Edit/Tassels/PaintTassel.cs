﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Program.UI;

namespace Program.Editor
{
    public class PaintTassel : Tassel
    {
        protected override void SetParams()
        {
            SetTypeInstrument(Instrument.Paint);
            SetIsSaveHistory(true);
        }

        protected override void Work(RaycastHit hit)
        {

            Model current = EditModel.Singleton.CurrentModel;
            Vector2Int pos = GetPixels.GetPixel(hit, current.GetTexture().width, current.GetTexture().height);
            current.UpdateTexture(new Vector2Int[1] { pos }, new Color[1] { PalitraPanel.ColorList.GetCurrentItem().GetColor() });

        }

        protected override void WorkUV(Vector2 pos)
        {
            Model current = EditModel.Singleton.CurrentModel;
            current.UpdateTexture(new Vector2Int[1] { new Vector2Int((int)pos.x, (int)pos.y) }, new Color[1] { PalitraPanel.ColorList.GetCurrentItem().GetColor() });
        }
    }
}
