﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Program.UI;

namespace Program.Editor
{
    public class TakeColor : Tassel
    {
        protected override void SetParams()
        {
            SetTypeInstrument(Instrument.TakeColor);
            SetIsSaveHistory(false);
        }

        protected override void Work(RaycastHit hit)
        {

            Model current = EditModel.Singleton.CurrentModel;
            Texture2D texture = current.GetTexture();
            Color color = texture.GetPixel((int)(hit.textureCoord.x * texture.width), (int)(hit.textureCoord.y * texture.height));
            PalitraPanel.ColorList.EditColor(color);

        }

        protected override void WorkUV(Vector2 pos)
        {
            Model current = EditModel.Singleton.CurrentModel;
            Texture2D texture = current.GetTexture();
            Color color = texture.GetPixel((int)pos.x, (int)pos.y);
            PalitraPanel.ColorList.EditColor(color);
        }
    }
}