﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Program.Editor
{
    public class Block : Model
    {
        public override float ScaleFactor()
        {
            return 1f;
        }

        protected override void ActiveEditorCallBack()
        {
            baseModel.gameObject.SetActive(true);
        }

        public override bool IsRotateCamera()
        {
            return true;
        }

        public override Vector3 GetDefaultRotate()
        {
            return Vector3.zero;
        }

        protected override void DisableEditorCallBack()
        {
            baseModel.gameObject.SetActive(false);
        }

        public override Vector2 GetSizeTexture()
        {
            return new Vector2(16, 16);
        }
    }
}