﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Program.UI;

namespace Program.Editor
{
    public class Skin : Model
    {
        [SerializeField] private Transform head;
        [SerializeField] private Transform headD;

        [Space(4)]
        [SerializeField] private Transform body;
        [SerializeField] private Transform bodyD;

        [Space(4)]
        [SerializeField] private Transform lhand;
        [SerializeField] private Transform lhandD;

        [Space(4)]
        [SerializeField] private Transform rhand;
        [SerializeField] private Transform rhandD;

        [Space(4)]
        [SerializeField] private Transform lfut;
        [SerializeField] private Transform lfutD;

        [Space(4)]
        [SerializeField] private Transform rfut;
        [SerializeField] private Transform rfutD;

        protected override void ActiveEditorCallBack()
        {
            baseModel.gameObject.SetActive(true);

            BodyPanel.Parts.EditBodyParts += UpdateMesh;
            BodyPanel.LayerDown.EditAction += UpdateMesh;
            UpdateMesh();
        }

        protected override void DisableEditorCallBack()
        {
            BodyPanel.Parts.EditBodyParts -= UpdateMesh;
            BodyPanel.LayerDown.EditAction -= UpdateMesh;
            baseModel.gameObject.SetActive(false);
        }

        public override Vector3 GetDefaultRotate()
        {
            return Vector3.zero;
        }

        public override bool IsRotateCamera()
        {
            return true;
        }

        private void UpdateMesh()
        {
            if (BodyPanel.LayerDown.Enable)
            {
                SetActiveDown(true);
                SetActiveUp(false);
            }
            else
            {
                SetActiveDown(true);
                SetActiveUp(true);
            }

            if (!BodyPanel.Parts.IsHead)
            {
                headD.gameObject.SetActive(false);
                head.gameObject.SetActive(false);
            }

            if (!BodyPanel.Parts.IsBody)
            {
                bodyD.gameObject.SetActive(false);
                body.gameObject.SetActive(false);
            }

            if (!BodyPanel.Parts.IsLHand)
            {
                lhandD.gameObject.SetActive(false);
                lhand.gameObject.SetActive(false);
            }

            if (!BodyPanel.Parts.IsRHand)
            {
                rhandD.gameObject.SetActive(false);
                rhand.gameObject.SetActive(false);
            }

            if (!BodyPanel.Parts.IsLFut)
            {
                lfutD.gameObject.SetActive(false);
                lfut.gameObject.SetActive(false);
            }

            if (!BodyPanel.Parts.IsRFut)
            {
                rfutD.gameObject.SetActive(false);
                rfut.gameObject.SetActive(false);
            }
        }

        private void SetActiveDown(bool isActive)
        {
            headD.gameObject.SetActive(isActive);
            bodyD.gameObject.SetActive(isActive);
            lhandD.gameObject.SetActive(isActive);
            rhandD.gameObject.SetActive(isActive);
            lfutD.gameObject.SetActive(isActive);
            rfutD.gameObject.SetActive(isActive);
        }

        private void SetActiveUp(bool isActive)
        {
            head.gameObject.SetActive(isActive);
            body.gameObject.SetActive(isActive);
            lhand.gameObject.SetActive(isActive);
            rhand.gameObject.SetActive(isActive);
            lfut.gameObject.SetActive(isActive);
            rfut.gameObject.SetActive(isActive);
        }

        public override Vector3 Position()
        {
            return transform.position + new Vector3(0, -1.071f, 0);
        }

        public override Vector2 GetSizeTexture()
        {
            return new Vector2(64, 64);
        }

        public override float ScaleFactor()
        {
            return 2.3f;
        }
    }
}