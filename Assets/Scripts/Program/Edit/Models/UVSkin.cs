﻿using Program.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.Editor
{
    public class UVSkin : UV
    {
        [SerializeField] private Image image;
        public override Vector2 GetPositionMouse()
        {
            Vector3 pos = image.transform.InverseTransformPoint(Input.GetTouch(0).position);

            if ((pos.x >= 1 && pos.x < image.rectTransform.rect.width) && (pos.y >= 1 && pos.y < image.rectTransform.rect.height))
            {
                pos.x /= image.rectTransform.rect.width;
                pos.y /= image.rectTransform.rect.height;

                pos.x = (int)(pos.x * image.sprite.texture.width);
                pos.y = (int)(pos.y * image.sprite.texture.height);

                return pos;
            }
            else
            {
                return new Vector2(-1, -1);
            }
        }

        public override void SetTexture(Texture2D texture)
        {
            image.sprite = Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f);
        }
    }
}