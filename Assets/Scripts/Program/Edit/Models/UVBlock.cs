﻿using Program.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Program.Editor
{
    public class UVBlock : UV
    {
        [SerializeField] private Image baseImage;

        [SerializeField] private Image[] copyImages;
        public override Vector2 GetPositionMouse()
        {
            Vector3 pos = baseImage.transform.InverseTransformPoint(Input.GetTouch(0).position);

            if ((pos.x >= 1 && pos.x < baseImage.rectTransform.rect.width) && (pos.y >= 1 && pos.y < baseImage.rectTransform.rect.height))
            {
                pos.x /= baseImage.rectTransform.rect.width;
                pos.y /= baseImage.rectTransform.rect.height;

                pos.x = (int)(pos.x * baseImage.sprite.texture.width);
                pos.y = (int)(pos.y * baseImage.sprite.texture.height);

                return pos;
            }
            else
            {
                return new Vector2(-1, -1);
            }
        }

        public override void SetTexture(Texture2D texture)
        {
            Sprite sprite = Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f);
            baseImage.sprite = sprite;
            for (int i = 0; i < copyImages.Length; i++)
            {
                copyImages[i].sprite = sprite;
            }
        }
    }
}
