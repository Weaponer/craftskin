﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Program.UI;

namespace Program.Editor
{
    public class Paint : MonoBehaviour
    {
        [SerializeField] private CameraMove moveCamera;
        [SerializeField] private EventSystem eventSystem;
        [SerializeField] private Camera camera;

        LayerMask mask;

        private void Start()
        {
            mask = LayerMask.GetMask("EditModel");
        }
        private void LateUpdate()
        {
            if (!moveCamera.IsBlock && eventSystem.currentSelectedGameObject == null && Input.GetMouseButton(0))
            {
                if (PalitraPanel.Instrument.InstrumentTake == Instrument.Paint)
                {
                    TasselPaint();
                }
                else if (PalitraPanel.Instrument.InstrumentTake == Instrument.FillColor)
                {
                    FillPaint();
                }
                else if (PalitraPanel.Instrument.InstrumentTake == Instrument.TakeColor)
                {
                    TakeColor();
                }
                else if (PalitraPanel.Instrument.InstrumentTake == Instrument.Erase)
                {
                    Erase();
                }
            }
        }

        private void TasselPaint()
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, float.MaxValue, mask))
            {
                Model current = EditModel.Singleton.CurrentModel;
                Vector2Int pos = GetPixels.GetPixel(hit, current.GetTexture().width, current.GetTexture().height);
                current.UpdateTexture(new Vector2Int[1] { pos }, new Color[1] { PalitraPanel.ColorList.GetCurrentItem().GetColor() });
            }
        }

        private void FillPaint()
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, float.MaxValue, mask))
            {
                Model current = EditModel.Singleton.CurrentModel;
                Vector2Int[] pos = GetPixels.GetPixelsFill(hit, current.GetTexture().width, current.GetTexture().height);
                Color[] col = new Color[pos.Length];
                for (int i = 0; i < col.Length; i++)
                {
                    col[i] = PalitraPanel.ColorList.GetCurrentItem().GetColor();
                }
                current.UpdateTexture(pos, col);
            }
        }

        private void TakeColor()
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, float.MaxValue, mask))
            {
                Model current = EditModel.Singleton.CurrentModel;
                Texture2D texture = current.GetTexture();
                Color color = texture.GetPixel((int)(hit.textureCoord.x * texture.width), (int)(hit.textureCoord.y * texture.height));
                PalitraPanel.ColorList.EditColor(color);
            }
        }

        private void Erase()
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, float.MaxValue, mask))
            {
                Model current = EditModel.Singleton.CurrentModel;
                Vector2Int pos = GetPixels.GetPixel(hit, current.GetTexture().width, current.GetTexture().height);
                current.UpdateTexture(new Vector2Int[1] { pos }, new Color[1] { new Color(1, 1, 1, 0) });
            }
        }
    }
}