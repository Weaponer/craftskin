﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Program.UI;

namespace Program.Editor
{
    public enum TypeControll
    {
        None,
        OneTouch,
        TwoTouch,
    }

    public class CameraMove : MonoBehaviour
    {
        public bool IsBlock
        {
            get
            {
                return isMove;
            }
        }

        [Range(0, 300)]
        [SerializeField] private float speedRotate;

        [Range(0, 2)]
        [SerializeField] private float speedMove;

        [Range(0, 2)]
        [SerializeField] private float speedScale;

        [Range(0, 6)]
        [SerializeField] private float limitScaleMin;

        [Range(0, 6)]
        [SerializeField] private float limitScaleMax;

        [SerializeField] private EventSystem eventSystem;
        [SerializeField] private Camera camera;
        [SerializeField] private Transform cameraPos;

        Vector3 rotate;

        Vector2 startScrenPos = Vector2.zero;
        Vector2 startScrenPos2 = Vector2.zero;

        bool isMove;

        private TypeControll controll = TypeControll.None;

        private void OnEnable()
        {
            DefaultCamera.ClickButton += SetDefaultPosition;
        }

        private void Update()
        {

            TypeControll t;
            if (!isMove && Input.touchCount > 0)
            {
                Ray ray = camera.ScreenPointToRay(Input.GetTouch(0).position);
                LayerMask mask = LayerMask.GetMask("EditModel");
                if (eventSystem.currentSelectedGameObject == null && !Physics.Raycast(ray, float.MaxValue, mask) && Input.GetMouseButtonDown(0))
                {
                    startScrenPos = Vector2.zero;
                    startScrenPos2 = Vector2.zero;
                    isMove = true;
                }
            }
            if (isMove)
            {
                if (Input.touchCount == 1)
                {
                    t = TypeControll.OneTouch;
                }
                else if (Input.touchCount == 2)
                {
                    t = TypeControll.TwoTouch;
                }
                else
                {
                    t = TypeControll.None;
                }
                controll = t;
            }

            if (Input.touchCount == 0 && isMove)
            {
                isMove = false;
                controll = TypeControll.None;
                startScrenPos = Vector2.zero;
                startScrenPos2 = Vector2.zero;
            }
        }

        private void FixedUpdate()
        {
            if (controll == TypeControll.OneTouch)
            {
                if (startScrenPos == Vector2.zero)
                {
                    startScrenPos = Input.GetTouch(0).position;
                }

                Vector2 direct = Input.GetTouch(0).position - startScrenPos;
                startScrenPos = Input.GetTouch(0).position;

                if (EditModel.Singleton.CurrentModel.IsRotateCamera())
                {
                    rotate.x = Mathf.Clamp(rotate.x + direct.y * speedRotate * Time.fixedDeltaTime, -90f, 90f);
                    rotate.y += direct.x * speedRotate * Time.fixedDeltaTime; cameraPos.eulerAngles = rotate;
                    cameraPos.eulerAngles = rotate;
                }
            }
            else if (controll == TypeControll.TwoTouch)
            {
                if (startScrenPos == Vector2.zero)
                {
                    startScrenPos = Input.GetTouch(0).position;
                }
                if (startScrenPos2 == Vector2.zero)
                {
                    startScrenPos2 = Input.GetTouch(1).position;
                }

                Vector2 direct1 = Input.GetTouch(0).position - startScrenPos;
                startScrenPos = Input.GetTouch(0).position;
                Vector2 direct2 = Input.GetTouch(1).position - startScrenPos2;
                startScrenPos2 = Input.GetTouch(1).position;

                if (Vector3.Dot(direct1, direct2) > 0.7f && direct1.magnitude > 2f)
                {
                    cameraPos.position += cameraPos.right * ((direct1.x + direct2.x) / 2) * speedMove * Time.fixedDeltaTime;
                    cameraPos.position += -cameraPos.up * ((direct1.y + direct2.y) / 2) * speedMove * Time.fixedDeltaTime;
                }
                else if (Vector3.Dot(direct1, direct2) < -0.7f)
                {
                    Vector2 d = (Input.GetTouch(1).position - Input.GetTouch(0).position).normalized;
                    if (Vector3.Dot(direct1.normalized, d) > 0)
                    {
                        camera.transform.localPosition = new Vector3(0, 0, Mathf.Clamp(camera.transform.localPosition.z + speedScale * Time.fixedDeltaTime, limitScaleMin, limitScaleMax));
                    }
                    else if (Vector3.Dot(direct1.normalized, d) < 0)
                    {
                        camera.transform.localPosition = new Vector3(0, 0, Mathf.Clamp(camera.transform.localPosition.z - speedScale * Time.fixedDeltaTime, limitScaleMin, limitScaleMax));
                    }
                }
            }
            if (EditModel.Singleton.CurrentModel.IsRotateCamera() == false)
            {
                rotate = EditModel.Singleton.CurrentModel.GetDefaultRotate();
                cameraPos.eulerAngles = rotate;
            }
        }
        public void SetDefaultPosition()
        {
            rotate = Vector3.zero;
            isMove = false;
            startScrenPos = Vector2.zero;
            startScrenPos2 = Vector2.zero;
            cameraPos.eulerAngles = EditModel.Singleton.CurrentModel.GetDefaultRotate();
            cameraPos.position = Vector3.zero;
            camera.transform.localPosition = new Vector3(0, 0, limitScaleMax);
            controll = TypeControll.None;
        }

        private void OnDisable()
        {
            DefaultCamera.ClickButton -= SetDefaultPosition;
        }
    }
}