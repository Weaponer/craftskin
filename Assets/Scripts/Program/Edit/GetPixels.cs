﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Program.Editor
{

    public static class GetPixels
    {
        public static Vector2Int GetPixel(RaycastHit hit, int wightTexture, int heightTexture)
        {
            Vector2 uv = hit.textureCoord;
            uv *= new Vector2(wightTexture, heightTexture);

            return new Vector2Int((int)uv.x, (int)uv.y);
        }

        public static Vector2Int[] GetPixelsFill(Vector2 pos, int wightTexture, int heightTexture, Mesh[] meshes)
        {
            float u = pos.x / wightTexture;
            float v = pos.y / heightTexture;

            for (int i = 0; i < meshes.Length; i++)
            {
                int[] triangle = meshes[i].triangles;
                Vector2[] uv = meshes[i].uv;

                for (int i2 = 0; i2 < triangle.Length / 3; i2++)
                {
                    Vector2 pos1 = uv[triangle[i2 * 3 + 0]];
                    Vector2 pos2 = uv[triangle[i2 * 3 + 1]];
                    Vector2 pos3 = uv[triangle[i2 * 3 + 2]];

                    bool side1 = Vector2.Dot(Vector2.Perpendicular(pos1 - pos2), new Vector2(u, v) - pos1) >= 0;
                    bool side2 = Vector2.Dot(Vector2.Perpendicular(pos2 - pos3), new Vector2(u, v) - pos2) >= 0;
                    bool side3 = Vector2.Dot(Vector2.Perpendicular(pos3 - pos1), new Vector2(u, v) - pos3) >= 0;
                    if (side1 == side2 && side2 == side3)
                    {
                        Triangle[] triangles1 = GetConnectTriangle(triangle[i2 * 3 + 0], triangle[i2 * 3 + 1], triangle[i2 * 3 + 2], meshes[i]);

                        return GetPixelsFromTriangles(triangles1, uv, wightTexture, heightTexture);
                    }
                }
            }

            return null;
        }

        public static Vector2Int[] GetPixelsFill(RaycastHit hit, int wightTexture, int heightTexture)
        {


            MeshCollider meshCollider = hit.collider as MeshCollider;
            if (meshCollider == null || meshCollider.sharedMesh == null)
                return null;

            Mesh mesh = meshCollider.sharedMesh;
            Vector2[] uv = mesh.uv;
            int[] triangles = mesh.triangles;



            Triangle[] triangles1 = GetConnectTriangle(triangles[hit.triangleIndex * 3 + 0], triangles[hit.triangleIndex * 3 + 1], triangles[hit.triangleIndex * 3 + 2], mesh);


            return GetPixelsFromTriangles(triangles1, uv, wightTexture, heightTexture);
        }

        private static Vector2Int[] GetPixelsFromTriangles(Triangle[] triangles, Vector2[] uv, int wightTexture, int heightTexture)
        {
            List<Vector2Int> vectors = new List<Vector2Int>();
            for (int i3 = 0; i3 < triangles.Length; i3++)
            {
                Vector2 p0 = uv[triangles[i3].point1];
                Vector2 p1 = uv[triangles[i3].point2];
                Vector2 p2 = uv[triangles[i3].point3];

                p0 *= new Vector2(wightTexture, heightTexture);
                p1 *= new Vector2(wightTexture, heightTexture);
                p2 *= new Vector2(wightTexture, heightTexture);

                Vector2 min = new Vector2();
                Vector2 max = new Vector2();



                min.x = Mathf.Min(p0.x, p1.x, p2.x);
                max.x = Mathf.Max(p0.x, p1.x, p2.x);

                min.y = Mathf.Min(p0.y, p1.y, p2.y);
                max.y = Mathf.Max(p0.y, p1.y, p2.y);

                for (int i = (int)min.x; i <= max.x; i++)
                {
                    for (int i2 = (int)min.y; i2 <= max.y; i2++)
                    {
                        bool side1 = Vector2.Dot(Vector2.Perpendicular(p0 - p1), new Vector2(i, i2) - p0) >= 0;
                        bool side2 = Vector2.Dot(Vector2.Perpendicular(p1 - p2), new Vector2(i, i2) - p1) >= 0;
                        bool side3 = Vector2.Dot(Vector2.Perpendicular(p2 - p0), new Vector2(i, i2) - p2) >= 0;
                        if (side1 == side2 && side2 == side3)
                        {
                            vectors.Add(new Vector2Int(i, i2));
                        }
                    }
                }
            }
            return vectors.ToArray();
        }

        private static Triangle[] GetConnectTriangle(int a, int b, int c, Mesh mesh)
        {
            int[] triangle = mesh.triangles;

            List<Triangle> triangleN = new List<Triangle>();
            List<Triangle> triangleCheck = new List<Triangle>();
            Triangle trigBase = new Triangle();
            for (int i = 0; i < mesh.triangles.Length / 3; i++)
            {
                triangleCheck.Add(new Triangle() { point1 = triangle[i * 3], point2 = triangle[i * 3 + 1], point3 = triangle[i * 3 + 2] });
                if (triangle[i * 3] == a && triangle[i * 3 + 1] == b && triangle[i * 3 + 2] == c)
                {
                    trigBase = triangleCheck[triangleCheck.Count - 1];
                }
            }

            for (int i = 0; i < triangleCheck.Count; i++)
            {
                for (int i2 = 0; i2 < triangleCheck.Count; i2++)
                {
                    if (i2 != i)
                    {
                        if (triangleCheck[i].connect.IndexOf(triangleCheck[i2]) == -1)
                        {
                            if (triangleCheck[i].point1 == triangleCheck[i2].point1 || triangleCheck[i].point1 == triangleCheck[i2].point2 || triangleCheck[i].point1 == triangleCheck[i2].point3)
                            {
                                triangleCheck[i].connect.Add(triangleCheck[i2]);

                            }
                            else if (triangleCheck[i].point2 == triangleCheck[i2].point1 || triangleCheck[i].point2 == triangleCheck[i2].point2 || triangleCheck[i].point2 == triangleCheck[i2].point3)
                            {
                                triangleCheck[i].connect.Add(triangleCheck[i2]);

                            }
                            else if (triangleCheck[i].point3 == triangleCheck[i2].point1 || triangleCheck[i].point3 == triangleCheck[i2].point2 || triangleCheck[i].point3 == triangleCheck[i2].point3)
                            {
                                triangleCheck[i].connect.Add(triangleCheck[i2]);

                            }
                        }
                    }
                }
            }
            return trigBase.GetConnect();
        }

        private class Triangle
        {
            public int point1;
            public int point2;
            public int point3;
            public bool isCheck = false;
            public List<Triangle> connect = new List<Triangle>();

            public Triangle[] GetConnect()
            {
                List<Triangle> triangles = new List<Triangle>();
                triangles.Add(this);
                isCheck = true;
                for (int i = 0; i < connect.Count; i++)
                {
                    if (!connect[i].isCheck)
                    {

                        triangles.AddRange(connect[i].GetConnect());
                    }
                }
                return triangles.ToArray();
            }
        }
    }
}
